DECLARE @keyname string;
DECLARE @key string; 
DECLARE @parent string;
DECLARE @stmt string;
DECLARE @ParentCur CURSOR AS
  SELECT * FROM tmpParent;

@keyname = 'Employeekey';
@key = '1767';


CREATE TABLE tmpParent (
  parent cichar(50));
  
CREATE TABLE tmpParentKeyCount (
  parent cichar(50),
  keycount integer);  
  
@stmt = 'INSERT INTO tmpParent SELECT left(parent, 50) FROM system.columns WHERE name = ' +  '''' + @keyname + '''';
EXECUTE immediate @stmt;


OPEN @ParentCur;
TRY
  WHILE FETCH @ParentCur DO
    @parent = @ParentCur.parent;
    @stmt = 'insert INTO tmpParentKeyCount SELECT ' + '''' + TRIM(@parent) + '''' + ', COUNT(*) FROM ' + '' + @Parent + '' 
      + ' WHERE cast( ' + '' + @keyname + '' + ' AS sql_char) = ' + '''' + @key + '''';
    EXECUTE immediate @stmt;
  END WHILE;
FINALLY
  CLOSE @ParentCur;
END TRY;

SELECT * FROM tmpParentKeyCount;

/*
DROP table tmpParent;
DROP TABLE tmpParentKeyCount;
*/
-- SELECT * FROM tmpparent

DELETE FROM keyMapDimEmp WHERE employeekey = 1767

UPDATE factClockHoursToday
SET employeekey = 1756
WHERE employeekey = 1767
