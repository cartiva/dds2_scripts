-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'integer,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'money,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'PDPPHDR' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'PDPPHDR'  
AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'PDPPHDR'  
AND data_type <> 'CHAR'

SELECT 
  'select MIN(' + trim(LEFT(column_name, 12)) + '), MAX(' + trim(LEFT(column_name, 12)) + ') FROM rydedata.pdpphdr'
from syscolumns
WHERE table_name = 'PDPPHDR'  
AND data_type <> 'CHAR'
  
-- DROP TABLE stgArkonaPDPPHDR
-- 3. revise DDL based ON step 2, CREATE table
CREATE TABLE stgArkonaPDPPHDR (
ptco#      cichar(3),          
ptpkey     integer,              
ptcpid     cichar(3),          
ptstat     cichar(1),          
ptdtyp     cichar(2),          
ptttyp     cichar(1),          
ptdate     date,              
ptdoc#     cichar(9),          
ptcus#     cichar(9),          
ptckey     integer,              
ptcnam     cichar(30),         
ptphon     cichar(10),              
ptskey     integer,              
ptpmth     cichar(2),          
ptstyp     cichar(1),          
ptplvl     integer,            
pttaxe     cichar(1),          
ptptot     money,              
ptshpt     money,              
ptspod     money,              
ptspdc     money,              
ptspdp     money,              
ptspdo     cichar(1),          
ptspoh     cichar(1),          
ptstax     money,              
ptstax2    money,              
ptstax3    money,              
ptstax4    money,              
ptcpnm     cichar(20),         
ptsrtk     cichar(20),         
ptactp     cichar(3),          
ptccar     cichar(9),          
ptpo#      cichar(12),         
ptrec#     integer,              
ptodoc     cichar(9),          
ptinta     cichar(10),         
ptrtyp     cichar(1),          
ptrsts     cichar(1),          
ptwaro     cichar(1),          
ptpaprv    cichar(1),          
ptsaprv    cichar(1),          
ptswid     cichar(3),          
ptrtch     cichar(3),          
pttest     money,              
ptsvco     money,              
ptsvco2    money,              
ptdeda     money,              
ptdeda2    money,              
ptwded     money,              
ptvin      cichar(17),         
ptstk#     cichar(9),          
pttrck     cichar(1),          
ptfran     cichar(3),          
ptodom     integer,              
ptmilo     integer,              
--pttmin     money,              
--ptpdtm     money,              
ptrtim     double,              
pttag#     cichar(5),          
ptchk#     cichar(7),          
pthcmt     cichar(20),         
ptssor     cichar(1),          
pthmor     cichar(1),          
ptltot     money,              
ptstot     money,              
ptdedp     money,
ptcphz     money,              
ptcpst     money,              
ptcpst2    money,              
ptcpst3    money,              
ptcpst4    money,              
ptwast     money,              
ptwast2    money,              
ptwast3    money,              
ptwast4    money,              
ptinst     money,              
ptinst2    money,              
ptinst3    money,              
ptinst4    money,              
ptscst     money,              
ptscst2    money,              
ptscst3    money,              
ptscst4    money,              
ptcpss     money,              
ptwass     money,              
ptinss     money,              
ptscss     money,              
pthcpn     integer,              
pthdsc     money,              
pttcdc     money,              
ptpbmf     money,              
ptpbdl     money,              
ptcptd     money,              
ptosts     cichar(1),          
ptdtar     date,              
ptarck     integer,              
ptwro#     integer,              
ptwsid     cichar(10),         
ptauth     cichar(6),      
ptapdt     integer,              
ptdtlc     integer,              
ptdtpi     integer,              
ptipty     cichar(3),          
ptprty     cichar(3),          
ptrnt#     integer,              
pttiin     cichar(1),          
ptcreate   timestamp,          
ptcthold   timestamp,          
ptauth#    cichar(15),         
ptauthid   cichar(10),         
ptdlvmth   integer,              
ptshipseq  integer,              
ptdspteam  cichar(10),         
pttaxgo    integer) IN database;            

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PDPPHDR';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PDPPHDR';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaPDPPHDR';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
 
