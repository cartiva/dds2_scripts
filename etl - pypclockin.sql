-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'integer,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'double,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'time,'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'PYPCLOCKIN' 
  AND column_name IN ('yico#', 'yiemp#', 'yiclkind', 'yiclkint', 'yiclkoutd', 'yiclkoutt', 'yicode')
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'PYPCLOCKIN'  
  AND column_name IN ('yico#', 'yiemp#', 'yiclkind', 'yiclkint', 'yiclkoutd', 'yiclkoutt', 'yicode')
  AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'PYPCLOCKIN'  
  AND column_name IN ('yico#', 'yiemp#', 'yiclkind', 'yiclkint', 'yiclkoutd', 'yiclkoutt', 'yicode')
  AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaPYPCLOCKIN
-- 3. revise DDL based ON step 2, CREATE table
CREATE TABLE stgArkonaPYPCLOCKIN (
yico#      cichar(3),          
yiemp#     cichar(7),          
yiclkind   date,               
yiclkint   time,               
yiclkoutd  date,               
yiclkoutt  time,               
yicode     cichar(3)) IN database;       

SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text, ordinal_position
INTO #Pypclockin 
from syscolumns
WHERE table_name = 'pypclockin'  
  AND column_name IN ('yico#', 'yiemp#', 'yiclkind', 'yiclkint', 'yiclkoutd', 'yiclkoutt', 'yicode');    
  
-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
@i = (
  SELECT MAX(ordinal_position)
  FROM #pymast);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = coalesce((SELECT '[' + TRIM(column_name) + '], '  FROM #pymast WHERE ordinal_position = @j), '');
-- just COLUMN names
--  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM #pymast WHERE ordinal_position = @j), '');
-- parameter names  
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM #pymast WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;     

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
@i = (
  SELECT MAX(ordinal_position)
  FROM #Pypclockin);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM #Pypclockin WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@i = (
  SELECT MAX(ordinal_position)
  FROM #Pypclockin);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM #Pypclockin WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaPYPCLOCKIN';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
 
-------------------------------------------------------------------------------- 
 
SELECT *
FROM stgArkonaPYPCLOCKIN 

SELECT CAST(yiclkind + ' ' + yiclkint AS sql_timestamp)
FROM stgArkonaPYPCLOCKIN 

SELECT yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt,
  timestampadd(sql_tsi_second, second(yiclkint), timestampadd(
    sql_tsi_minute, minute(yiclkint), timestampadd(
    sql_tsi_hour, hour(yiclkint), cast(yiclkind as sql_timestamp)))) AS ClockInTS,
  timestampadd(sql_tsi_second, second(yiclkoutt), timestampadd(
    sql_tsi_minute, minute(yiclkoutt), timestampadd(
    sql_tsi_hour, hour(yiclkoutt), cast(yiclkoutd as sql_timestamp)))) AS ClockOutTS,
  yicode    
FROM stgArkonaPYPCLOCKIN

SELECT yiemp#, yiclkind, sum(timestampdiff(sql_tsi_minute, ClockInTS, ClockOutTS))/ 60.0
FROM (
  SELECT yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt,
    timestampadd(sql_tsi_second, second(yiclkint), timestampadd(
      sql_tsi_minute, minute(yiclkint), timestampadd(
      sql_tsi_hour, hour(yiclkint), cast(yiclkind as sql_timestamp)))) AS ClockInTS,
    timestampadd(sql_tsi_second, second(yiclkoutt), timestampadd(
      sql_tsi_minute, minute(yiclkoutt), timestampadd(
      sql_tsi_hour, hour(yiclkoutt), cast(yiclkoutd as sql_timestamp)))) AS ClockOutTS,
    yicode    
  FROM stgArkonaPYPCLOCKIN) x
GROUP BY yiemp#, yiclkind  

-- 1/2/12
-- fact table at the granularity of store/emp#/clock in time
-- need to find those dups
-- rethinking granularity to be employee/day:
--   empKey
--   DayKey
--   RegTimeWorked
--   OTTimeWorked

DELETE FROM stgArkonaPYPCLOCKIN

SELECT yicode, COUNT(*)
FROM stgArkonaPYPCLOCKIN
GROUP BY yicode

-- yicode 666
-- ALL Ray Franks, ALL 666 rows have
SELECT *
FROM stgArkonaPYPCLOCKIN p
INNER JOIN (
  SELECT *
  FROM stgArkonaPYPCLOCKIN
  WHERE yicode = '666') x ON p.yico# = x.yico#
    AND p.yiemp# = x.yiemp#
    AND p.yiclkind = x.yiclkind
    
SELECT *
FROM edwEmployeeDim
WHERE employeenumber in ('148275', '178079')    

SELECT year(yiclkind), month(yiclkind), COUNT(*)
FROM tmpPYPCLOCKIN
GROUP BY year(yiclkind), month(yiclkind)

SELECT *
FROM tmpPYPCLOCKIN

-- getting rid of 666 got rid of most of the dups
SELECT yico#, yiemp#, yiclkind, yiclkint
FROM tmpPYPCLOCKIN
WHERE yicode <> '666'
GROUP BY yico#, yiemp#, yiclkind, yiclkint
  HAVING COUNT(*) > 1
-- remaining dups
SELECT *
FROM tmpPYPCLOCKIN
WHERE (
    (yiemp# = '148275' AND yiclkind = '10/06/2011') OR
    (yiemp# = '178079' AND yiclkind = '03/12/2010'))
  AND yiclkind = yiclkoutd  
  AND yiclkint <= yiclkoutt
  AND yicode <> '666'

DROP TABLE #jon  
SELECT yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, max(yiclkoutt) AS yiclkoutt, yicode
INTO #jon
FROM tmpPYPCLOCKIN
WHERE yicode <> '666'
GROUP BY yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode

SELECT COUNT(*) FROM #jon

-- bingo
SELECT yico#, yiemp#, yiclkind, yiclkint
FROM stgArkonaPYPCLOCKIN
GROUP BY yico#, yiemp#, yiclkind, yiclkint
  HAVING COUNT(*) > 1

  
SELECT *
FROM stgArkonaPYPCLOCKIN
WHERE yicode = 'I'



