SELECT a.thedate, b.*
FROM day a
LEFT JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
  AND b.employeekey > 1
WHERE a.thedate BETWEEN '06/17/2012' AND curdate()  

SELECT c.name, SUM(coalesce(FlagHours, 0)) AS FlagHours
FROM day a
LEFT JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
--  AND b.employeekey > 1
LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
WHERE b.employeekey IN (1351,1337,1325, 1270, 1261, 1249, 1198, 1170)
  AND a.thedate BETWEEN '06/17/2012' AND curdate()  
GROUP BY c.name

-- the point of the exercise IS to drill down to the differences BETWEEN HolyGraille
-- AND these numbers


/* 1. factTechLineFlagDateHours ***********************************************/


/* 2. total Clock/Flag Hours for a period **************************************/

------------------------------------------------------------------</ clock hours
-- first verify list of techs
/*
   extra: Reed - 583
6/3 - 6/16 
    missing Holwerda - 627 still IN distcode PTECH
    need an alternative way to identify techs to accomodate this kind of sitch
6/17 - 6/28 
    missing Mo Sampson 558: no emp# IN dimTech : see sp.maintDimTech
May 2012
    extra: Chad Gardner-251: 8.07 hours
    extra: Mitch Rogers-626: 22 hours
April 2012   
    Girodat, Heffernan, Yanish, Hunter 
March 2012 right on    
*/
-- employeenumber for techs
-- need employeekey for hours
-- clock hours
SELECT b.name, b.employeenumber, c.technumber, SUM(d.clockhours) AS clockhours
FROM day a
LEFT JOIN edwEmployeeDim b ON b.employeekeyfromdate <= a.thedate 
  AND b.employeekeythrudate BETWEEN a.thedate AND '12/31/9999'
  AND b.storecode = 'ry1'
  AND (b.distcode IN ('STEC','SRVM') OR b.employeenumber = '167580') -- Holwerda - 627 still IN distcode PTECH
LEFT JOIN dimTech c ON b.storecode = c.storecode
  AND b.employeenumber = c.employeenumber  
LEFT JOIN edwClockHoursFact d ON a.datekey = d.datekey
  AND b.employeekey = d.employeekey  
WHERE a.thedate BETWEEN '06/03/2012' AND '06/16/2012'
  AND c.storecode IS NOT NULL 
GROUP BY b.name, b.employeenumber, c.technumber
order BY c.technumber

------------------------------------------------------------------/> clock hours  


-------------------------------------------------------------------</ flag hours
/*
6/3 - 6/16
tech#     spreadsheet  query       diff (query - spreadsheet)
511       70.37        70.67       0.3
526       41.73        43.1        1.37
532       29.4         33.4        4.0
557       56.47        59.87       3.4
572       62.8         53.8        -9.0
573       45.78        45.98       0.2
599       22.6         missing
623       87.11        87.81       0.7
627       15.25        missing
631       77.57        75.57       -2.0                                            
*/
SELECT c.Name, c.TechNumber, sum(b.FlagHours)
FROM day a
LEFT JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
LEFT JOIN dimTech c ON b.TechKey = c.TechKey
  AND c.StoreCode = 'ry1'
WHERE a.thedate BETWEEN '06/03/2012' AND '06/16/2012'
  AND c.employeenumber IS NOT NULL 
  AND b.EmployeeKey IN (
    SELECT EmployeeKey
    FROM edwEmployeeDim 
    WHERE storecode = 'ry1'
      AND (distcode IN ('STEC','SRVM') OR employeenumber = '167580')) --Holwerda - 627 still IN distcode PTECH   
GROUP BY c.Name, c.TechNumber
ORDER BY TechNumber

-- diffs
SELECT d.technumber, a.thedate, b.ro, SUM(b.flaghours)
FROM day a
LEFT JOIN factTechLineFlagDateHours b ON a.datekey = b.flagdatekey
INNER JOIN bridgeTechGroup c ON b.TechGroupKey = c.TechGroupKey
INNER JOIN dimTech d ON c.techkey = d.techkey
  AND d.technumber = '572'
WHERE a.thedate BETWEEN '06/03/2012' AND '06/16/2012'
GROUP BY d.technumber, a.thedate, b.ro
ORDER BY b.ro
-------------------------------------------------------------------/> flag hours
  
/* 2. total Clock/Flag Hours for a period *******************************************/  

-- flagdate > finalclosedate
SELECT a.ro, a.closedate, b.finalclosedate, c.flagdate
FROM (
  SELECT ro, b.thedate AS closedate
  FROM factro a
  LEFT JOIN day b ON a.closedatekey = b.datekey
  WHERE b.thedate <> '12/31/9999') a
LEFT JOIN (
  SELECT ro, b.thedate AS finalclosedate
  FROM factro a
  LEFT JOIN day b ON a.finalclosedatekey = b.datekey
  WHERE b.thedate <> '12/31/9999') b ON a.ro = b.ro
LEFT JOIN (
  SELECT ro, MAX(thedate) AS flagdate
  FROM facttechlineflagdatehours a
  LEFT JOIN day b ON a.flagdatekey = b.datekey
  GROUP BY ro) c ON a.ro = c.ro
WHERE c.flagdate > b.finalclosedate  