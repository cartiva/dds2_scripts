DECLARE @FromDate date;
DECLARE @ThruDate date;

@FromDate = '03/11/2012';
@ThruDate = '03/24/2012';

--SELECT * FROM edwEmployeeDim 
-- what IF a tech has a change IN wage during interval
-- doesn't matter, taking the last empkey IN the interval
-- clock hours AND pay
SELECT a.thedate, a.empnum, a.RegHours, a.OTHours, a.TotalHours,
  ex.HourlyRate * a.RegHours AS RegPay, ex.HourlyRate * a.OTHours * 1.5 AS OTPay,
  (ex.HourlyRate * a.RegHours) + (ex.HourlyRate * a.OTHours * 1.5) AS TotalPay 
FROM(
  SELECT -- a: 1 row per store/emp/day
    e.EmployeeKey, e.storecode, d.thedate, sum(c.RegularHours) AS RegHours, sum(c.OverTimeHours) AS OTHours,
    sum(c.RegularHours) + sum(c.OverTimeHours) AS TotalHours,
    e.employeenumber AS EmpNum, year(d.theDate) AS "Year", week(d.theDate) AS "Week"
  FROM day d
  INNER JOIN edwClockHoursFact c ON d.datekey = c.datekey
  INNER JOIN edwEmployeeDim e ON c.EmployeeKey = e.EmployeeKey
    AND e.storecode = 'RY1'
    AND e.technumber IS NOT NULL
    AND e.storecode = 'RY1'
    AND distcode IN ('STEC','PR/S') 
    AND EmployeeKeyFromDate = (
      SELECT MAX(EmployeeKeyFromDate)
      FROM edwEmployeeDim
      WHERE EmployeeKey = e.EmployeeKey
        AND EmployeeKeyFromDate < @ThruDate) 
  WHERE d.theDate BETWEEN @FromDate AND @ThruDate
  GROUP BY d.thedate, e.employeenumber, e.storecode, e.employeekey) a
LEFT JOIN edwEmployeeDim ex ON a.EmployeeKey = ex.EmployeeKey


 
 
-- techs    
select sttech, ymname                                                              
                     from rydedata.sdptech t                                                     
                       inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn 
                     where t.stco# = 'RY1'
                       and p.ymdist in ('STEC', 'PR/S')
                       and not left(sttech, 1) in ('H', 'M')    
                       
SELECT * FROM edwEmployeeDim 
                       
SELECT distinct name, technumber
FROM edwEmployeeDim e
WHERE technumber IS NOT NULL
  AND storecode = 'RY1'
  AND distcode IN ('STEC','PR/S') 
  AND EmployeeKeyFromDate = (
    SELECT MAX(EmployeeKeyFromDate)
    FROM edwEmployeeDim
    WHERE EmployeeKey = e.EmployeeKey
      AND EmployeeKeyFromDate < '04/02/2012')
ORDER BY technumber  
   
   
-- flagged hours
select 
  date(substr(trim(cast(x."Date" as char(12))), 5, 2) || '/' || right(trim(cast(x."Date" as char(12))), 2) || '/' || left(trim(cast(x."Date" as char(12))), 4)) as "Date", 
  x."Tech", p.ymempn as "EmpNum", trim(p.ymname) as "Name", sum(x."Flagged") as "Flagged"
from ( 
  select xxx.ptdate as "Date", xxx.pttech as "Tech", sum(xxx.ptlhrs) as "Flagged"
  from (
    select d.ptco#, ptdoc# as ptro#, ptline, ptseq#, d.ptdate, pttech, ptlhrs
    from rydedata.pdppdet d                                                                      
    inner join rydedata.pdpphdr h on h.ptco# = d.ptco# and h.ptpkey = d.ptpkey                 
    where                                                                                        
      ptcode = 'TT'
      and ptspcd <> 'V' -- void?     
      and d.ptdate <> 0
      and pttech in (
        select sttech                                                              
        from rydedata.sdptech t                                                     
        inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn 
        where t.stco# = 'RY1'
        and p.ymdist in ('STEC', 'PR/S')
        and not left(sttech, 1) in ('H', 'M'))
    union                              
    select ptco#, ptro#, ptline, ptseq#, ptdate, pttech, ptlhrs
    from rydedata.sdprdet                                                                        
    where --ptdate = 20111010                                                                    
      ptcode = 'TT'
      and ptdbas <> 'V' -- void? 
      and ptdate <> 0
      and pttech in (
        select sttech                                                            
        from rydedata.sdptech t                                                     
        inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn   
        where t.stco# = 'RY1'
          and p.ymdist in ('STEC', 'PR/S')
          and not left(sttech, 1) in ('H', 'M'))) as xxx
    group by xxx.ptdate, xxx.pttech, xxx.ptro#) as x
  inner join rydedata.sdptech s on s.stco# = 'RY1' and s.sttech = x."Tech"                     
  inner join rydedata.pymast p on p.ymco# = 'RY1' and p.ymempn = s.stpyemp#
group by x."Date", x."Tech", p.ymempn, p.ymname   


--SELECT ptdoc#, ptline, ptseq#, pttech FROM (
SELECT d.ptco# AS ptro#, h.ptdoc#, d.ptline, d.ptseq#, d.ptdate, d.pttech, d.ptlhrs
FROM stgArkonaPDPPDET d
INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
WHERE ptcode = 'TT'
  AND d.ptlhrs <> 0
--) x GROUP BY ptdoc#, ptline, ptseq#, pttech HAVING COUNT(*) > 1  

--SELECT ptro#, ptline, ptseq#, pttech FROM (
SELECT ptco#, ptro#, ptline, ptseq#, ptdate, pttech, ptlhrs
FROM stgArkonaSDPRDET
WHERE ptdate IS NOT NULL 
  AND ptcode = 'TT'
  AND ptlhrs <> 0
--) x GROUP BY ptro#, ptline, ptseq#, pttech HAVING COUNT(*) > 1  
  

-- DROP TABLE #wtf
SELECT * INTO #wtf
FROM (
SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptseq#, d.ptdate, d.pttech, d.ptlhrs
FROM stgArkonaPDPPDET d
INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
WHERE ptcode = 'TT'
UNION 
SELECT ptco#, ptro#, ptline, ptseq#, ptdate, pttech, ptlhrs
FROM stgArkonaSDPRDET
WHERE ptdate IS NOT NULL 
  AND ptcode = 'TT'
  AND ptlhrs <> 0) x

-- bingo  
SELECT ptro#, ptline, ptseq#, pttech FROM #wtf GROUP BY ptro#, ptline, ptseq#, pttech HAVING COUNT(*) > 1

SELECT * FROM #wtf 

-- date/tech/ro/line - hours
-- DROP TABLE #jon
SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
INTO #jon
FROM (
  SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptseq#, d.ptdate, d.pttech, 
    CASE 
      WHEN x.ptpkey IS NULL THEN d.ptlhrs
      ELSE x.ptlhrs
    END AS ptlhrs
  FROM stgArkonaPDPPDET d
  INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
  LEFT JOIN (
    SELECT ptco#, ptpkey, ptro#, ptline, pttech, round(sum(ptlhrs), 2) AS ptlhrs
    FROM stgArkonaSDPXTIM 
    GROUP BY ptco#, ptpkey, ptro#, ptline, pttech) x ON h.ptdoc# = x.ptro#
      AND d.ptline = x.ptline 
  WHERE d.ptcode = 'TT'
    AND d.ptdate BETWEEN '03/11/2012' AND curdate()
  UNION 
  SELECT ptco#, ptro#, ptline, ptseq#, ptdate, pttech, ptlhrs
  FROM stgArkonaSDPRDET
  WHERE ptdate IS NOT NULL 
    AND ptcode = 'TT'
    AND ptlhrs <> 0
    AND ptdate BETWEEN '03/11/2012' AND curdate()) x
GROUP BY ptco#, ptdate, pttech, ptro#, ptline  

select * FROM #jon
-- ro 16084647 line 1 SDPXTIM
-- ads IS .2 higher than ark flag report for tech 511 ON 4/3/12
-- needs to be adjusted at the line level
SELECT *
FROM #jon
WHERE pttech = '511'
  AND ptdate = '04/03/2012'
  
SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16084647' 
  AND ptcode = 'TT'

the total number of hours needs to include sdpxtim
-- 1 ADD ptline to #jon
-- oo, DO i need to ADD ptpkey
-- nope, i have ro AND line, but should have store


-- store/date/tech/ro/line - hours
-- added xtim
-- ADD open/closed
-- DROP TABLE #jon

SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(hours), 2) AS FlagHours
INTO #jon
FROM (
  SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptseq#, d.ptdate, d.pttech, 
    CASE 
      WHEN x.ptpkey IS NULL THEN d.ptlhrs
      ELSE x.ptlhrs
    END AS hours
  FROM stgArkonaPDPPDET d
  INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
  LEFT JOIN (
    SELECT ptco#, ptdate, ptpkey, ptro#, ptline, pttech, round(sum(ptlhrs), 2) AS ptlhrs
    FROM stgArkonaSDPXTIM 
    GROUP BY ptco#, ptdate, ptpkey, ptro#, ptline, pttech) x ON h.ptco# = x.ptco#
      AND h.ptdoc# = x.ptro#
      AND d.ptline = x.ptline 
      AND d.ptdate = x.ptdate
  WHERE d.ptcode = 'TT'
    AND d.ptdate BETWEEN '03/11/2012' AND '03/24/2012'
  UNION 
  SELECT s.ptco#, s.ptro#, s.ptline, s.ptseq#, s.ptdate, s.pttech, 
    CASE 
      WHEN x1.ptpkey IS NULL THEN  s.ptlhrs
      ELSE x1.ptlhrs
    END AS hours
  FROM stgArkonaSDPRDET s
  LEFT JOIN (
    SELECT ptco#, ptdate, ptpkey, ptro#, ptline, pttech, round(sum(ptlhrs), 2) AS ptlhrs
    FROM stgArkonaSDPXTIM 
    GROUP BY ptco#, ptpkey, ptdate, ptro#, ptline, pttech) x1 ON s.ptco# = x1.ptco#
      AND s.ptro# = x1.ptro#
      AND s.ptline = x1.ptline 
      AND s.ptdate = x1.ptdate
  WHERE s.ptdate IS NOT NULL 
    AND s.ptcode = 'TT'
    AND s.ptdate BETWEEN '03/11/2012' AND '03/24/2012') x
WHERE hours <> 0    
GROUP BY ptco#, ptdate, pttech, ptro#, ptline  

SELECT ptco#, ptro#, pttech, SUM(FlagHours)
FROM #jon
WHERE ptco# = 'ry1'
  AND pttech in ('511', '545', '577')
GROUP BY ptco#, ptro#, pttech  
ORDER BY pttech, ptro#

SELECT *
FROM stgArkonaSDPRDET
WHERE pttech = '545'
  AND ptro# = '16083781'
  
SELECT h.ptdoc#, d.ptline, ptseq#, ptcode, h.ptdate, d.ptdate, d.pttech, d.ptlhrs, 
  x.*
FROM stgArkonaPDPPDET d
INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
LEFT JOIN stgArkonasdpxtim x ON h.ptdoc# = x.ptro# AND d.ptline = x.ptline AND d.ptdate = x.ptdate
WHERE h.ptdoc# = '16083781'
  AND d.pttech = '545'
  AND ptcode = 'TT'
ORDER BY d.ptline, d.ptseq#


ptspcd: special code
4/4/12 ALL are null
SELECT ptspcd, COUNT(*)
FROM stgArkonaPDPPDET
GROUP BY ptspcd


-- conclusion, the data can NOT be used to absolutely determine the last payrollendingdate  
-- across the entire organizaion
-- the date will have to be entered AS a parameter  
-- the last payrollending date IS used to determine the hours worked BETWEEN that date
-- AND the EOM, this IS what need to be accrued
-- temp tables: #emp, #clockhours, #totals
DECLARE @ThruDate date; // Last day of month
DECLARE @FromDate date; // Last payroll ending date prior to EOM
DECLARE @NumDays integer;
@FromDate = '1/14/2012';
@ThruDate = (
  SELECT thedate 
  FROM day WHERE LastDayOfMonth = true 
    AND month(thedate) = month(@FromDate) 
    AND year(thedate) = year(@FromDate));
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate);


-- bi-weekly pay period END dates
SELECT thedate, DayName, mod(cast(ABS(timestampdiff(sql_tsi_day, thedate, '03/24/2012')) AS sql_integer), 14)
FROM day
WHERE thedate BETWEEN '01/01/2011' AND curdate()
  AND mod(cast(ABS(timestampdiff(sql_tsi_day, thedate, '03/24/2012')) AS sql_integer), 14) = 0  
  
-- bi-weekly pay period start dates
SELECT thedate, DayName, mod(cast(ABS(timestampdiff(sql_tsi_day, thedate, '03/11/2012')) AS sql_integer), 14)
FROM day
WHERE thedate BETWEEN '01/01/2011' AND curdate()
  AND mod(cast(ABS(timestampdiff(sql_tsi_day, thedate, '03/11/2012')) AS sql_integer), 14) = 0  
 
-- ALL entries IN SDPXTIM cancel
SELECT * FROM (
SELECT ptco#, ptpkey, ptro#, ptline, ptlpym, pttech, round(SUM(ptlhrs), 2) AS hours
FROM stgArkonaSDPXTIM  
GROUP BY ptco#, ptpkey, ptro#, ptline, ptlpym, pttech) x
WHERE hours <> 0

-- here we go again
-- trying to get sdpxtim correct
-- DROP TABLE #jon
-- 4/7 DISTINCT sdpxtim
SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(hours), 2) AS FlagHours
INTO #jon
FROM (
  SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptseq#, d.ptdate, d.pttech, 
    coalesce(d.ptlhrs, 0) + coalesce(x.ptlhrs, 0) AS hours
  FROM stgArkonaPDPPDET d
  INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
  LEFT JOIN (
      SELECT ptco#, ptdate, ptro#, ptline, pttech, round(SUM(ptlhrs),2) AS ptlhrs
      FROM (
        SELECT DISTINCT ptco#, ptro#, ptline, ptdate, pttech, ptlhrs
        FROM stgArkonaSDPXTIM) yy  
      WHERE ptlhrs < 0
      GROUP BY ptco#, ptdate, ptro#, ptline, pttech) x ON h.ptco# = x.ptco#
        AND h.ptdoc# = x.ptro#
        AND d.ptline = x.ptline 
        AND d.ptdate = x.ptdate
        AND d.pttech = x.pttech
  WHERE d.ptcode = 'TT'
    AND d.ptdate BETWEEN '03/11/2012' AND '03/24/2012'
  UNION 
  SELECT s.ptco#, s.ptro#, s.ptline, s.ptseq#, s.ptdate, s.pttech, 
    coalesce(s.ptlhrs, 0) + coalesce(x1.ptlhrs, 0) as hours
  FROM stgArkonaSDPRDET s
  LEFT JOIN (
      SELECT ptco#, ptdate, ptro#, ptline, pttech, round(SUM(ptlhrs),2) AS ptlhrs
      FROM (
        SELECT DISTINCT ptco#, ptro#, ptline, ptdate, pttech, ptlhrs
        FROM stgArkonaSDPXTIM) xx 
      WHERE ptlhrs < 0
      GROUP BY ptco#, ptdate, ptro#, ptline, pttech) x1 ON s.ptco# = x1.ptco#
        AND s.ptro# = x1.ptro#
        AND s.ptline = x1.ptline 
        AND s.ptdate = x1.ptdate
        AND s.pttech = x1.pttech
  WHERE s.ptdate IS NOT NULL 
    AND s.ptcode = 'TT'
    AND s.ptdate BETWEEN '03/11/2012' AND '03/24/2012') y
WHERE hours <> 0    
GROUP BY ptco#, ptdate, pttech, ptro#, ptline  

SELECT * FROM #jon

SELECT ptco#, ptro#, pttech, SUM(FlagHours)
FROM #jon
WHERE ptco# = 'ry1'
  AND pttech in ('511', '545', '577')
GROUP BY ptco#, ptro#, pttech  
ORDER BY pttech, ptro#

-- 511 : 3/15 : 16083709 : ads:1.9, ark flag report: 1.5
SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16083802'
  AND ptcode = 'TT'
  AND pttech = '577'
  AND ptlhrs <> 0
  
SELECT h.ptdoc#, d.ptline, ptseq#, ptcode, h.ptdate, d.ptdate, d.pttech, d.ptlhrs
FROM stgArkonaPDPPDET d
INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey  
WHERE h.ptdoc# = '16083443'
  AND ptlhrs <> 0
  
SELECT *
FROM stgArkonaSDPXTIM
WHERE ptro# = '16083781'
WHERE pttech = '511'
  AND ptdate = '03/15/2012'
  


SELECT *
FROM stgArkonaPDPPDET
WHERE ptro# = '16083800'
  


SELECT ptline AS Sline, ptdate AS Sdate, ptlhrs AS Shours
FROM stgArkonaSDPRDET
WHERE ptro# = '16083781'
  AND ptcode = 'TT'
  AND pttech = '545'
  AND ptlhrs <> 0
  
  
SELECT d.ptline AS Pline, d.ptdate AS Pdate, d.ptlhrs AS Phours
FROM stgArkonaPDPPDET d
INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey  
WHERE h.ptdoc# = '16083781'
  AND d.ptcode = 'TT'
  AND d.pttech = '545'
  AND ptlhrs <> 0
  
SELECT ptline AS Xline, ptdate AS Xdate, ptlhrs AS XHours
-- SELECT *
FROM stgArkonaSDPXTIM
WHERE ptro# = '16083781'  
  
  
SELECT s.ptline AS Sline, s.ptdate AS Sdate, s.ptlhrs AS Shours,
  x.ptline AS Xline, x.ptdate AS Xdate, x.ptlhrs AS XHours
FROM stgArkonaSDPRDET s, stgArkonaSDPXTIM x
WHERE s.ptro# = '16083781'
  AND s.ptcode = 'TT'
  AND s.pttech = '577'
  AND s.ptlhrs <> 0  
  AND x.ptro# = '16083781'
  AND x.pttech = '577'
  AND s.ptline = x.ptline
  
  
SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16083152'
ORDER BY ptline, ptseq#  


SELECT d.ptro#, d.ptline AS Pline, d.ptdate AS Pdate, d.ptlhrs AS Phours,
  x.ptline AS Xline, x.ptdate AS Xdate, x.ptlhrs AS XHours
FROM stgArkonaSDPRDET d
left JOIN stgArkonaSDPXTIM x ON d.ptro# = x.ptro#
  AND d.ptline = x.ptline
  AND d.pttech = x.pttech
--WHERE d.ptdate BETWEEN '03/11/2012' AND '03/24/2012'
WHERE d.ptro# = '16083781'
  AND d.ptcode = 'TT'

-- for the entire period 3/11 - 3/24  
SELECT pttech, round(SUM(flaghours), 2) 
FROM #jon  
WHERE ptco# = 'ry1'
GROUP BY pttech

-- daily BY tech
SELECT ptro#, sum(flaghours)
FROM #jon
WHERE pttech = '506'
GROUP BY ptro#
ORDER BY ptro#

-- individual ro/tech
  -- sdprdet
SELECT ptdate, ptlhrs, ptro#, ptline, ptseq#
SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16083402'
  AND ptro# = '16083443'
  AND ptcode = 'TT'
  AND ptlhrs <> 0
  -- sdpxtim
SELECT ptdate, ptlhrs, ptro#, ptline
FROM stgArkonaSDPXTIM
WHERE pttech = '502'
  AND ptro# = '16083518'
  -- pdppdet  
SELECT d.ptdate, d.ptlhrs, h.ptdoc#, d.ptline, d.ptseq#
FROM stgArkonaPDPPDET d
INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey  
WHERE h.ptdoc# = '16083518'
  AND d.ptcode = 'TT'
  AND d.pttech = '502'
  AND ptlhrs <> 0      
  

-- DROP TABLE #jon   
--  going back to thinking, UNION xtim
-- 4/8, so what does that look LIKE
-- #jon
-- remove ptseq# FROM sdprdet & pdppdet
-- 4/9
-- 16084086/506: tech has hours ON 2 seq for same line
-- so the union with subselect ON sdprdet discards one of the lines
-- so, GROUP the sdprdet subselect AND SUM the hours

SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
INTO #jon
FROM (
    SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptdate, d.pttech, 
      coalesce(d.ptlhrs, 0) AS ptlhrs
    FROM stgArkonaPDPPDET d
    INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
      AND d.ptdate BETWEEN '03/11/2012' AND '03/24/2012'
  UNION 
    SELECT s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM stgArkonaSDPRDET s
    WHERE s.ptdate IS NOT NULL 
      AND s.ptcode = 'TT'
      AND s.ptdate BETWEEN '03/11/2012' AND '03/24/2012'
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech
  UNION 
    SELECT ptco#, ptro#, ptline, ptdate, pttech, round(SUM(ptlhrs),2) AS ptlhrs
    FROM (
      SELECT DISTINCT ptco#, ptro#, ptline, ptdate, pttech, coalesce(ptlhrs, 0 ) AS ptlhrs
      FROM stgArkonaSDPXTIM) xx 
    WHERE xx.ptlhrs < 0
      AND xx.ptdate BETWEEN '03/11/2012' AND '03/24/2012'
    GROUP BY ptco#, ptdate, ptro#, ptline, pttech) y  
WHERE ptlhrs <> 0    
GROUP BY ptco#, ptdate, pttech, ptro#, ptline  

SELECT ptro#, SUM(flaghours) 
FROM #jon
WHERE pttech = '557'
GROUP BY ptro#
order BY ptro#

-- ALL 3 tables together
SELECT *
FROM ( 
  SELECT *
  FROM ( 
    SELECT 'SDPRDET' AS source, ptro#, ptline, ptdate, pttech, sum(coalesce(ptlhrs, 0))
    FROM stgArkonaSDPRDET
    WHERE ptcode = 'TT'
      AND ptlhrs <> 0
    GROUP BY ptro#, ptline, ptdate, pttech) a
  UNION 
  SELECT 'SDPXTIM' AS source, ptro#, ptline, ptdate, pttech, ptlhrs
  FROM stgArkonaSDPXTIM
  UNION 
  SELECT 'PDPPDET' AS source, h.ptdoc# AS ptro#, d.ptline, d.ptdate, d.pttech, d.ptlhrs
  FROM stgArkonaPDPPDET d
  INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey  
  WHERE  d.ptcode = 'TT'
    AND d.ptlhrs <> 0) x
WHERE ptro# = '16078029'
  AND pttech = '631' 
  
SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16083617'  
  AND ptcode = 'tt'
  
SELECT *
FROM stgArkonaSDPXTIM
WHERE ptro# = '16083617'  
  
 


-- DROP TABLE #jon   
--  going back to thinking, UNION xtim
-- 4/8, so what does that look LIKE
-- #jon
-- remove ptseq# FROM sdprdet & pdppdet
-- 4/9
-- 16084086/506: tech has hours ON 2 seq for same line
-- so the union with subselect ON sdprdet discards one of the lines
-- so, GROUP the sdprdet subselect AND SUM the hours
-- verify existence of an offseting base record to include sdpxtim
-- have to get rid of the big anomaly 536: 536 hours
SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
INTO #jon
FROM (
    SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptdate, d.pttech, 
      coalesce(d.ptlhrs, 0) AS ptlhrs
    FROM stgArkonaPDPPDET d
    INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
      AND d.ptdate BETWEEN '04/09/2012' AND '04/12/2012'
  UNION 
    SELECT s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM stgArkonaSDPRDET s
    WHERE s.ptdate IS NOT NULL 
      AND s.ptcode = 'TT'
      AND s.ptdate BETWEEN  '04/09/2012' AND '04/12/2012'
      AND s.ptlhrs < 300
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech
  UNION -- negative xtim adj
  SELECT ptco#, ptro#, ptline, ptdate, pttech, round(SUM(ptlhrs),2) AS ptlhrs
  FROM (
    SELECT DISTINCT ptco#, ptpkey, ptro#, ptline, ptdate, pttech, coalesce(ptlhrs, 0 ) AS ptlhrs
    FROM stgArkonaSDPXTIM) xx 
  WHERE xx.ptlhrs < 0
    AND xx.ptdate BETWEEN  '04/09/2012' AND '04/12/2012'
    AND 
      EXISTS (
        SELECT 1
        FROM stgArkonaSDPRDET
        WHERE ptco# = xx.ptco#
          AND ptro# = xx.ptro#
          AND ptline = xx.ptline
          AND pttech = xx.pttech
          AND abs(ptlhrs) = abs(xx.ptlhrs)) 
      OR EXISTS (  
      SELECT 1
        FROM stgArkonaPDPPDET d
        WHERE d.ptco# = xx.ptco#
          AND d.ptpkey = xx.ptpkey
          AND d.ptline = xx.ptline
          AND d.pttech = xx.pttech
          AND abs(d.ptlhrs) = abs(xx.ptlhrs))     
  GROUP BY ptco#, ptdate, ptro#, ptline, pttech) y  
WHERE ptlhrs <> 0    
GROUP BY ptco#, ptdate, pttech, ptro#, ptline;


-- for the entire period   
SELECT pttech, round(SUM(flaghours), 2) 
FROM #jon  
WHERE ptco# = 'ry1'
GROUP BY pttech


SELECT ptro#, SUM(flaghours) 
FROM #jon
WHERE pttech = '536'
GROUP BY ptro#
order BY ptro#  

-- ALL 3 tables together
SELECT *
FROM ( 
  SELECT *
  FROM ( 
    SELECT 'SDPRDET' AS source, ptro#, ptline, ptdate, pttech, sum(coalesce(ptlhrs, 0))
    FROM stgArkonaSDPRDET
    WHERE ptcode = 'TT'
      AND ptlhrs <> 0
    GROUP BY ptro#, ptline, ptdate, pttech) a
  UNION 
  SELECT 'SDPXTIM' AS source, ptro#, ptline, ptdate, pttech, ptlhrs
  FROM stgArkonaSDPXTIM
  UNION 
  SELECT 'PDPPDET' AS source, h.ptdoc# AS ptro#, d.ptline, d.ptdate, d.pttech, d.ptlhrs
  FROM stgArkonaPDPPDET d
  INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey  
  WHERE  d.ptcode = 'TT'
    AND d.ptlhrs <> 0) x
WHERE ptro# = '16085860'
  AND pttech = '623'

--  AND EXISTS (
--    SELECT 1 
--    FROM (
--      SELECT 'PDPPDET' AS source, h.ptdoc# AS ptro#, d.ptline, d.ptdate, d.pttech, d.ptlhrs
--      FROM stgArkonaPDPPDET d
--      INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey  
--      WHERE  d.ptcode = 'TT'
--        AND d.ptlhrs <> 0) z 
--    WHERE z.ptro# = x.ptro#)       

ORDER BY pttech, ptro#, source
  
  
SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16083960'  
  AND ptcode = 'tt'
  
SELECT *
FROM stgArkonaSDPXTIM
WHERE ptro# = '16085210'   

SELECT *
FROM #jon
WHERE ptro# = '16083736'

SELECT *
FROM stgArkonaGlptrns
WHERE gtdoc# = '16085364'

DROP TABLE zzzjontest

select *
from stgArkonaSDPTECH
ORDER BY stco#, sttech

-- DROP TABLE #jon   
--  going back to thinking, UNION xtim
-- 4/8, so what does that look LIKE
-- #jon
-- remove ptseq# FROM sdprdet & pdppdet
-- 4/9
-- 16084086/506: tech has hours ON 2 seq for same line
-- so the union with subselect ON sdprdet discards one of the lines
-- so, GROUP the sdprdet subselect AND SUM the hours
-- verify existence of an offseting base record to include sdpxtim
-- have to get rid of the big anomaly 536: 536 hours
-- 4/13/12: at the point WHERE i say fuck xtim
-- oh shit, just got jeremies email
-- BUT, it think what he IS saying IS that sdpxtim IS only for payroll
-- so i don't need it
SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
INTO #jon
FROM (
    SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptdate, d.pttech, 
      coalesce(d.ptlhrs, 0) AS ptlhrs
    FROM stgArkonaPDPPDET d
    INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
      AND d.ptdate BETWEEN '03/11/2012' AND '03/24/2012'
  UNION 
    SELECT s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM stgArkonaSDPRDET s
    WHERE s.ptdate IS NOT NULL 
      AND s.ptcode = 'TT'
      AND s.ptdate BETWEEN  '03/11/2012' AND '03/24/2012'
      AND s.ptlhrs < 300
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) y  
WHERE ptlhrs <> 0    
GROUP BY ptco#, ptdate, pttech, ptro#, ptline;

SELECT ptco#, pttech, ptro#, ptline
FROM #jon
GROUP BY ptco#, pttech, ptro#, ptline
HAVING COUNT(*) > 1

SELECT *
FROM #jon 
WHERE ptro# = '16085748'

-- can a line have more than one tech?
-- i sure think so, now prove it
  SELECT ptco#, ptro#, ptline, ptdate, pttech, round(SUM(ptlhrs),2) AS ptlhrs
  FROM (
    SELECT DISTINCT ptco#, ptpkey, ptro#, ptline, ptdate, pttech, coalesce(ptlhrs, 0 ) AS ptlhrs
    FROM stgArkonaSDPXTIM) xx 
  WHERE xx.ptdate BETWEEN  '03/11/2012' AND '03/24/2012'
  GROUP BY ptco#, ptdate, ptro#, ptline, pttech
  
SELECT j.pttech, j.ptro#, x.ptdate, x.ptlhrs
FROM (
  SELECT ptro#, pttech, round(sum(FlagHours), 2)
  from #jon
  WHERE pttech IN ('502','506','511','519','522','526','528','532','536','537',
    '540','542','545','557','566','572','573','574','575','577','578','583',
    '585','595','596','598','608','610','611','623','624','625','631')
  GROUP BY ptro#, pttech) j
LEFT JOIN stgArkonaSDPXTIM x ON j.ptro# = x.ptro#
  AND j.pttech = x.pttech
WHERE EXISTS (
  SELECT 1
  FROM stgArkonaSDPXTIM
  WHERE ptco# = x.ptco#
    AND ptro# = x.ptro# 
    AND ptline = x.ptline
    AND pttech = x.pttech) 
ORDER BY j.pttech, j.ptro#    
    
SELECT pttech, round(SUM(flaghours), 2) 
FROM #jon  
WHERE ptco# = 'ry1'
GROUP BY pttech
    

SELECT *
FROM stgArkonaSDPXTIM
WHERE ptro# = '16079837'

-- 4/14/12
-- those sdpxtim records with different dates
select *
FROM stgArkonaSDPXTIM xx
WHERE EXISTS(
  SELECT 1
  FROM stgArkonaSDPXTIM  
  WHERE ptco# = xx.ptco#
    AND ptpkey = xx.ptpkey
    AND ptro# = xx.ptro#
    AND ptline = xx.ptline
    AND pttech = xx.pttech
    AND ABS(ptlhrs) = abs(xx.ptlhrs)
    AND ptdate <> xx.ptdate)
AND pttech IN ('502','506','511','519','522','526','528','532','536','537',
    '540','542','545','557','566','572','573','574','575','577','578','583',
    '585','595','596','598','608','610','611','623','624','625','631')   
AND ptdate > '12/31/2009'     
ORDER BY ptro#, ptline   
-- jeremy xcel sdpxtim
SELECT distinct x.pttech, x.ptro#, x.ptline, x.ptdate, x.ptlhrs
FROM #jon j
LEFT JOIN (
  select *
  FROM stgArkonaSDPXTIM xx
  WHERE EXISTS(
    SELECT 1
    FROM stgArkonaSDPXTIM  
    WHERE ptco# = xx.ptco#
      AND ptpkey = xx.ptpkey
      AND ptro# = xx.ptro#
      AND ptline = xx.ptline
      AND pttech = xx.pttech
      AND ABS(ptlhrs) = abs(xx.ptlhrs)
      AND ptdate <> xx.ptdate)
  AND pttech IN ('502','506','511','519','522','526','528','532','536','537',
      '540','542','545','557','566','572','573','574','575','577','578','583',
      '585','595','596','598','608','610','611','623','624','625','631')   
  AND ptdate > '12/31/2009') x ON j.ptro# = x.ptro#
WHERE x.ptro# IS NOT NULL
  AND j.pttech IN ('502','506','511','519','522','526','528','532','536','537',
      '540','542','545','557','566','572','573','574','575','577','578','583',
      '585','595','596','598','608','610','611','623','624','625','631') 
ORDER BY x.pttech, x.ptro#, x.ptline, x.ptdate  
    
/**************  compare to GL ************************************************/
-- works for one ro
SELECT ptro#, SUM(hours*rate), SUM(g.gttamt)
FROM (
SELECT ptro#,pttech, sum(coalesce(ptlhrs, 0)) AS hours, MAX(t.stlrat) AS rate
FROM stgArkonaSDPRDET d
LEFT JOIN stgArkonaSDPTECH t ON d.ptco# = t.stco#
  AND d.pttech = t.sttech
WHERE ptcode = 'TT'
  AND ptlhrs <> 0
  AND ptro# = '16079757'
GROUP BY ptro#, pttech) x
LEFT JOIN stgArkonaGLPTRNS g ON x.ptro# = g.gtdoc#
  AND g.gtacct = '124700'
GROUP BY ptro#

SELECT ptro#, SUM(hours*rate), SUM(g.gttamt)
FROM (
  SELECT ptro#,pttech, sum(coalesce(ptlhrs, 0)) AS hours, MAX(t.stlrat) AS rate
  FROM stgArkonaSDPRDET d
  LEFT JOIN stgArkonaSDPTECH t ON d.ptco# = t.stco#
    AND d.pttech LIKE '5%'
  WHERE ptcode = 'TT'
    AND ptlhrs <> 0
    AND d.pttech = t.sttech  
  --  AND ptro# = '19095948'
    AND ptro# in (
      SELECT ptro#
      FROM stgArkonaSDPRDET
      WHERE ptdate = '03/11/2012')
  GROUP BY ptro#, pttech) x
LEFT JOIN stgArkonaGLPTRNS g ON x.ptro# = g.gtdoc#
  AND g.gtacct in ('124700', '124701')
GROUP BY ptro#


SELECT gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gttamt , g.gmdesc
--SELECT *
FROM stgArkonaGLPTRNS t
LEFT JOIN stgArkonaGLPMAST g ON t.gtacct = g.gmacct
  AND g.gmyear = 2012
WHERE gtdoc# = '19095948'
ORDER BY gtacct

SELECT *
FROM edwEmployeeDim
WHERE technumber = '566'
/**************  compare to GL ************************************************/

-- 4/10
-- 4/11 NOT so sure about this one yet
-- here we go again
-- IF there IS an offsetting record IN DET, the adj IS negative
-- IF NOT, THEN the adj IS positive
SELECT * FROM (
  SELECT ptco#, ptro#, ptline, ptdate, pttech, round(SUM(ptlhrs),2) AS ptlhrs
  FROM (
    SELECT DISTINCT ptco#, ptpkey, ptro#, ptline, ptdate, pttech, coalesce(ptlhrs, 0 ) AS ptlhrs
    FROM stgArkonaSDPXTIM) xx 
  WHERE xx.ptlhrs < 0
    AND xx.ptdate BETWEEN  '03/25/2012' AND '04/07/2012'
    AND 
      EXISTS (
        SELECT 1
        FROM stgArkonaSDPRDET
        WHERE ptco# = xx.ptco#
          AND ptro# = xx.ptro#
          AND ptline = xx.ptline
          AND pttech = xx.pttech
          AND abs(ptlhrs) = abs(xx.ptlhrs)) 
      OR EXISTS (  
      SELECT 1
        FROM stgArkonaPDPPDET d
        WHERE d.ptco# = xx.ptco#
          AND d.ptpkey = xx.ptpkey
          AND d.ptline = xx.ptline
          AND d.pttech = xx.pttech
          AND abs(d.ptlhrs) = abs(xx.ptlhrs))     
  GROUP BY ptco#, ptdate, ptro#, ptline, pttech
  UNION
    SELECT ptco#, ptro#, ptline, ptdate, pttech, round(SUM(ptlhrs),2) AS ptlhrs
  FROM (
    SELECT DISTINCT ptco#, ptpkey, ptro#, ptline, ptdate, pttech, coalesce(ptlhrs, 0 ) AS ptlhrs
    FROM stgArkonaSDPXTIM) xx 
  WHERE xx.ptlhrs > 0
    AND xx.ptdate BETWEEN  '03/25/2012' AND '04/07/2012'
    AND 
      NOT EXISTS (
        SELECT 1
        FROM stgArkonaSDPRDET
        WHERE ptco# = xx.ptco#
          AND ptro# = xx.ptro#
          AND ptline = xx.ptline
          AND pttech = xx.pttech
          AND abs(ptlhrs) = abs(xx.ptlhrs)) 
      OR NOT EXISTS (  
      SELECT 1
        FROM stgArkonaPDPPDET d
        WHERE d.ptco# = xx.ptco#
          AND d.ptpkey = xx.ptpkey
          AND d.ptline = xx.ptline
          AND d.pttech = xx.pttech
          AND abs(d.ptlhrs) = abs(xx.ptlhrs))     
  GROUP BY ptco#, ptdate, ptro#, ptline, pttech) y
WHERE ptro# = '16085210'  

SELECT * FROM stgArkonaSDPXTIM WHERE ptro# = '16085210'
SELECT * FROM stgArkonaSDPRDET WHERE ptro# = '16085210' AND ptcode = 'tt'


/*************** 4/16 **********************/
-- DROP TABLE #jon   
--  going back to thinking, UNION xtim
-- 4/8, so what does that look LIKE
-- #jon
-- remove ptseq# FROM sdprdet & pdppdet
-- 4/9
-- 16084086/506: tech has hours ON 2 seq for same line
-- so the union with subselect ON sdprdet discards one of the lines
-- so, GROUP the sdprdet subselect AND SUM the hours
-- verify existence of an offseting base record to include sdpxtim
-- have to get rid of the big anomaly 536: 536 hours
-- 4/13/12: at the point WHERE i say fuck xtim
-- oh shit, just got jeremies email
-- BUT, it think what he IS saying IS that sdpxtim IS only for payroll
-- so i don't need it
-- 4/16 back to scratch, 
-- no xtim, DO the whole fucking deal (no date constraint)
-- 52 secs
SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
INTO #FlagHours
FROM (
    SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptdate, d.pttech, 
      coalesce(d.ptlhrs, 0) AS ptlhrs
    FROM stgArkonaPDPPDET d
    INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
--      AND d.ptdate BETWEEN '03/11/2012' AND '03/24/2012'
  UNION 
    SELECT s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM stgArkonaSDPRDET s
    WHERE s.ptdate IS NOT NULL 
      AND s.ptcode = 'TT'
--      AND s.ptdate BETWEEN  '03/11/2012' AND '03/24/2012'
      AND s.ptlhrs < 300
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) y  
WHERE ptlhrs <> 0    
GROUP BY ptco#, ptdate, pttech, ptro#, ptline;

SELECT * FROM #FlagHours 

SELECT COUNT(*) -- 446133
FROM #FlagHours

-- unique
SELECT ptco#, ptdate, pttech, ptro#, ptline
FROM #FlagHours
group by ptco#, ptdate, pttech, ptro#, ptline
HAVING COUNT(*) > 1