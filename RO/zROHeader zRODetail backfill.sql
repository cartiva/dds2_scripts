-- backfill zROHeader
/*
-- initial backfill was based ON existence IN zRODetail
-- every row IN factRO should have a row IN zROHeader
SELECT a.*
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
WHERE a.void = false
  AND b.thedate > '01/01/2013'
  AND NOT EXISTS (
    SELECT 1
    FROM zroheader
    WHERE ro = a.ro)
*/  
  
INSERT INTO zROHeader (storecode,ro,writernumber,rostatus,opendate, opendatedaynamemmdd,
  closedate, closedatedaynamemmdd, finalclosedate, finalclosedatedaynamemmdd,
  customer,homephone,workphone,cellphone,customeremail,vehicle)
SELECT a.storecode, a.ro, writerid, 
  CASE 
    WHEN b.ptrsts IS NOT NULL THEN 
      CASE b.ptrsts
        WHEN '1' THEN 'Open'
        WHEN '2' THEN 'In-Process'
        WHEN '3' THEN 'Appr-PD'
        WHEN '4' THEN 'Cashier'
        WHEN '5' THEN 'Cashier/D'
        WHEN '6' THEN 'Pre-Inv'
        WHEN '7' THEN 'Odom Rq'
        WHEN '9' THEN 'Parts-A'
        WHEN 'L' THEN 'G/L Error'
        WHEN 'P' THEN 'PI Rq'
        ELSE 'WTF'
      END
    ELSE 'Closed'
  END AS ROStatus,
  c.thedate, left(c.DayName,3) + ' ' + c.mmdd AS OpenDate,
  d.thedate, left(d.DayName,3) + ' ' + d.mmdd AS CloseDate,
  h.thedate, left(h.DayName,3) + ' ' + h.mmdd AS FinalCloseDate,
  a.customername,
  CASE 
    WHEN e.bnphon  = '0' THEN 'No Phone'
    WHEN e.bnphon  = '' THEN 'No Phone'
    WHEN e.bnphon IS NULL THEN 'No Phone'
    ELSE left(e.bnphon, 12)
  END AS HomePhone, 
  CASE 
    WHEN e.bnbphn  = '0' THEN 'No Phone'
    WHEN e.bnbphn  = '' THEN 'No Phone'
    WHEN e.bnbphn IS NULL THEN 'No Phone'
    ELSE left(e.bnbphn, 12)
  END  AS WorkPhone, 
  CASE 
    WHEN e.bncphon  = '0' THEN 'No Phone'
    WHEN e.bncphon  = '' THEN 'No Phone'
    WHEN e.bncphon IS NULL THEN 'No Phone'
    ELSE left(e.bncphon, 12)
  END  AS CellPhone,
  coalesce(case when e.bnemail = '' THEN 'None' ELSE e.bnemail END, 'None') as CustomerEMail,
  trim(TRIM(f.immake) + ' ' + TRIM(f.immodl)) AS vehicle   
FROM factro a
LEFT JOIN stgArkonaPDPPHDR b ON a.ro = b.ptdoc# -- OPEN ro status
LEFT JOIN day c ON a.opendatekey = c.datekey
LEFT JOIN day d ON a.closedatekey = d.datekey
LEFT JOIN day h ON a.finalclosedatekey = h.datekey
LEFT JOIN (
    select bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail
    from stgArkonaBOPNAME
    group by bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail) e ON a.customername = e.bnsnam
  AND a.customerkey = e.bnkey    
LEFT JOIN stgArkonaINPMAST f ON a.vin = f.imvin 
WHERE a.void = false
  AND c.thedate > '01/01/2013'
  AND NOT EXISTS (
    SELECT 1
    FROM zroheader
    WHERE ro = a.ro);
    
-- zRODetail backfill
INSERT INTO zRODetail (storecode,ro,line,linestatus,paytype,opcode,opcodedescription,
  corcode,corcodedescription)
SELECT b.storecode, b.ro, b.line, 
  CASE 
    WHEN h.ptlsts IS NULL THEN 'Closed'
    WHEN h.ptlsts = 'I' THEN 'Open'
    WHEN h.ptlsts = 'C' THEN 'Closed'
  END AS LineStatus, b.paytype, 
  b.opcode, trim(trim(d.sodes1) + ' ' + TRIM(d.sodes2)) AS OpCodeDesc,
  b.corcode, trim(trim(e.sodes1) + ' ' + TRIM(e.sodes2)) AS CorCodeDesc
FROM factro a
INNER JOIN factroline b ON a.storecode = b.storecode AND a.ro = b.ro  
INNER JOIN day c ON a.opendatekey = c.datekey
LEFT JOIN stgArkonaSDPLOPC d ON b.storecode = d.soco# AND b.opcode = d.solopc
LEFT JOIN stgArkonaSDPLOPC e ON b.storecode = e.soco# AND b.corcode = e.solopc
LEFT JOIN ( -- OPEN ro line status
  SELECT f.ptco#, f.ptdoc#, g.ptline, g.ptlsts
  FROM stgArkonaPDPPHDR f
  INNER JOIN stgArkonaPDPPDET g ON f.ptpkey = g.ptpkey
    AND g.ptltyp = 'A'
    AND g.ptseq# = 0) h ON b.storecode = h.ptco# AND b.ro = h.ptdoc# AND b.line = h.ptline
WHERE a.void = false
  AND c.thedate > '01/01/2013'   
  AND NOT EXISTS (
    SELECT 1
    FROM zRODetail
    WHERE storecode = b.storecode
      AND ro = b.ro
      AND line = b.line)
  
 
-- fucked up ON a bunch of corcode descriptions, the link was to opcode instead of corcode
-- fix them 
SELECT a.storecode, a.ro, a.line, a.corcode, a.corcodedescription, trim(trim(b.sodes1) + ' ' + TRIM(b.sodes2))
FROM zrodetail a
LEFT JOIN stgArkonaSDPLOPC b ON a.storecode = b.soco# AND a.corcode = b.solopc
WHERE a.corcodedescription <> trim(trim(b.sodes1) + ' ' + TRIM(b.sodes2))

UPDATE zrodetail
SET corcodedescription = newCor
FROM (
  SELECT a.storecode, a.ro, a.line, a.corcode, a.corcodedescription, trim(trim(b.sodes1) + ' ' + TRIM(b.sodes2)) AS newCor
  FROM zrodetail a
  LEFT JOIN stgArkonaSDPLOPC b ON a.storecode = b.soco# AND a.corcode = b.solopc
  WHERE a.corcodedescription <> trim(trim(b.sodes1) + ' ' + TRIM(b.sodes2))) x
WHERE zrodetail.storecode = x.storecode
  AND zrodetail.ro = x.ro
  AND zrodetail.line = x.line
  AND zrodetail.corcodedescription <> x.newcor  
  
  
SELECT * FROM factro WHERE ro = '16122291'  
SELECT * FROM stgArkonaSDPRHDR WHERE pttag# = '4800'<> '' ORDER BY ptdate DESC

SELECT * FROM stgArkonaSDPRHDR WHERE ptfcdt = '12/31/9999' AND pttag# <> '' ORDER BY ptdate desc

