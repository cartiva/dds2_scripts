/*
X     Please Note
A     Complaint
A.0   Line Status
Z     Correction
L     Cause
*/
/*
SELECT * FROM factrolinetoday
SELECT * FROM stgArkonaSDPLOPC
SELECT a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt, b.ptlsts
FROM tmpPDPPHDR a
LEFT JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
WHERE a.ptdtyp = 'RO'
  AND b.ptltyp IN ('A','Z','X','L')
*/
--DROP TABLE zROLineCCC;
DROP TABLE zRODetail;  
CREATE TABLE zRODetail (
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  LineStatus cichar(9),
  PayType cichar(1),
  OpCode cichar(10),
  OpCodeDescription cichar(101), -- sodes1 + sodes2
  Complaint memo,
  CorCode cichar(10),
  CorCodeDescription cichar(101),
  Correction memo,
  Cause memo) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
  'zRODetail','zRODetail.adi','PK','StoreCode;RO;Line','',2051,512,'' );   
  
-- 1. first the initial scrape OPEN ro stuff  
INSERT INTO zRODetail
SELECT a.storecode, a.ro, a.line, g.status AS LineStatus, a.paytype, a.opcode,
  trim(trim(b.sodes1) + ' ' + TRIM(b.sodes2)) AS OpCodeDesc,
  d.complaint, a.corcode,
  trim(trim(c.sodes1) + ' ' + TRIM(c.sodes2)) AS CorCodeDesc,
  e.correction, f.cause
FROM factroline a
LEFT JOIN stgArkonaSDPLOPC b ON a.storecode = b.soco# AND a.opcode = b.solopc
LEFT JOIN stgArkonaSDPLOPC c ON a.storecode = c.soco# AND a.corcode = c.solopc
LEFT JOIN zOpenROComplaint d ON a.storecode = d.storecode AND a.ro = d.ro AND a.line = d.line
LEFT JOIN zOpenROCorrection e ON a.storecode = e.storecode AND a.ro = e.ro AND a.line = e.line
LEFT JOIN zOpenROCause f ON a.storecode = f.storecode AND a.ro = f.ro AND a.line = f.line
LEFT JOIN zOpenROLineStatus g ON a.storecode = g.storecode AND a.ro = g.ro AND a.line = g.line
WHERE a.ro IN (
  SELECT ro FROM zOpenrocomplaint
  UNION SELECT ro FROM zOpenrocause
  UNION SELECT ro FROM zOpenrocorrection);
 
-- 2. THEN the initial scrape closed ro stuff
INSERT INTO zRODetail
SELECT a.storecode, a.ro, a.line, 'Closed' AS LineStatus, a.paytype, a.opcode,
  trim(trim(b.sodes1) + ' ' + TRIM(b.sodes2)) AS OpCodeDesc,
  d.complaint, a.corcode,
  trim(trim(c.sodes1) + ' ' + TRIM(c.sodes2)) AS CorCodeDesc,
  e.correction, f.cause 
FROM factroline a
LEFT JOIN stgArkonaSDPLOPC b ON a.storecode = b.soco# AND a.opcode = b.solopc
LEFT JOIN stgArkonaSDPLOPC c ON a.storecode = c.soco# AND a.corcode = c.solopc
LEFT JOIN zROComplaint d ON a.storecode = d.storecode AND a.ro = d.ro AND a.line = d.line
LEFT JOIN zROCorrection e ON a.storecode = e.storecode AND a.ro = e.ro AND a.line = e.line
LEFT JOIN zROCause f ON a.storecode = f.storecode AND a.ro = f.ro AND a.line = f.line
WHERE a.ro IN (
  SELECT ro FROM zrocomplaint
  UNION SELECT ro FROM zrocause
  UNION SELECT ro FROM zrocorrection)
AND NOT EXISTS (
  SELECT 1
  FROM zRODetail
  WHERE storecode = a.storecode 
    AND ro = a.ro
    AND line = a.line);

/*
-- pdp data to be added
SELECT a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt, b.ptlsts, c.*
--SELECT COUNT(*) --2274
FROM stgArkonaPDPPHDR a
LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
INNER JOIN factroline c ON a.ptco# = c.storecode AND a.ptdoc# = c.ro AND b.ptline = c.line
WHERE a.ptdtyp = 'RO' -- don't need this because of INNER JOIN ON factroline
  AND b.ptltyp IN ('A','Z','L')
  AND b.ptpkey IS NOT NULL 
  AND ptcmnt <> ''
  AND NOT EXISTS (
    SELECT 1
    FROM zRODetail
    WHERE ro = a.ptdoc#
    AND line = b.ptline)
*/    
-- 3. current (dds) pdp
-- 3a. ADD the factro/sdplopc data
INSERT INTO zRODetail (storecode,ro,line,paytype,opcode,opcodedescription,corcode,corcodedescription)
SELECT distinct c.storecode, c.ro, c.line, c.paytype, c.opcode,
  trim(trim(d.sodes1) + ' ' + TRIM(d.sodes2)) AS OpCodeDesc,
  c.corcode,
  trim(trim(e.sodes1) + ' ' + TRIM(e.sodes2)) AS CorCodeDesc
FROM stgArkonaPDPPHDR a
LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
INNER JOIN factroline c ON a.ptco# = c.storecode AND a.ptdoc# = c.ro AND b.ptline = c.line
LEFT JOIN stgArkonaSDPLOPC d ON c.storecode = d.soco# AND c.opcode = d.solopc
LEFT JOIN stgArkonaSDPLOPC e ON c.storecode = e.soco# AND c.corcode = e.solopc
WHERE b.ptltyp IN ('A','Z','L')
  AND b.ptpkey IS NOT NULL 
  AND ptcmnt <> ''
  AND NOT EXISTS (
    SELECT 1
    FROM zRODetail
    WHERE ro = a.ptdoc#
    AND line = b.ptline);
-- 3b. UPDATE with linestatus
UPDATE zRODetail
SET LineStatus = 
  CASE f.ptlsts
    WHEN 'C' THEN 'Closed'
    WHEN 'I' THEN 'Open'
  END 
FROM (
  SELECT c.storecode, c.ro, c.line, b.ptlsts
  FROM stgArkonaPDPPHDR a
  LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
  INNER JOIN factroline c ON a.ptco# = c.storecode AND a.ptdoc# = c.ro AND b.ptline = c.line
  LEFT JOIN stgArkonaSDPLOPC d ON c.storecode = d.soco# AND c.opcode = d.solopc
  LEFT JOIN stgArkonaSDPLOPC e ON c.storecode = e.soco# AND c.corcode = e.solopc
  WHERE b.ptltyp = 'A'
    AND b.ptpkey IS NOT NULL 
    AND b.ptseq# = 0) f
WHERE zRODetail.storecode = f.storecode
  AND zRODetail.ro = f.ro
  AND zRODetail.line = f.line; 
    
/* hmmm, NOT sure what to make of these
SELECT COUNT(*) FROM zRODetail WHERE linestatus IS NULL
*/
-- 3c. parse AND UPDATE complaint
/*
SELECT a.ptco#, a.ptdoc#, a.ptpkey, b.ptline
FROM stgArkonaPDPPHDR a
LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
INNER JOIN factroline c ON a.ptco# = c.storecode AND a.ptdoc# = c.ro AND b.ptline = c.line
LEFT JOIN stgArkonaSDPLOPC d ON c.storecode = d.soco# AND c.opcode = d.solopc
LEFT JOIN stgArkonaSDPLOPC e ON c.storecode = e.soco# AND c.opcode = e.solopc
WHERE  b.ptltyp = 'A'
  AND b.ptpkey IS NOT NULL 
  AND ptcmnt <> ''
GROUP BY a.ptco#, a.ptdoc#, a.ptpkey, b.ptline  
*/  
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ptpkey integer;
DECLARE @line integer;
DECLARE @ro string;
DECLARE @curAll cursor AS 
  SELECT a.ptco#, a.ptdoc#, a.ptpkey, b.ptline
  FROM stgArkonaPDPPHDR a
  LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
  INNER JOIN factroline c ON a.ptco# = c.storecode AND a.ptdoc# = c.ro AND b.ptline = c.line
  LEFT JOIN stgArkonaSDPLOPC d ON c.storecode = d.soco# AND c.opcode = d.solopc
  LEFT JOIN stgArkonaSDPLOPC e ON c.storecode = e.soco# AND c.corcode = e.solopc
  WHERE  b.ptltyp = 'A'
    AND b.ptpkey IS NOT NULL 
    AND ptcmnt <> ''
  GROUP BY a.ptco#, a.ptdoc#, a.ptpkey, b.ptline;   
DECLARE @curRO CURSOR AS
  SELECT ptpkey, ptline, ptltyp, ptseq#, ptcmnt
  FROM stgArkonaPDPPDET
  WHERE ptpkey = @curAll.ptpkey
    AND ptline = @curAll.ptline
    AND ptltyp = 'A'
  ORDER BY ptseq#;           
@string = '';
OPEN @curAll;
TRY 
  WHILE FETCH @curAll DO
    @storecode = @curAll.ptco#;
    @ro = @curAll.ptdoc#;
    @ptpkey = @curAll.ptpkey;
    @line = @curAll.ptline;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.ptcmnt);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY; 
    UPDATE zRODetail
    SET complaint = @string
    WHERE storecode = @storecode
      AND ro = @ro
      AND line = @line;
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY;  

-- 3d. parse AND UPDATE correction
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ptpkey integer;
DECLARE @line integer;
DECLARE @ro string;
DECLARE @curAll cursor AS 
  SELECT a.ptco#, a.ptdoc#, a.ptpkey, b.ptline
  FROM stgArkonaPDPPHDR a
  LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
  INNER JOIN factroline c ON a.ptco# = c.storecode AND a.ptdoc# = c.ro AND b.ptline = c.line
  LEFT JOIN stgArkonaSDPLOPC d ON c.storecode = d.soco# AND c.opcode = d.solopc
  LEFT JOIN stgArkonaSDPLOPC e ON c.storecode = e.soco# AND c.corcode = e.solopc
  WHERE  b.ptltyp = 'Z'
    AND b.ptpkey IS NOT NULL 
    AND ptcmnt <> ''
  GROUP BY a.ptco#, a.ptdoc#, a.ptpkey, b.ptline;   
DECLARE @curRO CURSOR AS
  SELECT ptpkey, ptline, ptltyp, ptseq#, ptcmnt
  FROM stgArkonaPDPPDET
  WHERE ptpkey = @curAll.ptpkey
    AND ptline = @curAll.ptline
    AND ptltyp = 'z'
  ORDER BY ptseq#;           
@string = '';
OPEN @curAll;
TRY 
  WHILE FETCH @curAll DO
    @storecode = @curAll.ptco#;
    @ro = @curAll.ptdoc#;
    @ptpkey = @curAll.ptpkey;
    @line = @curAll.ptline;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.ptcmnt);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY; 
    UPDATE zRODetail
    SET correction = @string
    WHERE storecode = @storecode
      AND ro = @ro
      AND line = @line;
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY;  
-- 3e. parse AND UPDATE cause
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ptpkey integer;
DECLARE @line integer;
DECLARE @ro string;
DECLARE @curAll cursor AS 
  SELECT a.ptco#, a.ptdoc#, a.ptpkey, b.ptline
  FROM stgArkonaPDPPHDR a
  LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
  INNER JOIN factroline c ON a.ptco# = c.storecode AND a.ptdoc# = c.ro AND b.ptline = c.line
  LEFT JOIN stgArkonaSDPLOPC d ON c.storecode = d.soco# AND c.opcode = d.solopc
  LEFT JOIN stgArkonaSDPLOPC e ON c.storecode = e.soco# AND c.corcode = e.solopc
  WHERE  b.ptltyp = 'L'
    AND b.ptpkey IS NOT NULL 
    AND ptcmnt <> ''
  GROUP BY a.ptco#, a.ptdoc#, a.ptpkey, b.ptline;   
DECLARE @curRO CURSOR AS
  SELECT ptpkey, ptline, ptltyp, ptseq#, ptcmnt
  FROM stgArkonaPDPPDET
  WHERE ptpkey = @curAll.ptpkey
    AND ptline = @curAll.ptline
    AND ptltyp = 'L'
  ORDER BY ptseq#;           
@string = '';
OPEN @curAll;
TRY 
  WHILE FETCH @curAll DO
    @storecode = @curAll.ptco#;
    @ro = @curAll.ptdoc#;
    @ptpkey = @curAll.ptpkey;
    @line = @curAll.ptline;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.ptcmnt);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY; 
    UPDATE zRODetail
    SET cause = @string
    WHERE storecode = @storecode
      AND ro = @ro
      AND line = @line;
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY;  

/* maybe a worthwhile refactoring
instead of running this fucking query every time, DO a tmp TABLE
SELECT distinct c.storecode, c.ro, c.line, c.paytype, c.opcode,
  trim(trim(d.sodes1) + ' ' + TRIM(d.sodes2)) AS OpCodeDesc,
  c.corcode,
  trim(trim(e.sodes1) + ' ' + TRIM(e.sodes2)) AS CorCodeDesc
FROM stgArkonaPDPPHDR a
LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
INNER JOIN factroline c ON a.ptco# = c.storecode AND a.ptdoc# = c.ro AND b.ptline = c.line
LEFT JOIN stgArkonaSDPLOPC d ON c.storecode = d.soco# AND c.opcode = d.solopc
LEFT JOIN stgArkonaSDPLOPC e ON c.storecode = e.soco# AND c.opcode = e.solopc
WHERE b.ptltyp IN ('A','Z','L')
  AND b.ptpkey IS NOT NULL 
  AND ptcmnt <> ''
  AND NOT EXISTS (
    SELECT 1
    FROM zRODetail
    WHERE ro = a.ptdoc#
    AND line = b.ptline);
*/  
-- 4. SDP Data
start with a 2 week scrape INTO tmpSDPRTXT
CREATE TABLE tmpSDPRTXT ( 
      [sxco#] CIChar( 3 ),
      [sxro#] CIChar( 9 ),
      sxline Integer,
      sxltyp CIChar( 1 ),
      [sxseq#] Integer,
      sxcode CIChar( 2 ),
      sxtext CIChar( 50 )) IN DATABASE;  
      
CREATE TABLE tmpSDPRTXTa ( 
      [sxco#] CIChar( 3 ),
      [sxro#] CIChar( 9 ),
      sxline Integer,
      sxltyp CIChar( 1 ),
      [sxseq#] Integer,
      sxcode CIChar( 2 ),
      sxtext CIChar( 50 )) IN DATABASE;       
      
SELECT * 
-- SELECT COUNT(*) -- cool, totally manageables
FROM tmpSDPRTXT a
WHERE NOT EXISTS (
  SELECT 1
  FROM zRODetail
  WHERE storecode = a.sxco#
    AND ro = a.sxro#
    AND line = a.sxline)
this IS the raw data to be used for ALL inserts AND updates   
DO NOT want to keep reusing this for ALL the updates, because, once the
factro/sdplopc data IS inserted, it no longer gives me the shit i need for updates
so, need a discreet TABLE that has the data
TRY to DELETE FROM tmpSDPRTXT - can NOT figure that shit out

DELETE FROM tmpSDPRTXTa;
INSERT INTO tmpSDPRTXTa
SELECT * 
FROM tmpSDPRTXT a
WHERE NOT EXISTS (
  SELECT 1
  FROM zRODetail
  WHERE storecode = a.sxco#
    AND ro = a.sxro#
    AND line = a.sxline);
    
SELECT * FROM tmpSDPRTXTa  WHERE sxtext = ''   
    
-- 4a. ADD the factro/sdplopc data, shit DO line status here, they are ALL just fucking Closed anyway
INSERT INTO zRODetail (storecode,ro,line,linestatus,paytype,opcode,opcodedescription,corcode,corcodedescription)
SELECT distinct c.storecode, c.ro, c.line, 'Closed', c.paytype, c.opcode,
  trim(trim(d.sodes1) + ' ' + TRIM(d.sodes2)) AS OpCodeDesc,
  c.corcode,
  trim(trim(e.sodes1) + ' ' + TRIM(e.sodes2)) AS CorCodeDesc
-- SELECT COUNT(*) 
FROM tmpSDPRTXTa a  
INNER JOIN factroline c ON a.sxco# = c.storecode AND a.sxro# = c.ro AND a.sxline = c.line
LEFT JOIN stgArkonaSDPLOPC d ON c.storecode = d.soco# AND c.opcode = d.solopc
LEFT JOIN stgArkonaSDPLOPC e ON c.storecode = e.soco# AND c.corcode = e.solopc
WHERE a.sxltyp IN ('A','Z','L')
  AND a.sxtext <> ''
-- 4b. linestatus , duh, this IS spd, ALL line statuses = 'Closed' - done IN 4a
-- 4c. complaint 
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ptpkey integer;
DECLARE @line integer;
DECLARE @ro string;
DECLARE @curAll cursor AS 
  SELECT a.sxco#, a.sxro#, a.sxline
  FROM tmpSDPRTXTa a
  WHERE  a.sxltyp = 'A'
    AND a.sxtext <> ''
  GROUP BY a.sxco#, a.sxro#, a.sxline;
DECLARE @curRO CURSOR AS
  SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
  FROM tmpSDPRTXTa a
  WHERE sxco# = @curAll.sxco#
    AND sxro# = @curAll.sxro#
    AND sxline = @curAll.sxline
    AND sxltyp = 'A'
  ORDER BY sxseq#;           
@string = '';
OPEN @curAll;
TRY 
  WHILE FETCH @curAll DO
    @storecode = @curAll.sxco#;
    @ro = @curAll.sxro#;
    @line = @curAll.sxline;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.sxtext);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY; 
    UPDATE zRODetail
    SET complaint = @string
    WHERE storecode = @storecode
      AND ro = @ro
      AND line = @line;
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY;  
-- 4d. correction
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ptpkey integer;
DECLARE @line integer;
DECLARE @ro string;
DECLARE @curAll cursor AS 
  SELECT a.sxco#, a.sxro#, a.sxline
  FROM tmpSDPRTXTa a
  WHERE  a.sxltyp = 'Z'
    AND a.sxtext <> ''
  GROUP BY a.sxco#, a.sxro#, a.sxline;
DECLARE @curRO CURSOR AS
  SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
  FROM tmpSDPRTXTa a
  WHERE sxco# = @curAll.sxco#
    AND sxro# = @curAll.sxro#
    AND sxline = @curAll.sxline
    AND sxltyp = 'Z'
  ORDER BY sxseq#;          
@string = '';
OPEN @curAll;
TRY 
  WHILE FETCH @curAll DO
    @storecode = @curAll.sxco#;
    @ro = @curAll.sxro#;
    @line = @curAll.sxline;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.sxtext);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY; 
    UPDATE zRODetail
    SET correction = @string
    WHERE storecode = @storecode
      AND ro = @ro
      AND line = @line;
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY;  
-- 4e. cause     
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ptpkey integer;
DECLARE @line integer;
DECLARE @ro string;
DECLARE @curAll cursor AS 
  SELECT a.sxco#, a.sxro#, a.sxline
  FROM tmpSDPRTXTa a
  WHERE  a.sxltyp = 'L'
    AND a.sxtext <> ''
  GROUP BY a.sxco#, a.sxro#, a.sxline;
DECLARE @curRO CURSOR AS
  SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
  FROM tmpSDPRTXTa a
  WHERE sxco# = @curAll.sxco#
    AND sxro# = @curAll.sxro#
    AND sxline = @curAll.sxline
    AND sxltyp = 'L'
  ORDER BY sxseq#;          
@string = '';
OPEN @curAll;
TRY 
  WHILE FETCH @curAll DO
    @storecode = @curAll.sxco#;
    @ro = @curAll.sxro#;
    @line = @curAll.sxline;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.sxtext);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY; 
    UPDATE zRODetail
    SET cause = @string
    WHERE storecode = @storecode
      AND ro = @ro
      AND line = @line;
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY;     
    
-- 10.  zROHEader need please note :: will be part of zROHeader

