/*
zBTG1 FROM tmpRHDR/RDET/PHDR/PDET :: 1 row per line/tech
zBTGTotal :: 1 row per line
zBTG :: 1 row per tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4
Constraints:
  zBTG1: unique index: storecode;ro;line;tech
  zBTGTotal: unique index: storecode;ro;line
Tests:
  1st Test: no more than 4 techs per line
  2nd Test: ZBTG one row per tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4
*/
DECLARE @i integer;
DECLARE @techkey integer;
DECLARE @weightfactor double(4);
DECLARE @TechGroupKey integer;       
DECLARE @ro string;
DECLARE @line integer;
DECLARE @seq integer; 
DECLARE @section string;

-- test1
DECLARE @curTechcount CURSOR AS 
  SELECT a.ro, a.line, COUNT(*) 
  FROM zbtg1 a 
  GROUP BY a.ro, a.line HAVING COUNT(*) > 4;

DECLARE @cur4Tech CURSOR as      
  SELECT * from zbtg4techs WHERE ro = @ro AND line = @line;
  
DECLARE @rolinecur CURSOR AS
  SELECT DISTINCT ro,line
  FROM zbtg4techs;

DECLARE @curZbtgTest CURSOR AS 
  SELECT tech1,coalesce(tech2,0),coalesce(tech3,0),coalesce(tech4,0),
    coalesce(wf1,0),coalesce(wf2,0),coalesce(wf3,0),coalesce(wf4,0) 
  FROM zbtg
  GROUP BY tech1,coalesce(tech2,0),coalesce(tech3,0),coalesce(tech4,0),
    coalesce(wf1,0),coalesce(wf2,0),coalesce(wf3,0),coalesce(wf4,0) 
  HAVING COUNT(*) > 1;  
  
DECLARE @curTechGroup CURSOR AS
  SELECT DISTINCT a.techGroupKey
  FROM tmpBrTechGroup a
  WHERE NOT EXISTS (
    SELECT 1
    FROM brTechGroup
    WHERE techkey = a.techkey
      AND weightfactor = a.weightfactor)
  ORDER BY TechGroupKey, TechKey;
  
DECLARE @curNew CURSOR AS 
  SELECT techkey, weightfactor
  FROM tmpBrTechGroup
  WHERE TechGroupKey = @TechGroupKey;
  
DECLARE @curZBTG CURSOR AS 
  SELECT * FROM (
    SELECT tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4
    FROM zbtgtotal 
    WHERE tech4 IS NOT NULL
    UNION
    -- 1 tech
    SELECT tech1, cast(NULL as sql_integer) AS tech2, cast(NULL as sql_integer) AS tech3, 
      cast(NULL as sql_integer) AS tech4, 
      cast(1 as sql_double) AS wf1, cast(NULL as sql_double) AS wf2, 
      cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
    FROM (     
      SELECT tech1, 1
      FROM zbtgtotal
      WHERE tech2 IS NULL 
      GROUP BY tech1) x
    UNION
    -- 2 techs
    SELECT DISTINCT tech1,tech2,
      cast(NULL as sql_integer) AS tech3, 
      cast(NULL as sql_integer) AS tech4,
      round(wf1,4) AS wf1, round(wf2,4) AS wf2, 
      cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
    FROM (
      SELECT 
        CASE WHEN tech1 <= tech2 THEN tech1 ELSE tech2 END AS tech1,
        CASE WHEN tech1 <= tech2 THEN tech2 ELSE tech1 END AS tech2,
        CASE WHEN tech1 <= tech2 THEN wf1 ELSE wf2 END AS wf1,
        CASE WHEN tech1 <= tech2 THEN wf2 ELSE wf1 END AS wf2
      FROM zbtgtotal 
      WHERE tech2 IS NOT NULL 
        AND tech3 IS NULL  
      GROUP BY tech1,tech2,wf1,wf2) x
    UNION
    -- 3 techs
    SELECT DISTINCT tech1,tech2,tech3,cast(NULL as sql_integer) AS tech4,
    round(wf1,4),round(wf2,4),round(wf3,4),cast(NULL as sql_double) AS wf4
    FROM (  
      SELECT
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech1, iif(tech1 <= tech3, tech1, tech3)) 
          ELSE iif(tech1 <= tech3, tech2, iif(tech2 <= tech3, tech2, tech3)) 
        END AS tech1,
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech2, iif(tech1 <= tech3, tech3 ,tech1)) 
          ELSE iif(tech1 <= tech3, tech1, iif(tech2 <= tech3, tech3, tech2)) 
        END AS tech2,
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech3, iif(tech1 <= tech3, tech2 ,tech2)) 
          ELSE iif(tech1 <= tech3, tech3, iif(tech2 <= tech3, tech1, tech1)) 
        END AS tech3,   
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf1, iif(tech1 <= tech3, wf1, wf3)) 
          ELSE iif(tech1 <= tech3, wf2, iif(tech2 <= tech3, wf2, wf3)) 
        END AS wf1,
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf2, iif(tech1 <= tech3, wf3 ,wf1)) 
          ELSE iif(tech1 <= tech3, wf1, iif(tech2 <= tech3, wf3, wf2)) 
        END AS wf2,
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf3, iif(tech1 <= tech3, wf2, wf2)) 
          ELSE iif(tech1 <= tech3, wf3, iif(tech2 <= tech3, wf1, wf1)) 
        END AS wf3    
      FROM zbtgtotal
      WHERE tech3 IS NOT NULL
        AND tech4 IS NULL 
      GROUP BY tech1,tech2,tech3,wf1,wf2,wf3) x) z;
 
BEGIN TRANSACTION; // zBTG1
TRY 
  DELETE FROM zbtg1;  
  INSERT INTO zBTG1 
  -- thinking, DO pdet, the sdet WHERE NOT EXISTS
  SELECT a.ptco#, a.ptdoc#, a.ptline, e.totalhours, a.seq, a.hrs, 
    CASE 
      WHEN a.ptco# = 'RY1' THEN
        CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
      WHEN a.ptco# = 'RY2' THEN 
        CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
    END AS TechKey 
  FROM (
    SELECT b.ptco#, a.ptdoc#, b.ptline, b.pttech, MAX(b.ptseq#) AS seq, SUM(b.ptlhrs) AS hrs, MIN(b.ptdate) AS mindate
    FROM tmpPDPPHDR a
    INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
    WHERE a.ptdtyp = 'RO'
      AND a.ptdoc# <> ''
      AND a.ptchk# NOT LIKE 'v%'
      AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
      AND b.ptline < 900
      AND b.pttech <> ''
      AND b.ptlhrs > 0
    GROUP BY b.ptco#, a.ptdoc#, b.ptline, b.pttech) a
  LEFT JOIN dimtech b ON a.pttech = b.technumber
    AND a.ptco# = b.storecode
    AND a.mindate BETWEEN b.techkeyfromdate AND b.techkeythrudate  
  LEFT JOIN (
    SELECT storecode, TechKey 
    FROM dimTech
    WHERE technumber = 'UNK') c ON a.ptco# = c.storecode  
  LEFT JOIN (    
    SELECT b.ptco#, a.ptdoc#, b.ptline, SUM(b.ptlhrs) AS totalhours
    FROM tmpPDPPHDR a
    INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
    WHERE a.ptdtyp = 'RO'
      AND a.ptdoc# <> ''
      AND a.ptchk# NOT LIKE 'v%'
      AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
      AND b.ptline < 900
      AND b.pttech <> ''
      AND b.ptlhrs > 0
    GROUP BY b.ptco#, a.ptdoc#, b.ptline) e ON a.ptco# = e.ptco# AND a.ptdoc# = e.ptdoc# AND a.ptline = e.ptline;
  INSERT INTO zBTG1   
  SELECT a.ptco#, a.ptro#, a.ptline, e.totalhours, a.seq, a.hrs, 
    CASE 
      WHEN a.ptco# = 'RY1' THEN
        CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
      WHEN a.ptco# = 'RY2' THEN 
        CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
    END AS TechKey 
  FROM (
    SELECT ptco#, ptro#, ptline, pttech, max(ptseq#) AS seq, SUM(ptlhrs) AS hrs, MIN(ptdate) AS mindate
    FROM tmpSDPRDET a
    WHERE a.pttech <> ''
      AND a.ptlhrs > 0
      AND a.ptco# IN ('RY1','RY2')
    GROUP BY ptco#, ptro#, ptline, pttech) a
  LEFT JOIN dimtech b ON a.pttech = b.technumber
    AND a.ptco# = b.storecode
    AND a.mindate BETWEEN b.techkeyfromdate AND b.techkeythrudate  
  LEFT JOIN (
    SELECT storecode, TechKey 
    FROM dimTech
    WHERE technumber = 'UNK') c ON a.ptco# = c.storecode    
  LEFT JOIN (  
    SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS totalhours
    FROM tmpSDPRDET a
    WHERE a.pttech <> ''
      AND a.ptlhrs > 0
      AND a.ptco# IN ('RY1','RY2')
    GROUP BY ptco#, ptro#, ptline) e ON a.ptco# = e.ptco# AND a.ptro# = e.ptro# AND a.ptline = e.ptline
  WHERE NOT EXISTS (
    SELECT 1
    FROM zbtg1 
    WHERE ro = a.ptro#
      AND line = a.ptline);
COMMIT WORK;  	  
CATCH ALL
  ROLLBACK;
  RAISE dimTechGroup(1, 'zBTG1 ' + __errtext);   
END TRY; // transaction      
-- 1st Test
OPEN @curTechcount;
TRY
  IF FETCH @curTechcount THEN 
    RAISE dimTechGroup(2, 'More than 4 techs');
  END IF;
FINALLY
  CLOSE @curTechCount;
END TRY;  

BEGIN TRANSACTION; // zBTGTotal
TRY 
  DELETE FROM zBTGTotal;  
  @section = '2tech';
  -- 2 techs 
  DELETE FROM zbtg2Techs;  
  INSERT INTO zbtg2Techs
  SELECT *
  FROM zbtg1 a
  WHERE EXISTS (
    SELECT 1
      FROM (
        SELECT storecode, ro, line
        FROM zbtg1 
        GROUP BY storecode, ro, line
        HAVING COUNT(*) = 2) z
      WHERE z.ro = a.ro
        AND z.line = a.line);  
  INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1,tech2,hours2,wf2)
  SELECT d.storecode, d.ro, d.line, d.totalhours, d.tech1, d.tech1hrs, 
    round(d.tech1hrs/d.totalhours,4) as wf1, e.tech2, e.tech2hrs, 
    1 - round(d.tech1hrs/d.totalhours,4) as wf2
  FROM (
    SELECT *
    FROM (
      SELECT storecode, ro, line, totalhours,
        CASE 
          WHEN seq = (SELECT MIN(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN techkey 
        END AS tech1,
        CASE 
          WHEN seq = (SELECT MIN(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN hours
        END AS tech1hrs
      FROM zbtg2techs a) b
    WHERE tech1 IS NOT NULL) d    
  LEFT JOIN (       
    SELECT *
    FROM (        
    SELECT storecode, ro, line,
      CASE 
        WHEN seq = (SELECT max(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN techkey 
      END AS tech2,
      CASE 
        WHEN seq = (SELECT max(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN hours
      END AS tech2hrs  
    FROM zbtg2techs a) c
    WHERE tech2 IS NOT NULL) e ON d.storecode = e.storecode AND d.ro = e.ro AND d.line = e.line;   
  -- 3 techs
  @section = '3tech';
  DELETE FROM zbtg3techs;  
  INSERT INTO zbtg3Techs
  SELECT *
  FROM zbtg1 a
  WHERE EXISTS (
    SELECT 1
      FROM (
        SELECT storecode, ro, line
        FROM zbtg1 
        GROUP BY storecode, ro, line
        HAVING COUNT(*) = 3) z
      WHERE z.ro = a.ro
        AND z.line = a.line);           
  INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1,
    tech2,hours2,wf2,tech3,hours3,wf3) 
  SELECT d.storecode, d.ro, d.line, d.totalhours, d.tech1, d.tech1hrs, 
    round(d.tech1hrs/d.totalhours,5) as wf1, e.tech2, e.tech2hrs, 
    round(e.tech2hrs/d.totalhours,5) AS wf2, f.tech3, f.tech3hrs,
    round(1 - (round(d.tech1hrs/d.totalhours,5) + round(e.tech2hrs/d.totalhours, 5)),5) 
  FROM (
    SELECT *
    FROM (
      SELECT storecode, ro, line, totalhours,
        CASE 
          WHEN seq = (SELECT MIN(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN techkey 
        END AS tech1,
        CASE 
          WHEN seq = (SELECT MIN(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN hours
        END AS tech1hrs
      FROM zbtg3techs a) b
    WHERE tech1 IS NOT NULL) d    
  LEFT JOIN (       
    SELECT *
    FROM (        
    SELECT storecode, ro, line,
      CASE 
        WHEN seq = (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN techkey 
      END AS tech2,
      CASE 
        WHEN seq = (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN hours
      END AS tech2hrs  
    FROM zbtg3techs a) c
    WHERE tech2 IS NOT NULL) e ON d.storecode = e.storecode AND d.ro = e.ro AND d.line = e.line
  LEFT JOIN (       
    SELECT *
    FROM (        
    SELECT storecode, ro, line,
      CASE 
        WHEN seq <> (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN 
          CASE 
            WHEN seq <> (SELECT min(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN techkey 
          END
      END AS tech3,
      CASE 
        WHEN seq <> (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN
          CASE
            WHEN seq <> (SELECT min(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN hours
          END
      END AS tech3hrs  
    FROM zbtg3techs a) d
    WHERE tech3 IS NOT NULL) f ON d.storecode = f.storecode AND d.ro = f.ro AND d.line = f.line;   
    
  -- 4 techs
  @section = '4tech';
  DELETE FROM zbtg4Techs; 
  INSERT INTO zbtg4Techs
  SELECT a.*, 0
  FROM zbtg1 a
  WHERE EXISTS (
    SELECT 1
      FROM (
        SELECT storecode, ro, line
        FROM zbtg1 
        GROUP BY storecode, ro, line
        HAVING COUNT(*) = 4) z
      WHERE z.ro = a.ro
        AND z.line = a.line);   
  OPEN @rolinecur;
  TRY
    WHILE FETCH @rolinecur DO
      @ro = @rolinecur.ro;
      @line = @rolinecur.line;
      @i = 1;    
      OPEN @cur4tech;
      TRY 
        WHILE FETCH @cur4tech DO
          @seq = @cur4tech.seq;
          UPDATE zbtg4techs
          SET newseq = @i
          WHERE seq = @seq;
          @i = @i + 1;
        END WHILE;
      FINALLY
        CLOSE @cur4tech;
      END TRY;   
    END WHILE;
  FINALLY 
    CLOSE @rolinecur;
  END TRY;   
  INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1,
    tech2,hours2,wf2,tech3,hours3,wf3, tech4,hours4,wf4) 
  SELECT d.storecode, d.ro, d.line, d.totalhours, d.tech1, d.tech1hrs, 
    round(d.tech1hrs/d.totalhours,4) as wf1, e.tech2, e.tech2hrs, 
    round(e.tech2hrs/d.totalhours, 4) AS wf2, g.tech3, g.tech3hrs,
    round(g.tech3hrs/d.totalhours, 4) AS wf3, tech4, tech4hrs,
    1 - (round(tech3hrs/d.totalhours, 4) + round(tech2hrs/d.totalhours, 4) + round(tech1hrs/d.totalhours, 4)) AS wf4
  FROM (
    SELECT *
    FROM (
      SELECT storecode, ro, line, totalhours,
        CASE 
          WHEN newseq = 1 THEN techkey 
        END AS tech1,
        CASE 
          WHEN newseq = 1 THEN hours
        END AS tech1hrs
      FROM zbtg4techs a) b
    WHERE tech1 IS NOT NULL) d    
  LEFT JOIN (       
    SELECT *
    FROM (        
    SELECT storecode, ro, line,
      CASE 
        WHEN newseq = 2 THEN techkey 
      END AS tech2,
      CASE 
        WHEN newseq = 2 THEN hours
      END AS tech2hrs  
    FROM zbtg4techs a) c
    WHERE tech2 IS NOT NULL) e ON d.storecode = e.storecode AND d.ro = e.ro AND d.line = e.line  
  LEFT JOIN (       
    SELECT *
    FROM (        
    SELECT storecode, ro, line,
      CASE 
        WHEN newseq = 3 THEN techkey 
      END AS tech3,
      CASE 
        WHEN newseq = 3 THEN hours
      END AS tech3hrs  
    FROM zbtg4techs a) f
    WHERE tech3 IS NOT NULL) g ON d.storecode = g.storecode AND d.ro = g.ro AND d.line = g.line 
  LEFT JOIN (       
    SELECT *
    FROM (        
    SELECT storecode, ro, line,
      CASE 
        WHEN newseq = 4 THEN techkey 
      END AS tech4,
      CASE 
        WHEN newseq = 4 THEN hours
      END AS tech4hrs  
    FROM zbtg4techs a) h
    WHERE tech4 IS NOT NULL) i ON d.storecode = i.storecode AND d.ro = i.ro AND d.line = i.line;   
  -- 1 tech  
  @section = '1tech';
  INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1)
  SELECT storecode,ro,line,totalhours, techkey, hours,1.0
  FROM zbtg1 a
  WHERE EXISTS (
    SELECT 1
      FROM (
        SELECT storecode, ro, line
        FROM zbtg1 
        GROUP BY storecode, ro, line
        HAVING COUNT(*) = 1) z
      WHERE z.ro = a.ro
        AND z.line = a.line);             
COMMIT WORK;  	  
CATCH ALL
  ROLLBACK;
  RAISE dimTechGroup(3, 'zBTGTotal ' + @section + ' :: ' + __errtext);   
END TRY; // transaction      
-- 2nd test
BEGIN TRANSACTION;
TRY 
  @i = 1;
  OPEN @curZBTG;
  TRY
    WHILE FETCH @curZBTG DO
      INSERT INTO zBTG values(@i, @curZBTG.tech1,@curZBTG.tech2,@curZBTG.tech3,
        @curZBTG.tech4,@curZBTG.wf1,@curZBTG.wf2,@curZBTG.wf3,@curZBTG.wf4);
      @i = @i + 1;
    END WHILE;
  FINALLY
    CLOSE @curZBTG;
  END TRY;  
  OPEN @curZbtgTest;  
  TRY 
    IF FETCH @curZbtgTest THEN 
      RAISE dimTechGroup(4, '2nd Test: zBTG');  
    END IF;
  FINALLY
    CLOSE @curZbtgTest;
  END TRY;  
COMMIT WORK;  	  
CATCH ALL
  ROLLBACK;
  RAISE dimTechGroup(4, 'zBTGT ' + __errtext);   
END TRY; // transaction   

BEGIN TRANSACTION; //tmpBrTechGroup
TRY 
  DELETE FROM tmpBrTechGroup;
  INSERT INTO tmpBrTechGroup   
  SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech2 IS NULL;
  INSERT INTO tmpBrTechGroup   
  SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NULL;
  INSERT INTO tmpBrTechGroup   
  SELECT brdgTechGroupKey,tech2,wf2 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NULL;
  INSERT INTO tmpBrTechGroup  
  SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NOT NULL AND tech4 IS NULL;
  INSERT INTO tmpBrTechGroup   
  SELECT brdgTechGroupKey,tech2,wf2 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NOT NULL AND tech4 IS NULL;
  INSERT INTO tmpBrTechGroup   
  SELECT brdgTechGroupKey,tech3,wf3 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NOT NULL AND tech4 IS NULL;
  INSERT INTO tmpBrTechGroup  
  SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech4 IS NOT NULL;
  INSERT INTO tmpBrTechGroup  
  SELECT brdgTechGroupKey,tech2,wf2 FROM zbtg WHERE tech4 IS NOT NULL;
  INSERT INTO tmpBrTechGroup   
  SELECT brdgTechGroupKey,tech3,wf3 FROM zbtg WHERE tech4 IS NOT NULL;
  INSERT INTO tmpBrTechGroup  
  SELECT brdgTechGroupKey,tech4,wf4 FROM zbtg WHERE tech4 IS NOT NULL; 
COMMIT WORK;  	  
CATCH ALL
  ROLLBACK;
  RAISE dimTechGroup(5, 'tmpBrTechGroup ' + __errtext);   
END TRY; // transaction  

BEGIN TRANSACTION; //brTechGroup
TRY 
  @i = (SELECT MAX(TechGroupKey) + 1 FROM dimTechGroup);  -- 2912
  OPEN @curTechGroup;
  TRY
    WHILE FETCH @curTechGroup DO 
      @TechGroupKey = @curTechGroup.TechGroupKey;
      OPEN @curNew;
      TRY 
        WHILE FETCH @curNew DO
          @TechKey = @curNew.TechKey;
          @WeightFactor = @curNew.WeightFactor;
          INSERT INTO brTechGroup values(@i, @TechKey, @WeightFactor);
        END WHILE;
      FINALLY
        CLOSE @curNew;
      END TRY;
      @i = @i + 1;
    END WHILE;
  FINALLY
    CLOSE @curTechGroup;
  END TRY;     
  
  INSERT INTO DimTechGroup
  SELECT DISTINCT TechGroupKey
  FROM brTechGroup a
  WHERE NOT EXISTS (
    SELECT 1
    FROM dimtechgroup
    WHERE techgroupkey = a.techgroupkey);
COMMIT WORK;  	  
CATCH ALL
  ROLLBACK;
  RAISE dimTechGroup(6, 'brTechGroup/dimTechGroup ' + __errtext);   
END TRY; // transaction  