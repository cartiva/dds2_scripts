-- IN the style of dimCorCodeGroup ala Kimball tip 142

CREATE TABLE xfmDimTechGroup1 (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  linehours double(6),
  TechKey integer,
  TechHours double(6)) IN database; 
-- need to GROUP to accomodate stupid arkona doubling lines
-- AND stupid employees entering the cor code twice   
INSERT INTO xfmDimTechGroup1
SELECT a.ptco#, a.ptdoc#, a.ptline, e.linehours, 
  CASE 
    WHEN a.ptco# = 'RY1' THEN
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
    WHEN a.ptco# = 'RY2' THEN 
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
  END AS TechKey, a.techhours  
FROM (
  SELECT b.ptco#, a.ptdoc#, b.ptline, b.pttech, SUM(b.ptlhrs) AS techhours, MIN(b.ptdate) AS mindate
  FROM stgArkonaPDPPHDR a
  INNER JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND a.ptchk# NOT LIKE 'v%'
    AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
    AND b.ptline < 900
    AND b.pttech <> ''
    AND b.ptlhrs > 0
  GROUP BY b.ptco#, a.ptdoc#, b.ptline, b.pttech) a
LEFT JOIN dimtech b ON a.pttech = b.technumber
  AND a.ptco# = b.storecode
  AND a.mindate BETWEEN b.techkeyfromdate AND b.techkeythrudate  
LEFT JOIN (
  SELECT storecode, TechKey 
  FROM dimTech
  WHERE technumber = 'UNK') c ON a.ptco# = c.storecode  
LEFT JOIN (    
  SELECT b.ptco#, a.ptdoc#, b.ptline, SUM(b.ptlhrs) AS linehours
  FROM tmpPDPPHDR a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND a.ptchk# NOT LIKE 'v%'
    AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
    AND b.ptline < 900
    AND b.pttech <> ''
    AND b.ptlhrs > 0
  GROUP BY b.ptco#, a.ptdoc#, b.ptline) e ON a.ptco# = e.ptco# AND a.ptdoc# = e.ptdoc# AND a.ptline = e.ptline;
    
INSERT INTO xfmDimTechGroup1
SELECT a.ptco#, a.ptro#, a.ptline, e.linehours, 
  CASE 
    WHEN a.ptco# = 'RY1' THEN
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
    WHEN a.ptco# = 'RY2' THEN 
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
  END AS TechKey, a.techhours 
FROM (
  SELECT ptco#, ptro#, ptline, pttech, SUM(ptlhrs) AS techHours, MIN(ptdate) AS mindate
  FROM stgArkonaSDPRDET a
  WHERE a.pttech <> ''
    AND a.ptlhrs > 0
    AND a.ptco# IN ('RY1','RY2')
  GROUP BY ptco#, ptro#, ptline, pttech) a
LEFT JOIN dimtech b ON a.pttech = b.technumber
  AND a.ptco# = b.storecode
  AND a.mindate BETWEEN b.techkeyfromdate AND b.techkeythrudate  
LEFT JOIN (
  SELECT storecode, TechKey 
  FROM dimTech
  WHERE technumber = 'UNK') c ON a.ptco# = c.storecode    
LEFT JOIN (  
  SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS linehours
  FROM stgArkonaSDPRDET a
  WHERE a.pttech <> ''
    AND a.ptlhrs > 0
    AND a.ptco# IN ('RY1','RY2')
  GROUP BY ptco#, ptro#, ptline) e ON a.ptco# = e.ptco# AND a.ptro# = e.ptro# AND a.ptline = e.ptline
WHERE NOT EXISTS (
  SELECT 1
  FROM xfmDimTechGroup1
  WHERE ro = a.ptro#
    AND line = a.ptline);    
    
DROP TABLE xfmDimTechGroup2;
CREATE TABLE xfmDimTechGroup2 (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  TechKey integer,
  WeightFactor double(4)) IN database; 
     
INSERT INTO xfmDimTechGroup2
SELECT storecode, ro, line, techkey, round(sum(techhours/linehours),4)
FROM xfmDimTechGroup1
GROUP BY storecode, ro, line, techkey;

-- constraint: unique index
EXECUTE PROCEDURE sp_CreateIndex90( 
   'xfmDimTechGroup2','xfmDimTechGroup2.adi','NK',
   'StoreCode;RO;line;techkey;weightfactor','',2051,512,'' );  
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimTechGroup2','storecode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimTechGroup2','RO', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimTechGroup2','line', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimTechGroup2','TechKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimTechGroup2','WeightFactor', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );     
  
  
  
DROP TABLE xfmDimTechGroup3;
CREATE TABLE xfmDimTechGroup3 (
  storecode cichar(3), 
  ro cichar(9),
  line integer,
  TechKeyGroup cichar(120)) IN database;  
  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'xfmDimTechGroup3','xfmDimTechGroup3.adi','NK',
   'StoreCode;RO;line','',2051,512,'' );  
   
DECLARE @cur1 CURSOR AS 
  SELECT storecode, ro, line
  FROM xfmDimTechGroup2
  GROUP BY storecode, ro, line
  ORDER BY storecode, ro, line;  
DECLARE @cur2 CURSOR AS 
  SELECT techkey, weightfactor
  FROM xfmDimTechGroup2
  WHERE storecode = @storecode
    AND ro = @ro
    AND line = @line
  ORDER BY techkey;
DECLARE @str string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @techkey string;  
DECLARE @weightfactor string;
OPEN @cur1;
TRY
  WHILE FETCH @cur1 DO
    @storecode = @cur1.storecode;
    @ro = @cur1.ro;
    @line = @cur1.line;
    OPEN @cur2;
    TRY
      @str = '';
      WHILE FETCH @cur2 DO
        @techkey = TRIM(CAST(@cur2.techkey AS sql_char));
        @weightfactor = TRIM(CAST(@cur2.weightfactor AS sql_char));
        @str = @str + @techkey + '|' + @weightfactor + '|';  
      END WHILE;
      INSERT INTO xfmDimTechGroup3 values(@storecode, @ro, @line, @str);
      @str = '';
    FINALLY
      CLOSE @cur2;
    END TRY;
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;    

-- ok, there are the pieces
-- define behavior for unknown & N/A
Unknown: line has a technumber with flag hours which does NOT decode IN dimtech
N/A: line has no technumber

dimTechGroup: TechGroupKey  FlatGroup
                  1           N/A     
brTechGroup: TechGroupKey  TechKey  
                  1           1     (storecode does NOT matter, just pick one of the 2 NA techkeys)  
     
DO NOT have to worry about UNKNOWN  
the appropriate UNK techkey will already have been assigned               

DROP TABLE dimTechGroup;  
CREATE TABLE dimTechGroup (
  TechGroupKey integer,
  FlatGroup cichar(120)) IN database;
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimTechGroup','dimTechGroup.adi','PK',
   'TechGroupKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimTechGroup','Table_Primary_Key',
  'PK', 'RETURN_ERROR', NULL);   
-- index: NK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimTechGroup','dimTechGroup.adi','NK',
   'FlatGroup','',2051,512,'' );  -- unique   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimTechGroup','FlatGroup', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   

-- store code IS irrelevant to N/A, no cor code IS simply no cor code  
INSERT INTO dimTechGroup values (1, 'N/A');  

DECLARE @cur1 CURSOR AS SELECT DISTINCT techkeygroup FROM xfmDimTechGroup3; 
DECLARE @i integer;
@i = (SELECT MAX(TechGroupKey) + 1 FROM dimTechGroup);  
OPEN @cur1;
TRY
  WHILE FETCH @cur1 DO
    INSERT INTO dimTechGroup values (@i, @cur1.TechKeyGroup);
    @i = @i + 1; 
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY; 

DROP TABLE brTechGroup;
CREATE TABLE brTechGroup (
  TechGroupKey integer,
  TechKey integer,
  WeightFactor double(4)) IN database;
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brTechGroup','brTechGroup.adi','PK',
   'TechGroupKey;TechKey;WeightFactor','',2051,512,'' );   
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('brTechGroup','Table_Primary_Key',
  'PK', 'RETURN_ERROR', NULL);  
-- index  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brTechGroup','brTechGroup.adi','TechGroupKey',
   'TechGroupKey','',2,512,'' );     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brTechGroup','brTechGroup.adi','TechKey',
   'TechKey','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brTechGroup','brTechGroup.adi','WeightFactor',
   'WeightFactor','',2,512,'' );   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'brTechGroup','TechGroupKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'brTechGroup','TechKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'brTechGroup','WeightFactor', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );     
       
--RI gets added ON the many side
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimTechGroup-brTechGroup');
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimTech-brTechGroup');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimTechGroup-brTechGroup','dimTechGroup','brTechGroup','TechGroupKey',2,2,NULL,'','');  
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimTech-brTechGroup','dimTech','brTechGroup','TechKey',2,2,NULL,'','');   
     
INSERT INTO brTechGroup values(1,335,1);  

---- brTechGroup
INSERT INTO brTechGroup
SELECT a.techgroupkey, c.techkey, c.weightfactor
FROM dimTechGroup a
INNER JOIN xfmDimTechGroup3 b ON a.flatgroup = b.Techkeygroup
INNER JOIN xfmDimTechGroup2 c ON b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line 
GROUP BY a.techgroupkey, c.techkey, c.weightfactor


-- UPDATE existing rows IN factRepairORder


UPDATE factRepairOrder
SET TechGroupKey = x.TechGroupKey
FROM (
  SELECT a.storecode, a.ro, a.line, coalesce(c.TechGroupKey, d.Techgroupkey) AS TechGroupKey
  FROM factRepairOrder a
  LEFT JOIN xfmDimTechGroup3 b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line  
  LEFT JOIN dimTechGroup c ON b.TechKeyGroup = c.flatgroup
  LEFT JOIN dimTechGroup d ON 1 = 1
    AND d.TechGroupKey = (
      SELECT TechGroupKey
      FROM brTechGroup
      WHERE Techkey = (
        SELECT Techkey
        FROM dimtech
        WHERE storecode = 'RY1'
           AND Technumber = 'N/A'))) x
WHERE factRepairOrder.storecode = x.storecode
  AND factRepairOrder.ro = x.ro
  AND factRepairOrder.line = x.line; 
  

-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimTechGroup-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimTechGroup-factRepairOrder',
     'dimTechGroup', 'factRepairOrder', 'TechGroupKey', 
     2, 2, NULL, '', '');            


     
SELECT e.technumber, coalesce(e.name, description), round(sum(a.flaghours * d.weightfactor),2)
FROM factRepairOrder a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND b.yearmonth = 201306
INNER JOIN dimTechGroup c ON a.TechGroupKey = c.TechGroupKey  
INNER JOIN brTechGroup d ON c.TechGroupKey = d.TechGroupKey
INNER JOIN dimTech e ON d.TechKey = e.TechKey
  AND e.FlagDeptCode = 'MR'
WHERE a.storecode = 'RY1'
GROUP BY e.technumber, coalesce(e.name, description)
ORDER BY technumber

--< Nightly -------------------------------------------------------------------
DELETE FROM xfmDimTechGroup1;
INSERT INTO xfmDimTechGroup1
SELECT a.ptco#, a.ptdoc#, a.ptline, e.linehours, 
  CASE 
    WHEN a.ptco# = 'RY1' THEN
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
    WHEN a.ptco# = 'RY2' THEN 
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
  END AS TechKey, a.techhours  
FROM (
  SELECT b.ptco#, a.ptdoc#, b.ptline, b.pttech, SUM(b.ptlhrs) AS techhours, MIN(b.ptdate) AS mindate
  FROM tmpPDPPHDR a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND a.ptchk# NOT LIKE 'v%'
    AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
    AND b.ptline < 900
    AND b.pttech <> ''
    AND b.ptlhrs > 0
  GROUP BY b.ptco#, a.ptdoc#, b.ptline, b.pttech) a
LEFT JOIN dimtech b ON a.pttech = b.technumber
  AND a.ptco# = b.storecode
  AND a.mindate BETWEEN b.techkeyfromdate AND b.techkeythrudate  
LEFT JOIN (
  SELECT storecode, TechKey 
  FROM dimTech
  WHERE technumber = 'UNK') c ON a.ptco# = c.storecode  
LEFT JOIN (    
  SELECT b.ptco#, a.ptdoc#, b.ptline, SUM(b.ptlhrs) AS linehours
  FROM tmpPDPPHDR a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND a.ptchk# NOT LIKE 'v%'
    AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
    AND b.ptline < 900
    AND b.pttech <> ''
    AND b.ptlhrs > 0
  GROUP BY b.ptco#, a.ptdoc#, b.ptline) e ON a.ptco# = e.ptco# AND a.ptdoc# = e.ptdoc# AND a.ptline = e.ptline;   
INSERT INTO xfmDimTechGroup1
SELECT a.ptco#, a.ptro#, a.ptline, e.linehours, 
  CASE 
    WHEN a.ptco# = 'RY1' THEN
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
    WHEN a.ptco# = 'RY2' THEN 
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
  END AS TechKey, a.techhours 
FROM (
  SELECT ptco#, ptro#, ptline, pttech, SUM(ptlhrs) AS techHours, MIN(ptdate) AS mindate
  FROM tmpSDPRDET a
  WHERE a.pttech <> ''
    AND a.ptlhrs > 0
    AND a.ptco# IN ('RY1','RY2')
  GROUP BY ptco#, ptro#, ptline, pttech) a
LEFT JOIN dimtech b ON a.pttech = b.technumber
  AND a.ptco# = b.storecode
  AND a.mindate BETWEEN b.techkeyfromdate AND b.techkeythrudate  
LEFT JOIN (
  SELECT storecode, TechKey 
  FROM dimTech
  WHERE technumber = 'UNK') c ON a.ptco# = c.storecode    
LEFT JOIN (  
  SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS linehours
  FROM stgArkonaSDPRDET a
  WHERE a.pttech <> ''
    AND a.ptlhrs > 0
    AND a.ptco# IN ('RY1','RY2')
  GROUP BY ptco#, ptro#, ptline) e ON a.ptco# = e.ptco# AND a.ptro# = e.ptro# AND a.ptline = e.ptline
WHERE NOT EXISTS (
  SELECT 1
  FROM xfmDimTechGroup1
  WHERE ro = a.ptro#
    AND line = a.ptline);   

DELETE FROM xfmDimTechGroup2; 
INSERT INTO xfmDimTechGroup2
SELECT storecode, ro, line, techkey, round(sum(techhours/linehours),4)
FROM xfmDimTechGroup1
GROUP BY storecode, ro, line, techkey;

DELETE FROM xfmDimTechGroup3;
   
DECLARE @cur1 CURSOR AS 
  SELECT storecode, ro, line
  FROM xfmDimTechGroup2
  GROUP BY storecode, ro, line
  ORDER BY storecode, ro, line;  
DECLARE @cur2 CURSOR AS 
  SELECT techkey, weightfactor
  FROM xfmDimTechGroup2
  WHERE storecode = @storecode
    AND ro = @ro
    AND line = @line
  ORDER BY techkey;
DECLARE @str string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @techkey string;  
DECLARE @weightfactor string;
OPEN @cur1;
TRY
  WHILE FETCH @cur1 DO
    @storecode = @cur1.storecode;
    @ro = @cur1.ro;
    @line = @cur1.line;
    OPEN @cur2;
    TRY
      @str = '';
      WHILE FETCH @cur2 DO
        @techkey = TRIM(CAST(@cur2.techkey AS sql_char));
        @weightfactor = TRIM(CAST(@cur2.weightfactor AS sql_char));
        @str = @str + @techkey + '|' + @weightfactor + '|';  
      END WHILE;
      INSERT INTO xfmDimTechGroup3 values(@storecode, @ro, @line, @str);
      @str = '';
    FINALLY
      CLOSE @cur2;
    END TRY;
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;   

/*
-- these are the NEW TechGroup rows
SELECT DISTINCT a.TechKeyGroup
FROM xfmDimTechGroup3 a
LEFT JOIN dimTechGroup b ON a.TechKeyGroup = b.FlatGroup
WHERE b.flatgroup IS NULL 
*/

-- need to ADD new rows to dimTechGroup AND brTechGroup
-- AND that IS fucking IT for nightly UPDATE to dimTechGroup 
-- dimTechGroup
DECLARE @cur1 CURSOR AS 
  SELECT DISTINCT a.TechKeyGroup
  FROM xfmDimTechGroup3 a
  LEFT JOIN dimTechGroup b ON a.TechKeyGroup = b.FlatGroup
  WHERE b.flatgroup IS NULL;
DECLARE @i integer;
@i = (SELECT MAX(TechGroupKey) + 1 FROM dimTechGroup);  
OPEN @cur1;
TRY
  WHILE FETCH @cur1 DO
    INSERT INTO dimTechGroup values (@i, @cur1.TechKeyGroup);
    @i = @i + 1; 
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;   

---- brTechGroup
INSERT INTO brTechGroup
SELECT a.techgroupkey, c.techkey, c.weightfactor
FROM dimTechGroup a
INNER JOIN xfmDimTechGroup3 b ON a.flatgroup = b.Techkeygroup
INNER JOIN xfmDimTechGroup2 c ON b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line 
WHERE NOT EXISTS (
  SELECT 1
  FROM brTechGroup
  WHERE TechGroupKey = a.TechGroupKey)
GROUP BY a.techgroupkey, c.techkey, c.weightfactor

-- wafuckinghooo
--/> Nightly -------------------------------------------------------------------

--- Dependencies & zProc ------------------------------------------------------
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimTechGroup'
  AND b.dObject = 'dimTech';
  
    
--/ Dependencies & zProc -----------------------------------------------------    
