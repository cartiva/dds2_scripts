-- hierarchy definition
-- DELETE FROM zcb WHERE name = 'cigar box' AND storecode = 'ry3';
INSERT INTO zcb
SELECT distinct name, 'RY3',dl1, dl2, dl3, al1, al2, al3, al4, glaccount, 1
FROM zcb
WHERE glaccount IS NULL 
  AND dl2 NOT IN ('ncr','body shop')
  AND dl3 NOT IN ('car wash','pdq','detail','main shop')
  AND name = 'cigar box';  

/***************** service doc 32 ********************************************/
select * FROM zcb WHERE storecode = 'ry3' AND glaccount IS NOT NULL ORDER BY glaccount
delete FROM zcb WHERE storecode = 'ry3' AND glaccount IS NOT NULL;
-- sales
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 32 AND l3 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl2 = 'Service'
  AND al3 = 'Sales';  
-- cogs 
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, c.gmdboa, 1
FROM zcb a,
(SELECT b.gmdboa FROM tmpdocs a 
  LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct 
    AND b.gmyear = 2012 
  WHERE reportnumber = 32 AND l3 = 10 AND glaccount IS NOT NULL
    AND gmdboa <> '') c
WHERE storecode = 'ry3'
  AND dl2 = 'Service'
  AND al3 = 'COGS'; 
-- parts split
-- hmm, appears service doc does NOT include the parts split
INSERT INTO zcb
SELECT distinct a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '346700', .5
FROM zcb a
WHERE storecode = 'ry3' AND dl2 = 'Service' AND al3 = 'Sales'; 
INSERT INTO zcb  
SELECT distinct a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '347800', .5
FROM zcb a
WHERE storecode = 'ry3' AND dl2 = 'Service' AND al3 = 'Sales'; 
INSERT INTO zcb
SELECT distinct a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '366700', .5
FROM zcb a
WHERE storecode = 'ry3' AND dl2 = 'Service' AND al3 = 'COGS';  
INSERT INTO zcb 
SELECT distinct a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '367800', .5
FROM zcb a
WHERE storecode = 'ry3' AND dl2 = 'Service' AND al3 = 'COGS'; 
-- personnel
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 32  and l2 = 20 and l3 = 20 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl2 = 'service'
  AND al3 = 'Personnel Expenses';
-- semi-fixed -- no point IN fucking with policy
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 32  and l2 = 20 and l3 = 30 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl2 = 'service'
  AND al3 = 'semi-fixed Expenses'
  AND al4 = 'NA';  
-- fixed
INSERT INTO zcb
SELECT distinct a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 32  and l2 = 20 and l3 = 40 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl2 = 'service'
  AND al3 = 'fixed Expenses';   
  
/* proof */
-- expenses are good, gross IS off
SELECT yearmonth, dl2, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth = 201210 --BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'service'
  AND storecode = 'ry3'
GROUP BY yearmonth, dl2, al3;   

-- gross/sales difference IS IN parts split, just be sure to check against parts
-- parts split sales: 9291, cogs 6165
SELECT yearmonth, dl2, al3, glaccount, gmdesc, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND gmyear = 2012
WHERE a.yearmonth = 201210 --BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'service'
  AND storecode = 'ry3'
  AND al3 in ('sales','cogs')
GROUP BY yearmonth, dl2, al3, glaccount, gmdesc; 
  
/***************** service doc 32 ********************************************/

/***************** parts doc 32 ********************************************/
-- sales, include parts split
--DELETE FROM zcb WHERE name = 'cigar box' AND storecode = 'ry3' AND dl2 = 'parts' AND glaccount IS NOT NULL;
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 
  CASE 
    WHEN b.glaccount IN ('346700','347800') THEN .5
    ELSE 1
  END 
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 33 AND l3 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl2 = 'Parts'
  AND al3 = 'Sales';  
-- cogs, include parts split
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, c.gmdboa, 
  CASE 
    WHEN c.gmdboa IN ('366700','367800') THEN .5
    ELSE 1
  END 
FROM zcb a,
(SELECT b.gmdboa, b.gmacct FROM tmpdocs a 
  LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct 
    AND b.gmyear = 2012 
  WHERE reportnumber = 33 AND l3 = 10 AND glaccount IS NOT NULL
    AND gmdboa <> '') c
WHERE storecode = 'ry3'
  AND dl2 = 'Parts'
  AND al3 = 'COGS';   
-- personnel
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 33  and l2 = 20 and l3 = 20 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl2 = 'parts'
  AND al3 = 'Personnel Expenses';  
-- semi-fixed  
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 33  and l2 = 20 and l3 = 30 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl2 = 'parts'
  AND al3 = 'semi-fixed Expenses'
  AND al4 = 'NA';  
-- fixed  
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 33  and l2 = 20 and l3 = 40 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl2 = 'parts'
  AND al3 = 'fixed Expenses';    
  
/* proof */
-- expenses are good, gross IS not
SELECT yearmonth, dl2, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth = 201210 --BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'parts'
  AND storecode = 'ry3'
GROUP BY yearmonth, dl2, al3;   

-- gross/sales difference IS IN parts split
SELECT yearmonth, dl2, al3, glaccount, gmdesc, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND gmyear = 2012
WHERE a.yearmonth = 201210 --BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'parts'
  AND storecode = 'ry3'
  AND al3 in ('sales','cogs')
GROUP BY yearmonth, dl2, al3, glaccount, gmdesc;   
/***************** parts doc 32 ********************************************/

/***************** new doc 30 ************************************************/
-- new sales
--SELECT * FROM zcb WHERE dl3 = 'new vehicles' AND storecode = 'ry3'
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 30 AND l2 = 10 AND l3 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl3 = 'new vehicles'
  AND al3 = 'Sales';  
-- cogs 
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, c.gmdboa, 1
FROM zcb a,
(SELECT b.gmdboa FROM tmpdocs a 
  LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct 
    AND b.gmyear = 2012 
  WHERE reportnumber = 30 AND l2 = 10 AND l3 = 10 AND glaccount IS NOT NULL
    AND gmdboa <> '') c
WHERE storecode = 'ry3'
  AND dl3 = 'new vehicles'
  AND al3 = 'COGS'; 
-- expenses variable
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 30 and l2 = 20 and l3 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl3 = 'new vehicles'
  AND al3 = 'variable Expenses'
  AND al4 = 'na';  
-- expenses personnel
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 30 and l2 = 20 and l3 = 20 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl3 = 'new vehicles'
  AND al3 = 'personnel Expenses'
  AND al4 = 'na';   
-- expenses semi-fixed
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 30 and l2 = 20 and l3 = 30 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl3 = 'new vehicles'
  AND al3 = 'semi-fixed Expenses'
  AND al4 = 'na';   
-- expenses fixed
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 30 and l2 = 20 and l3 = 40 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl3 = 'new vehicles'
  AND al3 = 'fixed Expenses'
  AND al4 = 'na';    
  
/* proof */
-- ALL good
SELECT yearmonth, dl2, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth = 201210 --BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND dl3 = 'new vehicles'
  AND storecode = 'ry3'
GROUP BY yearmonth, dl2, al3;    
/***************** new doc 30 ************************************************/

/***************** used doc 30 ************************************************/
-- sales
--delete FROM zcb WHERE dl3 = 'used vehicles' AND storecode = 'ry3' AND glaccount IS NOT NULL 
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 31 AND l2 = 10 AND l3 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl3 = 'used vehicles'
  AND al3 = 'Sales';  
-- cogs 
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, c.gmdboa, 1
FROM zcb a,
(SELECT b.gmdboa FROM tmpdocs a 
  LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct 
    AND b.gmyear = 2012 
  WHERE reportnumber = 31 AND l2 = 10 AND l3 = 10 AND glaccount IS NOT NULL
    AND gmdboa <> '') c
WHERE storecode = 'ry3'
  AND dl3 = 'used vehicles'
  AND al3 = 'COGS'; 
UPDATE zcb
SET al3 = 'COGS'
WHERE glaccount IN (
  SELECT gmacct 
  FROM zcb a
  LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
    AND gmyear = 2012
  WHERE storecode = 'ry3' and dl3 = 'used vehicles' AND al2 = 'gross profit'
    AND gmdesc LIKE 'CST%'
    AND al3 = 'sales');  
-- expenses variable
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 31 and l2 = 20 and l3 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl3 = 'used vehicles'
  AND al3 = 'variable Expenses'
  AND al4 = 'na'; 
-- expenses personnel
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 31 and l2 = 20 and l3 = 20 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl3 = 'used vehicles'
  AND al3 = 'personnel Expenses'
  AND al4 = 'na';   
-- expenses semi-fixed
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 31 and l2 = 20 and l3 = 30 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl3 = 'used vehicles'
  AND al3 = 'semi-fixed Expenses'
  AND al4 = 'na';   
-- expenses fixed
INSERT INTO zcb
SELECT a.name, 'RY3', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 31 and l2 = 20 and l3 = 40 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry3'
  AND dl3 = 'used vehicles'
  AND al3 = 'fixed Expenses'
  AND al4 = 'na';    
  
/* proof */
-- ALL good
SELECT yearmonth, dl2, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth = 201210 --BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND dl3 = 'used vehicles'
  AND storecode = 'ry3'
GROUP BY yearmonth, dl2, al3;   
-- no 
SELECT glaccount, dl2
FROM zcb
WHERE storecode = 'ry3'
  AND glaccount IS NOT NULL
GROUP BY glaccount, dl2
HAVING COUNT(*) > 1   

/***************** used doc 30 ************************************************/  

/**************** f & i ******************************************************/
/**/
-- f&i
DELETE FROM zcb WHERE name = 'RY3 F&I';
INSERT INTO zcb 
SELECT 'RY3 F&I'as name,'RY3'as storecode,'F&I'as dl1,
  'New' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'Sales' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota; 
INSERT INTO zcb 
SELECT 'RY3 F&I'as name,'RY3'as storecode,'F&I'as dl1,
  'New' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'COGS' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota;
INSERT INTO zcb 
SELECT 'RY3 F&I'as name,'RY3'as storecode,'F&I'as dl1,
  'Used' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'Sales' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota;  
INSERT INTO zcb 
SELECT 'RY3 F&I'as name,'RY3'as storecode,'F&I'as dl1,
  'Used' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'COGS' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota; 

INSERT INTO zcb   
SELECT 'RY3 F&I'as name,'RY3'as storecode,'F&I'as dl1,
  CASE 
    WHEN description LIKE '%new%' THEN 'New'
    WHEN description LIKE '%used%' THEN 'Used'
    ELSE 'XXXX'
  END AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  CASE 
    WHEN gmtype IN ('4','7') THEN 'Sales'
    WHEN gmtype = '5' THEN 'COGS'
    ELSE 'XXXXX'
  END AS al3, 'NA' as al4, glaccount, 1 AS split
FROM tmpdocs a
LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
WHERE reportnumber = 34
  AND glaccount IS NOT NULL 
UNION -- need to ADD those COGS accounts NOT specified IN doc
SELECT 'RY3 F&I'as name,'RY3'as storecode,'F&I'as dl1,
  CASE 
    WHEN description LIKE '%new%' THEN 'New'
    WHEN description LIKE '%used%' THEN 'Used'
    ELSE 'XXXX'
  END AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'COGS' AS al3, 'NA' as al4, gmdboa, 1 AS split
FROM tmpdocs a
INNER JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
WHERE reportnumber = 34
  AND glaccount IS NOT NULL 
  AND gmtype = '4';  
  
-- ALL the pieces are good, go ahead AND ADD f&i
-- f&i
INSERT INTO zcb
SELECT 'Cigar Box', storecode, 'Store', 'Variable', 'New Vehicles','Net Profit', al2, al3, al4, glaccount, 1
--SELECT *
FROM zcb
WHERE name = 'ry3 f&i'
  AND dl2 = 'new'
  AND glaccount IS NOT NULL;    
  
INSERT INTO zcb
SELECT 'Cigar Box', storecode, 'Store', 'Variable', 'Used Vehicles','Net Profit', al2, al3, al4, glaccount, 1
--SELECT *
FROM zcb
WHERE name = 'ry3 f&i'
  AND dl2 = 'used'
  AND glaccount IS NOT NULL;     
/**************** f & i ******************************************************/  


