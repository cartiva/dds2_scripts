DROP TABLE ext_arkona_sypffxmst;
CREATE TABLE ext_arkona_sypffxmst (
  factory_code cichar(3) constraint NOT NULL,
  the_year integer constraint NOT NULL,
  factory_account cichar(10) constraint NOT NULL,
  page integer constraint NOT NULL,
  line numeric(6,1) constraint NOT NULL,
  col integer constraint NOT NULL,
  start_position integer constraint NOT NULL) IN database;
  
  
select * from ext_arkona_sypffxmst WHERE factory_account = '240'


SELECT a.fxgact, a.fxfact, b.*, d.gmat06, d.gmut06, d.gmactive
FROM stgArkonaFFPXREFDTA a 
LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
-- LEFT JOIN zcb c on b.factory_account = c.gmcoa
LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
  AND d.gmyear = 2015
WHERE a.fxcyy = 2015 
  AND a.fxconsol = '' 
  AND LEFT(fxgact, 1) = '1'
ORDER BY b.page, b.line, b.col

-- april statement rox exc pages 1, 2, 4 L59 (parts split), 5k, 8
SELECT b.page, b.line, b.col, SUM(e.gttamt)
FROM stgArkonaFFPXREFDTA a 
LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
-- LEFT JOIN zcb c on b.factory_account = c.gmcoa
LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
  AND d.gmyear = 2015
LEFT JOIN stgArkonaGLPTRNS e on d.gmacct = e.gtacct
  AND e.gtdate BETWEEN '04/01/2015' AND '04/30/2015'
WHERE a.fxcyy = 2015 
  AND a.fxconsol = '' 
  AND LEFT(fxgact, 1) = '1'
  AND d.gmactive = 'Y'
  AND page = 16
GROUP BY b.page, b.line, b.col  
ORDER BY b.page, b.line, b.col

-- same thing for june
SELECT b.page, b.line, b.col, SUM(e.gttamt)
FROM stgArkonaFFPXREFDTA a 
LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
-- LEFT JOIN zcb c on b.factory_account = c.gmcoa
LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
  AND d.gmyear = 2015
LEFT JOIN stgArkonaGLPTRNS e on d.gmacct = e.gtacct
  AND e.gtdate BETWEEN '06/01/2015' AND '06/30/2015'
WHERE a.fxcyy = 2015 
  AND a.fxconsol = '' 
  AND LEFT(fxgact, 1) = '1'
  AND d.gmactive = 'Y'
GROUP BY b.page, b.line, b.col  
ORDER BY b.page, b.line, b.col


-- bugs
page 6 line 49 (body parts split?)
  june
    st: 162990   112555
    qu:  97968    63819
    
page 7 ALL lines > 24 are way low    


-- accounts NOT currently IN zcb
SELECT a.fxgact, fxfact, b.page, b.line, b.col
FROM stgArkonaFFPXREFDTA a 
LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
-- LEFT JOIN zcb c on b.factory_account = c.gmcoa
LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
  AND d.gmyear = 2015
WHERE a.fxcyy = 2015 
  AND a.fxconsol = '' 
  AND LEFT(fxgact, 1) = '1'
  AND d.gmactive = 'Y'
  AND NOT EXISTS (
    SELECT 1
    FROM zcb
    WHERE glaccount = a.fxgact)
ORDER BY b.page, b.line, b.col    

-- accounts NOT currently IN zcb page 6
SELECT a.fxgact, fxfact, b.page, b.line, b.col, e.*
FROM stgArkonaFFPXREFDTA a 
LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
  AND d.gmyear = 2015
LEFT JOIN zcb e on a.fxgact = e.glaccount  
  AND e.name = 'gmfs gross'
WHERE a.fxcyy = 2015 
  AND a.fxconsol = '' 
  AND LEFT(fxgact, 1) = '1'
  AND d.gmactive = 'Y'
  AND b.page = 16
ORDER BY b.page, b.line, b.col   

-- accounts NOT currently IN zcb page 6
-- with april total AND coa descriptions
SELECT *
FROM (
  SELECT a.fxgact, fxfact, b.page, b.line, b.col, d.gmdesc, SUM(gttamt)
  FROM stgArkonaFFPXREFDTA a 
  LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
  LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
    AND d.gmyear = 2015
  LEFT JOIN stgArkonaGLPTRNS e on d.gmacct = e.gtacct
    AND e.gtdate BETWEEN '04/01/2015' AND '04/30/2015'
  WHERE a.fxcyy = 2015 
    AND a.fxconsol = '' 
    AND LEFT(fxgact, 1) = '1'
    AND d.gmactive = 'Y'
    AND b.page = 16
  GROUP BY a.fxgact, fxfact, b.page, b.line, b.col, d.gmdesc) m  
LEFT JOIN zcb n on m.fxgact = n.glaccount
  AND n.name = 'gmfs gross'  
--WHERE n.storecode IS NULL   
--ORDER BY fxgact
ORDER BY m.page, m.line, m.col 


SELECT * FROM ext_arkona_sypffxmst


-- page 6 accounts

  SELECT a.fxconsol, a.fxgact, fxfact, b.page, b.line, b.col, d.gmdesc, d.gmtype, d.gmdept
  FROM stgArkonaFFPXREFDTA a 
  LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
  LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
    AND d.gmyear = 2015
  WHERE a.fxcyy = 2015 
    AND a.fxconsol <> '3'
--    AND LEFT(fxgact, 1) = '1'
    AND d.gmactive = 'Y'
    AND b.page = 16
ORDER BY fxconsol, page, line, col, fxgact, d.gmdept

-- april 2015 USING sypffxmst
SELECT fxconsol, fxgact, fxfact, page, line, col, gmdesc, SUM(gttamt)
INTO #april
FROM stgArkonaGLPTRNS a
INNER JOIN (
  SELECT a.fxconsol, a.fxgact, fxfact, b.page, b.line, b.col, d.gmdesc
  FROM stgArkonaFFPXREFDTA a 
  LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
  LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
    AND d.gmyear = 2015
  WHERE a.fxcyy = 2015 
    AND a.fxconsol <> '3'
--    AND LEFT(fxgact, 1) = '1'
    AND d.gmactive = 'Y') b on a.gtacct = b.fxgact
WHERE a.gtdate BETWEEN '04/01/2015' AND '04/30/2015'    
GROUP BY fxconsol, fxgact, fxfact, page, line, col, gmdesc   
ORDER BY fxconsol, page, line, col, fxgact, fxfact, gmdesc

select *
FROM #april

select fxconsol, page, line, col, SUM(expr)
FROM #april
WHERE fxconsol = ''
GROUP BY fxconsol, page, line, col

-- hmmm, honda seems to have doubled amounts (p 4 specifically)
-- deal with that later
/*
SELECT fxgact from (
  SELECT a.fxconsol, a.fxgact, fxfact, b.page, b.line, b.col, d.gmdesc
  FROM stgArkonaFFPXREFDTA a 
  LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
  LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
    AND d.gmyear = 2015
  WHERE a.fxcyy = 2015 
    AND a.fxconsol <> '3'
--    AND LEFT(fxgact, 1) = '1'
    AND d.gmactive = 'Y'
    AND b.page = 16
) x GROUP BY fxgact HAVING COUNT(*) > 1
*/
SELECT *
FROM #april
WHERE fxconsol = '2'
  AND page = 4
ORDER BY fxconsol, page, line, col


-- UC COGS accounts (including recon)
  SELECT a.fxconsol, a.fxgact, fxfact, b.page, b.line, b.col, d.gmdesc, d.gmtype, d.gmdept
  FROM stgArkonaFFPXREFDTA a 
  LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
  LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
    AND d.gmyear = 2015
  WHERE a.fxcyy = 2015 
    AND a.fxconsol <> '3'
--    AND LEFT(fxgact, 1) = '1'
    AND d.gmactive = 'Y'
    AND b.page = 16
    AND d.gmdept = 'UC'
    AND b.line < 11
    AND d.gmtype = '5'
ORDER BY fxconsol, d.gmdept, d.gmtype, fxgact    

-- UC SALES accounts
  SELECT a.fxconsol, a.fxgact, fxfact, b.page, b.line, b.col, d.gmdesc, d.gmtype, d.gmdept
  FROM stgArkonaFFPXREFDTA a 
  LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
  LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
    AND d.gmyear = 2015
  WHERE a.fxcyy = 2015 
    AND a.fxconsol <> '3'
--    AND LEFT(fxgact, 1) = '1'
    AND d.gmactive = 'Y'
    AND b.page = 16
    AND d.gmdept = 'UC'
    AND b.line < 11
    AND d.gmtype = '4'
ORDER BY fxconsol, d.gmdept, d.gmtype, fxgact   

-- UC INVNENTORY accounts 
  SELECT a.fxconsol, a.fxgact, fxfact, b.page, b.line, b.col, d.gmdesc, d.gmtype, d.gmdept
  FROM stgArkonaFFPXREFDTA a 
  LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
  LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
    AND d.gmyear = 2015
  WHERE a.fxcyy = 2015 
    AND a.fxconsol <> '3'
    AND page = 1 AND line IN (25,26) AND col = 2
    AND d.gmactive = 'Y'
    AND d.gmdept = 'UC'
ORDER BY fxconsol, d.gmdept, d.gmtype, fxgact 

DROP TABLE gmfs_accounts;
CREATE TABLE gmfs_accounts (
  storecode cichar(3) constraint NOT NULL,
  gl_account cichar(10) constraint NOT NULL,
  gm_account cichar(10) constraint NOT NULL,
  page integer,
  line integer,
  col integer,
  description cichar(30) constraint NOT NULL,
  account_type cichar(1) constraint NOT NULL,
  department cichar(2) constraint NOT NULL,
  cogs_account cichar(10),
  constraint pk primary key (storecode,gl_account)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('gmfs_accounts','gmfs_accounts.adi','storecode','storecode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('gmfs_accounts','gmfs_accounts.adi','gl_account','gl_account','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('gmfs_accounts','gmfs_accounts.adi','gm_account','gm_account','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('gmfs_accounts','gmfs_accounts.adi','page','page','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('gmfs_accounts','gmfs_accounts.adi','line','line','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('gmfs_accounts','gmfs_accounts.adi','col','col','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('gmfs_accounts','gmfs_accounts.adi','description','description','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('gmfs_accounts','gmfs_accounts.adi','account_type','account_type','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('gmfs_accounts','gmfs_accounts.adi','department','department','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('gmfs_accounts','gmfs_accounts.adi','cogs_account','cogs_account','',2,512,'');
 
INSERT INTO gmfs_accounts 
SELECT CASE WHEN a.fxconsol = '2' THEN 'RY2' ELSE 'RY1' END,
  a.fxgact, fxfact, b.page, b.line, b.col, d.gmdesc, d.gmtype, d.gmdept, d.gmdboa
FROM stgArkonaFFPXREFDTA a 
LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
  AND d.gmyear = 2015
WHERE a.fxcyy = 2015 
  AND a.fxconsol <> '3'
  AND d.gmactive = 'Y'
  AND fxcode = 'GM'
  AND fxfact <> '331A'; 

-- page 6 used cars  
SELECT *
FROM gmfs_accounts 
WHERE page = 16
  AND department = 'uc'
  
-- page 6 used cars cogs accounts 
SELECT *
FROM gmfs_accounts 
WHERE page = 16
  AND department = 'uc'
  AND account_type = '5'
  

-- ALL page 6  
SELECT *
FROM gmfs_accounts 
WHERE page = 16  
ORDER BY department, storecode, gm_account, line, col

-- april 2015 page 6
SELECT storecode, gl_account,gm_account,page,line,col,description,account_type,
  department, SUM(gttamt) AS gttamt
FROM gmfs_accounts a
INNER JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND b.gtdate BETWEEN '04/01/2015' AND '04/30/2015'
WHERE page = 16  
GROUP BY storecode, gl_account,gm_account,page,line,col,description,account_type,
  department
ORDER BY storecode, line, col, gm_account


-- page 1 uc inventory accounts
SELECT *
FROM gmfs_accounts 
WHERE page = 1 
  AND line IN (25,26) 
  AND col = 2
ORDER BY storecode, gm_account, line, col

-- april 2015 page 1 uc inventory accounts
SELECT storecode, gl_account,gm_account,page,line,col,description,account_type,
  department, gtjrnl, gttamt
FROM gmfs_accounts a
INNER JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND b.gtdate BETWEEN '04/01/2015' AND '04/30/2015'
WHERE page = 1 
  AND line IN (25,26) 
  AND col = 2 
ORDER BY storecode, line, col, gm_account

-- april 2015 recon ro numbers FROM inventory accounts
SELECT storecode, gl_account,gm_account,page,line,col,description,account_type,
  department, gtjrnl, gttamt, gtctl#, gtdoc#
FROM gmfs_accounts a
INNER JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND b.gtdate BETWEEN '04/01/2015' AND '04/30/2015'
WHERE page = 1 
  AND line IN (25,26) 
  AND col = 2
  AND b.gtjrnl = 'SVI'
  
-- april 2015 ro list 
SELECT *
FROM stgArkonaGLPTRNS a
WHERE gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
  AND 
    CASE 
      WHEN gtacct IN (
        SELECT gl_account
        FROM gmfs_accounts 
        WHERE page = 1 
          AND line IN (25,26) 
          AND col = 2) THEN a.gtjrnl = 'svi'
      WHEN gtacct IN ( -- uc cogs accts
        SELECT gl_account
        FROM gmfs_accounts 
        WHERE page = 16
          AND department = 'uc'
          AND account_type = '5') THEN a.gtjrnl IN ('svi','sca')
    END       
 
-- ro list based on vehicles already IN ucinv_tmp_gross   
SELECT *
FROM stgArkonaGLPTRNS a
INNER JOIN scotest.ucinv_tmp_gross_2 b on a.gtctl# = b.stocknumber 
WHERE 
  CASE 
    WHEN gtacct IN (
      SELECT gl_account
      FROM gmfs_accounts 
      WHERE page = 1 
        AND line IN (25,26) 
        AND col = 2) THEN a.gtjrnl = 'svi'
    WHEN gtacct IN ( -- uc cogs accts
      SELECT gl_account
      FROM gmfs_accounts 
      WHERE page = 16
        AND department = 'uc'
        AND account_type = '5') THEN a.gtjrnl IN ('svi','sca')
  END      
    
need to break down the ros INTO dept labor AND dept parts    
    
-- april 2015 ro list use this subset to figure out how to process this shit
-- DROP TABLE #april;
-- incomplete records for vehicles due to the date restriction on glptrns
-- don't sweat it
SELECT gtjrnl, gtacct, gtctl#, gtdoc#, sum(gttamt) AS gttamt
INTO #april
FROM stgArkonaGLPTRNS a
WHERE gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
  AND 
    CASE 
      WHEN gtacct IN (i
        SELECT gl_account
        FROM gmfs_accounts 
        WHERE page = 1 
          AND line IN (25,26) 
          AND col = 2) THEN a.gtjrnl = 'svi'
      WHEN gtacct IN ( -- uc cogs accts
        SELECT gl_account
        FROM gmfs_accounts 
        WHERE page = 16
          AND department = 'uc'
          AND account_type = '5') THEN a.gtjrnl IN ('svi','sca')
    END
  AND EXISTS (
    SELECT 1
    FROM factRepairOrder
    WHERE ro = a.gtdoc#)     
GROUP BY gtjrnl, gtacct, gtctl#, gtdoc#
HAVING SUM(gttamt) <> 0 -- eliminate offsetting entries (corrections, etc) 

SELECT * FROM #april

SELECT *
FROM gmfs_accounts
WHERE page = 16

SELECT DISTINCT department
FROM gmfs_accounts 
WHERE page = 16

-- page 6 fixed sales accounts  
SELECT *
FROM gmfs_accounts
WHERE page = 16
  AND department IN ('bs','cw','pd','ql','re','sd')   
  AND account_type = '4' 
  
-- page 6 fixed cogs accounts  
SELECT *
FROM gmfs_accounts
WHERE page = 16
  AND department IN ('bs','cw','pd','ql','re','sd')   
  AND account_type = '5'   
  
-- fixed sales on april uc sales  
SELECT a.gtctl#, a.gtdoc#, b.gtacct, b.gtjrnl, b.gttamt, c.*
FROM #april a
LEFT JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtctl#
LEFT JOIN gmfs_Accounts c on b.gtacct = c.gl_account
  AND c.page = 16
  AND c.department IN ('bs','cw','ql','re','sd','pd')   
  AND c.account_type = '4' 
WHERE c.gl_account IS NOT NULL   
  
  
-- multiple departments on an ro?
-- yep there IS, the issue IS trying to categorized parts sales BY dept, eg bs, sd, ql, re, etc
-- may NOT be a problem, no parts associated with RE
SELECT gtdoc#, department
FROM (
  SELECT a.gtctl#, a.gtdoc#, b.gtacct, b.gtjrnl, b.gttamt, c.*
  FROM #april a
  LEFT JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtctl#
  LEFT JOIN gmfs_Accounts c on b.gtacct = c.gl_account
    AND c.page = 16
    AND c.department IN ('bs','cw','ql','pd','re','sd')   
    AND c.account_type = '4' 
  WHERE c.gl_account IS NOT NULL) x  
GROUP BY gtdoc#, department  

SELECT a.gtctl#, a.gtdoc#, b.gtacct, b.gtjrnl, b.gttamt, c.*
FROM #april a
LEFT JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtctl#
LEFT JOIN gmfs_Accounts c on b.gtacct = c.gl_account
  AND c.page = 16
  AND c.department IN ('bs','cw','ql','re','sd','pd')   
  AND c.account_type = '4' 
WHERE c.gl_account IS NOT NULL 
  AND a.gtdoc# IN (
    SELECT gtdoc#
    FROM (
      SELECT gtdoc#, department
      FROM (
        SELECT a.gtctl#, a.gtdoc#, b.gtacct, b.gtjrnl, b.gttamt, c.*
        FROM #april a
        LEFT JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtctl#
        LEFT JOIN gmfs_Accounts c on b.gtacct = c.gl_account
          AND c.page = 16
          AND c.department IN ('bs','cw','ql','re','sd')   
          AND c.account_type = '4' 
        WHERE c.gl_account IS NOT NULL) x  
      GROUP BY gtdoc#, department) y
    GROUP BY gtdoc# HAVING COUNT(*) > 1) 
ORDER BY a.gtctl#, a.gtdoc#    
  
  
-- fixed sales on april uc sales  
DROP TABLE #april_1;
SELECT a.gtctl#, a.gtdoc#, b.gtacct, b.gtjrnl, b.gttamt, c.*
INTO #april_1
FROM #april a
LEFT JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtctl#
LEFT JOIN gmfs_Accounts c on b.gtacct = c.gl_account
  AND c.page = 16
  AND c.department IN ('bs','cw','ql','re','sd','pd')   
  AND c.account_type = '4' 
WHERE c.gl_account IS NOT NULL     

-- bs AND sd on same ro
SELECT *
FROM #april_1
WHERE gtdoc# IN (  
SELECT gtdoc#
FROM #april_1 a
WHERE department = 'sd'
  AND EXISTS (
    SELECT 1
    FROM #april_1
    WHERE gtdoc# = a.gtdoc#
      AND department = 'bs'))

-- the different bs cogs accounts   
-- might make sense to breakout paint&mat   
SELECT gl_account, description, min(gttamt), COUNT(*) FROM #april_1 WHERE department = 'bs' GROUP BY gl_account, description

SELECT a.*, b.gtjrnl, b.gtacct, b.gtctl#, b.gtdoc#, b.gttamt 
FROM #april_1 a
LEFT JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtctl#
WHERE gl_account = '147900'
AND b.gtacct = '167900'

SELECT gtctl#, gtdoc#, gtacct, gtjrnl, cogs_account, description, department, SUM(gttamt)
FROM #april_1 
GROUP BY gtctl#, gtdoc#, gtacct, gtjrnl, cogs_account, description, department     
  
SELECT department, COUNT(*) FROM #april_1 GROUP BY department
  
SELECT gtctl#, --gtdoc#, gtacct, cogs_Account,
  SUM(CASE WHEN department = 'sd' THEN gttamt ELSE 0 END) AS sd_labor,
  SUM(CASE WHEN department = 'bs' AND gl_account <> '147900' THEN gttamt ELSE 0 END) AS bs_labor,
  SUM(CASE WHEN department = 're' THEN gttamt ELSE 0 END) AS re_labor,
  SUM(CASE WHEN department = 'cw' THEN gttamt ELSE 0 END) AS cw_labor,
  SUM(CASE WHEN department = 'ql' THEN gttamt ELSE 0 END) AS ql_labor,
  SUM(CASE WHEN department = 'bs' AND gl_account = '147900' THEN gttamt ELSE 0 END) AS bs_paint_mat,
  SUM(CASE WHEN department = 'pd' THEN gttamt ELSE 0 END) AS parts
FROM #april_1 
GROUP BY gtctl#--, gtdoc#, gtacct, cogs_Account 
 
 
SELECT * FROM #april_1  
  
DROP TABLE #april_2;
SELECT a.gtctl#, a.gtdoc#, b.gtacct, b.gtjrnl, b.gttamt, c.*
--INTO #april_2
FROM #april a
LEFT JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtctl#
LEFT JOIN gmfs_Accounts c on b.gtacct = c.gl_account
  AND c.page = 16
  AND c.department IN ('bs','cw','ql','re','sd','pd')   
  AND c.account_type = '5' 
WHERE c.gl_account IS NOT NULL   

SELECT a.gtctl#, 
  SUM(CASE WHEN department = 'sd' THEN b.gttamt ELSE 0 END) AS sd_labor_cost,
  SUM(CASE WHEN department = 'bs' AND gl_account <> '167900' THEN b.gttamt ELSE 0 END) AS bs_labor_cost,
  SUM(CASE WHEN department = 're' THEN b.gttamt ELSE 0 END) AS re_labor_cost,
  SUM(CASE WHEN department = 'cw' THEN b.gttamt ELSE 0 END) AS cw_labor_cost,
  SUM(CASE WHEN department = 'ql' THEN b.gttamt ELSE 0 END) AS ql_labor_cost,
  SUM(CASE WHEN department = 'bs' AND gl_account = '167900' THEN b.gttamt ELSE 0 END) AS bs_paint_mat_cost,
  SUM(CASE WHEN department = 'pd' THEN b.gttamt ELSE 0 END) AS parts_cost
FROM #april a
LEFT JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtctl#
LEFT JOIN gmfs_Accounts c on b.gtacct = c.gl_account
  AND c.page = 16
  AND c.department IN ('bs','cw','ql','re','sd','pd')   
  AND c.account_type = '5' 
WHERE c.gl_account IS NOT NULL  
GROUP BY a.gtctl#
  
fucking parts IS making my head hurt

SELECT *
FROM ( 
  SELECT gtctl#, --gtdoc#, gtacct, cogs_Account,
    SUM(CASE WHEN department = 'sd' THEN gttamt ELSE 0 END) AS sd_labor,
    SUM(CASE WHEN department = 'bs' AND gl_account <> '147900' THEN gttamt ELSE 0 END) AS bs_labor,
    SUM(CASE WHEN department = 're' THEN gttamt ELSE 0 END) AS re_labor,
    SUM(CASE WHEN department = 'cw' THEN gttamt ELSE 0 END) AS cw_labor,
    SUM(CASE WHEN department = 'ql' THEN gttamt ELSE 0 END) AS ql_labor,
    SUM(CASE WHEN department = 'bs' AND gl_account = '147900' THEN gttamt ELSE 0 END) AS bs_paint_mat,
    SUM(CASE WHEN department = 'pd' THEN gttamt ELSE 0 END) AS parts
  FROM #april_1 
  GROUP BY gtctl#) a
LEFT JOIN (
  SELECT a.gtctl#, 
    SUM(CASE WHEN department = 'sd' THEN b.gttamt ELSE 0 END) AS sd_labor_cost,
    SUM(CASE WHEN department = 'bs' AND gl_account <> '167900' THEN b.gttamt ELSE 0 END) AS bs_labor_cost,
    SUM(CASE WHEN department = 're' THEN b.gttamt ELSE 0 END) AS re_labor_cost,
    SUM(CASE WHEN department = 'cw' THEN b.gttamt ELSE 0 END) AS cw_labor_cost,
    SUM(CASE WHEN department = 'ql' THEN b.gttamt ELSE 0 END) AS ql_labor_cost,
    SUM(CASE WHEN department = 'bs' AND gl_account = '167900' THEN b.gttamt ELSE 0 END) AS bs_paint_mat_cost,
    SUM(CASE WHEN department = 'pd' THEN b.gttamt ELSE 0 END) AS parts_cost
  FROM #april a
  LEFT JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtctl#
  LEFT JOIN gmfs_Accounts c on b.gtacct = c.gl_account
    AND c.page = 16
    AND c.department IN ('bs','cw','ql','re','sd','pd')   
    AND c.account_type = '5' 
  WHERE c.gl_account IS NOT NULL  
  GROUP BY a.gtctl#) b on a.gtctl# = b.gtctl#  
 
24690A paint/mat sales: 349, no cost  
SELECT * FROM #april_1 WHERE gtctl# = '24690A'
SELECT * FROM #april WHERE gtctl# = '24690A'

  SELECT a.gtctl#, 
    SUM(CASE WHEN department = 'sd' THEN b.gttamt ELSE 0 END) AS sd_labor_cost,
    SUM(CASE WHEN department = 'bs' AND gl_account <> '167900' THEN b.gttamt ELSE 0 END) AS bs_labor_cost,
    SUM(CASE WHEN department = 're' THEN b.gttamt ELSE 0 END) AS re_labor_cost,
    SUM(CASE WHEN department = 'cw' THEN b.gttamt ELSE 0 END) AS cw_labor_cost,
    SUM(CASE WHEN department = 'ql' THEN b.gttamt ELSE 0 END) AS ql_labor_cost,
    SUM(CASE WHEN department = 'bs' AND gl_account = '167900' THEN b.gttamt ELSE 0 END) AS bs_paint_mat_cost,
    SUM(CASE WHEN department = 'pd' THEN b.gttamt ELSE 0 END) AS parts_cost
-- SELECT a.gtctl#, a.gtdoc#, b.gtacct, b.gtjrnl, b.gttamt, c.*    
  FROM #april a
  LEFT JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtctl#
  LEFT JOIN gmfs_Accounts c on b.gtacct = c.gl_account
    AND c.page = 16
    AND c.department IN ('bs','cw','ql','re','sd','pd')   
    AND c.account_type = '5' 
  WHERE a.gtctl# = '24690a'
  GROUP BY a.gtctl#
  
  
  
  
    
    
SELECT *
INTO #april
FROM stgArkonaGLPTRNS a
WHERE gtdate BETWEEN '04/01/2015' AND '04/30/2015'      

SELECT gtjrnl, COUNT(*)
FROM #april
WHERE gtacct IN ('164700','164701','165100','165101')
GROUP BY gtjrnl

SELECT *
FROM #april
WHERE gtacct IN ('164700','164701','165100','165101')
  AND gtjrnl = 'pot'

SELECT gtjrnl, gtacct, gtctl#, gtdoc#, gttamt
FROM #april a
WHERE gtacct IN ('164700','164701','165100','165101')
  AND EXISTS (
    SELECT 1
    FROM factRepairOrder
    WHERE ro = a.gtdoc#)
ORDER BY gtacct


SELECT gtacct, gtjrnl, COUNT(*)
FROM #april
WHERE gtacct IN ( -- uc cogs accts
  SELECT gl_account
  FROM gmfs_accounts 
  WHERE page = 16
    AND department = 'uc'
    AND account_type = '5')
GROUP BY gtacct, gtjrnl   