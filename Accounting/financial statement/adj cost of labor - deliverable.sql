/*
-- day of month, working days/month, elapsed working days/month  
-- fuck need to change the grain to day, NOT month, IF i want elapsed working days
-- which i believe i need for pacing
-- mind fuck tailspin, what does working days mean, DO we pace on mon - fri,
-- OR pace on calendar days, fuck i am leaning towards calendar days 
-- still thinking i want this TABLE to be at day grain
-- nope, this IS just accrual date info, leave it at that for now
-- DROP TABLE acl_accrual_dates;
CREATE TABLE acl_accrual_dates (
  year_month integer CONSTRAINT NOT NULL,
  last_day_of_month date CONSTRAINT NOT NULL,
  first_payday date CONSTRAINT NOT NULL,
  last_payday date CONSTRAINT NOT NULL,
  check_date date CONSTRAINT NOT NULL,
  accrual_days integer CONSTRAINT NOT NULL,
  accrual_days_14 numeric(8,2) CONSTRAINT NOT NULL,
  constraint pk primary key (year_month)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('acl_accrual_dates','acl_accrual_dates.adi','first_payday','first_payday','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('acl_accrual_dates','acl_accrual_dates.adi','last_payday','last_payday','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('acl_accrual_dates','acl_accrual_dates.adi','check_date','check_date','',2,512,'');
INSERT INTO acl_accrual_dates
SELECT d.*, 
  CASE
    WHEN year(check_date) * 100 + month(check_date) = d.yearmonth THEN 
      timestampdiff(sql_tsi_day, last_payday, eom)
    ELSE timestampdiff(sql_tsi_day, last_payday, eom) + 14
  END AS accrual_days,
  CASE
    WHEN year(check_date) * 100 + month(check_date) = d.yearmonth THEN   
      (timestampdiff(sql_tsi_day, last_payday, eom))/14.0
    ELSE
      (timestampdiff(sql_tsi_day, last_payday, eom) + 14)/14.0
  END AS  accrual_days_14
FROM (  
  SELECT c.*, 
    (
      SELECT MIN(thedate) 
      FROM dds.day 
      WHERE dayofweek = 6 AND thedate > c.last_payday) AS check_date  
  FROM (          
    SELECT a.yearmonth, max(b.last) eom ,
    min(a.biweeklyPayPeriodEndDate) AS first_payday, 
    max(a.biweeklyPayPeriodEndDate) AS last_payday 
    FROM dds.day a
    inner JOIN (
      SELECT yearmonth, MAX(thedate) AS last, MIN(thedate) AS first
      FROM dds.day
      WHERE thedate between '01/01/2013' AND '08/31/2020'
      GROUP BY yearmonth) b on a.biweeklyPayPeriodEndDate BETWEEN b.first AND b.last
        AND a.yearmonth = b.yearmonth
    GROUP BY a.yearmonth) c) d;
*/


-- one row for each day/each acl employee
-- fucking dimtech, kristen swanson AND tyler magnuson, multiple rows
-- the issue IS that each of these folks have been assigned multiple tech 
-- numbers, AND the most recent row for each of those tech numbers have
-- currentrow = true, issue IS the grain of dimtech, IS it a person
-- OR a technumber, thinking it should be a person, so that any person
/*
SELECT name, currentrow
FROM dimtech
WHERE currentrow = true
GROUP BY name,currentrow
HAVING COUNT(*) > 1
*/


9-9 fixed techs IN "dimTech still fucked up - ryan lene.sql", my stupid notion
that an inactive tech gets a new row IN dimtecch which IS "active"
results IN multiple rows per employeenumber/day

-- 9/20 decided to start this with july 2015
-- 9/21 ADD techkey, date_key
-- 9/22 need to addpto rate, UPDATE hourly rate for flat rate, team, team prof? 
-- 9/30 extended to 6/1/15
-- 10/2 changed dept FROM body_shop to body shop, matches acl_v1
DROP TABLE acl_census;
CREATE TABLE acl_census (
  the_date date constraint NOT NULL,
  date_key integer constraint NOT NULL,
  year_month integer constraint NOT NULL,
  store_code cichar(3) constraint NOT NULL,
  dist_code cichar(4) constraint NOT NULL,
  department cichar(12) constraint NOT NULL,
  payroll_class cichar(12) constraint NOT NULL,
  category cichar(12) constraint NOT NULL,
  first_name cichar(25) constraint NOT NULL,
  last_name cichar(25) constraint NOT NULL,
  employee_number cichar(7) constraint NOT NULL,
  employee_key integer constraint NOT NULL,
  hourly_rate numeric(12,2) default '0' constraint NOT NULL,
  salary numeric(12,2) default '0' constraint NOT NULL,
  tech_number cichar(3) default 'N/A' constraint NOT NULL,
  tech_key integer default '0' constraint NOT NULL,
  labor_cost numeric(12,2) default '0' constraint NOT NULL,
  constraint pk primary key(the_date, employee_number)) IN database;
INSERT INTO acl_census  
SELECT a.thedate, a.datekey, a.yearmonth,
  b.storecode, b.distcode, 
  CASE 
    WHEN distcode IN ('stec','tech') THEN 'Main Shop'
    WHEN distcode = 'btec' THEN 'Body Shop'
    WHEN distcode = 'wtec' THEN 'Detail'
    WHEN distcode IN ('ptec','tpdq') THEN 'PDQ'
  END AS department,
  b.payrollclass,
  CASE 
    WHEN payrollclass = 'salary' THEN 'overhead'
    WHEN payrollclass = 'commission' THEN 'teampay'
    ELSE 'hourly'
  END AS category,
  b.firstname, b.lastname, b.employeenumber, b.employeekey,
  b.hourlyrate, b.salary,
  coalesce(d.technumber, 'N/A'), coalesce(d.techkey, 0), coalesce(d.laborcost, 0)
-- SELECT thedate, b.employeenumber 
FROM dds.day a
LEFT JOIN dds.edwEmployeeDim b on a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  AND b.termdate >= a.thedate
  AND b.distcode IN ('stec','btec','ptec','wtec','tech','tpdq')  
LEFT JOIN dds.dimtech d on b.employeenumber = d.employeenumber
  AND a.thedate BETWEEN d.techkeyfromdate AND d.techkeythrudate  
  AND b.storecode = d.storecode
WHERE a.thedate BETWEEN '06/01/2015' AND curdate() - 1;

execute procedure sp_createindex90('acl_census','acl_census.adi','deprtment','department','',2,512,'');
execute procedure sp_createindex90('acl_census','acl_census.adi','payroll_class','payroll_class','',2,512,'');
execute procedure sp_createindex90('acl_census','acl_census.adi','date_key','date_key','',2,512,'');
execute procedure sp_createindex90('acl_census','acl_census.adi','tech_key','tech_key','',2,512,'');
execute procedure sp_createindex90('acl_census','acl_census.adi','employee_number','employee_number','',2,512,'');
execute procedure sp_createindex90('acl_census','acl_census.adi','year_month','year_month','',2,512,'');
execute procedure sp_createindex90('acl_census','acl_census.adi','store_code','store_code','',2,512,'');

/* done 9/20/15
-- those emp with multiple categories - use it to clean up the bad categories
-- FROM payroll latency
-- 9/19 fuck, just start with june, better yet start with july
-- which gives me a couple of bs guys to fix
SELECT * 
FROM acl_census
WHERE the_date IN ('07/01/2015', '09/15/2015')
AND employee_number IN (
  SELECT employee_number 
  FROM (
    SELECT employee_number, category 
    FROM (
      SELECT * 
      FROM acl_census
      WHERE the_date IN ('07/01/2015', '09/15/2015')) x GROUP BY employee_number, category) y GROUP BY employee_number HAVING COUNT(*) > 1)
ORDER BY store_code, department, last_name
 
SELECT * FROM acl_census WHERE employee_number IN ('170150','174130')  

UPDATE acl_census
SET payroll_class = 'Commission',
    category = 'teampay',
    hourly_rate = 0,
    salary = 0
WHERE employee_number IN ('170150','174130')   
  AND payroll_class = 'hourly';
*/  


-- 9/11
wrasslin with DO i use pyhshdta OR glptrns for pay pacing

on 9/10 amt paid will be an est of pay for 9/1 -> 9/10
but on 9/11, amt paid will be actual for 9/1 -> 9/5 AND est for 9/6 -> 9/11
each day need to determine IF a check has been issued (AND for what period)

  
so how DO i match up the above with pyptbdta, IS check date good enuf
should i fucking decode the batch FROM the gtdesc



ok, also need to know the per day, AND how many days applicable to current month  
-- batch payroll_end_date won't WORK for period (per jeri, gets changed to 
-- accomodate 3 payrolls IN a month) eg, check date 7/31


-- 9/15 fuck, same person can get multiple checks on the same day FROM multiple 
-- batches; eg; 13725 2/13/15
-- have to fucking decode the batch number FROM the glptrns.gtdesc

-- run it with glptrns.gttamt, compare to check gross amount
-- nope, too many mismatches, go with check gross amount

-- only 2 of these goofy anomalies, kim paying bonuses OR some shit
SELECT *
FROM acl_paychecks
WHERE stats NOT IN (1,2)

SELECT * 
FROM acl_accrual_dates

possible combinations: stats
1  pr_start = bw_start AND pr_end = bw_end
2  pr_start = bw_start AND pr_end <> bw_end
3  pr_start <> bw_start AND pr_end = bw_end
4  pr_start <> bw_start AND pr_end <> bw_end

DROP TABLE acl_paychecks;
CREATE TABLE acl_paychecks (
  check_date date constraint NOT NULL,
  year_month integer constraint NOT NULL,
  check_number cichar(9) constraint NOT NULL,
  employee_number cichar(9) constraint NOT NULL,
  gross numeric(12,2) constraint NOT NULL,
  payroll_run_number integer constraint NOT NULL,
  payroll_start_date date constraint NOT NULL,
  payroll_end_date date constraint NOT NULL,
  payroll_days integer constraint NOT NULL,
  pay_period_start_date date constraint NOT NULL,
  pay_period_end_date date constraint NOT NULL,
  stats integer constraint NOT NULL, 
  constraint pk primary key(check_date,employee_number,payroll_run_number)) IN database;
DELETE FROM acl_paychecks;  
INSERT INTO acl_paychecks
SELECT h.*,
  CASE
    WHEN payroll_start_date = biweeklypayperiodstartdate AND payroll_end_date = biweeklypayperiodenddate THEN 1
    WHEN payroll_start_date = biweeklypayperiodstartdate AND payroll_end_date <> biweeklypayperiodenddate THEN 2
    WHEN payroll_start_date <> biweeklypayperiodstartdate AND payroll_end_date = biweeklypayperiodenddate THEN 3
    WHEN payroll_start_date <> biweeklypayperiodstartdate AND payroll_end_date <> biweeklypayperiodenddate THEN 4
  END AS stats
FROM (    
  SELECT gtdate, yearmonth, gtdoc#, gtctl#, yhdtgp, payroll_run_number,
    payroll_start_date, payroll_end_date, 
    (payroll_end_date - payroll_start_date) + 1 AS payroll_days,
    h.biweeklypayperiodstartdate, h.biweeklypayperiodenddate 
  FROM (
    SELECT a.*, b.yhdtgp, c.payroll_run_number, 
      CAST(
        substring(CAST(payroll_start_date AS sql_char), 5, 2) + '/' +
        substring(CAST(payroll_start_date AS sql_char), 7, 2) + '/' +
        LEFT(CAST(payroll_start_date AS sql_char), 4) AS sql_date) AS payroll_start_date,
      CAST(
        substring(CAST(payroll_end_date AS sql_char), 5, 2) + '/' +
        substring(CAST(payroll_end_date AS sql_char), 7, 2) + '/' +
        LEFT(CAST(payroll_end_date AS sql_char), 4) AS sql_date) AS payroll_end_date     
    -- SELECT COUNT(*)
    FROM ( -- check batch
      SELECT a.gtdate, a.gtdoc#, a.gtdesc, a.gtctl#, 
        cast(substring(gtdesc, position('-' IN gtdesc) + 1, 7) AS sql_integer) AS payroll_batch,
        SUM(gttamt) AS gttamt
      FROM dds.stgArkonaGLPTRNS a 
      INNER JOIN dds.gmfs_accounts b on a.gtacct = b.gl_account
      WHERE b.gm_account = '247'
        AND a.gtpost = 'Y'
        AND a.gtjrnl = 'pay'
        AND a.gtdate BETWEEN '09/12/2015' AND curdate() - 1
        AND EXISTS (
          SELECT 1
          FROM acl_census
          WHERE employee_number = a.gtctl#)
        GROUP BY a.gtdate, a.gtdoc#, a.gtdesc, a.gtctl#, 
        cast(substring(gtdesc, position('-' IN gtdesc) + 1, 7) AS sql_integer)) a
    INNER JOIN dds.stgArkonaPYHSHDTA b on a.payroll_batch = b.ypbnum -- check amount
      AND b.yhdseq = '00' -- exclude voids
      AND a.gtctl# = b.yhdemp
    INNER JOIN dds.ext_arkona_pyptbdta c on b.ypbnum = c.payroll_run_number -- check period
      AND b.yhdco# = c.company_number) g 
  LEFT JOIN dds.day h on g.payroll_start_date = h.thedate) h;  
  
SELECT * FROM acl_paychecks ORDER BY check_date DESC 

select * 
FROM acl_census a
LEFT JOIN acl_paychecks b on a.the_date = b.check_date
  AND a.employee_number = b.employee_number 
WHERE a.year_month = 201508
  AND b.check_date IS NOT NULL 

select * FROM acl_clock_hours WHERE employee_number = '1113940' ORDER BY the_date

DROP TABLE acl_clock_hours;
CREATE TABLE acl_clock_hours ( 
  year_month integer constraint NOT NULL,
  the_date Date constraint NOT NULL,
  employee_number CIChar( 9 ) constraint NOT NULL,
  regular_hours_day Numeric( 8 ,2 ) constraint NOT NULL,
  regular_hours_mtd Numeric( 8 ,2 ) constraint NOT NULL,
  overtime_hours_day Numeric( 8 ,2 ) constraint NOT NULL,
  overtime_hours_mtd Numeric( 8 ,2 ) constraint NOT NULL,
  pto_hours_day Numeric( 8 ,2 ) constraint NOT NULL,
  pto_hours_mtd Numeric( 8 ,2 ) constraint NOT NULL,
  constraint pk primary key (the_date,employee_number)) IN DATABASE;
INSERT INTO acl_clock_hours
SELECT b.yearmonth, a.the_date, a.employee_number,
  SUM(
    CASE a.payroll_class
      WHEN 'Salary' THEN 0
      WHEN 'Hourly' THEN coalesce(c.RegularHours, 0)
      WHEN 'Commission' THEN c.ClockHours
    END) AS ClockHours, 0,
  SUM(
    CASE a.payroll_class
      WHEN 'Salary' THEN 0
      WHEN 'Hourly' THEN coalesce(c.OverTimeHours, 0)
      WHEN 'Commission' THEN 0
    END) AS OvertimeHours, 0, 
  SUM(coalesce(c.vacationhours,0) + coalesce(c.ptohours,0) + coalesce(c.holidayhours,0)) AS ptoHours, 0  
FROM acl_census a
INNER JOIN dds.day b on a.the_date = b.thedate
INNER JOIN dds.edwClockHoursFact c on a.employee_key = c.employeekey
  AND b.datekey = c.datekey
GROUP BY b.yearmonth, a.the_date, a.employee_number;

UPDATE acl_clock_hours
SET regular_hours_mtd = x.mtd
FROM (
  SELECT a.the_date, a.employee_number, a.year_month, a.regular_hours_day, SUM(b.regular_hours_day) AS mtd
  FROM acl_clock_hours a, acl_clock_hours b
  WHERE a.year_month = b.year_month
    AND a.employee_number = b.employee_number
    AND b.the_date <= a.the_date
  GROUP BY a.the_date, a.employee_number, a.year_month, a.regular_hours_day) x
WHERE acl_clock_hours.the_date = x.the_date
  AND acl_clock_hours.employee_number = x.employee_number;
  
UPDATE acl_clock_hours
SET overtime_hours_mtd = x.mtd
FROM (
  SELECT a.the_date, a.employee_number, a.year_month, a.overtime_hours_day, SUM(b.overtime_hours_day) AS mtd
  FROM acl_clock_hours a, acl_clock_hours b
  WHERE a.year_month = b.year_month
    AND a.employee_number = b.employee_number
    AND b.the_date <= a.the_date
  GROUP BY a.the_date, a.employee_number, a.year_month, a.overtime_hours_day) x
WHERE acl_clock_hours.the_date = x.the_date
  AND acl_clock_hours.employee_number = x.employee_number;  
  
UPDATE acl_clock_hours
SET pto_hours_mtd = x.mtd
FROM (
  SELECT a.the_date, a.employee_number, a.year_month, a.pto_hours_day, SUM(b.pto_hours_day) AS mtd
  FROM acl_clock_hours a, acl_clock_hours b
  WHERE a.year_month = b.year_month
    AND a.employee_number = b.employee_number
    AND b.the_date <= a.the_date
  GROUP BY a.the_date, a.employee_number, a.year_month, a.pto_hours_day) x
WHERE acl_clock_hours.the_date = x.the_date
  AND acl_clock_hours.employee_number = x.employee_number; 

  
  
SELECT gtdate, gtacct, gtjrnl, gtctl#, 
  SUM(CASE WHEN gtjrnl = 'gli' AND gttamt > 0 THEN gttamt END) AS accrual,
  SUM(CASE WHEN gtjrnl = 'gli' AND gttamt < 0 THEN gttamt END) AS accrual_rev
FROM dds.stgArkonaGLPTRNS a
INNER JOIN dds.gmfs_accounts b on a.gtacct = b.gl_account
WHERE a.gtdate BETWEEN '01/01/2015' AND curdate()
  AND gtjrnl = 'gli'
  AND b.gm_account = '247' 
GROUP BY gtdate, gtacct, gtjrnl, gtctl#  

SELECT gtdate, gtctl#, gttamt
FROM dds.stgArkonaGLPTRNS a
INNER JOIN dds.gmfs_accounts b on a.gtacct = b.gl_account
WHERE a.gtdate BETWEEN '01/01/2015' AND curdate()
  AND gtjrnl = 'gli'
  AND b.gm_account = '247' 
  AND a.gttamt > 0
GROUP BY gtdate, gtctl#, gttamt
HAVING COUNT(*) > 1


SELECT gtdate, gtctl#, gttamt
FROM dds.stgArkonaGLPTRNS a
INNER JOIN dds.gmfs_accounts b on a.gtacct = b.gl_account
WHERE a.gtdate BETWEEN '01/01/2015' AND curdate()
  AND gtjrnl = 'gli'
  AND b.gm_account = '247' 
  AND a.gttamt < 0
GROUP BY gtdate, gtctl#, gttamt
HAVING COUNT(*) > 1


SELECT gtdate, gtctl#, gttamt
FROM dds.stgArkonaGLPTRNS a
INNER JOIN dds.gmfs_accounts b on a.gtacct = b.gl_account
WHERE a.gtdate BETWEEN '01/01/2015' AND curdate()
  AND gtjrnl = 'gli'
  AND b.gm_account = '247' 
  AND a.gttamt < 0
GROUP BY gtdate, gtctl#, gttamt

SELECT c.*, d.yearmonth AS accrual_month, e.yearmonth AS reversal_month
FROM (
  SELECT gtdate, gtctl#, 
    coalesce(SUM(CASE WHEN gttamt > 0 THEN gttamt END), 0) AS accrual,
    coalesce(SUM(CASE WHEN gttamt < 0 THEN gttamt END), 0) AS accrual_reversal
  FROM dds.stgArkonaGLPTRNS a
  INNER JOIN dds.gmfs_accounts b on a.gtacct = b.gl_account
  WHERE a.gtdate BETWEEN '01/01/2015' AND curdate()
    AND gtjrnl = 'gli'
    AND b.gm_account = '247' 
    AND EXISTS (
      SELECT 1
      FROM acl_census
      WHERE employee_number = a.gtctl#)    
  GROUP BY gtdate, gtctl#) c 
LEFT JOIN dds.day d on c.gtdate = d.thedate
LEFT JOIN dds.day e on cast(timestampadd(sql_tsi_month, -1, c.gtdate) AS sql_date) = e.thedate 

DROP TABLE acl_accruals;
CREATE TABLE acl_accruals (
  the_date date constraint NOT NULL,
  employee_number cichar(9) constraint NOT NULL,
  accrual numeric(12,2) constraint NOT NULL,
  reversal numeric(12,2) constraint NOT NULL,
  accrual_month integer constraint NOT NULL,
  reversal_month integer constraint NOT NULL,
  constraint pk primary key(the_date, employee_number)) IN database;
INSERT INTO acl_accruals
SELECT c.*, d.yearmonth AS accrual_month, e.yearmonth AS reversal_month
FROM (
  SELECT gtdate, gtctl#, 
    coalesce(SUM(CASE WHEN gttamt > 0 THEN gttamt END), 0) AS accrual,
    coalesce(SUM(CASE WHEN gttamt < 0 THEN gttamt END), 0) AS accrual_reversal
  FROM dds.stgArkonaGLPTRNS a
  INNER JOIN dds.gmfs_accounts b on a.gtacct = b.gl_account
  WHERE a.gtdate BETWEEN '01/01/2015' AND curdate()
    AND gtjrnl = 'gli'
    AND b.gm_account = '247' 
    AND EXISTS (
      SELECT 1
      FROM acl_census
      WHERE employee_number = a.gtctl#)    
  GROUP BY gtdate, gtctl#) c 
LEFT JOIN dds.day d on c.gtdate = d.thedate
LEFT JOIN dds.day e on cast(timestampadd(sql_tsi_month, -1, c.gtdate) AS sql_date) = e.thedate;
  

SELECT payroll_days, COUNT(*)
FROM acl_paychecks a
GROUP BY payroll_days

SELECT *
FROM acl_census a
LEFT JOIN (
  SELECT employee_number, COUNT(*) paychecks, avg(gross) AS avg_gros, 
    round(avg(gross)/14, 2) AS avg_per_day
  FROM acl_paychecks a
  WHERE payroll_days BETWEEN 14 AND 19
  GROUP BY employee_number) b on a.employee_number = b.employee_number
WHERE a.the_date = '08/01/2015'
ORDER BY store_code, department, category, last_name

SELECT employee_number, COUNT(*), avg(gross)
FROM acl_paychecks a
WHERE payroll_days BETWEEN 14 AND 19
GROUP BY employee_number

for team pay: avg team proficiency, current 


SELECT employeenumber, lastname, firstname, round(techhourlyrate, 2) AS techhourlyrate,
  round(techtfrrate,2) AS techtfrrate, teamname
FROM tpdata
WHERE thedate = curdate()

SELECT teamname, teamkey, count(*) as paychecks, round(avg(teamprofpptd), 2) AS avg_prof
FROM tpdata
WHERE thedate = payperiodend
  AND thedate > '01/10/2015'
GROUP BY teamname, teamkey

SELECT a.*, round(b.techhourlyrate, 2) AS techhourlyrate,
  round(b.techtfrrate,2) AS techtfrrate, b.teamname, b.teamkey, 
  c.paychecks, c.avg_prof, min_prof, max_prof
FROM acl_census a
LEFT JOIN tpdata b on a.the_date = b.thedate
  AND a.employee_number = b.employeenumber
LEFT JOIN (
  SELECT teamname, teamkey, count(*) as paychecks, round(avg(teamprofpptd), 2) AS avg_prof,
    MIN(teamprofpptd) AS min_prof, MAX(teamprofpptd) AS max_prof
  FROM tpdata
  WHERE thedate = payperiodend
    AND thedate > '01/10/2015'
  GROUP BY teamname, teamkey) c on b.teamkey = c.teamkey  
WHERE b.thedate IS NOT NULL   

SELECT teamname, teamkey, teamprofpptd
FROM tpdata
WHERE thedate = payperiodend
  AND thedate > '0/10/2015'
GROUP BY teamname, teamkey, teamprofpptd
-- !!!!!!!!!!!!!!! payroll did NOT have team pay techs categorized AS commission for a long time !!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!! tpdata does NOT have consistent flaghours before 1/10/15 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SELECT a.last_name, a.first_name, a.payroll_class, a.category, MIN(the_date), MAX(the_date)
FROM acl_census a
INNER JOIN tpTechs b on a.employee_number = b.employeenumber
INNER JOIN tpteamtechs c on b.techkey = c.techkey
  AND a.the_date BETWEEN c.fromdate AND c.thrudate
GROUP BY a.last_name, a.first_name, a.payroll_class, a.category  

-- the date interval during which a tech was on team pay, ie on a team
SELECT a.last_name, a.first_name, tech_number, c.fromdate, c.thrudate
FROM acl_census a
INNER JOIN tpTechs b on a.employee_number = b.employeenumber
INNER JOIN tpteamtechs c on b.techkey = c.techkey
WHERE c.thrudate > '12/31/2014'
GROUP BY a.last_name, a.first_name, a.tech_number, c.fromdate, c.thrudate

SELECT a.*, c.fromdate, c.thrudate
FROM acl_census a
LEFT JOIN tptechs b on a.employee_number = b.employeenumber
LEFT JOIN tpteamtechs c on b.techkey = c.techkey
  AND a.the_date BETWEEN c.fromdate AND c.thrudate
WHERE the_date > '04/30/2015'
  AND c.fromdate IS NOT null
ORDER BY a.last_name

-- ok, DO NOT want to get bogged down IN a shit load of history

SELECT *
FROM acl_Census
WHERE employee_number = '116185'


-- 9/18
what DO i need
DO avg_per_day for everybody AS a basis of comparison
hourly: clock hours * hourly rate
salary: average per day
main shop & body shop teampay: current rate * clockhours * avg team prof
detail commission: ? avg proficiency monthly ?

a mtd earnings TABLE that includes an employee identifier, avg_per_day, as
SELECT *
FROM tpdata WHERE thedate = '05/07/2015'
WHERE thedate > curdate() - 180
  AND thedate = payperiodend
  
Detail emp were reclassified AS commission on 5/7/15 
SELECT *
FROM acl_census
WHERE department = 'detail'
  AND last_name = 'sauls'
main shop on 1/31/15 
SELECT *
FROM acl_census
WHERE last_name = 'bohm'  
body shop on 2/12/15
SELECT *
FROM acl_census
WHERE last_name = 'mavity'

SELECT *
FROM acl_census
WHERE the_date = curdate() -5
ORDER BY store_code, department, category

SELECT *
FROM acl_paychecks

FUCK too many notes again
** straighten out the fucking acl_census category

-- !!!!!!!!!!!!!!! need hourly rate (pto) for team pay
1. avg_per_day FROM paychecksi

this needs to be updated daily, with, shit i DO NOT know, last 6 mos paychecks
SELECT *
FROM acl_census a
LEFT JOIN (
  SELECT employee_number, COUNT(*) paychecks, avg(gross) AS avg_gros, 
    round(avg(gross)/14, 2) AS avg_per_day
  FROM acl_paychecks a
  WHERE payroll_days BETWEEN 14 AND 19
  GROUP BY employee_number) b on a.employee_number = b.employee_number
WHERE a.the_date = '08/01/2015'
ORDER BY store_code, department, category, last_name

so, DO i need a avg_per_day for each day for each employee, think so
-- should this be check_Date OR pay_period_end???  big difference
-- executive decision: use pay_period_end_date
-- the numbers are comparable, USING check_date lags 
SELECT employee_number, avg_gross, avg_per_day, MIN(the_date) AS from_date,
  MAX(the_date) AS thru_date
FROM (
  SELECT a.employee_number, a.the_date, COUNT(*) AS paychecks, 
    avg(gross) AS avg_gross, round(avg(gross)/14, 2) AS avg_per_day
  FROM acl_Census a
  LEFT JOIN acl_paychecks b on a.employee_number = b.employee_number
--    AND b.check_date BETWEEN a.the_date - 180 AND a.the_date
    AND b.pay_period_end_date BETWEEN a.the_date - 180 AND a.the_date
  WHERE b.payroll_days BETWEEN 14 AND 19  
  GROUP BY a.employee_number, a.the_date) c
GROUP BY employee_number, avg_gross, avg_per_day

SELECT a.*, e.avg_paycheck_gross_per_day
FROM acl_census a
LEFT JOIN (
  SELECT employee_number, avg_gross, avg_paycheck_gross_per_day, MIN(the_date) AS from_date,
    MAX(the_date) AS thru_date
  FROM (
    SELECT a.employee_number, a.the_date, COUNT(*) AS paychecks, 
      avg(gross) AS avg_gross, round(avg(gross)/14, 2) AS avg_paycheck_gross_per_day
    FROM acl_Census a
    LEFT JOIN acl_paychecks b on a.employee_number = b.employee_number
  --    AND b.check_date BETWEEN a.the_date - 180 AND a.the_date
      AND b.pay_period_end_date BETWEEN a.the_date - 180 AND a.the_date
    WHERE b.payroll_days BETWEEN 14 AND 19  
    GROUP BY a.employee_number, a.the_date) c
  GROUP BY employee_number, avg_gross, avg_paycheck_gross_per_day) e on a.employee_number = e.employee_number
    AND a.the_date BETWEEN e.from_date AND e.thru_date
    
/* this convinced me to go with pay_period_End_date
SELECT *
FROM (  
  SELECT a.employee_number, a.the_date, COUNT(*) AS paychecks, 
    avg(gross) AS avg_gross, round(avg(gross)/14, 2) AS avg_per_day
  FROM acl_Census a
  LEFT JOIN acl_paychecks b on a.employee_number = b.employee_number
    AND b.check_date BETWEEN a.the_date - 180 AND a.the_date
--    AND b.pay_period_end_date BETWEEN a.the_date - 180 AND a.the_date
  WHERE b.payroll_days BETWEEN 14 AND 19  
    AND a.employee_number = '186210'
  GROUP BY a.employee_number, a.the_date) x  
LEFT JOIN (  
  SELECT a.employee_number, a.the_date, COUNT(*) AS paychecks, 
    avg(gross) AS avg_gross, round(avg(gross)/14, 2) AS avg_per_day
  FROM acl_Census a
  LEFT JOIN acl_paychecks b on a.employee_number = b.employee_number
--    AND b.check_date BETWEEN a.the_date - 180 AND a.the_date
    AND b.pay_period_end_date BETWEEN a.the_date - 180 AND a.the_date
  WHERE b.payroll_days BETWEEN 14 AND 19  
    AND a.employee_number = '186210'
  GROUP BY a.employee_number, a.the_date) y on x.employee_number = y.employee_number
    and x.the_Date = y.the_Date    
*/    

-- est_hourly_mtd
SELECT a.the_date, a.store_code, a.employee_number, a.year_month, a.category, 
  a.payroll_class, a.hourly_rate,
  b.regular_hours_mtd,  overtime_hours_mtd, pto_hours_mtd,
  round(b.regular_hours_mtd * hourly_rate, 2)
  + round(b.overtime_hours_mtd * hourly_rate * 1.5, 2)
  + round(b.pto_hours_mtd * hourly_rate, 2) AS est_hourly_mtd
FROM acl_census a
LEFT JOIN acl_clock_hours b on a.employee_number = b.employee_number
  AND a.the_date = b.the_date
WHERE a.payroll_class = 'hourly'  
-- set_salary_mtd
SELECT a.the_date, a.store_code, a.employee_number, a.year_month, a.category, 
  a.payroll_class, a.salary, a.salary/14 AS salary_per_day,
  round(dayofmonth(the_date) * a.salary/14, 2) AS est_salary_mtd
FROM acl_census a
WHERE a.payroll_class = 'salary'  

-- detail
-- need avg profi for complete pay periodsi
SELECT a.the_date, a.year_month, a.first_name, a.last_name, a.employee_number,
  a.tech_number, b.biweeklypayperiodstartdate, b.biweeklypayperiodenddate
FROM acl_census a
INNER JOIN dds.day b on a.the_date = b.thedate
WHERE a.department = 'detail'
  AND a.payroll_class = 'commission'

SELECT * 
FROM acl_clock_hours  

hmmm maybe need acl flaghours, back to jan 1?
but remember, core issue: detail tech historical avg proficiency

-- 9/24 for at least detail, need flag hours mtd
-- for mtd to WORK, need a row for each day
DROP TABLE acl_flag_hours;
CREATE TABLE acl_flag_hours (
  the_date date constraint NOT NULL,
  date_key integer constraint NOT NULL,
  year_month integer constraint NOT NULL,
  tech_key integer constraint NOT NULL,
  tech_number cichar(3) constraint NOT NULL,
  flag_hours_day numeric(8,2) constraint NOT NULL,
  flag_hours_mtd numeric(8,2) default '0' constraint NOT NULL,
  constraint pk primary key (date_key,tech_key)) IN database;
INSERT INTO acl_flag_hours  
SELECT a.thedate, a.datekey, a.yearmonth, b.techkey, c.technumber, 
--  round(SUM(b.flaghours), 2) AS flag_hours, 0
  coalesce(round(b.flaghours, 2), 0)
FROM dds.day a
LEFT JOIN dds.factRepairOrder b on a.datekey = b.closedatekey
LEFT JOIN dds.dimtech c on b.techkey = c.techkey
WHERE a.theyear = 2015
GROUP BY a.thedate, a.datekey, yearmonth, b.techkey, c.technumber
HAVING SUM(b.flaghours) <> 0;

SELECT b.thedate, b.datekey, b.yearmonth, a.techkey, c.technumber, coalesce(a.flaghours, 0)
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.closedatekey = b.datekey
INNER JOIN dds.dimtech c on a.techkey = c.techkey
WHERE b.theyear = 2015

-- 1 row for each day/tech
SELECT *
FROM (
  SELECT a.yearmonth, a.thedate, a.datekey
  FROM dds.day a
  WHERE a.theyear = 2015) a,
(
  SELECT DISTINCT employee_number, tech_number
  FROM acl_census
  WHERE tech_key <> 0) b

DROP TABLE #wtf;  
SELECT c.*, e.name, d.flaghours
INTO #wtf
FROM (-- c: 1 row for each day/tech
  SELECT *
  FROM (
    SELECT a.yearmonth, a.thedate, a.datekey
    FROM dds.day a
    WHERE a.theyear = 2015) a,
  (
    SELECT DISTINCT employee_number, tech_number
    FROM acl_census
    WHERE tech_key <> 0) b) c  
LEFT JOIN dds.factRepairOrder d on c.datekey = d.closedatekey
INNER JOIN dds.dimtech e on c.tech_number = e.technumber
  AND d.techkey = e.techkey   
  
SELECT *
FROM #wtf
WHERE tech_number = 'd40'  

UPDATE acl_flag_hours
SET flag_hours_mtd = x.mtd
FROM (
  SELECT a.the_date, a.tech_number, a.year_month, a.flag_hours_day, SUM(b.flag_hours_day) AS mtd
  FROM acl_flag_hours a, acl_flag_hours b
  WHERE a.year_month = b.year_month
    AND a.tech_number = b.tech_number
    AND b.the_date <= a.the_date
  GROUP BY a.the_date, a.tech_number, a.year_month, a.flag_hours_day) x
WHERE acl_flag_hours.the_date = x.the_date
  AND acl_flag_hours.tech_number = x.tech_number;   

SELECT * FROM acl_flag_hours where tech_number = 'd40' ORDER BY tech_number


SELECT a.*, b.flag_hours_day, b.flag_hours_mtd
FROM acl_census a
LEFT JOIN acl_flag_hours b on a.the_date = b.the_date
  AND a.tech_number = b.tech_number
WHERE a.department = 'detail'
  AND a.payroll_class = 'commission'
ORDER BY employee_number, a.the_date 
  
  
-- promising
-- DROP TABLE #wtf;
-- 9/22 include pto hours ? NOT IN calc of proficiency
-- MIN clock hours per pay period IS 40 to invoke bonus, including pto, so should include
-- pto to determine eligibility for bonus, but NOT proficiency
-- separate generation of flag AND clock  
-- ALL data for bonus qualifying pay periods only
-- still struggling, leave out the notion of only qualifying pay periods
-- maybe more interesting to look at how frequently does a tech qualify
DROP TABLE #wtf;
SELECT e.*, f.reg_hours, f.total_hours, round(flag_hours/reg_hours, 2) * 100 AS prof
INTO #wtf  
  FROM ( -- flag hours per pay period
    SELECT c.biweeklypayperiodstartdate, c.biweeklypayperiodenddate, 
      c.biweeklypayperiodsequence, b.last_name, 
      b.first_name, b.employee_number, b.tech_number, SUM(a.flag_hours) AS flag_hours 
    FROM acl_flag_hours a
    INNER JOIN (
      SELECT DISTINCT first_name, last_name, employee_number, tech_number
      FROM acl_census
      WHERE department = 'detail'
        AND payroll_class = 'commission') b on a.tech_number = b.tech_number
    LEFT JOIN dds.day c on a.the_date = c.thedate   
    GROUP BY c.biweeklypayperiodstartdate, c.biweeklypayperiodenddate, 
      c.biweeklypayperiodsequence, b.last_name, 
      b.first_name, b.employee_number, b.tech_number) e
  LEFT JOIN ( -- clock hours per pay period
    SELECT c.biweeklypayperiodstartdate, c.biweeklypayperiodenddate, 
      c.biweeklypayperiodsequence, b.last_name, 
      b.first_name, b.employee_number, 
      SUM(a.regular_hours_day) AS reg_hours,
      SUM(a.regular_hours_day + a.pto_hours_day) AS total_hours
    FROM acl_clock_hours a
    INNER JOIN ( 
      SELECT DISTINCT employee_number, first_name, last_name
      FROM acl_census
      WHERE department = 'detail'
        AND payroll_class = 'commission') b on a.employee_number = b.employee_number
    LEFT JOIN dds.day c on a.the_date = c.thedate   
    GROUP BY c.biweeklypayperiodstartdate, c.biweeklypayperiodenddate, 
      c.biweeklypayperiodsequence, b.last_name, 
      b.first_name, b.employee_number) f on e.employee_number = f.employee_number
        AND e.biweeklypayperiodstartdate = f.biweeklypayperiodstartdate
--   WHERE total_hours >= 40
  
SELECT * FROM #wtf ORDER BY biweeklypayperiodstartdate, last_name, first_name

SELECT a.*, b.biweeklypayperiodsequence, c.biweeklypayperiodstartdate, c.biweeklypayperiodenddate, c.prof
FROM acl_census a
INNER JOIN dds.day b on a.the_date = b.thedate
LEFT JOIN #wtf c on a.employee_number = c.employee_number
  AND c.biweeklypayperiodsequence BETWEEN b.biweeklypayperiodsequence - 6 AND b.biweeklypayperiodsequence - 1
WHERE a.department = 'detail'
  AND a.payroll_class = 'commission'

SELECT the_date, first_name, last_name, COUNT(*), round(avg(prof), 2)
FROM (
  SELECT a.*, b.biweeklypayperiodsequence, c.biweeklypayperiodstartdate, c.biweeklypayperiodenddate, c.prof
  FROM acl_census a
  INNER JOIN dds.day b on a.the_date = b.thedate
  LEFT JOIN #wtf c on a.employee_number = c.employee_number
    AND c.biweeklypayperiodsequence BETWEEN b.biweeklypayperiodsequence - 8 AND b.biweeklypayperiodsequence - 1
  WHERE a.department = 'detail'
    AND a.payroll_class = 'commission') x
GROUP BY the_date, first_name, last_name

so now i am struggling on what IS the algorith for determining detail rate
-- maybe more interesting to look at how frequently does a tech qualify
SELECT a.*, 
  CASE 
    WHEN qual = 0 THEN 0
    ELSE round(100.00 * qual/the_count, 2)
  END 
FROM (
  SELECT last_name, first_name, employee_number, COUNT(*) AS the_count,
    SUM (CASE WHEN total_hours >= 40 THEN 1 ELSE 0 END) AS QUAL,
    SUM (CASE WHEN total_hours < 40 THEN 1 ELSE 0 END) AS NOT_QUAL
  FROM #wtf
  GROUP BY last_name, first_name, employee_number) a
WHERE round(100.00 * qual/the_count, 2) > 50  

SELECT * 
FROM #wtf 

IF the techs qual % for the previous 8 pay periods IS > 50, 
THEN avg prof determines rate, ELSE rate = 12

-- now need proficiency to calc rate (WHEN qualified)
-- IS that the avg prof for the previous 8 pay periods ?
SELECT a.the_date, a.last_name, a.first_name, 
  SUM(CASE WHEN total_hours >= 40 THEN 1 ELSE 0 END) AS qual,
  round(100.00 * SUM(CASE WHEN total_hours >= 40 THEN 1 ELSE 0 END)/COUNT(*), 2) AS qual_perc
FROM acl_census a
INNER JOIN dds.day b on a.the_date = b.thedate
LEFT JOIN #wtf c on a.employee_number = c.employee_number
  AND c.biweeklypayperiodsequence BETWEEN b.biweeklypayperiodsequence - 8 AND b.biweeklypayperiodsequence - 1
WHERE a.department = 'detail'
  AND a.payroll_class = 'commission'
--  AND a.last_name = 'gonzalez'
GROUP BY a.the_date, a.last_name, a.first_name  

-- avg prof
SELECT the_date, first_name, last_name, round(avg(prof), 2)
FROM (
  SELECT a.*, b.biweeklypayperiodsequence, c.biweeklypayperiodstartdate, c.biweeklypayperiodenddate, c.prof
  FROM acl_census a
  INNER JOIN dds.day b on a.the_date = b.thedate
  LEFT JOIN #wtf c on a.employee_number = c.employee_number
    AND c.biweeklypayperiodsequence BETWEEN b.biweeklypayperiodsequence - 8 AND b.biweeklypayperiodsequence - 1
  WHERE a.department = 'detail'
    AND a.payroll_class = 'commission') x
GROUP BY the_date, first_name, last_name

-- day grain IS too fine
-- DO it BY pay period, does someone qualify for bonus for a pay period
-- but this IS forensic, how does it apply to real time pacing
-- it will be fine for the current pay period, the emp qual for bonus based
-- on the perc of qual FROM the prev 8 pay periods
-- prof IS calculated for pay periods ONLY


i need the pay period for the date rather than the date

-- yes, this IS it
-- bear IN mind, this IS NOT forensic, this IS generated for real time
-- ie we know IN retrospect what teach techs prof was IN a closed pay period
-- regardless of previous averages, same with bonus qualification
SELECT j.*, k.prof,
  CASE 
    WHEN qual_perc < 50 THEN 12
    ELSE 
      CASE
        WHEN prof <= 130 THEN 12.0   
        WHEN prof >= 130 AND prof < 140 THEN 12.5
        WHEN prof >= 140 AND prof < 150 THEN 13.5
        WHEN prof >= 150 AND prof < 160 THEN 15
        WHEN prof >= 160 AND prof < 170 THEN 16
        WHEN prof >= 170 AND prof < 180 THEN 17
        WHEN prof >= 180 AND prof < 190 THEN 18
        WHEN prof >= 190 AND prof < 200 THEN 19
        WHEN prof > 200 THEN 20.0  
      END 
  end AS rate
FROM ( -- this looks pretty good for qual for a pay period
  SELECT e.biweeklypayperiodstartdate, e.biweeklypayperiodenddate, d.last_name, 
    d.first_name, d.qual_perc
  FROM (
    SELECT a.the_date, a.last_name, a.first_name, 
      round(100.00 * SUM(CASE WHEN total_hours >= 40 THEN 1 ELSE 0 END)/COUNT(*), 2) AS qual_perc
    FROM acl_census a
    INNER JOIN dds.day b on a.the_date = b.thedate
    LEFT JOIN #wtf c on a.employee_number = c.employee_number
      AND c.biweeklypayperiodsequence BETWEEN b.biweeklypayperiodsequence - 8 AND b.biweeklypayperiodsequence - 1
    WHERE a.department = 'detail'
      AND a.payroll_class = 'commission'
    GROUP BY a.the_date, a.last_name, a.first_name) d
  LEFT JOIN dds.day e on d.the_date = e.thedate  
  GROUP BY e.biweeklypayperiodstartdate, e.biweeklypayperiodenddate, d.last_name, 
    d.first_name, d.qual_perc) j
LEFT JOIN ( -- now need the effective prof for each pay period: avg of previous 8  
  SELECT i.biweeklypayperiodstartdate, i.biweeklypayperiodenddate, h.last_name, 
    h.first_name, h.prof
  FROM (
    SELECT the_date, first_name, last_name, round(avg(prof), 2) AS prof
    FROM (
        SELECT a.*, b.biweeklypayperiodsequence, c.biweeklypayperiodstartdate, c.biweeklypayperiodenddate, c.prof
        FROM acl_census a
        INNER JOIN dds.day b on a.the_date = b.thedate
        LEFT JOIN #wtf c on a.employee_number = c.employee_number
          AND c.biweeklypayperiodsequence BETWEEN b.biweeklypayperiodsequence - 8 AND b.biweeklypayperiodsequence - 1
        WHERE a.department = 'detail'
          AND a.payroll_class = 'commission') d
      GROUP BY the_date, first_name, last_name) h
  LEFT JOIN dds.day i on h.the_date = i.thedate
  GROUP BY i.biweeklypayperiodstartdate, i.biweeklypayperiodenddate, h.last_name, 
    h.first_name, h.prof) k on j.first_name = k.first_name 
  AND j.last_name = k.last_name 
  AND j.biweeklypayperiodstartdate = k.biweeklypayperiodstartdate

      
-- !!!!!!!!!! detail IS flag hours * rate   

SELECT * FROM acl_clock_hours

SELECT * FROM acl_flag_hours