body shop costing discrepancy BETWEEN department AND employee level

-- costing FROM accounting
DROP TABLE #acct;
SELECT control, SUM(amount) AS cost
-- SELECT SUM(amount)
INTO #acct
FROM acl_v1
WHERE year_month = 201509
  AND store_code = 'ry1'
  AND department = 'body shop'
  AND journal IN ('sca','svi','swa')
GROUP BY control 


-- DROP TABLE #ro;
-- SELECT ro, c.employeenumber, c.technumber, sum(b.flaghours * c.laborcost) AS costed
SELECT ro,sum(b.flaghours * c.laborcost) AS costed
INTO #ro
FROM dds.factRepairOrder b
INNER JOIN dds.dimtech c on b.techkey = c.techkey
WHERE b.ro IN (  
  SELECT control
  FROM acl_v1 
  WHERE year_month = 201509
    AND store_code = 'ry1'
    AND department = 'body shop'
    AND journal IN ('sca','svi','swa')  
    GROUP BY control) 
--  AND b.flaghours <> 0    
--  AND c.employeenumber <> 'NA'
GROUP BY ro
--GROUP BY ro, c.employeenumber, c.technumber

SELECT a.*, b.*, cost + costed
FROM #acct a
full OUTER JOIN #ro b on a.control = b.ro
--WHERE abs(round(cost, 2)) <> ABS(round(costed,2))
WHERE ABS(cost + costed) > 10
ORDER BY cost + costed DESC 

SELECT round(SUM(cost), 0) FROM #acct
UNION 
SELECT round(SUM(costed), 0) FROM #ro
    
SELECT sum(cost + costed)
FROM #acct a
full OUTER JOIN #ro b on a.control = b.ro
--WHERE abs(round(cost, 2)) <> ABS(round(costed,2))
WHERE ABS(cost + costed) > 10
ORDER BY cost + costed DESC     
  
SELECT SUM(costed)
FROM (EXECUTE PROCEDURE acl_get_people(201509,'ry1','mainshop')) a 


    SELECT employee_number, first_name, last_name
    FROM acl_census
    WHERE year_month = 201507
      AND store_code = 'ry1'
      AND department = 'body shop'
    GROUP BY employee_number, first_name, last_name
    
    
    SELECT store_code, department, control, SUM(amount) AS paid
    FROM acl_v1 a
    WHERE a.year_month = 201507
      AND store_code = 'ry1'
      AND a.department = 'body shop'
      AND a.journal IN ('pay','gli')
    GROUP BY store_code, department, control    
    
    
-- 11/19
looking at detail acl for 10/15  
dept costing: 56595
SUM empl costing: 54250
dept level controlled BY ro
people costed FROM factrepairorder AND dimtech laborcost
yuk

-- why are some of these flaghours/laborsales zero?
-- eg 16209648
SELECT a.control, a.amount, b.techkey, c.thedate, SUM(flaghours) flaghours, 
  SUM(laborsales) laborsales
-- SELECT *
FROM acl_v1 a
LEFT JOIN dds.factrepairOrder b on a.control = b.ro
LEFT JOIN dds.day c on b.finalclosedatekey = c.datekey
WHERE a.year_month = 201510
  AND a.department = 'detail'
GROUP BY a.control, a.amount, b.techkey, c.thedate  

-- even more interesting, look at 16200701 why IS it only costed $20  
SELECT *
FROM (
  SELECT a.control, a.amount, b.techkey, c.thedate, SUM(flaghours) flaghours, 
    SUM(laborsales) laborsales
  -- SELECT *
  FROM acl_v1 a
  LEFT JOIN dds.factrepairOrder b on a.control = b.ro
  LEFT JOIN dds.day c on b.finalclosedatekey = c.datekey
  WHERE a.year_month = 201510
    AND a.department = 'detail'
    AND a.journal IN ('sca','svi','swa')
  GROUP BY a.control, a.amount, b.techkey, c.thedate) c
LEFT JOIN dds.dimtech d on c.techkey = d.techkey    
ORDER BY c.control


-- 11/24
-- shit, limit it to detail techs
SELECT *
FROM (
  SELECT a.control, a.amount, b.techkey, c.thedate, SUM(flaghours) flaghours, 
    SUM(laborsales) laborsales
  -- SELECT *
  FROM acl_v1 a
  LEFT JOIN dds.factrepairOrder b on a.control = b.ro
  LEFT JOIN dds.day c on b.finalclosedatekey = c.datekey
  WHERE a.year_month = 201510
    AND a.department = 'detail'
    AND a.journal IN ('sca','svi','swa')
  GROUP BY a.control, a.amount, b.techkey, c.thedate) c
INNER JOIN dds.dimtech d on c.techkey = d.techkey    
WHERE d.flagdept = 'Detail'
ORDER BY c.control

-- IS the issue the dept techs (621, 135)
SELECT employeenumber, name, description, technumber, SUM(flaghours * laborcost)
FROM (
SELECT *
FROM (
  SELECT a.control, a.amount, b.techkey, c.thedate, SUM(flaghours) flaghours, 
    SUM(laborsales) laborsales
  -- SELECT *
  FROM acl_v1 a
  LEFT JOIN dds.factrepairOrder b on a.control = b.ro
  LEFT JOIN dds.day c on b.finalclosedatekey = c.datekey
  WHERE a.year_month = 201510
    AND a.department = 'detail'
    AND a.journal IN ('sca','svi','swa')
  GROUP BY a.control, a.amount, b.techkey, c.thedate) c
INNER JOIN dds.dimtech d on c.techkey = d.techkey    
WHERE d.flagdept = 'Detail'
) x GROUP BY employeenumber, name, description, technumber

-- here's some detail 
--SELECT technumber, description, left(description_1, 35), sum(flaghours) FROM (
SELECT b.thedate as close_date, a.ro, a.line, a.flaghours, c.techNumber, 
  c.description, c.laborcost, d.description AS writer,
  e.opcode, e.description
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.closedatekey = b.datekey
  AND b.thedate BETWEEN '08/01/2015' AND curdate()
INNER JOIN dds.dimtech c on a.techkey = c.techkey
  AND c.techkey IN (28,210,634,637)
INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey  
INNER JOIN dds.dimopcode e on a.opcodekey = e.opcodekey
--) x GROUP BY technumber, description, description_1
ORDER BY a.ro, a.line  

-- 12/7/15 none of the fucking acl match the statement, what IS the difference
-- start with body shop

shop/training time IS the difference
paid, but NOT billed

statement: 12,517
vision:     9,726

SELECT gtacct, SUM(gttamt) 
FROM #wtf
WHERE gtacct IN ('124703',
GROUP BY gtacct 

SELECT SUM(gttamt) -- 2792 + 9726 = 12518
FROM dds.stgarkonaglptrns
WHERE gtacct = '167501'
  AND gtdate BETWEEN '11/01/2015' AND '11/30/2015'
  
SELECT SUM(gttamt) 
FROM dds.stgarkonaglptrns
WHERE gtacct = '167501'
  AND gtdate BETWEEN '10/01/2015' AND '10/31/2015'    

shop
statement: 14,354
vision:
    main:  15,953
    pdq:   -4,293
    deta:  -3,727
    -------------
            7,933
            
            
SELECT SUM(gttamt) -- 6420 + 7933 = 145353
FROM dds.stgarkonaglptrns
WHERE gtacct = '166504A'
  AND gtdate BETWEEN '11/01/2015' AND '11/30/2015'          
  
SELECT SUM(gttamt) -- 6420 + 7933 = 145353
FROM dds.stgarkonaglptrns
WHERE gtacct = '166504A'
  AND gtdate BETWEEN '10/01/2015' AND '10/31/2015'      
  
DO a compare 167501/166504A vs flag hours * costing WHERE opcode IN shop,st,tr,train

SELECT a.flaghours, d.laborcost, a.flaghours * d.laborcost AS paid, 
  d.name, d.flagdeptcode, opcode, a.ro
FROM dds.factRepairOrder a  
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
INNER JOIN dds.dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dds.dimtech d on a.techkey = d.techkey
WHERE b.thedate BETWEEN '11/01/2015' AND '11/30/2015'
  AND c.opcode IN ('SHOP','ST','TR','TRAIN')
  
SELECT flagdeptcode, SUM(paid)
FROM (
  SELECT a.flaghours, d.laborcost, a.flaghours * d.laborcost AS paid, 
    d.name, d.flagdeptcode, opcode, a.ro
  FROM dds.factRepairOrder a  
  INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
  INNER JOIN dds.dimopcode c on a.opcodekey = c.opcodekey
  INNER JOIN dds.dimtech d on a.techkey = d.techkey
  WHERE b.thedate BETWEEN '11/01/2015' AND '11/30/2015'
    AND c.opcode IN ('SHOP','ST','TR','TRAIN')
) x GROUP BY flagdeptcode  

SELECT technumber, ro, SUM(paid)
FROM (
  SELECT a.flaghours, d.laborcost, a.flaghours * d.laborcost AS paid, 
    d.name, d.flagdeptcode, opcode, a.ro, d.technumber
  FROM dds.factRepairOrder a  
  INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
  INNER JOIN dds.dimopcode c on a.opcodekey = c.opcodekey
  INNER JOIN dds.dimtech d on a.techkey = d.techkey
  WHERE b.thedate BETWEEN '11/01/2015' AND '11/30/2015'
    AND c.opcode IN ('SHOP','ST','TR','TRAIN')
) x GROUP BY technumber, ro