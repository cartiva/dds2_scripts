SELECT a.fxgact, fxfact, b.gmdept, b.gmdesc, c.gtctl#, SUM(c.gttamt) AS amount
FROM stgArkonaFFPXREFDTA a
LEFT JOIN stgArkonaGLPMAST b on a.fxgact = b.gmacct
  AND b.gmyear = 2015
LEFT JOIN stgArkonaGLPTRNS c on b.gmacct = c.gtacct
  AND c.gtdate BETWEEN '04/01/2015' AND '04/30/2015' 
WHERE fxfact IN('665','675')
  AND fxconsol = ''
  AND fxcyy = 2015  
GROUP BY a.fxgact, fxfact, b.gmdept, b.gmdesc, c.gtctl#  
HAVING SUM(c.gttamt) <> 0

SELECT 
  SUM(CASE WHEN gmdept = 'BS' THEN amount END) AS BS,
  SUM(CASE WHEN gmdept <> 'BS' THEN amount END) AS SERV
FROM (  
  SELECT a.fxgact, fxfact, b.gmdept, b.gmdesc, c.gtctl#, SUM(c.gttamt) AS amount
  FROM stgArkonaFFPXREFDTA a
  LEFT JOIN stgArkonaGLPMAST b on a.fxgact = b.gmacct
    AND b.gmyear = 2015
  LEFT JOIN stgArkonaGLPTRNS c on b.gmacct = c.gtacct
    AND c.gtdate BETWEEN '04/01/2015' AND '04/30/2015' 
  WHERE fxfact IN('665','675')
    AND fxconsol = ''
    AND fxcyy = 2015  
  GROUP BY a.fxgact, fxfact, b.gmdept, b.gmdesc, c.gtctl#  
  HAVING SUM(c.gttamt) <> 0) x



-- pdq ------------------------------------------------------------------------
select *
FROM stgArkonaGLPTRNS
WHERE gtacct = '166503'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015' 
  
SELECT 
  sum(case when gttamt > 0 then gttamt END),
  sum(case when gttamt < 0 then gttamt END)
--select *  
FROM stgArkonaGLPTRNS
WHERE gtacct = '124701'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'   

SELECT gtctl#, SUM(gttamt) 
-- select *
--SELECT 
--  sum(case when gttamt > 0 then gttamt END),
--  sum(case when gttamt < 0 then gttamt END)
FROM stgArkonaGLPTRNS
WHERE gtacct = '124723'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'    
GROUP BY gtctl#   

SELECT a.*, b.ymname
FROM (
  SELECT gtctl#, SUM(gttamt) 
  -- select *
  FROM stgArkonaGLPTRNS
  WHERE gtacct = '124723'
    AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'    
  GROUP BY gtctl#) a
LEFT JOIN stgArkonaPYMAST b on a.gtctl# = b.ymempn     

SELECT 
  SUM(CASE WHEN amount > 0 THEN amount END),
  SUM(CASE WHEN amount < 0 THEN amount END)
FROM (  
  SELECT a.*, b.ymname
  FROM (
    SELECT gtctl#, SUM(gttamt) AS amount
    -- select *
    FROM stgArkonaGLPTRNS
    WHERE gtacct = '124723'
      AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'    
    GROUP BY gtctl#) a
  LEFT JOIN stgArkonaPYMAST b on a.gtctl# = b.ymempn) c  
  
-- detail ------------------------------------------------------------------------
select gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
FROM stgArkonaGLPTRNS
WHERE gtacct = '166524'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015' 
  
select gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
FROM stgArkonaGLPTRNS
WHERE gtacct = '124702'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'   
  
SELECT gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
-- SELECT SUM(gttamt)
FROM stgArkonaGLPTRNS
WHERE gtacct = '124724'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'   
  
SELECT a.*, b.ymname
FROM (
  SELECT gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
  FROM stgArkonaGLPTRNS
  WHERE gtacct = '124724'
    AND gtdate BETWEEN '04/01/2015' AND '04/30/2015' ) a
LEFT JOIN stgArkonaPYMAST b on a.gtctl# = b.ymempn   
  
SELECT a.gtctl#, b.ymname, SUM(a.gttamt)
FROM (
  SELECT gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
  FROM stgArkonaGLPTRNS
  WHERE gtacct = '124724'
    AND gtdate BETWEEN '04/01/2015' AND '04/30/2015' ) a
LEFT JOIN stgArkonaPYMAST b on a.gtctl# = b.ymempn   
GROUP BY a.gtctl#, b.ymname    
  
-- bs ------------------------------------------------------------------------- 
select gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
FROM stgArkonaGLPTRNS
WHERE gtacct in ('167500', '167501')
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'
-- ROs & Payroll/Accrual :: RO (sca+std+svi+swa) = Pay (gli+pay)
-- ROs neg amounts, Pay pos amounts
select gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
FROM stgArkonaGLPTRNS
WHERE gtacct = '124703'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
  
select gtjrnl, sum(gttamt)
FROM stgArkonaGLPTRNS
WHERE gtacct = '124703'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'
GROUP BY gtjrnl  
    
select gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
FROM stgArkonaGLPTRNS
WHERE gtacct = '167100'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
  
select gtjrnl, sum(gttamt)
FROM stgArkonaGLPTRNS
WHERE gtacct = '167100'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'
GROUP BY gtjrnl    
  
  
-- main shop -------------------------------------------------------------------------   
select gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
FROM stgArkonaGLPTRNS
WHERE gtacct in ('166504', '166504a')
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'
  
select gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
FROM stgArkonaGLPTRNS
WHERE gtacct = '124704'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
  
select gtjrnl, sum(gttamt)
FROM stgArkonaGLPTRNS
WHERE gtacct = '124704'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
GROUP BY gtjrnl   
  
 
select gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
FROM stgArkonaGLPTRNS
WHERE gtacct = '166000'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'   
ORDER BY gttamt DESC  
    
select 
  SUM(CASE WHEN gttamt > 0 AND gtdesc = 'shop time' THEN gttamt END),
  SUM(CASE WHEN gttamt < 0 THEN gttamt END)
FROM stgArkonaGLPTRNS
WHERE gtacct = '166000'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'     

select gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gttamt, gtdesc
FROM stgArkonaGLPTRNS
WHERE gtacct = '124700'
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
    

-- shop AND training time   
-- jeri gets the value FROM running labor op report which uses CLOSE date
SELECT *
FROM dimopcode
WHERE description like '%traini%'

SELECT *
FROM dimopcode
WHERE description like '%shop%'  

SELECT f.thedate as closeDate, a.ro, a.line, a.flaghours, a.roflaghours, 
  d.servicetypecode, c.opcode, e.technumber, e.name, e.laborcost, 
  flaghours * laborcost AS cost
FROM factRepairOrder a
INNER JOIN day b on a.closedatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimServiceType d on a.serviceTypeKey = d.serviceTypeKey
INNER JOIN dimTech e on a.techkey = e.techkey
INNER JOIN day f on a.closedatekey = f.datekey
WHERE f.thedate BETWEEN '04/01/2015' AND '04/30/2015'   
  AND c.opcode IN ('SHOP','ST','TR','TRAIN')
ORDER BY ro, technumber

SELECT servicetypecode, opcode, SUM(cost)
FROM (
  SELECT b.thedate, a.ro, a.line, a.flaghours, a.roflaghours, d.servicetypecode, 
    c.opcode, e.name, e.laborcost, flaghours * laborcost AS cost
  FROM factRepairOrder a
  INNER JOIN day b on a.closedatekey = b.datekey
  INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
  INNER JOIN dimServiceType d on a.serviceTypeKey = d.serviceTypeKey
  INNER JOIN dimTech e on a.techkey = e.techkey
  WHERE b.thedate BETWEEN '04/01/2015' AND '04/30/2015'     
    AND c.opcode IN ('SHOP','ST','TR','TRAIN')) x
GROUP BY servicetypecode, opcode