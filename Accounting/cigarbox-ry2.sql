--
-- per greg  
UPDATE zcb
SET al3 = 'Gross Profit'
-- SELECT * FROM zcb
WHERE storecode = 'ry1'
  AND al3 IN ('cogs','sales')

--
--
-- hierarchy definition
INSERT INTO zcb
SELECT name, 'RY2', dl1, dl2, dl3, al1, al2, al3, al4, glaccount, 1
FROM zcb
WHERE glaccount IS NULL s
  AND dl2 NOT IN ('ncr','body shop')
  AND dl3 <> 'car wash';

-- SELECT * FROM zcb WHERE storecode = 'ry2'
-- delete FROM zcb WHERE storecode = 'ry2' AND glaccount IS NOT NULL 
-- this looks LIKE it will do
SELECT name,storecode,dl2, glaccount
FROM zcb
WHERE glaccount IS NOT NULL 
GROUP BY name,storecode,dl2, glaccount
HAVING COUNT(*) > 1

/**************** f & i ******************************************************/
/*
-- f&i
DELETE FROM zcb WHERE name = 'RY2 F&I';
INSERT INTO zcb 
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  'New' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'Sales' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota; 
INSERT INTO zcb 
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  'New' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'COGS' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota;
INSERT INTO zcb 
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  'Used' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'Sales' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota;  
INSERT INTO zcb 
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  'Used' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'COGS' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota; 

INSERT INTO zcb   
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  CASE 
    WHEN description LIKE '%new%' THEN 'New'
    WHEN description LIKE '%used%' THEN 'Used'
    ELSE 'XXXX'
  END AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  CASE 
    WHEN gmtype IN ('4','7') THEN 'Sales'
    WHEN gmtype = '5' THEN 'COGS'
    ELSE 'XXXXX'
  END AS al3, 'NA' as al4, glaccount, 1 AS split
FROM tmpdocs a
LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
WHERE reportnumber = 24
  AND glaccount IS NOT NULL 
UNION -- need to ADD those COGS accounts NOT specified IN doc
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  CASE 
    WHEN description LIKE '%new%' THEN 'New'
    WHEN description LIKE '%used%' THEN 'Used'
    ELSE 'XXXX'
  END AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'COGS' AS al3, 'NA' as al4, gmdboa, 1 AS split
FROM tmpdocs a
INNER JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
WHERE reportnumber = 24
  AND glaccount IS NOT NULL 
  AND gmtype = '4';  
*/   
/**************** f & i ******************************************************/  
/***************** service main shop doc 74 **********************************/

-- main shop sales
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 74 AND ((l2 = 10 AND l3 = 10) 
  OR (l2 = 10 AND l3 = 30 AND l4 = 10)
  OR (l2 = 10 AND l3 = 20)) AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Sales';

-- main shop cogs 
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, c.gmdboa, 1
FROM zcb a,
(SELECT b.gmdboa FROM tmpdocs a 
  LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct 
    AND b.gmyear = 2012 
  WHERE reportnumber = 74 
    AND ((l2 = 10 AND l3 = 10) OR (l2 = 10 AND l3 = 30 AND l4 = 10) OR (l2 = 10 AND l3 = 20))
    AND glaccount IS NOT NULL
    AND gmdboa <> '') c
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'COGS'; 
  
-- parts split
UPDATE zcb
SET split = .5
--SELECT * FROM zcb 
WHERE glaccount IN ('246700','246705','246709','246800','246809','266700','266705','266709','266800','266809'); 
  
-- main shop expenses
-- personnel
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 74 AND l2 = 20 AND l3 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Personnel Expenses';

-- semi-fixed policy
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 74 AND l2 = 20 AND l3 = 20 and l4 = 70 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy';
  
-- semi-fixed non policy
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 74 AND l2 = 20 AND l3 = 20 and l4 <> 70 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na';  

-- fixed
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 74 AND l2 = 20 AND l3 = 30 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Fixed Expenses';

/* proof */
-- aug differs FROM cb, but agrees with doc
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'main shop'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al1

-- expenses
SELECT yearmonth, dl2, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'main shop'
  AND c.al2 = 'total expenses'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al3

/***************** service main shop doc 74 **********************************/

/***************** service pdq doc 69 ****************************************/
--SELECT * FROM tmpdocs WHERE title = 'ry2 pdq'

--SELECT * FROM tmpdocs WHERE title = 'ry2 pdq' AND glaccount LIKE '2665%'
--SELECT * FROM zcb WHERE glaccount LIKE '2665%'

-- pdq sales
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 69 AND l2 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'pdq'
  AND al3 = 'Sales';
-- pdq cogs 
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, c.gmdboa, 1
FROM zcb a,
(SELECT b.gmdboa FROM tmpdocs a 
  LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct 
    AND b.gmyear = 2012 
  WHERE reportnumber = 69 
    AND l2 = 10
    AND glaccount IS NOT NULL
    AND gmdboa <> '') c
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'pdq'
  AND al3 = 'COGS'; 
-- parts split
UPDATE zcb
SET split = .5
--SELECT * FROM zcb 
WHERE glaccount IN ('246701','246702','246801','266701','266702','266801');  

-- pdq expenses
-- personnel
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 69 AND l2 = 20 and l3 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'pdq'
  AND al3 = 'Personnel Expenses';   
-- semi policy  
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 69 AND l2 = 20 and l3 = 20 AND l4 = 40 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'pdq'
  AND al3 = 'Semi-Fixed Expenses'  
  AND al4 = 'Policy'; 
-- semi non policy
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 69 AND l2 = 20 and l3 = 20 AND l4 <> 40 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'pdq'
  AND al3 = 'Semi-Fixed Expenses'  
  AND al4 = 'na';   
  
-- fixed
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 69 AND l2 = 20 and l3 = 30 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'Service'
  AND dl3 = 'pdq'
  AND al3 = 'Fixed Expenses'  
  AND al4 = 'na';    
  
/* proof */
-- august IS off, agrees with doc
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'pdq'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al1

/***************** service pdq doc 69 ****************************************/  

/***************** parts doc 23 **********************************************/
--SELECT * FROM tmpdocs WHERE title = 'ry2 parts dept'
-- delete FROM zcb WHERE storecode = 'ry2' AND dl2 = 'parts' AND glaccount IS NOT NULL 
-- parts sales ros
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, .5
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 23 AND l3 = 10 and l4 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'parts'
  AND al3 = 'Sales';
-- parts cogs ros
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, c.gmdboa, .5
FROM zcb a,
(SELECT b.gmdboa FROM tmpdocs a 
  LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct 
    AND b.gmyear = 2012 
  WHERE reportnumber = 23 
    AND l3 = 10 and l4 = 10
    AND glaccount IS NOT NULL
    AND gmdboa <> '') c
WHERE storecode = 'ry2'
  AND dl2 = 'parts'
  AND al3 = 'COGS'; 
-- parts sales non ro
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 23 AND ((l3 = 10 and l4 <> 10) OR (l2 = 10 and l3 between 30 and 50)) AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'parts'
  AND al3 = 'Sales'; 
-- parts cogs non ros
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, c.gmdboa, 1
FROM zcb a,
(SELECT b.gmdboa FROM tmpdocs a 
  LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct 
    AND b.gmyear = 2012 
  WHERE reportnumber = 23 
    AND ((l3 = 10 and l4 <> 10) OR (l2 = 10 and l3 between 30 and 50))
    AND glaccount IS NOT NULL
    AND gmdboa <> '') c
WHERE storecode = 'ry2'
  AND dl2 = 'parts'
  AND al3 = 'COGS';   
  
-- parts expenses
-- personnel
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 23 AND l2 = 20 and l3 = 20 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'parts'
  AND al3 = 'Personnel Expenses';    
-- semi policy
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 23 AND l2 = 20 and l3 = 30 AND l4 = 95 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'parts'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy';    
-- semi non policy
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 23 AND l2 = 20 and l3 = 30 AND l4 <> 95 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'parts'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na'; 
-- fixed
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 23 AND l2 = 20 and l3 = 40 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl2 = 'parts'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na';   
  
/*  proof */
-- october semi-fixed off compared to cb, but agrees with doc, so good enough
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'parts'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al1

/***************** parts doc 23 ****************************************/

/***************** new vehicles doc 20, f&i 24 ******************************/

DELETE FROM zcb WHERE storecode = 'ry2' AND dl3 = 'New Vehicles' AND glaccount IS NOT NULL; 
-- sales 
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 20 AND l2 = 10 AND l3 BETWEEN 10 AND 20 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'New Vehicles'
  AND al3 = 'sales'; 
-- cogs 
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, c.gmdboa, 1
FROM zcb a,
(SELECT b.gmdboa FROM tmpdocs a 
  LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct 
    AND b.gmyear = 2012 
  WHERE reportnumber = 20 AND l2 = 10 AND l3 BETWEEN 10 AND 20
    AND glaccount IS NOT NULL
    AND gmdboa <> '') c
WHERE storecode = 'ry2'
  AND dl3 = 'new vehicles'
  AND al3 = 'COGS'; 
-- expenses
-- variable policy   
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 20 AND l2 = 20 AND l3 = 10 AND l4 BETWEEN 30 AND 35 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'New Vehicles'
  AND al3 = 'Variable Expenses'
  AND al4 = 'Policy'; 
-- variable non policy   
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 20 AND l2 = 20 AND l3 = 10 AND (l4 NOT BETWEEN 30 AND 35) AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'New Vehicles'
  AND al3 = 'Variable Expenses'
  AND al4 = 'na';   
-- personnel
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 20 AND l2 = 20 AND l3 = 20 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'New Vehicles'
  AND al3 = 'personnel Expenses'
  AND al4 = 'na';  
-- semi-fixed
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 20 AND l2 = 20 AND l3 = 30 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'New Vehicles'
  AND al3 = 'semi-fixed Expenses'
  AND al4 = 'na'; 
-- semi-fixed missing accts    
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '25601', 1
FROM zcb a
WHERE storecode = 'ry2'
  AND dl3 = 'new Vehicles'
  AND al3 = 'semi-fixed Expenses'
  AND al4 = 'na';  
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '25610', 1
FROM zcb a
WHERE storecode = 'ry2'
  AND dl3 = 'new Vehicles'
  AND al3 = 'semi-fixed Expenses'
  AND al4 = 'na';    
-- fixed
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 20 AND l2 = 20 AND l3 = 40 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'New Vehicles'
  AND al3 = 'fixed Expenses'
  AND al4 = 'na';  
-- f&i
INSERT INTO zcb
SELECT 'Cigar Box', storecode, 'Store', 'Variable', 'New Vehicles','Net Profit', al2, al3, al4, glaccount, 1
--SELECT *
FROM zcb
WHERE name = 'ry2 f&i'
  AND dl2 = 'new'
  AND glaccount IS NOT NULL         
 
/* proof */
-- total expenses right on
-- oct gross lo 1200 (f&i right ON - only have oct FS to check new/used f&i)
-- sep gross lo 798 
-- aug gross lo 1602 
SELECT yearmonth, dl2, al1, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'new vehicles'
  AND storecode = 'ry2'
  AND name = 'cigar box'
GROUP BY yearmonth, dl2, al1, al2

/***************** new vehicles doc 20 **************************************/


/***************** used vehicles doc 21 **************************************/
SELECT * FROM tmpdocs WHERE title = 'ry2 used veh dept'

DELETE FROM zcb WHERE storecode = 'ry2' AND dl3 = 'Used Vehicles' AND glaccount IS NOT NULL; 

-- expenses
-- variable policy   
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 21 AND  l2 = 20 and l3 = 10 and l4 = 30 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'Variable Expenses'
  AND al4 = 'Policy'; 
-- variable non policy   
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 21 AND l2 = 20 and l3 = 10 and l4 <> 30 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'Variable Expenses'
  AND al4 = 'na';   
-- personnel
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 21 AND l2 = 20 and l3 = 20 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'personnel Expenses'
  AND al4 = 'na';  
-- semi-fixed
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 21 AND l2 = 20 AND l3 = 30 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'used Vehicles'
  AND al3 = 'semi-fixed Expenses'
  AND al4 = 'na';  
-- semi-fixed missing accts
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '25602', 1
FROM zcb a
WHERE storecode = 'ry2'
  AND dl3 = 'used Vehicles'
  AND al3 = 'semi-fixed Expenses'
  AND al4 = 'na';    
-- fixed
INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 21 AND l2 = 20 AND l3 = 40 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'used Vehicles'
  AND al3 = 'fixed Expenses'
  AND al4 = 'na';  
  
  
-- sales 

INSERT INTO zcb
SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT glaccount FROM tmpdocs WHERE reportnumber = 21 AND l2 = 10 AND glaccount IS NOT NULL) b
WHERE storecode = 'ry2'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'sales'; 
-- cogs 
INSERT INTO zcb
SELECT a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, c.gmdboa, 1
FROM zcb a,
(SELECT b.gmdboa FROM tmpdocs a 
  LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct 
    AND b.gmyear = 2012 
  WHERE reportnumber = 21 AND l2 = 10
    AND glaccount IS NOT NULL
    AND gmdboa <> '') c
WHERE storecode = 'ry2'
  AND dl3 = 'used vehicles'
  AND al3 = 'COGS'; 
-- ok, this takes care of categorizing the recon accounts AS cogs
UPDATE zcb
SET al3 = 'COGS'
WHERE glaccount IN (
  SELECT gmacct 
  FROM zcb a
  LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
    AND gmyear = 2012
  WHERE storecode = 'ry2' and dl3 = 'used vehicles' AND al2 = 'gross profit'
    AND gmdesc LIKE 'CST%'
    AND al3 = 'sales');
    
-- f&i
INSERT INTO zcb
SELECT 'Cigar Box', storecode, 'Store', 'Variable', 'Used Vehicles','Net Profit', al2, al3, al4, glaccount, 1
FROM zcb
WHERE name = 'ry2 f&i'
  AND dl2 = 'used'
  AND glaccount IS NOT NULL;   
  
/*  proof */
-- perfect-ish
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'used vehicles'
  AND storecode = 'ry2'
  AND name = 'cigar box'
GROUP BY yearmonth, dl2, al1

SELECT yearmonth,dl2,al1,al2,SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'used vehicles'
  AND storecode = 'ry2'
  AND name = 'cigar box'
GROUP BY yearmonth,dl2,al1,al2


/***************** used vehicles doc 20 **************************************/


