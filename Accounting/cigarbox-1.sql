-- how many accounts are we talking about here?
SELECT COUNT(*) -- 1660
FROM stgArkonaGLPMAST a
WHERE a.gmyear = 2012
  AND a.gmstyp = 'a'

SELECT gmstyp, COUNT(*) -- 1660 + 1053 + 899 = 3612
FROM stgArkonaGLPMAST a
WHERE a.gmyear = 2012
GROUP BY a.gmstyp

so that IS 3612 rows for each account hierarchy

SELECT hierarchytype, COUNT(*)
FROM dimAccountHierarchies
GROUP BY hierarchytype

SELECT glaccount, COUNT(*)
FROM dimaccounthierarchies
GROUP BY glaccount
HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC 

SELECT *
FROM dimaccounthierarchies
WHERE glaccount = '185900'

hmmm, there should NOT be any accounts for any except leaf nodes

ALL leaf nodes
SELECT * 
FROM dimaccounthierarchies
WHERE rgt - lft = 1
AND glaccount IS NULL 
-- these are ALL the non leaf nodes i fucked up somehow AND assigned accounts
SELECT * 
FROM dimaccounthierarchies
WHERE rgt - lft > 1
AND glaccount IS NOT NULL  AND description <> 'Gross Profit'

ALL i am fucking trying to DO IS come up with a list of accounts

am i dealing with a separate headers issue?

am totally mind fucking ON multiple hierarchy

based ON the account what DO i know?
  IN cigar box land i know the dept AND the account type (IS the assumption)

so a new dimaccthier
?? these rows are ALL leaves?
hierarcy   dept   dept-lft  dept-rgt    accttype  acct-lft      acct-rgt     glaccount    
cb         pdq    9         10          sales         

-- 10/31
does every account# have a dept/acct type  
select *
FROM stgArkonaGLPMAST a
WHERE a.gmyear = 2012
  AND a.gmstyp = 'a'       
-- some have no dept  
select *
FROM stgArkonaGLPMAST a
WHERE a.gmyear = 2012
  AND a.gmstyp = 'a'  
  AND (gmtype = '' OR gmdept = '')  
-- AND there are transactions ON those accounts  
SELECT gtacct, MIN(gtdate), MAX(gtdate), COUNT(*)
FROM stgArkonaGLPTRNS  
WHERE gtacct IN (
  select gmacct
  FROM stgArkonaGLPMAST a
  WHERE a.gmyear = 2012
    AND a.gmstyp = 'a'  
    AND (gmtype = '' OR gmdept = ''))
GROUP BY gtacct    

-- what's the differencts BETWEEN payroll depts AND coa depts?
SELECT * FROM stgarkonapymast
coa depts: stkArkonaGLDEPT:
store      gddept          gddesc
  RY1      NC              New Vehicle
  ry2      PD              Parts
  ry3      SD              Service
etc   
payroll depts: stgArkonaPYPCLKCTL      
yicco#  yicdept   yicdesc              
ry1     01        New Car
ry1     11        Office 11
ry2     10        Honda New Car
ry3     06        parts dept     

payroll depts are assigned to glaccounts IN pyactgr (dist codes)

SELECT ytaco#, ytadic, ytadsc, ytagpa, b.*
FROM stgArkonaPYACTGR a
LEFT JOIN (
  select *
  FROM stgArkonaGLPMAST c
  WHERE c.gmyear = 2012) b ON a.ytagpa = b.gmacct  
WHERE a.currentrow = true       
-- shit, think i am going down an orthogonal rabbit hole

what i think i want IS a single factGLTransaction TABLE
but to be able to roll it up ON whatever dept/accttype hierarchy i want
this would allow me to DO "real time", MTD, etc data for the different hierarchies
vs
hmm, single factGLTransaction
THEN separate accumulating snapshot fact tables for the various hierarchies?

Mistake 8: Split hierarchies and hierarchy levels into multiple dimensions.
A hierarchy is a cascaded series of many-to-one relationships. For example,
many products may roll up to a single brand; and many brands may roll
up to a single category. If your dimension is expressed at the lowest level
of granularity (for example, product), then all the higher levels of the hierarchy
can be expressed as unique values in the product row. Users understand
hierarchies. Your job is to present the hierarchies in the most natural
and efficient manner. A hierarchy belongs together in a single physical flat
dimension table. Resist the urge to snowflake a hierarchy by generating a
set of progressively smaller subdimension tables. In this case you would be
confusing backroom data staging with front room data presentation!

Finally, if more than one roll-up exists simultaneously for a dimension, in
most cases it�s perfectly reasonable to include multiple hierarchies in the
same dimension, as long as the dimension has been defined at the lowest
possible grain (and the hierarchies are uniquely labeled).

 ngalemmo on Mon Dec 06, 2010 6:56 am
You need to ask yourself if a flattened structure is appropriate for the hierarchy you describe.
In generall, a flatted hierarchy works best when the levels are well defined and stable. 
If the hierarchy is ragged or the levels are not well defined, a flattened form 
does not t help queries as no one would know what level 3, for example, represents.

-- 11/2 going with (pussy mode) flattened hierarchy(s) IN dimAccount

-- filter for dimaccount
WHERE cbdl1 = 'Store' AND cbal1 = 'Net Profit'

ALTER TABLE dimAccount
ADD COLUMN cbDeptL1 cichar(12)
ADD COLUMN cbDeptL2 cichar(12)
ADD COLUMN cbDeptL3 cichar(12)
ADD COLUMN cbAcctL1 cichar(24)
ADD COLUMN cbAcctL2 cichar(24)
ADD COLUMN cbAcctL3 cichar(24)
ADD COLUMN cbAcctL4 cichar(24);

UPDATE dimaccount
  SET cbDeptL1 = 'NA',
      cbDeptL2 = 'NA',
      cbDeptL3 = 'NA',
      cbAcctL1 = 'NA',
      cbAcctL2 = 'NA',
      cbAcctL3 = 'NA',
      cbAcctL4 = 'NA';
      
-- there are no accounts categorized AS fi IN COA
--SELECT * FROM stgArkonaGLPMAST WHERE gmyear = 2012 AND gmdept = 'fi' AND gmstyp = 'a'
    
UPDATE dimaccount  
  SET cbDeptL2 = 
--SELECT a.*,   
        CASE 
          WHEN gmfsDepartmentLevel2 IN ('Finance & Insurance','New Vehicle Department','Used Vehicle Department') THEN 'Variable'
          WHEN gmfsDepartmentLevel2 = 'Mechanical'  THEN 'Service'  
          WHEN gmfsDepartmentLevel2 = 'Parts & Accessories' THEN 'Parts'   
          WHEN gmfsDepartmentLevel2 = 'Body Shop' THEN 'Body Shop'  
          WHEN gmfsDepartmentLevel2 = 'Lease & Rental'  THEN 'NCR' 
          ELSE 'NA'
        END,                                  
      cbDeptL3 = 
        CASE
          WHEN gmfsDepartmentLevel2 = 'New Vehicle Department' 
            OR CAST(LEFT(gmfsAccount, 3) AS sql_integer) IN (806,807,850,810,494,443,444,853,855) THEN 'New Vehicles'
          WHEN gmfsDepartmentLevel2 = 'Used Vehicle Department'
            OR CAST(LEFT(gmfsAccount, 3) AS sql_integer) IN (808,809,851,811,454,455,854,856) THEN 'Used Vehicles'        
          WHEN gmfsDepartmentLevel2 = 'Mechanical' THEN 
            CASE 
              WHEN glDepartment = 'SD' THEN 'Main Shop'
              WHEN glDepartment = 'QL' THEN 'PDQ'
              WHEN glDepartment = 'RE' THEN 'Detail'
              WHEN glDepartment = 'CW' THEN 'Car Wash'
            END
          ELSE 'NA'
        END,
--FROM dimaccount a        
      cbAcctL2 = 
        CASE 
          WHEN gmfsAccountLevel1 IN ('Sales','COGS') THEN 'Gross Profit'
          WHEN gmfsAccountLevel1 = 'Expenses' THEN 'Total Expenses'
          ELSE 'NA'
        END,
      cbAcctL3 =
        CASE 
--          WHEN gmfsAccountLevel1 = 'Sales' THEN 'Sales'
--          WHEN gmfsAccountLevel1 = 'COGS' THEN 'COGS'
          WHEN gmfsAccountLevel2 = 'Variable Selling Expenses' THEN 'Variable Expenses'
          WHEN gmfsAccountLevel2 = 'Personnel Expenses' THEN 'Personnel Expenses'
          WHEN gmfsAccountLevel2 = 'Fixed Expenses' THEN 'Fixed Expenses'
          WHEN gmfsAccountLevel2 = 'Semi-Fixed Expenses' THEN 'Semi-Fixed Expenses'
          ELSE 'NA'
        END,
      cbAcctL4 = 
        CASE
          WHEN LEFT(gmfsAccount, 3) = '015' THEN 'Policy'
          WHEN LEFT(gmfsAccount, 3) = '067' THEN 'Policy' 
          ELSE 'NA'
        END;
 
UPDATE dimaccount 
SET cbdeptl1 = 'Store'
  WHERE cbDeptL2 <> 'NA';
UPDATE dimaccount 
SET cbAcctl1 = 'Net Profit'
  WHERE cbAcctL2 <> 'NA';           

-- why ALL the nulls, BY default ALL values should BY NA BY default  
SELECT cbdeptL1,cbdeptl2,cbdeptl3,cbacctl1,cbacctl2,cbacctl3,cbacctl4
FROM dimaccount 
GROUP BY cbdeptL1,cbdeptl2,cbdeptl3,cbacctl1,cbacctl2,cbacctl3,cbacctl4

SELECT *
FROM stgArkonaGLPTRNS
WHERE gtdate BETWEEN '08/01/2012' AND '08/31/2012'

-- expenses good except pdq, car wash, detail
-- NOT happy with some rows, NOT getting totals at higher levels
SELECT cbdeptL1,cbdeptl2,cbdeptl3,cbacctl1,cbacctl2,cbacctl3,cbacctl4, SUM(gttamt)
FROM dimaccount a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'
GROUP BY cbdeptL1,cbdeptl2,cbdeptl3,cbacctl1,cbacctl2,cbacctl3,cbacctl4
ORDER BY cbdeptL1,cbdeptl2,cbdeptl3

SELECT cbdeptL1,cbdeptl2,cbdeptl3,cbacctl1,cbacctl2, cbacctl3, SUM(gttamt)
FROM dimaccount a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'
GROUP BY cbdeptL1,cbdeptl2,cbdeptl3,cbacctl1,cbacctl2, cbacctl3
ORDER BY cbdeptL1,cbdeptl2,cbdeptl3

SELECT *
FROM dimaccount
WHERE cbacctl2 = 'Total Expenses'
  AND cbdeptl3 = 'Car Wash'

 -- 11/3
 
SELECT cbdeptL1,cbdeptl2,cbdeptl3,cbacctl1,cbacctl2,cbacctl3,cbacctl4, COUNT(*)
FROM dimaccount a
GROUP BY cbdeptL1,cbdeptl2,cbdeptl3,cbacctl1,cbacctl2,cbacctl3,cbacctl4
-- why rows with dept = NA but only some acct
-- 1. no dept but acct
--     acctL2/3 exclude glDept AD, GN
--     that took care of most
--   3 unused accounts ('199900','199996','186100') -- no use 2010-2012
-- the rest are ALL glDept LR other income, COGS -- no use 2010-2012
--   ALL are cbAcctL2 gmfsDepartmentLevel1 IS NULL   
SELECT *
FROM dimaccount
WHERE cbdeptl1 = 'na'
  AND cbacctl1 <> 'na'

UPDATE dimaccount
  SET cbDeptL1 = 'NA',
      cbDeptL2 = 'NA',
      cbDeptL3 = 'NA',
      cbAcctL1 = 'NA',
      cbAcctL2 = 'NA',
      cbAcctL3 = 'NA',
      cbAcctL4 = 'NA';
-- 11/3 exclude dept AD, GN  
    
UPDATE dimaccount  
  SET cbDeptL2 = 
--SELECT a.*,   
        CASE 
          WHEN gmfsDepartmentLevel2 IN ('Finance & Insurance','New Vehicle Department','Used Vehicle Department') THEN 'Variable'
          WHEN gmfsDepartmentLevel2 = 'Mechanical'  THEN 'Service'  
          WHEN gmfsDepartmentLevel2 = 'Parts & Accessories' THEN 'Parts'   
          WHEN gmfsDepartmentLevel2 = 'Body Shop' THEN 'Body Shop'  
          WHEN gmfsDepartmentLevel2 = 'Lease & Rental'  THEN 'NCR' 
          ELSE 'NA'
        END,                                  
      cbDeptL3 = 
        CASE
          WHEN gmfsDepartmentLevel2 = 'New Vehicle Department' 
            OR CAST(LEFT(gmfsAccount, 3) AS sql_integer) IN (806,807,850,810,494,443,444,853,855) THEN 'New Vehicles'
          WHEN gmfsDepartmentLevel2 = 'Used Vehicle Department'
            OR CAST(LEFT(gmfsAccount, 3) AS sql_integer) IN (808,809,851,811,454,455,854,856) THEN 'Used Vehicles'        
          WHEN gmfsDepartmentLevel2 = 'Mechanical' THEN 
            CASE 
              WHEN glDepartment = 'SD' THEN 'Main Shop'
              WHEN glDepartment = 'QL' THEN 'PDQ'
              WHEN glDepartment = 'RE' THEN 'Detail'
              WHEN glDepartment = 'CW' THEN 'Car Wash'
            END
          ELSE 'NA'
        END,
--FROM dimaccount a        
      cbAcctL2 = 
        CASE 
          WHEN gmfsAccountLevel1 IN ('Sales','COGS') 
            AND glDepartment NOT IN ('AD','GN') 
            AND glAccount NOT IN ('199900','199996','186100') 
            AND gmfsDepartmentLevel1 IS NOT NULL THEN 'Gross Profit'
          WHEN gmfsAccountLevel1 = 'Expenses' 
            AND glDepartment NOT IN ('AD','GN') 
            AND glAccount NOT IN ('199900','199996','186100') 
            AND gmfsDepartmentLevel1 IS NOT NULL THEN 'Total Expenses'
          ELSE 'NA'
        END,
      cbAcctL3 =
        CASE 
          WHEN gmfsAccountLevel1 = 'Sales' THEN 'Sales'
          WHEN gmfsAccountLevel1 = 'COGS' THEN 'COGS'
          WHEN gmfsAccountLevel2 = 'Variable Selling Expenses' AND glDepartment NOT IN ('AD','GN') THEN 'Variable Expenses'
          WHEN gmfsAccountLevel2 = 'Personnel Expenses' AND glDepartment NOT IN ('AD','GN') THEN 'Personnel Expenses'
          WHEN gmfsAccountLevel2 = 'Fixed Expenses' AND glDepartment NOT IN ('AD','GN') THEN 'Fixed Expenses'
          WHEN gmfsAccountLevel2 = 'Semi-Fixed Expenses' AND glDepartment NOT IN ('AD','GN') THEN 'Semi-Fixed Expenses'
          ELSE 'NA'
        END,
      cbAcctL4 = 
        CASE
          WHEN LEFT(gmfsAccount, 3) = '015' THEN 'Policy'
          WHEN LEFT(gmfsAccount, 3) = '067' THEN 'Policy' 
          ELSE 'NA'
        END;
-- exceptions
-- bs non gm parts
UPDATE dimaccount
SET cbdeptl2 = 'Body Shop'
WHERE glaccount IN ('147701','167701');
 
UPDATE dimaccount 
SET cbdeptl1 = 'Store'
  WHERE cbDeptL2 <> 'NA';
UPDATE dimaccount 
SET cbAcctl1 = 'Net Profit'
  WHERE cbAcctL2 <> 'NA';    
  
  
  
  
SELECT cbdeptL1,cbdeptl2,cbacctl1,cbacctl2, SUM(gttamt)
FROM dimaccount a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'
WHERE a.cbdeptL1 <> 'na'  
GROUP BY cbdeptL1,cbdeptl2,cbacctl1,cbacctl2
--ORDER BY cbdeptL1,cbdeptl2,cbdeptl3  

SELECT cbdeptL2,cbdeptL3,cbacctl1,cbacctl2,SUM(gttamt)
FROM dimaccount a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'
WHERE a.cbdeptL2 <> 'na'  
GROUP BY cbdeptL2,cbdeptL3,cbacctl1,cbacctl2

-- store 
SELECT cbdeptL1,cbacctL2,cbacctL3,SUM(gttamt)
FROM dimaccount a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'
WHERE a.cbdeptL1 <> 'na'  
  AND cbacctL2 <> 'na'
GROUP BY cbdeptL1,cbacctL2,cbacctL3

SELECT cbdeptL1,SUM(gttamt)
FROM dimaccount a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'
WHERE a.cbdeptL1 <> 'na'  
  AND cbacctL2 <> 'na'
GROUP BY cbdeptL1

-- store 
SELECT cbdeptL1,cbacctL1,SUM(gttamt)
FROM dimaccount a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'
WHERE a.cbdeptL1 <> 'na'  
  AND cbacctL2 <> 'na'
GROUP BY cbdeptL1,cbacctL1

-- store 
SELECT cbdeptL1,cbacctL1,cbacctL2,SUM(gttamt)
FROM dimaccount a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'
WHERE a.cbdeptL1 <> 'na'  
  AND cbacctL2 <> 'na'
GROUP BY cbdeptL1,cbacctL1,cbAcctL2

-- 11/4 
--11/6/12
back to WORK
use october
-- body shop
-- expenses ok, the way i am modelling semi-fixed IS problematic, 
-- semifixed total should include policy
-- sales/cogs off BY 13k (cb: 203446, qu: 190478
-- think it's the parts split
SELECT cbdeptL1, cbdeptL2,cbacctl1,cbacctl2,cbacctl3, cbacctl4, SUM(gttamt)
FROM dimaccount a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE a.cbdeptL2 = 'Body Shop'  
GROUP BY cbdeptL1, cbdeptL2,cbacctl1,cbacctl2,cbacctl3, cbacctl4
-- bingo, that IS it, the diff IS the parts split
SELECT glaccount, SUM(gttamt)
FROM dimaccount a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
--WHERE a.cbdeptL2 = 'Body Shop'  
WHERE a.glaccount IN ('147700', '167700')
GROUP BY glaccount
-- ok, cool, now modelling it, this IS WHERE i get hung up,
1. ADD a row to dimaccount
   but what about the gmfs heirarchy, i DO NOT want to fuck with that
   which makes me think it has to be broken out
   so what does that look LIKE
   
-- cartesian product of dept/accttype
SELECT cbdeptl1, cbdeptl2, cbdeptl3
FROM dimaccount
WHERE cbdeptl1 <> 'NA'
GROUP BY cbdeptl1, cbdeptl2, cbdeptl3  
-- some cleanup, which ok, IF this works out, the base construction will be different than above
SELECT *
FROM dimaccount
UPDATE dimAccount
  SET cbdeptl3 = 'NA'
WHERE cbdeptl1 ='store'
  AND cbdeptl2 = 'service'
  AND cbdeptl3 IS NULL 
  
SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
FROM dimaccount
WHERE cbdeptl1 <> 'NA'
GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4  

SELECT * FROM dimaccount WHERE cbacctl1 = 'NA' AND (cbacctl3 <> 'NA' OR cbacctl4 <> 'NA')
-- some more cleanup, these are uglier
  
-- this IS a weired one
-- accounts applied to NA depts
SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
FROM dimaccount
WHERE cbdeptl1 <> 'NA'
GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4  
  
-- cartesian product of dept/accttype
SELECT a.*, b.*, 1.0 AS split -- COUNT(*) 88 rows, DO they ALL make sense?
FROM (
  SELECT cbdeptl1, cbdeptl2, cbdeptl3
  FROM dimaccount
  WHERE cbdeptl1 <> 'NA'
  GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
(
  SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
  FROM dimaccount
  WHERE cbacctl1 <> 'NA'
  GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b  
ORDER BY cbdeptl1, cbdeptl2, cbdeptl3, cbacctl1, cbacctl2, cbacctl3, cbacctl4  
   
shit, with accounts only at the leaves of accounttypes? 

SELECT COUNT(*) FROM dimaccount WHERE cbdeptl1 <> 'NA' -- 1300

SELECT c.*, d.glaccount, d.gmfsaccount
  FROM (
  SELECT * -- COUNT(*) 88 rows, DO they ALL make sense?
  FROM (
    SELECT cbdeptl1, cbdeptl2, cbdeptl3
    FROM dimaccount
    WHERE cbdeptl1 <> 'NA'
    GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
  (
    SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
    FROM dimaccount
    WHERE cbacctl1 <> 'NA'
    GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b) c
LEFT JOIN dimaccount d ON c.cbdeptl1 = d.cbdeptl1
  and c.cbdeptl2 = d.cbdeptl2
  AND c.cbdeptl3 = d.cbdeptl3  
  AND d.cbacctl3 = d.cbacctl3
WHERE c.cbdeptl3 = 'PDQ'

-- SELECT COUNT(*) FROM (10768
SELECT c.*, d.glaccount, d.gmfsaccount
  FROM (
  SELECT * -- COUNT(*) 88 rows, DO they ALL make sense?
  FROM (
    SELECT cbdeptl1, cbdeptl2, cbdeptl3
    FROM dimaccount
    WHERE cbdeptl1 <> 'NA'
    GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
  (
    SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
    FROM dimaccount
    WHERE cbacctl1 <> 'NA'
    GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b) c
LEFT JOIN dimaccount d ON c.cbdeptl1 = d.cbdeptl1
  and c.cbdeptl2 = d.cbdeptl2
  AND c.cbdeptl3 = d.cbdeptl3  
  AND d.cbacctl1 = d.cbacctl1
  AND d.cbacctl2 = d.cbacctl2
  AND d.cbacctl3 = d.cbacctl3
  AND d.cbacctl4 = d.cbacctl4

-- hmmm, with amounts  
SELECT x.*, b.gttamt
FROM (
  SELECT c.*, d.glaccount, d.gmfsaccount
    FROM (
    SELECT * -- COUNT(*) 88 rows, DO they ALL make sense?
    FROM (
      SELECT cbdeptl1, cbdeptl2, cbdeptl3
      FROM dimaccount
      WHERE cbdeptl1 <> 'NA'
      GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
    (
      SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
      FROM dimaccount
      WHERE cbacctl1 <> 'NA'
      GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b) c
  LEFT JOIN dimaccount d ON c.cbdeptl1 = d.cbdeptl1
    and c.cbdeptl2 = d.cbdeptl2
    AND c.cbdeptl3 = d.cbdeptl3  
    AND d.cbacctl1 = d.cbacctl1
    AND d.cbacctl2 = d.cbacctl2
    AND d.cbacctl3 = d.cbacctl3
    AND d.cbacctl4 = d.cbacctl4) x  
INNER JOIN stgArkonaGLPTRNS b ON x.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'    
WHERE cbdeptl3 = 'PDQ'    


-- look at an individual account, how often IS it listed
-- AND it does NOT make any sense
SELECT c.*, d.glaccount, d.gmfsaccount 
  FROM (
  SELECT * -- COUNT(*) 88 rows, DO they ALL make sense?
  FROM (
    SELECT cbdeptl1, cbdeptl2, cbdeptl3
    FROM dimaccount
    WHERE cbdeptl1 <> 'NA'
    GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
  (
    SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
    FROM dimaccount
    WHERE cbacctl1 <> 'NA'
    GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b) c
LEFT JOIN dimaccount d ON c.cbdeptl1 = d.cbdeptl1
  and c.cbdeptl2 = d.cbdeptl2
  AND c.cbdeptl3 = d.cbdeptl3  
  AND d.cbacctl1 = d.cbacctl1
  AND d.cbacctl2 = d.cbacctl2
  AND d.cbacctl3 = d.cbacctl3
  AND d.cbacctl4 = d.cbacctl4
WHERE c.cbdeptl3 = 'pdq'
  AND glaccount = '12103'  
  
an account IS associated only with leaf level accttype?  
-- translate to greg speak AS a name associated only with leaf level

SELECT *
FROM stgArkonaGLPTRNS
WHERE gtdate = '10/05/2012'

-- 11/7
-- fuck around with cartesian product joined to dimaccount for a single dept
-- cartesian product of dept/accttype
SELECT a.*, b.* -- COUNT(*) 88 rows, DO they ALL make sense?
FROM (
  SELECT cbdeptl1, cbdeptl2, cbdeptl3
  FROM dimaccount
  WHERE cbdeptl1 <> 'NA'
  GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
(
  SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
  FROM dimaccount
  WHERE cbacctl1 <> 'NA'
  GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b  
WHERE cbdeptl2 = 'Body Shop'  
ORDER BY cbdeptl1, cbdeptl2, cbdeptl3, cbacctl1, cbacctl2, cbacctl3, cbacctl4

-- generic heirarchy TABLE
DROP TABLE zcb;
CREATE TABLE zCB (
  Name cichar(20),
  dl1 cichar(20),
  dl2 cichar(20),
  dl3 cichar(20),
  dl4 cichar(20),
  dl5 cichar(20),
  dl6 cichar(20),
  dl7 cichar(20),
  dl8 cichar(20),
  al1 cichar(20),
  al2 cichar(20),
  al3 cichar(20),
  al4 cichar(20),
  al5 cichar(20),
  al6 cichar(20),
  al7 cichar(20),
  al8 cichar(20),
  split double(2),
  glAccount cichar(10)) IN database;
INSERT INTO zCB
SELECT 'Cigar Box', a.*, b.*, CAST(NULL AS sql_varchar) AS glAccount
SELECT *
FROM (
  SELECT cbdeptl1, cbdeptl2, cbdeptl3
  FROM dimaccount
  WHERE cbdeptl1 <> 'NA'
  GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
(
  SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
  FROM dimaccount
  WHERE cbacctl1 <> 'NA'
  GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b  
WHERE cbdeptl2 = 'Body Shop';    

SELECT * FROM zcb;

UPDATE zcb
  SET glAccount = x.glaccount

-- this IS looking promising
-- observe, for accounts with subaccounts, no transactions are made against the parent account ? (header)  
-- basic heirarchy
SELECT * FROM zcb WHERE glaccount IS NULL 

SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl4 <> 'na'
-- detail up
-- al4 <> na
INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy'

  
-- semi 
SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na'

INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na'

-- fixed  
SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na'
 
INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na' 
  
-- personnel  
SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na'
INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na' 
 
  
-- ok, lets play around a bit, looks ok  
SELECT a.dl2, a.al3, sum(b.gttamt)
FROM zcb a
left JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
GROUP BY a.dl2, a.al3  

-- sales
-- lets DO it with account resolution FROM gmfs/gldept/etc instead of the existing cbdept/cbacct categorizations IN dimAccount
-- first ADD more levels for dept & acct, ADD split COLUMN
-- DO the columns later, ADD split now
ALTER TABLE zcb
ADD COLUMN split double(2);
UPDATE zcb SET split = 1;

-- have to deal with exceptions such AS:
UPDATE dimaccount
SET cbdeptl2 = 'Body Shop'
WHERE glaccount IN ('147701','167701');

SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na'
INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'Sales'
  AND al4 = 'na' 
  
INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'COGS'
  AND al4 = 'na'   
  
-- partssplit  
DELETE FROM zcb WHERE split <> 1
INSERT INTO zcb
SELECT distinct a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '147700', .5
FROM zcb a
WHERE dl2 = 'Body Shop'
  AND al3 = 'Sales'
  AND al4 = 'na' 
  
INSERT INTO zcb
SELECT distinct a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '167700', .5
FROM zcb a
WHERE dl2 = 'Body Shop'
  AND al3 = 'COGS'
  AND al4 = 'na'   

  
-- ok, lets play around a bit, looks ok  
SELECT a.dl2, a.al3, sum(b.gttamt*split)
FROM zcb a
left JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
GROUP BY a.dl2, a.al3    

SELECT a.*, b.gttamt
FROM zcb a
left JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE split <> 1
 
-- ok ADD service 
INSERT INTO zCB
SELECT 'Cigar Box', a.*, b.*, CAST(NULL AS sql_varchar) AS glAccount, 1
--SELECT *
FROM (
  SELECT cbdeptl1, cbdeptl2, cbdeptl3
  FROM dimaccount
  WHERE cbdeptl1 <> 'NA'
  GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
(
  SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
  FROM dimaccount
  WHERE cbacctl1 <> 'NA'
  GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b  
WHERE cbdeptl2 = 'Service';  

-- al4 semi fixed policy
SELECT * FROM dimaccount WHERE cbdeptl2 = 'Service' and cbdeptl3 = 'Detail' AND cbacctl4 <> 'na'
--DELETE FROM zcb WHERE dl2 = 'service' AND glaccount IS NOT null
-- detail up
-- al4 <> na
INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Service' and cbdeptl3 = 'Detail' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy';
INSERT INTO zcb  
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Service' and cbdeptl3 = 'Car Wash' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy'; 
INSERT INTO zcb   
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Service' and cbdeptl3 = 'PDQ' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy'; 
INSERT INTO zcb  
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Service' and cbdeptl3 = 'Main Shop' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy';  

-- semi-fixed 
--DELETE FROM zcb WHERE dl2 = 'service' AND glaccount IS NOT NULL; 
INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Detail' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na';
INSERT INTO zcb  
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Car Wash' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na'; 
INSERT INTO zcb   
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'PDQ' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na'; 
INSERT INTO zcb  
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Main Shop' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na';  
  
-- personnel  
  
INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Detail' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na';
INSERT INTO zcb  
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Car Wash' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na'; 
INSERT INTO zcb   
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'PDQ' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na'; 
INSERT INTO zcb  
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Main Shop' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na';    

/* 
-- wtf, personnel IS wrong main shop $100 lo, detail 2k hi, car wash 2k lo
-- detail/car wash issue, cigar box is wrong, acct 12324 (Kevin Hanson) being applied to car wash instead of detail
SELECT a.dl3, b.gldepartment, a.glaccount, gmfsaccount, cbdeptl3
FROM zcb a
LEFT JOIN dimaccount b ON a.glaccount = b.glaccount
WHERE dl2 = 'service'
  AND al3 = 'Personnel Expenses'
  AND a.glaccount IS NOT NULL 
ORDER BY 
  case
    when a.glaccount is null then a.glaccount
    ELSE dl3 
  END, a.glaccount 
 
  
SELECT dl3, al3, gtacct, sum(gttamt)
--SELECT SUM(gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE dl3 = 'Detail'
  AND al3 = 'Personnel Expenses'
GROUP BY dl3, al3, gtacct  
HAVING SUM(b.gttamt) > 0
   
SELECT * FROM stgArkonaGLPTRNS WHERE gtacct = '12324' AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
SELECT * FROM stgArkonaGLPMAST WHERE gmacct = '12324'
*/

-- sales  
INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Detail' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'Sales'
  AND al4 = 'na';
INSERT INTO zcb  
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Car Wash' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'Sales'
  AND al4 = 'na'; 
INSERT INTO zcb   
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'PDQ' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'Sales'
  AND al4 = 'na'; 
INSERT INTO zcb  
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Main Shop' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Sales'
  AND al4 = 'na';  
  
-- cogs
INSERT INTO zcb
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Detail' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'COGS'
  AND al4 = 'na';
INSERT INTO zcb  
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Car Wash' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'COGS'
  AND al4 = 'na'; 
INSERT INTO zcb   
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'PDQ' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'COGS'
  AND al4 = 'na'; 
INSERT INTO zcb  
SELECT a.name, a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Main Shop' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'COGS'
  AND al4 = 'na';    
  
      

-- looks LIKE i am missing a row LIKE
cigar box     store          
SELECT * FROM zcb WHERE glaccount IS NULL 

SELECT dl2, al2,sum(b.gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
-- WHERE dl3 <> 'na'
--  AND al3 = 'sales'
GROUP BY dl2, al2
HAVING SUM(b.gttamt) <> 0

