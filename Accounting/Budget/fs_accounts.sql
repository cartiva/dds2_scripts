

select * FROM gmfs_accounts WHERE page = 2

select * FROM gmfs_accounts where gm_Account LIKE '400%'


select * 
FROM ext_arkona_sypffxmst
WHERE the_year = 2015
  AND page = 16
ORDER BY page, line,col, start_position

select * 
FROM ext_arkona_sypffxmst
WHERE factory_Account LIKE '400%'


select * 
FROM ext_arkona_sypffxmst a
LEFT JOIN gmfs_accounts b on a.factory_account = b.gm_account
WHERE a.the_year = 2015
  AND a.page = 5 -- AND line = 2
ORDER BY a.page, a.line, a.col, a.start_position


DROP TABLE #fs;
select a.*, b.storecode, b.gl_account, b.description, b.account_type, b.department, c.amount 
INTO #fs
FROM ext_arkona_sypffxmst a
LEFT JOIN gmfs_accounts b on a.factory_account = b.gm_account
LEFT JOIN (
  SELECT m.gtacct, SUM(m.gttamt) AS amount
  FROM stgArkonaGlptrns m
  INNER JOIN gmfs_accounts n on m.gtacct = n.gl_account
  WHERE m.gtdate BETWEEN '05/01/2016' AND '05/31/2016'
  GROUP BY m.gtacct) c on b.gl_account = c.gtacct
WHERE a.the_year = 2015
--  AND a.page = 5
  AND a.factory_account NOT LIKE '%<%'
  AND a.factory_account NOT LIKE '%&%'
  AND a.factory_account NOT LIKE '%*%' 
  AND c.amount <> 0;  
  
SELECT *
FROM #fs  
WHERE page = 17
  AND storecode = 'RY1'
--  AND line IN (10,20)
--ORDER BY gl_Account  
ORDER BY page, line, col, start_position

SELECT page, description, line, col, start_position, SUM(amount)
FROM #fs  
WHERE page = 17
  AND storecode = 'RY1'
GROUP BY page, description, line, col, start_position 
ORDER BY page, line, col

-- no rows (currently populated IN #fs) with nultiple start_positions
SELECT page, line, col
FROM (
  SELECT page,line,col,start_position
  FROM (
    SELECT page, description, line, col, start_position, SUM(amount)
    FROM #fs  
    WHERE page = 16
      AND storecode = 'RY1'
    GROUP BY page, description, line, col, start_position) x
  GROUP BY page,line,col,start_position) y
GROUP BY page,  line, col 
HAVING COUNT(*) > 1    

SELECT * FROM stgarkonaglptrns WHERE gtacct = '185001'
AND gtdate BETWEEN '05/01/2016' AND '05/31/2016'

select * FROM #fs WHERE gl_account IN ('185000','185001','185002','185003','185100','185101','185102')



-- DROP TABLE #fs_all;
select a.*, b.storecode, b.gl_account, b.description, b.account_type, b.department, c.amount 
INTO #fs_all
FROM ext_arkona_sypffxmst a
LEFT JOIN gmfs_accounts b on a.factory_account = b.gm_account
LEFT JOIN (
  SELECT m.gtacct, SUM(m.gttamt) AS amount
  FROM stgArkonaGlptrns m
  INNER JOIN gmfs_accounts n on m.gtacct = n.gl_account
  WHERE m.gtdate BETWEEN '05/01/2016' AND '05/31/2016'
  GROUP BY m.gtacct) c on b.gl_account = c.gtacct
WHERE a.the_year = 2015
  AND b.storecode = 'RY1'
ORDER BY a.page,a.line,a.col,a.start_position

-- p15 (5k) commercial
-- this IS just totals FROM p 5-14 line 43
SELECT * 
FROM ext_arkona_sypffxmst a
LEFT JOIN gmfs_accounts b on a.factory_account = b.gm_account
WHERE a.page = 15
  AND a.the_year = 2015
ORDER BY a.line, a.col  





-- new car net sales: 5,810,879.87
SELECT -SUM(amount) from #fs_all WHERE page BETWEEN 5 AND 14 AND col = 1 
-- new car cogs: 5,728,203.77
SELECT SUM(amount) from #fs_all WHERE page BETWEEN 5 AND 14 AND col = 3 
-- new car gross profit/income: 82,676.10
SELECT -SUM(amount) from #fs_all WHERE page BETWEEN 5 AND 14

-- used car net sales: 3,089,920.91
select -SUM(amount) FROM #fs_all WHERE page = 16 and line between 1 and 13 AND col = 1
-- used car cogs 2867878.25
select -SUM(amount) FROM #fs_all WHERE page = 16 and line between 1 and 13 AND col = 3
-- used gross profit/income: 222042.66
select -SUM(amount) FROM #fs_all WHERE page = 16 and line between 1 and 13 

-- f/i net sales: 334365.31
select -SUM(amount) FROM #fs_all WHERE page = 17 and line between 1 and 21 AND col = 1
-- f/i cogs: : -218092.55
select -SUM(amount) FROM #fs_all WHERE page = 17 and line between 1 and 21 AND col = 3
-- f/i gross profit/income: 116272.76
select -SUM(amount) FROM #fs_all WHERE page = 17 and line between 1 and 21 

-- variable net sales
select -sum(amount) 
FROM #fs_all 
WHERE (
  (page BETWEEN 5 AND 14 AND col = 1 )
  OR
  (page = 16 and line between 1 and 13 AND col = 1)
  OR
  (page = 17 and line between 1 and 21 AND col = 1))
-- variable gross  
select -sum(amount) 
FROM #fs_all 
WHERE (
  (page BETWEEN 5 AND 14)
  OR
  (page = 16 and line between 1 and 13)
  OR
  (page = 17 and line between 1 and 21))
  
-- fixed sales
SELECT line, col, SUM(amount) 
FROM #fs_all
WHERE page = 16 
  AND line BETWEEN 21 AND 33
GROUP BY line, col  

-- line 27 sales are low
--fs: 114,330  qu: 112,008
-- check the doc
-- doc has problems
-- the real issue IS  #fs_all IS missing account 146424
SELECT *
FROM #fs_all
WHERE page = 16 
  AND line = 27
  AND col = 1
ORDER BY department, gl_account  
  
select col, SUM(amount)
FROM #fs_all
WHERE page = 16 
  AND line = 27  
GROUP BY col


SELECT gl_account, description, department, amount
FROM #fs_all
WHERE page = 16 
  AND line = 27
  AND col = 1
ORDER BY gl_account  

SELECT SUM(amount)
FROM #fs_all
WHERE page = 16 
  AND line = 27
  AND col = 1


select *
FROM stgarkonaffpxrefdta
WHERE fxfact = '462'
  AND fxcyy = 2016
  AND fxconsol = ''
  
select gtacct, SUM(gttamt)
FROM stgarkonaglptrns  
WHERE gtdate BETWEEN '05/01/2016' AND '05/31/2016'
  AND gtacct IN (
    select fxgact
    FROM stgarkonaffpxrefdta
    WHERE fxfact = '462'
      AND fxcyy = 2016
      AND fxconsol = '')
GROUP BY gtacct  
















/*
parts split page 4
-- this doesn't look right
SELECT *
FROM #fs_all 
WHERE page = 4

SELECT storecode, glaccount, gmcoa, page, line
FROM zcb
WHERE split <> 1
GROUP BY storecode, glaccount, gmcoa, page, line

SELECT *
FROM #fs_all
WHERE gl_account IN (
  SELECT glaccount
  FROM zcb
  WHERE split <> 1)
  
SELECT * FROM gmfs_accounts WHERE gm_account LIKE '477%' OR gm_account LIKE '467%'

-- splits
select *
FROM stgarkonaffpxrefdta
WHERE fxcyy = 2016
  AND fxfp01 = .5
 

-- FROM accounting/financial statement/sypffxmst.sql 
INSERT INTO gmfs_accounts 
SELECT CASE WHEN a.fxconsol = '2' THEN 'RY2' ELSE 'RY1' END,
  a.fxgact, fxfact, b.page, b.line, b.col, d.gmdesc, d.gmtype, d.gmdept, d.gmdboa
FROM stgArkonaFFPXREFDTA a 
LEFT JOIN ext_arkona_sypffxmst b on a.fxfact = b.factory_account
LEFT JOIN stgArkonaGLPMAST d on a.fxgact = d.gmacct
  AND d.gmyear = 2015
WHERE a.fxcyy = 2015 
  AND a.fxconsol <> '3'
  AND d.gmactive = 'Y'
  AND fxcode = 'GM'
  AND fxfact <> '331A';  
  
*/