SELECT *
FROM zcb
WHERE name = 'gmfs gross'
  AND dl2 = 'ry1's

SELECT dl3,dl4,dl5,dl6,dl7,dl8,al2, al3, page, line, gmcoa
FROM zcb
WHERE name = 'gmfs gross'
  AND dl2 = 'ry1'
  AND al2 = 'cogs'
GROUP BY dl3,dl4,dl5,dl6,dl7,dl8,al2, al3, page, line, gmcoa
ORDER BY dl3,dl4,dl5,dl6,dl7,dl8, line  


SELECT '  ' + trim(CAST(page AS sql_char)) + '::' + trim(CAST(line AS sql_char)) + '::' + trim(gmcoa) + '::' + trim(al3)
FROM zcb
WHERE name = 'gmfs gross'
  AND dl4 = 'f&i'
  AND dl5 = 'used'
  AND al2 = 'sales'
GROUP BY dl3,dl4,dl5,dl6,dl7,dl8,al2, al3, page, line, gmcoa
ORDER BY dl3,dl4,dl5,dl6,dl7,dl8, line  


SELECT glaccount, gmcoa, page, line
FROM zcb
WHERE name = 'gmfs gross'
GROUP BY glaccount, gmcoa, page, line

SELECT 
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt, 0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
--  AND dl3 = 'variable'
GROUP BY dl4


SELECT dl2,name, SUM(coalesce(gttamt, 0))
FROM (
  SELECT *
  FROM zcb
  WHERE name = 'gmfs gross'
  UNION 
  SELECT *
  FROM zcb
  WHERE name = 'gmfs expenses') a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10  
GROUP BY dl2, name 
/********************       jeri      ****************************************/
-- ry1 expenses $400 lo
SELECT dl2,name, al2, SUM(coalesce(gttamt, 0))
FROM (
  SELECT *
  FROM zcb
  WHERE name = 'gmfs gross'
  UNION 
  SELECT *
  FROM zcb
  WHERE name = 'gmfs expenses') a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10  
WHERE dl2 = 'ry1'  
GROUP BY dl2, name, al2
-- fixed off BY 90
SELECT dl2,name, al3, SUM(coalesce(gttamt, 0)), line
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10  
WHERE dl2 = 'ry1' 
  AND al2 = 'fixed expenses'
  AND name IN ('gmfs gross','gmfs expenses')
GROUP BY dl2, name, al3, line
ORDER BY line
-- diff IS line 42
SELECT dl2, dl3, name, al3, SUM(coalesce(gttamt, 0)), line
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10  
WHERE dl2 = 'ry1' 
  AND al2 = 'fixed expenses'
  AND name IN ('gmfs gross','gmfs expenses')
  AND line = 42
GROUP BY dl2, dl3, name, al3, line
ORDER BY line
-- diff IS IN fixed
-- diff IS mechanical, query agrees with doc, statemnt IS $90 higher
SELECT dl2, dl3, name, al3, dl4, glaccount, SUM(coalesce(gttamt, 0)), line
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10  
WHERE dl2 = 'ry1' 
  AND al2 = 'fixed expenses'
  AND name IN ('gmfs gross','gmfs expenses')
  AND line = 42
  AND dl3 = 'fixed'
GROUP BY dl2, dl3, dl4, name, al3, line, glaccount
ORDER BY line
-- personnel of BY 280
-- which dept
SELECT dl2, dl3, name,  SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10  
WHERE dl2 = 'ry1' 
  AND al2 = 'personnel expenses'
  AND name IN ('gmfs gross','gmfs expenses')
GROUP BY dl2, dl3, name
-- personnel of BY 280
-- which dept
-- diff IS IN fixed
-- mech & parts are lo
-- shit these agree with the doc too, fs IS diff
-- talk to jeri about these
SELECT dl2, dl3, dl4, name,  SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10  
WHERE dl2 = 'ry1' 
  AND al2 = 'personnel expenses'
  AND name IN ('gmfs gross','gmfs expenses')
GROUP BY dl2, dl3, dl4, name

/********************       jeri      ****************************************/


SELECT dl2, SUM(coalesce(gttamt, 0)) AS "Oper Profit",
  SUM(CASE WHEN name = 'gmfs gross' AND al2 = 'sales' THEN coalesce(gttamt,0) ELSE 0 END) AS Sales,
  SUM(CASE WHEN name = 'gmfs gross' AND al2 = 'cogs' THEN coalesce(gttamt,0) ELSE 0 END) AS COGS,
  SUM(CASE WHEN name = 'gmfs gross'  THEN coalesce(gttamt,0) ELSE 0 END) AS "Gross Profit",
  SUM(CASE WHEN name = 'gmfs expenses' AND al2 = 'variable selling expenses' THEN coalesce(gttamt,0) ELSE 0 END) AS "Var Exp",
  SUM(CASE WHEN name = 'gmfs expenses' AND al2 = 'personnel expenses' THEN coalesce(gttamt,0) ELSE 0 END) AS "Personnel",
  SUM(CASE WHEN name = 'gmfs expenses' AND al2 = 'semi-fixed expenses' THEN coalesce(gttamt,0) ELSE 0 END) AS "Semi Fixed",
  SUM(CASE WHEN name = 'gmfs expenses' AND al2 = 'fixed expenses' THEN coalesce(gttamt,0) ELSE 0 END) AS "Fixed",
  SUM(CASE WHEN name = 'gmfs expenses'  THEN coalesce(gttamt,0) ELSE 0 END) AS "Total Expenses"
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10  
WHERE name in ('gmfs gross','gmfs expenses')  
GROUP BY dl2

-- need adds & deducts
-- page 2 line 62
-- page 3 lines 63 - 68

-- bingo  
SELECT fxconsol, SUM(gttamt)
FROM stgarkonaffpxrefdta a
INNER JOIN stgArkonaGLPTRNS b ON fxgact = gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE fxfact LIKE '9__%'
  AND fxcyy = 2012
  AND (
    fxconsol = ''
    OR fxconsol = '3'
    OR (fxconsol = '2' AND fxcode = 'GM'))
GROUP BY fxconsol



<body>
<form action='cigarbox.php' method='post'>
<?php
$StoreList = array("RY1", "RY2", "RY3");
$AreaList = array("Variable", "Service", "Body Shop", "Parts", "NCR");
$DepartmentList = array("Used Vehicles", "New Vehicles", "Main Shop", "PDQ", "Detail", "Car Wash");
if (isset($_POST['Department'])) {
  $Department = $_POST['Department'];
  if (in_array($Department, $StoreList)) {
    $Level = 1;
  }
  if (in_array($Department, $AreaList)) {
    $Level = 2;
  }
  if (in_array($Department, $DepartmentList)) {
    $Level = 3;
  }
}  
else {
  $Department = 'RY1';
  $Level = 1;
}
?>
<div id='container'>
<table class='bordered cigarbox'>
 <caption>
   <select name='Department' onchange='this.form.submit();'>
    <option value='RY1'>GM Store</option>
    <option value='Variable'>Variable</option>
    <option value='Service'>Service</option>
    <option value='Body Shop'>Body Shop</option>
    <option value='Parts'>Parts</option>
    <option value='NCR'>NCR</option>
    <option value='Used Vehicles'>Used</option>
    <option value='New Vehicles'>New</option>
    <option value='Main Shop'>Main Shop</option>
    <option value='PDQ'>PDQ</option>
    <option value='Detail'>Detail</option>
    <option value='Car Wash'>Car Wash</option>
   </select>
 </caption>
 <tr>
  <th><?php echo $Department?></th>
  <th>Oct</th>
  <th>2012</th>
  <th>Oct</th>
  <th>2011</th>
  <th>Oct</th>
  <th>2010</th>
  <th>MAP</th>
  <th>Par</th>
  <th>Target</th>
 </tr>
<?php
$CategoryCount = 0;
$EndNode = 0;
$CigarBox = array(array(array()), array(array()), array(array()));
$EndNode = 0;
$Node = 0;
$vSeriesConn = ads_connect("DataDirectory=\\\\ads.cartiva.com:6363\\advantage\\dds\\dds.add;TrimTrailingSpaces=True", "adssys", "cartiva");
$SQL = "select * from (execute procedure phpGetCigarBox('RY1', ".$Level.", '".$Department."', '10/1/2012', '10/31/2012', 'sum', 1)) a order by Level1, Level2";
$Result = ads_exec($vSeriesConn, $SQL);
$Row = 0;
while (ads_fetch_row($Result)) {
  ++$Row;
  $CigarBox[0][$Row]["Category"] = ads_result($Result, "Category");
  $CigarBox[0][$Row]["Line"] = ads_result($Result, "Line");
  $CigarBox[0][$Row]["Amount"] = ads_result($Result, "Amount");
  $CigarBox[0][$Row]["Percent"] = ads_result($Result, "Percent");
  $CigarBox[0][$Row]["MAP"] = ads_result($Result, "MAP");
  $CigarBox[0][$Row]["Par"] = ads_result($Result, "Par");
  $CigarBox[0][$Row]["Target"] = ads_result($Result, "Target");
  $CigarBox[0][$Row]["Color"] = ads_result($Result, "Color");
  if ($CigarBox[0][$Row]["Line"] == 'Line') {
    ++$CategoryCount;
    $CigarBox[0][$Row]["css"] = 'style="padding-left: 19px"';
    $CigarBox[0][$Row]["Node"] = 'node-'.$CategoryCount;
    $CigarBox[0][$Row]["Child"] = '';
    $EndNode = 0;
    $CigarBox[0][$Row]["Title"] = $CigarBox[0][$Row]["Category"];
  }
  else {
    ++$EndNode;
    $CigarBox[0][$Row]["css"] = '';
    $CigarBox[0][$Row]["Node"] = 'node-'.$CategoryCount.'-'.$EndNode;
    $CigarBox[0][$Row]["Child"] = 'child-of-node-'.$CategoryCount;
    $CigarBox[0][$Row]["Title"] = $CigarBox[0][$Row]["Line"];
//    $Worked = '';
//    $Proficiency = '';
  }
}
$SQL = "select * from (execute procedure phpGetCigarBox('RY1', ".$Level.", '".$Department."', '10/1/2011', '10/31/2011', 'sum', 1)) a order by Level1, Level2";
$Result = ads_exec($vSeriesConn, $SQL);
$Row = 0;
while (ads_fetch_row($Result)) {
  ++$Row;
  $CigarBox[1][$Row]["Category"] = ads_result($Result, "Category");
  $CigarBox[1][$Row]["Line"] = ads_result($Result, "Line");
  $CigarBox[1][$Row]["Amount"] = ads_result($Result, "Amount");
  $CigarBox[1][$Row]["Percent"] = ads_result($Result, "Percent");
  $CigarBox[1][$Row]["MAP"] = ads_result($Result, "MAP");
  $CigarBox[1][$Row]["Par"] = ads_result($Result, "Par");
  $CigarBox[1][$Row]["Target"] = ads_result($Result, "Target");
  $CigarBox[1][$Row]["Color"] = ads_result($Result, "Color");
}
$SQL = "select * from (execute procedure phpGetCigarBox('RY1', ".$Level.",'".$Department."', '10/1/2010', '10/31/2010', 'sum', 1)) a order by Level1, Level2";
$Result = ads_exec($vSeriesConn, $SQL);
$Row = 0;
while (ads_fetch_row($Result)) {
  ++$Row;
  $CigarBox[2][$Row]["Category"] = ads_result($Result, "Category");
  $CigarBox[2][$Row]["Line"] = ads_result($Result, "Line");
  $CigarBox[2][$Row]["Amount"] = ads_result($Result, "Amount");
  $CigarBox[2][$Row]["Percent"] = ads_result($Result, "Percent");
  $CigarBox[2][$Row]["MAP"] = ads_result($Result, "MAP");
  $CigarBox[2][$Row]["Par"] = ads_result($Result, "Par");
  $CigarBox[2][$Row]["Target"] = ads_result($Result, "Target");
  $CigarBox[2][$Row]["Color"] = ads_result($Result, "Color");
}
for ($Row = 1; $Row < sizeof($CigarBox[0]); ++$Row) {
  $Node = $CigarBox[0][$Row]["Node"];
  $Child = $CigarBox[0][$Row]["Child"];
  $css = $CigarBox[0][$Row]["css"];
  $Color = $CigarBox[0][$Row]["Color"];
  echo " <tr id='$Node' class='$Child'>\n";
  echo "  <td class='column-title' ".$css." ".$Color.">".$CigarBox[0][$Row]['Title']."</td>\n";
  echo "  <td class='column-amount'>".number_format($CigarBox[0][$Row]['Amount'])."</td>\n";
  echo "  <td class='column-percent cb".$CigarBox[0][$Row]['Color']."'>".$CigarBox[0][$Row]['Percent']."%</td>\n";
  echo "  <td class='column-amount'>".number_format($CigarBox[1][$Row]['Amount'])."</td>\n";
  echo "  <td class='column-percent cb".$CigarBox[1][$Row]['Color']."'>".$CigarBox[1][$Row]['Percent']."%</td>\n";
  echo "  <td class='column-amount'>".number_format($CigarBox[2][$Row]['Amount'])."</td>\n";
  echo "  <td class='column-percent cb".$CigarBox[2][$Row]['Color']."'>".$CigarBox[2][$Row]['Percent']."%</td>\n";
  echo "  <td class='column-map'>".$CigarBox[0][$Row]['MAP']."</td>\n";
  echo "  <td class='column-par'>".$CigarBox[0][$Row]['Par']."</td>\n";
  echo "  <td class='column-target'>".$CigarBox[0][$Row]['Target']."</td>\n";
  echo " </tr>\n";
}
?>
</table>
</div>
</body>
</html>
<script type="text/javascript">
</script>