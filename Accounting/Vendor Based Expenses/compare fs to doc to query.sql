
-- page 4 fixed-mechanical
SELECT a.al2, a.al3, SUM(coalesce(b.gttamt,0)) AS Mech, MIN(line) AS line 
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '09/01/2014' AND '09/30/2014'
WHERE name = 'gmfs expenses'
  AND dl2 = 'ry1'
  AND dl3 = 'fixed'
  AND dl4 = 'mechanical'
GROUP BY a.dl2, dl3, a.al2, a.al3
ORDER BY MIN(line)

-- mechanical, body, parts IN one query
select f.*, g.body, h.parts
FROM (
  SELECT a.al2, a.al3, SUM(coalesce(b.gttamt,0)) AS Mech, MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2014' AND '09/30/2014'
  WHERE name = 'gmfs expenses'
    AND dl2 = 'ry1'
    AND dl3 = 'fixed'
    AND dl4 = 'mechanical'
    AND line <> 999
  GROUP BY a.dl2, dl3, a.al2, a.al3) f
LEFT JOIN (
  SELECT a.al2, a.al3, SUM(coalesce(b.gttamt,0)) AS Body, MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2014' AND '09/30/2014'
  WHERE name = 'gmfs expenses'
    AND dl2 = 'ry1'
    AND dl3 = 'fixed'
    AND dl4 = 'body shop'
    AND line <> 999
  GROUP BY a.dl2, dl3, a.al2, a.al3) g on f.line = g.line
LEFT JOIN (
  SELECT a.al2, a.al3, SUM(coalesce(b.gttamt,0)) AS Parts, MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2014' AND '09/30/2014'
  WHERE name = 'gmfs expenses'
    AND dl2 = 'ry1'
    AND dl3 = 'fixed'
    AND dl4 = 'parts'
    AND line <> 999
  GROUP BY a.dl2, dl3, a.al2, a.al3) h on f.line = h.line  
ORDER BY f.line  

discrepancies BETWEEN query AND FS
use doc RY1 SERVICE DEPT/JERI to pick up accounts
semi-fixed
 line 11: Other Salaries & Wages
 line 12: Absentee Compensation
 line 18: Company Vehicle Expense
 line 19: Office Supplies & Expenses
 line 31: telephone
fixed
  line 42: Amortization-Leaseholds 
  
FROM doc
  line 11: 12303, 12304, 12304A, 12324, 12327
missing 12304A  
SELECT  *
FROM zcb
WHERE name = 'gmfs expenses'
  AND dl2 = 'ry1'
  AND dl3 = 'fixed'
  AND dl4 = 'mechanical'
  AND line = 11  
  
SELECT glaccount, SUM(coalesce(b.gttamt,0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '09/01/2014' AND '09/30/2014'
WHERE name = 'gmfs expenses'
  AND dl2 = 'ry1'
  AND dl3 = 'fixed'
  AND dl4 = 'mechanical'
  AND line = 11
GROUP BY glaccount

SELECT *
FROM zcb
WHERE glaccount IN ('12303','12304','12324','12327')
  AND name = 'gmfs expenses'
  
SELECT *
FROM stgArkonaGLPMAST  
WHERE gmacct = '12304A'


-- line 12
FROM doc
  line 12: 12403, 12404, 12424, 12427
missing 12427  
SELECT  *
FROM zcb
WHERE name = 'gmfs expenses'
  AND dl2 = 'ry1'
  AND dl3 = 'fixed'
  AND dl4 = 'mechanical'
  AND line = 12
  
-- line 18  
FROM doc, 2 lines, Company Veh Exp AND Courtesy Car Exp
  15104, 15104D, 15104F, 15104I, 15104M, 15104P, 15104R
  15204
SELECT *
FROM zcb
WHERE name = 'gmfs expenses'
  AND dl2 = 'ry1'
  AND dl3 = 'fixed'
  AND dl4 = 'mechanical'
  AND line = 18

/*
ok, above IS about synching zcb with statement, will get to that
lets look at some vendor stuff
*/
line 27 outside services

SELECT  *
FROM zcb
WHERE name = 'gmfs expenses'
  AND dl2 = 'ry1'
  AND dl3 = 'fixed'
  AND dl4 = 'mechanical'
  AND line = 27
  
SELECT gtdtyp, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gtref#, gtvnd#, gtdesc, gttamt
FROM stgArkonaGLPTRNS a
WHERE a.gtacct IN ('16903','16904','16924','16927')  
  AND a.gtdate BETWEEN '09/01/2014' AND '09/30/2014'
  AND gtjrnl <> 'pay'
  
-- accounts with activity IN month missing FROM zcb 
SELECT fxgact, fxfact, SUM(gttamt)
--INTO #wtf
FROM (
  SELECT fxgact, fxfact
  FROM stgArkonaFFPXREFDTA a
  WHERE fxcyy = 2014
    AND NOT EXISTS (
      SELECT 1
      FROM zcb
      WHERE glaccount = a.fxgact)
    AND fxconsol = '' ) c 
LEFT JOIN stgArkonaGLPTRNS d on c.fxgact = d.gtacct
WHERE d.gtdate BETWEEN '09/01/2014' AND '09/30/2014' 
GROUP BY fxgact, fxfact     