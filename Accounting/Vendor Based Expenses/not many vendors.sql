SELECT *
FROM zcb

SELECT DISTINCT name FROM zcb


SELECT b.glAccount, b.al4, b.al5, SUM(a.gttamt) AS amount
FROM stgArkonaGLPTRNS a
INNER JOIN (
  SELECT glAccount, al4, al5
  FROM zcb
  WHERE name = 'cb2' -- cigar box
    AND dl2 = 'RY1' -- GM
    AND al4 IN ('fixed expenses','semi-fixed expenses')
    AND dl5 = 'PARTS') b on a.gtacct = b.glAccount
WHERE a.gtdate BETWEEN '10/01/2014' AND '10/31/2014'  
GROUP BY b.glAccount, b.al4, b.al5

DROP TABLE #wtf;
SELECT b.dl2, b.dl5, b.glAccount, b.al4, b.al5, a.gttamt, a.gtvnd#, a.gtctl#
INTO #wtf
FROM stgArkonaGLPTRNS a
INNER JOIN (
  SELECT dl2, dl5, glAccount, al4, al5
  FROM zcb
  WHERE name = 'cb2' -- cigar box
    AND dl2 = 'RY1' -- GM
    AND al4 IN ('fixed expenses','semi-fixed expenses')
    AND dl5 = 'PARTS') b on a.gtacct = b.glAccount
WHERE a.gtdate BETWEEN '10/01/2014' AND '10/31/2014'  

-- was worried that joining glpcust to glptrns.gtvnd# returned damned few vendors
-- per conv with jeri, often the glptrns.gtctl# IS the vendor #
-- so ADD a JOIN AND coalesce the names
-- which looks pretty good, need some additional grouping to take care of shit
-- LIKE policy WHERE there IS no vendors, so i don't need the gtctl# detail
SELECT b.dl5,  b.glAccount, b.al4, b.al5, b.gtvnd#, gtctl#, c.gcsnam, b.amount, e.gcsnam,
  coalesce(c.gcsnam, e.gcsnam)
FROM (
  SELECT dl2, dl5, glAccount, al4, al5, gtvnd#, gtctl#, SUM(gttamt) AS amount
  FROM (
    SELECT *
    FROM #wtf) a
  GROUP BY dl2, dl5, glAccount, al4, al5, gtvnd#, gtctl#) b
LEFT JOIN stgArkonaGLPCUST c on b.gtvnd# = c.gcvnd#  
  AND c.gcvnd# <> ''
  AND b.dl2 = c.gcco#
LEFT JOIN stgArkonaGLPMAST d on b.glaccount = d.gmacct
  AND d.gmyear = 2014  
LEFT JOIN stgArkonaGLPCUST e on b.gtctl# = e.gcvnd#
  AND b.dl2 = e.gcco#  
  
  
-- was worried that joining glpcust to glptrns.gtvnd# returned damned few vendors
-- per conv with jeri, often the glptrns.gtctl# IS the vendor #
-- so ADD a JOIN AND coalesce the names
-- which looks pretty good, need some additional grouping to take care of shit
-- LIKE policy WHERE there IS no vendors, so i don't need the gtctl# detail
SELECT dl5, glaccount, al4, al5, vendor, SUM(amount) AS amount
-- looks pretty good
FROM (
  SELECT b.dl5,  b.glAccount, b.al4, b.al5, b.amount, coalesce(c.gcsnam, e.gcsnam) AS vendor
  FROM (
    SELECT dl2, dl5, glAccount, al4, al5, gtvnd#, gtctl#, SUM(gttamt) AS amount
    FROM (
      SELECT *
      FROM #wtf) a
    GROUP BY dl2, dl5, glAccount, al4, al5, gtvnd#, gtctl#) b
  LEFT JOIN stgArkonaGLPCUST c on b.gtvnd# = c.gcvnd#  
    AND c.gcvnd# <> ''
    AND b.dl2 = c.gcco#
  LEFT JOIN stgArkonaGLPMAST d on b.glaccount = d.gmacct
    AND d.gmyear = 2014  
  LEFT JOIN stgArkonaGLPCUST e on b.gtctl# = e.gcvnd#
    AND b.dl2 = e.gcco# ) f 
GROUP BY dl5, glaccount, al4, al5, vendor    


SELECT dl3, dl4, dl5, dl6, al1, al2, al3,al4
FROM zcb 
WHERE name = 'cb2'
GROUP BY dl3, dl4, dl5, dl6, al1, al2, al3,al4


SELECT gtacct, SUM(gttamt) AS gttamt
INTO #gl
FROM stgArkonaGLPTRNS
WHERE gtdate BETWEEN '10/01/2014' AND '10/31/2014'
GROUP BY gtacct

--comparing against jeri's cigar box for october 14
SELECT dl3, al3, al4, sum(b.gttamt) AS amount
FROM zcb a
LEFT JOIN #gl b on a.glaccount = b.gtacct
WHERE name = 'cb2'
  AND dl2 = 'RY1'
GROUP BY dl3, al3, al4

variable personnel   
doc		  cb		 zcb
175376	  175376	 169063

SELECT dl6 al3, al4, al5, glaccount, sum(b.gttamt) AS amount
FROM zcb a
LEFT JOIN #gl b on a.glaccount = b.gtacct
WHERE name = 'cb2'
  AND dl2 = 'RY1'
  AND dl6 = 'new'
  AND al4 = 'Personnel Expenses'
GROUP BY  dl6, al3, al4, al5, glaccount

new other salaries
doc		  cb
26355	  23198	  	

new account: 12301A

SELECT *
FROM zcb
WHERE glaccount like '12301'
  AND name IN ('gmfs expenses','cb2')
-- before i get to giddy with adding rows
-- get some notion of a pk going  
dl4 takes care of accts LIKE 146700 which applies to multiple fixed depts
SELECT name, storecode, dl4, glaccount
FROM zcb
--WHERE glaccount <> 'NA'
GROUP BY name, storecode, dl4, glaccount
HAVING COUNT(*) > 1

-- just fucking get rid of the shit no longer need
delete FROM zcb WHERE name = 'cigar box';
DELETE FROM zcb WHERE storecode = 'ry3';

-- hmm, don't know what these are, look at the arkona ffxprdasdfasdfas stuff
SELECT * FROM zcb WHERE glaccount = 'NA'

SELECT name, storecode, dl4, glaccount
FROM zcb
--WHERE glaccount <> 'NA'
GROUP BY name, storecode, dl4, glaccount
HAVING COUNT(*) > 1

SELECT *
FROM zcb
WHERE name = 'cb2'  AND glaccount = '146700'

SELECT *
FROM zcb
WHERE gmcoa = '067'

-- accounts IN FFPXREFDTA that have activity gl IN october
-- that DO NOT exist IN zcb
SELECT fxgact, fxfact
--INTO #missingAccounts
-- SELECT COUNT(*) -- 3 fucking hundred 12 of them
FROM stgArkonaFFPXREFDTA a
WHERE a.fxcyy = 2014
  AND a.fxconsol = ''
  AND EXISTS (
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtacct = a.fxgact
      AND gtdate BETWEEN '10/01/2014' AND '10/31/2014')
  AND NOT EXISTS (
    SELECT 1
    FROM zcb
    WHERE glaccount = a.fxgact)      
    
    
SELECT *
FROM dimaccount a
INNER JOIN #missingAccounts b on a.glaccount = b.fxgact   

-- ok the first fussy think i am stressing on IS WHERE does the finest level
-- of account description come FROM: chart of accounts OR gm manual
looking at 146700
looks LIKE i am taking it off the fs, but WHERE IS that verbage stored OR 
did i just fucking type it ALL IN, looks LIKE i did

so the problem, DO i just fixed what does NOT match current docs/fs, OR DO i come up with
a generalized way to maintian a dimAccount
OR at least be able to tell what accounts i need that are NOT included IN zcb