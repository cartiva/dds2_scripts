ALTER TABLE zcb
ALTER COLUMN al4 al4 cichar(40)
ADD COLUMN al5 cichar(40)


DROP TABLE #dept;
CREATE TABLE #dept (
  dl1 cichar(24),
  dl2 cichar(24),
  dl3 cichar(24),
  dl4 cichar(24),
  dl5 cichar(24),
  dl6 cichar(24));
  
insert into #dept values	('Grand Forks',	'RY3',	'Variable',	'Sales',	    'F&I',	          'New');
insert into #dept values	('Grand Forks',	'RY3',	'Variable',	'Sales',	    'F&I',	          'Used');
insert into #dept values	('Grand Forks',	'RY3',	'Variable',	'Sales',	    'Vehicle Sales',	'New');
insert into #dept values	('Grand Forks',	'RY3',	'Variable',	'Sales',	    'Vehicle Sales',	'Used');
--insert into #dept values	('Grand Forks',	'RY3',	'Variable',	'NCR',	      'NCR',	          'NCR');
--insert into #dept values	('Grand Forks',	'RY3',	'Fixed',	  'Body Shop',	'Body Shop',	    'Body Shop');
insert into #dept values	('Grand Forks',	'RY3',	'Fixed',	  'Parts',	    'Parts',	        'Parts');
insert into #dept values	('Grand Forks',	'RY3',	'Fixed',	  'Mechanical',	'Main Shop',	    'Main Shop');
--insert into #dept values	('Grand Forks',	'RY3',	'Fixed',	  'Mechanical',	'PDQ',	          'PDQ');  
  
insert into #dept values	('Grand Forks',	'RY2',	'Variable',	'Sales',	    'F&I',	          'New');
insert into #dept values	('Grand Forks',	'RY2',	'Variable',	'Sales',	    'F&I',	          'Used');
insert into #dept values	('Grand Forks',	'RY2',	'Variable',	'Sales',	    'Vehicle Sales',	'New');
insert into #dept values	('Grand Forks',	'RY2',	'Variable',	'Sales',	    'Vehicle Sales',	'Used');
--insert into #dept values	('Grand Forks',	'RY2',	'Variable',	'NCR',	      'NCR',	          'NCR');
--insert into #dept values	('Grand Forks',	'RY2',	'Fixed',	  'Body Shop',	'Body Shop',	    'Body Shop');
insert into #dept values	('Grand Forks',	'RY2',	'Fixed',	  'Parts',	    'Parts',	        'Parts');
insert into #dept values	('Grand Forks',	'RY2',	'Fixed',	  'Mechanical',	'Main Shop',	    'Main Shop');
insert into #dept values	('Grand Forks',	'RY2',	'Fixed',	  'Mechanical',	'PDQ',	          'PDQ');
--insert into #dept values	('Grand Forks',	'RY2',	'Fixed',	  'Mechanical',	'Detail',	        'Detail');
--insert into #dept values	('Grand Forks',	'RY2',	'Fixed',	  'Mechanical',	'Car Wash',	      'Car Wash');  

insert into #dept values	('Grand Forks',	'RY1',	'Variable',	'Sales',	    'F&I',	          'New');
insert into #dept values	('Grand Forks',	'RY1',	'Variable',	'Sales',	    'F&I',	          'Used');
insert into #dept values	('Grand Forks',	'RY1',	'Variable',	'Sales',	    'Vehicle Sales',	'New');
insert into #dept values	('Grand Forks',	'RY1',	'Variable',	'Sales',	    'Vehicle Sales',	'Used');
insert into #dept values	('Grand Forks',	'RY1',	'Variable',	'NCR',	      'NCR',	          'NCR');
insert into #dept values	('Grand Forks',	'RY1',	'Fixed',	  'Body Shop',	'Body Shop',	    'Body Shop');
insert into #dept values	('Grand Forks',	'RY1',	'Fixed',	  'Parts',	    'Parts',	        'Parts');
insert into #dept values	('Grand Forks',	'RY1',	'Fixed',	  'Mechanical',	'Main Shop',	    'Main Shop');
insert into #dept values	('Grand Forks',	'RY1',	'Fixed',	  'Mechanical',	'PDQ',	          'PDQ');
insert into #dept values	('Grand Forks',	'RY1',	'Fixed',	  'Mechanical',	'Detail',	        'Detail');
insert into #dept values	('Grand Forks',	'RY1',	'Fixed',	  'Mechanical',	'Car Wash',	      'Car Wash');
-------------------------------dl1---------dl2------dl3----------dl4------------dl5--------------dl6
SELECT * FROM #dept
  
SELECT *
FROM vzcb
WHERE name in ('gmfs gross', 'gmfs expenses')
  AND dl4 = 'F&I'
  AND dl5 = 'New'   
DELETE FROM zcb WHERE name = 'cb2'
-- variable operations --------------------------------------------------------
-- f&i gross profit  -- no f&i expenses -----------------------------
-- have to include new/used IN join
SELECT * FROM zcb WHERE name = 'cb2'
INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 'Oper Profit' AS al2, b.al1 AS al3, b.al2 AS al4, b.al3 as al5, glaccount, gmcoa, line, split, page 
FROM #dept a 
left join zcb b on a.dl2 = b.dl2 and a.dl3 = b.dl3 and a.dl5 = b.dl4 AND a.dl6 = b.dl5
WHERE a.dl5 = 'f&i' 
-- MIN reqd for uniqueness 
SELECT dl2, al4, dl6, glaccount
FROM zcb
WHERE name = 'cb2'
GROUP BY dl2, al4, dl6, glaccount
HAVING COUNT(*) > 1
-- ok, ry3 goofy, with line 16/11, but totals ok
SELECT dl2, dl6, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
AND dl5 = 'F&I' 
GROUP BY dl2, dl6, line
ORDER BY line asc

-- /f&i -------------------------------------------------------------

-- vehicle sales ----------------------------------------------------
-- need to DO gross & expenses separately
-- what should i use for al5: dl8 (make) OR glpmast DESC 
-- IF used THEN al3 (Used Cars Rtl - GM Certified, etc) 
-- IF new dl8 (make
-- INSERT INTO zcb
-- gross
INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 'Oper Profit' AS al2, b.al1 AS al3, b.al2 AS al4, 
  CASE a.dl6
    WHEN 'New' THEN dl8
    WHEN 'used' THEN al3 
  END as al5, glaccount, gmcoa, line, split, page 
FROM #dept a 
left join zcb b on a.dl2 = b.dl2 /*store*/ and a.dl3 = b.dl3 /*var/fixed*/ and a.dl4 = b.dl4 AND a.dl6 = b.dl5 /*new/used*/
  AND b.name = 'gmfs gross'
WHERE a.dl5 = 'vehicle sales' 
-- expenses
-- al5 = b.al3
INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 'Oper Profit' AS al2, b.al1 AS al3, b.al2 AS al4, 
  b.al3 as al5, glaccount, gmcoa, line, split, page 
FROM #dept a 
left join zcb b on a.dl2 = b.dl2 /*store*/ and a.dl3 = b.dl3 /*var/fixed*/ and a.dl4 = b.dl4 AND a.dl6 = b.dl5 /*new/used*/
  AND b.name = 'gmfs expenses'
WHERE a.dl5 = 'vehicle sales' 
-- uniqueness
SELECT dl2, dl6, al4, glaccount
FROM zcb
WHERE name = 'cb2'
  AND glaccount <> 'NA'
GROUP BY dl2, dl6, al4, glaccount
HAVING COUNT(*) > 1

-- /vehicle sales ---------------------------------------------------

-- ncr --------------------------------------------------------------
-- SELECT * FROM zcb WHERE name = 'cb2' AND dl4 = 'ncr'
DELETE FROM zcb WHERE name = 'cb2' AND dl4 = 'ncr';
INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 'Oper Profit' AS al2, b.al1 AS al3, b.al2 AS al4, 
  b.al3 as al5, glaccount, gmcoa, line, split, page 
FROM #dept a 
left join zcb b on a.dl2 = b.dl2 /*store*/ and a.dl3 = b.dl3 /*var/fixed*/ and a.dl4 = b.dl4 
  AND b.name = 'gmfs gross'
WHERE a.dl4 = 'ncr'
--AND glaccount = '19054'

INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 'Oper Profit' AS al2, b.al1 AS al3, b.al2 AS al4, 
  b.al3 as al5, glaccount, gmcoa, line, split, page 
FROM #dept a 
left join zcb b on a.dl2 = b.dl2 /*store*/ and a.dl3 = b.dl3 /*var/fixed*/ and a.dl4 = b.dl4 AND a.dl6 = b.dl5 /*new/used*/
  AND b.name = 'gmfs expenses'
WHERE a.dl5 = 'ncr' 
-- /ncr -------------------------------------------------------------

-- variable
-- page 2 lines 2,57
SELECT al3, dl2, dl3, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl3 = 'variable'
GROUP BY al3, dl2, dl3

-- variable
-- page 3 lines 2, 57
-- f&i page 7 ines 10, 19
SELECT al3, dl2, dl3, dl5, dl6, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl3 = 'variable'
GROUP BY al3, dl2, dl3, dl5, dl6
ORDER BY dl2, dl5, dl3

-- variable 
-- page 3 lines 7,17,40,55
SELECT dl2, dl5, dl6, al4, SUM(coalesce(gttamt, 0)), MAX(line)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl3 = 'variable'
  AND al3 = 'total expenses'
GROUP BY al4, dl2, dl5, dl6
ORDER BY dl2, dl6, MAX(line)

-- /variable operations -------------------------------------------------------

-- fixed operations -----------------------------------------------------------
-- body shop ----------------------------------------------
-- gross
INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 'Oper Profit' AS al2, b.al1 AS al3, b.al2 AS al4, 
  b.al3 as al5, glaccount, gmcoa, line, split, page 
FROM #dept a 
left join zcb b on a.dl2 = b.dl2 /*store*/ and a.dl3 = b.dl3 /*var/fixed*/ and a.dl4 = b.dl4 
  AND b.name = 'gmfs gross'
WHERE a.dl4 = 'body shop';
-- expenses
INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 'Oper Profit' AS al2, b.al1 AS al3, b.al2 AS al4, 
  b.al3 as al5, glaccount, gmcoa, line, split, page 
FROM #dept a 
left join zcb b on a.dl2 = b.dl2 /*store*/ and a.dl3 = b.dl3 /*var/fixed*/ and a.dl4 = b.dl4 
  AND b.name = 'gmfs expenses'
WHERE a.dl4 = 'body shop';
-- expenses
-- page 4 lines 2,17,40,55
SELECT dl2, dl4, al4, SUM(coalesce(gttamt, 0)), MAX(line)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'body shop'
  AND al3 = 'total expenses'
GROUP BY dl2, dl4, al4
UNION
SELECT dl2, dl4, 'total', SUM(coalesce(gttamt, 0)), 56
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'body shop'
  AND al3 = 'total expenses'
GROUP BY dl2, dl4
ORDER BY max(line)
-- gross
--page 6 lines 35 - 43
SELECT dl2, dl4, line, al5, 
  SUM(CASE WHEN al4 = 'Sales' THEN gttamt ELSE 0 END) AS Sales,
  SUM(CASE WHEN al4 = 'cogs' THEN gttamt ELSE 0 END) AS COGS,
  SUM(coalesce(gttamt, 0)) AS Gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'body shop'
  AND al3 = 'gross profit'
GROUP BY dl2, dl4, al5, line
UNION 
SELECT dl2, dl4, 43, 'total', 
  SUM(CASE WHEN al4 = 'Sales' THEN gttamt ELSE 0 END) AS Sales,
  SUM(CASE WHEN al4 = 'cogs' THEN gttamt ELSE 0 END) AS COGS,
  SUM(coalesce(gttamt, 0)) AS Gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'body shop'
  AND al3 = 'gross profit'
GROUP BY dl2, dl4
ORDER BY line
-- /body shop ---------------------------------------------

-- parts --------------------------------------------------
-- gross
INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 'Oper Profit' AS al2, b.al1 AS al3, b.al2 AS al4, 
  b.al3 as al5, glaccount, gmcoa, line, split, page 
FROM #dept a 
left join zcb b on a.dl2 = b.dl2 /*store*/ and a.dl3 = b.dl3 /*var/fixed*/ and a.dl4 = b.dl4 
  AND b.name = 'gmfs gross'
WHERE a.dl4 = 'parts';
-- expenses
INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 'Oper Profit' AS al2, b.al1 AS al3, b.al2 AS al4, 
  b.al3 as al5, glaccount, gmcoa, line, split, page 
FROM #dept a 
left join zcb b on a.dl2 = b.dl2 /*store*/ and a.dl3 = b.dl3 /*var/fixed*/ and a.dl4 = b.dl4 
  AND b.name = 'gmfs expenses'
WHERE a.dl4 = 'parts';

-- page 4 lines 2,17,40,55 (expenses)
SELECT dl2, dl4, al4, SUM(coalesce(gttamt, 0)), MAX(line)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'parts'
  AND al3 = 'total expenses'
GROUP BY dl2, dl4, al4
UNION
SELECT dl2, dl4, 'total', SUM(coalesce(gttamt, 0)), 56
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'parts'
  AND al3 = 'total expenses'
GROUP BY dl2, dl4
ORDER BY dl2, max(line)

--page 6 lines 45 - 61 (gross)
SELECT dl2, dl4, line, al5, 
  SUM(CASE WHEN al4 = 'Sales' THEN gttamt ELSE 0 END) AS Sales,
  SUM(CASE WHEN al4 = 'cogs' THEN gttamt ELSE 0 END) AS COGS,
  SUM(coalesce(gttamt, 0)) AS Gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'parts'
  AND al3 = 'gross profit'
GROUP BY dl2, dl4, al5, line
UNION 
SELECT dl2, dl4, 61, 'total', 
  SUM(CASE WHEN al4 = 'Sales' THEN gttamt ELSE 0 END) AS Sales,
  SUM(CASE WHEN al4 = 'cogs' THEN gttamt ELSE 0 END) AS COGS,
  SUM(coalesce(gttamt, 0)) AS Gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'parts'
  AND al3 = 'gross profit'
GROUP BY dl2, dl4
ORDER BY dl2, line
-- /parts -------------------------------------------------

-- mechanical ---------------------------------------------
-- DELETE FROM zcb WHERE name = 'cb2' AND dl4 = 'mechanical'
INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 
  'Oper Profit' AS al2, b.al2 AS al3, b.al3 AS al4, c.al3 AS al5,
  b.glaccount, c.gmcoa, c.line, c.split, c.page
-- SELECT *  
FROM #dept a 
LEFT JOIN zcb b ON b.name = 'cigar box'
  AND b.dl2 = 'service' 
  AND b.al2 = 'total expenses'
  AND b.glaccount IS NOT NULL 
  AND a.dl5 = b.dl3 
  AND a.dl2 = b.storecode
LEFT JOIN zcb c ON c.name = 'gmfs expenses'
  AND b.glaccount = c.glaccount
WHERE a.dl4 = 'mechanical'  


-- page 4 lines 2,17,40,55 (expenses)
-- expenses are ok at fs (dl4) level
SELECT dl2, dl4, al4, SUM(coalesce(gttamt, 0)), MAX(line)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'mechanical'
  AND al3 = 'total expenses'
GROUP BY dl2, dl4, al4
UNION
SELECT dl2, dl4, 'total', SUM(coalesce(gttamt, 0)), 56
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'mechanical'
  AND al3 = 'total expenses'
GROUP BY dl2, dl4
ORDER BY dl2, max(line)

-- cigar box expenses  
-- ry1 only pdq ok
-- compare cigar box excel to doc for oct
--pdq ok
--car wash cb2 & doc agree, cbExcel IS fucked up
--detail cb2 & doc agree, cbExcel IS fucked up
--main shop cb2 & doc agree, cbExcel IS fucked up
SELECT dl2, dl5, al4, SUM(coalesce(gttamt, 0)), MAX(line)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'mechanical'
  AND al3 = 'total expenses'
GROUP BY dl2, dl5, al4
UNION
SELECT dl2, dl5, 'total', SUM(coalesce(gttamt, 0)), 56
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'mechanical'
  AND al3 = 'total expenses'
GROUP BY dl2, dl5
ORDER BY dl2, dl5



-- gross
  
-- here are ALL the gross profit accounts FROM 'cigar box' 
-- for service sub depts 
select a.*, b.dl3, b.al3, b.glaccount
FROM #dept a 
LEFT JOIN zcb b ON b.name = 'cigar box'
  AND b.dl2 = 'service' 
  AND b.al2 = 'gross profit'
  AND b.glaccount IS NOT NULL 
  AND a.dl5 = b.dl3 
  AND a.dl2 = b.storecode
WHERE a.dl4 = 'mechanical'  
DELETE FROM zcb WHERE name = 'cb2' AND dl4 = 'mechanical';

INSERT INTO zcb
SELECT 'cb2', left(a.dl2, 3) AS storecode, a.*, 'NA' AS dl7,'NA' AS dl8,'Net Profit' AS al1, 
  'Oper Profit' AS al2, b.al2 AS al3, b.al3 AS al4, c.al3 AS al5,
  b.glaccount, c.gmcoa, c.line, c.split, c.page
FROM #dept a 
LEFT JOIN zcb b ON b.name = 'cigar box'
  AND b.dl2 = 'service' 
  AND b.al2 = 'gross profit'
  AND b.glaccount IS NOT NULL 
  AND a.dl5 = b.dl3 
  AND a.dl2 = b.storecode
LEFT JOIN zcb c ON c.name = 'gmfs gross'
  AND b.glaccount = c.glaccount
WHERE a.dl4 = 'mechanical'  

-- ok, but for parts split confusion caused BY populating FROM cigar box
-- page 6 lines 45 - 61 (gross)
-- line IS no longer consistently relevant IN the cigar box world
-- ry3 missing adjusted cost of labor
INSERT INTO zcb
SELECT 'cb2',storecode, dl1,dl2,dl3,dl4,'Main Shop','Main Shop','NA','NA','Net Profit','Oper Profit','Gross Profit','COGS','Adj Cost of Lbr Sls',glaccount,gmcoa,line,split,page FROM zcb WHERE glaccount = '366500'
-- ok, but for parts split confusion caused BY populating FROM cigar box
-- page 6 lines 45 - 61 (gross)
SELECT dl2, dl4, line, al5, 
  SUM(CASE WHEN al4 = 'Sales' THEN round(gttamt*split,0) ELSE 0 END) AS Sales,
  SUM(CASE WHEN al4 = 'cogs' THEN round(gttamt*split,0) ELSE 0 END) AS COGS,
  SUM(coalesce(round(gttamt*split,0), 0)) AS Gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'mechanical'
  AND al3 = 'gross profit'
GROUP BY dl2, dl4, al5, line
ORDER BY dl2, line
-- define the fucking parts splits accounts

UPDATE zcb
  SET split = .5
--SELECT name, dl2, dl3, dl4, dl5, dl6, glaccount, gmcoa, page, line, split FROM zcb
WHERE left(gmcoa, 3) IN ('467','468','477','478','667','668','677','678')
  AND name = 'cb2'
  
UPDATE zcb
  SET dl4 = 'Body Shop',
      dl5 = 'Body Shop',
      dl6 = 'Body Shop', 
     split = 1
WHERE name = 'cb2'
  AND glaccount = '147701'   
  
UPDATE zcb
  SET dl4 = 'Body Shop',
      dl5 = 'Body Shop',
      dl6 = 'Body Shop', 
     split = 1
WHERE name = 'cb2'
  AND glaccount = '167701'    
-- 147700: ALL parts, need  to ADD body shop part  
INSERT INTO zcb
SELECT name,storecode, dl1,dl2,dl3,'Body Shop','Body Shop','Body Shop',dl7,dl8,al1,al2,al3,al4,al5,glaccount,gmcoa,line,split,page FROM zcb WHERE glaccount = '167700' AND name = 'cb2'
  

-- w/parts split
-- cigar box
-- line IS no longer consistently relevant IN the cigar box world

SELECT dl2, dl4, dl5, 
  SUM(CASE WHEN al4 = 'Sales' THEN round(gttamt*split,0) ELSE 0 END) AS Sales,
  SUM(CASE WHEN al4 = 'cogs' THEN round(gttamt*split,0) ELSE 0 END) AS COGS,
  SUM(coalesce(round(gttamt*split,0), 0)) AS Gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'mechanical'
  AND al3 = 'gross profit'
GROUP BY dl2, dl4, dl5

-- check parts & body shop with the parts split now IN place
-- parts ok
SELECT dl2, dl4, dl5, 
  round(SUM(CASE WHEN al4 = 'Sales' THEN gttamt*split ELSE 0 END), 0) AS Sales,
  round(SUM(CASE WHEN al4 = 'cogs' THEN gttamt*split ELSE 0 END), 0) AS COGS,
  round(SUM(gttamt*split), 0) AS Gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'parts'
  AND al3 = 'gross profit'
GROUP BY dl2, dl4, dl5

-- check parts & body shop with the parts split now IN place
-- body shop ok
SELECT dl2, dl4, dl5, 
  round(SUM(CASE WHEN al4 = 'Sales' THEN gttamt*split ELSE 0 END), 0) AS Sales,
  round(SUM(CASE WHEN al4 = 'cogs' THEN gttamt*split ELSE 0 END), 0) AS COGS,
  round(SUM(gttamt*split), 0) AS Gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'body shop'
  AND al3 = 'gross profit'
GROUP BY dl2, dl4, dl5

-- also need to check mech sup depts
-- $500 diff main shop/pdq
-- good enuf
SELECT dl2, dl4, dl5, 
  round(SUM(CASE WHEN al4 = 'Sales' THEN gttamt*split ELSE 0 END), 0) AS Sales,
  round(SUM(CASE WHEN al4 = 'cogs' THEN gttamt*split ELSE 0 END), 0) AS COGS,
  round(SUM(gttamt*split), 0) AS Gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'cb2' 
  AND dl4 = 'mechanical'
  AND al3 = 'gross profit'
GROUP BY dl2, dl4, dl5

-- /mechanical --------------------------------------------
-- /fixed operations ---------------------------------------------------------- 

-- adds/deducts ---------------------------------------------------------------

-- /adds/deducts --------------------------------------------------------------
    
so it seems WHERE this IS going IS a fact TABLE that looks LIKE
yearmonth - gf - ry1 - fixed - bs - bs - bs - net prof - oper prof - gross prof - sales - cogs - total exp - var - per - semi - fixed
  201210    dl1  dl2    dl3   dl4  dl5  dl6      $$      $$          $$           $$      $$     $$          $$    $$    $$     $$
  
fuck, too many text values IN fact  


store values for market AND each al
so would that look LIKE

grand forks NA NA NA NA NA net profit 


SELECT c.yearmonth, dl2, al3, SUM(gttamt*split)
FROM zcb a
INNER JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
INNER JOIN day c ON b.gtdate = c.thedate
  AND c.yearmonth = 201210
WHERE a.name = 'cb2'
  AND dl2 = 'ry1'
GROUP BY c.yearmonth, dl2, al3

