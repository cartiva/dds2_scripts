-- delivery exp hi ON statement due to detail fucking up ros, find some examples of what that means

uc del exp 013
looks LIKE the culprit IS 11302B used del wash
SELECT a.glaccount, a.gmcoa, c.gmdesc,
  SUM(CASE WHEN month(gtdate) = 09 THEN b.gttamt END) AS sep,
  SUM(CASE WHEN month(gtdate) = 10 THEN b.gttamt END) AS oct,
  SUM(CASE WHEN month(gtdate) = 11 THEN b.gttamt END) AS nov,
  SUM(CASE WHEN month(gtdate) = 12 THEN b.gttamt END) AS dec
FROM zcb a
INNER JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '09/01/2012' AND '12/31/2012'
LEFT JOIN stgArkonaGLPMAST c ON a.glaccount = c.gmacct
  AND c.gmyear = 2012 
WHERE name = 'cb2'
  AND gmcoa = '013'
  AND dl2 = 'ry1'
GROUP BY a.glaccount, a.gmcoa, c.gmdesc 

SELECT *
FROM stgarkonaglptrns
WHERE gtacct = '11302b'
  AND gtdate BETWEEN '12/01/2012' AND '12/31/2012'
  
so this would imply that pdi will be much lower IN dec acct 464, nope, it IS actually twice AS large AS normal 
SELECT a.glaccount, a.gmcoa, c.gmdesc,
  SUM(CASE WHEN month(gtdate) = 09 THEN b.gttamt END) AS sep,
  SUM(CASE WHEN month(gtdate) = 10 THEN b.gttamt END) AS oct,
  SUM(CASE WHEN month(gtdate) = 11 THEN b.gttamt END) AS nov,
  SUM(CASE WHEN month(gtdate) = 12 THEN b.gttamt END) AS dec
FROM zcb a
INNER JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '09/01/2012' AND '12/31/2012'
LEFT JOIN stgArkonaGLPMAST c ON a.glaccount = c.gmacct
  AND c.gmyear = 2012 
WHERE name = 'cb2'
  AND gmcoa = '464'
  AND dl2 = 'ry1'
GROUP BY a.glaccount, a.gmcoa, c.gmdesc 