

SELECT a.*, b.Dec, c.Jan
FROM (
  SELECT a.al2, a.al3, MIN(line) AS line, SUM(coalesce(b.gttamt,0)) AS Oct 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '10/01/2012' AND '10/31/2012'
  WHERE name = 'gmfs expenses'
    AND dl3 = 'variable'
    AND dl2 = 'ry1'
  GROUP BY a.dl2, dl3, a.al2, a.al3) a
LEFT JOIN ( 
  SELECT a.al2, a.al3, SUM(coalesce(b.gttamt,0)) AS Dec, MIN(line) AS line
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '12/01/2012' AND '12/31/2012'
  WHERE name = 'gmfs expenses'
    AND dl3 = 'variable'
    AND dl2 = 'ry1'
  GROUP BY a.dl2, dl3, a.al2, a.al3) b ON a.line = b.line  
LEFT JOIN ( 
  SELECT a.al2, a.al3, SUM(coalesce(b.gttamt,0)) AS Jan, MIN(line) AS line
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '01/01/2013' AND '01/31/2013'
  WHERE name = 'gmfs expenses'
    AND dl3 = 'variable'
    AND dl2 = 'ry1'
  GROUP BY a.dl2, dl3, a.al2, a.al3) c ON a.line = c.line    
ORDER BY a.line



-- ADD gtrdtyp = P
SELECT dl5, al3, gtctl#, c.name, SUM(gttamt) AS Dec
--SELECT SUM(gttamt)
--SELECT gtctl#, c.name, SUM(gttamt) AS Dec
-- SELECT gtjrnl, SUM(gttamt) AS dec
-- SELECT *
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '12/01/2012' AND '12/31/2012'
LEFT JOIN (
  SELECT employeenumber, name
  FROM edwEmployeeDim
  GROUP BY employeenumber, name) c ON b.gtctl# = c.employeenumber  
WHERE a.name = 'gmfs expenses'
  AND dl3 = 'variable'
  AND dl2 = 'ry1'
  AND gmcoa = '023'
--  AND b.gtrdtyp = 'P'
--  AND gtjrnl = 'PAY'
GROUP BY gtjrnl
GROUP BY gtctl#, c.name ORDER BY gtctl#  
GROUP BY a.dl2, dl3, a.al2, dl5, al3, gtctl#, c.name
ORDER BY gtctl#


SELECT dl5, al3, gtctl#, c.name, SUM(gttamt) AS Jan
--SELECT SUM(gttamt)
-- SELECT *
--SELECT gtctl#, c.name, SUM(gttamt) AS Jan
-- SELECT gtjrnl, SUM(gttamt) AS Jan
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '01/01/2013' AND '01/31/2013'
LEFT JOIN (
  SELECT employeenumber, name
  FROM edwEmployeeDim
  GROUP BY employeenumber, name) c ON b.gtctl# = c.employeenumber  
WHERE a.name = 'gmfs expenses'
  AND dl3 = 'variable'
  AND dl2 = 'ry1'
  AND gmcoa = '023'
--  AND b.gtrdtyp = 'P'
 AND gtjrnl = 'GJE'
GROUP BY gtjrnl
GROUP BY gtctl#, c.name ORDER BY gtctl#  
GROUP BY a.dl2, dl3, a.al2, dl5, al3, gtctl#, c.name
ORDER BY gtctl#

SELECT * FROM zcb
WHERE name = 'gmfs expenses'
  AND dl3 = 'variable'
  AND dl2 = 'ry1'