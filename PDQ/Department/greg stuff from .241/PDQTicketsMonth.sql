select char(gtdate, USA) as TheDate, ptco# as StoreCode, count(distinct gtctl#) as Tickets, sum(case when gmtype = 4 then gttamt else 0 end)* -1 as Sales,
  sum(case when gmtype = 4 then gttamt else 0 end) * -1 - sum(case when gmtype = 5 then gttamt else 0 end) as Gross,
  case when count(distinct gtctl#) <> 0 then (sum(case when gmtype = 4 then gttamt else 0 end)* -1) / count(distinct gtctl#) else 0 end as AvgTicket 
from rydedata.glptrns a
left join rydedata.glpmast b on a.gtacct = b.gmacct and gmyear = year(gtdate)
inner join rydedata.sdprhdr c on ptcdat <> 0 and a.gtctl# = c.ptro# and a.gtdate = substr(digits(c.PTCDAT), 5, 2) || '/' || substr(digits(c.PTCDAT), 7, 2)  || '/' || left(digits(c.PTCDAT), 4) and ptco# in ('RY1', 'RY2')
where gtctl# in
  (select distinct b.ptro#
	from rydedata.sdprdet a
	left join rydedata.sdprhdr b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
	where
      (((a.ptco# = 'RY1') and (ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDP', '13A', 'ROT', 'FREEROT', 'ROTEMP', 'NT2', 'NT4'))
        )
       or
       ((a.ptco# = 'RY2') and (ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S', 'LOFD', 'LOF0205', 'MSS', 'M1S', 'FROT', 'ROT', 'RAF', 'NT2', 'NT4'))
       ))
	  and b.ptdate >= year(curdate() - 45 days) * 10000 + (Month(curdate() - 45 days) * 100) + dayofmonth(curdate() - 45 days)
  )
  and gmtype not in ('1', '2') 
  and gtdate >= curdate() - 45 days
group by gtdate, ptco#


