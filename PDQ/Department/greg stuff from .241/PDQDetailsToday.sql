select substr(digits(a.PTDATE), 5, 2) || '/' || substr(digits(a.PTDATE), 7, 2)  || '/' || left(digits(a.PTDATE), 4) as TheDate,
  a.ptco# as StoreCode,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDEL1', 'PDQDP') then 1
           when a.ptco# = 'RY2' and ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S') then 1
      else 0 end) as OilChanges,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQDP') then 1
           when a.ptco# = 'RY2' and ptlopc in ('LOFD', 'LOF0205', 'MSS', 'M1S') then 1
      else 0 end) as BOCs,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('13A') then 1
           when a.ptco# = 'RY2' and ptlopc in ('RAF') then 1
      else 0 end) as AirFilters,
  sum(case when a.ptco# in 'RY1' and ptlopc in ('ROT', 'FREEROT', 'ROTEMP') then 1
           when a.ptco# in 'RY2' and ptlopc in ('FROT', 'ROT') then 1
      else 0 end) as Rotates,
  sum(case when ptlopc in ('NT2', 'NT4') then 1 else 0 end) as Tires
from rydedata.sdprhdr a
left join rydedata.sdprdet b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
where a.ptro# in
   (select distinct b.ptro#
	from rydedata.sdprdet a
	left join rydedata.sdprhdr b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
    where
      (((a.ptco# = 'RY1') and (ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDP', '13A', 'ROT', 'FREEROT', 'ROTEMP', 'NT2', 'NT4'))
        )
       or
       ((a.ptco# = 'RY2') and (ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S', 'LOFD', 'LOF0205', 'MSS', 'M1S', 'FROT', 'ROT', 'RAF', 'NT2', 'NT4'))
       ))
	  and b.ptdate >= year(curdate()) * 10000 + (Month(curdate()) * 100) + dayofmonth(curdate()))
group by a.ptdate, a.ptco#
order by a.ptdate desc, a.ptco#