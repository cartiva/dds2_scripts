
hmm store IS different, IN that i have QL lines on ros (18025335, 16133683) WHERE 
the writer IS nick sattler AND cashier
? a COLUMN for pdq vs nonpdq writer?
ADD INNER JOIN to dimServiceType
shit, which means a different base TABLE, tmptmpPDQWriterStats IS pdq writers only
too far, at least the extra columns are too far
maybe a tmpPDQStoreStats TABLE

--
unrelated, the notion of storing the pivoted data, both for store AND writers
-- 
-- 10/20
the difference BETWEEN writer stats AND store stats 
  writers: for only writers where dimServiceWriter.CensusDept = QL AND the opcode IS one that has a pdqcatXXX value
  store: any ro with a line WHERE service type = QL and
so:
  store numbers will NOT = the SUM of writer numbers for that store  

both are based on the ro HAVING an lof, air filter OR rotate line
both are USING CLOSE date rather than final CLOSE date  


-- CREATE a TABLE
DROP TABLE tmpPDQStoreStats;
CREATE TABLE tmpPDQStoreStats (
  theDate date constraint NOT NULL,
  mmdd cichar(5) constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  ro cichar(9) constraint NOT NULL,
  servicewriterkey integer constraint NOT NULL,
  lof integer constraint NOT NULL,
  boc integer constraint NOT NULL,
  airfilters integer constraint NOT NULL,
  rotate integer constraint NOT NULL,
CONSTRAINT PK PRIMARY KEY (ro)) IN database; 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpPDQStoreStats','tmpPDQStoreStats.adi','theDate','theDate',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpPDQStoreStats','tmpPDQStoreStats.adi','StoreCode','StoreCode',
   '',2,512,'' );    

DELETE FROM tmpPDQStoreStats;  
INSERT INTO tmpPDQStoreStats  
SELECT d.thedate, d.mmdd, b.storecode, b.ro, b.ServiceWriterKey, 
  SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
  SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
  SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
  SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
FROM factRepairOrder b
INNER JOIN day d on b.closedatekey = d.datekey  -- ** closedate NOT finalclosedate
INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
INNER JOIN dimServiceType c on b.ServiceTypeKey = c.ServiceTypeKey
WHERE b.closedatekey IN (SELECT datekey FROM day WHERE theyear IN (2011, 2012, 2013))-- ** closedate NOT finalclosedate  
  AND b.opcodekey IN (SELECT opcodekey FROM dimopcode WHERE pdqcat1 <> 'N/A')
  AND c.ServiceTypeKey = (SELECT ServiceTypeKey FROM dimServiceType WHERE ServiceTypeCode = 'QL')
GROUP BY d.thedate, d.mmdd, b.ro, b.storecode, b.ServiceWriterKey;

-- daily, last 10 days
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilter = 0 THEN 0
    ELSE round(100.0 * airfilter/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM (  
  SELECT mmdd AS "Last 10 Days", storecode, SUM(lof) AS lof, SUM(boc) AS boc, 
    SUM(airfilters) AS airfilter, SUM(rotate) AS rotate
  FROM tmpPDQStoreStats
  WHERE thedate BETWEEN curdate() - 9 AND curdate()
  GROUP BY mmdd, storecode
  UNION 
  SELECT d.mmdd, b.storecode, 
    SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
    SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
    SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
    SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate  
  FROM todayFactRepairOrder b
  INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
    AND d.thedate = curdate()
  INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
    AND e.pdqcat2 <> 'N/A'
  INNER JOIN dimServiceType a on b.ServiceTypeKey = a.ServiceTypeKey
    AND a.ServiceTypeCode = 'QL'   
  GROUP BY d.mmdd, b.storecode) x
ORDER BY "Last 10 Days" DESC, storecode ;

-- week to date
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilters = 0 THEN 0
    ELSE round(100.0 * airfilters/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM ( 
  SELECT *
  FROM (
    SELECT MIN(mmdd) + ' thru ' +  MAX(mmdd) AS "Week to Date"
    FROM day
    WHERE year(thedate) = year(curdate())
      AND week(thedate) = week(curdate())) m
    LEFT JOIN ( 
      -- n :: ALL data grouped BY store/writer   
      SELECT storecode, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
      FROM ( 
        -- c :: historical data for the desired date interval   
        SELECT storecode, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
        FROM tmpPDQStoreStats a
        WHERE year(thedate) = year(curdate())
          AND week(thedate) = week(curdate())
        GROUP BY storecode
        UNION 
        SELECT b.storecode, 
          SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
          SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
          SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
          SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate  
        FROM todayFactRepairOrder b
        INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
          AND d.thedate = curdate()
        INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
          AND e.pdqcat2 <> 'N/A'
        INNER JOIN dimServiceType a on b.ServiceTypeKey = a.ServiceTypeKey
          AND a.ServiceTypeCode = 'QL'   
        GROUP BY d.mmdd, b.storecode) c  
      GROUP BY storecode) n on 1 = 1) x
ORDER BY storecode;      
      
-- month to date
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilters = 0 THEN 0
    ELSE round(100.0 * airfilters/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM ( 
  SELECT *
  FROM (
    SELECT distinct YearMonthShort AS "Month to Date"
    FROM day
    WHERE year(thedate) = year(curdate())
      AND month(thedate) = month(curdate())) m
    LEFT JOIN (   
      -- n :: ALL data grouped BY store/writer 
      SELECT storecode, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
      FROM ( --
        -- c :: historical data for the desired date interval  
        SELECT storecode, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
        FROM tmpPDQStoreStats a
        WHERE year(thedate) = year(curdate()) 
          AND month(thedate) = month(curdate())
        GROUP BY storecode
        UNION 
        SELECT b.storecode, 
          SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
          SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
          SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
          SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate  
        FROM todayFactRepairOrder b
        INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
          AND d.thedate = curdate()
        INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
          AND e.pdqcat2 <> 'N/A'
        INNER JOIN dimServiceType a on b.ServiceTypeKey = a.ServiceTypeKey
          AND a.ServiceTypeCode = 'QL'   
        GROUP BY d.mmdd, b.storecode) c  
      GROUP BY storecode) n on 1 = 1) x
ORDER BY storecode;  

-- last month          
SELECT year(curdate()) - 1 AS "Year to Date Last Year", storecode,
  SUM(lof) AS lof, SUM(boc) AS boc, 
  SUM(airfilters) AS airfilters, SUM(rotate) as rotate,
  CASE 
    WHEN SUM(lof) = 0 OR SUM(boc) = 0 THEN 0
    ELSE round(100.0 * SUM(boc)/SUM(lof), 0) 
  END AS "BOC PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(airfilters) = 0 THEN 0
    ELSE round(100.0 * SUM(airfilters)/SUM(lof), 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(rotate) = 0 THEN 0
    ELSE round(100.0 * SUM(rotate)/SUM(lof), 0) 
  END AS "ROTATE PEN"    
FROM tmpPDQStoreStats a
WHERE year(thedate) = year(timestampadd(SQL_TSI_MONTH, - 1, curdate()))
  AND month(thedate) = month(timestampadd(SQL_TSI_MONTH, - 1, curdate()))
GROUP BY storecode;      
      
-- year to date  
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilters = 0 THEN 0
    ELSE round(100.0 * airfilters/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM ( 
  SELECT *
  FROM (
    SELECT year(curdate()) AS "Year to Date"
    FROM system.iota) m
    LEFT JOIN (   
      -- n :: ALL data grouped BY store/writer 
      SELECT storecode, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
      FROM ( --
        -- c :: historical data for the desired date interval  
        SELECT storecode, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
        FROM tmpPDQStoreStats a
        WHERE year(thedate) = year(curdate())
        GROUP BY storecode
        UNION 
        SELECT b.storecode, 
          SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
          SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
          SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
          SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate  
        FROM todayFactRepairOrder b
        INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
          AND d.thedate = curdate()
        INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
          AND e.pdqcat2 <> 'N/A'
        INNER JOIN dimServiceType a on b.ServiceTypeKey = a.ServiceTypeKey
          AND a.ServiceTypeCode = 'QL'   
        GROUP BY d.mmdd, b.storecode) c  
      GROUP BY storecode) n on 1 = 1) x
ORDER BY storecode;  

-- year to date last year
SELECT year(curdate()) - 1 AS "Year to Date Last Year", storecode,
  SUM(lof) AS lof, SUM(boc) AS boc, 
  SUM(airfilters) AS airfilters, SUM(rotate) as rotate,
  CASE 
    WHEN SUM(lof) = 0 OR SUM(boc) = 0 THEN 0
    ELSE round(100.0 * SUM(boc)/SUM(lof), 0) 
  END AS "BOC PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(airfilters) = 0 THEN 0
    ELSE round(100.0 * SUM(airfilters)/SUM(lof), 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(rotate) = 0 THEN 0
    ELSE round(100.0 * SUM(rotate)/SUM(lof), 0) 
  END AS "ROTATE PEN"    
FROM tmpPDQStoreStats a
WHERE year(thedate) = year(curdate()) - 1
  AND thedate <= cast(timestampadd(sql_tsi_year, -1, curdate()) AS sql_date)
GROUP BY storecode;