SELECT *
FROM dimopcode
WHERE pdqcat1 <> 'N/A'
AND storecode = 'ry1'
ORDER BY pdqcat1,pdqcat2,pdqcat3

DROP TABLE #ros;
SELECT a.ro, c.opcode
INTO #ros
FROM factrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
INNER JOIN dimOpcode c on a.opcodekey = c.opcodekey
WHERE b.thedate BETWEEN curdate() - 365 AND curdate() - 1
  AND a.storecode = 'ry1'
  AND a.paymentTypeKey <> 3 -- exclude internal
  AND c.opcode IN ('pdq','pdqd','pdqm1','pdq520','pdqd020','pdqdel1')
GROUP BY a.ro, c.opcode  

SELECT opcode, COUNT(*)
FROM #ros
GROUP BY opcode

DROP TABLE #penetration;
SELECT a.*,
  CASE 
    WHEN EXISTS (
      SELECT 1
      FROM factRepairOrder
      WHERE ro = a.ro
        AND paymenttypekey <> 3
        AND opcodekey in (
          SELECT opcodekey
          FROM dimOpcode
          WHERE opcode = 'WB')) THEN 1
     ELSE 0
   END AS wb,
  CASE 
    WHEN EXISTS (
      SELECT 1
      FROM factRepairOrder
      WHERE ro = a.ro
        AND paymenttypekey <> 3
        AND opcodekey in (
          SELECT opcodekey
          FROM dimOpcode
          WHERE opcode = '13A')) THEN 1
     ELSE 0
   END AS AF,
  CASE 
    WHEN EXISTS (
      SELECT 1
      FROM factRepairOrder
      WHERE ro = a.ro
        AND paymenttypekey <> 3
        AND opcodekey in (
          SELECT opcodekey
          FROM dimOpcode
          WHERE opcode = 'ROT')) THEN 1
     ELSE 0
   END AS rot    
INTO #penetration   
FROM #ros a

SELECT * FROM #penetration

SELECT opcode, opcodeTotal, round(100.0*opcodeTotal/total, 2) AS opcodePerc, 
  round(100.0*wb/opcodeTotal, 2) AS wbPerc, 
  round(100.0*af/opcodeTotal, 2) AS afPerc, 
  round(100.0*rot/opcodeTotal, 2) AS rotPerc
FROM (
  SELECT *
  FROM (
    SELECT opcode, sum(wb) AS wb, sum(AF) AS af, sum(rot) AS rot, COUNT(*) AS opcodeTotal
    FROM #penetration
    GROUP BY opcode) a
  LEFT JOIN (
    SELECT COUNT(*) AS total
    FROM #ros) b on 1 = 1) c  
union
SELECT 'Totals', totalros, CAST(NULL AS sql_double),
  round(100.0 * totalWB/totalros, 2) AS WB,    
  round(100.0 * totalAF/totalros, 2) AS AF,    
  round(100.0 * totalROT/totalros, 2) AS ROT 
FROM (    
  SELECT COUNT(*) AS totalros,
    SUM(wb) AS totalWB,
    SUM(af) AS totalAF,
    SUM(rot) AS totalROT
  FROM #penetration) x    