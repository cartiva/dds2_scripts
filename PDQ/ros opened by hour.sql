DROP TABLE #wtf;
SELECT a.storecode, a.ro, a.rocreatedts, b.thedate, b.dayname, e.thehour, e.theminute
--INTO #wtf
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimServiceWriter c on a.serviceWriterKey = c.serviceWriterKey
INNER JOIN dimOpcode d on a.opcodekey = d.opcodekey
INNER JOIN timeofday e on hour(a.rocreatedts) = e.theHour
  AND minute(a.roCreatedTS) = e.theMinute
WHERE b.theyear = 2014
  AND c.censusDept= 'QL'
  AND d.pdqcat1 = 'lof'
GROUP BY a.storecode, a.ro, a.rocreatedts, b.thedate, b.dayname, e.thehour, e.theminute  


SELECT storecode, dayname, thehour, COUNT(*)
FROM #wtf
GROUP BY storecode, dayname, thehour


SELECT storecode, dayname, COUNT(*)
FROM #wtf
GROUP BY storecode, dayname


SELECT a.storecode, a.dayname, COUNT(*)
FROM #wtf a
INNER JOIN day b on a.thedate = b.thedate
GROUP BY a.storecode, b.dayofweek, a.dayname




SELECT a.storecode, a.ro, a.rocreatedts, b.thedate, b.monthName, b.dayname
INTO #wtf
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE b.theyear = 2014
  AND a.serviceWriterKey IN (
    SELECT serviceWriterKey
    FROM dimServiceWriter
    WHERE censusDept = 'QL')
  AND a.opcodeKey IN (
   SELECT opcodeKey
   FROM dimOpcode
   WHERE pdqcat1 = 'lof')
GROUP BY a.storecode, a.ro, a.rocreatedts, b.thedate, b.monthName, b.dayname     

  
DROP TABLE #wtf;

SELECT COUNT(*) FROM #wtf


SELECT a.*, CAST(rocreatedts AS sql_time) AS time, 
  hour(roCreatedTS) AS thehour, minute(roCreatedTS) AS theMinute
FROM #wtf a


-- 11/19/14
-- AND BY day
-- DROP TABLE #wtf;
SELECT storecode, thedate, COUNT(*) AS rosOpened
INTO #wtf
FROM(
  SELECT a.storecode, b.thedate, ro
  --INTO #wtf
  FROM factRepairOrder a
  INNER JOIN day b on a.opendatekey = b.datekey
  INNER JOIN dimServiceWriter c on a.serviceWriterKey = c.serviceWriterKey
  INNER JOIN dimOpcode d on a.opcodekey = d.opcodekey
  WHERE b.theyear = 2014
    AND c.censusDept= 'QL'
    AND d.pdqcat1 = 'lof'
  GROUP BY a.storecode, a.ro, b.thedate) x
GROUP BY storecode, thedate

SELECT  * FROM #wtf