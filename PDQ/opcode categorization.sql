ADD columns to dimOpCode
default value IS 'N/A'
            PDQcat1         PDQcat2         PDQcat3
            Oil Change

ALTER TABLE dimOpcode
--DROP COLUMN PDQcat1;
ADD COLUMN PDQcat1 cichar(24) DEFAULT 'N/A' CONSTRAINT NOT NULL
ADD COLUMN PDQcat2 cichar(24) DEFAULT 'N/A' CONSTRAINT NOT NULL 
ADD COLUMN PDQcat3 cichar(24) DEFAULT 'N/A' CONSTRAINT NOT NULL ;     
-- ok, adding the columns AS above sets the constraints for new rows, but NOT 
-- existing rows
-- duh, only ALL of them
-- select * FROM dimopcode WHERE pdqcat1 IS NULL  
UPDATE dimOpcode
SET PDQcat1 = 'N/A',
    PDQcat2 = 'N/A',
    PDQcat3 = 'N/A';     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimOpcode','dimOpcode.adi','PDQcat1',
   'PDQcat1','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimOpcode','dimOpcode.adi','PDQcat2',
   'PDQcat2','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimOpcode','dimOpcode.adi','PDQcat3',
   'PDQcat3','',2,512,'' );      
          
 
 
-- these are the "19" ros, need to DO it based on servicetype            
SELECT b.opcode, left(b.description, 25), COUNT(*), MIN(a.ro), MAX(a.ro), MIN(c.thedate), MAX(c.thedate)
FROM factRepairOrder a
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
LEFT JOIN day c on a.finalclosedatekey = c.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE yearmonth IN (201307,201308,201309, 201310))
  AND LEFT(a.ro, 2) = '19'
GROUP BY b.opcode, left(b.description, 25)  
ORDER BY COUNT(*) DESC 

-- these are the QL ros       
SELECT b.opcode, left(b.description, 25), COUNT(*), MIN(a.ro), MAX(a.ro), MIN(c.thedate), MAX(c.thedate)
FROM factRepairOrder a
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
INNER JOIN dimServiceType bb on a.ServiceTypeKey = bb.ServiceTypeKey
  AND bb.ServiceTypeCode = 'QL'
LEFT JOIN day c on a.finalclosedatekey = c.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE yearmonth IN (201307,201308,201309, 201310))
  AND a.storecode = 'RY2'
GROUP BY b.opcode, left(b.description, 25)  
ORDER BY COUNT(*) DESC 


select * 
FROM dimOpcode
WHERE opcode LIKE 'pdq%'
  AND storecode = 'ry1'
  
SELECT b.opcode, left(b.description, 60), COUNT(*), MIN(a.ro), MAX(a.ro), MIN(c.thedate), MAX(c.thedate)
FROM factrepairorder a  
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
  AND b.opcode  LIKE 'pdq%'
LEFT JOIN day c on a.finalclosedatekey = c.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE yearmonth = 201309)
GROUP BY b.opcode, left(b.description, 60) 

-- pdq doc
SELECT title, description, glaccount, glcostaccount, gldepartment, gtacct, total
--select *
FROM tmpDocs a
LEFT JOIN (
  SELECT gtacct, SUM(gttamt) AS total
  FROM stgArkonaGLPTRNS
  WHERE gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  GROUP BY gtacct) b ON a.glAccount = b.gtacct 
WHERE a.reportnumber = 66

-- differences IN counts BETWEEN fact AND  DOC
some pdq oil change opcodes have account overrides, some DO NOT
NOT sure IF this matters, but i think doc counts come FROM these 
counts
pdq opcode with no acct override
pdqed

SELECT c.thedate, a.*
FROM factRepairOrder a
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
  AND b.opcode = 'PDQED'
LEFT JOIN day c on a.finalclosedatekey = c.datekey  
ORDER BY thedate DESC 
-- labor sales goes to the defaul PDQ labor account: 146002
SELECT *
FROM stgArkonaGLPTRNS
WHERE gtctl# = '19149076'

-- fucking honda opcodes yw1
SELECT b.opcode, LEFT(b.description, 25), a.*
FROM factrepairorder a
INNER JOIN dimopcode b on a.opcodekey = b.opcodekey
  AND opcode LIKE 'yw1%'
           

SELECT b.storecode, b.opcode, LEFT(description, 35),
  SUM(CASE WHEN ro IS NULL THEN 0 ELSE 1 END)
FROM stgArkonaSDPLOPC a
LEFT JOIN dimOpcode b on a.soco# = b.storecode
  AND a.solopc = b.opcode
LEFT JOIN factRepairOrder c on b.opcodekey = c.opcodekey
  AND c.FinalCloseDateKey IN (SELECT datekey FROM day WHERE theyear = 2013)  
WHERE a.sodftsvct = 'QL'
GROUP BY b.storecode, b.opcode, LEFT(description, 35)
-- fucking honda again, labor ops without default service type
SELECT *
FROM stgArkonaSDPLOPC 
WHERE soco# = 'ry2'
  AND sodftsvct = 'QL'

SELECT a.*, b.sodftsvct
FROM ( 
  SELECT b.opcode, left(b.description, 45), COUNT(*), MIN(a.ro), MAX(a.ro), MIN(c.thedate), MAX(c.thedate)
  FROM factRepairOrder a
  INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
  INNER JOIN dimServiceType bb on a.ServiceTypeKey = bb.ServiceTypeKey
    AND bb.ServiceTypeCode = 'QL'
  LEFT JOIN day c on a.finalclosedatekey = c.datekey
  WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)
    AND a.storecode = 'RY2'
  GROUP BY b.opcode, left(b.description, 45)) a 
LEFT JOIN stgArkonaSDPLOPC b on b.soco# = 'RY2' 
  AND a.opcode = b.solopc
  
-- 10/15 i am pretty sure this IS a lame model for scalable categorization, but
-- rather than worrying about it, let's go ahead AND implement for PDQ AND go FROM there
-- basing it on writer metrics (boc) AND jeri's budgeting categories
-- start with ALL opcodes FROM QL service type ros    

-- these are the QL ros    
DROP TABLE #wtf;   
SELECT b.storecode, b.opcode, left(b.description, 60), COUNT(*) AS theCount, MIN(a.ro), MAX(a.ro), MIN(c.thedate), MAX(c.thedate)
INTO #wtf
FROM factRepairOrder a
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
INNER JOIN dimServiceType bb on a.ServiceTypeKey = bb.ServiceTypeKey
  AND bb.ServiceTypeCode = 'QL'
LEFT JOIN day c on a.finalclosedatekey = c.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear IN (2012,2013))
--  AND a.storecode = 'RY2'
GROUP BY b.storecode, b.opcode, left(b.description, 60)  

select a.*, b.pdqcat1, pdqcat2, pdqcat3
FROM #wtf a
LEFT JOIN dimopcode b on a.storecode = b.storecode
  AND a.opcode = b.opcode
ORDER BY thecount DESC   
  
UPDATE dimOpcode
SET pdqcat1 = 'LOF'
WHERE storecode = 'RY1'
  AND opcode IN ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDEL1', 'PDQDP','PDQE020');  
  
UPDATE dimOpcode
SET pdqcat1 = 'LOF'
WHERE storecode = 'RY2'
  AND opcode IN ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S');  
  
UPDATE dimOpcode
SET pdqcat3 = 'BOC'
WHERE storecode = 'RY1'  
  AND opcode IN ('PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQDP','PDQE020');
  
UPDATE dimOpcode
SET pdqcat3 = 'BOC'
WHERE storecode = 'RY2'  
  AND opcode IN ('LOFD', 'LOF0205', 'MSS', 'M1S');  
  
  
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Air Filters',
    pdqcat3 = 'Air Filters'
WHERE storecode = 'RY1' 
  AND opcode = '13A';
  
  
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Air Filters',
    pdqcat3 = 'Air Filters'
WHERE storecode = 'RY2' 
  AND opcode = 'RAF';  
  
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Rotate',
    pdqcat3 = 'Rotate'
WHERE storecode = 'RY1' 
  AND opcode IN ('ROT', 'FREEROT', 'ROTEMP');
  
  
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Rotate',
    pdqcat3 = 'Rotate'
WHERE storecode = 'RY2' 
  AND opcode IN ('FROT', 'ROT');  

UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Nitrogen'
WHERE storecode = 'RY1' 
  AND opcode IN ('NITROINV', 'NITROP');
    
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Nitrogen'
WHERE storecode = 'RY2' 
  AND opcode = 'NITRO';    
  
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Bulbs'
WHERE storecode = 'RY1' 
  AND opcode IN ('BULB', 'BULBN');
  
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Bulbs'
WHERE storecode = 'RY2' 
  AND opcode = 'BULB';  

UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Wiper Blades'
WHERE storecode = 'RY1' 
  AND opcode IN ('WB', 'WBN');
  
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Wiper Blades'
WHERE storecode = 'RY2' 
  AND opcode IN ('RWB', 'RWI'); 
  
  
select a.*, b.pdqcat1, pdqcat2, pdqcat3
FROM #wtf a
INNER JOIN dimopcode b on a.storecode = b.storecode
  AND a.opcode = b.opcode
WHERE b.pdqcat1 = 'lof'

SELECT storecode, opcode, LEFT(description, 100), pdqcat1,pdqcat2,pdqcat3
FROM dimopcode
WHERE pdqcat1 = 'lof'
-- fucking honda again, are these comparable to dexos?
select a.*, b.pdqcat1, pdqcat2, pdqcat3
FROM #wtf a
INNER JOIN dimopcode b on a.storecode = b.storecode
  AND a.opcode = b.opcode
WHERE a.storecode = 'ry2' and expr like '%oil%' //AND b.pdqcat1 = 'lof'

select a.*, b.pdqcat1, pdqcat2, pdqcat3
FROM #wtf a
INNER JOIN dimopcode b on a.storecode = b.storecode
  AND a.opcode = b.opcode
WHERE a.storecode = 'ry1' AND b.pdqcat1 = 'lof'

-- need to get clarification of definitions

what the fuck are 020 vs 520 vs mobil one
jeri why some pdq with acct override AND others NOT
get fucking specific
-- wtf includes ALL ql ros FROM 2012 & 2013
SELECT a.*, b.pdqcat1, b.pdqcat2, b.pdqcat3
FROM (
  select *
  FROM #wtf
  WHERE expr LIKE '%oil%' 
    AND opcode NOT IN ('topoff','995','ros')) a
LEFT JOIN dimopcode b on a.storecode = b.storecode AND a.opcode = b.opcode  
WHERE expr_4 > '01/01/2013'
ORDER BY expr_4

honda exceptions/exclusions
  LOFC: customer supplied oil
  PDQD: 1 transaction
  
so i just talked to jeri
how did you get your different lof categoriztions i asked
FROM parts stocking groups she said
fuck me  

Select pmco#, pmpart, pmdesc, pmsgrp, pmdprt as "disp part" 
from RYDEDATA.PDPMAST where trim(pmsgrp) in ('516', '515','517','514')
order by pmsgrp
which yields:
PMCO# PMPART                    PMDESC                         PMSGRP disp part
RY1   12345615                  530 GM OIL                     514    12345615                 
RY1   B520G                     5W20 OIL                       515    B520G                    
RY1   89021512                  B520G                          515    89021512                 
RY1   B020SS                    MOBIL SUPER SYN                516    B020SS                   
RY1   020DEX1                   0W20 DEXOS QUART               516    020DEX1                  
RY1   020HON                    OIL                            516    020HON                   
RY1   19259487                  0W20 DEXOS QUART               516    19259487                 
RY1   B1030S                    SHELLOIL                       517    B1030S                   
RY1   B1540S                    SHELLOIL                       517    B1540S                   
RY1   1540DEL                   DELVAC 1                       517    1540DEL                  
RY1   1540S                     ROTELLA                        517    1540S                    
RY1   540DELS                   MOBIL DELVAC 1                 517    540DELS                  
RY1   540ROT                    GALLON SYN OIL                 517    540ROT                   
RY1   88862469                  1540DEL GAL                    517    88862469                 
RY1   88862497                  1540DEL QUART                  517    88862497 

talked to greg
we are actually talking about marketing of products, parts stock groups IS NOT what
we market,
opcode should be good enuf, the lof products are good, better, best, diesel

so i need to reformulate jeris template
AND be able to prove to her that the money IS the same (AS compared to her parts GROUP based doc)

ok, finish with the dimopcode updates

SELECT DISTINCT pdqcat2 FROM dimopcode

UPDATE dimOpcode
SET pdqcat1 = 'LOF',
    pdqcat2 = 'Best',
    pdqcat3 = 'BOC'
WHERE storecode = 'RY1' 
  AND opcode IN ('PDQ020', 'PDQEM1','PDQM1','PDQE020');
    
UPDATE dimOpcode
SET pdqcat1 = 'LOF',
    pdqcat2 = 'Best',
    pdqcat3 = 'BOC'
WHERE storecode = 'RY2' 
  AND opcode IN ('M1S','MS','MSHM','MSS','PDQ020');

UPDATE dimOpcode
SET pdqcat1 = 'LOF',
    pdqcat2 = 'Better',
    pdqcat3 = 'BOC'
WHERE storecode = 'RY1' 
  AND opcode IN ('PDQD', 'PDQD020','PDQDP', 'PDQED');
  
UPDATE dimOpcode
SET pdqcat1 = 'LOF',
    pdqcat2 = 'Better',
    pdqcat3 = 'BOC'
WHERE storecode = 'RY2' 
  AND opcode IN ('LOF0205','LOFD','PDQD','YW15AA');  

UPDATE dimOpcode
SET pdqcat1 = 'LOF',
    pdqcat2 = 'Diesel',
    pdqcat3 = 'N/A'
WHERE storecode = 'RY1' 
  AND opcode IN ('PDQDEL1','PDQDS');  
  
UPDATE dimOpcode
SET pdqcat1 = 'LOF',
    pdqcat2 = 'Good'
WHERE storecode = 'RY1' 
  AND opcode IN ('PDQ', 'PDQ520');
  
UPDATE dimOpcode
SET pdqcat1 = 'LOF',
    pdqcat2 = 'Good'
WHERE storecode = 'RY2' 
  AND opcode IN ('LOF', 'PDQ');    
  
SELECT a.*, b.pdqcat1, b.pdqcat2, b.pdqcat3
FROM (
  select *
  FROM #wtf
  WHERE expr LIKE '%oil%' 
    AND opcode NOT IN ('topoff','995','ros')) a
LEFT JOIN dimopcode b on a.storecode = b.storecode AND a.opcode = b.opcode  
WHERE expr_4 > '01/01/2013'  

SELECT storecode, opcode, LEFT(description, 50), pdqcat1, pdqcat2, pdqcat3
FROM dimopcode
WHERE pdqcat1 <> 'N/A'


SELECT d.yearmonth, a.storecode, c.description, b.pdqcat1, b.pdqcat2, b.pdqcat3, COUNT(*)
FROM factRepairOrder a
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
INNER JOIN dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
INNER JOIN dimServiceType cc on a.ServiceTypeKey = cc.ServiceTypeKey
  AND cc.ServiceTypeCode = 'QL'
LEFT JOIN day d on a.finalclosedatekey = d.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)
GROUP BY d.yearmonth, a.storecode, c.description, b.pdqcat1, b.pdqcat2, b.pdqcat3

SELECT d.yearmonth, a.storecode, c.description, b.pdqcat1, COUNT(*)
FROM factRepairOrder a
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
INNER JOIN dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
INNER JOIN dimServiceType cc on a.ServiceTypeKey = cc.ServiceTypeKey
  AND cc.ServiceTypeCode = 'QL'
LEFT JOIN day d on a.finalclosedatekey = d.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)
GROUP BY d.yearmonth, a.storecode, c.description, b.pdqcat1

SELECT d.yearmonth, a.storecode, b.pdqcat1, COUNT(*)
FROM factRepairOrder a
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
--INNER JOIN dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
INNER JOIN dimServiceType cc on a.ServiceTypeKey = cc.ServiceTypeKey
  AND cc.ServiceTypeCode = 'QL'
LEFT JOIN day d on a.finalclosedatekey = d.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)
GROUP BY d.yearmonth, a.storecode, b.pdqcat1

SELECT d.yearmonth, a.storecode, 'LOF', COUNT(*)
FROM factRepairOrder a
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
  AND b.pdqcat1 = 'LOF'
--INNER JOIN dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
INNER JOIN dimServiceType cc on a.ServiceTypeKey = cc.ServiceTypeKey
  AND cc.ServiceTypeCode = 'QL'
LEFT JOIN day d on a.finalclosedatekey = d.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)
GROUP BY d.yearmonth, a.storecode, b.pdqcat1

-- 3/12/15, some UPDATE FROM Ben C
SELECT pdqcat1,pdqcat2,pdqcat3, COUNT(*)
FROM dimopcode
group by pdqcat1,pdqcat2,pdqcat3

SELECT opcodekey,storecode,opcode,LEFT(description,20),pdqcat1,pdqcat2,pdqcat3
FROM dimopcode
WHERE LEFT(opcode,2) = 'DF'
  AND opcode NOT LIKE 'DFD%'
ORDER BY opcode

UPDATE dimOpcode
SET pdqcat1 = 'Drop And Fill', 
    pdqcat2 = 'N/A',
    pdqcat3 = 'N/A'
WHERE opcodekey IN (
  SELECT opcodekey
  FROM dimopcode
  WHERE LEFT(opcode,2) = 'DF'
    AND opcode NOT LIKE 'DFD%')
    
SELECT *
FROM dimOpcode
WHERE opcode = 'pdqe'    

UPDATE dimOpcode
SET pdqcat1 = 'LOF'
WHERE opcode = 'PDQE'

SELECT opcodekey,storecode,opcode,LEFT(description,40),pdqcat1,pdqcat2,pdqcat3
FROM dimopcode
WHERE opcode LIKE 'rto%'

UPDATE dimOpcode
SET pdqcat1 = 'Aftermarket Retorque'
WHERE opcode = 'RTORQ'

SELECT opcodekey,storecode,opcode,LEFT(description,40),pdqcat1,pdqcat2,pdqcat3
FROM dimopcode
WHERE opcode = 'pdqed020'

UPDATE dimOpcode
SET pdqcat1 = 'LOF'
WHERE opcode = 'pdqed020'