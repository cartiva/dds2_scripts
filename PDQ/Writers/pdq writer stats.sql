-- opcodes on ros w/servicetype = QL 
SELECT a.storecode, g.opcode, left(g.description, 25), min(ro), max(ro),min(b.thedate), max(b.thedate), COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 90
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey   
WHERE flaghours > 0
GROUP BY a.storecode, g.opcode, left(g.description, 25)
ORDER BY a.storecode, g.opcode


-- opcodes on ros w/servicetype = QL  
SELECT a.storecode, g.opcode, left(g.description, 25), min(b.thedate), max(b.thedate), COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 90
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey   
WHERE flaghours > 0
GROUP BY a.storecode, g.opcode, left(g.description, 25)
ORDER BY a.storecode, g.opcode

SELECT * FROM dimservicewriter WHERE active = true

SELECT DISTINCT censusdept FROM dimservicewriter

SELECT storecode, name, description, writernumber
FROM dimservicewriter
WHERE censusdept = 'ql'
  AND active = true
  AND currentrow = true
ORDER BY storecode, name  


-- WHERE the fuck IS DROT
-- remove flaghours
-- opcodes on ros w/servicetype = QL  
SELECT a.storecode, g.opcode, left(g.description, 50), min(b.thedate), max(b.thedate), round(avg(flaghours),2), COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 90
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey   
--WHERE flaghours > 0
GROUP BY a.storecode, g.opcode, left(g.description, 50)
ORDER BY a.storecode, g.opcode



-- need to ADD any opcodes written BY a pdq writer regardless of service type


SELECT storecode, name, description, writernumber
FROM dimservicewriter
WHERE censusdept = 'ql'
  AND active = true
-- this IS sort of ok, but gives me multiple lines based on the diff dates, flaghours, count  
SELECT a.storecode, g.opcode, left(g.description, 50), max(b.thedate), round(avg(flaghours),2), COUNT(*)
FROM factRepairOrder a 
INNER JOIN day b on finalclosedatekey = b.datekey
  AND b.thedate > curdate() - 90 
INNER JOIN dimservicewriter c on a.servicewriterkey = c.servicewriterkey
  AND c.censusDept = 'QL'  
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey  
INNER JOIN dimServiceType h on a.ServiceTypeKey = h.ServiceTypeKey   
GROUP BY a.storecode, g.opcode, left(g.description, 50)
UNION 
SELECT a.storecode, g.opcode, left(g.description, 50), max(b.thedate), round(avg(flaghours),2), COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 90
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey   
--WHERE flaghours > 0
GROUP BY a.storecode, g.opcode, left(g.description, 50)
-- so
SELECT storecode, opcode, left(description, 50)
FROM (
  SELECT a.storecode, g.opcode, left(g.description, 50) AS description
  FROM factRepairOrder a 
  INNER JOIN day b on finalclosedatekey = b.datekey
    AND b.thedate > curdate() - 90
  INNER JOIN dimservicewriter c on a.servicewriterkey = c.servicewriterkey
    AND c.censusDept = 'QL'  
  INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey  
  INNER JOIN dimServiceType h on a.ServiceTypeKey = h.ServiceTypeKey   
  UNION 
  SELECT a.storecode, g.opcode, left(g.description, 50) AS description
  FROM factrepairorder a
  INNER JOIN day b on a.opendatekey = b.datekey
    AND b.thedate > curdate() - 90
  INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
    AND f.servicetypecode = 'ql' 
  INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey) x   
GROUP BY storecode, opcode, description
ORDER BY storecode, opcode

-- which IS better, but shit LIKE brakes show up IN the corcode, NOT the opcode
-- which IS julst maint
-- so
SELECT storecode, LaborOp, LaborOpDesc,CorCode, CorCodeDesc  
FROM (
  SELECT a.storecode, g.opcode AS LaborOp, left(g.description, 50) AS LaborOpDesc,
    m.Opcode AS CorCode, LEFT(m.description, 50) AS CorCodeDesc
  FROM factRepairOrder a 
  INNER JOIN day b on finalclosedatekey = b.datekey
    AND b.thedate > curdate() - 90
  INNER JOIN dimservicewriter c on a.servicewriterkey = c.servicewriterkey
    AND c.censusDept = 'QL'  
  INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey  
  INNER JOIN dimCorCodeGroup j on a.CorCodeGroupKey = j.CorCodeGroupKey
  INNER JOIN brCorCodeGroup k on j.CorCodeGroupKey = k.corCodeGroupKey
  INNER JOIN dimOpcode m on k.OpCodeKey = m.OpcodeKey
  INNER JOIN dimServiceType h on a.ServiceTypeKey = h.ServiceTypeKey   
  UNION 
  SELECT a.storecode, g.opcode AS LaborOp, left(g.description, 50) AS LaborOpDesc,
    m.Opcode AS CorCode, LEFT(m.description, 50) AS CorCodeDesc
  FROM factrepairorder a
  INNER JOIN day b on a.opendatekey = b.datekey
    AND b.thedate > curdate() - 90
  INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
    AND f.servicetypecode = 'ql' 
  INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey
  INNER JOIN dimCorCodeGroup j on a.CorCodeGroupKey = j.CorCodeGroupKey
  INNER JOIN brCorCodeGroup k on j.CorCodeGroupKey = k.corCodeGroupKey
  INNER JOIN dimOpcode m on k.OpCodeKey = m.OpcodeKey) x
GROUP BY storecode, LaborOp, LaborOpDesc,CorCode, CorCodeDesc  
ORDER BY storecode, laborop, corcode

SELECT year(ptdate), LEFT(ptro#,2), COUNT(*)
FROM stgArkonaSDPRDET
WHERE ptlopc = '6b'
GROUP BY year(ptdate), LEFT(ptro#,2)

-- 9/24
-- ALL ros written BY a pdq writer, closed past 90 days
SELECT a.storecode, a.ro, a.line, b.thedate, c.*
FROM factRepairOrder a 
INNER JOIN day b on finalclosedatekey = b.datekey
  AND b.thedate > curdate() - 90
INNER JOIN dimservicewriter c on a.servicewriterkey = c.servicewriterkey
  AND c.censusDept = 'QL'  
  
-- ros with servicetype 'QL' NOT written BY a PDQ writer
DROP TABLE #wtf;
SELECT a.storecode, a.ro, a.line, a.thedate, d.*
INTO #wtf
FROM (
  SELECT *
  FROM factRepairOrder a 
  INNER JOIN day b on opendatekey = b.datekey -- opendate, that was WHEN the gets logged
  AND b.thedate > curdate() - 90) a
INNER JOIN dimServiceType c on a.ServiceTypeKey = c.ServiceTypeKey
  AND c.ServiceTypeCode = 'QL'
INNER JOIN dimServiceWriter d on a.ServiceWriterKey = d.ServiceWRiterKey
  AND d.CensusDept <> 'QL'
  AND a.thedate BETWEEN ServiceWriterKeyFromDAte AND ServiceWriterKeyThruDate


SELECT storecode, name, description, writernumber, MIN(thedate), MAX(thedate), MIN(ro), MAX(ro), COUNT(*)
--SELECT *
FROM #wtf
GROUP BY storecode, name, description, writernumber

SELECT * 
FROM #wtf
WHERE writernumber = '127'
ORDER BY thedate desc
  
  

-- cor codes
-- shit, they are ALL N/A
SELECT a.storecode, g.opcode, left(g.description, 25), min(ro), max(ro),min(b.thedate), max(b.thedate), COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 60
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
INNER JOIN dimCorCodeGroup m on a.corcodegroupkey = m.corcodegroupkey
INNER JOIN brCorCodeGroup n on m.CorCodeGroupKey = n.CorCodeGroupKey
INNER JOIN dimOpCode g on n.OpCodeKey = g.OpCodeKey 
WHERE flaghours > 0  
GROUP BY a.storecode, g.opcode, left(g.description, 25)
ORDER BY a.storecode, g.opcode  

-- 9/26 "19" ros AND their writers

SELECT *
FROM (
  SELECT a.servicewriterkey, COUNT(*)
  FROM factRepairORder a
  INNER JOIN day b on a.opendatekey = b.datekey
    AND b.yearmonth IN (201307,201308,201309)
  WHERE LEFT(a.ro,2) = '19'    
  GROUP BY a.servicewriterkey) a
LEFT JOIN dimServiceWriter b on a.ServiceWriterKey = b.ServiceWriterKey  

-- 10/13/13
-- "19" ros op codes

SELECT b.opcode, left(b.description, 25), COUNT(*), MIN(a.ro), MAX(a.ro), MIN(c.thedate), MAX(c.thedate)
FROM factRepairOrder a
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
LEFT JOIN day c on a.finalclosedatekey = c.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE yearmonth IN (201307,201308,201309, 201310))
  AND LEFT(a.ro, 2) = '19'
GROUP BY b.opcode, left(b.description, 25)  
ORDER BY opcode
ORDER BY COUNT(*) DESC 



