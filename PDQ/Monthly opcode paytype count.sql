SELECT pdqcat1,pdqcat2,pdqcat3, COUNT(*)
FROM dimopcode
group by pdqcat1,pdqcat2,pdqcat3

-- DROP TABLE #ben;
-- ADD CLOSE date, final CLOSE date
SELECT a.ro, a.line, e.thedate AS flagDate, c.paymentType, d.opcode, pdqcat1,
  f.thedate AS closeDate, g.thedate AS finalCloseDate
-- SELECT COUNT(*)
INTO #ben
FROM factRepairOrder a
INNER JOIN dimServiceType b on a.serviceTypeKey = b.serviceTypeKey
  AND b.serviceTypeCode = 'QL'
INNER JOIN dimPaymentType c on a.paymentTypeKey = c.paymentTypeKey  
INNER JOIN dimOpcode d on a.opcodekey = d.opcodekey
INNER JOIN day e on a.flagdatekey = e.datekey
INNER JOIN day f on a.closeDateKey = f.datekey
INNER JOIN day g on a.finalCloseDateKey = g.datekey
WHERE a.storecode = 'RY1'
  AND d.pdqcat1 <> 'N/A'
  AND flagdatekey IN (
    SELECT datekey
    FROM day
    WHERE yearmonth = 201503)



SELECT COUNT(*) -- 3811
FROM #ben
WHERE pdqcat1 <> 'N/A' -- 3783

SELECT opcode, count(*)
FROM #ben
GROUP BY opcode
ORDER BY COUNT(*) DESC 

SELECT pdqcat1, COUNT(*)
FROM #ben
GROUP BY pdqcat1

select * 
FROM #ben  
WHERE pdqcat1 = 'N/A'

SELECT *
FROM #ben

SELECT COUNT(DISTINCT ro) FROM #ben

SELECT paymenttype, COUNT(DISTINCT ro) FROM #ben GROUP BY paymentType

-- ros with multiple lines
SELECT *
FROM #ben
WHERE ro IN (
  SELECT ro
  FROM #ben a
  GROUP BY ro
  HAVING COUNT(*) > 3)
ORDER BY ro, line

-- ros with multiple payment types
SELECT ro
FROM (
  SELECT ro, paymenttype
  FROM #ben
  GROUP BY ro,paymenttype) a
GROUP BY ro  
HAVING COUNT(*) > 1

SELECT *
FROM #ben
WHERE ro IN (
  SELECT ro
  FROM (
    SELECT ro, paymenttype
    FROM #ben
    GROUP BY ro,paymenttype) a
  GROUP BY ro  
  HAVING COUNT(*) > 1)
ORDER BY ro, line 

SELECT COUNT(DISTINCT ro) FROM #ben WHERE finalCloseDate < '02/28/2015'

-- each DISTINCT ro has a single final CLOSE date
SELECT ro
FROM (
  SELECT ro, finalclosedate
  FROM #ben
  GROUP BY ro,finalclosedate) a
GROUP BY ro
HAVING COUNT(*) > 1  

SELECT paymenttype, COUNT(*)
FROM #ben
GROUP BY paymenttype

SELECT opcode, 
  SUM(CASE WHEN paymenttype = 'customer pay' THEN 1 ELSE 0 END) AS CP,
  SUM(CASE WHEN paymenttype = 'warranty' THEN 1 ELSE 0 END) AS Warranty,
  SUM(CASE WHEN paymenttype = 'internal' THEN 1 ELSE 0 END) AS Internal,
  COUNT(*) AS Total
FROM #ben
GROUP BY opcode
ORDER BY opcode
  