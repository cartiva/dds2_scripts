2/3/14
  the initial limited need IS for email capture only
  
IS a car deal a fact OR a dimension
a dimension of an inventory fact?

SELECT bmco#, bmkey
FROM stgArkonaBOPMAST
GROUP BY bmco#, bmkey HAVING COUNT(*) > 1

yeah, but what about 20660A, 2 rows, bmkey 21536 & 21537, identical IN every other aspect
same car, same customer, same amounts, same dates
WTF


anyway, besides the above
a vehicleSale IS of an inventoried vehicle, NOT just a vehicle
dimVehicle IS any vehicle with which we have done business (fixed OR variable)

-- factSale >--- dimVehicle ---< factInventory
--                   |---< dimInventoryVehicle ---< factInventory


/*  BOPMAST */
FIELD   Field Def      VALUE      
BMSTAT  Record Status             In Process Deal (Unaccepted)
BMSTAT  Record Status   A         Accepted Deal
BMSTAT  Record Status   U         Capped Deal
BMTYPE  Record Type     C         Cash, Wholesale, Fleet, or Dealer Xfer
BMTYPE  Record Type     F         Financed Retail Deal
BMTYPE  Record Type     L         Financed Lease Deal
BMTYPE  Record Type     O         Cash Deal w/ Owner Financing
BMVTYP  Vehicle Type    N         New  
BMVTYP  Vehicle Type    U         Used  
BMWHSL  Sale Type       F         Fleet Deal
BMWHSL  Sale Type       L         Lease Deal
BMWHSL  Sale Type       R         Retail Deal
BMWHSL  Sale Type       W         Wholesale Deal
BMWHSL  Sale Type       X         Dealer Xfer

bmunwd: unwind flag
SELECT bmunwd, COUNT(*) FROM stgArkonaBOPMAST GROUP BY bmunwd

bmdelvdt: delivery date
bmdtaprv: approved date
bmdtcap: capped date
bmdtsv: saved date
bmsact: sale account
bmvcst: vehicle cost
bmpric: retail price
bmdtor: origination date
-- these 2 fields are NOT used IN bopmast
--bmfimgr: f/i mgr
--bmslmgr: sales mgr

what the fuck does an Accepted mean IN terms of the data, what IS the acccepted date

FROM arkona help:
Deal Statuses can be custom-defined by each dealership. Deals are assigned to Deal Statuses for tracking the sales process.
You can ONLY use function Deal Status if you have Defined Deal Statuses. Otherwise, this function will display as "Accept Deal".
Four deal statuses are system-defined, two of which have special programming: 
  In Process - The default status for any deal that is currently being worked 
  Closed - Only applies to dealerships using the Customer Showroom Management application
  Accepted - When a deal is placed in "Accepted" status, the vehicle will be moved 
    out of inventory and into the Buyers name. Any trade vehicle will be stocked into inventory.
  Capped - Journal Entries will be posted for the sold vehicle and the trade vehicle.
 

IN arkona, ran deal analysis for 1/25/14, that list includes 20459, the only attribute
that IS 1/25/14 IS bmdtor

so i run the query against db2 for today(2/3), the report matches the query
WHERE IN the query results, both bmdtor AND bmdtaprv = 2/3/14

OMFG, i forgot about bmstat, but of course that IS ok for today, but i have
no way of knowing what bmstat value was on 1/25

SELECT a.bmco#, a.bmkey, bmstk#, bmvin, a.bmstat, a.bmtype, a.bmvtyp, a.bmwhsl, 
  bmdtor, bmdtaprv, bmdtcap, a.bmdelvdt, bmdtsv, bmsact, bmvcst, bmpric, 
  b.*
-- SELECT *
FROM stgArkonaBOPMAST a
LEFT JOIN dimVehicle b on a.bmvin = b.vin
WHERE bmstk# <> ''
  AND bmstk# = '20842A'
  AND bmdtor = '02/03/2014' ORDER BY bmstk#
  AND (bmdtcap = '01/25/2014' OR bmdtcap = '01/25/2014')
  
-- 2/3
so, WHERE i am leaving off today
  what about the 2 rows IN bopmast, 21536 & 21537
  some combination of factVehicleSale & factVehicleInventory, maybe a subtyped
    dimVehicle, coordinating the dates of transition
      Inv: acquired, sold
      Deal: sold
-- 2/14, hmm, no vin & no stock#    
-- which leads me to speculate, no car, no car deal regardless of row IN bopmast  
-- duh, better yet, use bmstat
SELECT a.bmco#, a.bmkey, bmstk#, bmvin, a.bmstat, a.bmtype, a.bmvtyp, a.bmwhsl, 
  bmdtor, bmdtaprv, bmdtcap, a.bmdelvdt, bmdtsv, bmsact, bmvcst, bmpric
-- SELECT *  
FROM stgArkonaBOPMAST a
WHERE bmkey IN (21536,21537)
 
-- bingo      
SELECT bmco#, bmkey
FROM stgArkonaBOPMAST
WHERE bmstat <> '' -- eliminates IN process deals
GROUP BY bmco#, bmkey HAVING COUNT(*) > 1      


SELECT a.bmco#, a.bmkey, bmstk#, bmvin, a.bmstat, a.bmtype, a.bmvtyp, a.bmwhsl, 
  bmdtor, bmdtaprv, bmdtcap, a.bmdelvdt, bmdtsv, bmsact, bmvcst, bmpric, 
  a.bmbuyr, a.bmcbyr,
  b.*
FROM stgArkonaBOPMAST a
LEFT JOIN dimcustomer b on a.bmbuyr = b.bnkey 
WHERE bmstat <> '' 


-- customer indentifiers (bnkey) are NOT repeated across stores
select *
FROM (
  SELECT *
  FROM dimcustomer
  WHERE storecode = 'ry1') a
INNER JOIN (
  SELECT *
  FROM dimcustomer
  WHERE storecode = 'ry2') b  on a.bnkey = b.bnkey
  
salesmen AND managers actually seem to come FROM bopslss

select bqco#, bqkey, bqtype
from stgArkonabopslss
group by bqco#, bqkey, bqtype having count(*) > 1  

select bqco#, bqkey, bqtype, bqstyp
from stgArkonabopslss
group by bqco#, bqkey, bqtype, bqstyp having count(*) > 1

SELECT bqtype, COUNT(*)
FROM stgArkonaBOPSLSS
GROUP BY bqtype

SELECT bqstyp, COUNT(*)
FROM stgArkonaBOPSLSS
GROUP BY bqstyp

select*
FROM stgArkonaBOPSLSS
WHERE bqkey = 12777

-- anomalies
SELECT *
FROM stgArkonaBOPMAST
WHERE bmkey = 12777

-- 2/11/14
-- SELECT bqco#, bqkey FROM (
select bqco#, bqkey, bqdate, 
  MAX(CASE WHEN bqtype = 'S' THEN bqspid END) AS Consultant,
  MAX(CASE WHEN bqtype = 'F' THEN bqspid END) AS Manager
-- SELECT *  
FROM stgArkonaBOPSLSS 
WHERE bqco# IN ('RY1','RY2')
  AND bqkey <> 16916 -- one multiple date anomaly
GROUP BY bqco#, bqkey, bqdate
-- ) x GROUP BY bqco#, bqkey HAVING COUNT(*) > 1

select * FROM stgArkonaBOPSLSS WHERE bqkey = 21633
select * FROM stgArkonaBOPMAST WHERE bmkey = 21633


-- consultant, manager, vehicle, buyer, cobuyer
SELECT a.bmco#, a.bmkey, bmstk#, bmvin, a.bmstat, a.bmtype, a.bmvtyp, a.bmwhsl, 
  bmdtor, bmdtaprv, bmdtcap, bmvcst, bmpric, 
  a.bmbuyr, a.bmcbyr, 
  iif(c.fullName = 'NA', c.description, c.fullname) AS consultant,
  iif(d.fullName = 'NA', d.description, d.fullname) AS manager, 
  e.VehicleKey, f.*, g.*
FROM stgArkonaBOPMAST a
LEFT JOIN (
  select bqco#, bqkey,
    MAX(CASE WHEN bqtype = 'S' THEN bqspid END) AS Consultant,
    MAX(CASE WHEN bqtype = 'F' THEN bqspid END) AS Manager
  -- SELECT *  
  FROM stgArkonaBOPSLSS 
  WHERE bqco# IN ('RY1','RY2')
  GROUP BY bqco#, bqkey, bqdate) b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
LEFT JOIN dimSalesPerson c on a.bmco# = c.storeCode AND b.consultant = c.salesPersonID  
LEFT JOIN dimSalesPerson d on a.bmco# = d.storeCode AND b.manager = d.salesPersonID 
LEFT JOIN dimvehicle e on a.bmvin = e.vin
LEFT JOIN dimCustomer f on a.bmbuyr = f.bnkey
LEFT JOIN dimCustomer g on a.bmcbyr = g.bnkey
WHERE bmstat <> '' 
  AND a.bmco# IN ('ry1','ry2')

-- cobuyer bmcbyr = 0 WHEN there IS none, this does NOT WORK with dimCustomer,
-- IN service customerkey 0 = inventory
-- IN deals customerkey 0 = none 
-- looks LIKE i need an N/A customerDim row
SELECT a.bmco#, a.bmkey, bmstk#, h.carDealInfoKey, e.VehicleKey,
  i.datekey AS originationDateKey, j.datekey AS approvedDateKey,
  k.datekey AS cappedDateKey,
  f.customerKey AS buyerKey, 
  iif(a.bmcbyr = 0, gg.customerKey, g.customerKey) AS cobuyerKey, 
  c.salesPersonKey AS consultantKey, d.salesPersonKey AS managerKey,
  bmvcst, bmpric
FROM stgArkonaBOPMAST a
LEFT JOIN (
  select bqco#, bqkey,
    MAX(CASE WHEN bqtype = 'S' THEN bqspid END) AS Consultant,
    MAX(CASE WHEN bqtype = 'F' THEN bqspid END) AS Manager
  -- SELECT *  
  FROM stgArkonaBOPSLSS 
  WHERE bqco# IN ('RY1','RY2')
  GROUP BY bqco#, bqkey, bqdate) b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
LEFT JOIN dimSalesPerson c on a.bmco# = c.storeCode AND b.consultant = c.salesPersonID  
LEFT JOIN dimSalesPerson d on a.bmco# = d.storeCode AND b.manager = d.salesPersonID 
LEFT JOIN dimvehicle e on a.bmvin = e.vin
LEFT JOIN dimCustomer f on a.bmbuyr = f.bnkey
LEFT JOIN dimCustomer g on a.bmcbyr = g.bnkey
LEFT JOIN dimCustomer gg on 1 = 1
  AND gg.fullName = 'N/A'
LEFT JOIN dimCarDealInfo h on a.bmstat = h.dealStatusCode
  AND a.bmtype = h.dealTypeCode 
  AND a.bmvtyp = h.vehicleTypeCode
  AND a.bmwhsl = h.saleTypeCode 
LEFT JOIN day i on a.bmdtor = i.thedate
LEFT JOIN day j on a.bmdtaprv = j.thedate
LEFT JOIN day k on a.bmdtcap = k.thedate  
WHERE bmstat <> '' 
  AND a.bmco# IN ('ry1','ry2')

select bmco#, bmkey FROM ( -- bingo
SELECT a.bmco#, a.bmkey, bmstk#, h.carDealInfoKey, e.VehicleKey,
  i.datekey AS originationDateKey, j.datekey AS approvedDateKey,
  k.datekey AS cappedDateKey,
  f.customerKey AS buyerKey, 
  iif(a.bmcbyr = 0, gg.customerKey, g.customerKey) AS cobuyerKey,
  c.salesPersonKey AS consultantKey, d.salesPersonKey AS managerKey, 
  bmvcst, bmpric
FROM stgArkonaBOPMAST a
LEFT JOIN (
  select bqco#, bqkey,
    MAX(CASE WHEN bqtype = 'S' THEN bqspid END) AS Consultant,
    MAX(CASE WHEN bqtype = 'F' THEN bqspid END) AS Manager
  -- SELECT *  
  FROM stgArkonaBOPSLSS 
  WHERE bqco# IN ('RY1','RY2')
  GROUP BY bqco#, bqkey, bqdate) b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
LEFT JOIN dimSalesPerson c on a.bmco# = c.storeCode AND b.consultant = c.salesPersonID  
LEFT JOIN dimSalesPerson d on a.bmco# = d.storeCode AND b.manager = d.salesPersonID 
LEFT JOIN dimvehicle e on a.bmvin = e.vin
LEFT JOIN dimCustomer f on a.bmbuyr = f.bnkey
LEFT JOIN dimCustomer g on a.bmcbyr = g.bnkey
LEFT JOIN dimCustomer gg on 1 = 1
  AND gg.fullName = 'N/A'
LEFT JOIN dimCarDealInfo h on a.bmstat = h.dealStatusCode
  AND a.bmtype = h.dealTypeCode 
  AND a.bmvtyp = h.vehicleTypeCode
  AND a.bmwhsl = h.saleTypeCode 
LEFT JOIN day i on a.bmdtor = i.thedate
LEFT JOIN day j on a.bmdtaprv = j.thedate
LEFT JOIN day k on a.bmdtcap = k.thedate  
WHERE bmstat <> '' 
  AND a.bmco# IN ('ry1','ry2')
) x GROUP BY bmco#, bmkey HAVING COUNT(*) > 1;  

-- 3/1/15, change primary key to storecode,bmkey,stocknumber
-- 3/3/15 added noY column
DROP TABLE factVehicleSale;
CREATE TABLE factVehicleSale (
  storeCode cichar(3) constraint NOT NULL,
  bmkey integer constraint NOT NULL,
  stockNumber cichar(9) constraint NOT NULL,
  carDealInfoKey integer constraint NOT NULL,
  vehicleKey integer constraint NOT NULL,
  originationDateKey integer constraint NOT NULL,
  approvedDateKey integer constraint NOT NULL,
  cappedDateKey integer constraint NOT NULL,
  buyerKey integer constraint NOT NULL,
  coBuyerKey integer constraint NOT NULL,
  consultantKey integer constraint NOT NULL,
  managerKey integer constraint NOT NULL,
  vehicleCost numeric(12,2) constraint NOT NULL default '0', 
  vehiclePrice numeric(12,2) constraint NOT NULL default '0',
  noY cichar(9) constraint NOT NULL,
  constraint PK primary key (storecode,bmkey,stocknumber)) IN database;
-- regular indexes 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factVehicleSale','factVehicleSale.adi','stockNumber',
   'stockNumber','',2,512,'' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factVehicleSale','factVehicleSale.adi','carDealInfoKey',
   'carDealInfoKey','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factVehicleSale','factVehicleSale.adi','vehicleKey',
   'vehicleKey','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factVehicleSale','factVehicleSale.adi','originationDateKey',
   'originationDateKey','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factVehicleSale','factVehicleSale.adi','approvedDateKey',
   'approvedDateKey','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factVehicleSale','factVehicleSale.adi','cappedDateKey',
   'cappedDateKey','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factVehicleSale','factVehicleSale.adi','buyerKey',
   'buyerKey','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factVehicleSale','factVehicleSale.adi','coBuyerKey',
   'coBuyerKey','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factVehicleSale','factVehicleSale.adi','consultantKey',
   'consultantKey','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factVehicleSale','factVehicleSale.adi','managerKey',
   'managerKey','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleSale', 
  'factVehicleSale.adi', 'noY', 'noY', 
  '', 2, 512, NULL );                           
-- there have been some tweeks to this, compared to above, to get constraints
-- mollified  
-- salesPersonID, buyer & cobuyer, vehicle

/*
3/23/14

  NOT hugely comfortable with this, but here it IS
  move bopmast FROM executables.stgZapAndReload to exe.BOPMAST
  full DELETE AND scrape of BOPMAST
  THEN run sp.xfmVehicleSale, which of course IS a full DELETE AND populate factVehicleSale
*/

DELETE 
FROM zprocprocedures  
WHERE executable = 'stgZapAndReload'
  AND proc = 'bopmast';
  
INSERT INTO zProcexecutables values('BOPMAST','daily',CAST('00:15:00' AS sql_time),0);
INSERT INTO zProcProcedures values('BOPMAST','BOPMAST',1,'daily');
INSERT INTO DependencyObjects values('BOPMAST','BOPMAST','BOPMAST');  

INSERT INTO zProcexecutables values('factVehicleSale','daily',CAST('01:15:00' AS sql_time),0);
INSERT INTO zProcProcedures values('factVehicleSale','factVehicleSale',1,'daily');
INSERT INTO DependencyObjects values('factVehicleSale','factVehicleSale','factVehicleSale');  

INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b on 1 = 1
  WHERE a.dObject = 'factVehicleSale'
  AND b.dObject = 'BOPMAST';
  
-- this will be the stored proc xfmVehicleSale  
  DELETE FROM factVehicleSale;
  INSERT INTO factVehicleSale(storecode,bmkey,stockNumber,carDealInfoKey,
    vehicleKey,originationDateKey,approvedDateKey,cappedDateKey,buyerKey,
    coBuyerKey,consultantKey,managerKey,vehicleCost,vehiclePrice,noY)
  SELECT a.bmco#, a.bmkey, bmstk#, h.carDealInfoKey, 
    coalesce(e.VehicleKey, ee.vehicleKey),
    i.datekey AS originationDateKey, j.datekey AS approvedDateKey,
    k.datekey AS cappedDateKey,
    coalesce(f.customerKey, gg.customerKey) AS buyerKey, 
    CASE -- 
      WHEN g.bnkey IS NULL THEN ggg.customerKey
      ELSE iif(a.bmcbyr = 0, gg.customerKey, g.customerKey) 
    END AS cobuyerKey,
    iif(c.salesPersonKey IS NULL, cc.salesPersonKey, c.salesPersonKey) AS consultantKey, 
    iif(d.salesPersonKey IS NULL, cc.salesPersonKey, d.salesPersonKey) AS managerKey, 
    bmvcst, bmpric, replace(bmstk#,'Y','')
  FROM stgArkonaBOPMAST a
  LEFT JOIN (
    select bqco#, bqkey,
      MAX(CASE WHEN bqtype = 'S' THEN bqspid END) AS Consultant,
      MAX(CASE WHEN bqtype = 'F' THEN bqspid END) AS Manager
    -- SELECT *  
    FROM stgArkonaBOPSLSS 
    WHERE bqco# IN ('RY1','RY2')
    GROUP BY bqco#, bqkey, bqdate) b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
  LEFT JOIN dimSalesPerson c on a.bmco# = c.storeCode AND b.consultant = c.salesPersonID  
  LEFT JOIN dimSalesPerson cc on a.bmco# = cc.storeCode AND 1 = 1 AND cc.salesPersonID = 'UNK'
  LEFT JOIN dimSalesPerson d on a.bmco# = d.storeCode AND b.manager = d.salesPersonID 
  LEFT JOIN dimvehicle e on a.bmvin = e.vin
  LEFT JOIN dimVehicle ee on 1 = 1
    AND ee.vin = 'UNKNOWN'
  LEFT JOIN dimCustomer f on a.bmbuyr = f.bnkey
  LEFT JOIN dimCustomer g on a.bmcbyr = g.bnkey
  LEFT JOIN dimCustomer gg on 1 = 1
    AND gg.fullName = 'N/A'
  LEFT JOIN dimCustomer ggg on 1 = 1
    AND ggg.fullName = 'UNKNOWN'  
  LEFT JOIN dimCarDealInfo h on a.bmstat = h.dealStatusCode
    AND a.bmtype = h.dealTypeCode 
    AND a.bmvtyp = h.vehicleTypeCode
    AND a.bmwhsl = h.saleTypeCode 
  LEFT JOIN day i on a.bmdtor = i.thedate
  LEFT JOIN day j on a.bmdtaprv = j.thedate
  LEFT JOIN day k on a.bmdtcap = k.thedate  
  WHERE bmstat <> '' 
    AND a.bmco# IN ('ry1','ry2');   
   


