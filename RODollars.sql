SELECT g.gtdoc#, gtdate, 
  sum(case when m.gmtype = '4' and m.gmdept = 'SD' then gttamt else 0 END) AS "Labor Sales",
  sum(CASE WHEN m.gmtype = '5' and m.gmdept = 'SD' THEN gttamt else 0 END) AS "Labor Cost",
  sum(CASE WHEN m.gmtype = '4' and m.gmdept = 'PD' THEN gttamt else 0 END) AS "Parts Sales",
  sum(CASE WHEN m.gmtype = '5' and m.gmdept = 'PD' THEN gttamt else 0 END) AS "Parts Cost"
--SELECT *
-- SELECT DISTINCT g.gtacct
--INTO #wtf
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmyear = 2012
  AND m.gmtype in ('4', '5')
  AND m.gmdept in ('SD', 'PD')
WHERE g.gtdtyp = 's'
  AND g.gtdate > '07/01/2012'
GROUP BY g.gtdoc#, gtdate
ORDER BY g.gtdoc#


SELECT g.gtdoc#, gtdate, 
  sum(case when m.gmtype = '4' and m.gmdept = 'SD' then gttamt else 0 END) AS "Labor Sales",
  sum(CASE WHEN m.gmtype = '5' and m.gmdept = 'SD' THEN gttamt else 0 END) AS "Labor Cost",
  sum(CASE WHEN m.gmtype = '4' and m.gmdept = 'PD' THEN gttamt else 0 END) AS "Parts Sales",
  sum(CASE WHEN m.gmtype = '5' and m.gmdept = 'PD' THEN gttamt else 0 END) AS "Parts Cost"
--SELECT *
-- SELECT DISTINCT g.gtacct
--INTO #wtf
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmyear = 2012
  AND m.gmtype in ('4', '5')
  AND m.gmdept in ('SD', 'PD', 'QL')
WHERE g.gtdtyp = 's'
  AND g.gtdate > '07/01/2012'
GROUP BY g.gtdoc#, gtdate



SELECT a.gtdoc#, a.gtdtyp, a.gtdate, a.gttamt, a.gtacct, b.gmyear, b.gmtype, b.gmdept
FROM stgArkonaGLPTRNS a
INNER JOIN stgArkonaGLPMAST b ON a.gtacct = b.gmacct
  AND b.gmyear = 2012
WHERE a.gtdoc# = '19106404'
  AND a.gtdate > '07/01/2012'
ORDER BY b.gmtype

-- looks LIKE hazardous waste IS Acct Type 8: Expense (acct: 16103C,
--transaction type S Type 8 account:
SELECT a.gtacct, b.gmdept, COUNT(*)
FROM stgArkonaGLPTRNS a
INNER JOIN stgArkonaGLPMAST b ON a.gtacct = b.gmacct
  AND b.gmyear = 2012
  AND b.gmdept in ('SD', 'PD', 'QL')  
  AND b.gmtype = '8' -- in ('4', '5', '8')
WHERE a.gtdtyp = 's'
  AND a.gtdate > '05/01/2012'
GROUP BY a.gtacct, b.gmdept  

SELECT a.gtdoc#, a.gtdtyp, a.gtdate, a.gttamt, a.gtacct, b.gmyear, b.gmtype, b.gmdept
FROM stgArkonaGLPTRNS a
INNER JOIN stgArkonaGLPMAST b ON a.gtacct = b.gmacct
  AND b.gmyear = 2012
WHERE a.gtacct = '12204'
  AND a.gtdate > '07/01/2012'

-- wtf acct 12204 Salaries Clerical AS a $20.10 charge ON 16092234
SELECT a.gtdoc#, a.gtdtyp, a.gtdate, a.gttamt, a.gtacct, b.gmyear, b.gmtype, b.gmdept
FROM stgArkonaGLPTRNS a
INNER JOIN stgArkonaGLPMAST b ON a.gtacct = b.gmacct
  AND b.gmyear = 2012
WHERE a.gtdoc# = '16092234'
ORDER BY gmtype 


SELECT a.gtdoc#, a.gtdtyp, a.gtdate, a.gttamt, a.gtacct, b.gmyear, b.gmtype, b.gmdept

SELECT DISTINCT gtdtyp
FROM stgArkonaGLPTRNS a
INNER JOIN stgArkonaGLPMAST b ON a.gtacct = b.gmacct
  AND b.gmyear = 2012
WHERE a.gtdoc# LIKE '19%'
  AND a.gtdate > '05/01/2012'
  
-- some anomalies AND adjustments, basically type s should be good enuf 
SELECT a.gtdoc#, a.gtdtyp, a.gtdate, a.gttamt, a.gtacct, b.gmyear, b.gmtype, b.gmdept
FROM stgArkonaGLPTRNS a
INNER JOIN stgArkonaGLPMAST b ON a.gtacct = b.gmacct
  AND b.gmyear = 2012
WHERE a.gtdoc# LIKE '19%'
  AND length(TRIM(a.gtdoc#)) = 8
  AND a.gtdate > '05/01/2011'  
  AND a.gtdtyp NOT IN ('S')
  
-- for ben's numbers (quick lube
-- hazardous waste/shop supples are just an offset to an expense
(labor sales - labor cost) + (parts sales - parts cost)/2
-- take out the date, GROUP BY doc only
SELECT g.gtdoc#,
  sum(case when m.gmtype = '4' and m.gmdept = 'SD' then gttamt else 0 END) AS "Labor Sales",
  sum(CASE WHEN m.gmtype = '5' and m.gmdept = 'SD' THEN gttamt else 0 END) AS "Labor Cost",
  sum(CASE WHEN m.gmtype = '4' and m.gmdept = 'PD' THEN gttamt else 0 END) AS "Parts Sales",
  sum(CASE WHEN m.gmtype = '5' and m.gmdept = 'PD' THEN gttamt else 0 END) AS "Parts Cost"
--SELECT *
-- SELECT DISTINCT g.gtacct
--INTO #wtf
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmyear = 2012
  AND m.gmtype in ('4', '5')
  AND m.gmdept in ('SD', 'PD', 'QL')
WHERE g.gtdtyp = 's'
  AND g.gtdate > '07/01/2012'
GROUP BY g.gtdoc#


SELECT COUNT(*) -- 1073
FROM factro
WHERE LEFT(ro, 2) like '19%'
  AND closedatekey <> finalclosedatekey
  
SELECT COUNT(*) -- 104971
FROM factro
WHERE LEFT(ro, 2) like '19%'
  AND closedatekey = finalclosedatekey  
  
SELECT *
FROM stgArkonaGLPTRNS  
-- 7/11
DELETE FROM zrodollars;
INSERT INTO zROdollars  
SELECT a.StoreCode, a.RO, a.Franchise, a.WriterID, a.CustomerName, a.CustomerKey, 
  a.VIN, a.Void, a.OpenDateKey, a.CloseDateKey, a.FinalCloseDateKey, 
  coalesce(b.LaborSales, 0), coalesce(b.LaborCost, 0), 
  coalesce(b.PartsSales, 0), coalesce(b.PartsCost, 0), b.gtdoc#
FROM factro a
LEFT JOIN (
  SELECT b.gtdoc#,
    sum(case when c.gmtype = '4' and c.gmdept in ('SD', 'QL', 'BS') then b.gttamt else 0 END) AS "LaborSales",
    sum(CASE WHEN c.gmtype = '5' and c.gmdept in ('SD', 'QL', 'BS') THEN b.gttamt else 0 END) AS "LaborCost",
    sum(CASE WHEN c.gmtype = '4' and c.gmdept = 'PD' THEN b.gttamt else 0 END) AS "PartsSales",
    sum(CASE WHEN c.gmtype = '5' and c.gmdept = 'PD' THEN b.gttamt else 0 END) AS "PartsCost"
  FROM stgArkonaGLPTRNS b
  INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
    AND year(b.gtdate) = c.gmyear
    AND c.gmtype in ('4', '5')
    AND c.gmdept in ('SD', 'PD', 'QL', 'BS')
  WHERE b.gtdtyp = 's'
    AND b.gtdoc# IN (SELECT ro FROM factro)
  GROUP BY b.gtdoc#) b ON a.ro = b.gtdoc#  


-- shit, need bs too IN the CASE statement too
SELECT *
FROM zrodollars
WHERE gtdoc IS NULL 

UPDATE factro
SET LaborSales = 0,
    LaborCost = 0,
    PartsSales = 0,
    PartsCost = 0

UPDATE fro
SET LaborSales = coalesce(x.LaborSales, 0),
    LaborCost = coalesce(x.LaborCost, 0),
    PartsSales = coalesce(x.PartsSales, 0),
    PartsCost = coalesce(x.PartsCost, 0)
FROM factro fro
LEFT JOIN (
  SELECT b.gtdoc#,
    sum(case when c.gmtype = '4' and c.gmdept <> 'PD' then b.gttamt else 0 END) AS "LaborSales",
    sum(CASE WHEN c.gmtype = '5' and c.gmdept <> 'PD' THEN b.gttamt else 0 END) AS "LaborCost",
    sum(CASE WHEN c.gmtype = '4' and c.gmdept = 'PD' THEN b.gttamt else 0 END) AS "PartsSales",
    sum(CASE WHEN c.gmtype = '5' and c.gmdept = 'PD' THEN b.gttamt else 0 END) AS "PartsCost"
  FROM stgArkonaGLPTRNS b
  INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
    AND year(b.gtdate) = c.gmyear
    AND c.gmtype in ('4', '5')
    AND c.gmdept in ('SD', 'PD', 'QL', 'BS')
  WHERE b.gtdtyp = 's'
    AND b.gtdoc# IN (SELECT ro FROM factro)
  GROUP BY b.gtdoc#) x ON fro.ro = x.gtdoc#  
WHERE ro = fro.ro 
    
    
SELECT * 
FROM factro a
INNER JOIN zrodollars b ON a.ro = b.ro
WHERE a.PartsCost <> b.PartsCost


SELECT a.gtdoc#, a.gtdtyp, a.gtdate, a.gttamt, a.gtacct, b.gmyear, b.gmtype, b.gmdept
FROM stgArkonaGLPTRNS a
INNER JOIN stgArkonaGLPMAST b ON a.gtacct = b.gmacct
    AND year(a.gtdate) = b.gmyear
    AND b.gmtype in ('4', '5')
WHERE a.gtdoc# LIKE '180167%'
  AND a.gtdate > '05/01/2011'  

-- 7/11
/*
after doing ALL this, i realize, the ro grain IS NOT going to give the pdq numbers wanted
ro knows nothing FROM depts
total labor sales
19069238
oil change & car wash
laborsales:
ql : 29.76
cs    4
    ------
    33.76, which IS NOT the number ben wants
    
think we need a pdq datamart
can sales/cos be resolved at the line level
let''s see     
-- these are the glpmast.depts which ave glptrns.gtdtyp = s WHERE glptmast.gmtype IN 4,5 AND the doc IS an RO
BS -   65944
CW -    6022
GN -  187816
NC -    5109
PD -  798352
QL -  250827
RE -   15070
SD -  248362
UC -    2637
SELECT *
FROM stgArkonaGLPTRNS
WHERE gtdoc# IN (SELECT ro FROM factro)

SELECT b.gtdoc#, b.gtdtyp, b.gtdate, b.gttamt, b.gtacct, c.gmyear, c.gmtype, c.gmdept    
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
  AND c.gmdept = 'CW'
WHERE b.gtdtyp = 's'
  AND gtdoc# IN (SELECT ro FROM factro)


-- IN 13 sec
  SELECT b.gtdoc#,
    sum(case when c.gmtype = '4' and c.gmdept <> 'PD' then b.gttamt else 0 END) AS "LaborSales",
    sum(CASE WHEN c.gmtype = '5' and c.gmdept <> 'PD' THEN b.gttamt else 0 END) AS "LaborCost",
    sum(CASE WHEN c.gmtype = '4' and c.gmdept = 'PD' THEN b.gttamt else 0 END) AS "PartsSales",
    sum(CASE WHEN c.gmtype = '5' and c.gmdept = 'PD' THEN b.gttamt else 0 END) AS "PartsCost"
  FROM stgArkonaGLPTRNS b
  INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
    AND year(b.gtdate) = c.gmyear
    AND c.gmtype in ('4', '5')
    AND c.gmdept in ('SD', 'PD', 'QL', 'BS')
  WHERE b.gtdtyp = 's'
    AND gtdoc# IN (
      SELECT ptro#
      FROM xfmRO a
      GROUP BY ptco#, ptro#)  
  GROUP BY b.gtdoc#
*/  

-- 7/11
can sales/cos be resolved at the line level
let''s see   

SELECT b.gtdoc#, b.gtdtyp, b.gtdate, b.gttamt, b.gtacct, c.gmyear, c.gmtype, c.gmdept    
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
--  AND c.gmdept = 'CW'
WHERE b.gtdoc# = '19069238'

-- NOT gl.dept, but coa.dept
looking to link gl to ro''s BY coa.dept to ro.servicetype

so what about WHEN there are multiple lines of a service type
the gl doesn''t know shit about lines
go against the line with the flaghours - nope
what about multiple lines of a service typ each of which have flaghours

that might be the datamart rollup
BY dept
won''t know opcode, or date OR flag hours, but those are availabe elsewhere AND they are NOT available at this grain
i know i''m leaving shit out, i''m mostly interested IN the highest grain possible
relationship BETWEEN gl AND ro

grain
this IS an aggregate of factroline exposing a different slice: servicetype
-- don't know about paytpe
-- 1 row per ro/dept w/sales/cos labor & parts

SELECT servicetype, COUNT(*)
FROM factroline
GROUP BY servicetype

these need to be mapped 
        RO            GL
    ------------------------
    AM -  11571             
    BS -  35874   BS -   65944
    CM -      2   
    CW -   8044   CW -    6022
    EM -   2103
    MR - 265274   SD -  248362
    QL - 422205   QL -  250827
    RE -  22239   RE -   15070
-- these are the glpmast.depts which ave glptrns.gtdtyp = s WHERE glptmast.gmtype IN 4,5 AND the doc IS an RO    
SELECT c.gmdept, COUNT(*)    
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
WHERE b.gtdtyp = 's'
  AND gtdoc# IN (SELECT ro FROM factro)  
GROUP BY c.gmdept  
BS -   65944
CW -    6022
GN -  187816
NC -    5109
PD -  798352
QL -  250827
RE -   15070
SD -  248362
UC -    2637    

so what doesn''t map:  
ro : AM, CM, EM
coa: NC, UC
-- thinking don't give a shit about nc/uc

-- gl transactions type sm, coa.dept = NC,UC coa.type: sales/cos
SELECT b.gtdoc#, b.gtdtyp, b.gtdate, b.gttamt, b.gtacct, c.gmyear, c.gmtype, c.gmdept 
INTO #coaNcUc   
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
  AND c.gmdept IN ('NC','UC')
WHERE b.gtdtyp = 's'
  AND gtdoc# IN (SELECT ro FROM factro)  
  
SELECT *
FROM #coaNCUC
--WHERE gtdoc# = '16088812'
ORDER BY gtdate desc  

select *
FROM stgArkonaGLPTRNS
WHERE gtdoc# = 'ab016720'
  

SELECT *
FROM factroline
WHERE servicetype = 'EM'
  AND linedatekey > 3000


SELECT b.gtdoc#, b.gtdtyp, b.gtdate, b.gttamt, b.gtacct, c.gmyear, c.gmtype, c.gmdept    
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
WHERE b.gtdtyp = 's'
  AND b.gtdoc# = '2657623'   
ORDER BY gmdept, gmtype  

-- shit, parts split IS going to matter
-- eg 16085870
/**/

/**/


select*
FROM stgArkonaGLPMAST
WHERE gmacct = '146007'

DROP TABLE #ro;
DROP TABLE #gl;
SELECT *
INTO #ro
FROM factroline
WHERE servicetype = 'EM'
  AND linedatekey > 3000;
SELECT b.gtdoc#, c.gmtype, c.gmdept, SUM(gttamt)
INTO #gl 
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
  AND c.gmdept <> 'PD'
WHERE b.gtdtyp = 's'
  AND gtdoc# IN (
    SELECT ro 
    FROM factroline 
    WHERE servicetype = 'EM'
      AND linedatekey > 3000)    
GROUP BY b.gtdoc#, gmdept, gmtype;  

SELECT *
FROM #ro a
full JOIN #gl  b ON a.ro = b.gtdoc#
  AND a.servicetype = b.gmdept;
  
SELECT c.gmtype, c.gmdept, COUNT(*)
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
--  AND c.gmdept <> 'PD'
WHERE b.gtdtyp = 's'
  AND gtdoc# IN (
    SELECT ro 
    FROM factroline 
    WHERE servicetype = 'EM')
--      AND linedatekey > 3000)    
GROUP BY c.gmtype, c.gmdept;

SELECT c.gmtype, c.gmdept--, COUNT(*)
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
--  AND c.gmdept <> 'PD'
WHERE b.gtdtyp = 's'
  AND gtdoc# IN (
    SELECT ro 
    FROM factroline 
    WHERE servicetype = 'AM')
--      AND linedatekey > 3000)    
GROUP BY c.gmtype, c.gmdept;

SELECT b.gtdoc#, b.gtdtyp, b.gtdate, b.gttamt, b.gtacct, c.gmyear, c.gmtype, c.gmdept    
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
  AND c.gmdept <> 'PD'
WHERE b.gtdtyp = 's'
  AND gtdoc# IN (
    SELECT ro 
    FROM factroline 
    WHERE servicetype = 'AM')
--      AND linedatekey > 3000)    
  AND gmyear = 2012
GROUP BY c.gmtype, c.gmdept;

-- i keep thinking i'm missing something obvious
-- i don't care about gmdept of nc/uc


    
-- paytype may be the distinguishing factor    
-- feeling now LIKE paytype IS a separate slice


DECLARE @ro string;
DROP TABLE #ro;
DROP TABLE #gl;
@ro = '19097515';
SELECT ro, servicetype
INTO #ro
FROM factroline
WHERE ro = @ro
GROUP BY ro, servicetype;
SELECT b.gtdoc#, c.gmtype, c.gmdept, SUM(gttamt) 
INTO #gl   
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
--  AND c.gmdept <> 'PD'
WHERE b.gtdtyp = 's'
  AND b.gtdoc# = @ro
GROUP BY b.gtdoc#, gmdept, gmtype;
SELECT *
FROM #ro a
full JOIN #gl  b ON a.ro = b.gtdoc#
  AND a.servicetype = b.gmdept
ORDER BY gmdept, gmtype  
  
-- this one IS interesting
-- 5250: $66.94 labor sales
-- reversing entry against sales acct 146204: adj for coupon: $17.92
-- 66.94 - 17.92 = 49.02 which IS what shows up AS total above
SELECT * FROM stgArkonaGLPTRNS
WHERE gtdoc# = '19097754'
  
SELECT servicetype, COUNT(*)
FROM factroline
WHERE LEFT(ro, 2) = '19'  
GROUP BY servicetype


SELECT *
FROM factroline
WHERE LEFT(ro, 2) = '19'  
  AND servicetype = 'MR'
  AND linedatekey > 3000
  
19097754  

-- 7/12
what IS the break down for laborsales/cos, to which depts does this notion apply


-- ros to multiple departments
SELECT ro, COUNT(*)
FROM (
SELECT ro, servicetype
FROM factroline
GROUP BY ro, servicetype) a
GROUP BY ro
HAVING COUNT(*) > 2
ORDER BY ro asc, COUNT(*) desc




mind fucking continues IN RODollars2
including a new scrape of pdptdet, trying to associate parts sales/cos to an ro line
the take away FROM this IS TOTAL parts/labor sales/cos per ro

-- exempt coa.dept = NUC, UC
UPDATE factro
SET LaborSales = 0,
    LaborCost = 0,
    PartsSales = 0,
    PartsCost = 0;

UPDATE fro
SET LaborSales = coalesce(x.LaborSales, 0),
    LaborCost = coalesce(x.LaborCost, 0),
    PartsSales = coalesce(x.PartsSales, 0),
    PartsCost = coalesce(x.PartsCost, 0)    
FROM factro fro
LEFT JOIN (
  SELECT b.gtdoc#,
    sum(case when c.gmtype = '4' and c.gmdept <> 'PD' then b.gttamt else 0 END) AS "LaborSales",
    sum(CASE WHEN c.gmtype = '5' and c.gmdept <> 'PD' THEN b.gttamt else 0 END) AS "LaborCost",
    sum(CASE WHEN c.gmtype = '4' and c.gmdept = 'PD' THEN b.gttamt else 0 END) AS "PartsSales",
    sum(CASE WHEN c.gmtype = '5' and c.gmdept = 'PD' THEN b.gttamt else 0 END) AS "PartsCost"
  FROM stgArkonaGLPTRNS b
  INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
    AND year(b.gtdate) = c.gmyear
    AND c.gmtype in ('4', '5')
    AND c.gmdept not in ('NC', 'UC')
  WHERE b.gtdtyp = 's'
    AND b.gtdoc# IN (SELECT ro FROM factro)
  GROUP BY b.gtdoc#) x ON fro.ro = x.gtdoc#  
WHERE ro = fro.ro;



find an example for 19 ro

-- basic accounting piece    
SELECT b.gtdoc#,
  sum(case when c.gmtype = '4' and c.gmdept <> 'PD' then b.gttamt else 0 END) AS "LaborSales",
  sum(CASE WHEN c.gmtype = '5' and c.gmdept <> 'PD' THEN b.gttamt else 0 END) AS "LaborCost",
  sum(CASE WHEN c.gmtype = '4' and c.gmdept = 'PD' THEN b.gttamt else 0 END) AS "PartsSales",
  sum(CASE WHEN c.gmtype = '5' and c.gmdept = 'PD' THEN b.gttamt else 0 END) AS "PartsCost"
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
  AND c.gmdept not in ('NC', 'UC')
WHERE b.gtdtyp = 's'
  AND b.gtdate > curdate() - 90
GROUP BY b.gtdoc#