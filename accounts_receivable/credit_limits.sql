﻿/*
I have a special request that I was hoping you might add to your “to-do” list…. We are trying to 
come up with accurate credit limits for our current accounts receivable customers. I was hoping 
that with your wizard-like skill you might be able to spin me up a report of CURRENT A/R customers 
that includes the average monthly charges for the last 12-18 months and the 
maximum monthly A/R balance for the same period. Do you think this is possible? 

Thanks a million! 

June Gustafson

By current I am referring to any A/R customer that has the payment terms DUE IN FULL BY 10th Status. 
This should cover all accounts that have not been closed due to inactivity or delinquency. Does this answer your question ? 
*/

drop table if exists ar;
create temp table ar as
select a.company_number, a.record_key, a.active, a.customer_number, a.search_name,
  ar_payment_terms, a.credit_limit, b.description as customer_type,
  d.description,
  a.last_statmnt_date, a.last_statment_bal, last_stmt_tran_no
from dds.ext_glpcust a
left join dds.ext_glpctyp b on a.customer_type = b.record_key
  and a.company_number = b.company_number
left join dds.ext_glpptrm d on a.company_number = d.company_number
  and a.ar_payment_terms = d.record_key 
where a.active not in  ('N','V')  -- vendor only, inactive
  and d.record_type = 'R' -- this appears to be the correct one  
  and d.record_key = 1;
create index on ar (customer_number);
create index on ar (customer_type);

select * from ar order by customer_type;

  

select * from dds.ext_glpcust where customer_number = '11450'

select * from dds.tmp_glptrns_122000 a where gtctl_ = '1150950'

select gtctl_, b.yearmonth, c.search_name, customer_type, sum(gttamt), count(*)
from dds.tmp_glptrns_122000 a
inner join dds.day b on a.gtdate = b.thedate
left join ar c on a.gtctl_ = c.customer_number
group by gtctl_, b.yearmonth, c.search_name, customer_type
order by c.search_name, b.yearmonth

-- customers and count of months with transactions
create temp table cust_count as
select gtctl_, search_name, customer_type, count(*)
from (
  select gtctl_, b.yearmonth, c.search_name, customer_type, sum(gttamt), count(*)
  from dds.tmp_glptrns_122000 a
  inner join dds.day b on a.gtdate = b.thedate
  left join ar c on a.gtctl_ = c.customer_number
  group by gtctl_, b.yearmonth, c.search_name, customer_type) a
group by gtctl_, search_name, customer_type 
having count(*) > 10
order by count(*) desc 

select * from cust_count

select * from cust_count a left join ar b on a.gtctl_ = b.customer_number

select * from ar limit 100

-- customer types
select customer_type, count(*) 
from ar 
where company_number = 'RY1' 
  and customer_type not like '%RY3%'
group by customer_type

-- what is the starting balance on any give month?

-- 6/17/17 talked to june, need statement info: avg statement, last statement, last activity

-- 8/12/16 shit it has been a while
for some goofy reason, report2 connection no longer shows table description in aqt
glparsd: a/r statement detail
  pk: company_number, trans_number, control_number, reference_number, tran_seq_number, trans_number_gttrn#
glparsh: a/r statement header
  pk: company_number, transaction_key, cust_type_key, customer_name, customer_number
glparsy: a/r statement customer types
  pk: company_number, transaction_key, cust_type_key

glpptrm: payment terms  
  pk: company_number, record_type, record_key
glpcust: a/r and a/p customer file
  pk: company_number, record_key  
glpctyp: customer type
  pk: company_number, record_key


-- ok, here are the statement header records for abra (11634), 
-- where the fuck do i get statement date
AND this is weird, multiple transaction_keys (multiple rows) with the same last_stmt_tran_no but that last_stmt_tran_no does not appear to exist as a transaction key
eg transaction_key 3395589/3395546/3395520 refer to last_stmt_trn_no 3393487, 3393487 does not exist in glparsh as a transaction_key
select * from dds.ext_glparsh where transaction_key = 3393487
hmmm but 3393487 does exist in glptrns with a control of 11634 and a date of 7/28/16 and account 122000
so maybe it means the last gl transaction in that a/r transaction rather than
what i assumed: the previous a/r transaction_key

though, i do not know why there are ~ 3 rows for each statement


select a.* 
from dds.ext_glparsh a
where a.customer_number = '11634'
order by a.transaction_key desc
limit 400

select a.customer_number, customer_name, current1, last_stmt_tran_no,
  (
    select max(yearmonth)
    from fin.fact_gl e
    inner join dds.day f on e.date_key = f.datekey
    where e.trans = a.last_stmt_tran_no)
from dds.ext_glparsh a
where a.customer_number = '11634'
group by a.customer_number, customer_name, current1, last_stmt_tran_no

drop table if exists wtf;
create temp table wtf as
select a.customer_number, customer_name, current1, last_stmt_tran_no
from dds.ext_glparsh a
group by a.customer_number, customer_name, current1, last_stmt_tran_no;

create index on wtf(last_stmt_tran_no);

drop table if exists wtf1;
create temp table wtf1 as
select customer_number, customer_name, current1, last_stmt_tran_no, max(c.yearmonth) as year_month
from wtf a
left join fin.fact_gl b on a.last_Stmt_tran_no = b.trans
inner join dds.day c on b.date_key = c.datekey
group by customer_number, customer_name, current1, last_stmt_tran_no;

select * from wtf1 order by customer_name, year_month

-- 8/15 -- getting closer
select * from wtf1 where customer_number = '11634'
seems odd to me that abra (11634) only has statement info going back to 201602
whereas there are ar gl transactions all the way back to 2011
select b.thedate, a.*, c.*
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ('122000','222000')
where a.control = '11634'  
order by b.thedate limit 1000

-- so are there even glparsd records corresponding to those gl transactions? 
-- doesn't look like it
drop table if exists abra_gl;
create temp table abra_gl as
select b.thedate, a.*, c.account
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '122000'
where a.control = '11634'  

select * 
from abra_gl a
left join dds.ext_glparsd b on a.trans = b.trans_number_gttrn_
order by a.thedate desc 

select *
from dds.ext_glparsh a
left join dds.ext_glparsd b on a.transaction_key = b.trans_number
  and a.customer_number = b.control_number
where a.customer_number = '11634'


drop table if exists gl_ar_charges;
create temp table gl_ar_charges as 
select b.yearmonth, a.control, sum(a.amount)
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '122000'
where b.theyear = 2016  
  and post_status = 'y'
  and amount > 0
group by b.yearmonth, a.control;  


select * from gl_ar_charges

select b.thedate, a.*
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '122000'
where b.yearmonth = 201606  
  and post_status = 'y'
  and amount > 0
  and a.control = '1105800'
order by trans, seq  

-- 8/15 ------------------------------------------------------------------------------------------------------
-- just talked to june, can probably use gl activity by month and that will be good

1. gl charge activity 1/1/15 -> 7/31/2016

drop table if exists gl_ar_charges;
create temp table gl_ar_charges as 
select b.yearmonth, a.control, sum(a.amount) as amount
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ('122000', '222000')
where b.thedate between '01/01/2015' and '07/31/2016'
  and post_status = 'y'
  and amount > 0
group by b.yearmonth, a.control;  

2. the relevant customers
drop table if exists ar_customers;
create temp table ar_customers as
select a.company_number, a.record_key, a.active, a.customer_number, a.search_name,
  ar_payment_terms, a.credit_limit, b.description as customer_type,
  d.description,
  a.last_statmnt_date, a.last_statment_bal, last_stmt_tran_no
from dds.ext_glpcust a
inner join dds.ext_glpctyp b on a.customer_type = b.record_key
  and a.company_number = b.company_number
inner join dds.ext_glpptrm d on a.company_number = d.company_number
  and a.ar_payment_terms = d.record_key 
where a.active not in  ('N','V')  -- vendor only, inactive
  and d.record_type = 'R' -- this appears to be the correct one  
  and d.record_key = 1;
create index on ar_customers (customer_number);
create index on ar_customers (customer_type);

select a.search_name as customer, a.customer_number, a.credit_limit, a.last_statmnt_date,
  coalesce(sum(case when b.yearmonth = 201501 then round(b.amount, 0) end), 0) as "201501",
  coalesce(sum(case when b.yearmonth = 201502 then round(b.amount, 0) end), 0) as "201502",
  coalesce(sum(case when b.yearmonth = 201503 then round(b.amount, 0) end), 0) as "201503",
  coalesce(sum(case when b.yearmonth = 201504 then round(b.amount, 0) end), 0) as "201504",
  coalesce(sum(case when b.yearmonth = 201505 then round(b.amount, 0) end), 0) as "201505",
  coalesce(sum(case when b.yearmonth = 201506 then round(b.amount, 0) end), 0) as "201506",
  coalesce(sum(case when b.yearmonth = 201507 then round(b.amount, 0) end), 0) as "201507",
  coalesce(sum(case when b.yearmonth = 201508 then round(b.amount, 0) end), 0) as "201508",
  coalesce(sum(case when b.yearmonth = 201509 then round(b.amount, 0) end), 0) as "201509",
  coalesce(sum(case when b.yearmonth = 201510 then round(b.amount, 0) end), 0) as "201510",
  coalesce(sum(case when b.yearmonth = 201511 then round(b.amount, 0) end), 0) as "201511",
  coalesce(sum(case when b.yearmonth = 201512 then round(b.amount, 0) end), 0) as "201512",
  coalesce(sum(case when b.yearmonth = 201601 then round(b.amount, 0) end), 0) as "201601",
  coalesce(sum(case when b.yearmonth = 201602 then round(b.amount, 0) end), 0) as "201602",
  coalesce(sum(case when b.yearmonth = 201603 then round(b.amount, 0) end), 0) as "201603",
  coalesce(sum(case when b.yearmonth = 201604 then round(b.amount, 0) end), 0) as "201604",
  coalesce(sum(case when b.yearmonth = 201605 then round(b.amount, 0) end), 0) as "201605",
  coalesce(sum(case when b.yearmonth = 201606 then round(b.amount, 0) end), 0) as "201606",
  coalesce(sum(case when b.yearmonth = 201607 then round(b.amount, 0) end), 0) as "201607"
from ar_customers a
left join gl_ar_charges b on a.customer_number = b.control
where a.last_statmnt_date > '01/01/2016'
group by a.search_name, a.customer_number, a.credit_limit, a.last_statmnt_date


