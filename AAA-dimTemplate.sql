--< DDL -----------------------------------------------------------------------
-- table
CREATE TABLE /**/ (
  )IN database;
  
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   /*tablename: 'dimVehicle'*/,/*index filename:'dimVehicle.adi'*/,/*indexname: 'PK'*/,
   /*columnname(s): 'VehicleKey'*/,'',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty(/*tablename; 'dimVehicle'*/,'Table_Primary_Key',
  /*index name: 'PK'*/, 'RETURN_ERROR', NULL);   
-- index: NK (unique)
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimVehicle','dimVehicle.adi','NK',
   'Vin', '',2051,512,'' );   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimVehicle','Vin', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
--/> DDL -----------------------------------------------------------------------  
 
--< keyMap --------------------------------------------------------------------
--DROP TABLE keyMapDimVehicle;     
CREATE TABLE keyMapDimVehicle (
  VehicleKeyKey integer,
  Vin cichar(17)) IN database;         
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'keyMapDimVehicle','keyMapDimVehicle.adi','PK',
   'VehicleKeyKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('keyMapDimVehicle','Table_Primary_Key',
  'PK', 'RETURN_ERROR', NULL);   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimVehicle','Vin', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
  
--RI  
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimVehicle-KeyMap');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimVehicle-KeyMap','dimVehicle','keyMapDimVehicle','PK',2,2,NULL,'','');   
--/> keyMap --------------------------------------------------------------------  

--< Dependencies & zProc ------------------------------------------------------
INSERT INTO zProcExecutables values('dimVehicle','daily',CAST('12:15:15' AS sql_time),14);
DELETE FROM zProcProcedures WHERE proc = 'INPMAST';
INSERT INTO zProcProcedures values('dimVehicle','INPMAST',1,'Daily');
INSERT INTO DependencyObjects values('dimVehicle','INPMAST','dimVehicle');
--/> Dependencies & zProc ------------------------------------------------------  

--< initial load --------------------------------------------------------------
SELECT * from stgArkonaINPMAST
  VehicleKey autoinc,
  Vin cichar(17),
  ModelYear cichar(4),
  Make cichar(25),
  Model cichar(25),
  ModelCode cichar (10),
  Body cichar(25),
  Color cichar(25))IN database;

EXECUTE PROCEDURE sp_zaptable('dimVehicle')
DELETE FROM dimVehicle;  
INSERT INTO dimvehicle (Vin, ModelYear, Make, Model, ModelCode, Body, Color) 
values('Unknown','Unkn','Unknown','Unknown','Unknown','Unknown','Unknown');
INSERT INTO dimVehicle (Vin, ModelYear, Make, Model, ModelCode, Body, Color)
SELECT imvin, left(cast(imyear AS sql_char), 4), immake, immodl, immcode, imbody, imcolr FROM stgArkonaINPMAST;  
--/> initial load --------------------------------------------------------------
