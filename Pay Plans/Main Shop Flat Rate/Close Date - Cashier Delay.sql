-- cashier delay DOES NOT have a CLOSE date
SELECT a.ro, b.thedate AS opendate, c.thedate AS closedate, d.rostatus, a.line,
  cc.thedate AS flagdate, e.linestatus
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN day c on a.closedatekey = c.datekey
INNER JOIN day cc on a.flagdatekey = cc.datekey
INNER JOIN dimRoStatus d on a.rostatuskey = d.rostatuskey
INNER JOIN dimLineStatus e on a.statuskey = e.linestatuskey
WHERE b.datekey IN (SELECT datekey FROM day WHERE thedate BETWEEN curdatE() - 180 AND curdatE())
  AND a.storecode = 'ry1'
  AND d.rostatuskey = 6
  
  
SELECT d.rostatus,
    sum(case when c.thedate = '12/31/9999' THEN 1 END) AS "No Close Date",
    sum(case when c.thedate <> '12/31/9999' THEN 1 END) AS "Has Close Date",
    MIN(ro), MAX(ro)    
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN day c on a.closedatekey = c.datekey
INNER JOIN day cc on a.flagdatekey = cc.datekey
INNER JOIN dimRoStatus d on a.rostatuskey = d.rostatuskey
INNER JOIN dimLineStatus e on a.statuskey = e.linestatuskey
WHERE b.datekey IN (SELECT datekey FROM day WHERE thedate BETWEEN curdatE() - 180 AND curdatE())
  AND a.storecode = 'ry1' 
GROUP BY d.rostatus   
  
  
--every combination of rostatus, closedate, finalclosedate

   
SELECT *
FROM stgarkonasdprhdr
WHERE ptro# = '16107256'   

SELECT d.rostatus,
    sum(case when c.thedate = '12/31/9999' THEN 1 END) AS "No Close Date",
    sum(case when c.thedate <> '12/31/9999' THEN 1 END) AS "Has Close Date",
    MIN(ro), MAX(ro)    
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN day c on a.closedatekey = c.datekey
INNER JOIN day cc on a.flagdatekey = cc.datekey
INNER JOIN dimRoStatus d on a.rostatuskey = d.rostatuskey
INNER JOIN dimLineStatus e on a.statuskey = e.linestatuskey
WHERE b.datekey IN (SELECT datekey FROM day WHERE theyear = 2013)
  AND a.storecode = 'ry1' 
GROUP BY d.rostatus   



SELECT * FROM day WHERE yearmonth = 201310


9/22 - 10/5
techs 519, 611, 608, 612

SELECT *
FROM zcb
WHERE name = 'cb2'
  AND dl3 = 'fixed'
  AND dl4 = 'mechanical'
  AND dl2 = 'ry1'
  AND al4 = 'sales'
  AND dl5 = 'main shop'
SELECT COUNT(*) FROM zcb