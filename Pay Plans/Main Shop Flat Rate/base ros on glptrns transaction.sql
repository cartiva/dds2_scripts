-- just for shits AND giggles 
-- limit the ros to only those ros with a laborsales TRANSACTION
-- IN glptrns during the interval
-- it IS no fucking better than USING finalclosedate
-- just fucked up IN different ways
-- total flag hours IS 3583.22
SELECT SUM(flaghours - xtim + adj + neg) FROM( 
SELECT description, SUM(flaghours) AS flaghours, coalesce(SUM(xtimHours),0) AS xtim, 
  coalesce(SUM(adjHours),0) AS adj, coalesce(SUM(negHours),0) AS neg
FROM (
  SELECT ro, c.description, c.technumber, round(SUM(flaghours), 2) AS flaghours 
  FROM factrepairorder a
  INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c on b.techkey = c.techkey
  WHERE a.storecode = 'RY1'
    AND a.ServiceTypeKey IN (
      SELECT ServiceTypeKey
      FROM dimServiceType
      WHERE ServiceTypeCode IN ('am','mr'))
    AND EXISTS (
    SELECT 1
    FROM stgArkonaGLPTRNS 
    WHERE gtdoc# = a.ro
      AND gtdate BETWEEN '09/01/2013' AND '09/30/2013'
      AND gtacct IN (-- Service labor sales accounts
        select glaccount
        FROM zcb a
        WHERE name = 'cb2' -- narrows it down to main shop
          AND dl2 = 'RY1'
          and dl3 = 'fixed'
          and dl5 = 'main shop'
          AND al4 = 'Sales'
          AND gmcoa NOT IN ('466','469') -- elim shop supplies, sublet
          AND split = 1))     
  GROUP BY ro, c.description, c.technumber) m   
LEFT JOIN (  
  SELECT Storecode, ro, technumber,
    coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
    coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
    coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
  FROM stgFlagTimeAdjustments  
  GROUP BY Storecode, ro, technumber) n on m.ro = n.ro AND m.technumber = n.technumber     
WHERE flaghours <> 0    
GROUP BY description
) x