-- october totals
SELECT a.*, b.*, laborsales/flaghours
FROM ( -- flag hours
  SELECT round(SUM(flaghours), 2) AS flaghours, COUNT(DISTINCT ro) AS ros             
  FROM factRepairOrder a
  WHERE storecode = 'ry1'
    AND flaghours <> 0
    AND finalclosedatekey IN (  
      SELECT datekey
      FROM day
      WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
    AND a.ServiceTypeKey IN (
      SELECT ServiceTypeKey
      FROM dimServiceType
      WHERE ServiceTypeCode IN ('am','mr'))) a
INNER JOIN ( -- labor sales   
  SELECT SUM(gttamt) * -1 AS laborsales
  FROM ( -- the ros
    SELECT ro
    FROM factRepairOrder a
    WHERE storecode = 'ry1'
      AND flaghours <> 0
      AND finalclosedatekey IN (  
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro) a  
  LEFT JOIN stgArkonaGLPTRNS d on a.ro = d.gtdoc#   
  WHERE d.gtacct IN (-- Service labor sales accounts
    select glaccount
    FROM zcb a
    WHERE name = 'cb2' -- narrows it down to main shop
      AND dl2 = 'RY1'
      and dl3 = 'fixed'
      and dl5 = 'main shop'
      AND al4 = 'Sales'
      AND gmcoa NOT IN ('466','469') -- elim shop supplies, sublet
      AND split = 1)) b on 1 = 1       
      
-- oct flaghours, COUNT BY tech
SELECT c.description, count(distinct ro), round(SUM(flaghours*weightfactor), 2) AS flaghours  
FROM factRepairOrder a
INNER JOIN brTechGroup b on a.TechGroupKey = b.TechGroupKey
INNER JOIN dimTech c on b.TechKey = c.TechKey
WHERE a.storecode = 'ry1'
--  AND flaghours <> 0
  AND finalclosedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'))
GROUP BY c.description      

-- october for single tech
SELECT ro, round(SUM(flaghours*weightfactor), 2) AS flaghours  
FROM factRepairOrder a
INNER JOIN brTechGroup b on a.TechGroupKey = b.TechGroupKey
INNER JOIN dimTech c on b.TechKey = c.TechKey
  AND c.technumber = '537'
WHERE a.storecode = 'ry1'
--  AND flaghours <> 0
  AND finalclosedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'))
GROUP BY ro
UNION 
SELECT 'AAATotal', round(SUM(flaghours*weightfactor), 2) AS flaghours  
FROM factRepairOrder a
INNER JOIN brTechGroup b on a.TechGroupKey = b.TechGroupKey
INNER JOIN dimTech c on b.TechKey = c.TechKey
  AND c.description = 'PDQ Tech'
WHERE a.storecode = 'ry1'
--  AND flaghours <> 0
  AND finalclosedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'))
    
    
    
CREATE TABLE #566(ro cichar(12), hours double); 
insert into #566 values('16121966',0.5);
insert into #566 values('16126805',0.6);
insert into #566 values('16131212',9);
insert into #566 values('16131270',5.9);
insert into #566 values('16131743',0.5);
insert into #566 values('16131782',1.7);
insert into #566 values('16131795',2.4);
insert into #566 values('16131836',2.4);
insert into #566 values('16131958',0.5);
insert into #566 values('16132044',3.9);
insert into #566 values('16132067',0.7);
insert into #566 values('16132190',1.37);
insert into #566 values('16132194',0.3);
insert into #566 values('16132200',1);
insert into #566 values('16132203',1);
insert into #566 values('16132204',4.3);
insert into #566 values('16132234',0.37);
insert into #566 values('16132235',2.1);
insert into #566 values('16132260',0.97);
insert into #566 values('16132367',2.47);
insert into #566 values('16132369',2.9);
insert into #566 values('16132412',0.5);
insert into #566 values('16132440',0.3);
insert into #566 values('16132494',1.7);
insert into #566 values('16132519',0.5);
insert into #566 values('16132535',1.1);
insert into #566 values('16132547',1.3);
insert into #566 values('16132648',1.7);
insert into #566 values('16132662',1);
insert into #566 values('16132663',1.9);
insert into #566 values('16132742',2.4);
insert into #566 values('16132744',0.5);
insert into #566 values('16132755',0.5);
insert into #566 values('16132841',0.7);
insert into #566 values('16132934',1.3);
insert into #566 values('16132954',1);
insert into #566 values('16133115',0.8);
insert into #566 values('16133135',0.5);
insert into #566 values('16133144',1);
insert into #566 values('16133151',1.77);
insert into #566 values('16133222',4.5);
insert into #566 values('16133241',2.07);
insert into #566 values('16133315',2.3);
insert into #566 values('16133473',2.3);
insert into #566 values('16133510',0.5);
insert into #566 values('16133514',2.8);
insert into #566 values('16133526',0.7);
insert into #566 values('16133779',2.27);
insert into #566 values('16133812',0.8);
insert into #566 values('16133851',1.1);
insert into #566 values('16133909',0.4);
insert into #566 values('16133913',1.5);
insert into #566 values('16133943',2);
insert into #566 values('16133957',4.3);
insert into #566 values('16133961',0.9);
insert into #566 values('16134021',1);
insert into #566 values('16134093',1.6);
insert into #566 values('16134131',1.2);
insert into #566 values('16134152',8.6);
insert into #566 values('16134295',1);
insert into #566 values('16134344',0.5);
insert into #566 values('16134360',0.3);
insert into #566 values('16134361',3);
insert into #566 values('16134373',1.8);
insert into #566 values('16134430',1);
insert into #566 values('16134508',0.8);
insert into #566 values('16134509',0.5);
insert into #566 values('16134513',2.6);
insert into #566 values('16134515',1.7);
insert into #566 values('16134540',3.4);
insert into #566 values('16134543',0.77);
insert into #566 values('16134701',4.3);
insert into #566 values('16134813',0.8);
insert into #566 values('16134845',2.3);
insert into #566 values('16134861',1.2);
insert into #566 values('16135015',1.6);
insert into #566 values('16135052',0.5);
insert into #566 values('18025209',0);
insert into #566 values('18025687',0.5);


DROP TABLE #566a;
SELECT ro, round(SUM(flaghours*weightfactor), 2) AS flaghours 
INTO #566a 
FROM factRepairOrder a
INNER JOIN brTechGroup b on a.TechGroupKey = b.TechGroupKey
INNER JOIN dimTech c on b.TechKey = c.TechKey
  AND c.technumber = '566'
WHERE a.storecode = 'ry1'
--  AND flaghours <> 0
  AND finalclosedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'))
GROUP BY ro  
-- hmm ADD xtim

DROP TABLE #566a;
SELECT a.ro, a.flaghours - coalesce(xHours,0) AS flaghours
INTO #566a 
FROM (
  SELECT ro, c.technumber, round(SUM(flaghours*weightfactor), 2) AS flaghours 
  FROM factRepairOrder a
  INNER JOIN brTechGroup b on a.TechGroupKey = b.TechGroupKey
  INNER JOIN dimTech c on b.TechKey = c.TechKey
    AND c.technumber = '566'
  WHERE a.storecode = 'ry1'
  --  AND flaghours <> 0
    AND finalclosedatekey IN (  
      SELECT datekey
      FROM day
      WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
    AND a.ServiceTypeKey IN (
      SELECT ServiceTypeKey
      FROM dimServiceType
      WHERE ServiceTypeCode IN ('am','mr'))
  GROUP BY ro, c.technumber) a
LEFT JOIN (
  SELECT ptro#, pttech, SUM(ptlhrs) AS xHours
  FROM stgarkonaSDPXTIM
  GROUP BY ptro#, pttech) d on a.ro = d.ptro# AND a.Technumber = d.pttech 

SELECT a.ro AS ArkonaRO, a.hours, b.*
FROM #566 a
full OUTER JOIN #566a b on a.ro = b.ro
WHERE a.ro IS NULL OR b.ro IS NULL 
UNION 
SELECT *
FROM #566 a
full OUTER JOIN #566a b on a.ro = b.ro
WHERE a.hours <> b.flaghours


--< oct including xtime -------------------------------------------------------
SELECT *
FROM (
  SELECT a.ro, c.technumber, round(SUM(a.flaghours*weightfactor), 2) AS flaghours 
  -- INTO #566a 
  FROM factRepairOrder a
  INNER JOIN brTechGroup b on a.TechGroupKey = b.TechGroupKey
  INNER JOIN dimTech c on b.TechKey = c.TechKey
    AND c.technumber = '566'
  WHERE a.storecode = 'ry1'
  --  AND flaghours <> 0
    AND finalclosedatekey IN (  
      SELECT datekey
      FROM day
      WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
    AND a.ServiceTypeKey IN (
      SELECT ServiceTypeKey
      FROM dimServiceType
      WHERE ServiceTypeCode IN ('am','mr'))
  GROUP BY a.ro, c.technumber) a
LEFT JOIN (
  SELECT ptro#, pttech, SUM(ptlhrs) AS xHours
  FROM stgarkonaSDPXTIM
  GROUP BY ptro#, pttech) d on a.ro = d.ptro# AND a.Technumber = d.pttech    

  
-- oct low BY 20 hours 
SELECT COUNT(*) AS ros, round(SUM(flaghours), 2) AS flaghours
FROM (  
  SELECT a.ro, flaghours - coalesce(xhours, 0) AS flaghours
  FROM (   
    SELECT ro, round(SUM(flaghours), 2) AS flaghours      
    FROM factRepairOrder a
    WHERE storecode = 'ry1'
      AND flaghours <> 0
      AND finalclosedatekey IN (  
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro) a       
  LEFT JOIN (
    SELECT ptro#, SUM(ptlhrs) AS xHours
    FROM stgarkonaSDPXTIM
    GROUP BY ptro#) b on a.ro = b.ptro#) c    
    
-- oct low BY 20 hours 
-- ADD techs
SELECT description, COUNT(*) AS ros, round(SUM(flaghours), 2) AS flaghours
FROM (  
  SELECT a.ro, description, flaghours - coalesce(xhours, 0) AS flaghours
  FROM (   
    SELECT ro, c.description, round(SUM(flaghours), 2) AS flaghours      
    FROM factRepairOrder a
    INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dimtech c on b.techkey = c.techkey
    WHERE a.storecode = 'ry1'
      AND flaghours <> 0
      AND finalclosedatekey IN (  
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro, c.description) a       
  LEFT JOIN (
    SELECT ptro#, SUM(ptlhrs) AS xHours
    FROM stgarkonaSDPXTIM
    GROUP BY ptro#) b on a.ro = b.ptro#) c   
GROUP BY description      

DROP TABLE #wtf1;
CREATE TABLE #wtf1(tech cichar(50), ros integer, hours double);
insert into #wtf1 values('QUICK LANE TEAM',1,1.5);
insert into #wtf1 values('PDQ Tech',24,9.83);
insert into #wtf1 values('Loren Shereck',1,1);
insert into #wtf1 values('Shop Misc',1,0.2);
insert into #wtf1 values('Tech 502',24,61.97);
insert into #wtf1 values('Tech 511',73,169.25);
insert into #wtf1 values('Tech 519',84,142.63);
insert into #wtf1 values('Tech 522',31,119.61);
insert into #wtf1 values('Tech 526',123,123.6);
insert into #wtf1 values('Tech 528',21,138.31);
insert into #wtf1 values('Tech 532',26,63.27);
insert into #wtf1 values('Tech 536',49,74.95);
insert into #wtf1 values('Tech 537',66,99.85);
insert into #wtf1 values('Tech 545',21,160.04);
insert into #wtf1 values('Tech 557',111,169.83);
insert into #wtf1 values('Tech 566',79,134.76);
insert into #wtf1 values('Tech 573',70,97.89);
insert into #wtf1 values('Tech 574',19,48.64);
insert into #wtf1 values('Tech 575',96,159.83);
insert into #wtf1 values('Tech 577',22,99.97);
insert into #wtf1 values('Tech 580',66,97.69);
insert into #wtf1 values('Tech 583',86,98.6);
insert into #wtf1 values('Tech 585',103,133.4);
insert into #wtf1 values('Tech 595',58,100.97);
insert into #wtf1 values('Tech 599',131,168.59);
insert into #wtf1 values('Tech 608',85,154);
insert into #wtf1 values('Tech 611',77,125.9);
insert into #wtf1 values('Tech 612',68,119.2);
insert into #wtf1 values('Tech 623',42,80.68);
insert into #wtf1 values('Tech 625',122,155.7);
insert into #wtf1 values('Tech 626',86,94.91);
insert into #wtf1 values('Tech 627',118,122.41);
insert into #wtf1 values('Tech 629',93,117.06);
insert into #wtf1 values('Tech 630',21,20.54);
insert into #wtf1 values('Tech 631',86,152.81);
insert into #wtf1 values('Tech 634',64,115.04);
insert into #wtf1 values('Tech 636',120,131.6);

DROP TABLE #wtf2;
SELECT description, COUNT(*) AS ros, round(SUM(flaghours), 2) AS flaghours
INTO #wtf2
FROM (  
  SELECT a.ro, description, flaghours - coalesce(xhours, 0) AS flaghours
  FROM (   
    SELECT ro, c.description, round(SUM(flaghours), 2) AS flaghours      
    FROM factRepairOrder a
    INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dimtech c on b.techkey = c.techkey
    WHERE a.storecode = 'ry1'
      AND flaghours <> 0
      AND finalclosedatekey IN (  
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro, c.description) a       
  LEFT JOIN (
    SELECT ptro#, SUM(ptlhrs) AS xHours
    FROM stgarkonaSDPXTIM
    GROUP BY ptro#) b on a.ro = b.ptro#) c   
GROUP BY description    

SELECT a.*, b.*, ABS(round(hours-flaghours, 2))
FROM #wtf1 a
full OUTER JOIN #wtf2 b on a.tech = b.description
WHERE hours <> flaghours
ORDER BY  ABS(round(hours-flaghours, 2)) DESC 

-- individual tech
SELECT *
FROM (
  SELECT a.ro, description, flaghours, coalesce(xhours, 0) AS xhours
  FROM (   
    SELECT ro, c.description, round(SUM(flaghours), 2) AS flaghours      
    FROM factRepairOrder a
    INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dimtech c on b.techkey = c.techkey
    WHERE a.storecode = 'ry1'
      AND flaghours <> 0
      AND finalclosedatekey IN (  
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN '10/01/2013' AND '10/31/2013')      
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro, c.description) a       
  LEFT JOIN (
    SELECT ptro#, SUM(ptlhrs) AS xHours
    FROM stgarkonaSDPXTIM
    GROUP BY ptro#) b on a.ro = b.ptro#) x
WHERE description = 'tech 577'    
ORDER BY ro

--/> oct including xtime -------------------------------------------------------

--< oct including stgFlagTimeAdjustments -------------------------------------------------------

DROP TABLE #wtf2;
SELECT description, COUNT(*) AS ros, round(SUM(flaghours), 2) AS flaghours, 
  SUM(xtimhours) AS xtimhours, SUM(adjhours) AS adjhours, SUM(neghours) AS neghours
INTO #wtf2
FROM (  
  SELECT a.ro, description, a.technumber, flaghours, xtimhours, adjhours, neghours
  FROM (   
    SELECT ro, c.description, c.technumber, round(SUM(flaghours), 2) AS flaghours      
    FROM factRepairOrder a
    INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dimtech c on b.techkey = c.techkey
    WHERE a.storecode = 'ry1'
      AND flaghours <> 0
      AND finalclosedatekey IN (  
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN '09/01/2013' AND '09/30/2013')      
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro, c.description, c.technumber) a       
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on a.ro = d.ro AND a.Technumber = d.technumber) c   
GROUP BY description    

SELECT a.*, b.*, flaghours -xtimhours+adjhours+neghours, ABS(round(hours-flaghours -xtimhours+adjhours+neghours, 2))
FROM #wtf1 a
full OUTER JOIN #wtf2 b on a.tech = b.description
WHERE hours <> flaghours -xtimhours+adjhours+neghours
ORDER BY  ABS(round(hours-flaghours, 2)) DESC 


SELECT SUM(flaghours) - SUM(xtimhours) + SUM(adjhours) + SUM(neghours) FROM #wtf2

SELECT SUM(flaghours), SUM(xtimhours), SUM(adjhours), SUM(neghours) FROM #wtf2
--< oct including stgFlagTimeAdjustments -------------------------------------------------------