--< compare full shop to query ------------------------------------------------
DROP TABLE #wtf1;
CREATE TABLE #wtf1(tech cichar(50), ros integer, hours double);
insert into #wtf1 values('QUICK LANE TEAM',3,4.5);
insert into #wtf1 values('PAINT TEAM',1,0.5);
insert into #wtf1 values('Kenneth Ohlson',1,1);
insert into #wtf1 values('PDQ Tech',185,84.22);
insert into #wtf1 values('Chris Walden',1,0.5);
insert into #wtf1 values('Loren Shereck',1,1);
insert into #wtf1 values('Shop Misc',13,2.7);
insert into #wtf1 values('Tech 502',189,399.62);
insert into #wtf1 values('Tech 506',3,2.7);
insert into #wtf1 values('Tech 511',419,759.56);
insert into #wtf1 values('Tech 519',416,813.88);
insert into #wtf1 values('Tech 522',342,620.96);
insert into #wtf1 values('Tech 526',567,590.04);
insert into #wtf1 values('Tech 528',96,609.79);
insert into #wtf1 values('Tech 532',131,189.54);
insert into #wtf1 values('Tech 532',1,0.08);
insert into #wtf1 values('Tech 536',516,869.13);
insert into #wtf1 values('Tech 537',410,605.21);
insert into #wtf1 values('Tech 545',122,710.13);
insert into #wtf1 values('Tech 557',561,765.15);
insert into #wtf1 values('Tech 566',406,641.8);
insert into #wtf1 values('Tech 572',61,343.14);
insert into #wtf1 values('Tech 573',383,539.04);
insert into #wtf1 values('Tech 574',121,413.63);
insert into #wtf1 values('Tech 575',495,735.15);
insert into #wtf1 values('Tech 577',145,619.12);
insert into #wtf1 values('Tech 578',22,19.57);
insert into #wtf1 values('Tech 580',281,457.62);
insert into #wtf1 values('Tech 583',372,398.2);
insert into #wtf1 values('Tech 585',469,680.01);
insert into #wtf1 values('Tech 595',422,628.98);
insert into #wtf1 values('Tech 599',722,817.57);
insert into #wtf1 values('Tech 608',390,788.05);
insert into #wtf1 values('Tech 610',1,1.9);
insert into #wtf1 values('Tech 611',325,591.25);
insert into #wtf1 values('Tech 612',350,513.04);
insert into #wtf1 values('Recon Tech3',1,0.5);
insert into #wtf1 values('Tech 623',227,331.84);
insert into #wtf1 values('Tech 625',652,846.76);
insert into #wtf1 values('Tech 626',377,461.68);
insert into #wtf1 values('Tech 627',597,517.27);
insert into #wtf1 values('Tech 629',456,612.68);
insert into #wtf1 values('Tech 630',191,157.31);
insert into #wtf1 values('Tech 631',384,744.16);
insert into #wtf1 values('Tech 633',131,141.94);
insert into #wtf1 values('Tech 634',251,422.51);
insert into #wtf1 values('Tech 636',416,419.82);


DROP TABLE #wtf2;
SELECT c.description, count(distinct ro), round(SUM(flaghours*weightfactor), 2) AS flaghours  
INTO #wtf2
FROM factRepairOrder a
INNER JOIN brTechGroup b on a.TechGroupKey = b.TechGroupKey
INNER JOIN dimTech c on b.TechKey = c.TechKey
WHERE a.storecode = 'ry1'
  AND flaghours > 0
  AND finalclosedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN (
      SELECT fromdate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -12
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate))
      AND (
      SELECT thrudate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -1
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate)))      
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'))
GROUP BY c.description

SELECT a.*, b.*, ABS(ros-expr) AS CountDiff, round(ABS(hours-flaghours), 2) AS HourDiff
FROM #wtf1 a
full OUTER JOIN #wtf2 b on a.tech = b.description
WHERE (
  a.ros <> b.expr
  OR
  a.hours <> b.flaghours) 
ORDER BY  round(ABS(hours-flaghours), 2) desc       

select * FROM #wtf2

SELECT SUM(flaghours) from #wtf2

--/> compare full shop to query ------------------------------------------------

--< compare tech 545 ros ------------------------------------------------------
CREATE TABLE #wtf545(ro cichar(12), hours double);
insert into #wtf545 values('16117935',15.4);
insert into #wtf545 values('16117974',1);
insert into #wtf545 values('16118311',4.2);
insert into #wtf545 values('16119255',0.5);
insert into #wtf545 values('16119354',9.8);
insert into #wtf545 values('16119380',2.6);
insert into #wtf545 values('16119597',11.3);
insert into #wtf545 values('16119610',6.5);
insert into #wtf545 values('16119752',12.1);
insert into #wtf545 values('16119924',7.5);
insert into #wtf545 values('16119976',0.5);
insert into #wtf545 values('16120049',8.57);
insert into #wtf545 values('16120084',13.5);
insert into #wtf545 values('16120439',6.6);
insert into #wtf545 values('16120552',0.77);
insert into #wtf545 values('16120608',7.7);
insert into #wtf545 values('16120647',1);
insert into #wtf545 values('16120709',4.3);
insert into #wtf545 values('16120741',3.9);
insert into #wtf545 values('16121009',7.3);
insert into #wtf545 values('16121023',10.57);
insert into #wtf545 values('16121051',0.5);
insert into #wtf545 values('16121071',2.3);
insert into #wtf545 values('16121078',1.4);
insert into #wtf545 values('16121705',16.2);
insert into #wtf545 values('16121844',12.4);
insert into #wtf545 values('16121983',0.87);
insert into #wtf545 values('16122119',1.7);
insert into #wtf545 values('16122232',5);
insert into #wtf545 values('16122331',1.3);
insert into #wtf545 values('16122336',7.3);
insert into #wtf545 values('16122430',0.6);
insert into #wtf545 values('16122810',10.5);
insert into #wtf545 values('16122973',1.07);
insert into #wtf545 values('16122978',8.2);
insert into #wtf545 values('16123169',16.3);
insert into #wtf545 values('16123359',13.9);
insert into #wtf545 values('16123539',0.3);
insert into #wtf545 values('16123801',5.2);
insert into #wtf545 values('16123840',9.9);
insert into #wtf545 values('16123971',8);
insert into #wtf545 values('16123977',17.1);
insert into #wtf545 values('16124160',0.5);
insert into #wtf545 values('16124218',1.2);
insert into #wtf545 values('16124232',0.5);
insert into #wtf545 values('16124289',2.6);
insert into #wtf545 values('16124421',0.5);
insert into #wtf545 values('16124649',3.2);
insert into #wtf545 values('16124867',5.7);
insert into #wtf545 values('16124871',24.8);
insert into #wtf545 values('16124955',5.9);
insert into #wtf545 values('16124991',0.5);
insert into #wtf545 values('16125029',7.2);
insert into #wtf545 values('16125101',0.3);
insert into #wtf545 values('16125601',6.7);
insert into #wtf545 values('16125792',1);
insert into #wtf545 values('16125809',30.8);
insert into #wtf545 values('16126032',0.5);
insert into #wtf545 values('16126034',2.3);
insert into #wtf545 values('16126055',0.5);
insert into #wtf545 values('16126098',2.1);
insert into #wtf545 values('16126147',2.9);
insert into #wtf545 values('16126183',0.5);
insert into #wtf545 values('16126270',0.5);
insert into #wtf545 values('16126309',4);
insert into #wtf545 values('16126505',0.5);
insert into #wtf545 values('16126622',4.5);
insert into #wtf545 values('16126639',1);
insert into #wtf545 values('16126858',8.9);
insert into #wtf545 values('16127198',0.4);
insert into #wtf545 values('16127218',0.5);
insert into #wtf545 values('16127219',0.87);
insert into #wtf545 values('16127220',0.5);
insert into #wtf545 values('16127221',0.2);
insert into #wtf545 values('16127992',11);
insert into #wtf545 values('16128479',6.5);
insert into #wtf545 values('16128528',16);
insert into #wtf545 values('16128568',12.6);
insert into #wtf545 values('16128591',3.1);
insert into #wtf545 values('16129106',1);
insert into #wtf545 values('16129119',0.4);
insert into #wtf545 values('16129160',9.5);
insert into #wtf545 values('16129411',2.6);
insert into #wtf545 values('16129660',7.3);
insert into #wtf545 values('16129814',0.8);
insert into #wtf545 values('16130009',1);
insert into #wtf545 values('16130050',8.97);
insert into #wtf545 values('16130265',1.7);
insert into #wtf545 values('16130272',1);
insert into #wtf545 values('16130312',20.5);
insert into #wtf545 values('16130328',21.07);
insert into #wtf545 values('16130337',1);
insert into #wtf545 values('16130347',0.2);
insert into #wtf545 values('16130388',1);
insert into #wtf545 values('16130763',10.4);
insert into #wtf545 values('16130827',10.4);
insert into #wtf545 values('16131025',2.2);
insert into #wtf545 values('16131404',0.4);
insert into #wtf545 values('16131424',1);
insert into #wtf545 values('16131455',1.2);
insert into #wtf545 values('16131580',16.5);
insert into #wtf545 values('16131611',13.2);
insert into #wtf545 values('16131706',0.7);
insert into #wtf545 values('16131899',6.9);
insert into #wtf545 values('16132275',15.2);
insert into #wtf545 values('16132756',2.1);
insert into #wtf545 values('16132930',7.5);
insert into #wtf545 values('16132952',1.3);
insert into #wtf545 values('16133644',0.4);
insert into #wtf545 values('16133650',8.07);
insert into #wtf545 values('16133660',15.5);
insert into #wtf545 values('16133694',10.5);
insert into #wtf545 values('16133970',8.9);
insert into #wtf545 values('16134023',6.1);
insert into #wtf545 values('16134132',0.6);
insert into #wtf545 values('16134233',1.2);
insert into #wtf545 values('16134345',2.8);
insert into #wtf545 values('16134486',3);
insert into #wtf545 values('16134503',15.4);
insert into #wtf545 values('16135029',6);
insert into #wtf545 values('18023752',16.7);
insert into #wtf545 values('19141626',1);


-- original query, including only ros with closedate IN interval
DROP TABLE #wtf545a;
SELECT ro, round(SUM(flaghours*weightfactor), 2) AS flaghours
INTO #wtf545a
FROM factRepairOrder a
INNER JOIN brTechGroup b on a.TechGroupKey = b.TechGroupKey
INNER JOIN dimTech c on b.TechKey = c.TechKey
  AND c.technumber = '545'
WHERE a.storecode = 'ry1'
  AND flaghours > 0
  AND closedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN (
      SELECT fromdate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -12
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate))
      AND (
      SELECT thrudate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -1
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate)))      
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'))
GROUP BY ro  

-- include ros with closedate outside of interval but finalclose IN interval
-- duh, just DO finalclosedate AND see what it looks LIKE
-- yep, that IS it
DROP TABLE #wtf545a;
SELECT ro, round(SUM(flaghours*weightfactor), 2) AS flaghours
INTO #wtf545a
FROM factRepairOrder a
INNER JOIN brTechGroup b on a.TechGroupKey = b.TechGroupKey
INNER JOIN dimTech c on b.TechKey = c.TechKey
  AND c.technumber = '545'
WHERE a.storecode = 'ry1'
  AND flaghours > 0
  AND finalclosedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN (
      SELECT fromdate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -12
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate))
      AND (
      SELECT thrudate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -1
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate)))
--    OR (closedatekey IN (
--          SELECT datekey 
--          FROM day
--          WHERE thedate < '05/19/2013')
--        AND finalclosedatekey IN (
--          SELECT datekey 
--          FROM day
--          WHERE thedate BETWEEN '05/19/2013' AND '11/02/2013')))           
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'))
GROUP BY ro   

SELECT a.ro AS ArkonaRO, a.hours, b.*
FROM #wtf545 a
full OUTER JOIN #wtf545a b on a.ro = b.ro
WHERE a.ro IS NULL OR b.ro IS NULL 
UNION 
SELECT *
FROM #wtf545 a
full OUTER JOIN #wtf545a b on a.ro = b.ro
WHERE a.hours <> b.flaghours
--/> compare tech 545 ros ------------------------------------------------------

oh shit, need to include those that have a finalclosedate IN the range?
-- this doesn't match
SELECT SUM(flaghours)
FROM (
SELECT ro, round(SUM(flaghours), 2) AS flaghours              
FROM factRepairOrder a
WHERE storecode = 'ry1'
  AND closedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN (
      SELECT fromdate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -12
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate))
      AND (
      SELECT thrudate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -1
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate)))             
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'))
GROUP BY ro     
UNION 
SELECT ro, round(SUM(flaghours), 2) AS flaghours              
FROM factRepairOrder a
WHERE storecode = 'ry1'
  AND finalclosedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN (
      SELECT fromdate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -12
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate))
      AND (
      SELECT thrudate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -1
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate)))             
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'))
GROUP BY ro) x  
    

SELECT ro, SUM(flaghours), b.thedate AS CloseDate, c.thedate AS FinalClose
FROM factRepairOrder a
INNER JOIN day b on a.Closedatekey = b.datekey
INNER JOIN day c on a.finalclosedatekey = c.datekey
AND ro IN (
'16117935',
'16117974',
'16118311',
'16119255',
'16119354',
'16119380',
'16119597',
'16119610',
'16119752',
'16119924',
'16119976',
'16120049',
'16120084',
'16120439',
'16120552',
'16120608',
'16120647',
'16120709',
'16120741',
'16121009',
'16121023',
'16121051',
'16121071',
'16121078',
'16121705',
'16121844',
'16121983',
'16122119',
'16122232',
'16122331',
'16122336',
'16122430',
'16122810',
'16122973',
'16122978',
'16123169',
'16123359',
'16123539',
'16123801',
'16123840',
'16123971',
'16123977',
'16124160',
'16124218',
'16124232',
'16124289',
'16124421',
'16124649',
'16124867',
'16124871',
'16124955',
'16124991',
'16125029',
'16125101',
'16125601',
'16125792',
'16125809',
'16126032',
'16126034',
'16126055',
'16126098',
'16126147',
'16126183',
'16126270',
'16126309',
'16126505',
'16126622',
'16126639',
'16126858',
'16127198',
'16127218',
'16127219',
'16127220',
'16127221',
'16127992',
'16128479',
'16128528',
'16128568',
'16128591',
'16129106',
'16129119',
'16129160',
'16129411',
'16129660',
'16129814',
'16130009',
'16130050',
'16130265',
'16130272',
'16130312',
'16130328',
'16130337',
'16130347',
'16130388',
'16130763',
'16130827',
'16131025',
'16131404',
'16131424',
'16131455',
'16131580',
'16131611',
'16131706',
'16131899',
'16132275',
'16132756',
'16132930',
'16132952',
'16133644',
'16133650',
'16133660',
'16133694',
'16133970',
'16134023',
'16134132',
'16134233',
'16134345',
'16134486',
'16134503',
'16135029',
'18023752',
'19141626')
GROUP BY ro, b.thedate, c.thedate



--< compare tech 545 ros ------------------------------------------------------
CREATE TABLE #wtf566(ro cichar(12), hours double);
insert into #wtf566 values('16113943',0.57);
insert into #wtf566 values('16118370',0.3);
insert into #wtf566 values('16118780',2.9);
insert into #wtf566 values('16118861',3.9);
insert into #wtf566 values('16119488',1);
insert into #wtf566 values('16119596',1.7);
insert into #wtf566 values('16119597',1.2);
insert into #wtf566 values('16119697',0.2);
insert into #wtf566 values('16119806',4.7);
insert into #wtf566 values('16119849',1);
insert into #wtf566 values('16119860',1);
insert into #wtf566 values('16119866',1);
insert into #wtf566 values('16119867',3.5);
insert into #wtf566 values('16119922',0.5);
insert into #wtf566 values('16119963',1.1);
insert into #wtf566 values('16120039',0.3);
insert into #wtf566 values('16120042',1.3);
insert into #wtf566 values('16120054',2.37);
insert into #wtf566 values('16120062',4.3);
insert into #wtf566 values('16120073',1.6);
insert into #wtf566 values('16120088',4.3);
insert into #wtf566 values('16120192',0.3);
insert into #wtf566 values('16120194',2);
insert into #wtf566 values('16120289',1.7);
insert into #wtf566 values('16120294',4);
insert into #wtf566 values('16120338',4);
insert into #wtf566 values('16120434',3.9);
insert into #wtf566 values('16120439',0.5);
insert into #wtf566 values('16120447',1.87);
insert into #wtf566 values('16120464',2.17);
insert into #wtf566 values('16120567',0.3);
insert into #wtf566 values('16120572',1);
insert into #wtf566 values('16120584',0.7);
insert into #wtf566 values('16120691',2);
insert into #wtf566 values('16120749',0.5);
insert into #wtf566 values('16120775',0.7);
insert into #wtf566 values('16120835',1.2);
insert into #wtf566 values('16120848',0.5);
insert into #wtf566 values('16120866',1);
insert into #wtf566 values('16120867',1);
insert into #wtf566 values('16120895',1.3);
insert into #wtf566 values('16120909',0.3);
insert into #wtf566 values('16120913',0.67);
insert into #wtf566 values('16120924',1.17);
insert into #wtf566 values('16120980',0.3);
insert into #wtf566 values('16121053',0.3);
insert into #wtf566 values('16121057',2);
insert into #wtf566 values('16121059',2.9);
insert into #wtf566 values('16121069',1.3);
insert into #wtf566 values('16121164',0.4);
insert into #wtf566 values('16121186',0.6);
insert into #wtf566 values('16121200',0.3);
insert into #wtf566 values('16121212',2.3);
insert into #wtf566 values('16121295',0.2);
insert into #wtf566 values('16121306',0.5);
insert into #wtf566 values('16121311',7);
insert into #wtf566 values('16121317',2.1);
insert into #wtf566 values('16121326',5.9);
insert into #wtf566 values('16121333',1);
insert into #wtf566 values('16121452',0.3);
insert into #wtf566 values('16121477',0.3);
insert into #wtf566 values('16121505',1);
insert into #wtf566 values('16121551',1);
insert into #wtf566 values('16121552',0.9);
insert into #wtf566 values('16121583',0.4);
insert into #wtf566 values('16121608',0.8);
insert into #wtf566 values('16121627',0.4);
insert into #wtf566 values('16121692',0.4);
insert into #wtf566 values('16121701',3);
insert into #wtf566 values('16121702',0.2);
insert into #wtf566 values('16121737',0.5);
insert into #wtf566 values('16121746',0.8);
insert into #wtf566 values('16121853',0.2);
insert into #wtf566 values('16121857',0.6);
insert into #wtf566 values('16121878',0.8);
insert into #wtf566 values('16121898',2);
insert into #wtf566 values('16121945',0.9);
insert into #wtf566 values('16121957',0.8);
insert into #wtf566 values('16121966',0.5);
insert into #wtf566 values('16122188',1.4);
insert into #wtf566 values('16122197',0.77);
insert into #wtf566 values('16122211',0.77);
insert into #wtf566 values('16122214',0.3);
insert into #wtf566 values('16122229',1);
insert into #wtf566 values('16122247',0.5);
insert into #wtf566 values('16122283',2.5);
insert into #wtf566 values('16122321',1);
insert into #wtf566 values('16122331',0.5);
insert into #wtf566 values('16122356',1.5);
insert into #wtf566 values('16122407',1.07);
insert into #wtf566 values('16122472',2.7);
insert into #wtf566 values('16122493',2.3);
insert into #wtf566 values('16122511',1.6);
insert into #wtf566 values('16122565',7.3);
insert into #wtf566 values('16122609',0.4);
insert into #wtf566 values('16122618',1);
insert into #wtf566 values('16122827',1.07);
insert into #wtf566 values('16122830',2.77);
insert into #wtf566 values('16122871',1);
insert into #wtf566 values('16122893',2.5);
insert into #wtf566 values('16122896',4.3);
insert into #wtf566 values('16122920',1.3);
insert into #wtf566 values('16122924',1);
insert into #wtf566 values('16122948',0.87);
insert into #wtf566 values('16122957',1.4);
insert into #wtf566 values('16122974',3.1);
insert into #wtf566 values('16122981',0.3);
insert into #wtf566 values('16123037',2.4);
insert into #wtf566 values('16123063',1);
insert into #wtf566 values('16123128',1.8);
insert into #wtf566 values('16123244',1.17);
insert into #wtf566 values('16123277',3.6);
insert into #wtf566 values('16123322',0.4);
insert into #wtf566 values('16123368',0.8);
insert into #wtf566 values('16123379',0.67);
insert into #wtf566 values('16123490',0.6);
insert into #wtf566 values('16123501',0.5);
insert into #wtf566 values('16123505',0.5);
insert into #wtf566 values('16123511',1.5);
insert into #wtf566 values('16123614',5.27);
insert into #wtf566 values('16123664',0.4);
insert into #wtf566 values('16123717',0.7);
insert into #wtf566 values('16123718',0.6);
insert into #wtf566 values('16123792',0.5);
insert into #wtf566 values('16123793',1.3);
insert into #wtf566 values('16123798',0.5);
insert into #wtf566 values('16123816',0.5);
insert into #wtf566 values('16123898',1.7);
insert into #wtf566 values('16124051',2.6);
insert into #wtf566 values('16124145',1.6);
insert into #wtf566 values('16124166',4.1);
insert into #wtf566 values('16124171',0.3);
insert into #wtf566 values('16124178',0.57);
insert into #wtf566 values('16124364',4.6);
insert into #wtf566 values('16124557',4.3);
insert into #wtf566 values('16124846',1.2);
insert into #wtf566 values('16124893',1);
insert into #wtf566 values('16124895',0.4);
insert into #wtf566 values('16124907',2.8);
insert into #wtf566 values('16124909',0.5);
insert into #wtf566 values('16124911',0.6);
insert into #wtf566 values('16124928',1.5);
insert into #wtf566 values('16124948',1.1);
insert into #wtf566 values('16124950',1.4);
insert into #wtf566 values('16124994',4.6);
insert into #wtf566 values('16125025',0.5);
insert into #wtf566 values('16125054',1);
insert into #wtf566 values('16125089',1);
insert into #wtf566 values('16125100',0.3);
insert into #wtf566 values('16125111',1);
insert into #wtf566 values('16125114',1.5);
insert into #wtf566 values('16125169',1);
insert into #wtf566 values('16125243',0.3);
insert into #wtf566 values('16125248',3.5);
insert into #wtf566 values('16125255',3);
insert into #wtf566 values('16125322',0.2);
insert into #wtf566 values('16125351',1);
insert into #wtf566 values('16125357',1.7);
insert into #wtf566 values('16125368',0.3);
insert into #wtf566 values('16125423',3.6);
insert into #wtf566 values('16125465',0.3);
insert into #wtf566 values('16125468',1.6);
insert into #wtf566 values('16125499',2.47);
insert into #wtf566 values('16125519',0.5);
insert into #wtf566 values('16125613',2.77);
insert into #wtf566 values('16125674',2);
insert into #wtf566 values('16125744',0.3);
insert into #wtf566 values('16125751',1.5);
insert into #wtf566 values('16125913',1.87);
insert into #wtf566 values('16125954',6.3);
insert into #wtf566 values('16125955',0.2);
insert into #wtf566 values('16125973',4.3);
insert into #wtf566 values('16125999',3.1);
insert into #wtf566 values('16126038',2.1);
insert into #wtf566 values('16126061',0.5);
insert into #wtf566 values('16126064',1.5);
insert into #wtf566 values('16126071',0.3);
insert into #wtf566 values('16126091',1);
insert into #wtf566 values('16126109',0.5);
insert into #wtf566 values('16126113',2.3);
insert into #wtf566 values('16126114',4.1);
insert into #wtf566 values('16126143',2.4);
insert into #wtf566 values('16126160',0.3);
insert into #wtf566 values('16126175',1.77);
insert into #wtf566 values('16126176',1.4);
insert into #wtf566 values('16126229',0.3);
insert into #wtf566 values('16126262',0.6);
insert into #wtf566 values('16126285',0.4);
insert into #wtf566 values('16126335',0.87);
insert into #wtf566 values('16126338',0.2);
insert into #wtf566 values('16126403',2);
insert into #wtf566 values('16126423',0.5);
insert into #wtf566 values('16126528',0.7);
insert into #wtf566 values('16126529',1);
insert into #wtf566 values('16126579',0.5);
insert into #wtf566 values('16126802',4);
insert into #wtf566 values('16126805',0.6);
insert into #wtf566 values('16127237',2.3);
insert into #wtf566 values('16127274',1);
insert into #wtf566 values('16127275',1);
insert into #wtf566 values('16127276',0.3);
insert into #wtf566 values('16127282',0.5);
insert into #wtf566 values('16127297',4.1);
insert into #wtf566 values('16127329',1.3);
insert into #wtf566 values('16127371',2.2);
insert into #wtf566 values('16127375',0.3);
insert into #wtf566 values('16127381',0.5);
insert into #wtf566 values('16127389',4);
insert into #wtf566 values('16127487',1.5);
insert into #wtf566 values('16127492',0.3);
insert into #wtf566 values('16127495',1.3);
insert into #wtf566 values('16127501',1.2);
insert into #wtf566 values('16127530',1.37);
insert into #wtf566 values('16127594',1.4);
insert into #wtf566 values('16127607',3);
insert into #wtf566 values('16127616',4);
insert into #wtf566 values('16127708',0);
insert into #wtf566 values('16127727',1);
insert into #wtf566 values('16127801',0.9);
insert into #wtf566 values('16127909',1.3);
insert into #wtf566 values('16128005',0.3);
insert into #wtf566 values('16128028',0.4);
insert into #wtf566 values('16128060',4.7);
insert into #wtf566 values('16128097',0.5);
insert into #wtf566 values('16128103',0.87);
insert into #wtf566 values('16128163',0.5);
insert into #wtf566 values('16128174',0.3);
insert into #wtf566 values('16128220',1);
insert into #wtf566 values('16128254',1);
insert into #wtf566 values('16128265',1.3);
insert into #wtf566 values('16128273',1.1);
insert into #wtf566 values('16128283',1.4);
insert into #wtf566 values('16128334',1.6);
insert into #wtf566 values('16128361',7.37);
insert into #wtf566 values('16128415',3.2);
insert into #wtf566 values('16128437',1);
insert into #wtf566 values('16128480',8);
insert into #wtf566 values('16128510',1);
insert into #wtf566 values('16128521',1.1);
insert into #wtf566 values('16128588',7.6);
insert into #wtf566 values('16128607',1.5);
insert into #wtf566 values('16128711',2.7);
insert into #wtf566 values('16128715',0.2);
insert into #wtf566 values('16128734',0.5);
insert into #wtf566 values('16128737',1);
insert into #wtf566 values('16128744',0.4);
insert into #wtf566 values('16128760',1.5);
insert into #wtf566 values('16128807',2.8);
insert into #wtf566 values('16128813',6.7);
insert into #wtf566 values('16128814',0.5);
insert into #wtf566 values('16128826',1.5);
insert into #wtf566 values('16128833',0.4);
insert into #wtf566 values('16128972',0.4);
insert into #wtf566 values('16128988',1);
insert into #wtf566 values('16129107',2.2);
insert into #wtf566 values('16129109',1.2);
insert into #wtf566 values('16129112',1.2);
insert into #wtf566 values('16129120',0.5);
insert into #wtf566 values('16129142',2.2);
insert into #wtf566 values('16129181',0.87);
insert into #wtf566 values('16129305',6.2);
insert into #wtf566 values('16129308',1.2);
insert into #wtf566 values('16129423',0.2);
insert into #wtf566 values('16129424',0.5);
insert into #wtf566 values('16129440',0.5);
insert into #wtf566 values('16129487',0.7);
insert into #wtf566 values('16129529',1);
insert into #wtf566 values('16129555',0.4);
insert into #wtf566 values('16129557',1.2);
insert into #wtf566 values('16129598',0.3);
insert into #wtf566 values('16129668',1.9);
insert into #wtf566 values('16129672',4.1);
insert into #wtf566 values('16129695',1.4);
insert into #wtf566 values('16129832',2.4);
insert into #wtf566 values('16129853',3.8);
insert into #wtf566 values('16129933',0.5);
insert into #wtf566 values('16129940',1.7);
insert into #wtf566 values('16129949',3.9);
insert into #wtf566 values('16129950',1);
insert into #wtf566 values('16129972',5.3);
insert into #wtf566 values('16129990',2.3);
insert into #wtf566 values('16130064',4);
insert into #wtf566 values('16130086',1.07);
insert into #wtf566 values('16130118',0.4);
insert into #wtf566 values('16130309',1);
insert into #wtf566 values('16130313',0.5);
insert into #wtf566 values('16130314',1);
insert into #wtf566 values('16130339',0.4);
insert into #wtf566 values('16130418',0.5);
insert into #wtf566 values('16130444',1.2);
insert into #wtf566 values('16130447',0.4);
insert into #wtf566 values('16130454',0.97);
insert into #wtf566 values('16130524',0.67);
insert into #wtf566 values('16130533',2.7);
insert into #wtf566 values('16130536',0.4);
insert into #wtf566 values('16130564',2.57);
insert into #wtf566 values('16130640',0.5);
insert into #wtf566 values('16130646',1.4);
insert into #wtf566 values('16130671',5.8);
insert into #wtf566 values('16130694',3.9);
insert into #wtf566 values('16130803',1.5);
insert into #wtf566 values('16130891',2.5);
insert into #wtf566 values('16130894',0.67);
insert into #wtf566 values('16130915',0.5);
insert into #wtf566 values('16130972',1.7);
insert into #wtf566 values('16131006',1.2);
insert into #wtf566 values('16131013',2.4);
insert into #wtf566 values('16131015',3.4);
insert into #wtf566 values('16131049',0.37);
insert into #wtf566 values('16131141',2.4);
insert into #wtf566 values('16131171',1);
insert into #wtf566 values('16131172',0.7);
insert into #wtf566 values('16131212',9);
insert into #wtf566 values('16131229',0.4);
insert into #wtf566 values('16131265',0.4);
insert into #wtf566 values('16131268',0.8);
insert into #wtf566 values('16131270',5.9);
insert into #wtf566 values('16131308',0.4);
insert into #wtf566 values('16131400',1.2);
insert into #wtf566 values('16131412',1);
insert into #wtf566 values('16131432',1);
insert into #wtf566 values('16131440',0.7);
insert into #wtf566 values('16131743',0.5);
insert into #wtf566 values('16131782',1.7);
insert into #wtf566 values('16131795',2.4);
insert into #wtf566 values('16131836',2.4);
insert into #wtf566 values('16131958',0.5);
insert into #wtf566 values('16132044',3.9);
insert into #wtf566 values('16132067',0.7);
insert into #wtf566 values('16132190',1.37);
insert into #wtf566 values('16132194',0.3);
insert into #wtf566 values('16132200',1);
insert into #wtf566 values('16132203',1);
insert into #wtf566 values('16132204',4.3);
insert into #wtf566 values('16132234',0.37);
insert into #wtf566 values('16132235',2.1);
insert into #wtf566 values('16132260',0.97);
insert into #wtf566 values('16132367',2.47);
insert into #wtf566 values('16132369',2.9);
insert into #wtf566 values('16132412',0.5);
insert into #wtf566 values('16132440',0.3);
insert into #wtf566 values('16132494',1.7);
insert into #wtf566 values('16132519',0.5);
insert into #wtf566 values('16132535',1.1);
insert into #wtf566 values('16132547',1.3);
insert into #wtf566 values('16132648',1.7);
insert into #wtf566 values('16132653',5.3);
insert into #wtf566 values('16132662',1);
insert into #wtf566 values('16132663',1.9);
insert into #wtf566 values('16132742',2.4);
insert into #wtf566 values('16132744',0.5);
insert into #wtf566 values('16132755',0.5);
insert into #wtf566 values('16132841',0.7);
insert into #wtf566 values('16132934',1.3);
insert into #wtf566 values('16132954',1);
insert into #wtf566 values('16133115',0.8);
insert into #wtf566 values('16133135',0.5);
insert into #wtf566 values('16133144',1);
insert into #wtf566 values('16133151',1.77);
insert into #wtf566 values('16133222',4.5);
insert into #wtf566 values('16133241',2.07);
insert into #wtf566 values('16133315',2.3);
insert into #wtf566 values('16133473',2.3);
insert into #wtf566 values('16133510',0.5);
insert into #wtf566 values('16133514',2.8);
insert into #wtf566 values('16133526',0.7);
insert into #wtf566 values('16133779',2.27);
insert into #wtf566 values('16133812',0.8);
insert into #wtf566 values('16133851',1.1);
insert into #wtf566 values('16133909',0.4);
insert into #wtf566 values('16133913',1.5);
insert into #wtf566 values('16133943',2);
insert into #wtf566 values('16133957',4.3);
insert into #wtf566 values('16133961',0.9);
insert into #wtf566 values('16134021',1);
insert into #wtf566 values('16134093',1.6);
insert into #wtf566 values('16134131',1.2);
insert into #wtf566 values('16134152',8.6);
insert into #wtf566 values('16134295',1);
insert into #wtf566 values('16134344',0.5);
insert into #wtf566 values('16134360',0.3);
insert into #wtf566 values('16134361',3);
insert into #wtf566 values('16134373',1.8);
insert into #wtf566 values('16134430',1);
insert into #wtf566 values('16134508',0.8);
insert into #wtf566 values('16134509',0.5);
insert into #wtf566 values('16134513',2.6);
insert into #wtf566 values('16134515',1.7);
insert into #wtf566 values('16134540',3.4);
insert into #wtf566 values('16134543',0.77);
insert into #wtf566 values('16134701',4.3);
insert into #wtf566 values('16134813',0.8);
insert into #wtf566 values('16134845',2.3);
insert into #wtf566 values('16134861',1.2);
insert into #wtf566 values('16135015',1.6);
insert into #wtf566 values('16135052',0.5);
insert into #wtf566 values('16135131',1);
insert into #wtf566 values('16135148',0.4);
insert into #wtf566 values('16135150',1);
insert into #wtf566 values('16135182',0.77);
insert into #wtf566 values('16135183',0.3);
insert into #wtf566 values('18025209',0);
insert into #wtf566 values('18025687',0.5);
insert into #wtf566 values('19136928',0.8);
insert into #wtf566 values('19141626',1.2);
insert into #wtf566 values('19146383',0.8);




DROP TABLE #wtf566a;
SELECT ro, round(SUM(flaghours*weightfactor), 2) AS flaghours
INTO #wtf566a
FROM factRepairOrder a
INNER JOIN brTechGroup b on a.TechGroupKey = b.TechGroupKey
INNER JOIN dimTech c on b.TechKey = c.TechKey
  AND c.technumber = '566'
WHERE a.storecode = 'ry1'
  AND flaghours > 0
  AND closedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN (
      SELECT fromdate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -12
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate))
      AND (
      SELECT thrudate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -1
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate)))      
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'))
GROUP BY ro    

SELECT a.ro AS ArkonaRO, a.hours, b.*
FROM #wtf566 a
full OUTER JOIN #wtf566a b on a.ro = b.ro
WHERE a.ro IS NULL OR b.ro IS NULL 
UNION 
SELECT *
FROM #wtf566 a
full OUTER JOIN #wtf566a b on a.ro = b.ro
WHERE a.hours <> b.flaghours


SELECT DISTINCT a.*, c.thedate AS FinalCloseDate, d.thedate AS CloseDate
FROM (
  SELECT a.ro AS ArkonaRO, a.hours, b.*
  FROM #wtf566 a
  full OUTER JOIN #wtf566a b on a.ro = b.ro
  WHERE a.ro IS NULL OR b.ro IS NULL) a
LEFT JOIN factrepairorder b on a.arkonaro = b.ro 
LEFT JOIN day c on b.finalclosedatekey = c.datekey 
LEFT JOIN day d on b.closedatekey = d.datekey 
  
--/> compare tech 566 ros ------------------------------------------------------
