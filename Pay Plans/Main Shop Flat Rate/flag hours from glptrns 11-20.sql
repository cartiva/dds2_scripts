-- feel LIKE i am running out of options
-- TRY grouping BY account

DROP TABLE #gl;
SELECT a.gtdate, a.gtdoc#, gtacct, sum(gttamt) AS gttamt, gtjrnl, gtdesc, COUNT(*)
INTO #gl
FROM stgArkonaGLPTRNS a
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
  AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
group by a.gtdate, a.gtdoc#, gtacct, gtjrnl, gtdesc  

-- GROUP ro BY serv/paytype 
DROP TABLE #ro;
SELECT ro, servicetypekey, paymenttypekey, sum(laborsales) AS sales
INTO #ro
FROM factRepairOrder b 
WHERE ro  IN ( SELECT gtdoc# FROM #gl)
  AND b.ServiceTypeKey IN (
    SELECT ServiceTypeKey 
    FROM dimServiceType 
    WHERE ServiceTypeCode IN ('AM','EM','MR'))      
GROUP BY ro, servicetypekey, paymenttypekey




-- ALL tech totals
SELECT description, round(SUM(flaghours), 2) AS flaghours FROM ( 
--SELECT round(SUM(flaghours),2) FROM (
SELECT distinct d.ro, d.description, e.paymenttypecode, d.flaghours
FROM (
  SELECT DISTINCT b.*
  FROM #gl a
  inner JOIN #ro b on a.gtdoc# = b.ro AND abs(a.gttamt) = abs(b.sales)) c
LEFT JOIN #roTech d on c.ro = d.ro AND c.paymenttypekey = d.paymenttypekey
LEFT JOIN dimPaymentType e on d.paymentTypeKey = e.paymenttypekey
WHERE d.flaghours <> 0
-- ORDER BY d.description, e.paymenttypecode, d.ro
) x GROUP BY description
ORDER BY description

-- 1 tech total on paytype
SELECT description, paymenttypecode, round(SUM(flaghours), 2) AS flaghours 
FROM ( 
  SELECT distinct d.ro, d.description, e.paymenttypecode, d.flaghours
  FROM (
    SELECT DISTINCT b.*
    FROM #gl a
    inner JOIN #ro b on a.gtdoc# = b.ro AND abs(a.gttamt) = abs(b.sales)) c
  LEFT JOIN #roTech d on c.ro = d.ro AND c.paymenttypekey = d.paymenttypekey
  LEFT JOIN dimPaymentType e on d.paymentTypeKey = e.paymenttypekey
  WHERE d.flaghours <> 0) x 
WHERE description = 'Tech 537'  
GROUP BY description, paymenttypecode
ORDER BY description, paymenttypecode

-- 1 tech ALL ros for a paytype
SELECT ro, description, paymenttypecode, round(SUM(flaghours), 2) AS flaghours 
FROM ( 
  SELECT distinct d.ro, d.description, e.paymenttypecode, d.flaghours
  FROM (
    SELECT DISTINCT b.*
    FROM #gl a
    inner JOIN #ro b on a.gtdoc# = b.ro AND abs(a.gttamt) = abs(b.sales)) c
  LEFT JOIN #roTech d on c.ro = d.ro AND c.paymenttypekey = d.paymenttypekey
  LEFT JOIN dimPaymentType e on d.paymentTypeKey = e.paymenttypekey
  WHERE d.flaghours <> 0) x 
WHERE description = 'Tech 537'
  AND paymenttypecode = 'i'  
GROUP BY ro, description, paymenttypecode
ORDER BY ro

536 i 8 vs 8.5 : negative hour adjustment (16134340), but this ro IS NOT IN the GROUP of ros FROM #gl
  ALL that means IS that the ro did NOT CLOSE IN the interval
  need a date IN adjustments TABLE, methinks
  
537 i 4.7 vs 4.4
query missing 16133882
SELECT * FROM #gl WHERE gtdoc# = '16133882'
select * FROM #ro WHERE ro = '16133882'
SELECT * FROM #rotech WHERE ro = '16133882'
