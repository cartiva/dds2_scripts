refactor edwClockHoursFact to factClockHours 
will have to change ALL the references to edwClockHoursFact
-- 1. PYPCLOCKIN
-- remove FROM stgZapAndReload
DELETE 
FROM zprocProcedures
WHERE proc = 'pypclockin';

INSERT INTO zProcExecutables values('extPYPCLOCKIN','daily',CAST('12:15:15' AS sql_time),0);
INSERT INTO zProcProcedures values('extPYPCLOCKIN','tmpPYPCLOCKIN',0,'daily');
INSERT INTO DependencyObjects values('extPYPCLOCKIN','tmpPYPCLOCKIN','tmpPYPCLOCKIN');

-- 9/22
a full blown refactoring of Employee an ClockHours IS more than i am willing to take on right now, so just 
start with moving exe from the Executables group to the EXE group, 
separating clock hours FROM dimEmployee


changed UPDATE propert of RI Exe-Proc to Cascade
changed zProcExecutables.executable FROM EmployeeDim to dimEmployee

DELETE FROM zProcProcedures WHERE proc = 'fctClockHours';
DELETE FROM zProcProcedures WHERE proc = 'fctClockMinutesByHour';
INSERT INTO zProcExecutables values ('factClockHours','daily', CAST('01:25:01' AS sql_time),0);
INSERT INTO zProcProcedures values ('factClockHours','factClockHours', 0, 'daily');
INSERT INTO DependencyObjects values ('factClockHours','factClockHours','factClockHours');

INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b on 1 = 1
  WHERE a.dObject = 'factClockHours'
  AND b.dObject = 'tmpPYPCLOCKIN';    
  

Something that has been bothering me, that there are so many rows IN edwClockHoursFact 
with 0 clockhours
it has to DO with how i am generating the rows, ie, based on a row for every
day based on an employeekey From/Thru
this should be more LIKE what i am doing with ros, IF there are clock hours, THEN
there IS a row

more 0 hour rows than non 0 rows 
SELECT 
  SUM(CASE WHEN clockhours + vacationhours + ptohours + holidayhours <> 0 THEN 1 END) AS HasHours,  
  SUM(CASE WHEN clockhours + vacationhours + ptohours + holidayhours = 0 THEN 1 END) AS NoHours
FROM edwClockHoursFact 

this one IS NOT so bad
SELECT
  SUM(CASE WHEN minutes > 0 THEN 1 END) AS HasMinutes,
  SUM(CASE WHEN minutes = 0 THEN 1 END) AS NoMinutes
FROM factClockMinutesByHour

DO i really want to change this shit up now??

-- current fctClockHours
-- INSERT INTO edwClockHoursFact
  SELECT 
    (SELECT employeekey FROM edwEmployeeDim WHERE employeenumber = e.employeenumber AND d.thedate BETWEEN employeekeyfromdate AND employeekeythrudate) AS EmployeeKey,
    d.datekey, 
    coalesce(h.clockhours, 0) AS ClockHours,
    coalesce(o.regularhours, 0) AS RegularHours,
    coalesce(o.OvertimeHours, 0) AS OvertimeHours, 
    coalesce(h.vachours, 0) AS VacationHours,
    coalesce(h.ptohours, 0) AS PTOHours,
    coalesce(h.holhours, 0) AS HolidayHours 
  FROM day d -- one row per day going back 4 weeks FROM last sunday thru today
  LEFT JOIN -- 1 row per empKey/day, should also be emp#/day, granularity of empDim IS day! there can NOT be multiple records for an emp# ON any given day
    edwEmployeeDim e ON d.thedate BETWEEN e.EmployeekeyFromDate AND e.EmployeeKeyThruDate
  LEFT JOIN ( -- one row per co/emp#/clockindate
      SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
        SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
      FROM tmpClockHours
      GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind   
    AND e.employeenumber = h.yiemp#    
  LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind -- one row per emp#/clockindate
    AND h.yico# = o.yico#
    AND h.yiemp# = o.yiemp#  
  WHERE d.thedate BETWEEN (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate();

SELECT * FROM tmpOvertime
  

-- ok, i DO believe that WHEN an employee IS termed, the last employeekey remains open
SELECT  storecode, employeenumber, employeekey, name, termdate, employeekeyfromdate, 
  employeekeythrudate, currentrow, active 
FROM   edwEmployeeDim a
WHERE EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE employeenumber = a.employeenumber
    AND termdate BETWEEN curdate() - 60 AND curdate())
ORDER BY employeenumber, employeekey    



-- ok, this IS going to generate some rows with NULL employeekey
-- eg Abdulllah Bashir, 110062, HireDate (& EKFromDate) = 9/1/13, has clockhours on 8/30 & 8/31
--    Alvarado 11770 HireDate 9/9  clockhours 9/3,9/4,9/5
-- AND there are no rows for those dudes on those dates IN edwClockHoursFact !!
SELECT a.*, b.regularhours, b.overtimehours, datekey, d.employeekey
FROM (
  SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
    SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
  FROM tmpClockHours
  GROUP BY yico#, yiemp#, yiclkind) a
LEFT JOIN tmpOvertime b on a.yico# = b.yico#
  AND a.yiemp# = b.yiemp#
  AND a.yiclkind = b.yiclkind
LEFT JOIN day c on a.yiclkind = c.thedate  
LEFT JOIN edwEmployeeDim d on a.yico# = d.storecode
  AND a.yiemp# = d.employeenumber
  AND a.yiclkind BETWEEN d.employeekeyfromdate AND d.employeekeythrudate
WHERE employeekey IS NULL   

-- no dup rows  
SELECT yiclkind, yiemp#
FROM (
  SELECT a.*, b.regularhours, b.overtimehours, datekey, employeekey
  FROM (
    SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
      SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
    FROM tmpClockHours
    GROUP BY yico#, yiemp#, yiclkind) a
  LEFT JOIN tmpOvertime b on a.yico# = b.yico#
    AND a.yiemp# = b.yiemp#
    AND a.yiclkind = b.yiclkind
  LEFT JOIN day c on a.yiclkind = c.thedate  
  LEFT JOIN edwEmployeeDim d on a.yico# = d.storecode
    AND a.yiemp# = d.employeenumber
    AND a.yiclkind BETWEEN d.employeekeyfromdate AND d.employeekeythrudate) x
GROUP BY yiclkind, yiemp# 
HAVING COUNT(*) > 1

-- handles multiple empkeys for an emp# IN the date range ok
SELECT a.*, b.regularhours, b.overtimehours, datekey, d.employeekey
FROM (
  SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
    SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
  FROM tmpClockHours
  GROUP BY yico#, yiemp#, yiclkind) a
LEFT JOIN tmpOvertime b on a.yico# = b.yico#
  AND a.yiemp# = b.yiemp#
  AND a.yiclkind = b.yiclkind
LEFT JOIN day c on a.yiclkind = c.thedate  
LEFT JOIN edwEmployeeDim d on a.yico# = d.storecode
  AND a.yiemp# = d.employeenumber
  AND a.yiclkind BETWEEN d.employeekeyfromdate AND d.employeekeythrudate
WHERE a.yiemp# IN ( 
  SELECT yiemp#
  FROM (
    SELECT yiemp#, employeekey
    FROM (
      SELECT a.*, b.regularhours, b.overtimehours, datekey, employeekey
      FROM (
        SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
          SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
        FROM tmpClockHours
        GROUP BY yico#, yiemp#, yiclkind) a
      LEFT JOIN tmpOvertime b on a.yico# = b.yico#
        AND a.yiemp# = b.yiemp#
        AND a.yiclkind = b.yiclkind
      LEFT JOIN day c on a.yiclkind = c.thedate  
      LEFT JOIN edwEmployeeDim d on a.yico# = d.storecode
        AND a.yiemp# = d.employeenumber
        AND a.yiclkind BETWEEN d.employeekeyfromdate AND d.employeekeythrudate) x
    GROUP BY yiemp#, employeekey) y
  GROUP BY yiemp#
  HAVING COUNT(*) > 1) 
  
--9/21
so what i am thinking
fact rows ONLY for rows with hours
need to account for AND accomodate the clockhours before hiredate anomalies
either exclude those rows with a NULL employeekey OR include an UNKNOWN employeekey
either way, expose the anomalies, probably IN maintDimEmp

THEN i will just fucking ADD the rows to the fact TABLE with the appropriate
employeekey

-- 9/22 
fuck me, the whole notion of doing last x number of days means that the anomalies
reappear with every scrape for however long they are IN the date range

SELECT * FROM tmpPYPCLOCKIN

this IS WHERE i always mindfuck,
  DELETE 
  FROM stgArkonaPYPCLOCKIN
  WHERE yiclkind >= (
    SELECT MIN(yiclkind)
    FROM tmpPYPCLOCKIN); 
  INSERT INTO stgArkonaPYPCLOCKIN
  SELECT * FROM tmpPYPCLOCKIN;  
what IF there are rows IN stgArkonaPYPCLOCKIN that are IN that date range
but those rows are NOT IN tmpPYPCLOCKIN
instead i think i should be doing a MERGE
IF the row EXISTS IN tmpPYPCLOCKIN, shit based on what

-- 9/22
--< fucking great, a new anomaly ----------------------------------------------
someone has clocked IN but has NOT clocked out:

SELECT *    
FROM tmpPYPCLOCKIN a
WHERE yiemp# = '1110300'
AND yiclkind = '09/20/2013'

SELECT a.*, b.name, b.managername, b.payrollclass   
FROM tmpPYPCLOCKIN a
LEFT JOIN edwEmployeeDim b on a.yico# = b.storecode
  AND a.yiemp# = b.employeenumber
  AND a.yiclkind BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
WHERE yicode = 'i'
  AND yiclkind BETWEEN '08/25/2013' AND '09/07/2013'
ORDER BY name, yiclkind

SELECT * FROM stgArkonaPYPCLOCKIN
WHERE yiemp# = '110010'
AND yiclkind = '08/26/2013'

compare to paycheck
tori babinski 110010
8/26 clocked IN , did NOT clock out
payperiod 8/25 - 9/7
checkbatch 913130
-- so, edwClockHoursFact agrees with total hours on the paycheck
-- which raises the question, what about her 8:30 AM clock IN on 8/26
-- was she actually here? did she work?, IF she did, she did NOT get paid for it
-- edwClockHoursFact shows 2 later clockin/clockout on the 26th
SELECT a.*, c.thedate
--SELECT SUM(clockhours)
FROM edwClockHoursFact a
INNER JOIN edwEmployeeDim b on a.employeekey = b.employeekey
  AND b.employeenumber = '110010'
INNER JOIN day c on a.datekey = c.datekey
  AND c.thedate BETWEEN '08/25/2013' AND '09/07/2013'
  
-- lets TRY another
Amanda Hopkins 167812  
09/07 clocked IN, did NOT clock out
payperiod 8/25 - 9/7
checkbatch 913130
SELECT a.*, c.thedate
--SELECT SUM(clockhours)
FROM edwClockHoursFact a
INNER JOIN edwEmployeeDim b on a.employeekey = b.employeekey
  AND b.employeenumber = '167812'
INNER JOIN day c on a.datekey = c.datekey
  AND c.thedate BETWEEN '08/25/2013' AND '09/07/2013'
-- edwClockHoursFact agrees with paycheck
-- IF she worked on 9/7, she didn't get paid for it  

Brian Peterson 1110425
08/27 clocked IN, did NOT clock out
SELECT a.*, c.thedate
--SELECT SUM(regularhours), SUM(overtimehours)
FROM edwClockHoursFact a
INNER JOIN edwEmployeeDim b on a.employeekey = b.employeekey
  AND b.employeenumber = '1110425'
INNER JOIN day c on a.datekey = c.datekey
  AND c.thedate BETWEEN '08/25/2013' AND '09/07/2013'
  
he was paid for the morning hours, but because he did NOT clock out IN the 
afternoon, he was NOT paid for those hours  


-- upcoming paycheck
SELECT a.*, b.name, b.managername, b.pydeptcode, b.pydept
FROM tmpPYPCLOCKIN a
INNER JOIN edwEmployeeDim b on a.yico# = b.storecode
  AND a.yiemp# = b.employeenumber
  AND a.yiclkind BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  AND b.payrollclass = 'hourly'
WHERE yicode = 'i'
  AND yiclkind BETWEEN '09/08/2013' AND '09/21/2013'
ORDER BY name, yiclkind
  
    
--/> fucking great, a new anomaly ----------------------------------------------
  
so, what are ALL the anomalies
overlap
dup
clock hours before hiredate
clockin with no clockout  

--< Executables, procs, dependencies
  