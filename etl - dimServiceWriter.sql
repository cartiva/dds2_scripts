/*
3/18
  needs to be better IN anticipation of service checkout
DROP TABLE zdimServiceWriter;  
CREATE TABLE zdimServiceWriter (
  StoreCode cichar(3),
  WriterNumber cichar(3),
  Name cichar(25),
  EmployeeNumber cichar(7),
  dtUserName cichar(20),
  active logical) IN database;  
*/
Need to scrape sdpswtr
SEPUSER for dtUserName
sdpswtr:
  storecode
  writernumber 
  ~name~ has to be matched to edwEmployeeDim to get:
    ~employeenumber
?? what DO i DO with the name info IN sdpswtr, store it IN a description field?
?? departmental writers: CA
?? similar issue to techs: the active field IS useful for the deptmental COUNT
           but cannot be a limit for ros, any writer ON an ro IS valid regardless
           of HR (dimEmp) OR sdpswtr.active
/*
DROP TABLE zsepuser;
CREATE TABLE zSEPUSER (
  storecode cichar(3),
  dtUserName cichar(10),
  name cichar(20),
  firstname cichar(20),
  lastname cichar(20)) IN database;        
  

-- FROM aqt.etl - sepuser.sql
Insert into zsepuser values('RY1', 'RYDEAARONH', 'AARON HOMMERDING', 'AARON', 'HOMMERDING');

*/   

SELECT * 
FROM zsepuser ORDER BY lastname


SELECT a.storecode, a.name AS edName, LEFT(a.name,position(',' IN a.name) -1) AS edLastName, b.*
FROM edwEmployeeDim a
LEFT JOIN zsepuser b ON LEFT(a.name,position(',' IN a.name) -1) = b.lastname
  AND a.storecode = b.storecode

-- does NOT account for sepuser first name = jeff, edwEmployeeDim first name = jeffrey  
SELECT *
FROM (  
  SELECT storecode, name,
    CASE   
      WHEN position(' ' IN trim(substring(name, position(',' IN name) + 1, 25))) = 0 THEN
        trim(substring(name, position(',' IN name) + 1, 25))
      ELSE  
        LEFT(trim(substring(name, position(',' IN name) + 1, 25)), position(' ' IN trim(substring(name, position(',' IN name) + 1, 25))))
    END AS edFirstName,
    LEFT(name,position(',' IN name) -1) AS edLastName
  FROM edwEmployeeDim 
  WHERE currentrow = true
    AND active = 'active') a
LEFT JOIN zsepuser b ON a.storecode = b.storecode
  AND a.edfirstname = b.firstname
  AND a.edlastname = b.lastname 


DROP TABLE stgArkonaSDPSWTR;
CREATE TABLE stgArkonaSDPSWTR (
  swco# cichar(3),
  swswid cichar(3),
  swname cichar(20),
  swemp# cichar(9),
  swactive cichar(1)) IN database;
-- FROM aqt etl - sdpswtr.sql  
-- Insert into stgArkonaSDPSWTR values('RY1', '999', 'Arkona Support', '', 'Y');


need to account for ALL writers FROM the past AS well
SELECT *
FROM (
  SELECT storecode, writerid, COUNT(*)
  FROM factro
  GROUP BY storecode, writerid) a
LEFT JOIN stgArkonaSDPSWTR b ON a.storecode = b.swco#
  AND a.writerid = b.swswid   
-- bingo  
SELECT storecode, writerid
FROM ( 
  SELECT *
  FROM (
    SELECT storecode, writerid, COUNT(*)
    FROM factro
    GROUP BY storecode, writerid) a
  LEFT JOIN stgArkonaSDPSWTR b ON a.storecode = b.swco#
    AND a.writerid = b.swswid  ) c  
GROUP BY storecode, writerid 
HAVING COUNT(*) > 1    
  
/* 
this IS what i fucking hate, dt user name
IS it an attribute of dimEmp, that would seem easiest,
but no, an employee can have logins for both stores
are they different?
-- yep, sometimes\
-- IN each of these cases the name IS spelled differently for the multiple user names
select *
FROM zsepuser
WHERE dtusername IN (
  SELECT dtusername
  FROM (
    SELECT dtusername, name
    FROM zsepuser
    GROUP BY dtusername, name) a
    GROUP BY dtusername HAVING COUNT(*) > 1) 
ORDER BY dtusername

SELECT * FROM zsepuser WHERE dtusername IN ('RYDEBEVERL','RYDEBRADS','RYDEBRADS','RYDEJENNIF') ORDER BY dtusername

SELECT * FROM zsepuser ORDER BY dtusername 

it's unfortunately looks to be a  largely manual process
Grain: store/writer#

*/
-- oh yeah, AND writer changes dept, what THEN
-- what dept applies to the cashier (AS a writer) ?
-- initial scrape of sdpswtr
SELECT * FROM stgarkonasdpswtr
-- have to have some notion of dept
DROP TABLE zdimServiceWriter;  
CREATE TABLE zdimServiceWriter (
  StoreCode cichar(3),
  WriterNumber cichar(3),
  Name cichar(25),
  EmployeeNumber cichar(7),
  dtUserName cichar(20),
  active logical) IN database;  
  
-- oh shit, here we go
so for each writer COUNT of flagdept
-- DROP TABLE #wtf;
SELECT a.swco#, a.swswid, a.swname, f.flagdept, COUNT(*)
INTO #wtf
FROM stgArkonaSDPSWTR a
inner JOIN factro b ON a.swswid = b.writerid
  AND b.void = false
inner JOIN factroline c ON b.ro = c.ro
  AND c.lineflaghours > 0
LEFT JOIN factTechLineFlagDateHours d ON c.ro = d.ro AND c.line = d.line
LEFT JOIN bridgetechgroup e ON d.techgroupkey = e.techgroupkey
LEFT JOIN dimtech f ON e.techkey = f.techkey
GROUP BY a.swco#, a.swswid, a.swname, f.flagdept


SELECT a.swco#, a.swswid, a.swname, a.flagdept,  
FROM #wtf a
INNER JOIN #wtf b ON a.swco# = b.swco#
  AND a.swswid = b.swswid
  AND a.swname = b.swname
  AND a.flagdept = (
    SELECT flagdept
    FROM #wtf
    WHERE swswid
GROUP BY a.swco#, a.swswid, a.swname, a.flagdept

-- 3/31/13
-- fuck ALL this, need to get what i need for SCO

ALTER TABLE dimservicewriter
ADD COLUMN DefaultServiceType cichar(2)
ADD COLUMN Department cichar(12);

-- need some way to ascertain who the actual fucking writers are
-- who surfaces AS the writers that need to be doing checkouts
-- ALL sorts of issues here,
-- cull out the subset of writers needed for SCO
SELECT a.swco#, a.swswid, a.swname, a.swactive, a.swmisc,
  CASE a.swmisc
    WHEN 'MR' THEN 'Main Shop'
    WHEN 'AM' THEN 'Main Shop'
    WHEN 'QL' THEN 'PDQ'
    WHEN 'RE' THEN 'Detail'
    WHEN 'BS' THEN 'Body Shop'
  END,
  b.writernumber 
FROM stgArkonaSDPSWTR a  
LEFT JOIN dimServiceWriter b ON a.swco# = b.storecode
  AND a.swswid = b.writernumber
WHERE a.swco# IN ('ry1','ry2')
  AND a.swactive = 'Y'
ORDER BY swco#, swmisc


-- cull out the subset of writers needed for SCO
-- shane no longer active, leave out al berry
SELECT a.swco#, a.swswid, a.swname, a.swactive, a.swmisc,
  CASE a.swmisc
    WHEN 'MR' THEN 'Main Shop'
    WHEN 'AM' THEN 'Main Shop'
    WHEN 'QL' THEN 'PDQ'
    WHEN 'RE' THEN 'Detail'
    WHEN 'BS' THEN 'Body Shop'
  END,
  b.writernumber 
FROM stgArkonaSDPSWTR a  
LEFT JOIN dimServiceWriter b ON a.swco# = b.storecode
  AND a.swswid = b.writernumber
WHERE a.swco# = 'ry1'
--  AND swswid IN ('402','645','704','705','714','720','426','721','428','429')
  AND swswid IN ('402','403','645','705','714','720','426','428','429')
  AND a.swactive = 'Y'
  AND a.swmisc = 'mr'
ORDER BY swswid

-- ry2
SELECT a.swco#, a.swswid, a.swname, a.swactive, a.swmisc,
  CASE a.swmisc
    WHEN 'MR' THEN 'Main Shop'
    WHEN 'AM' THEN 'Main Shop'
    WHEN 'QL' THEN 'PDQ'
    WHEN 'RE' THEN 'Detail'
    WHEN 'BS' THEN 'Body Shop'
  END,
  b.writernumber 
FROM stgArkonaSDPSWTR a  
LEFT JOIN dimServiceWriter b ON a.swco# = b.storecode
  AND a.swswid = b.writernumber
WHERE a.swco# = 'ry2'
  AND a.swactive = 'Y'
  AND a.swmisc = 'mr'
ORDER BY swswid
  