/************ 3/21/12 ****************/
/***************** PDPPDET ****************************/

DECLARE @ro string;
DECLARE @key integer;

@ro = '16085133'; @key = 423141; 

select 
  (select count(*) from stgArkonaglptrns where gtdoc# = @ro) as glptrns,
  (select count(*) from stgArkonasdprhdr where ptro# = @ro) as sdprhdr,
  (select count(*) from stgArkonasdprdet where ptro# = @ro) as sdprdet,
  (select count(*) from stgArkonapdpphdr where ptdoc# = @ro) as pdpphdr,
  (select count(*) from stgArkonapdppdet where ptpkey = @key) as pdppdet
from system.iota;

SELECT ptco#, ptpkey, ptline, ptltyp, ptseq#, ptcode, ptdate, ptmeth, ptlsts,
  ptsvctyp, ptlpym, pttech, ptlopc, ptcrlo, ptlhrs, ptlchr, ptfact
FROM stgArkonaPDPPDET  



PTCODE - Transaction Code
AP = Add Part
BO = Back Ordered
CM = Comment
CN = Cancelled
CP = RO Customer Pay Sale
CQ = Converted Quantity
CR = RO Correction
CS = RO Cause
DC = Discounts
DP = Delete Part
FR = Factory Return
GC = Stock Group Change
IA = Manual Inventory Adjustment
IS = RO Internal Sale
LA = Lifo Adjust
LS = Lost Sale
MP = Merged Part
OF = Fees
OR = Ordered
PA = Physical Inventory Adjust
PC = Part count from last physical inventory
PO = Purchase order
RC = Special Order Receipt
RC = Received
RO = Reorder
RS = Restocking charge
RT = Counter Return
SA = Counter Sale
SC = RO Service Contract Sale
SH = Shipping
SL = Sublet
SR = RO Return
TT = RO Tech Time
WS = RO Warranty sale
ZA = assign core part
ZR = remove core part

-- ptlsts: line status
select ptlsts, count(*)
--from stgArkonapdppdet
FROM #wtf
group by ptlsts

-- codes for status = C
select ptcode, count(*)
--from stgArkonapdppdet
FROM #wtf
where ptlsts = 'C'
group by ptcode

-- codes for status = I
select ptcode, count(*)
--from stgArkonapdppdet
FROM #wtf
where ptlsts = 'I'
group by ptcode

-- lines with code but no status
select ptcode, count(*)
--from stgArkonapdppdet
FROM #wtf
where ptlsts = ''
group by ptcode

-- one question, what are TT lines with no ptcode?
select *
from (
select (select ptdoc# from stgArkonapdpphdr where ptpkey = p.ptpkey) as RO, p.*
from stgArkonapdppdet p
where ptcode = 'TT'
  and ptlsts = '') x
where trim(ro) like '16%'
order by RO


select *
from stgArkonapdppdet
where ptpkey = (
  select ptpkey
  from stgArkonapdpphdr
  where trim(ptdoc#) = '16061936')

-- ro 16083959  
SELECT *
FROM stgArkonaPDPPDET
WHERE ptpkey = 446454    

SELECT ptcode, COUNT(*)
FROM stgArkonaPDPPDET
GROUP BY ptcode

-- so limit det based ON hdr ptdtyp = 'RO'
-- PDPPHDR
-- ServiceLine.exe.PDPPHDR.stgArkPDPPHDR ensures ptdoc# (RO) IS unique
-- PDPPDET
-- ServiceLine.exe.PDPPDET.stgArkPDPPDET ensures ptco#, ptpkey, ptline, ptltyp, ptseq# IS unique

SELECT h.ptdoc#, d.ptco#, d.ptpkey, d.ptline, d.ptltyp, d.ptseq#, d.ptcode, d.ptdate, 
  d.ptmeth, d.ptlsts, d.ptsvctyp, d.ptlpym, d.pttech, d.ptlopc, d.ptcrlo, 
  d.ptlhrs, d.ptlchr, d.ptfact, d.ptcost, d.ptnet
-- INTO #wtf  
FROM stgArkonaPDPPDET d
INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
  AND h.ptdtyp = 'RO'
order BY ptdoc#, ptline, ptseq#  
  
select * FROM #wtf WHERE ptdoc# = '16084619' ORDER BY ptline, ptseq#

select * FROM stgArkonaSDPRDET WHERE ptro# = '16084100'

SELECT * FROM stgArkonaPDPPDET WHERE ptpkey = 449650 AND ptline = 1 ORDER BY ptseq#

SELECT ptco#, ptpkey, ptline, ptseq#
FROM stgArkonaPDPPDET
GROUP BY ptco#, ptpkey, ptline, ptseq#
HAVING COUNT(*) > 1

SELECT ptpkey, COUNT(*)
FROM stgArkonaPDPPDET
GROUP BY ptco#, ptpkey, ptline, ptseq#
HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC 

SELECT h.ptdoc#, d.*
FROM stgArkonaPDPPDET d
INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
  AND h.ptdtyp = 'RO'
WHERE d.ptpkey = 452548
ORDER BY d.ptline, ptseq#

SELECT *
FROM stgArkonaPDPPHDR
WHERE ptpkey = 452548

SELECT *
FROM stgArkonaPDPPDET
WHERE ptpkey = 452548
AND ptline = 1
ORDER BY ptline, ptseq#

SELECT *
FROM stgArkonasdprdet
WHERE ptro# = '16085233'

SELECT *
FROM stgArkonapdppdet
WHERE ptckey = 452548

/***************** PDPPDET ****************************/  

/***************** PDPPHDR ****************************/ 
SELECT * 
FROM stgArkonaPDPPHDR
WHERE ptdtyp = 'RO'

SELECT ptdtyp, COUNT(*)
FROM stgArkonaPDPPHDR
GROUP BY ptdtyp

/***************** 4/14 statuses ****************************/ 

-- Records that exist for ROS IN different statuses
-- OPEN (no closed lines): single record IN pdpphdr 
-- in-proc (no closed lines): 1 row IN pdpphdr, at least one row IN pdppdet
-- in-proc (with closed lines): 1 row IN pdpphdr, at least one row IN pdppdet, 1 row IN sdprdhr, at least one row IN sdprdet
-- Appr-PD (no closed lines): 1 row IN pdpphdr, at least one row IN pdppdet
-- Appr-PD (with closed lines): 1 row IN pdpphdr, at least one row IN pdppdet, 1 row IN sdprdhr, at least one row IN sdprdet
DECLARE @ro string;
@ro = '16061936';
select 
  (select count(*) from stgArkonaglptrns where gtdoc# = @ro) as glptrns,
  (select count(*) from stgArkonasdprhdr where ptro# = @ro) as sdprhdr,
  (select count(*) from stgArkonasdprdet where ptro# = @ro) as sdprdet,
  (select count(*) from stgArkonapdpphdr where ptdoc# = @ro) as pdpphdr,
  (select count(*) from stgArkonapdppdet where ptpkey = (
    select ptpkey 
    FROM stgArkonaPDPPHDR
    where ptdoc# = @ro)) as pdppdet
from system.iota


SELECT * FROM stgArkonaPDPPHDR WHERE ptdoc# = '19088614'

SELECT * FROM stgArkonapdppdet WHERE ptpkey = 409202


/******* 4/16 ******/
-- combinations of ptltyp & ptcode
SELECT ptltyp, ptcode, COUNT(*),
  MAX(
    CASE
      WHEN ptltyp = 'A' THEN
        CASE
          WHEN ptcode = 'CP' THEN 'Header :: Cust Pay Info'
          WHEN ptcode = 'IS' THEN 'Header :: Internal Flag Info'
          WHEN ptcode = 'PR' THEN 'Header :: Estimate Data'
          WHEN ptcode = 'SC' THEN 'Header :: Service Contract Info'
          WHEN ptcode = 'WS' THEN 'Header :: Warranty Info Pay Line'
        END
      WHEN ptltyp = 'L' THEN 
        CASE
          WHEN ptcode = 'CR' THEN 'Labor Line :: Correction Data'
          WHEN ptcode = 'TT' THEN 'Labor Line :: Tech Time Flag'
        END  
      WHEN ptltyp = 'M' THEN
        CASE  
          WHEN ptcode = 'PM' THEN 'Paint/Materials :: Paint/Materials Line'
        END 
      WHEN ptltyp = 'N' THEN
        CASE
          WHEN ptcode = 'PO' THEN 'Sublet Line :: Purchase Order'
          WHEN ptcode = 'SL' THEN 'Sublet Line :: Sublet Sale Type'
        END  
      WHEN ptltyp = 'W' THEN
        CASE
          WHEN ptcode = 'HZ' THEN 'Hazardous Materials :: Hazardous Materials Line'
        END  
      ELSE 'WTF' 
    END) AS  Descr
FROM stgArkonaSDPRDET
-- WHERE ptdate > '12/31/2011'
GROUP BY ptltyp, ptcode
ORDER BY ptltyp, ptcode

SELECT d.ptltyp, d.ptcode, COUNT(*)
FROM stgArkonaPDPPHDR h
LEFT JOIN stgArkonapdppdet d ON h.ptpkey = d.ptpkey
WHERE h.ptdtyp = 'RO' 
  AND d.ptcode IS NOT NULL 
GROUP BY ptltyp, ptcode



-- combinations of type/code/fran
SELECT h.ptfran, d.ptltyp, d.ptcode, COUNT(*),
  MAX(
    CASE
      WHEN d.ptltyp = 'A' THEN
        CASE
          WHEN d.ptcode = 'CP' THEN 'Header :: Cust Pay Info'
          WHEN d.ptcode = 'IS' THEN 'Header :: Internal Flag Info'
          WHEN d.ptcode = 'PR' THEN 'Header :: Estimate Data'
          WHEN d.ptcode = 'SC' THEN 'Header :: Service Contract Info'
          WHEN d.ptcode = 'WS' THEN 'Header :: Warranty Info Pay Line'
        END
      WHEN d.ptltyp = 'L' THEN 
        CASE
          WHEN d.ptcode = 'CR' THEN 'Labor Line :: Correction Data'
          WHEN d.ptcode = 'TT' THEN 'Labor Line :: Tech Time Flag'
        END  
      WHEN d.ptltyp = 'M' THEN
        CASE  
          WHEN d.ptcode = 'PM' THEN 'Paint/Materials :: Paint/Materials Line'
        END 
      WHEN d.ptltyp = 'N' THEN
        CASE
          WHEN d.ptcode = 'PO' THEN 'Sublet Line :: Purchase Order'
          WHEN d.ptcode = 'SL' THEN 'Sublet Line :: Sublet Sale Type'
        END  
      WHEN d.ptltyp = 'W' THEN
        CASE
          WHEN d.ptcode = 'HZ' THEN 'Hazardous Materials :: Hazardous Materials Line'
        END  
      ELSE 'WTF' 
    END) AS  Descr
FROM stgArkonaSDPRHDR h 
LEFT JOIN stgArkonaSDPRDET d ON h.ptro# = d.ptro#
WHERE h.ptdate > '12/31/2011'
GROUP BY h.ptfran, d.ptltyp, d.ptcode


-- combination of labor opp AND corr code
SELECT ptlopc, ptcrlo, COUNT(*)
FROM stgArkonaSDPRDET
GROUP BY ptlopc, ptcrlo

SELECT *
FROM stgArkonaSDPRDET
WHERE ptcrlo = '4131Q8'

SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '2649588'

-- Hazardous waste charge:
SELECT ptco#, ptro#, ptline, ptlamt 
FROM stgArkonaSDPRDET 
WHERE ptltyp = 'W'
  AND ptcode = 'HZ'
  
SELECT ptco#, ptro#, ptline, ptlamt 
FROM (  
SELECT ptco#, ptro#, ptline, ptlamt 
FROM stgArkonaSDPRDET 
WHERE ptltyp = 'W'
  AND ptcode = 'HZ') x
GROUP BY ptco#, ptro#, ptline, ptlamt 
HAVING COUNT(*) > 1   

-- #FlagHours (flag hours) 
SELECT ptco#, ptro#, ptline
FROM #FlagHours
WHERE ptdate > '12/31/2011'
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1


SELECT *
FROM #FlagHours
WHERE ptro# = '16077630'

/*************** 4/19 **********************/
-- Flag Hours
-- DROP TABLE #FlagHours   
--  going back to thinking, UNION xtim
-- 4/8, so what does that look LIKE
-- #jon
-- remove ptseq# FROM sdprdet & pdppdet
-- 4/9
-- 16084086/506: tech has hours ON 2 seq for same line
-- so the union with subselect ON sdprdet discards one of the lines
-- so, GROUP the sdprdet subselect AND SUM the hours
-- verify existence of an offseting base record to include sdpxtim
-- have to get rid of the big anomaly 536: 536 hours
-- 4/13/12: at the point WHERE i say fuck xtim
-- oh shit, just got jeremies email
-- BUT, it think what he IS saying IS that sdpxtim IS only for payroll
-- so i don't need it
-- 4/16 back to scratch, 
-- no xtim, DO the whole fucking deal (no date constraint)
-- 52 secs
SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
INTO #FlagHours
FROM (
    SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptdate, d.pttech, 
      coalesce(d.ptlhrs, 0) AS ptlhrs
    FROM stgArkonaPDPPDET d
    INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
--      AND d.ptdate BETWEEN '03/11/2012' AND '03/24/2012'
  UNION 
    SELECT s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM stgArkonaSDPRDET s
    WHERE s.ptdate IS NOT NULL 
      AND s.ptcode = 'TT'
--      AND s.ptdate BETWEEN  '03/11/2012' AND '03/24/2012'
      AND s.ptlhrs < 300
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) y  
WHERE ptlhrs <> 0    
GROUP BY ptco#, ptdate, pttech, ptro#, ptline;

SELECT * FROM #FlagHours 

SELECT COUNT(*) -- 446133
FROM #FlagHours

-- unique
SELECT ptco#, ptdate, pttech, ptro#, ptline
FROM #FlagHours
group by ptco#, ptdate, pttech, ptro#, ptline
HAVING COUNT(*) > 1

-- added stlrat (costing from sdptech) to stgArkonaSDPTECH
-- #LaborSales
-- DROP TABLE #LaborSales
SELECT f.ptco# AS StoreCode, f.ptdate AS theDate, f.pttech AS TechNumber, 
  f.ptro# AS RONumber, f.ptline AS ROLine, f.Flaghours * t.stlrat AS LaborSales 
INTO #LaborSales
FROM #FlagHours f
LEFT JOIN stgARkonaSDPTECH t ON f.ptco# = t.stco#
  AND f.pttech = t.sttech

-- unique
SELECT storecode, thedate, technumber, ronumber, roline 
FROM #LaborSales
GROUP BY storecode, thedate, technumber, ronumber, roline
HAVING COUNT(*) > 1

select *
FROM #LaborSales
WHERE TechNumber = '536'

SELECT s.*, h.ptlamt AS HazardousWaste
FROM #LaborSales s
LEFT JOIN (
    SELECT ptco#, ptro#, ptline, ptlamt 
    FROM stgArkonaSDPRDET 
    WHERE ptltyp = 'W'
      AND ptcode = 'HZ') AS h ON s.StoreCode = h.ptco#
  AND s.RoNumber = h.ptro#
  AND s.ROLine = h.ptline      
  
-- what about shop supplies?  
-- RO level only
-- based ON config of serv type, pay type, type veh AND labor op

-- only 1 service type per line
SELECT ptco#, ptro#, ptline
FROM stgArkonaSDPRDET d
WHERE ptsvctyp <> ''
  AND left(ptro#, 4) =  '1608'
  AND EXISTS (
    SELECT 1
    FROM stgArkonaSDPRDET
    WHERE ptsvctyp <> ''
      AND ptco# = d.ptco#
      AND ptro# = d.ptro#
      AND ptline = d.ptline
      AND ptsvctyp <> d.ptsvctyp)

-- ro's with only AM    
SELECT *
FROM stgArkonaSDPRHDR
WHERE ptro# IN (
SELECT ptro#
FROM stgArkonaSDPRDET d
WHERE ptsvctyp = 'AM' 
  AND NOT EXISTS (
    SELECT 1 
    FROM stgArkonaSDPRDET
    WHERE ptro# = d.ptro#
    AND ptsvctyp IN (
      SELECT DISTINCT ptsvctyp
      FROM stgArkonaSDPRDET
      WHERE ptsvctyp <> 'AM')))
      
-- ADD service type

SELECT s.*, h.ptlamt AS HazardousWaste
--INTO #RoLine
FROM #LaborSales s
LEFT JOIN (
    SELECT ptco#, ptro#, ptline, ptlamt 
    FROM stgArkonaSDPRDET 
    WHERE ptltyp = 'W'
      AND ptcode = 'HZ') AS h ON s.StoreCode = h.ptco#
  AND s.RoNumber = h.ptro#
  AND s.ROLine = h.ptline       
  
  


-- #ROLine
-- DROP TABLE #ROLine 
SELECT f.ptco# AS StoreCode, f.ptdate AS theDate, f.pttech AS TechNumber, 
  f.ptro# AS RONumber, f.ptline AS ROLine, f.FlagHours, t.stlrat as Costing, 
  f.Flaghours * t.stlrat AS LaborSales,
  h.ptlamt AS HazardousWaste
INTO #ROLine  
FROM (   
  SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
  FROM ( -- flag hours
      SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptdate, d.pttech, 
        coalesce(d.ptlhrs, 0) AS ptlhrs
      FROM stgArkonaPDPPDET d
      INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
      WHERE d.ptcode = 'TT'
    UNION 
      SELECT s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech, 
        sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
      FROM stgArkonaSDPRDET s
      WHERE s.ptdate IS NOT NULL 
        AND s.ptcode = 'TT'
        AND s.ptlhrs < 300
      GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) y  
  WHERE ptlhrs <> 0    
  GROUP BY ptco#, ptdate, pttech, ptro#, ptline) f
LEFT JOIN stgARkonaSDPTECH t ON f.ptco# = t.stco# -- tech costing 
  AND f.pttech = t.sttech
LEFT JOIN ( -- Hazardous Waste
    SELECT ptco#, ptro#, ptline, ptlamt 
    FROM stgArkonaSDPRDET 
    WHERE ptltyp = 'W'
      AND ptcode = 'HZ') AS h ON f.ptco# = h.ptco#
  AND f.ptro# = h.ptro#
  AND f.ptline = h.ptline 

-- unique  
SELECT storecode, thedate, technumber, ronumber, roline
-- SELECT *
FROM #ROLine  
GROUP BY storecode, thedate, technumber, ronumber, roline
HAVING COUNT(*) > 1

-- service types  
SELECT ptsvctyp, COUNT(*)
-- SELECT *
FROM stgArkonaSDPRDET 
GROUP BY ptsvctyp 

-- DROP TABLE #servtype
-- hazardous material shows up ON a line with a service type of MR
-- which can CREATE multiple rows for line AND service type
-- header rows (ptlytp = A) always have a value for ptsvctyp
-- this query generates a single anomaly: 16058003 Line 3 with multiple service types for a single line
SELECT distinct ptco#, ptro#, ptline, ptsvctyp
INTO #servtype
FROM stgArkonaSDPRDET
WHERE ptsvctyp <> ''
  AND ptltyp = 'A'
-- so  
SELECT ptco#, ptro#, ptline, max(ptsvctyp) AS ptsvctyp
INTO #servtype
FROM stgArkonaSDPRDET
WHERE ptsvctyp <> ''
  AND ptltyp = 'A'
GROUP BY ptco#, ptro#, ptline  
-- unique ok now
SELECT storecode, thedate, technumber, ronumber, roline
FROM (
SELECT r.* , x.ptsvctyp
FROM #ROLine r
LEFT JOIN #servtype x ON r.storecode= x.ptco#
  AND r.ronumber = x.ptro#
  AND r.roline = x.ptline) y
GROUP BY storecode, thedate, technumber, ronumber, roline
HAVING COUNT(*) > 1


-- payment types
SELECT ptlpym, COUNT(*)
-- SELECT *
FROM stgArkonaSDPRDET 
GROUP BY ptlpym 

  
-- hmm does every ro have an A row?
-- yes
SELECT *
FROM stgArkonaSDPRDET s
WHERE NOT EXISTS (
  SELECT 1
  FROM stgArkonaSDPRDET
  WHERE ptro# = s.ptro#
    AND ptltyp = 'A')



-- 4/20, struggling with from WHERE to derive payment method for a line
-- 16075715 line 1
line linetype  ptcode  ptlpym
1       A        IS        I
1       L        CR        C
1       L        TT        I
-- so what IS the payment type for line 1?

-- oh shit, maybe it's AS simple AS the payment method FROM FROM the header line
-- it's the same 
-- DROP TABLE #paytype
-- just LIKE service type, there are dups (14, 16083782, 1608342 ...) with multiple payment methods per line
-- so i'll just pick one
SELECT ptco#, ptro#, ptline, max(ptlpym) AS ptlpym
INTO #paytype
FROM stgArkonaSDPRDET
WHERE ptlpym <> ''
  AND ptltyp = 'A'
GROUP BY ptco#, ptro#, ptline  

-- unique  
SELECT ptco#, ptro#, ptline
-- SELECT *
FROM #paytype
group by ptco#, ptro#, ptline
HAVING COUNT(*) > 1  

-- unique still cool
SELECT storecode, thedate, technumber, ronumber, roline
FROM (
SELECT r.* , x.ptsvctyp, p.ptlpym
FROM #ROLine r
LEFT JOIN #servtype x ON r.storecode= x.ptco#
  AND r.ronumber = x.ptro#
  AND r.roline = x.ptline
LEFT JOIN #paytype p ON r.storecode = p.ptco#
  AND r.ronumber = p.ptro#
  AND r.roline = p.ptline) x 
GROUP BY storecode, thedate, technumber, ronumber, roline
HAVING COUNT(*) > 1  


-- DROP TABLE #roline
SELECT f.ptco# AS StoreCode, f.ptdate AS theDate, f.pttech AS TechNumber, 
  f.ptro# AS RONumber, f.ptline AS ROLine, f.FlagHours, t.stlrat as "Tech Costing", 
  f.Flaghours * t.stlrat AS LaborCost,
  h.ptlamt AS HazardousWaste, st.ptsvctyp AS "Service Type",
  pm.ptlpym AS "Payment Method"
INTO #ROLine  
FROM (   
  SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
  FROM ( -- flag hours
      SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptdate, d.pttech, 
        coalesce(d.ptlhrs, 0) AS ptlhrs
      FROM stgArkonaPDPPDET d
      INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
      WHERE d.ptcode = 'TT'
    UNION 
      SELECT s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech, 
        sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
      FROM stgArkonaSDPRDET s
      WHERE s.ptdate IS NOT NULL 
        AND s.ptcode = 'TT'
        AND s.ptlhrs < 300
      GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) y  
  WHERE ptlhrs <> 0    
  GROUP BY ptco#, ptdate, pttech, ptro#, ptline) f
LEFT JOIN stgARkonaSDPTECH t ON f.ptco# = t.stco# -- tech costing 
  AND f.pttech = t.sttech
LEFT JOIN ( -- Hazardous Waste
    SELECT ptco#, ptro#, ptline, ptlamt 
    FROM stgArkonaSDPRDET 
    WHERE ptltyp = 'W'
      AND ptcode = 'HZ') AS h ON f.ptco# = h.ptco#
  AND f.ptro# = h.ptro#
  AND f.ptline = h.ptline 
LEFT JOIN ( -- service type
    SELECT ptco#, ptro#, ptline, max(ptsvctyp) AS ptsvctyp
    FROM stgArkonaSDPRDET
    WHERE ptsvctyp <> ''
      AND ptltyp = 'A'
    GROUP BY ptco#, ptro#, ptline) st on f.ptco# = st.ptco#
  AND f.ptro# = st.ptro#
  AND f.ptline = st.ptline 
LEFT JOIN ( -- payment method      
    SELECT ptco#, ptro#, ptline, max(ptlpym) AS ptlpym
    FROM stgArkonaSDPRDET
    WHERE ptlpym <> ''
      AND ptltyp = 'A'
    GROUP BY ptco#, ptro#, ptline) pm ON f.ptco# = pm.ptco# 
  AND f.ptro# = pm.ptro#
  AND f.ptline = pm.ptline      
    
-- unique
SELECT storecode, thedate, technumber, ronumber, roline
FROM #Roline
GROUP BY storecode, thedate, technumber, ronumber, roline
HAVING COUNT(*) > 1

SELECT * FROM #roline
WHERE ronumber = '16080000'
ORDER BY roline

-- 4/22 ahh, did NOT UPDATE tech with stlrat

SELECT *
FROM stgARkonaSDPRDET
where ptro# = '16080000'

/******************************************************************************/
-- so it's dawning ON me ( AND i have changed the fieldnames IN #roline) that i have
-- dervied labor cost, NOT labor sales
-- so, need labor sales, parts cost, parts sales, op code, corr code, sublet
/******************************************************************************/
-- parts
SELECT * FROM stgArkonaPDPTDET

ptcost: part cost
ptnet: part sale amount

SELECT *
FROM #roline r
LEFT JOIN stgArkonaPDPTDET p ON r.ronumber = p.ptinv#
  AND r.roline = p.ptline
WHERE ronumber = '16080000' ORDER BY roline
  
WHERE r.theDate > '04/01/2012'
 AND r.storecode = 'ry1'

-- oh duh, this returns one line because #roline IS limited to ptcode = TT 
SELECT *
FROM #roline r
LEFT JOIN (
  SELECT ptco#, ptinv#, ptline, SUM(ptcost) AS ptcost, SUM(ptlist) AS ptlist,
    SUM(ptnet) AS ptnet
  FROM stgArkonaPDPTDET 
  WHERE ptinv# = '16085348'
  GROUP BY ptco#, ptinv#, ptline) p ON r.ronumber = p.ptinv#
    AND r.roline = p.ptline  
WHERE ronumber = '16085348' ORDER BY roline    
    
WHERE r.theDate > '04/01/2012'
 AND r.storecode = 'ry1' 
 
/*************** 4/26 *************************************/
SELECT ptco#, ptdate, pttech, ptro#, ptline,
  SUM ( -- flag hours
    CASE
      WHEN ptcode = 'TT' THEN coalesce(ptlhrs, 0)
      ELSE 0
    END) AS FlagHours,
  SUM ( -- hazardous waste
    CASE
      WHEN ptltyp = 'W' and ptcode = 'HZ' THEN coalesce(ptlamt, 0)
      ELSE 0
    END) AS Hazard,
  MAX ( -- service type
    CASE 
      WHEN ptsvctyp <> '' AND ptltyp = 'A' THEN ptsvctyp
    END) AS ServType,
  MAX (
    CASE
      WHEN ptlpym <> '' AND ptltyp = 'A' THEN ptlpym
    END) AS PayMethod
FROM stgArkonaSDPRDET  
WHERE ptro# = '16081767'
--WHERE ptdate > '04/01/2012'
-- AND ptco# = 'ry1' 
GROUP BY ptco#, ptdate, pttech, ptro#, ptline


 
 
SELECT *
INTO #ROBase
FROM ( -- ALL the ro's
  SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline 
  FROM stgArkonaPDPPDET d
  INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
  GROUP BY d.ptco#, h.ptdoc#, d.ptline
  UNION
  SELECT s.ptco#, s.ptro#, s.ptline
  FROM stgArkonaSDPRDET s
  GROUP BY ptco#, ptro#, ptline) x
WHERE ptro# <> ''
  AND ptco# IN ('RY1','RY2','RY3')
 
SELECT ptco#, ptro#, ptline
FROM #ROBase
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

DROP TABLE edwRODim;
CREATE TABLE edwRoDim (
  RoKey autoinc, 
  StoreCode cichar(30),
  Store  cichar(30),
  RoNumber cichar(9),
  RoLine integer) IN database;
  
INSERT INTO edwRoDim (StoreCode, Store, RoNumber, RoLine)
SELECT ptco#,
  CASE ptco#
    WHEN 'RY1' THEN 'Rydells'
    WHEN 'RY2' THEN 'Honda'
    WHEN 'RY3' THEN 'Crookston'
  END, 
  ptro#, ptline
FROM #ROBase

SELECT *
FROM edwRoDim

/********************** factROLine ********************************************/
-- edwRoDim:  1 row per store/ro/line

-- line date: 1 row per store/ro/line/date
-- nope it's 1 row per store/ro/line/seq/date
-- DROP TABLE #wtf

SELECT ptco#, ptro#, ptline, ptseq#, ptdate
INTO #wtf
-- SELECT *
FROM stgArkonaSDPRDET
GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate

SELECT *
FROM #wtf
WHERE ptdate IS NULL 

-- does every sequence have a date?

SELECT RoKey, StoreCode, RoNumber, RoLine, ptdate AS RoLineDate
INTO #d 
FROM edwRODim r
LEFT JOIN #wtf d ON r.storecode = d.ptco#
  AND r.RoNumber = d.ptro#
  AND r.RoLine = d.ptline
WHERE ptdate IS NOT NULL   

SELECT storecode, ronumber, roline
FROM #d
GROUP BY storecode, ronumber, roline
HAVING COUNT(*) > 1

can a single line have more than one date
i'm thinking it shouldn't

SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16001002'
ORDER BY ptline

SELECT *
FROM stgArkonaSDPRDET
WHERE ptdate IS NULL 

CREATE TABLE factRoLine (
  RoNumber cichar(9),
  RoLine integer, 
  StoreCode cichar(3),
  LineDate date,
  
  
SELECT *
FROM edwRoDim r

-- 4/28
-- redo #robase to include ptseq# & ptdate:: zRoBase
CREATE TABLE zRoBase (
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  Seq integer,
  SeqDate date) IN database;
-- DELETE FROM zROBase  
-- exclude len ro < 6: 1676446
-- exclude WHERE no record for sdprdet EXISTS IN glptrns: 1777820
-- why should the fucking number go up?, should hav gone down
-- why the fuck DO i have parts invoices IN ro?
-- allright, ctfd, exclude nonRO FROM PDPPHDR
INSERT INTO zROBase 
SELECT *
FROM ( -- ALL the ro's
  SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptseq#, d.ptdate 
  FROM stgArkonaPDPPDET d
  INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
  WHERE ptdtyp = 'RO'
  GROUP BY d.ptco#, h.ptdoc#, d.ptline, d.ptseq#, d.ptdate 
  UNION
  SELECT s.ptco#, s.ptro#, s.ptline, s.ptseq#, s.ptdate
  FROM stgArkonaSDPRDET s -- there are no detail records without a matching header row
  WHERE length(trim(ptro#)) > 6 -- exclude conversion data
    AND EXISTS (
      SELECT 1
      FROM stgArkonaGLPTRNS
      WHERE gtdoc# = s.ptro#)
  GROUP BY ptco#, ptro#, ptline, s.ptseq#, s.ptdate) x
WHERE ptro# <> '' 
  AND ptco# IN ('RY1','RY2','RY3')

  
SELECT * -- these s/b the the OPEN ros
FROM zrobase z
WHERE NOT EXISTS (
  SELECT 1
  FROM stgArkonaGLPTRNS
  WHERE gtctl# = z.ro)

-- unique  
SELECT storecode, ro, line, seq, seqdate
FROM zROBase
GROUP BY storecode, ro, line, seq, seqdate
HAVING COUNT(*) > 1


select z.*,
  (SELECT pttech -- what about tech FROM p.det
    FROM stgArkonaSDPRDET
    WHERE ptltyp = 'L'
      AND ptcode = 'TT'
      AND ptco# = z.storecode
      AND ptro# = z.ro
      AND ptline = z.line
      AND ptseq# = z.seq
      AND ptdate = z.seqdate)
FROM zROBase z

-- tech
-- DROP TABLE #tech 
SELECT ptco#, ptro#, ptline, ptseq#, ptdate, pttech, ptlhrs
INTO #tech 
FROM (
  SELECT ptco#, ptro#, ptline, ptseq#, ptdate, pttech, sum(ptlhrs) AS ptlhrs
  FROM stgArkonaSDPRDET s
  WHERE length(trim(ptro#)) > 6 -- exclude conversion data
--    AND ptro# = '16083781'
    AND EXISTS ( -- excludes voids
      SELECT 1
      FROM stgArkonaGLPTRNS
      WHERE gtdoc# = s.ptro#)
    AND ptltyp = 'L'
    AND ptcode = 'TT'
    GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech
  UNION   
  SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptseq#, d.ptdate, pttech, sum(ptlhrs) AS ptlhrs 
  FROM stgArkonaPDPPDET d
  INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
  WHERE ptdtyp = 'RO'
--    AND h.ptdoc# = '16083781'
    AND ptltyp = 'L'
    AND ptcode = 'TT'
    GROUP BY d.ptco#, h.ptdoc#, ptline, ptseq#, d.ptdate, pttech) x
WHERE ptlhrs <> 0    

-- DROP TABLE #FlagHours
INSERT INTO zFlagHours
SELECT z.*, t.pttech, coalesce(t.ptlhrs, 0) AS FlagHours
FROM zROBase z
LEFT JOIN #tech t ON z.storecode = t.ptco#
  AND z.ro = t.ptro#
  AND z.line = t.ptline
  AND z.seq = t.ptseq#
  AND z.seqdate = t.ptdate
  
ALTER TABLE zRoBase
ALTER COLUMN RoDate SeqDate date;  
  
CREATE TABLE zFlagHours (
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  Seq integer, 
  SeqDate date,
  TechNumber cichar(3),
  FlagHours double) IN database;
  

--more than one tech per line?: yes
SELECT storecode, ro, line
FROM (
SELECT distinct storecode, ro, line, pttech
FROM zFlagHours
WHERE expr <> 0) x
GROUP BY storecode, ro, line
HAVING COUNT(*) > 1
-- more than one tech per seq?: no
SELECT storecode, ro, line, seq
FROM (
SELECT distinct storecode, ro, line, pttech, seq
FROM zFlagHours) x 
--WHERE expr <> 0) x
GROUP BY storecode, ro, line, seq
HAVING COUNT(*) > 1

-- tech ON line but no hours: nope
SELECT *
FROM zFlagHours
WHERE pttech IS NOT NULL
  AND expr = 0
  
-- multiple dates for same seq
-- uh oh, lots of seq = 0 with multiple dates: 1 NULL & 1 date
-- so what IS stored at the level of seq 0
SELECT storecode, ro, line, seq
FROM (
SELECT distinct storecode, ro, line, seq, rodate
FROM zFlagHours) x
GROUP BY storecode, ro, line, seq
HAVING COUNT(*) > 1

SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16058239'

-- shit it's looking LIKE it's real
-- ptltyp W IS always ptseq# 0
-- sometimes it has a date, sometimes it doesn't

SELECT ptltyp, ptseq#, COUNT(*)
FROM stgArkonaSDPRDET
WHERE ptltyp = 'W'
GROUP BY ptltyp, ptseq#

SELECT *
FROM zFlagHours

SELECT *
FROM stgArkonaSDPRDET
WHERE LEFT(ptro#, 4) = '1608'
  AND length(ptro#) = 8
ORDER BY ptro#, ptline, ptseq#

whoa can ptltyp/ptcode repl seq?

so, for each line, IS there more than one occurrence of a typ/code combination


SELECT ptco#, ptro#, ptline, ptltyp, ptcode
FROM stgArkonaSDPRDET
WHERE LEFT(ptro#, 4) = '1608'
  AND length(ptro#) = 8
GROUP BY ptco#, ptro#, ptline, ptltyp, ptcode
HAVING COUNT(*) >1

this might be doable based ON what i need FROM each typ/code combination

simply decide what information comes form what typ/code comnpbination

flag hours: L/TT
date: L/TT

-- insert into zRoBase
-- take out sequence, substitue typ/code
SELECT *
FROM ( -- ALL the ro's
  SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptltyp, d.ptcode, d.ptdate 
  FROM stgArkonaPDPPDET d
  INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
  WHERE h.ptdoc# = '16083781'
--  WHERE ptdtyp = 'RO'
  GROUP BY d.ptco#, h.ptdoc#, d.ptline, d.ptltyp, d.ptcode, d.ptdate 
  UNION
  SELECT s.ptco#, s.ptro#, s.ptline, s.ptltyp, s.ptcode, s.ptdate
  FROM stgArkonaSDPRDET s -- there are no detail records without a matching header row
  WHERE ptro# = '16083781'
--  WHERE length(trim(ptro#)) > 6 -- exclude conversion data
--    AND EXISTS (
--      SELECT 1
--      FROM stgArkonaGLPTRNS
--      WHERE gtdoc# = s.ptro#)
  GROUP BY ptco#, ptro#, ptline, s.ptltyp, s.ptcode, s.ptdate) x
WHERE ptro# <> '' 
  AND ptco# IN ('RY1','RY2','RY3')

  -- DROP TABLE zRoTypCode
-- shit, take out the date, the date will be provided FROM one (OR more?) of the typ/code combinations  
CREATE TABLE zRoTypCode (
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  Typ cichar(1),
  Code cichar(2)) IN database;  
  
INSERT INTO zRoTypCode  
-- 3.5 minutes to populate w/out indexes IN place
SELECT *
FROM ( -- ALL the ro's
  SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, d.ptltyp, d.ptcode
  FROM stgArkonaPDPPDET d
  INNER JOIN stgArkonaPDPPHDR h ON d.ptpkey = h.ptpkey
  WHERE ptdtyp = 'RO'
  GROUP BY d.ptco#, h.ptdoc#, d.ptline, d.ptltyp, d.ptcode
  UNION
  SELECT s.ptco#, s.ptro#, s.ptline, s.ptltyp, s.ptcode
  FROM stgArkonaSDPRDET s -- there are no detail records without a matching header row
  WHERE length(trim(ptro#)) > 6 -- exclude conversion data
    AND EXISTS (
      SELECT 1
      FROM stgArkonaGLPTRNS
      WHERE gtdoc# = s.ptro#)
  GROUP BY ptco#, ptro#, ptline, s.ptltyp, s.ptcode) x
WHERE ptro# <> '' 
  AND ptco# IN ('RY1','RY2','RY3');

-- wah fucking hoo, unique  
SELECT storecode, ro, line, typ, code
FROM zROTypCode  
GROUP BY storecode, ro, line, typ, code
HAVING COUNT(*) > 1


SELECT *
FROM zRoTypCode
-- !!! grain IS line
IF there IS L/TT for a line -> date 
IF not?
-- lots AND lots
SELECT *
FROM zRoTypCode z
WHERE NOT EXISTS (
  SELECT 1
  FROM zRotypCode
  WHERE storecode = z.storecode
    AND ro = z.ro
    AND line = z.line
    AND code = 'TT')
    
-- franchise
-- great, truely unique across ro
SELECT h.ptro#, ptfran 
FROM (
  select storecode, ro
  from zRoTypCode
  GROUP BY storecode, ro) z
LEFT JOIN stgArkonaSDPRHDR h ON z.storecode = h.ptco#
  AND z.ro = h.ptro#
WHERE ptfran <> ''  
GROUP BY h.ptro#, ptfran  
HAVING COUNT(*) > 1

-- franchise, but only FROM SHdr
SELECT z.*, h.ptfran
FROM (
  SELECT storecode, ro   
  FROM zRoTypCode
  GROUP BY storecode, ro) z
LEFT JOIN stgArkonaSDPRHDR h ON z.storecode = h.ptco#
  AND z.ro = h.ptro# 
 
-- store/ro/line are unique  
SELECT storecode, ro, line
FROM zRoTypCode
WHERE typ = 'L'
  AND code = 'TT'
GROUP BY storecode, ro, line
HAVING COUNT(*) > 1  

SELECT z.*, d.ptlhrs
SELECT *
FROM zRoTypCode
WHERE typ = 'L'
  AND code = 'TT'


-- including ptfran  
  SELECT pd.ptco#, ph.ptdoc# AS ptro#, pd.ptline, pd.ptltyp, pd.ptcode, ph.ptfran
  FROM stgArkonaPDPPDET pd
  LEFT JOIN stgArkonaPDPPHDR ph ON pd.ptpkey = ph.ptpkey
  WHERE ph.ptdtyp = 'RO'
  GROUP BY pd.ptco#, ph.ptdoc#, pd.ptline, pd.ptltyp, pd.ptcode, ph.ptfran
  UNION
  SELECT sh.ptco#, sh.ptro#, sd.ptline, sd.ptltyp, sd.ptcode, sh.ptfran
  FROM stgArkonaSDPRHDR sh
  LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
    AND sh.ptro# = sd.ptro#
  WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
    AND EXISTS (
      SELECT 1
      FROM stgArkonaGLPTRNS
      WHERE gtdoc# = sh.ptro#)
  GROUP BY sh.ptco#, sh.ptro#, sd.ptline, sd.ptltyp, sd.ptcode, sh.ptfran
  

SELECT z.storecode, z.ro, z.line, sd.pttech, sd.ptdatesum(sd.ptlhrs)
FROM (
  SELECT *
  FROM zRoTypCode 
  WHERE typ = 'L'
    AND code = 'TT') z
LEFT JOIN (
    select ptco#, ptro#, ptline, pttech, ptlhrs
    FROM stgArkonaSDPRDET
    WHERE ptltyp = 'L'
      AND ptcode = 'TT'
      AND ptlhrs <> 0) sd ON z.storecode = sd.ptco#
  AND z.ro = sd.ptro# 
  AND z.line = sd.ptline
WHERE sd.ptro# IS NOT null
GROUP BY z.storecode, z.ro, z.line, sd.pttech  

SELECT * FROM stgArkonaSDPRDET WHERE pttech = ''

-- figure out WHERE line date comes FROM for each typ/code
IF L/TT :: date
ELSE

SELECT *
FROM stgArkonaSDPRDET
WHERE length(ptro#) = 8
  AND LEFT(ptro#, 4) = '1608'
  AND ptcode <> 'TT'
ORDER BY ptro#, ptline  

-- Line Date
-- a single date/line for typ = A
SELECT ptco#, ptro#, ptline
FROM (
SELECT DISTINCT ptco#, ptro#, ptline, ptdate
FROM stgArkonaSDPRDET
WHERE length(ptro#) IN (7, 8)
--  AND ptdate BETWEEN curdate() - 30 AND curdate()
  AND ptltyp = 'A') x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

SELECT ptco#, ptro#, ptline
FROM (
SELECT DISTINCT ptco#, ptro#, ptline, ptsvctyp
FROM stgArkonaSDPRDET
WHERE length(ptro#) IN (7, 8)
--  AND ptdate BETWEEN curdate() - 30 AND curdate()
  AND ptltyp = 'A') x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1


SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16085138'
  AND ptltyp <> 'L'

  
CREATE TABLE zAllTheRos (
  ptco# cichar(3),
  ptro# cichar(9),
  ptfran cichar(3),
  ptline integer,
  ptltyp cichar(1),
  ptcode cichar(2),
  pttech cichar(3),
  ptlhrs double,
  ptsvctyp cichar(2),
  ptlpym cichar(1),
  ptdate date,
  ptlamt money,
  ptlopc cichar(10),
  ptcrlo cichar(10)) IN database
 
-- 16 minutes  
INSERT INTO zAllTheRos
-- 5/4/12: pdet type A: mult dates/lin
--         exclude WHERE ptdate = 0
--         exclude ptdate FROM grouping, SELECT MIN ptdate          
SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, min(pd.ptdate) AS ptdate, 0 as ptlamt, pd.ptlopc,
  pd.ptcrlo
FROM stgArkonaPDPPHDR ph
LEFT JOIN stgArkonaPDPPDET pd ON ph.ptco# = pd.ptco#
  AND ph.ptpkey = pd.ptpkey
WHERE ph.ptdtyp = 'RO' 
  AND ph.ptdoc# <> ''
  AND ph.ptco# IN ('RY1','RY2','RY3')
  AND pd.ptdate <> '12/31/1999'
GROUP BY ph.ptco#, ph.ptdoc#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptlopc, pd.ptcrlo  
  
UNION

SELECT sh.ptco#, sh.ptro#, sh.ptfran, sd.ptline, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, max(sd.ptsvctyp) AS ptsvctyp, max(sd.ptlpym) AS ptlpym, 
  sd.ptdate, sd.ptlamt, sd.ptlopc, sd.ptcrlo
FROM stgArkonaSDPRHDR sh
LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS (
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#)        
GROUP BY sh.ptco#, sh.ptro#, sh.ptfran, sd.ptline, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, sd.ptdate, sd.ptlamt, sd.ptlopc, sd.ptcrlo    
  
SELECT *
FROM zAllTheRos
WHERE ptltyp = 'A'
  AND ptdate > '04/01/2012'

SELECT * FROM stgArkonaSDPRDET WHERE ptro# = '16083781' ORDER BY ptltyp, ptline, ptcode
SELECT * FROM zalltheros WHERE ptro# = '16083781' ORDER BY ptltyp, ptline, ptcode
SELECT * FROM zalltheros WHERE ptcrlo <> ''
  
Type Code  Yields                     Grain (store/ro)             
 A    CP||IS||SC||WS
            line creation date        line
            service type              line
            payment method            line                          
 L    TT    tech                      tech
            flag hours                tech, tech/line
 L    CR        
                            
/****  5/3  *******************************************************************/
/* multiple dates per line? ***************************************************/
-- WHERE type = A 
-- yep
-- issue appears to be IN pdet
SELECT ptco#, ptro#, ptline
INTO #wtf
FROM (
SELECT ptco#, ptro#, ptline, ptdate
FROM zalltheros
WHERE ptltyp = 'A'
GROUP BY ptco#, ptro#, ptline, ptdate) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1  

SELECT * FROM #wtf

SELECT COUNT (DISTINCT ptro#) FROM #wtf -- 18


-- no dups IN SDET
SELECT ptco#, ptro#, ptline
FROM (  
SELECT ptco#, ptro#, ptline, ptdate
FROM stgArkonaSDPRDET
WHERE length(ptro#) > 6
  AND ptltyp = 'A'  
GROUP BY ptco#, ptro#, ptline, ptdate) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1  

-- DROP TABLE #wtf
SELECT ptco#, ptdoc#, ptline
INTO #wtf
FROM (
  SELECT ph.ptco#, ph.ptdoc#, pd.ptline, min(pd.ptdate) AS ptdate
  FROM stgArkonaPDPPHDR ph
  LEFT JOIN stgArkonaPDPPDET pd ON ph.ptco# = pd.ptco#
    AND ph.ptpkey = pd.ptpkey
  WHERE ph.ptdtyp = 'RO' 
    AND ph.ptdoc# <> ''
    AND ph.ptco# IN ('RY1','RY2','RY3')
    AND pd.ptltyp = 'A'
    AND pd.ptdate <> '12/31/9999'
  GROUP BY ph.ptco#, ph.ptdoc#, pd.ptline) x
GROUP BY ptco#, ptdoc#, ptline
HAVING COUNT(*) > 1

SELECT * FROM #wtf

SELECT ph.ptdoc#, pd.ptline, pd.ptltyp, pd.ptseq#, pd.ptcode, pd.ptdate, 
  pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptlopc, pd.ptcmnt
FROM stgArkonaPDPPHDR ph
LEFT JOIN stgArkonaPDPPDET pd ON ph.ptco# = pd.ptco#
  AND ph.ptpkey = pd.ptpkey
WHERE ptdoc# IN (SELECT DISTINCT ptdoc# FROM #wtf)
  AND ptltyp = 'A'

SELECT ph.ptdoc#, pd.ptline, pd.ptltyp, pd.ptseq#, pd.ptcode, pd.ptdate, 
  pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptlopc, pd.ptcmnt
FROM stgArkonaPDPPHDR ph
LEFT JOIN stgArkonaPDPPDET pd ON ph.ptco# = pd.ptco#
  AND ph.ptpkey = pd.ptpkey
WHERE ptdoc# = '16087474'  
ORDER BY ptline, ptltyp
  
  
SELECT *
FROM #wtf w
INNER JOIN stgArkonaSDPRDET sd ON w.ptco# = sd.ptco#
  AND w.ptdoc# = sd.ptro#
  AND w.ptline = sd.ptline  



////* multiple dates per line? ***************************************************/
////****  5/3  *******************************************************************/

/** 5/2 **/
-- what IS the grain of alltheros?  
-- it's the whole fucking shooting match         

-- so i'm still at decodeing the type/code
--

/******** 4/30 Exceptions ******************/                       
-- ptcode (LEFT 1) does NOT match ptlpym 
SELECT  *
FROM zalltheros
WHERE ptro# IN (                 
SELECT ptro#
FROM zalltheros
WHERE ptltyp = 'A'
  AND LEFT(ptcode,1) <> ptlpym)
  
SELECT  *
FROM stgArkonaSDPRDET
WHERE ptro# IN (                 
SELECT ptro#
FROM stgArkonaSDPRDET
WHERE length(ptro#) > 6
  AND ptltyp = 'A'
  AND LEFT(ptcode,1) <> ptlpym)  
  
-- multiple ptlpym / line         
SELECT ptco#, ptro#, ptline, ptltyp
FROM (
  select distinct ptco#, ptro#, ptline, ptltyp, ptlpym
  from zAlltheros
  WHERE ptltyp = 'A') x
GROUP BY ptco#, ptro#, ptline, ptltyp
HAVING COUNT(*) > 1  

SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# IN (
  SELECT ptro#
  FROM (
    select distinct ptco#, ptro#, ptline, ptltyp, ptlpym
    from zAlltheros
    WHERE ptltyp = 'A') x
  GROUP BY ptco#, ptro#, ptline, ptltyp
  HAVING COUNT(*) > 1)
                        
-- multiple ptsvctyp / line
-- 16058003 Line 3
SELECT ptco#, ptro#, ptline, ptltyp
FROM (
  select distinct ptco#, ptro#, ptline, ptltyp, ptsvctyp
  from zAlltheros
  WHERE ptltyp = 'A') x
GROUP BY ptco#, ptro#, ptline, ptltyp
HAVING COUNT(*) > 1  
/*****************************************************************/

SELECT ptsvctyp, COUNT(*)
FROM zalltheros
WHERE ptsvctyp <> ''
GROUP BY ptsvctyp    

-- shit, this might actually be some sketchy data
-- grouping ON line, how OR why the fuck would i have mutliple payment types 
-- for a single line
-- oops, didn't i already decide that IS an exceptioin
SELECT *
FROM zalltheros z
WHERE EXISTS (
  SELECT 1
  FROM zalltheros
  WHERE ptco# = z.ptco# 
    AND ptro# = z.ptro#
    AND ptline = z.ptline
    AND ptlpym = 'C')
  AND EXISTS (
    SELECT 1
    FROM zalltheros
    WHERE ptco# = z.ptco# 
      AND ptro# = z.ptro#
      AND ptline = z.ptline
      AND ptlpym = 'W')
  AND EXISTS (
    SELECT 1
    FROM zalltheros
    WHERE ptco# = z.ptco# 
      AND ptro# = z.ptro#
      AND ptline = z.ptline
      AND ptlpym = 'I')      

SELECT ptco#, ptro#, ptline, ptfran,
  max(CASE WHEN ptltyp = 'A' THEN ptdate END) AS LineDate,
  MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServTyp,
  MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayMeth
FROM zalltheros         
GROUP BY ptco#, ptro#, ptline, ptfran  


SELECT *
FROM zalltheros
WHERE ptro# = '16045948'
  AND ptline = 4
-- line 4 has 2 payM 
-- screen shows WARR- GENERAL MO
-- AND          S-TECH ERR
SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16045948'
  AND ptline = 4 
-- ptpadj IS L for one line, AND blank for the other  

SELECT ptpadj, COUNT(*)
FROM stgArkonaSDPRDET
WHERE length(ptro#) <> 6
GROUP BY ptpadj
 
-- aha: ptpadj
SELECT *
from stgArkonaSDPRDET s
WHERE LEFT(ptro#, 4) = '1608'
AND EXISTS (
  SELECT 1 
  FROM stgArkonaSDPRDET
  WHERE ptro# = s.ptro#
  AND ptline = s.ptline
  AND ptpadj <> '')

SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16013156'  


SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16038519'

SELECT *
FROM stgArkonaGLPTRNS
WHERE gtctl# = '16045878'

SELECT ptco#, ptro#, ptline
FROM zalltheros
GROUP BY ptco#, ptro#, ptline

SELECT *
FROM zalltheros
WHERE ptro# = '16085212'



  