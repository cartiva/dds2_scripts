CREATE TABLE extArkonaBOPSLSP (
  bpco# cichar(3),
  bpspid cichar(3),
  bpname cichar(20),
  bptype cichar(1),
  bpactive cichar(1),
  bpemp# cichar(9),
  bpteam cichar(2)) IN database;
  
thinking simply type 1,
have the stored proc check for changes (active) OR a new sp AND notify BY email
  AND UPDATE manually
include employeenumber, synch with edwEmployeeDim for name
description: FROM BOPSLSP.name
employeeNumber = NA WHERE NOT a person
NK: storeCode, salesPersonID

DROP TABLE dimSalesPerson;
CREATE TABLE dimSalesPerson (
  salesPersonKey autoinc constraint not null,
  storeCode cichar(3) constraint not null,
  salesPersonID cichar(3) constraint not null,
  employeeNumber cichar(7) constraint NOT NULL,
  description cichar(20) constraint NOT NULL,
  firstName cichar(25) constraint not null, 
  lastName cichar(25) constraint not null,
  fullName cichar(52) constraint not null,
  salesPersonTypeCode cichar(1) constraint not null,
  salesPersonType cichar(12) constraint not null,
  active logical constraint not null,
  CONSTRAINT PK PRIMARY KEY (salesPersonKey)) IN database; 
  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimSalesPerson','dimSalesPerson.adi','NK',
   'storeCode;salesPersonID','',2051,512,'' );   
  
  
SELECT bpco#, bpspid FROM (  
SELECT a.*, b.*
FROM extArkonaBOPSLSP a
LEFT JOIN edwEmployeeDim b on substring(bpname, position(' ' IN bpname) + 1, 25) = b.lastname
  AND substring(bpname, 1, position(' ' IN bpname) - 1) = b.firstname
  AND a.bpco# = b.storecode
  AND b.currentrow = true  
) x GROUP BY bpco#, bpspid HAVING COUNT(*) > 1  


INSERT INTO dimSalesPerson (storeCode, salesPersonID, employeeNumber, description, firstName,
  lastName, fullName, salesPersonTypeCode, salesPersonType, active)
SELECT a.bpco#, a.bpspid, coalesce( b.employeenumber, 'XXX'),  bpname, coalesce(b.firstName, 'XXX'), 
  coalesce(b.lastName, 'XXX'),
  coalesce(trim(b.firstName), 'XXX') + ' ' + coalesce(TRIM(b.lastName), 'XXX'), 
  bptype,
  CASE bptype
    WHEN 'B' THEN 'BDC'
    WHEN 'F' THEN 'F/I Mgr'
    WHEN 'M' THEN 'Sales Mgr'
    WHEN 'S' THEN 'Consultant'
    WHEN 'D' THEN 'Desk Mgr'
    ELSE 'X'
  END,
  CASE WHEN bpactive = 'N' THEN false ELSE true end
FROM extArkonaBOPSLSP a
LEFT JOIN edwEmployeeDim b on substring(bpname, position(' ' IN bpname) + 1, 25) = b.lastname
  AND substring(bpname, 1, position(' ' IN bpname) - 1) = b.firstname
  AND a.bpco# = b.storecode
  AND b.currentrow = true;
  
ALTER TABLE dimSalesPerson
ALTER COLUMN salesPersonKey salesPersonKey integer;  
  
  
SELECT * FROM dimSalesPerson ORDER BY storecode, lastname, firstname  


UPDATE dimSalesPerson
  SET firstName = 'NA',
      lastName = 'NA',
      fullName = 'NA'
WHERE employeenumber = 'NA';      


-- 2/11 clean up
/* get info FROM
SELECT storecode, name, employeenumber, firstname, lastname
FROM edwEmployeeDim
WHERE active = 'Active'
  AND currentrow = true
ORDER BY storecode, lastname  
*/
SELECT *
FROM dimSalesPerson a 
WHERE lastname = 'XXX' 
  AND active = true
ORDER BY substring(description, position(' ' IN description) + 1, 25)


UPDATE dimSalesPerson
SET fullName = TRIM(firstname) + ' ' + TRIM(lastname)
WHERE fullname = 'xxx xxx'
  AND firstname <> 'xxx'
  AND lastname <> 'xxx'

SELECT *
FROM dimsalesperson
WHERE lastname = 'car'  

-- 2/12
fuck me sideways, dealersocket AND arkona maintain non synchronized lists of
sales people
so, a salesperson can have multiple concurrent identifiers
which seriously changes dimSalesPerson

need to generate it FROM a combination of anything IN bopslss, whether OR NOT
it EXISTS IN bopslsp

SELECT * FROM stgArkonaBOPSLSS WHERE bqspid in ('DUL', 'DDU')

SELECT bqco#, bqname
FROM (
  SELECT bqco#, bqspid, bqname
  FROM stgArkonaBOPSLSS
  WHERE bqco# IN ('ry1','ry2')
  GROUP BY bqco#, bqspid, bqname) a
GROUP BY bqco#, bqname
HAVING COUNT(*) > 1  

SELECT bqco#, bqspid, bqname, MIN(bqdate), MAX(bqdate), COUNT(*)
FROM stgArkonaBOPSLSS
WHERE bqco# IN ('ry1','ry2')
GROUP BY bqco#, bqspid, bqname
ORDER BY bqname

SELECT *
FROM stgARkonaBOPSLSS
WHERE bqname = 'ARNE HOLMEN'
  and bqco# = 'RY1'
ORDER BY bqdate  
  
SELECT * FROM extArkonaBOPSLSP ORDER BY bpname

SELECT * FROM stgARkonaBOPSLSS WHERE bqspid = 'HOL' AND bqco# = 'ry1'

SELECT * FROM extArkonaBOPSLSP WHERE bpspid = 'HOL'

select * FROM dimsalesperson WHERE salespersonid = 'HOL'
  
SELECT bqco#, bqspid, bqname
FROM stgArkonaBOPSLSS a
WHERE bqco# IN ('ry1','ry2')
AND NOT EXISTS (
    SELECT 1
    FROM extArkonaBOPSLSP
    WHERE bpco# = a.bqco# 
      AND bpspid = a.bqspid
      AND bpname = bqname) 
GROUP BY bqco#, bqspid, bqname 


SELECT bqco# AS storeCode, bqspid AS salesPersonID, bqname AS name
INTO #wtf1
FROM stgArkonaBOPSLSS a
WHERE bqco# IN ('ry1','ry2')
GROUP BY bqco#, bqspid, bqname; 

SELECT bpco# AS storeCode, bpspid AS salesPersonID, bpname AS name
INTO #wtf2
FROM extArkonaBOPSLSP
WHERE bpco# IN ('ry1','ry2')
GROUP BY bpco#, bpspid, bpname;


SELECT *
FROM #wtf1 a
full OUTER JOIN #wtf2 b on a.storecode = b.storecode AND a.salespersonid = b.salespersonid
ORDER BY a.name

so, what i need IS a comprehensive list of possible storecode/salespersonid with a canonical name

feels LIKE BOPSLSP becomes irrelevant, BOPSLSS IS ALL that matters

-- fix em  up
select *
FROM (
  SELECT bqco# AS storeCode, bqspid AS salesPersonID, bqname AS name
  FROM stgArkonaBOPSLSS
  WHERE bqco# IN ('ry1','ry2')
  GROUP BY bqco#, bqspid, bqname) a
LEFT JOIN dimSalesPerson b on a.storecode = b.storecode AND a.salespersonid = b.salespersonid --AND a.name = b.description
ORDER BY a.storecode, a.salespersonid

INSERT INTO dimsalesperson (salesPersonKey, storecode, salespersonid, employeenumber, description,
  firstname, lastname, fullname, salespersontypecode, salespersontype, active)
SELECT 
  (SELECT MAX(salesPersonKey)+ 1 FROM dimSalesPerson),
  'RY2', 'TAR', employeenumber, description,
  firstname, lastname, fullname, salespersontypecode, salespersontype, active
FROM dimsalesperson
WHERE salespersonkey = 180

DELETE FROM dimSalesPerson WHERE salespersonkey = 209
SELECT name, employeenumber FROM edwEmployeeDim WHERE lastname like '%wilken%';
SELECT * FROM dimSalesPerson WHERE lastname like '%tarr%';

UPDATE dimSalesPerson
  SET employeenumber = '2148950',
      firstname = 'Ronald',
      lastname = 'Wilkening',
      fullname = 'Ronald Wilkening'
WHERE salespersonkey = 151
  

-- 2/13/14 need an unknown row for ry1 AND ry2
-- AND lets get fucking consistent with NA, WHERE it fits, use N/A
SELECT * FROM dimSalesPerson

UPDATE dimSalesPerson
SET employeenumber = 'N/A'
WHERE employeenumber = 'NA';
UPDATE dimSalesPerson
SET firstName = 'N/A'
WHERE firstName = 'NA';
UPDATE dimSalesPerson
SET lastName = 'N/A'
WHERE lastName = 'NA';
UPDATE dimSalesPerson
SET fullName = 'N/A'
WHERE fullName = 'NA';

select * FROM dimsalesperson


INSERT INTO dimsalesperson (salesPersonKey, storecode, salespersonid, employeenumber, description,
  firstname, lastname, fullname, salespersontypecode, salespersontype, active)
SELECT 
  (SELECT MAX(salesPersonKey)+ 1 FROM dimSalesPerson),
  'RY1', 'UNK', 'N/A', 'Unknown',
  'Unknown', 'Unknown', 'Unknown', 'S', 'Consultant', true
FROM system.iota;

INSERT INTO dimsalesperson (salesPersonKey, storecode, salespersonid, employeenumber, description,
  firstname, lastname, fullname, salespersontypecode, salespersontype, active)
SELECT 
  (SELECT MAX(salesPersonKey)+ 1 FROM dimSalesPerson),
  'RY2', 'UNK', 'N/A', 'Unknown',
  'Unknown', 'Unknown', 'Unknown', 'S', 'Consultant', true
FROM system.iota;

-- 2/16 
ok, we have unknown, what about N/A, does every deal have both a consultant AND a manager?
unknown: storecode/spID that DO NOT exist IN dimSalesPerson
n/a: no spID IN bopslss

SELECT * FROM stgArkonaBOPSLSS a WHERE bqco# IN ('ry1','ry2')

-- there ARE deals for which there IS no consultant OR NOT manager
-- but there are no deals for which there IS no consultant AND no manager
-- so, IF on IS NULL IN bopslss, THEN coalesce to the other
SELECT b.*, c.bqspid AS Consultant, d.bqspid AS Manager
FROM (
  SELECT DISTINCT bqco#, bqkey, bqdate
  FROM stgArkonaBOPSLSS a
  WHERE bqco# IN ('ry1','ry2')) b
LEFT JOIN (
  SELECT bqco#, bqkey, bqspid, bqtype
  FROM stgArkonaBOPSLSS
  WHERE bqtype = 'S') c on b.bqkey = c.bqkey  
LEFT JOIN (
  SELECT bqco#, bqkey, bqspid, bqtype
  FROM stgArkonaBOPSLSS
  WHERE bqtype = 'F') d on b.bqkey = d.bqkey 
WHERE c.bqspid IS NULL OR d.bqspid IS null   
  AND EXISTS (
    SELECT 1
    FROM stgArkonaBOPMAST 
    WHERE bmkey = b.bqkey
      AND bmco# = b.bqco#
      AND bmstat <> '')
  
      
      
SELECT a.bmco#, a.bmkey, bmstk#, bmvin, a.bmstat, a.bmtype, a.bmvtyp, a.bmwhsl, 
  bmdtor, bmdtaprv, bmdtcap, a.bmdelvdt, bmdtsv, bmsact, bmvcst, bmpric, 
  a.bmbuyr, a.bmcbyr
FROM stgArkonaBOPMAST a
WHERE bmkey IN (21321,21563,21690,20539,21002 )       

fuck it, the data IS too loosey goosey for overthinking this shit
reduce the fields IN dimSalesPerson
no more natural key
keep the existing data, but no need to scrape bopslsp, it has become irrelevant
scrape last 45 days of bopbslss
any combination of storecode/salespersonid that does NOT exist IN dimSalesPerson
generates an email

-- 2/16/14  

1. CREATE TABLE tmpBOPSLSS: populate nightly with 45 days worth of BOPSLSS
          45 days should be ok, it IS the orig date FROM bopmast
2. sp.xfmDimSalesPerson: generate an email IF any store/spID does NOT exist 
   IN dimSalesPerson
   fix with DDS2\Scripts\dimSalesPerson\new sales person.sql                                   