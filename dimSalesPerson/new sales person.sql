SELECT *
FROM (
SELECT bqco#, bqspid
FROM tmpBOPSLSS
WHERE bqspid NOT IN ( 'HAL', 'ENT', 'WWW', '000', 'KUE', 'KTK', 'HAR', 'KLA','JLS')
GROUP BY bqco#, bqspid) a
LEFT JOIN dimSalesPerson b on a.bqco# = b.storecode
  AND a.bqspid = b.salesPersonID
WHERE b.storecode IS NULL 

-------------------- go to the END of the script for the most recent INSERT script

SELECT * FROM tmpBOPSLSS where bqspid IN ('jbj')

SELECT * FROM tmpBOPSLSS where bqspid IN ('000','WWW')

select* FROM stgArkonaBOPMAST WHERE bmkey IN (13279)

SELECT a.bqkey, a.bqspid, a.bqname, b.bmstk#, b.bmvin, b.bmstat, b.bmpslp, b.bmslp2
FROM tmpbopslss a
INNER JOIN stgArkonaBOPMAST b on a.bqkey = b.bmkey
WHERE a.bqspid = 'jls' 

select * from edwEmployeeDim WHERE lastname = 'yunker'

SELECT * FROM dimsalesperson ORDER by lastname = 'harris'

SELECT * FROM tmpBOPSLSS where bqspid IN ('MUL')
select* FROM stgArkonaBOPMAST WHERE bmkey IN (12679)

select *
FROM tmpbopslss
WHERE bqspid = 'jko'u

SELECT *
FROM extArkonaBOPSLSP
WHERE bpspid = 'BSB'

SELECT * FROM stgarkonabopmast WHERE bmkey = 34365


SELECT * FROM dimSalesperson WHERE salespersonid = 'emm' 
SELECT * FROM tmpbopslss WHERE bqspid = 'emm'

-- rick stout f/i mgr
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)    
SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'STO', employeeNumber, left(name,20), firstname, lastname, 
  TRIM(firstname) + ' ' + lastname, 'F','F/I Mgr', true
FROM edwEmployeeDim 
WHERE employeenumber = '1132700'
  AND currentrow = true;
  
-- james harris
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)    
SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'JAH', employeeNumber, left(name,20), firstname, lastname, 
  TRIM(firstname) + ' ' + lastname, 'S','Consultant', true
FROM edwEmployeeDim 
WHERE employeenumber = '260045'
  AND currentrow = true;  

-- kass dvorak
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)    
SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'DVO', employeeNumber, left(name,20), firstname, lastname, 
  TRIM(firstname) + ' ' + lastname, 'S','Consultant', true
FROM edwEmployeeDim 
WHERE employeenumber = '231121'
  AND currentrow = true; 
-- 6/17/16
-- every new consultant IS assigned an identifier for both stores
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)  
SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'DHA', employeeNumber, left(name,20), firstname, lastname, 
  TRIM(firstname) + ' ' + lastname, 'S','Consultant', true
FROM edwEmployeeDim 
WHERE employeenumber = '163700'
  AND currentrow = true;
  
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)  
SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'DHA', employeeNumber, left(name,20), firstname, lastname, 
  TRIM(firstname) + ' ' + lastname, 'S','Consultant', true
FROM edwEmployeeDim 
WHERE employeenumber = '163700'
  AND currentrow = true;  
  
-- larry stadstad ry2  
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)  
SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'STD', employeeNumber, left(name,20), firstname, lastname, 
  TRIM(firstname) + ' ' + lastname, 'S','Consultant', true
FROM edwEmployeeDim 
WHERE employeenumber = '1130690'
  AND currentrow = true;  
   

-- chad calcaterra ry1
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
SELECT (
  SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', salespersonid, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active
FROM dimsalesperson
WHERE salespersonkey = 294   
  
SELECT   
-- chris nielson ry1
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
SELECT (
  SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', salespersonid, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active
FROM dimsalesperson
WHERE salespersonkey = 9

-- fred spencer ry1
SELECT *
FROM tmpbopslss
WHERE bqspid = 'spe'  
  AND bqco# = 'RY1'
  
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
SELECT (
  SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', salespersonid, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active
FROM dimsalesperson
WHERE salespersonkey = 156
  
-- steve flaat/RY2
SELECT *
FROM tmpbopslss
WHERE bqspid = 'kva'  
  AND bqco# = 'RY2'
  
SELECT *
FROM dimsalesperson
WHERE salespersonid = 'kva'  

SELECT * FROM tmpbopslss WHERE bqspid = 'wal'

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
SELECT (
  SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', salespersonid, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active
FROM dimsalesperson
WHERE salespersonkey = 215  

/*
3/3/14
  Bradley Aschnewitz, new RY1 consultant
  SELECT * FROM tmpbopslss WHERE bqspid = 'jga'
  SELECT * FROM tmpbopbslsp
*/
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)  
SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  storecode, 'PRE', employeeNumber, left(name,20), firstname, lastname, 
  TRIM(firstname) + ' ' + lastname, 'S','Consultant', true
FROM edwEmployeeDim 
WHERE employeenumber = '1112410' 
  AND currentrow = true;       
  
-- 3/15 Joe Garceau for RY2
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
SELECT (
  SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', salespersonid, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active
FROM dimsalesperson
WHERE salespersonKey = 64

-- 3-14 kvsager for ry2
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
SELECT (
  SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', salespersonid, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active
FROM dimsalesperson
WHERE salespersonKey = 199

-- 4/22
hmmm, does NOT already exist
SELECT * FROM dimSalesperson WHERE salespersonid = 'ame' 
SELECT * FROM stgArkonaBOPMAST WHERE bmpslp = 'ame'

ry2 salesman Joshua Ames AME, NOT IN payroll

SELECT * FROM edwEmployeeDim WHERE lastname = 'ames'

INSERT INTO dimSalesperson 
  SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2','AME','22910','JOSHUA AMES','Joshua','Ames','Joshua Ames','S','Consultant',true
  FROM system.iota
  
-- 4/24
SELECT * FROM stgArkonaBOPMAST WHERE bmpslp = 'REA'  

INSERT INTO dimSalesperson 
  SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2','REA','2118100','DEVLIN REASY','Devlin','Reasy','Devlin Reasy','S','Consultant',true
  FROM system.iota
  
-- 4/26
SELECT * FROM stgArkonaBOPMAST WHERE bmpslp = 'KEI'  
  
INSERT INTO dimSalesperson 
  SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2','KEI','2132420','STROTHER, LEKEITH','Lekeith','Strother','Lekeith Strother','S','Consultant',true
  FROM system.iota  
  
-- 5/4
SELECT * FROM dimSalesperson WHERE salespersonid = 'pof' 

SELECT * FROM stgArkonaBOPMAST WHERE bmpslp = 'POF'  
  
INSERT INTO dimSalesperson 
  SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2','POF','2106490','POFF, AARON W','Poff','Aaron','Aaron Poff','S','Consultant',true
  FROM system.iota  
  
-- 5/13/14  strother for ry1
SELECT * FROM dimsalesperson WHERE lastname = 'strother'

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
SELECT (
  SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', salespersonid, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active
FROM dimsalesperson
WHERE salespersonKey = 258;

-- 6/5/14  allen preston for ry2
SELECT * FROM dimsalesperson WHERE lastname = 'walker'
select * FROM dimsalesperson WHERE salespersonid = 'ker'

ry1 nick walker, IN arkona he IS WAL, apparently IN dealer socket he IS KER,
  this deal was under KER, currently no deals under WAL
  
-- 10/10/16 george yunker
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'YUN', '1151450', 
  'YUNKER, GEORGE P', 'George', 'Yunker', 'George Yunker', 'S',
  'Consultant', True)

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'YUN', '1151450', 
  'YUNKER, GEORGE P', 'George', 'Yunker', 'George Yunker', 'S',
  'Consultant', True)  
    
-- 10/26/16 tonya gable	
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'TGA', '149507', 
  'GABLE, TONYA', 'Tonya', 'Gable', 'Tonya Gable', 'S',
  'Consultant', True)

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'TGA', '149507', 
  'GABLE, TONYA', 'Tonya', 'Gable', 'Tonya Gable', 'S',
  'Consultant', True)

-- 12/21/16 Ryan Rea

-- 10/26/16 tonya gable	
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'RRE', '2118521', 
  'REA, RYAN', 'Ryan', 'Rea', 'Ryan Rea', 'S',
  'Consultant', True);

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'RRE', '2118521', 
  'REA, RYAN', 'Ryan', 'Rea', 'Ryan Rea', 'S',
  'Consultant', True);
  
-- 1/6/17 Kody Hughes
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'HUG', '264510', 
  'HUGHES, KODY', 'Kody', 'Hughes', 'Kody Huges', 'S',
  'Consultant', True);

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'HUG', '264510', 
  'HUGHES, KODY', 'Kody', 'Hughes', 'Kody Huges', 'S',
  'Consultant', True);
  
-- 1/6/17 Kody Hughes
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'HUG', '264510', 
  'HUGHES, KODY', 'Kody', 'Hughes', 'Kody Huges', 'S',
  'Consultant', True);

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'HUG', '264510', 
  'HUGHES, KODY', 'Kody', 'Hughes', 'Kody Huges', 'S',
  'Consultant', True);  
  
-- 1/5/17 Nicolas Bellmore
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'BEL', '111352', 
  'BELLMORE, NICHOLAS', 'Nicholas', 'Bellmore', 'Nicholas Bellmore', 'S',
  'Consultant', True);

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'BEL', '111352', 
  'BELLMORE, NICHOLAS', 'Nicholas', 'Bellmore', 'Nicholas Bellmore', 'S',
  'Consultant', True);   
  
-- 1/5/17 Alan Mulhern
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'MUL', '151702', 
  'MULHERN, ALAN', 'Alan', 'Mulhern', 'Alan Mulhern', 'S',
  'Consultant', True);

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'MUL', '151702', 
  'MULHERN, ALAN', 'Alan', 'Mulhern', 'Alan Mulhern', 'S',
  'Consultant', True);  
  
-- 2/6/17
-- Damian Brooks - no deals yet (no rows IN bopslss), but need to get him IN scpp anyway  
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'BRO', '117205', 
  'BROOKS, DAMIAN', 'Damian', 'Brooks', 'Damian Brooks', 'S',
  'Consultant', True);

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'BRO', '117205', 
  'BROOKS, DAMIAN', 'Damian', 'Brooks', 'Damian Brooks', 'S',
  'Consultant', True);
  
-- 3/14/17
-- Colt Mavity - no deals yet (no rows IN bopslss), but need to get him IN scpp anyway  
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'MAV', '19055', 
  'MAVITY, COLT', 'Colt', 'Mavity', 'Colt Mavity', 'S',
  'Consultant', True);

INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'MAV', '19055', 
  'MAVITY, COLT', 'Colt', 'Mavity', 'Colt Mavity', 'S',
  'Consultant', True); 
  
-- 5/2/17 
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'EAS', '1212688', 
  'EASTMAN, DONOVAN', 'Donovan', 'Eastman', 'Donavan Eastman', 'S',
  'Consultant', True);
  
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'EAS', '1212688', 
  'EASTMAN, DONOVAN', 'Donovan', 'Eastman', 'Donavan Eastman', 'S',
  'Consultant', True);

  
-- 7/1/17 Justin Bjarnason
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'JBJ', '147741', 
  'BJARNASON, JUSTIN', 'Justin', 'Bjarnason', 'Justin Bjarnason', 'S',
  'Consultant', True);
  
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'JBJ', '147741', 
  'BJARNASON, JUSTIN', 'Justin', 'Bjarnason', 'Justin Bjarnason', 'S',
  'Consultant', True);
  
-- 7/1/17 Brian Schneider
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'BSC', '1566882', 
  'SCHNEIDER, BRIAN', 'Brian', 'Schneider', 'Brian Schneider', 'S',
  'Consultant', True);
  
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'BSC', '1566882', 
  'SCHNEIDER, BRIAN', 'Brian', 'Schneider', 'Brian Schneider', 'S',
  'Consultant', True);  
  
-- 7/8/17 Ross Belgarde
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'RBE', '12242', 
  'BELGARDE, ROSS', 'Ross', 'Belgarde', 'Ross Belgarde', 'S',
  'Consultant', True);
  
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2', 'RBE', '12242', 
  'BELGARDE, ROSS', 'Ross', 'Belgarde', 'Ross Belgarde', 'S',
  'Consultant', True);   