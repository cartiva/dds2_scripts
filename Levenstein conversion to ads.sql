-- The STUFF function inserts a string into another string. It deletes a 
-- specified length of characters in the first string at the start position 
-- and then inserts the second string into the first string at the start position.
-- STUFF ( character_expression , start , length , replaceWith_expression )

need an ads version of stuff

tsql:
--Step 2
SET @i=0
WHILE @i<=@n
	BEGIN
	SET @d=STUFF(@d,@i+1,1,CHAR(@i))--d(i, 0) = i
	SET @i=@i+1
	END

    --Step 2
    @i=0;
    WHILE @i<=@n DO
      @d = INSERT(@d,@i+1,1,CHAR(@i));--d(i, 0) = i
      @i=@i+1;
    END WHILE;

SELECT repeat(CHAR(0),100) FROM system.iota    

failes IN step 2 WHILE @i < @m WHEN @i = 8