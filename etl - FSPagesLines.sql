--
--select fxfact, fxgact, fxmpge, fxmlne
--from rydedata.ffpxrefdta fs
--left join eisglobal.sypffxmst sy on fs.fxcyy = sy.fxmcyy and fs.fxfact = sy.fxmact and fs.fxcode = sy.fxmcde
--where fxconsol = ''
--  and fxcyy = 2011
--  and fxgact <> ''
--
--
--This is “stgFSPagesLines”
--

-- SELECT * FROM system.columns
-- based ON a query
-- DROP TABLE FSPagesLines
CREATE TABLE stgFSPagesLines (
fxcyy integer,
fxfact cichar(10),
fxgact cichar(10),
fxmpge integer,
fxmlne double) IN database;            

-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'stgFSPagesLines';
@i = (
  SELECT MAX(Field_Num)
  FROM system.columns
  WHERE parent = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = coalesce((SELECT '[' + TRIM(column_name) + '], '  FROM #pymast WHERE ordinal_position = @j), '');
-- just COLUMN names
  @col = coalesce((SELECT TRIM(name) + ', '  FROM system.columns WHERE Field_Num = @j and parent = @tableName), '');
-- parameter names  
--  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM #pymast WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'stgFSPagesLines';
@i = (
  SELECT MAX(Field_Num)
  FROM system.columns
  WHERE parent = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(name), '#', '') + ', '  FROM system.columns WHERE Field_Num = @j and parent = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgFSPagesLines';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
 
