-- IS edwEmployeeDim ok to use?
-- no missing records
SELECT *  
FROM tmpSSN a
LEFT JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber 
  AND b.currentrow = true
  AND b.active = 'active'
WHERE b.storecode IS NULL
-- no dup records
SELECT a.employeenumber 
FROM tmpSSN a
LEFT JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber 
  AND b.currentrow = true
  AND b.active = 'active'  
GROUP BY a.employeenumber 
HAVING COUNT(*) > 1  
-- ok

SELECT ymco# AS Company, fullparttime AS "Full/Part Time",
  CASE 
    WHEN lastname LIKE 'RYDELL%' THEN 'Owner'
    ELSE 'Active'
  END AS "Employer Defined Contribution Code Description",
  a.employeenumber AS "Employee ID", hiredate AS "Date of Hire",
  '1' AS "Employement Status Code", 
  replace(a.ssn, '-', '') AS "Subscriber/Family SSN",
  'xxxxxxxxx' AS "Member SSN", 
  'XX' AS "Relationship Type Code", 
  Firstname, middlename, lastname, BirthDate, c.ymsex AS Gender,
  TRIM(coalesce(ymarea, '')) + TRIM(coalesce(ymphon, '')) AS "Contact Phone Number",
  '' AS "Email Address", 
  ymstre AS "Home/Permanet Street Address1",
  '' AS "Home/Permanent Street Address2", 
  ymcity AS "Home/Permanent City", ymstat AS "Home/Permanent State",
  ymzip AS "Home/Permanent Zip Code", 
  '1' AS "Qualifying Event Code",
  '' AS "Qualifying Event Date", '' AS "Qualifying Event Effective Date", 
  '' AS "Reporting Code 1"
FROM tmpSSN a
LEFT JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber 
  AND b.currentrow = true
  AND b.active = 'active'
LEFT JOIN stgArkonaPYMAST c on a.employeenumber = c.ymempn -- need it for phone & address
ORDER BY lastname

