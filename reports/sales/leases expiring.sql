
select b.fullname AS customer, a.bmstk# AS [stock #], d.description AS [sales consultant],
   a.bmltrm AS [lease term], 
  a.bmdtcap AS [sale date], a.bmdtlp AS [last payment], 
  b.homephone, b.businessphone, b.cellphone, b.email, b.city,
  b.state, b.zip, c.modelyear AS [year], c.make, c.model  
-- SELECT *
FROM stgArkonaBOPMAST a
LEFT JOIN dimCustomer b on a.bmbuyr = b.bnkey
LEFT JOIN dimVehicle c on a.bmvin = c.vin
LEFT JOIN dimSalesPerson d on a.bmpslp = d.salesPersonID
  AND a.bmco# = d.storecode
WHERE a.bmwhsl = 'L'
  AND a.bmco# = 'RY1'
  AND a.bmvtyp = 'N'
  AND a.bmdtlp BETWEEN curdate() AND cast(timestampadd(sql_tsi_month, 6, curdate()) AS sql_date)
  AND a.bmstk# <> ''
ORDER BY a.bmdtlp  
