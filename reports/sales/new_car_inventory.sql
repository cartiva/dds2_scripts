﻿-- pdi: 146301

select *
from fin.dim_account
where account_type = 'asset'
  and department_code = 'NC'
  and store_code = 'ry1'
  and account <> '126104'



select a.control, c.account, c.description, min(b.the_date), max(b.the_date)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_year > 2015
inner join (
  select account_key, account, description
  from fin.dim_account
  where account_type = 'asset'
    and department_code = 'NC'
    and store_code = 'ry1'
    and account <> '126104') c on a.account_key = c.account_key
where a.doc = a.control    
group by a.control, c.account, c.description
order by min(b.the_date)

-- first date in gl as inventory
create temp table gl_in as
select a.control, c.account, c.description, min(b.the_date)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_year > 2014
inner join (
  select account_key, account, description
  from fin.dim_account
  where account_type = 'asset'
    and department_code = 'NC'
    and store_code = 'ry1'
    and account <> '126104') c on a.account_key = c.account_key
where a.doc = a.control    
  and a.amount > 0
group by a.control, c.account, c.description
order by min(b.the_date)


select *
from gl_in a
left join 

-- last gl sale date
create temp table gl_out as
select a.control, c.account, c.description, max(b.the_date)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_year > 2014
inner join (
  select account_key, account, description
  from fin.dim_account
  where account_type = 'asset'
    and department_code = 'NC'
    and store_code = 'ry1'
    and account <> '126104') c on a.account_key = c.account_key
where a.doc = a.control    
  and a.amount < 0
group by a.control, c.account, c.description
order by max(b.the_date)


-- transport check in account 11301d, ctl <> doc
create temp table transport as 
select a.control, a.doc, b.the_date, a.amount, d.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_year > 2015
inner join (
  select account_key, account, description
  from fin.dim_account
  where account = '11301D') c on a.account_key = c.account_key
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key  
where a.doc <> a.control    
  and doc = ref
  and a.amount = 19.60

select * from transport

select * from dds.ext_inpmast where inpmast_stock_number = '29699'

select a.control as stock_number, a.min as gl_date, c.the_date as transport_date, b.max as sale_date,
  d.inpmast_vin as vin, d.year, d.make, d.model, d.body_style, d.model_code
from gl_in a
left join gl_out b on a.control = b.control
left join transport c on a.control = c.control
left join dds.ext_inpmast d on a.control = d.inpmast_stock_number
where a.min > '12/31/2015'




select a.control as stock_number, a.min as gl_date, c.the_date as transport_date, b.max as sale_date,
  d.inpmast_vin as vin, d.year, d.make, d.model, d.body_style, d.model_code,
  f.saletype
from gl_in a
left join gl_out b on a.control = b.control
left join transport c on a.control = c.control
left join dds.ext_inpmast d on a.control = d.inpmast_stock_number
left join ads.ext_fact_vehicle_sale e on a.control = e.stocknumber
left join ads.ext_dim_Car_deal_info f on e.cardealinfokey = f.cardealinfokey
where a.min > '12/31/2015'

