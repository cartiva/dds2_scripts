﻿drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select control, sum(-amount) as amount
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month > 201500
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201705
      and b.page = 16 
      and b.line between 8 and 10
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'
    and d.store = 'RY1') h
group by store, page, line, line_label, control
order by store, control;

select * from step_1

select control as "stock#", year, make, model, odometer, date_acq as "date acquired", 
  date_del as "date sold", date_del - date_acq as "age at sale", amount as gross,
  case
    when date_del - date_acq < 31 then '00 - 30'
    when date_del - date_acq between 31 and 60 then '31 - 60'
    when date_del - date_acq between 61 and 90 then '61 - 90'
    when date_del - date_acq between 91 and 120 then '91 - 120'
    when date_del - date_acq > 120 then 'OVER 120'
  end as "Age Band"
from (
  select a.*, b.year, b.make, b.model, b.odometer, b.date_delivered,
    (select arkona.db2_integer_to_date_long(date_in_invent)) as date_acq,
    (select arkona.db2_integer_to_date_long(date_delivered)) as date_del
  -- select *  
  from step_1 a
  inner join dds.ext_inpmast b on a.control = b.inpmast_stock_number
  where (select arkona.db2_integer_to_date_long(date_delivered))  
      > (select arkona.db2_integer_to_date_long(date_in_invent)) 
    and b.date_delivered > 20160000) c
order by date_del - date_acq



select "Age Band", count(*) as units, round(sum(gross), 0) total_gross, 
  round(sum(gross)/count(*), 0) as per_unit
from (
  select control as "stock#", year, make, model, odometer, date_acq as "date acquired", 
    date_del as "date sold", date_del - date_acq as "age at sale", amount as gross,
    case
      when date_del - date_acq < 31 then '00 - 30'
      when date_del - date_acq between 31 and 60 then '31 - 60'
      when date_del - date_acq between 61 and 90 then '61 - 90'
      when date_del - date_acq between 91 and 120 then '91 - 120'
      when date_del - date_acq > 120 then 'OVER 120'
    end as "Age Band"
  from (
    select a.*, b.year, b.make, b.model, b.odometer, b.date_delivered,
      (select arkona.db2_integer_to_date_long(date_in_invent)) as date_acq,
      (select arkona.db2_integer_to_date_long(date_delivered)) as date_del
    -- select *  
    from step_1 a
    inner join dds.ext_inpmast b on a.control = b.inpmast_stock_number
    where (select arkona.db2_integer_to_date_long(date_delivered))  
        > (select arkona.db2_integer_to_date_long(date_in_invent)) 
      and b.date_delivered > 20160000) c) d
  group by "Age Band"    


-- 6/9/17  talked to dave, what he is interested in is comparying the wholesale of fresh (0-30) vehicles
-- need year_month of sale

select control as "stock#", year, make, model, odometer, date_acq as "date acquired", 
  date_del as "date sold", date_del - date_acq as "age at sale", amount as gross,
  case
    when date_del - date_acq < 31 then '00 - 30'
    when date_del - date_acq between 31 and 60 then '31 - 60'
    when date_del - date_acq between 61 and 90 then '61 - 90'
    when date_del - date_acq between 91 and 120 then '91 - 120'
    when date_del - date_acq > 120 then 'OVER 120'
  end as "Age Band",
  d.year_month as year_month_sold
from (
  select a.*, b.year, b.make, b.model, b.odometer, b.date_delivered,
    (select arkona.db2_integer_to_date_long(date_in_invent)) as date_acq,
    (select arkona.db2_integer_to_date_long(date_delivered)) as date_del
  -- select *  
  from step_1 a
  inner join dds.ext_inpmast b on a.control = b.inpmast_stock_number
  where (select arkona.db2_integer_to_date_long(date_delivered))  
      > (select arkona.db2_integer_to_date_long(date_in_invent)) 
    and b.date_delivered > 20160000) c
left join dds.dim_date d on c.date_del = d.the_date 
where (d.year_month between 201601 and 201605 or d.year_month between 201701 and 201705)
order by year_month_sold, "Age Band"

-- 2016 left joined to 2017
-- check these numbers agains fs
select * 
from ( -- m: 2016
  select  year_month_sold, "Age Band", count(*) as units, round(sum(gross), 0) as gross
  from (
  select control as "stock#", year, make, model, odometer, date_acq as "date acquired", 
    date_del as "date sold", date_del - date_acq as "age at sale", amount as gross,
    case
      when date_del - date_acq < 31 then '00 - 30'
      when date_del - date_acq between 31 and 60 then '31 - 60'
      when date_del - date_acq between 61 and 90 then '61 - 90'
      when date_del - date_acq between 91 and 120 then '91 - 120'
      when date_del - date_acq > 120 then 'OVER 120'
    end as "Age Band",
    d.year_month as year_month_sold
  from (
    select a.*, b.year, b.make, b.model, b.odometer, b.date_delivered,
      (select arkona.db2_integer_to_date_long(date_in_invent)) as date_acq,
      (select arkona.db2_integer_to_date_long(date_delivered)) as date_del
    -- select *  
    from step_1 a
    inner join dds.ext_inpmast b on a.control = b.inpmast_stock_number
    where (select arkona.db2_integer_to_date_long(date_delivered))  
        > (select arkona.db2_integer_to_date_long(date_in_invent)) 
      and b.date_delivered > 20160000) c
  left join dds.dim_date d on c.date_del = d.the_date 
  where d.year_month between 201601 and 201605) x
  group by year_month_sold, "Age Band") m
left join ( -- 2017  
  select  year_month_sold, "Age Band", count(*) as units, round(sum(gross), 0) as gross
  from (
  select control as "stock#", year, make, model, odometer, date_acq as "date acquired", 
    date_del as "date sold", date_del - date_acq as "age at sale", amount as gross,
    case
      when date_del - date_acq < 31 then '00 - 30'
      when date_del - date_acq between 31 and 60 then '31 - 60'
      when date_del - date_acq between 61 and 90 then '61 - 90'
      when date_del - date_acq between 91 and 120 then '91 - 120'
      when date_del - date_acq > 120 then 'OVER 120'
    end as "Age Band",
    d.year_month as year_month_sold
  from (
    select a.*, b.year, b.make, b.model, b.odometer, b.date_delivered,
      (select arkona.db2_integer_to_date_long(date_in_invent)) as date_acq,
      (select arkona.db2_integer_to_date_long(date_delivered)) as date_del
    -- select *  
    from step_1 a
    inner join dds.ext_inpmast b on a.control = b.inpmast_stock_number
    where (select arkona.db2_integer_to_date_long(date_delivered))  
        > (select arkona.db2_integer_to_date_long(date_in_invent)) 
      and b.date_delivered > 20160000) c
  left join dds.dim_date d on c.date_del = d.the_date 
  where d.year_month between 201701 and 201705) x
  group by year_month_sold, "Age Band") n on m.year_month_sold + 100 = n.year_month_sold and m."Age Band" = n."Age Band"



-- from luigi:sales_consultant_payroll.AccountsRoutes
-- these are the accounts for ry1 wholesale deals
select * 
from sls.deals_accounts_routes
where page = 16
  and store_code = 'ry1'
  and line in (8,10)

-- close but not close enuf, eg 201603, count off by 3
-- count seems like the only issue, gross is fine, which leads me to think the issue is not 
-- the raw data, but how count is being calculated
-- adding VSN brought it from 39 to 41, still missing 1
-- added PVU and now 42
-- all good until 201702: gross was off, added line 9, good
-- all months check out.
drop table if exists count_gross;
create temp table count_gross as
select b.year_month, control, 
  round(sum(case when c.account_type_code = '4' then -a.amount else 0 end), 0) as sales,
  round(sum(case when c.account_type_code = '5' then -a.amount else 0 end), 0) as cogs,
  round(sum(-a.amount), 0) as gross,
  sum(case when c.account_type_code = '4' and aa.journal_code in('VSN','VSU','PVU') and a.amount < 0 then 1 else 0 end) as unit_count
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and  (b.year_month between 201601 and 201605 or b.year_month between 201701 and 201705)
inner join fin.dim_account c on a.account_key = c.account_key
-- -- add journal for count only
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join sls.deals_accounts_routes d on c.account = d.gl_account  
  and d.page = 16
    and d.store_code = 'ry1'
    and d.line between 8 and 10
where a.post_status = 'Y'
group by b.year_month, control;

select year_month, sum(unit_count) as units, sum(gross) as gross
from count_gross
group by year_month
order by year_month

select * from count_gross

now need the info from inpmast
-- first item of note,  inpmast is not going to have a record of the gm stocknumber
-- on a vehicle that has been wholesaled (intramarket) to honda
select a.*, b.year, b.make, b.model, b.odometer, b.date_delivered,
  (select arkona.db2_integer_to_date_long(date_in_invent)) as date_acq,
  (select arkona.db2_integer_to_date_long(date_delivered)) as date_del
from count_gross a
left join dds.ext_inpmast b on a.control = b.inpmast_stock_number
where b.year is null
-- where (select arkona.db2_integer_to_date_long(date_delivered))  
--     > (select arkona.db2_integer_to_date_long(date_in_invent)) 
--   and b.date_delivered > 20160000
     
select *
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join 
where a.control = '30892P'
      
    select a.*, b.year, b.make, b.model, b.odometer, b.date_delivered,
      (select arkona.db2_integer_to_date_long(date_in_invent)) as date_acq,
      (select arkona.db2_integer_to_date_long(date_delivered)) as date_del
    -- select *  
    from step_1 a
    inner join dds.ext_inpmast b on a.control = b.inpmast_stock_number
    where (select arkona.db2_integer_to_date_long(date_delivered))  
        > (select arkona.db2_integer_to_date_long(date_in_invent)) 
      and b.date_delivered > 20160000

