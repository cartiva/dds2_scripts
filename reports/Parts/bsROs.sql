SELECT *
FROM stgarkonapdptdet
WHERE ptinv# LIKE '180%'
  AND ptdate BETWEEN '06/01/2012' AND '06/30/2012'
  
SELECT franchise, SUM(partscost), SUM(partssales)
-- SELECT *
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
WHERE LEFT(ro, 2) = '18'
  AND b.thedate BETWEEN '06/01/2012' AND '06/30/2012'
  AND void = false
GROUP BY franchise  

SELECT d.franchise, SUM(partscost), SUM(partssales)
FROM factro d
INNER JOIN (
  SELECT b.thedate, a.ro
  FROM factroline a 
  INNER JOIN day b ON a.linedatekey = b.datekey
  WHERE servicetype = 'bs'
    AND b.thedate BETWEEN '06/01/2012' AND '06/30/2012'
  GROUP BY b.thedate, a.ro) c ON d.ro = c.ro
GROUP BY d.franchise  

-- 7/2
-- request FROM GM Peter O

-- ro's of interest
SELECT b.thedate, a.vin, a.ro, a.partscost,
  CASE
    WHEN 
      substring(vin, 2, 1) = 'G'  OR
      substring(vin, 1, 2) = 'KL' OR
      substring(vin, 1, 2) = '5Y' OR 
      substring(vin, 1, 2) = '2C' OR 
      substring(vin, 1, 3) = '1Y1' OR 
      substring(vin, 1, 3) = 'W06' OR
      substring(vin, 1, 3) = '1Z3' OR
      substring(vin, 1, 3) = 'CKL' OR
      substring(vin, 1, 3) = 'W08' THEN 'GM'
    ELSE 'Not GM'
  end
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND (thedate BETWEEN '01/01/2011' AND '04/30/2011' OR thedate BETWEEN '01/01/2012' AND '04/30/2012')
WHERE a.void = false
  AND a.franchise = 'ot'
  AND EXISTS (
    SELECT 1
    FROM factroline
    WHERE ro = a.ro
      AND servicetype = 'bs')
      
SELECT ptinv#, ptpart, sum(ptqty*ptcost)
FROM stgArkonaPDPTDET
WHERE LEFT(ptinv#, 6) = '180078'
GROUP BY ptinv#, ptpart   

SELECT b.thedate, a.ro, a.partscost, c.*,
  CASE
    WHEN 
      substring(vin, 2, 1) = 'G'  OR
      substring(vin, 1, 2) = 'KL' OR
      substring(vin, 1, 2) = '5Y' OR 
      substring(vin, 1, 2) = '2C' OR 
      substring(vin, 1, 3) = '1Y1' OR 
      substring(vin, 1, 3) = 'W06' OR
      substring(vin, 1, 3) = '1Z3' OR
      substring(vin, 1, 3) = 'CKL' OR
      substring(vin, 1, 3) = 'W08' THEN 'GM'
    ELSE 'Not GM'
  end 
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND (thedate BETWEEN '01/01/2011' AND '04/30/2011' OR thedate BETWEEN '01/01/2012' AND '04/30/2012')
LEFT JOIN (
  SELECT ptinv#, ptpart, sum(ptqty*ptcost) AS cost
  FROM stgArkonaPDPTDET
  WHERE (LEFT(ptinv#,4) = '1800' OR LEFT(ptinv#,4) = '1801')
    AND ptmanf = 'OT'  
  GROUP BY ptinv#, ptpart) c ON a.ro = c.ptinv#
WHERE a.void = false
  AND a.franchise = 'ot'
  AND EXISTS (
    SELECT 1
    FROM factroline
    WHERE ro = a.ro
      AND servicetype = 'bs')
  AND c.ptinv# IS NOT NULL   
  
-- shit, have to cull out "hard parts"  
SELECT b.thedate, a.ro, a.partscost, c.*, d.* 
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND (thedate BETWEEN '01/01/2011' AND '04/30/2011' OR thedate BETWEEN '01/01/2012' AND '04/30/2012')
INNER JOIN factroline d ON a.ro = d.ro
  AND d.servicetype = 'bs'  
LEFT JOIN (
  SELECT ptinv#, ptpart, ptline, ptsgrp, ptqty, ptcost
  FROM stgArkonaPDPTDET
  WHERE (LEFT(ptinv#,4) = '1800' OR LEFT(ptinv#,4) = '1801')
    AND ptmanf = 'OT'
    AND ptsgrp NOT IN ('501','510','511','512','514','515','518','900')) c ON a.ro = c.ptinv#
    AND c.ptline = d.line
WHERE a.void = false
  AND a.franchise = 'ot'
  AND c.ptinv# IS NOT NULL   


SELECT c.ptpart, COUNT(*)
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND (thedate BETWEEN '01/01/2011' AND '04/30/2011' OR thedate BETWEEN '01/01/2012' AND '04/30/2012')
INNER JOIN factroline d ON a.ro = d.ro
  AND d.servicetype = 'bs'  
LEFT JOIN (
  SELECT ptinv#, ptpart, ptline, ptsgrp, ptqty, ptcost
  FROM stgArkonaPDPTDET
  WHERE (LEFT(ptinv#,4) = '1800' OR LEFT(ptinv#,4) = '1801')
    AND ptmanf = 'OT'
    AND ptsgrp NOT IN ('501','510','511','512','514','515','518','900')) c ON a.ro = c.ptinv#
    AND c.ptline = d.line
WHERE a.void = false
  AND a.franchise = 'ot'
  AND c.ptinv# IS NOT NULL   
  AND b.thedate > '03/01/2012'   
GROUP BY c.ptpart  

  
  
SELECT ptsgrp, COUNT(*)
FROM stgArkonaPDPTDET
WHERE (LEFT(ptinv#,4) = '1800' OR LEFT(ptinv#,4) = '1801')
  AND ptmanf = 'OT'  
GROUP BY ptsgrp
  
exclude ptsgrp
501: bg products
510: other oil
511: mobil 1
512: dexos
514: bulk oil
515: 5/20
518: quart oil
900: Bodyshop Hardware

SELECT *
from stgArkonaPDPTDET
WHERE ptpart = '8880'
  
      
 
                     

SELECT opcode, COUNT(*)
FROM factroline
WHERE opcode LIKE 'pdq%'
GROUP BY opcode      

-- 8/6/12
-- need to exclude non gm vehicles

  
-- this might be close  
SELECT b.thedate, a.ro, a.partscost, c.*, d.* 
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND (thedate BETWEEN '01/01/2011' AND '04/30/2011' OR thedate BETWEEN '01/01/2012' AND '04/30/2012')
INNER JOIN factroline d ON a.ro = d.ro
  AND d.servicetype = 'bs'  
LEFT JOIN (
  SELECT ptinv#, ptpart, ptline, ptsgrp, ptqty, ptcost
  FROM stgArkonaPDPTDET
  WHERE (LEFT(ptinv#,4) = '1800' OR LEFT(ptinv#,4) = '1801')
    AND ptmanf = 'OT'
    AND ptsgrp NOT IN ('501','510','511','512','514','515','518','900')) c ON a.ro = c.ptinv#
    AND c.ptline = d.line
WHERE a.void = false
  AND c.ptinv# IS NOT NULL
  AND (
      substring(a.vin, 2, 1) = 'G'  OR
      substring(a.vin, 1, 2) = 'KL' OR
      substring(a.vin, 1, 2) = '5Y' OR 
      substring(a.vin, 1, 2) = '2C' OR 
      substring(a.vin, 1, 3) = '1Y1' OR 
      substring(a.vin, 1, 3) = 'W06' OR
      substring(a.vin, 1, 3) = '1Z3' OR
      substring(a.vin, 1, 3) = 'CKL' OR
      substring(a.vin, 1, 3) = 'W08')      
      
SELECT c.ptpart, sum(a.partscost) 
INTO #2011       
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND thedate BETWEEN '01/01/2011' AND '04/30/2011'
INNER JOIN factroline d ON a.ro = d.ro
  AND d.servicetype = 'bs'  
LEFT JOIN (
  SELECT ptinv#, ptpart, ptline, ptsgrp, ptqty, ptcost
  FROM stgArkonaPDPTDET
  WHERE (LEFT(ptinv#,4) = '1800' OR LEFT(ptinv#,4) = '1801')
    AND ptmanf = 'OT'
    AND ptsgrp NOT IN ('501','510','511','512','514','515','518','900')) c ON a.ro = c.ptinv#
    AND c.ptline = d.line
WHERE a.void = false
  AND c.ptinv# IS NOT NULL
  AND (
      substring(a.vin, 2, 1) = 'G'  OR
      substring(a.vin, 1, 2) = 'KL' OR
      substring(a.vin, 1, 2) = '5Y' OR 
      substring(a.vin, 1, 2) = '2C' OR 
      substring(a.vin, 1, 3) = '1Y1' OR 
      substring(a.vin, 1, 3) = 'W06' OR
      substring(a.vin, 1, 3) = '1Z3' OR
      substring(a.vin, 1, 3) = 'CKL' OR
      substring(a.vin, 1, 3) = 'W08')        
GROUP BY c.ptpart      

SELECT c.ptpart, sum(a.partscost) 
INTO #2012      
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND thedate BETWEEN '01/01/2012' AND '04/30/2012'
INNER JOIN factroline d ON a.ro = d.ro
  AND d.servicetype = 'bs'  
LEFT JOIN (
  SELECT ptinv#, ptpart, ptline, ptsgrp, ptqty, ptcost
  FROM stgArkonaPDPTDET
  WHERE (LEFT(ptinv#,4) = '1800' OR LEFT(ptinv#,4) = '1801')
    AND ptmanf = 'OT'
    AND ptsgrp NOT IN ('501','510','511','512','514','515','518','900')) c ON a.ro = c.ptinv#
    AND c.ptline = d.line
WHERE a.void = false
  AND c.ptinv# IS NOT NULL
  AND (
      substring(a.vin, 2, 1) = 'G'  OR
      substring(a.vin, 1, 2) = 'KL' OR
      substring(a.vin, 1, 2) = '5Y' OR 
      substring(a.vin, 1, 2) = '2C' OR 
      substring(a.vin, 1, 3) = '1Y1' OR 
      substring(a.vin, 1, 3) = 'W06' OR
      substring(a.vin, 1, 3) = '1Z3' OR
      substring(a.vin, 1, 3) = 'CKL' OR
      substring(a.vin, 1, 3) = 'W08')        
GROUP BY c.ptpart 

SELECT *
FROM #2011 a
full JOIN #2012 b ON a.ptpart = b.ptpart

-- duh, ro man for the vehicle
DROP TABLE #2011;
DROP TABLE #2012;
SELECT c.ptpart, sum(a.partscost) 
INTO #2011      
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND thedate BETWEEN '01/01/2011' AND '04/30/2011'
INNER JOIN factroline d ON a.ro = d.ro
  AND d.servicetype = 'bs'  
LEFT JOIN (
  SELECT ptinv#, ptpart, ptline, ptsgrp, ptqty, ptcost
  FROM stgArkonaPDPTDET
  WHERE (LEFT(ptinv#,4) = '1800' OR LEFT(ptinv#,4) = '1801')
    AND ptmanf = 'OT'
    AND ptsgrp NOT IN ('501','510','511','512','514','515','518','900')) c ON a.ro = c.ptinv#
    AND c.ptline = d.line
WHERE a.void = false
  AND c.ptinv# IS NOT NULL
  AND a.franchise = 'gm'       
GROUP BY c.ptpart;
SELECT c.ptpart, sum(a.partscost) 
INTO #2012      
FROM factro a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND thedate BETWEEN '01/01/2012' AND '04/30/2012'
INNER JOIN factroline d ON a.ro = d.ro
  AND d.servicetype = 'bs'  
LEFT JOIN (
  SELECT ptinv#, ptpart, ptline, ptsgrp, ptqty, ptcost
  FROM stgArkonaPDPTDET
  WHERE (LEFT(ptinv#,4) = '1800' OR LEFT(ptinv#,4) = '1801')
    AND ptmanf = 'OT'
    AND ptsgrp NOT IN ('501','510','511','512','514','515','518','900')) c ON a.ro = c.ptinv#
    AND c.ptline = d.line
WHERE a.void = false
  AND c.ptinv# IS NOT NULL
  AND a.franchise = 'gm'       
GROUP BY c.ptpart

SELECT *
FROM #2011 a
inner JOIN #2012 b ON a.ptpart = b.ptpart