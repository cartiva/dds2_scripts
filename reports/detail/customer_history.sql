/*
Good Morning Jon.  We would like you to compile two lists of customer detail 
work we have done.  The labor op codes are INT (Interior), TOT (Total Detail) 
and SHO (Showroom Shine).  The first list are to be from those customers 
we detailed vehicles for from 6 months ago to 18 months ago, and the second 
list is for all the rest that do not fit in that time frame.  We are then going 
to forward this list to Mike D. to do 2 different Facebook ads to drive more 
customer pay detail work.  Your help is greatly appreciated with this project!

Thank You,
Steve
*/
select * 
FROM dimopcode
WHERE opcode IN ('INT','TOT','SHO')


select a.ro, b.opcode, c.thedate, d.fullname, lastname, firstname, homephone,
  cellphone, 
  CASE hasvalidemail
    WHEN true THEN coalesce(email, email2)
    ELSE 'None'
  END AS email, 
  e.bnadr1, e.bnadr2, e.bncity, e.bnstcd, e.bnzip 
FROM factrepairorder a
INNER JOIN dimopcode b on a.opcodekey = b.opcodekey
  AND b.opcode IN ('INT','TOT','SHO')
INNER JOIN day c on a.finalclosedatekey = c.datekey  
INNER JOIN dimcustomer d on a.customerkey = d.customerkey
  AND d.customertypecode = 'I'
LEFT JOIN stgarkonabopname e on d.bnkey = e.bnkey  
ORDER BY ro


SELECT * FROM dimcustomer

select * FROM stgarkonabopname WHERE bnadr2 <> '' AND bntype = 'I'

DROP TABLE #wtf;
select a.ro, b.opcode, c.thedate, d.fullname, lastname, firstname, homephone,
  cellphone, 
  CASE hasvalidemail
    WHEN true THEN coalesce(email, email2)
    ELSE 'None'
  END AS email, 
  e.bnadr1 AS addr1, e.bnadr2 AS addr2, e.bncity AS city, 
  e.bnstcd AS state, e.bnzip AS zip
INTO #wtf  
FROM factrepairorder a
INNER JOIN dimopcode b on a.opcodekey = b.opcodekey
  AND b.opcode IN ('INT','TOT','SHO')
INNER JOIN day c on a.finalclosedatekey = c.datekey  
INNER JOIN dimcustomer d on a.customerkey = d.customerkey
  AND d.customertypecode = 'I'
LEFT JOIN stgarkonabopname e on d.bnkey = e.bnkey  
GROUP BY a.ro, b.opcode, c.thedate, d.fullname, lastname, firstname, homephone,
  cellphone, 
  CASE hasvalidemail
    WHEN true THEN coalesce(email, email2)
    ELSE 'None'
  END,
  e.bnadr1, e.bnadr2, e.bncity, e.bnstcd, e.bnzip;
 
--SELECT COUNT(*) FROM (  
select fullname,lastname,firstname,homephone,cellphone,email,addr1,addr2,
  city,state,zip
INTO #six_18  
FROM #wtf  
WHERE thedate BETWEEN curdate() - 540 AND curdate() - 180
  AND firstname <> ''
GROUP BY fullname,lastname,firstname,homephone,cellphone,email,addr1,addr2,
  city,state,zip
--) x  

/*
Just straight customers, no Fleet such as Auto Finance, Pro Transport, 
  Trans Systems and Strata and NDDOT. Labor op code, date of service, 
  phone number and e-mail for guest info on list.

Thank You,
Steve
*/


DROP TABLE #wtf;
select b.opcode, c.thedate, d.fullname, homephone,
  cellphone, 
  CASE hasvalidemail
    WHEN true THEN coalesce(email, email2)
    ELSE 'None'
  END AS email
INTO #wtf  
FROM factrepairorder a
INNER JOIN dimopcode b on a.opcodekey = b.opcodekey
  AND b.opcode IN ('INT','TOT','SHO')
INNER JOIN day c on a.finalclosedatekey = c.datekey  
  AND c.thedate < curdate()
INNER JOIN dimcustomer d on a.customerkey = d.customerkey
  AND d.customertypecode = 'I'
GROUP BY b.opcode, c.thedate, d.fullname, homephone,
  cellphone, 
  CASE hasvalidemail
    WHEN true THEN coalesce(email, email2)
    ELSE 'None'
  END;

select * FROM #wtf
  
SELECT * 
INTO #list_1
FROM (  
  SELECT opcode, MAX(thedate) AS thedate, fullname, homephone,cellphone, email 
  FROM #wtf 
  GROUP BY opcode, fullname, homephone,cellphone, email) x 
WHERE thedate BETWEEN curdate() - 540 AND curdate() - 180;


SELECT * 
INTO #list_2
FROM (  
  SELECT opcode, MAX(thedate) AS thedate, fullname, homephone,cellphone, email 
  FROM #wtf 
  GROUP BY opcode, fullname, homephone,cellphone, email) x 
WHERE thedate < curdate() - 540 OR thedate > curdate() - 180;

/*
Just the one from 8 months ago.

From: Jon Andrews [mailto:jandrews@cartiva.com] 
Sent: Monday, June 06, 2016 11:05 AM
To: 'Steve Bergh' <sbergh@rydellcars.com>
Subject: RE: Detail Customer Lists

If I had a detail done 8 months ago and another one done 2 months ago, 
do you want me to show up on both lists?
*/

select a.* 
FROM #list_2 a
LEFT JOIN #list_1 b on a.fullname = b.fullname
WHERE b.fullname IS NULL 