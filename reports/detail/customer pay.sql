-- Customer Pay opcodes: SHO, INT, EXT, TOT
DROP TABLE #detail;
SELECT a.ro, b.thedate, b.yearmonth, left(d.fullname, 40) AS cust, e.opcode, left(e.description, 25)
INTO #detail  
FROM factRepairOrder a
INNER JOIN day b on a.closedatekey = b.datekey
  AND b.theyear = 2015
INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
  AND c.serviceTypeCode = 'RE'
INNER JOIN dimcustomer d on a.customerkey = d.customerkey
INNER JOIN dimopcode e on a.opcodekey = e.opcodekey
  AND e.opcode IN ('SHO','INT','EXT','TOT')
-- multiple ros IF multiple techs flagged time  
GROUP BY a.ro, b.thedate, b.yearmonth, left(d.fullname, 40), e.opcode, left(e.description, 25)  

SELECT * FROM #detail ORDER BY thedate desc
SELECT ro FROM #detail GROUP BY ro HAVING COUNT(*) > 1

select cust, COUNT(*) FROM #detail GROUP BY cust ORDER BY COUNT(*) DESC 

SELECT * FROM #detail WHERE cust = 'INVENTORY' ORDER BY thedate DESC 

SELECT d.opcode, d.jan, e.feb, f.mar, g.apr, h.may
FROM (
  SELECT '1-ALL'as opcode, SUM(CASE WHEN a.yearmonth = 201501 THEN 1 ELSE 0 END) AS Jan FROM #detail a
  UNION 
  SELECT '2-SHO', SUM(CASE WHEN a.yearmonth = 201501 AND opcode = 'SHO' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '3-TOT', SUM(CASE WHEN a.yearmonth = 201501 AND opcode = 'TOT' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '4-EXT', SUM(CASE WHEN a.yearmonth = 201501 AND opcode = 'EXT' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '5-INT', SUM(CASE WHEN a.yearmonth = 201501 AND opcode = 'INT' THEN 1 ELSE 0 END) FROM #detail a) d
LEFT JOIN (
  SELECT '1-Total'as opcode, SUM(CASE WHEN a.yearmonth = 201502 THEN 1 ELSE 0 END) AS Feb FROM #detail a
  UNION 
  SELECT '2-SHO', SUM(CASE WHEN a.yearmonth = 201502 AND opcode = 'SHO' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '3-TOT', SUM(CASE WHEN a.yearmonth = 201502 AND opcode = 'TOT' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '4-EXT', SUM(CASE WHEN a.yearmonth = 201502 AND opcode = 'EXT' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '5-INT', SUM(CASE WHEN a.yearmonth = 201502 AND opcode = 'INT' THEN 1 ELSE 0 END) FROM #detail a) e on LEFT(d.opcode,1) = LEFT(e.opcode,1)
LEFT JOIN (
  SELECT '1-Total'as opcode, SUM(CASE WHEN a.yearmonth = 201503 THEN 1 ELSE 0 END) AS Mar FROM #detail a
  UNION 
  SELECT '2-SHO', SUM(CASE WHEN a.yearmonth = 201503 AND opcode = 'SHO' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '3-TOT', SUM(CASE WHEN a.yearmonth = 201503 AND opcode = 'TOT' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '4-EXT', SUM(CASE WHEN a.yearmonth = 201503 AND opcode = 'EXT' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '5-INT', SUM(CASE WHEN a.yearmonth = 201503 AND opcode = 'INT' THEN 1 ELSE 0 END) FROM #detail a) f on LEFT(e.opcode,1) = LEFT(f.opcode,1) 
LEFT JOIN (
  SELECT '1-Total'as opcode, SUM(CASE WHEN a.yearmonth = 201504 THEN 1 ELSE 0 END) AS Apr FROM #detail a
  UNION 
  SELECT '2-SHO', SUM(CASE WHEN a.yearmonth = 201504 AND opcode = 'SHO' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '3-TOT', SUM(CASE WHEN a.yearmonth = 201504 AND opcode = 'TOT' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '4-EXT', SUM(CASE WHEN a.yearmonth = 201504 AND opcode = 'EXT' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '5-INT', SUM(CASE WHEN a.yearmonth = 201504 AND opcode = 'INT' THEN 1 ELSE 0 END) FROM #detail a) g on LEFT(f.opcode,1) = LEFT(g.opcode,1)   
LEFT JOIN (
  SELECT '1-Total'as opcode, SUM(CASE WHEN a.yearmonth = 201505 THEN 1 ELSE 0 END) AS May FROM #detail a
  UNION 
  SELECT '2-SHO', SUM(CASE WHEN a.yearmonth = 201505 AND opcode = 'SHO' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '3-TOT', SUM(CASE WHEN a.yearmonth = 201505 AND opcode = 'TOT' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '4-EXT', SUM(CASE WHEN a.yearmonth = 201505 AND opcode = 'EXT' THEN 1 ELSE 0 END) FROM #detail a
  UNION 
  SELECT '5-INT', SUM(CASE WHEN a.yearmonth = 201505 AND opcode = 'INT' THEN 1 ELSE 0 END) FROM #detail a) h on LEFT(g.opcode,1) = LEFT(h.opcode,1) 
