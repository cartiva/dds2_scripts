-- BY day
SELECT thedate, dayname, coalesce(round(SUM(flaghours),2), 0)
FROM day a
LEFT JOIN (
  SELECT flagdatekey, round(SUM(flaghours), 2) AS flaghours
  FROM factTechFlagHoursByDay a
  INNER JOIN dimtech b ON a.techkey = b.techkey
    AND b.flagdeptcode = 're'
    AND b.storecode = 'ry1'
  WHERE a.flagdatekey IN (SELECT datekey FROM day WHERE thedate BETWEEN curdate() -14 AND curdate())
  GROUP BY a.flagdatekey) b ON a.datekey = b.flagdatekey
WHERE thedate BETWEEN curdate() - 14 AND curdate()
GROUP BY a.thedate, a.dayname
ORDER BY thedate DESC
-- BY tech/day
SELECT thedate, dayname, technumber, coalesce(round(SUM(flaghours),2), 0)
FROM day a
LEFT JOIN (
  SELECT flagdatekey, technumber, round(SUM(flaghours), 2) AS flaghours
  FROM factTechFlagHoursByDay a
  INNER JOIN dimtech b ON a.techkey = b.techkey
    AND b.flagdeptcode = 're'
    AND b.storecode = 'ry1'
  WHERE a.flagdatekey IN (SELECT datekey FROM day WHERE thedate BETWEEN curdate() -14 AND curdate())
  GROUP BY a.flagdatekey, b.technumber) b ON a.datekey = b.flagdatekey
WHERE thedate BETWEEN curdate() - 14 AND curdate()
GROUP BY a.thedate, a.dayname, b.technumber
ORDER BY thedate DESC


-- 1/12/13 --------------

-- one row for each day & tech combination
SELECT a.thedate, b.technumber, b.techkey, coalesce(b.employeenumber, 'NA')
FROM day a, dimtech b
WHERE b.flagdeptcode = 're'
  AND a.thedate BETWEEN curdate() - 180 AND curdate() - 1  
ORDER BY thedate, technumber


SELECT a.thedate, coalesce(d.employeenumber, 'NA'), d.technumber, d.techkey, 
  coalesce(d.name, description) AS tech, SUM(flaghours) AS flaghours
FROM day a
LEFT JOIN facttechlineflagdatehours b ON a.datekey = b.flagdatekey
INNER JOIN bridgetechgroup c ON b.techgroupkey = c.techgroupkey
INNER JOIN dimtech d ON c.techkey = d.techkey
  AND d.flagdeptcode = 're'
WHERE a.thedate BETWEEN curdate() - 180 AND curdate() - 1  
GROUP BY a.thedate, coalesce(d.employeenumber, 'NA'), d.technumber, d.techkey, 
  coalesce(d.name, description)
  
-- one row for each day & tech combination
-- ADD flaghours  
SELECT s.*, coalesce(flaghours, 0) AS hours, 'flag' AS hourtype
FROM (
  SELECT a.thedate, b.technumber, b.techkey, 
    coalesce(b.employeenumber, 'NA') AS employeenumber,
    coalesce(b.name, description) AS name
  FROM day a, dimtech b
  WHERE b.flagdeptcode = 're'
    AND a.thedate BETWEEN curdate() - 31 AND curdate()) s
LEFT JOIN (
    SELECT a.thedate, coalesce(d.employeenumber, 'NA'), d.technumber, d.techkey, 
      coalesce(d.name, description) AS tech, SUM(flaghours) AS flaghours
    FROM day a
    LEFT JOIN facttechlineflagdatehours b ON a.datekey = b.flagdatekey
    INNER JOIN bridgetechgroup c ON b.techgroupkey = c.techgroupkey
    INNER JOIN dimtech d ON c.techkey = d.techkey
      AND d.flagdeptcode = 're'
    WHERE a.thedate BETWEEN curdate() - 31 AND curdate()
    GROUP BY a.thedate, coalesce(d.employeenumber, 'NA'), d.technumber, d.techkey, 
      coalesce(d.name, description)) t ON s.thedate = t.thedate
  AND s.techkey = t.techkey  
--WHERE s.thedate = '01/09/2013'   
ORDER BY s.thedate, s.technumber    

-- still have to fix the 103 hours shit
SELECT b.thedate, a.* FROM factroline a LEFT JOIN day b ON linedatekey = b.datekey WHERE lineflaghours = 103
  
full OUTER JOIN resulting IN one row per day
ON each day there will be a row for every employee AND every tech
every tech: dimtech.flagdeptcode (RE)
every employee dimEmp.pydeptcode (24)

date    tech    emp#   flag   clock
1/3     135     NA     7        0
1/3           131500   0        8  -- gil

-- flag hours
SELECT s.*, coalesce(flaghours, 0) AS hours, 'flag' AS hourtype
FROM (
  SELECT a.thedate, b.technumber, b.techkey, 
    coalesce(b.employeenumber, 'NA') AS employeenumber,
    coalesce(b.name, description) AS name
  FROM day a, dimtech b
  WHERE b.flagdeptcode = 're'
    AND a.thedate BETWEEN curdate() - 31 AND curdate()) s
LEFT JOIN (
    SELECT a.thedate, coalesce(d.employeenumber, 'NA'), d.technumber, d.techkey, 
      coalesce(d.name, description) AS tech, SUM(flaghours) AS flaghours
    FROM day a
    LEFT JOIN facttechlineflagdatehours b ON a.datekey = b.flagdatekey
    INNER JOIN bridgetechgroup c ON b.techgroupkey = c.techgroupkey
    INNER JOIN dimtech d ON c.techkey = d.techkey
      AND d.flagdeptcode = 're'
    WHERE a.thedate BETWEEN curdate() - 31 AND curdate()
    GROUP BY a.thedate, coalesce(d.employeenumber, 'NA'), d.technumber, d.techkey, 
      coalesce(d.name, description)) t ON s.thedate = t.thedate
  AND s.techkey = t.techkey  
UNION 
-- clockhours
SELECT x.thedate, coalesce(y.technumber, 'NA') AS technumber, 
  coalesce(y.techkey, -1) AS techkey, x.employeenumber, x.name, 
  SUM(clockhours) AS hours, 'clock'
FROM (
  SELECT b.thedate, c.name, c.employeenumber, c.distcode, a.clockhours 
  FROM factclockhourstoday a
  INNER JOIN day b ON a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c ON a.employeekey = c.employeekey
    AND c.pydeptcode = '24'
  UNION
  SELECT a.thedate, c.name, c.employeenumber, c.distcode, b.clockhours
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
    AND c.pydeptcode = '24'
  WHERE a.thedate BETWEEN curdate() - 31 AND curdate()) x
LEFT JOIN dimtech y ON x.employeenumber = y.employeenumber
GROUP BY x.thedate, coalesce(y.technumber, 'NA'), coalesce(y.techkey, -1), 
  x.employeenumber, x.name
ORDER BY s.thedate, s.technumber

DROP TABLE #wtf;
SELECT thedate, name, technumber, techkey, employeenumber,
  SUM(CASE WHEN hourtype = 'flag' THEN hours ELSE 0 END) AS flaghours,
  SUM(CASE WHEN hourtype = 'clock' THEN hours ELSE 0 END) AS clockhours
--INTO #wtf  
FROM (
  SELECT s.*, coalesce(flaghours, 0) AS hours, 'flag' AS hourtype
  FROM (
    SELECT a.thedate, b.technumber, b.techkey, 
      coalesce(b.employeenumber, 'NA') AS employeenumber,
      coalesce(b.name, description) AS name
    FROM day a, dimtech b
    WHERE b.flagdeptcode = 're'
      AND a.thedate BETWEEN curdate() - 61 AND curdate()) s
  LEFT JOIN (
      SELECT a.thedate, coalesce(d.employeenumber, 'NA'), d.technumber, d.techkey, 
        coalesce(d.name, description) AS tech, SUM(flaghours) AS flaghours
      FROM day a
      LEFT JOIN facttechlineflagdatehours b ON a.datekey = b.flagdatekey
      INNER JOIN bridgetechgroup c ON b.techgroupkey = c.techgroupkey
      INNER JOIN dimtech d ON c.techkey = d.techkey
        AND d.flagdeptcode = 're'
      WHERE a.thedate BETWEEN curdate() - 61 AND curdate()
      GROUP BY a.thedate, coalesce(d.employeenumber, 'NA'), d.technumber, d.techkey, 
        coalesce(d.name, description)) t ON s.thedate = t.thedate
    AND s.techkey = t.techkey  
  UNION 
  -- clockhours
  SELECT x.thedate, coalesce(y.technumber, 'NA') AS technumber, 
    coalesce(y.techkey, -1) AS techkey, x.employeenumber, x.name, 
    SUM(clockhours) AS hours, 'clock'
  FROM (
    SELECT b.thedate, c.name, c.employeenumber, c.distcode, a.clockhours 
    FROM factclockhourstoday a
    INNER JOIN day b ON a.datekey = b.datekey
    INNER JOIN edwEmployeeDim c ON a.employeekey = c.employeekey
      AND c.pydeptcode = '24'
    UNION
    SELECT a.thedate, c.name, c.employeenumber, c.distcode, b.clockhours
    FROM day a
    LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
    INNER JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
      AND c.pydeptcode = '24'
    WHERE a.thedate BETWEEN curdate() - 61 AND curdate()) x
  LEFT JOIN dimtech y ON x.employeenumber = y.employeenumber
  GROUP BY x.thedate, coalesce(y.technumber, 'NA'), coalesce(y.techkey, -1), 
    x.employeenumber, x.name) w  
GROUP BY thedate, name, technumber, techkey, employeenumber    


SELECT * FROM #wtf

SELECT b.*
FROM day a
LEFT JOIN #wtf b ON a.thedate = b.thedate
  AND b.flaghours + b.clockhours <> 0
--WHERE b.thedate = '01/08/2013'  
WHERE a.thedate BETWEEN curdate() - 61 AND curdate()
ORDER BY b.thedate, b.technumber, b.employeenumber

-- want to correlate tool jobs with op codes
SELECT c.opcode, f.technumber, COUNT(*), MIN(thedate), MAX(thedate), MIN(b.ro), MAX(b.ro)
FROM day a
INNER JOIN factro b ON a.datekey = b.opendatekey
INNER JOIN factroline c ON b.storecode = c.storecode
  AND b.ro = c.ro
--  AND c.servicetype = 're'
INNER JOIN facttechlineflagdatehours d ON c.ro = d.ro
  AND c.line = d.line  
INNER JOIN bridgetechgroup e ON d.techgroupkey = e.techgroupkey
INNER JOIN dimtech f ON e.techkey = f.techkey
  AND f.flagdeptcode = 're'  
WHERE a.thedate BETWEEN curdate() - 180 AND curdate()
GROUP BY f.technumber, c.opcode
ORDER BY opcode, technumber
-- ) x GROUP BY ro,line HAVING COUNT(*) > 1
