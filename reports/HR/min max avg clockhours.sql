Store code │Emp# │First │Last │ Hire date │ Min Hrs. │ Max Hrs. │ Avg. Hrs. │ Weeks ≥ 30 Hrs.
I’d like to see these data for the last 3 months.  For all employees, both stores.
Thanks, Jon. Have a great day!

SELECT storecode, employeenumber, firstname, lastname, fullparttime, hiredate
FROM edwEmployeeDim a
WHERE currentrow = true 
  AND active = 'active' 

-- dates
    
SELECT sundaytosaturdayweek, MIN(thedate) AS weekFromDate, 
  MAX(thedate) AS weekThruDate
FROM day a
WHERE sundaytosaturdayweek BETWEEN (
  SELECT sundaytosaturdayweek
  FROM day
  WHERE thedate = curdate()) - 17
  AND (
    SELECT sundaytosaturdayweek
    FROM day
    WHERE thedate = curdate()) - 1
GROUP BY sundaytosaturdayweek    
-- number of weeks : 17 
SELECT COUNT(DISTINCT sundaytosaturdayweek)
FROM ( 
  SELECT sundaytosaturdayweek, MIN(thedate) AS weekFromDate, 
    MAX(thedate) AS weekThruDate
  FROM day a
  WHERE sundaytosaturdayweek BETWEEN (
    SELECT sundaytosaturdayweek
    FROM day
    WHERE thedate = curdate()) - 17
    AND (
      SELECT sundaytosaturdayweek
      FROM day
      WHERE thedate = curdate()) - 1
  GROUP BY sundaytosaturdayweek) x

-- date range  6/22/2014 -> 10/18/2014
SELECT MIN(weekfromdate), MAX(weekthrudate)
FROM ( 
  SELECT sundaytosaturdayweek, MIN(thedate) AS weekFromDate, 
    MAX(thedate) AS weekThruDate
  FROM day a
  WHERE sundaytosaturdayweek BETWEEN (
    SELECT sundaytosaturdayweek
    FROM day
    WHERE thedate = curdate()) - 17
    AND (
      SELECT sundaytosaturdayweek
      FROM day
      WHERE thedate = curdate()) - 1
  GROUP BY sundaytosaturdayweek) x  
 


DROP TABLE #name;  
SELECT storecode, employeenumber, firstname, lastname, payrollclass, 
  fullparttime, hiredate
INTO #name
FROM edwEmployeeDim a
WHERE currentrow = true 
  AND active = 'active'; 

DROP TABLE #empkey; 
SELECT employeekey, employeenumber
INTO #empkey
FROM edwEmployeeDim
WHERE employeenumber IN (
  SELECT employeenumber
  FROM #name) 
--
SELECT employeenumber, sundayToSaturdayWeek, SUM(clockHours) AS weekClockHours
INTO #empWkHours
FROM #empkey aa
LEFT JOIN (
  SELECT sundaytosaturdayweek, b.employeekey, MIN(thedate) AS weekFromDate, 
    MAX(thedate) AS weekThruDate, SUM(clockHours) AS clockHours
  FROM day a
  LEFT JOIN edwClockHoursFact b on a.datekey = b.datekey
  WHERE a.sundaytosaturdayweek BETWEEN (
    SELECT sundaytosaturdayweek
    FROM day
    WHERE thedate = curdate()) - 17
    AND (
      SELECT sundaytosaturdayweek
      FROM day
      WHERE thedate = curdate()) - 1
  GROUP BY a.sundaytosaturdayweek, b.employeekey) c on aa.employeekey = c.employeekey
WHERE clockhours IS NOT NULL 
  AND clockhours > 0 --
GROUP BY employeenumber, sundayToSaturdayWeek  

-- MIN, MAX, avg
SELECT employeenumber, MIN(weekClockHours)AS minWeek, MAX(weekClockHours) AS maxWeek,
  round(avg(weekClockHours), 2) AS avgWeek
FROM #empWkHours  
GROUP BY employeenumber
-- weeks over 30
SELECT employeenumber, COUNT(*) AS weeksOf30
FROM #empWkHours  
WHERE weekClockHours >= 30
GROUP BY employeenumber 

SELECT a.*, coalesce(minWeek, 0) AS minWeek, coalesce(maxWeek, 0) AS maxWeek,
  coalesce(avgWeek, 0) AS avgWeek, coalesce(weeksof30, 0) AS weeksOf30Plus
FROM #name a
LEFT JOIN (
  SELECT employeenumber, MIN(weekClockHours)AS minWeek, MAX(weekClockHours) AS maxWeek,
    round(avg(weekClockHours), 2) AS avgWeek
  FROM #empWkHours  
  GROUP BY employeenumber) b on a.employeenumber = b.employeenumber
LEFT JOIN (  
  SELECT employeenumber, COUNT(*) AS weeksOf30
  FROM #empWkHours  
  WHERE weekClockHours >= 30
  GROUP BY employeenumber) c on a.employeenumber = c.employeenumber
ORDER BY lastname  
      
