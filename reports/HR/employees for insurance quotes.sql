-- DROP TABLE zEmployees;
-- this data comes FROM arkona 
-- AQT HR - Employees for insurance quotes
DELETE FROM zEmployees;
CREATE TABLE zEmployees (
  employeeNumber cichar(7),
  name cichar(25),
  job cichar(35),
  dl cichar(25),
  dlstate cichar(2),
  fp cichar(4)) IN database;
  
SELECT b.lastname, b.firstname, a.employeenumber, a.job, a.dl, a.dlstate,
  b.birthdate, a.fp
FROM zEmployees a
LEFT JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  
  
SELECT employeenumber FROM (
SELECT a.*, b.firstname, b.lastname, b.birthdate
FROM zEmployees a
LEFT JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
) x GROUP BY employeenumber HAVING COUNT(*) > 1    