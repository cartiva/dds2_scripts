SELECT a.employeenumber AS [Emp #], a.lastname AS [Last Name], 
  a.firstname AS [First Name], d.ssn, a.fullPartTime AS Status, 
  b.ymstre AS Address, b.ymcity AS City, b.ymstat AS State, b.ymzip AS Zip, 
  a.birthdate AS [Birth Date], a.payrollclass AS [Comp Type],
  b.ymhdte AS [Hire Date], b.ymsex AS Gender, c.email, c.title
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYMAST b on a.storecode = b.ymco#
  AND a.employeenumber = b.ymempn
LEFT JOIN scotest.pto_compli_users c on a.employeenumber = c.userid  
LEFT JOIN tmpssn d on a.employeenumber = d.employeenumber
WHERE a.currentrow = true
  AND a.active = 'active'
ORDER BY a.lastname, a.firstname  