SELECT employeenumber FROM (
SELECT a.storeCode, employeenumber, lastname, firstname, 
  d.ymhdto AS origHireDate, d.ymhdte AS lastHireDate, 
  fullPartTime AS status, 
  payrollClass, iif(salary = 0, hourlyRate, salary) AS pay,
  yrtext AS job, 
  pyDept AS deparment, distCode, distribution as distCodeDescription
--SELECT COUNT(*)  
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
  AND b.yrjobd <> ''
  AND a.storecode = b.yrco#
LEFT JOIN stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd
  AND c.yrtext NOT IN (/*'VEICHLE SALES',*/ 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', /*'BUILDING MAINTENANCE',*/ '')
  AND b.yrco# = c.yrco#
LEFT JOIN stgArkonaPYMAST d on a.employeeNumber = d.ymempn  
WHERE currentrow = true 
  and active = 'active' ORDER BY job
--  AND active = 'Active' AND employeenumber IN ('263095','264110')
) x GROUP BY employeenumber HAVING COUNT(*) > 1

-- dups generated BY job DESC: '263095','264110'


SELECT a.storeCode, a.employeenumber, a.lastname, a.firstname, 
  a.fullPartTime AS status, 
  a.distribution as [Distribution],
  a.pyDept AS [Arkona Dept],
  left(c.yrtext, 35) AS [Arkona Job], 
  f.locationid AS [Compli Store], g.department AS [Compli Group], 
  f.supervisorname as [Compli Supervisor], f.title AS [Compli Job],
  e.department AS [PTO Dept], e.position AS [PTO Job],
  TRIM(k.firstname) + ' ' + k.lastname AS [PTO Supervisor]
--SELECT COUNT(*)  
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
  AND b.yrjobd <> ''
  AND a.storecode = b.yrco#
LEFT JOIN stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd
  AND c.yrtext <> '' -- gets rid of the dups
  AND b.yrco# = c.yrco#
LEFT JOIN scotest.ptoEmployees d on a.employeenumber = d.employeenumber
LEFT JOIN scotest.ptoPositionFulfillment e on d.employeenumber = e.employeenumber
LEFT JOIN scotest.pto_compli_users f on a.employeenumber = f.userid
LEFT JOIN scotest.pto_compli_groups g on f.firstname = g.firstname AND f.lastname = g.lastname
LEFT JOIN scotest.ptoAuthorization h on e.department = h.authForDepartment AND e.position = h.authForPosition
LEFT JOIN scotest.ptoPositionFulFillment i on h.authByDepartment = i.department AND h.authByPosition = i.position
  AND i.position <> 'Board of Directors'
LEFT JOIN scotest.ptoEmployees j on i.employeenumber = j.employeenumber
LEFT JOIN edwEmployeeDim k on j.employeenumber = k.employeenumber
  AND k.currentrow = true
WHERE a.currentrow = true 
  and a.active = 'active'  
ORDER BY a.lastname, a.firstname