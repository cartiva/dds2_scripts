/*
I need an employee list  for  the federated ins audit on Thursday.  
If I could get it today that would be great so I can add any lic # we are missing.        

What I need on the report is name , hire date, term date, job title, 
part-time/full time, drivers lic#, and state  .  
the audit period is 9/1/13 to 09/01/14

10/6/15, need this again, the time range IN this query IS wrong, AND 
IS returning folks it should NOT, eg Zoellick, hired 4/1/04, still employed
need to redefine the data parameters

SELECT storecode, name, hiredate, termdate, fullparttime 
FROM edwEmployeeDim
WHERE hiredate <= '09/01/2014'
  AND termdate > '08/31/2013'
  AND currentrow = true

--SELECT ymempn FROM (  
select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  a.ymhdto AS [Hire Date], 
  CASE
    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
    ELSE a.ymtdte
  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN ''
  END AS [Full/Part Time],
  a.ymdriv AS [DL #], a.ymdvst AS [DL State] 
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
WHERE ymhdto <= '09/01/2014'
  AND ymtdte > '08/31/2013'
--) x GROUP BY ymempn HAVING COUNT(*) > 1
ORDER BY name
*/
/*
ymhdte: hire date
ymhdto: orig hire date
ymtdte: term date
*/
DECLARE @from_date date;
DECLARE @thru_date date;
@from_date = '09/01/2014';
@thru_date = '08/31/2015';

select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  a.ymhdte AS [Hire Date],
  a.ymhdto AS [Orig Hire Date], 
--  a.ymtdte AS [Term Date],
  CASE
    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
    ELSE a.ymtdte
  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN d.fullparttime
  END AS [Full/Part Time]
--  a.ymdriv AS [DL #], a.ymdvst AS [DL State] 
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
  AND d.currentrow = true  
WHERE ymhdte BETWEEN @from_date AND @thru_date
  OR ymhdto BETWEEN @from_date AND @thru_date
  OR ymhdte BETWEEN @from_date AND @thru_date  
ORDER BY name;

-- AND current employees
select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  a.ymhdte AS [Hire Date],
--  a.ymhdto AS [Orig Hire Date], 
--  a.ymtdte AS [Term Date],
--  CASE
--    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
--    ELSE a.ymtdte
--  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN d.fullparttime
  END AS [Full/Part Time],
  a.ymdriv AS [DL #], a.ymdvst AS [DL State]
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
  AND d.currentrow = true  
WHERE a.ymactv <> 'T'
ORDER BY name;


I also need a list of all employees who worked during the audit period�probably like the one you did last year.

DECLARE @from_date date;
DECLARE @thru_date date;
@from_date = '09/01/2015';
@thru_date = '08/31/2016';

select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  a.ymhdte AS [Hire Date],
  a.ymhdto AS [Orig Hire Date], 
--  a.ymtdte AS [Term Date],
  CASE
    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
    ELSE a.ymtdte
  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN d.fullparttime
  END AS [Full/Part Time]
--  a.ymdriv AS [DL #], a.ymdvst AS [DL State] 
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
  AND d.currentrow = true  
WHERE ymhdto <= @thru_date
  AND ymtdte >= @from_date
ORDER BY name;
