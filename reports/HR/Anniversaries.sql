SELECT storecode, employeenumber, name, FirstName, LastName, hiredate,
  cast(timestampadd(sql_tsi_year, 5, hiredate) AS sql_date) AS "5 YR",
  cast(timestampadd(sql_tsi_year, 10, hiredate) AS sql_date) AS "10 YR",
  cast(timestampadd(sql_tsi_year, 15, hiredate) AS sql_date) AS "15 YR",
  cast(timestampadd(sql_tsi_year, 20, hiredate) AS sql_date) AS "20 YR",
  cast(timestampadd(sql_tsi_year, 25, hiredate) AS sql_date) AS "25 YR",
  cast(timestampadd(sql_tsi_year, 30, hiredate) AS sql_date) AS "30 YR",
  cast(timestampadd(sql_tsi_year, 35, hiredate) AS sql_date) AS "35 YR",
  cast(timestampadd(sql_tsi_year, 40, hiredate) AS sql_date) AS "40 YR",
  cast(timestampadd(sql_tsi_year, 45, hiredate) AS sql_date) AS "45 YR",
  cast(timestampadd(sql_tsi_year, 50, hiredate) AS sql_date) AS "50 YR"
-- SELECT COUNT(*)
FROM edwEmployeeDim
WHERE activecode = 'A'
  AND currentrow = true