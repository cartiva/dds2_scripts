SELECT *
FROM edwEmployeeDim a
WHERE year(termdate) BETWEEN 2012 AND 2014
  AND EXISTS (
    SELECT 1
    FROM edwEmployeeDim
    WHERE firstname = a.firstname
      AND lastname = a.lastname
      AND storecode <> a.storecode
      AND hiredate BETWEEN a.termdate - 10 AND a.termdate + 10)
  AND a.storecode <> 'ry3'    
    
-- need to isolate the notion of a xfr employee
-- for example, kim bestul xfrd FROM ry1 to ry2, but THEN subsequently termed    


SELECT b.employeekey, b.storecode, b.termdate, b.employeenumber, b.firstname, b.lastname, c.*
FROM (
  SELECT *
  FROM edwEmployeeDim a
  WHERE year(termdate) BETWEEN 2012 AND 2014
    AND EXISTS (
      SELECT 1
      FROM edwEmployeeDim
      WHERE firstname = a.firstname
        AND storecode <> 'ry3'
        AND lastname = a.lastname
        AND storecode <> a.storecode
        AND hiredate BETWEEN a.termdate - 10 AND a.termdate + 10)
    AND a.storecode <> 'ry3') b
LEFT JOIN (
  SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
  FROM edwEmployeeDim
  WHERE storecode <> 'ry3') c on b.storecode <> c.storecode
    AND b.firstname = c.firstname AND b.lastname = c.lastname
    AND c.hiredate BETWEEN b.termdate - 10 AND b.termdate + 10
ORDER BY b.lastname, b.firstname, b.employeekey    
     
     
-- 4/20
-- getting nowhere, need to identify those terms that constitute a xfr AND eliminate those
--   FROM the term report     

-- never been termed
SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
FROM edwEmployeeDim a
WHERE NOT EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE employeenumber = a.employeenumber
    AND termdate < curdate())
ORDER BY lastname, firstname    
     
-- have been termed
SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
FROM edwEmployeeDim a
WHERE termdate < curdate()
ORDER BY lastname, firstname     

-- this looks promising, these terms are transfers
-- any of these b.* rows with a termdate are valid terms ?
SELECT *
FROM (-- have been termed
  SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
  FROM edwEmployeeDim a
  WHERE termdate BETWEEN '01/01/2012' AND  curdate()) a     
LEFT JOIN (
  SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
  FROM edwEmployeeDim) b on a.employeekey <> b.employeekey
    AND a.firstname = b.firstname
    AND a.lastname = b.lastname
    AND a.termdate < b.termdate
    AND a.storecode <> b.storecode
    AND b.hiredate BETWEEN a.termdate - 2 AND a.termdate + 2 -- lindsey was rehired at ry1, but a year later, NOT a transfer
WHERE b.employeekey IS NOT NULL     
ORDER BY a.lastname, a.firstname, a.employeekey    
    
    
so ALL terms - xfr terms + xfr terms subsequently termed   

SELECT * 
FROM (-- ALL terms
  SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
  FROM edwEmployeeDim a
  WHERE termdate BETWEEN '01/01/2012' AND curdate()) a   
LEFT JOIN ( -- transfers
  SELECT b.*
  FROM (-- have been termed
    SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
    FROM edwEmployeeDim a
    WHERE termdate BETWEEN '01/01/2012' AND  curdate()) a     
  LEFT JOIN (
    SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
    FROM edwEmployeeDim) b on a.employeekey <> b.employeekey
      AND a.firstname = b.firstname
      AND a.lastname = b.lastname
      AND a.termdate < b.termdate
      AND a.storecode <> b.storecode
      AND b.hiredate BETWEEN a.termdate - 2 AND a.termdate + 2 -- lindsey was rehired at ry1, but a year later, NOT a transfer
  WHERE b.employeekey IS NOT NULL) b on a.employeekey = b.employeekey  
ORDER BY a.lastname  
  

--< 4/23 for ben brian mtg tomorrow, just the terms for 2014 ------------------<
SELECT *
FROM edwEmployeeDim a
WHERE termdate BETWEEN '01/01/2014' AND curdate()
  AND currentrow = true
  AND lastname <> 'wiebusch'
  AND termdate = (
    SELECT MAX(termdate)
    FROM edwEmployeeDim
    WHERE firstname = a.firstname
      AND lastname = a.lastname)
ORDER BY lastname, firstname  
  
SELECT *
FROM edwEmployeeDim
WHERE employeenumber IN (  
SELECT employeenumber
FROM edwEmployeeDim
WHERE termdate BETWEEN '01/01/2014' AND curdate()
  AND currentrow = true) 
ORDER BY lastname, firstname  


SELECT distinct a.storecode AS store, a.name, a.firstname, a.lastname, a.termdate, 
  a.pyDept AS dept, c.yrtext AS job
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
LEFT JOIN stgArkonaPYPRJOBD c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
WHERE termdate BETWEEN '01/01/2014' AND curdate()
  AND currentrow = true
  AND lastname <> 'wiebusch'
  AND termdate = (
    SELECT MAX(termdate)
    FROM edwEmployeeDim
    WHERE firstname = a.firstname
      AND lastname = a.lastname)
ORDER BY storecode, lastname, firstname  

--/> 4/23 for ben brian mtg tomorrow, just the terms for 2014 -----------------/>

 what a mind fuck this is turning out to be
 
-- 5/6/14
what a mind fuck this IS turning out to be
 the only way to correlate a person involved IN a transfer IS BY the name
 employeenumber changes
 employeenumber does NOT identify a person, but identifies an employment
 
 
SELECT *
FROM (
  SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
  FROM edwEmployeeDim 
  WHERE termdate BETWEEN '01/01/2014' AND curdate()
    AND currentrow = true) a 
LEFT JOIN (
  SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
  FROM edwEmployeeDim 
    WHERE currentrow = true) b on a.lastname = b.lastname
      AND a.firstname = b.firstname
      AND a.employeenumber <> b.employeenumber   
      AND b.hiredate BETWEEN a.termdate - 7 AND a.termdate + 7 
ORDER BY a.lastname

so bragunier, termed 1/13/14 FROM ry1 that IS a transfer, so should NOT be included
termed 3/28/14 FROM ry2,that IS a terms

-- could it be just this simple?
-- this may be good enough, some anomalies: brian collins, arne holmen, meagan draper ...
SELECT *
-- SELECT COUNT(*)
FROM (
  SELECT employeekey, storecode, hiredate, termdate, employeenumber, firstname, lastname
  FROM edwEmployeeDim 
  WHERE termdate BETWEEN '01/01/2012' AND curdate()
    AND currentrow = true) a 
WHERE NOT EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE currentrow = true
    AND lastname = a.lastname
    AND firstname = a.firstname
    AND hiredate BETWEEN a.termdate - 7  AND a.termdate + 7)    
ORDER BY a.lastname    


-- fuck it
SELECT distinct a.storecode, a.name, a.firstname, a.lastname, a.pydept AS department, 
  a.hiredate, a.termdate, c.yrtext as job 
-- SELECT COUNT(*)
FROM (
  SELECT storecode, name, pydept, hiredate, termdate, employeenumber, 
  firstname, lastname
  FROM edwEmployeeDim 
  WHERE termdate BETWEEN '01/01/2012' AND curdate()
    AND currentrow = true) a 
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
LEFT JOIN stgArkonaPYPRJOBD c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd    
WHERE NOT EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE currentrow = true
    AND lastname = a.lastname
    AND firstname = a.firstname
    AND hiredate BETWEEN a.termdate - 7  AND a.termdate + 7)    
AND NOT (lastname = 'vacura' AND yrtext = '')
AND NOT (lastname = 'hovey' AND yrtext = '')
ORDER BY lastname    

-- for 4/14 - 6/14

SELECT distinct a.storecode, a.name, a.firstname, a.lastname, a.pydept AS department, 
  a.hiredate, a.termdate, c.yrtext as job 
-- SELECT COUNT(*)
FROM (
  SELECT storecode, name, pydept, hiredate, termdate, employeenumber, 
  firstname, lastname
  FROM edwEmployeeDim 
  WHERE termdate BETWEEN '04/01/2014' AND curdate()
    AND currentrow = true) a 
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
LEFT JOIN stgArkonaPYPRJOBD c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd    
WHERE NOT EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE currentrow = true
    AND lastname = a.lastname
    AND firstname = a.firstname
    AND hiredate BETWEEN a.termdate - 7  AND a.termdate + 7)    
AND NOT (lastname = 'vacura' AND yrtext = '')
AND NOT (lastname = 'hovey' AND yrtext = '')
ORDER BY lastname    


-- for 7/10/14 ytd
-- 2014 ytd
SELECT distinct a.storecode, a.name, a.firstname, a.lastname, a.pydept AS department, 
  a.hiredate, a.termdate, c.yrtext as job 
-- SELECT COUNT(*)
FROM (
  SELECT storecode, name, pydept, hiredate, termdate, employeenumber, 
  firstname, lastname
  FROM edwEmployeeDim 
--  WHERE termdate BETWEEN '01/01/2014' AND curdate()
  WHERE year(termdate) = 2014
    AND currentrow = true) a 
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
LEFT JOIN stgArkonaPYPRJOBD c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd    
WHERE NOT EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE currentrow = true
    AND lastname = a.lastname
    AND firstname = a.firstname
    AND hiredate BETWEEN a.termdate - 7  AND a.termdate + 7)    
AND NOT (lastname = 'vacura' AND yrtext = '')
AND NOT (lastname = 'hovey' AND yrtext = '')
ORDER BY lastname    