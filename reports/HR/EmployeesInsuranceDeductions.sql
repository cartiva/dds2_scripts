/* 
9/18/12 maryanne
Hello,  I need a census report.  Could you please send me a report with the following information:

Employee Name
Address
Social Security Number
Birthdate
Male or Female (if we track this)
Part Time or Full Time
Insurance Premium Deductions
-- 9/25 needs hiredate, salary
-- added ymclass
*/
SELECT a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, a.CODE_TYPE, a.DED_PAY_CODE, a.FIXED_DED_AMT,
  b.ytdtyp, b.ytddcd, b.ytddsc
FROM stgArkonaPYDEDUCT a
LEFT JOIN stgArkonaPYPCODES b ON a.COMPANY_NUMBER = b.ytdco#
  AND a.DED_PAY_CODE = b.ytddcd
WHERE a.EMPLOYEE_NUMBER = '16425'

ry2 299 :: flex prem
ry2 293 :: 16 critica care
ry2 281 :: 14 usabl cancer

health dental & vision
-- DISTINCT health, dental, vision codes
SELECT ytddcd, MAX(ytddsc) AS descr
FROM stgARkonaPYPCODES
WHERE (
  ytddsc LIKE '%den%' 
  OR ytddsc LIKE '%med%'
  OR ytddsc LIKE '%vis%')
  AND ytddsc NOT LIKE 'AFLAC%'
GROUP BY ytddcd

-- store, emp#, h-d-v deduction
SELECT a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, sum(a.FIXED_DED_AMT) AS deduction, a.DED_PAY_CODE, MAX(descr)
FROM stgArkonaPYDEDUCT a
INNER JOIN (
  SELECT ytddcd, MAX(ytddsc) AS descr
  FROM stgARkonaPYPCODES
  WHERE (
    ytddsc LIKE '%den%' 
    OR ytddsc LIKE '%med%'
    OR ytddsc LIKE '%vis%')
    AND ytddsc NOT LIKE 'AFLAC%'
  GROUP BY ytddcd) b ON a.DED_PAY_CODE = b.ytddcd
GROUP BY a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, a.DED_PAY_CODE

-- this IS what i sent her
SELECT a.ymco# AS Store, a.ymname AS Name, a.ymstre AS Address, a.ymcity AS City, 
  a.ymstat AS State, a.ymzip AS Zip, '   ' AS SS#, a.ymbdte AS Birthday, a.ymsex AS Gender, 
  CASE a.ymactv
    WHEN 'A' THEN 'Full Time'
    ELSE 'Part Time'
  END AS "Full/Part", coalesce(sum(c.deduction), 0) AS "Med/Dental/Vision Deductions"
FROM stgArkonaPYMAST a
LEFT JOIN (
  SELECT a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, sum(a.FIXED_DED_AMT) AS deduction, a.DED_PAY_CODE, MAX(descr)
  FROM stgArkonaPYDEDUCT a
  INNER JOIN (
    SELECT ytddcd, MAX(ytddsc) AS descr
    FROM stgARkonaPYPCODES
    WHERE (
      ytddsc LIKE '%den%' 
      OR ytddsc LIKE '%med%'
      OR ytddsc LIKE '%vis%')
      AND ytddsc NOT LIKE 'AFLAC%'
    GROUP BY ytddcd) b ON a.DED_PAY_CODE = b.ytddcd
  GROUP BY a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, a.DED_PAY_CODE) c ON a.ymco# = c.COMPANY_NUMBER
    AND a.ymempn = c.EMPLOYEE_NUMBER
WHERE a.ymactv <> 'T'
GROUP BY a.ymco#, a.ymname, a.ymstre, a.ymcity, a.ymstat, a.ymzip, a.ymbdte, a.ymsex, 
  CASE a.ymactv
    WHEN 'A' THEN 'Full Time'
    ELSE 'Part Time'
  END
  
-- she needs it broken out BY types of coverage  
/*
-- 9/18
SELECT a.ymco# AS Store, a.ymname AS Name, a.ymstre AS Address, a.ymcity AS City, 
  a.ymstat AS State, a.ymzip AS Zip, '   ' AS SS#, a.ymbdte AS Birthday, 
  a.ymsex AS Gender, 
  CASE a.ymactv
    WHEN 'A' THEN 'Full Time'
    ELSE 'Part Time'
  END AS "Full/Part", 
  SUM(CASE WHEN c.DED_PAY_CODE = '107' THEN deduction ELSE 0 END) AS "6  MEDICAL PREM",
  SUM(CASE WHEN c.DED_PAY_CODE = '111' THEN deduction ELSE 0 END) AS "7  DENTAL PREM",
  SUM(CASE WHEN c.DED_PAY_CODE = '119' THEN deduction ELSE 0 END) AS "9  MED/DENT EXP",
  SUM(CASE WHEN c.DED_PAY_CODE = '295' THEN deduction ELSE 0 END) AS "VISION PREMIUM",
  coalesce(sum(c.deduction), 0) AS "Total Deductions"
FROM stgArkonaPYMAST a
LEFT JOIN (
  SELECT a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, sum(a.FIXED_DED_AMT) AS deduction, a.DED_PAY_CODE, MAX(descr)
  FROM stgArkonaPYDEDUCT a
  INNER JOIN (
    SELECT ytddcd, MAX(ytddsc) AS descr
    FROM stgARkonaPYPCODES
    WHERE (
      ytddsc LIKE '%den%' 
      OR ytddsc LIKE '%med%'
      OR ytddsc LIKE '%vis%')
      AND ytddsc NOT LIKE 'AFLAC%'
    GROUP BY ytddcd) b ON a.DED_PAY_CODE = b.ytddcd
  GROUP BY a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, a.DED_PAY_CODE) c ON a.ymco# = c.COMPANY_NUMBER
    AND a.ymempn = c.EMPLOYEE_NUMBER
WHERE a.ymactv <> 'T'
GROUP BY a.ymco#, a.ymname, a.ymstre, a.ymcity, a.ymstat, a.ymzip, 
  ymhdte, a.ymbdte, a.ymsex,
  CASE a.ymactv
    WHEN 'A' THEN 'Full Time'
    ELSE 'Part Time'
  END
ORDER BY store, name  
*/

-- 9/25
-- ADD hiredate, payroll class, pay period, rate/salary

SELECT a.ymco# AS Store, a.ymname AS Name, a.ymstre AS Address, a.ymcity AS City, 
  a.ymstat AS State, a.ymzip AS Zip, '   ' AS SS#, a.ymbdte AS Birthday, ymhdte as "Hire Date",
  a.ymsex AS Gender, 
  CASE a.ymactv
    WHEN 'A' THEN 'Full Time'
    ELSE 'Part Time'
  END AS "Full/Part", 
  SUM(CASE WHEN c.DED_PAY_CODE = '107' THEN deduction ELSE 0 END) AS "6  MEDICAL PREM",
  SUM(CASE WHEN c.DED_PAY_CODE = '111' THEN deduction ELSE 0 END) AS "7  DENTAL PREM",
  SUM(CASE WHEN c.DED_PAY_CODE = '119' THEN deduction ELSE 0 END) AS "9  MED/DENT EXP",
  SUM(CASE WHEN c.DED_PAY_CODE = '295' THEN deduction ELSE 0 END) AS "VISION PREMIUM",
  coalesce(sum(c.deduction), 0) AS "Total Deductions", 
  CASE ymclas
    WHEN 'C' THEN 'Commision'
    WHEN 'H' THEN 'Hourly'
    WHEN 'S' THEN 'Salary'
  END AS "Payroll Class",
  CASE ympper
    WHEN 'B' THEN 'Bi-weekly'
    WHEN 'S' THEN 'Semi-monthly'
  END AS PayPeriod,
  MAX(
    CASE ymclas
    WHEN 'H' THEN ymrate
    ELSE ymsaly
  END) AS "Salayr/Hourly Rate"
FROM stgArkonaPYMAST a
LEFT JOIN (
  SELECT a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, sum(a.FIXED_DED_AMT) AS deduction, a.DED_PAY_CODE, MAX(descr)
  FROM stgArkonaPYDEDUCT a
  INNER JOIN (
    SELECT ytddcd, MAX(ytddsc) AS descr
    FROM stgARkonaPYPCODES
    WHERE (
      ytddsc LIKE '%den%' 
      OR ytddsc LIKE '%med%'
      OR ytddsc LIKE '%vis%')
      AND ytddsc NOT LIKE 'AFLAC%'
    GROUP BY ytddcd) b ON a.DED_PAY_CODE = b.ytddcd
  GROUP BY a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, a.DED_PAY_CODE) c ON a.ymco# = c.COMPANY_NUMBER
    AND a.ymempn = c.EMPLOYEE_NUMBER
WHERE a.ymactv <> 'T'
GROUP BY a.ymco#, a.ymname, a.ymstre, a.ymcity, a.ymstat, a.ymzip, 
  ymhdte, a.ymbdte, a.ymsex, ymclas, ympper,
  CASE a.ymactv
    WHEN 'A' THEN 'Full Time'
    ELSE 'Part Time'
  END
ORDER BY store, name  

/*
-- 1 row per store/employee
SELECT ymco#, ymname
FROM (
SELECT a.ymco#, a.ymname, a.ymstre, a.ymcity, a.ymstat, a.ymzip, '   ' AS SS#, a.ymbdte, a.ymsex, 
  CASE a.ymactv
    WHEN 'A' THEN 'Full Time'
    ELSE 'Part Time'
  END AS "Full/Part", coalesce(sum(c.deduction), 0) AS "Med/Dental/Vision Deductions"
FROM stgArkonaPYMAST a
LEFT JOIN (
  SELECT a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, sum(a.FIXED_DED_AMT) AS deduction, a.DED_PAY_CODE, MAX(descr)
  FROM stgArkonaPYDEDUCT a
  INNER JOIN (
    SELECT ytddcd, MAX(ytddsc) AS descr
    FROM stgARkonaPYPCODES
    WHERE (
      ytddsc LIKE '%den%' 
      OR ytddsc LIKE '%med%'
      OR ytddsc LIKE '%vis%')
      AND ytddsc NOT LIKE 'AFLAC%'
    GROUP BY ytddcd) b ON a.DED_PAY_CODE = b.ytddcd
  GROUP BY a.COMPANY_NUMBER, a.EMPLOYEE_NUMBER, a.DED_PAY_CODE) c ON a.ymco# = c.COMPANY_NUMBER
    AND a.ymempn = c.EMPLOYEE_NUMBER
WHERE a.ymactv <> 'T'
GROUP BY a.ymco#, a.ymname, a.ymstre, a.ymcity, a.ymstat, a.ymzip, a.ymbdte, a.ymsex, 
  CASE a.ymactv
    WHEN 'A' THEN 'Full Time'
    ELSE 'Part Time'
  END) x
GROUP BY ymco#, ymname 
HAVING COUNT(*) > 1  
/*