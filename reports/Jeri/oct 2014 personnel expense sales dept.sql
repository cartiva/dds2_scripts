Will you please run a detail by employee for the following accounts for October? They are controlled by employee number.

12101
12102
12201
12202
12301
12302


SELECT c.lastname, c.firstname, b.*, d.gmdesc AS [Account Desc]
FROM (
  SELECT a.gtctl# AS control, a.gtacct AS account, SUM(a.gttamt) AS amount
  FROM stgArkonaGLPTRNS a
  WHERE a.gtdate BETWEEN '10/01/2014' AND '10/31/2014'
    AND a.gtacct IN ('12101','12102','12201','12202','12301','12302')
  GROUP BY a.gtctl#, a.gtacct) b
LEFT JOIN edwEmployeeDim c on b.control = c.employeenumber
  AND c.currentrow = true  
LEFT JOIN stgArkonaGLPMAST d on b.account = d.gmacct
  AND d.gmyear = 2014
ORDER by account, lastname