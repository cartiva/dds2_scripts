/*  BOPMAST */
FIELD   Field Def      VALUE      
BMSTAT  Record Status             In Process Deal (Unaccepted)
BMSTAT  Record Status   A         Accepted Deal
BMSTAT  Record Status   U         Capped Deal
BMTYPE  Record Type     C         Cash, Wholesale, Fleet, or Dealer Xfer
BMTYPE  Record Type     F         Financed Retail Deal
BMTYPE  Record Type     L         Financed Lease Deal
BMTYPE  Record Type     O         Cash Deal w/ Owner Financing
BMVTYP  Vehicle Type    N         New  
BMVTYP  Vehicle Type    U         Used  
BMWHSL  Sale Type       F         Fleet Deal
BMWHSL  Sale Type       L         Lease Deal
BMWHSL  Sale Type       R         Retail Deal
BMWHSL  Sale Type       W         Wholesale Deal
BMWHSL  Sale Type       X         Dealer Xfer

BMDTCAP      Date Capped
BMDTAPRV     Date Approved 
BMDTOR       Origination Date
BMDTSV       Date Saved
BMDELVDT     Delivery Date

-- NOT used
  bmpack

-- 9/20  
-- unit COUNT FROM BOPMAST
DROP TABLE #wtf;
SELECT bmco# AS storecode, bmkey AS DealKey, bmstat AS RecordStatus, bmtype AS RecordType, 
  bmvtyp AS NewUsed, bmwhsl AS SaleType, bmstk# AS StockNumber, bmvin AS Vin, 
  bmbuyr AS BuyerKey, bmcbyr AS CoBuyerKey, bmsnam AS BuyerName,
  bmdtor AS OriginationDate, bmdtaprv ApprovalDate, bmdtcap CapDate, 
  bmdtsv as SavedDate, bmdelvdt as DeliveryDate, bmsact AS SaleAccount,
  bmunwd AS UnwindFlag, bmdoc# AS Document, bmvcst AS Cost, bmhgrs AS TotalGross
INTO #wtf  
FROM stgArkonaBOPMAST
WHERE bmdoc# <> ''
  AND bmvin <> ''
  AND bmsact <> ''
  AND bmdoc# NOT IN (
    SELECT bmdoc#
    FROM stgArkonaBOPMAST
    WHERE bmdoc# <> ''
      AND bmvin <> ''
      AND bmsact <> ''
    GROUP BY bmdoc# 
    HAVING COUNT(*) > 1)   
/*
-- 1 row per document  
SELECT document
FROM #wtf
GROUP BY document
HAVING COUNT(*) > 1  
*/

/******************************************************************************/
unit COUNT IS fucking stupid
9/1 - 9/26
nc doc: 73 units
deal analysis: 83 units
wtf
cap date, approval date, delivery date, accepted, capped 
/******************************************************************************/
-- deal analysis appears to look at delivery date OR apprv OR who fucking knows?
SELECT COUNT(*)
FROM #wtf
WHERE recordstatus = 'u'
  AND deliverydate BETWEEN '09/01/2012' AND '09/15/2012'
  AND newused = 'n'
  AND storecode = 'ry1'

-- 2 high for august  
SELECT COUNT(*)
FROM #wtf
WHERE recordstatus = 'u'
  AND deliverydate BETWEEN '08/01/2012' AND '08/31/2012'
  AND newused = 'n'
  AND storecode = 'ry1'  
GROUP BY deliverydate

            

-- deal analysis: 9/1-9/15 ALL new capped: 43
SELECT 'DeliveryDate', 'Capped', COUNT(*) -- 42
FROM #wtf
WHERE deliverydate BETWEEN '09/01/2012' AND '09/15/2012'
  AND newused = 'n'
  AND storecode = 'ry1'
  AND recordstatus = 'u'

/**************** Gross ********************************************************/
from gl: sales - cogs, values available only for capped deals
ry1 cost: exclude acct 166400
  acct 164400 categorized AS ND sales, but IS actually an f/i sale
9/26: jeri says she will CREATE an f/i dept for recategorizing
  
-- GL NC Sales Accts 
SELECT *
FROM stgArkonaGLPMAST
WHERE gmstyp = 'a'
  AND gmyear = 2012
  AND gmtype = '4'
  AND gmdept = 'NC' 
  
SELECT a.gmacct, a.gmtype, a.gmdesc, a.gmdept, b.gmacct, b.gmtype, b.gmdesc, b.gmdept
FROM stgArkonaGLPMAST a  -- sales acct
LEFT JOIN stgArkonaGLPMAST b ON a.gmdboa = b.gmacct -- cost accts
--  AND b.gmstyp = 'c'
  AND b.gmyear = 2012
WHERE a.gmyear = 2012
  AND a.gmtype = '4'
  AND a.gmdept = 'NC'  
--  AND a.gmstyp = 'c' 

-- crookston cost accts that need to have the dept changed
SELECT a.gmacct, a.gmtype, a.gmdesc, a.gmdept, b.gmacct, b.gmtype, b.gmdesc, b.gmdept, COUNT(c.gtdoc#)
FROM stgArkonaGLPMAST a
LEFT JOIN stgArkonaGLPMAST b ON a.gmdboa = b.gmacct -- sales accts
  AND b.gmstyp = 'c'
  AND b.gmyear = 2012
LEFT JOIN stgArkonaGLPTRNS c ON a.gmacct = c.gtacct -- cost accts
  AND gtdate > '12/31/2011'  
WHERE a.gmstyp = 'c'
  AND a.gmyear = 2012
  AND a.gmtype = '4'
  AND a.gmdept = 'NC'     
GROUP BY a.gmacct, a.gmtype, a.gmdesc, a.gmdept, b.gmacct, b.gmtype, b.gmdesc, b.gmdept
HAVING COUNT(c.gtdoc#) > 0


-- base accts
SELECT a.gmacct AS SaleAcct, a.gmdesc, b.gmacct AS CostAcct, b.gmdesc
FROM stgArkonaGLPMAST a  -- sales acct
LEFT JOIN stgArkonaGLPMAST b ON a.gmdboa = b.gmacct -- cost accts
--  AND b.gmacct <> '164400' -- don't need the exception, based ON sale acct
  AND b.gmyear = 2012
WHERE a.gmyear = 2012
  AND a.gmtype = '4'
  AND a.gmdept = 'NC'  

-- this looks pretty good, 
-- though it includes shit LIKE aftermarket
-- but that should be filtered BY using actual deals, eh, maybe  
-- NOT really, take another look 16846 $51920 for sales
-- 9/27 added
SELECT a.gmacct, a.gmdesc, b.gmacct, b.gmdesc, 
  d.gtdoc#, sum(c.gttamt), sum(d.gttamt)
FROM stgArkonaGLPMAST a  -- sales acct
LEFT JOIN stgArkonaGLPMAST b ON a.gmdboa = b.gmacct -- cost accts
  AND b.gmyear = 2012
LEFT JOIN stgArkonaGLPTRNS c ON a.gmacct = c.gtacct
  AND c.gtdate > '08/31/2012'
LEFT JOIN stgArkonaGLPTRNS d ON b.gmacct = d.gtacct
  AND c.gtdoc# = d.gtdoc#
  AND d.gtdate > '08/31/2012'  
INNER JOIN #wtf e ON c.gtdoc# = e.stocknumber  
  AND a.gmacct = e.saleaccount
WHERE a.gmyear = 2012
  AND a.gmtype = '4'
  AND a.gmdept = 'NC'    
  AND c.gtco# IS NOT NULL
  AND d.gtco# IS NOT NULL  
  AND e.approvaldate > '08/31/2012' 
GROUP BY a.gmacct, a.gmdesc, b.gmacct, b.gmdesc, 
  d.gtdoc#
ORDER BY a.gmacct

-- look at individual accounts
-- ok, the issue IS that cost accounts can have multiple entries
-- may need to DO sales AND cost separately
SELECT a.gmacct, a.gmdesc, b.gmacct, b.gmdesc, 
  d.gtdoc#, c.gttamt, d.gttamt
FROM stgArkonaGLPMAST a  -- sales acct
LEFT JOIN stgArkonaGLPMAST b ON a.gmdboa = b.gmacct -- cost accts
  AND b.gmyear = 2012
LEFT JOIN stgArkonaGLPTRNS c ON a.gmacct = c.gtacct
  AND c.gtdate > '08/31/2012'
LEFT JOIN stgArkonaGLPTRNS d ON b.gmacct = d.gtacct
  AND c.gtdoc# = d.gtdoc#
  AND d.gtdate > '08/31/2012'  
INNER JOIN #wtf e ON c.gtdoc# = e.stocknumber
  AND a.gmacct = e.saleaccount
WHERE a.gmyear = 2012
  AND a.gmtype = '4'
  AND a.gmdept = 'NC'    
  AND c.gtco# IS NOT NULL
  AND d.gtco# IS NOT NULL  
  AND e.approvaldate > '08/31/2012' 
  AND e.stocknumber = '16846'

SELECT * FROM #wtf WHERE stocknumber = '17345'
-- 9/29
-- 2 separate queries
-- sales
SELECT a.gmacct, a.gmdesc, sum(c.gttamt), COUNT(*)
FROM stgArkonaGLPMAST a  -- sales acct
--LEFT JOIN stgArkonaGLPMAST b ON a.gmdboa = b.gmacct -- cost accts
--  AND b.gmyear = 2012
LEFT JOIN stgArkonaGLPTRNS c ON a.gmacct = c.gtacct
  AND c.gtdate BETWEEN '08/01/2012' and '08/31/2012'
--LEFT JOIN stgArkonaGLPTRNS d ON b.gmacct = d.gtacct
--  AND c.gtdoc# = d.gtdoc#
--  AND d.gtdate > '08/31/2012'  
INNER JOIN #wtf e ON c.gtdoc# = e.stocknumber  
  AND a.gmacct = e.saleaccount
WHERE a.gmyear = 2012
  AND a.gmtype = '4'
  AND a.gmdept = 'UC'    
  AND c.gtco# IS NOT NULL
  AND e.approvaldate BETWEEN '08/01/2012' and '08/31/2012'
GROUP BY a.gmacct, a.gmdesc

-- cost
SELECT b.gmacct, b.gmdesc, sum(d.gttamt)
FROM stgArkonaGLPMAST a  -- sales acct
LEFT JOIN stgArkonaGLPMAST b ON a.gmdboa = b.gmacct -- cost accts
  AND b.gmyear = 2012
--LEFT JOIN stgArkonaGLPTRNS c ON a.gmacct = c.gtacct
--  AND c.gtdate > '08/31/2012'
LEFT JOIN stgArkonaGLPTRNS d ON b.gmacct = d.gtacct
--  AND c.gtdoc# = d.gtdoc#
  AND d.gtdate > '08/31/2012'  
INNER JOIN #wtf e ON d.gtdoc# = e.stocknumber  
  AND a.gmacct = e.saleaccount
WHERE a.gmyear = 2012
  AND a.gmtype = '4'
  AND a.gmdept = 'NC'    
--  AND c.gtco# IS NOT NULL
  AND d.gtco# IS NOT NULL  
  AND e.approvaldate > '08/31/2012' 
GROUP BY b.gmacct, b.gmdesc
-- AND combine them
SELECT a. gmacct as "Sales Acct", a.gmdesc, b.gmacct AS "Cost Acct", "Sales Amt", "Cost Amt",
  Units, -("Sales Amt" + "Cost Amt") AS Gross 
--SELECT *
FROM ( -- sales
  SELECT a.gmacct, a.gmdesc, sum(c.gttamt) AS "Sales Amt", COUNT(*) AS units
  FROM stgArkonaGLPMAST a  -- sales acct
  LEFT JOIN stgArkonaGLPTRNS c ON a.gmacct = c.gtacct
    AND c.gtdate > '08/31/2012'
  INNER JOIN #wtf e ON c.gtdoc# = e.stocknumber  
    AND a.gmacct = e.saleaccount
  WHERE a.gmyear = 2012
    AND a.gmtype = '4'
    AND a.gmdept = 'NC'    
    AND c.gtco# IS NOT NULL
    AND e.approvaldate > '08/31/2012' 
  GROUP BY a.gmacct, a.gmdesc) a
LEFT JOIN ( -- cogs
  SELECT max(a.gmacct) as sales, b.gmacct, b.gmdesc, sum(d.gttamt) AS "Cost Amt"
  FROM stgArkonaGLPMAST a  -- sales acct
  LEFT JOIN stgArkonaGLPMAST b ON a.gmdboa = b.gmacct -- cost accts
    AND b.gmyear = 2012
  LEFT JOIN stgArkonaGLPTRNS d ON b.gmacct = d.gtacct
    AND d.gtdate > '08/31/2012'  
  INNER JOIN #wtf e ON d.gtdoc# = e.stocknumber  
    AND a.gmacct = e.saleaccount
  WHERE a.gmyear = 2012
    AND a.gmtype = '4'
    AND a.gmdept = 'NC'    
    AND d.gtco# IS NOT NULL  
    AND e.approvaldate > '08/31/2012' 
  GROUP BY b.gmacct, b.gmdesc) b ON a.gmacct = b.sales
  
-- looks pretty good, pick through individual discrepancies  
-- ry2 incentives gmtype = 7 
SELECT *
FROM #wtf
WHERE approvaldate > '08/31/2012'
  AND newused = 'N'
/**************** Gross END ***************************************************/   

/**************** inpmast/inventory *******************************************/  
-- WHERE they differ
16071 
looks LIKE current cost needs to be inventory + service
sometimes the service WORK goes to 11301D (16071) OR 123100(17249)
16874 imcost does NOT include 123100/AFM
SELECT a.*, b.*, expr-imcost
FROM (
  SELECT a.gmacct, b.gtdoc#, sum(b.gttamt)
--  INTO #jon
  FROM stgArkonaGLPMAST a
  LEFT JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  WHERE a.gmyear = 2012
    AND a.gmtype = '1'
    AND a.gmdept IN ('NC','UC')
    AND b.gtdoc# IS NOT NULL 
    AND b.gtdate > '01/01/2012'
  GROUP BY a.gmacct, b.gtdoc#) a
left JOIN (
  SELECT imstk#, imcost
  FROM stgArkonaINPMAST
  WHERE imstat = 'I') b ON a.gtdoc# = b.imstk#
WHERE b.imstk# IS NOT NULL  
  AND expr <> imcost 
  
/**************** inpmast/inventory END ***************************************/  
  
-- september malibus  
comparing to jeri doc
sales amounts look ok
cost DO NOT
-- emailed question to jeri, should 164400 be included IN cost for figuring gross?
-- othergross matches doc
-- 9/25 3 capped malibus, 1 accepted, AND ON the accepted deal no gl values (AS expected)
--   so ON those, for RACGross

SELECT a.*, d.glcost, d.glsales, d.glcost + d.glsales AS gross, 
  cost + glsales AS othergross, d.glinv
FROM #wtf a
LEFT JOIN (  
  SELECT b.gtdoc#, 
    SUM(CASE when c.gmtype = '5' THEN gttamt END) AS glCost,
    sum(CASE when c.gmtype = '4' THEN gttamt END) AS glSales,
    sum(CASE when c.gmtype = '1' THEN gttamt END) AS glINv
  FROM stgArkonaGLPTRNS b
  INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
    AND year(b.gtdate) = c.gmyear
    AND c.gmco# = 'ry1'
    AND c.gmtype in ('4', '5', '1')
    AND c.gmdept = 'NC'
  WHERE b.gtdtyp = 'b' -- deal
    AND b.gtdoc# in (SELECT document FROM #wtf WHERE saleaccount =  '1404001')
  GROUP BY b.gtdoc#) d ON a.document = d.gtdoc#
WHERE a.saleaccount = '1404001'
  AND a.ApprovalDate > '08/31/2012'  

  stk      DOC          Query                         
17367    24412.18    24447.18 
16846    26022.63    27985.63    

query includes 164400 5/NC  c/s new other plans             
SELECT gtdtyp, gtjrnl, gtdate, gtdoc#, gtdesc, gtacct, gttamt
FROM stgArkonaGLPTRNS
WHERE gtdoc# = '17367' 
ORDER BY gtjrnl, gtacct 

query includes 164400 5/NC  c/s new other plans  
SELECT gtdtyp, gtjrnl, gtdate, gtdoc#, gtdesc, gtacct, gttamt
FROM stgArkonaGLPTRNS
WHERE gtdoc# = '16846' 
ORDER BY gtjrnl, gtacct 
  
  
SELECT *
FROM stgarkonaglptrns
WHERE gtdate BETWEEN '01/01/2012' AND curdate()
  AND gtacct = '164400'
  
-- 9/21
-- waiting for answer FROM jeri ON what IS gross
-- need to look at accepted (NOT capped) deals ON the doc  
-- compare inventory to cost
-- 9/26 per jeri

  
-- summarize for sept malibu (3)
SELECT SUM(cost) AS bopmCost, SUM(TotalGross) AS bopmGross, 
  SUM(glcost) AS glcost, SUM(glsales) AS glsales, SUM(glcost + glsales) AS glGross,
  SUM(cost + glsales) AS "bopCost+glSales", SUM(glinv) AS Inv
FROM (  
  SELECT a.*, d.glcost, d.glsales, d.glcost + d.glsales AS gross, 
    cost + glsales AS othergross, d.glinv
  FROM #wtf a
  LEFT JOIN (  
    SELECT b.gtdoc#, 
      SUM(CASE when c.gmtype = '5' THEN gttamt END) AS glCost,
      sum(CASE when c.gmtype = '4' THEN gttamt END) AS glSales,
      sum(CASE when c.gmtype = '1' THEN gttamt END) AS glINv
    FROM stgArkonaGLPTRNS b
    INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
      AND year(b.gtdate) = c.gmyear
      AND c.gmco# = 'ry1'
      AND c.gmtype in ('4', '5', '1')
      AND c.gmdept = 'NC'
      AND c.gmacct <> '164400'
    WHERE b.gtdtyp = 'b' -- deal
      AND b.gtdoc# in (SELECT document FROM #wtf WHERE saleaccount =  '1404001')
    GROUP BY b.gtdoc#) d ON a.document = d.gtdoc#
  WHERE a.saleaccount = '1404001'
    AND a.ApprovalDate BETWEEN '09/01/2012' AND '09/21/2012') x 
GROUP BY document    

inventory: 73674
          Sales      Cost       Gross
doc       74166      73674        491                 
gl        74166      75672      -1506
bopmast              73674       4699

-- shit need to DO ALL stores
-- ALL make
SELECT storecode, SUM(cost) AS bopmCost, SUM(TotalGross) AS bopmGross, 
  SUM(glcost) AS glcost, SUM(glsales) AS glsales, SUM(glcost + glsales) AS glGross,
  SUM(cost + glsales) AS "bopCost+glSales", SUM(glinv) AS Inv
FROM (  
  SELECT a.*, d.glcost, d.glsales, d.glcost + d.glsales AS gross, 
    cost + glsales AS othergross, d.glinv
  FROM #wtf a
  LEFT JOIN (  
    SELECT b.gtdoc#, 
      SUM(CASE when c.gmtype = '5' THEN gttamt END) AS glCost,
      sum(CASE when c.gmtype = '4' THEN gttamt END) AS glSales,
      sum(CASE when c.gmtype = '1' THEN gttamt END) AS glINv
    FROM stgArkonaGLPTRNS b
    INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
      AND year(b.gtdate) = c.gmyear
--      AND c.gmco# = 'ry1'
      AND c.gmtype in ('4', '5', '1')
      AND c.gmdept = 'NC'
    WHERE b.gtdtyp = 'b' -- deal
    GROUP BY b.gtdoc#) d ON a.document = d.gtdoc#
  WHERE  a.ApprovalDate between '09/01/2012' AND curdatE()) x 
GROUP BY storecode  
   

-- 9/21 NC Delivery expense & Policy
NC Del Exp: 11301/a/b/...  gmtype = 8, gmdept = nc
SELECT *
FROM stgArkonaGLPMAST
WHERE gmtype = '8' 
  AND gmdept = 'nc'
  AND gmyear = 2012
  
ry1 gmtype = 8, gmdept = nc :: 11301/a  11501/1
IS there anything IN the transactions that will differentiate the 2 groups of accounts, nope
1301: delivery exp
1501: policy

-- fuck, these 2 accounts aren't controlled BY the stocknumber
-- eg 11301A control IS 119401 which IS a vendor number, the doc IS a po, so
-- deals NOT reqd, just glptrns
-- CLOSE enough8
SELECT left(gtacct, 5), SUM(gttamt)
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmco# = 'ry1'
  AND c.gmtype = '8'
  AND c.gmdept = 'NC'
WHERE gtdate BETWEEN '09/01/2012' AND curdate()  
  AND (b.gtacct LIKE '11301%' OR b.gtacct LIKE '11501%')
GROUP BY left(gtacct, 5)


-- Writedown
should be ALL journal WTD
DO NOT know WHERE OR IF this shows up ON the doc
SELECT COUNT(*), SUM(gttamt)
FROM stgArkonaGLPTRNS a
INNER JOIN stgArkonaGLPMAST b ON a.gtacct = b.gmacct
  AND year(a.gtdate) = b.gmyear
  AND b.gmco# = 'ry1'
  AND b.gmtype = '5'
  AND b.gmdept = 'nc'
WHERE gtjrnl = 'wtd'
  AND gtdate BETWEEN '09/01/2012' AND curdate()
  
-- 9/22
-- shit need to DO ALL stores
-- ALL makes
-- first cut isn't even CLOSE boo hoo
-- remove inventory
-- cost IS NULL ON many no it's NOT, it's separate rows for the accounts!
-- of course, IF i GROUP ON accounts,
-- what i want IS to GROUP ON chevy cars, chevy trucks, gmc cars etc
-- WHERE the fuck IS that information
-- what will GROUP sales & cost of sales accounts for a make/model onto a single row
-- those fucking arkona doc tables would be good
-- maybe an email to charlie
-- that would be good, configure domain terminology IN the dms (docs) rather than IN yapospos
SELECT storecode, gtacct, SUM(cost) AS bopmCost, SUM(TotalGross) AS bopmGross, 
  SUM(glcost) AS glcost, SUM(glsales) AS glsales, SUM(glcost + glsales) AS glGross,
  SUM(cost + glsales) AS "bopCost+glSales"
FROM (  
  SELECT a.*, d.gtacct, d.glcost, d.glsales, d.glcost + d.glsales AS gross, 
    cost + glsales AS othergross
  FROM #wtf a
  LEFT JOIN (  
    SELECT b.gtdoc#, b.gtacct,
      SUM(CASE when c.gmtype = '5' THEN gttamt END) AS glCost,
      sum(CASE when c.gmtype = '4' THEN gttamt END) AS glSales
    FROM stgArkonaGLPTRNS b
    INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
      AND year(b.gtdate) = c.gmyear
--      AND c.gmco# = 'ry1'
      AND c.gmtype in ('4', '5')
      AND c.gmdept = 'NC'
    WHERE b.gtdtyp = 'b' -- deal
    GROUP BY b.gtdoc#, b.gtacct) d ON a.document = d.gtdoc#
  WHERE  a.ApprovalDate between '09/01/2012' AND curdatE()
    AND d.gtdoc# IS NOT NULL) x 
GROUP BY storecode, gtacct  

cost account = gmdboa for a sales account
which changes the query
gmtype = '4'
SELECT storecode, gtacct, SUM(cost) AS bopmCost, SUM(TotalGross) AS bopmGross, 
  SUM(glcost) AS glcost, SUM(glsales) AS glsales, SUM(glcost + glsales) AS glGross,
  SUM(cost + glsales) AS "bopCost+glSales"
FROM (  
  SELECT a.*, d.gtacct, d.glcost, d.glsales, d.glcost + d.glsales AS gross, 
    cost + glsales AS othergross
  FROM #wtf a
  LEFT JOIN (  
    SELECT b.gtdoc#, b.gtacct,
      SUM(CASE when c.gmtype = '5' THEN gttamt END) AS glCost,
      sum(CASE when c.gmtype = '4' THEN gttamt END) AS glSales
    FROM stgArkonaGLPTRNS b
    INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
      AND year(b.gtdate) = c.gmyear
--      AND c.gmco# = 'ry1'
      AND c.gmtype = '4'
      AND c.gmdept = 'NC'
    WHERE b.gtdtyp = 'b' -- deal
    GROUP BY b.gtdoc#, b.gtacct) d ON a.document = d.gtdoc#
  WHERE  a.ApprovalDate between '09/01/2012' AND curdatE()
    AND d.gtdoc# IS NOT NULL) x 
GROUP BY storecode, gtacct


    SELECT b.gtdoc#, b.gtacct, c.gmdboa, b.gttamt
    FROM stgArkonaGLPTRNS b
    INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
      AND year(b.gtdate) = c.gmyear
--      AND c.gmco# = 'ry1'
      AND c.gmtype = '4'
      AND c.gmdept = 'NC'
    WHERE b.gtdtyp = 'b' -- deal
    GROUP BY b.gtdoc#, b.gtacct

what this has to be IS a grouping of accounts tied to domain language

-- 9/24
SELECT * 
FROM #wtf a
WHERE a.ApprovalDate between '09/01/2012' AND curdate()
  AND a.newused ='n'
  
WHERE IS the connection BETWEEN acct 1400001 AND nc-chevy-malibu sales

SELECT  gtdtyp, gtjrnl, COUNT(*)
FROM stgArkonaGLPTRNS
WHERE gtacct = '1404001'
  AND gtdate BETWEEN curdate() - 365 AND curdate()
GROUP BY gtdtyp, gtjrnl
  
SELECT *
FROM stgArkonaGLPTRNS
WHERE gtacct = '1404001'
  AND gtdate BETWEEN curdate() - 365 AND curdate()
ORDER BY gtdoc#

SELECT *
FROM edwaccountdim
WHERE accounttype IN ('4','5')
  AND gldepartmentcode = 'nc'
  
SELECT a.gmco#, min(a.gmacct), MAX(a.gmacct), min(a.gmdesc), MAX(a.gmdesc), b.gtjrnl, b.gtdtyp, 
  sum(case when a.gmtype = '4' then gttamt end) as glsales,
  sum(case when a.gmtype = '5' then gttamt END) AS glcost
FROM stgArkonaGLPMAST a
LEFT JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
INNER JOIN #wtf c ON b.gtdoc# = c.document
WHERE a.gmyear = 2012
  AND a.gmtype IN ('4','5')
  AND a.gmdept = 'nc'
  AND c.approvaldate > '08/31/2012'
GROUP BY a.gmco#, b.gtjrnl, b.gtdtyp  

SELECT 
  CASE a.gmstyp 
    WHEN 'A' THEN 'RY1'
    WHEN 'B' THEN 'RY2'
    WHEN 'C' THEN 'RY3'
    ELSE '666'
  END AS storecode, 
  a.gmacct, a.gmdesc, SUM(b.gttamt), gmtype
FROM stgArkonaGLPMAST a
LEFT JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
INNER JOIN #wtf c ON b.gtdoc# = c.document
WHERE a.gmyear = 2012
  AND a.gmtype IN ('4','5')
  AND a.gmdept = 'nc'
  AND c.approvaldate > '08/31/2012'
GROUP BY   CASE a.gmstyp 
    WHEN 'A' THEN 'RY1'
    WHEN 'B' THEN 'RY2'
    WHEN 'C' THEN 'RY3'
    ELSE '666'
  END,
  a.gmacct, a.gmdesc, gmtype  
  
-- fuck, great, so what links the sales/cost accounts  
SELECT storecode, SUM(amount), gmtype
FROM (
  SELECT 
    CASE a.gmstyp 
      WHEN 'A' THEN 'RY1'
      WHEN 'B' THEN 'RY2'
      WHEN 'C' THEN 'RY3'
      ELSE '666'
    END AS storecode, 
    a.gmacct, a.gmdesc, SUM(b.gttamt) AS Amount, gmtype
  FROM stgArkonaGLPMAST a
  LEFT JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  INNER JOIN #wtf c ON b.gtdoc# = c.document
  WHERE a.gmyear = 2012
    AND a.gmtype IN ('4','5', '1')
    AND a.gmdept = 'nc'
    AND c.approvaldate > '08/31/2012'
  GROUP BY   CASE a.gmstyp 
      WHEN 'A' THEN 'RY1'
      WHEN 'B' THEN 'RY2'
      WHEN 'C' THEN 'RY3'
      ELSE '666'
    END,
    a.gmacct, a.gmdesc, gmtype) x 
GROUP BY storecode, gmtype    

-- fuck, ALL of honda's nc cost accounts are dept General

select *
FROM (
  SELECT 
    CASE a.gmstyp 
      WHEN 'A' THEN 'RY1'
      WHEN 'B' THEN 'RY2'
      WHEN 'C' THEN 'RY3'
      ELSE '666'
    END AS storecode, 
    a.gmacct, a.gmdesc, SUM(b.gttamt) AS Amount, gmtype
  FROM stgArkonaGLPMAST a
  LEFT JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  INNER JOIN #wtf c ON b.gtdoc# = c.document
  WHERE a.gmyear = 2012
    AND a.gmtype IN ('4','5')
    AND a.gmdept = 'nc'
    AND c.approvaldate > '08/31/2012'
  GROUP BY   CASE a.gmstyp 
      WHEN 'A' THEN 'RY1'
      WHEN 'B' THEN 'RY2'
      WHEN 'C' THEN 'RY3'
      ELSE '666'
    END,
    a.gmacct, a.gmdesc, gmtype) x
WHERE storecode = 'RY2'    

/*
-- 4dr accord sales IS low IN query
-- no sales for h5049
-- the issue heere IS the current month glptrns update
SELECT a.*, d.glcost, d.glsales, d.glcost + d.glsales AS gross, 
  cost + glsales AS othergross, d.glinv
FROM #wtf a
LEFT JOIN (  
  SELECT b.gtdoc#, 
    SUM(CASE when c.gmtype = '5' THEN gttamt END) AS glCost,
    sum(CASE when c.gmtype = '4' THEN gttamt END) AS glSales,
    sum(CASE when c.gmtype = '1' THEN gttamt END) AS glINv
  FROM stgArkonaGLPTRNS b
  INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
    AND year(b.gtdate) = c.gmyear
    AND c.gmstyp = 'b'
    AND c.gmtype in ('4', '5', '1')
    AND c.gmdept = 'NC'
  WHERE b.gtdtyp = 'b' -- deal
    AND b.gtdoc# in (SELECT document FROM #wtf WHERE saleaccount =  '240500')
  GROUP BY b.gtdoc#) d ON a.document = d.gtdoc#
WHERE a.saleaccount = '240500'
  AND a.ApprovalDate > '08/31/2012'  

 SELECT gtdate, COUNT(*)
 FROM stgarkonaglptrns
 WHERE year(gtdate) = 2012
   AND month(gtdate) = 9
  GROUP BY gtdate
  
  SELECT month(gtdate), COUNT(*)
  FROM stgarkonaglptrns
  WHERE year(gtdate) = 2012
  GROUP BY month(gtdate)
  
 
-- 9/25 -- this just looks LIKE more of daily glptrns NOT being current
-- ry1 sales doc: 2,187,602
--           query: 2,123,555
SELECT storecode, SUM(amount), gmtype
FROM (
  SELECT 
    CASE a.gmstyp 
      WHEN 'A' THEN 'RY1'
      WHEN 'B' THEN 'RY2'
      WHEN 'C' THEN 'RY3'
      ELSE '666'
    END AS storecode, 
    a.gmacct, a.gmdesc, SUM(b.gttamt) AS Amount, gmtype
  FROM stgArkonaGLPMAST a
  LEFT JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  INNER JOIN #wtf c ON b.gtdoc# = c.document
  WHERE a.gmyear = 2012
    AND a.gmtype IN ('4','5')
    AND a.gmdept = 'nc'
    AND c.approvaldate > '08/31/2012'
  GROUP BY   CASE a.gmstyp 
      WHEN 'A' THEN 'RY1'
      WHEN 'B' THEN 'RY2'
      WHEN 'C' THEN 'RY3'
      ELSE '666'
    END,
    a.gmacct, a.gmdesc, gmtype) x 
GROUP BY storecode, gmtype      
              query     doc
             2123555   2187602
cruze          59915     83480
silverado/45  314384    352857
equinox      247484     159977

SELECT c.document,
  a.gmacct, a.gmdesc, b.gttamt AS Amount, gmtype
FROM stgArkonaGLPMAST a
LEFT JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
INNER JOIN #wtf c ON b.gtdoc# = c.document
  AND c.saleaccount = '1434001'd
WHERE a.gmyear = 2012
  AND a.gmtype ='4'
  AND a.gmdept = 'nc'
  AND c.approvaldate > '08/31/2012'

-- returns 9  
SELECT *
FROM #wtf
WHERE saleaccount = '1434001' 
  AND approvaldate > '08/31/2012' 
ORDER BY document  

SELECT *
FROM stgArkonaGLPTRNS
WHERE gtdoc# = '17838'
*/ 

SELECT *
FROM stgarkonaglptrns
WHERE gtacct = '164400'
AND gtdate > curdate() - 90

SELECT *
FROM #wtf
WHERE approvaldate > '08/31/2012'
  AND newused = 'n'