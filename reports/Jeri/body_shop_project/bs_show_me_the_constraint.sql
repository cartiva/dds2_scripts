﻿-- think i should also send out this list of op codes
select op_code, left(op_code_description, 25) as description, count(*)
from ads.bs_ro_details
group by op_code, op_code_description
order by count(*) desc


-- let us look at op code/tech class grouping
select a.op_code, left(a.op_code_description, 25), a.cor_code, left(a.cor_code_description, 25), b.classification, count(*)
from ads.bs_ro_details a
left join ads.bs_techs b on a.tech_number = b.tech_number
-- left join ads.bs_multiple_ros c on a.ro = c.ro
-- where c.ro is null
  and flag_hours <> 0
group by a.op_code, a.cor_code, left(a.op_code_description, 25), b.classification, left(a.cor_code_description, 25)
order by op_code, count(*) desc 

-- the current classifications
select classification, count(*)
from ads.bs_techs
group by classification
order by count(*) desc;
    
-- get rid of 0 flag_hour lines & group by ro/op_code/tech
select *
from (
  select ro, vin, string_agg(distinct 'op_code[' || op_code || ']' ||  'flag_hours[' ||flag_hours::citext || ']' || 'tech[' ||classification || ']', '|') as techs
  from (
    select a.ro, a.vin, a.op_code, sum(a.flag_hours) as flag_hours, b.classification
    from ads.bs_ro_details a
    left join ads.bs_techs b on a.tech_number = b.tech_number
    left join ads.bs_multiple_ros c on a.ro = c.ro
    where c.ro is null
      and flag_hours <> 0
    group by a.ro, a.vin, a.op_code, b.classification) x
  group by ro, vin) y
where techs is not null 

-- now the same thing for the multiples
select *
from (
  select ro || '*', vin, string_agg(distinct 'op_code[' || op_code || ']' ||  'flag_hours[' ||
      flag_hours::citext || ']' || 'tech[' ||classification || ']', '|') as techs
  from (
    select min(a.ro) as ro, a.vin, a.op_code, sum(a.flag_hours) as flag_hours, b.classification
    from ads.bs_ro_details a
    left join ads.bs_techs b on a.tech_number = b.tech_number
    inner join ads.bs_multiple_ros c on a.ro = c.ro
--     where flag_hours <> 0
    group by a.vin, a.op_code, b.classification) x
  group by ro, vin) y
where techs is not null

-- ok, interesting start for showing opcode correlation
need to add touchtime

-- need to group on ro and classification and derive a combined proficiency for the classification
select a.ro, a.vin, a.flag_hours, b.tech_number, b.classification, d.*
from ads.bs_ro_details a
left join ads.bs_techs b on a.tech_number = b.tech_number
left join ads.bs_multiple_ros c on a.ro = c.ro
left join ads.bs_tech_proficiency d on a.tech_number = d.tech_number
  and a.pay_period_start = d.pay_period_start
where c.ro is null

-- classification/proficiency per ro
select a.ro, b.classification, sum(d.flag_hours) as flag_hours,
  sum(d.clock_hours) as clock_hours,
  case sum(clock_hours)
    when 0 then 0
    else round(100 * sum(d.flag_hours)/sum(d.clock_hours), 2)
  end as class_proficiency
from ads.bs_ro_details a
left join ads.bs_techs b on a.tech_number = b.tech_number
left join ads.bs_multiple_ros c on a.ro = c.ro
left join ads.bs_tech_proficiency d on a.tech_number = d.tech_number
  and a.pay_period_start = d.pay_period_start
where c.ro is null  
group by a.ro, b.classification  

-- classification touch time  100 * flag hours/proficiency
-- without multiple ros
select ro, classification, 
  case pay_per_class_proficiency
    when 0 then 0
    else round(100 * class_flag_hours/pay_per_class_proficiency, 2) 
  end as class_touch_time
from (
  select a.ro, b.classification, sum(a.flag_hours) as class_flag_hours, sum(d.flag_hours) as pay_per_flag_hours,
    sum(d.clock_hours) as pay_per_clock_hours,
    case sum(clock_hours)
      when 0 then 0
      else round(100 * sum(d.flag_hours)/sum(d.clock_hours), 2)
    end as pay_per_class_proficiency
  from ads.bs_ro_details a
  left join ads.bs_techs b on a.tech_number = b.tech_number
  left join ads.bs_multiple_ros c on a.ro = c.ro
  left join ads.bs_tech_proficiency d on a.tech_number = d.tech_number
    and a.pay_period_start = d.pay_period_start
  where c.ro is null  
  group by a.ro, b.classification) e 
where e.classification is not null   

-- classification touch time  100 * flag hours/proficiency
-- without multiple ros
-- group by ro
select ro, string_agg(distinct 'tech_class[' || classification || ']' ||  'touch_time[' ||class_touch_time::citext || ']', '|') as techs
from (
  select ro, classification, 
    case pay_per_class_proficiency
      when 0 then 0
      else round(100 * class_flag_hours/pay_per_class_proficiency, 2) 
    end as class_touch_time
  from (
    select a.ro, b.classification, sum(a.flag_hours) as class_flag_hours, sum(d.flag_hours) as pay_per_flag_hours,
      sum(d.clock_hours) as pay_per_clock_hours,
      case sum(clock_hours)
        when 0 then 0
        else round(100 * sum(d.flag_hours)/sum(d.clock_hours), 2)
      end as pay_per_class_proficiency
    from ads.bs_ro_details a
    left join ads.bs_techs b on a.tech_number = b.tech_number
    left join ads.bs_multiple_ros c on a.ro = c.ro
    left join ads.bs_tech_proficiency d on a.tech_number = d.tech_number
      and a.pay_period_start = d.pay_period_start
    where c.ro is null  
    group by a.ro, b.classification) e 
  where e.classification is not null) f
group by ro  

-- classification touch time  100 * flag hours/proficiency
-- with multiple ros
select ro, vin, classification, 
  case pay_per_class_proficiency
    when 0 then 0
    else round(100 * class_flag_hours/pay_per_class_proficiency, 2) 
  end as class_touch_time
from (
  select min(a.ro) as ro, a.vin, b.classification, sum(a.flag_hours) as class_flag_hours, sum(d.flag_hours) as pay_per_flag_hours,
    sum(d.clock_hours) as pay_per_clock_hours,
    case sum(clock_hours)
      when 0 then 0
      else round(100 * sum(d.flag_hours)/sum(d.clock_hours), 2)
    end as pay_per_class_proficiency
  from ads.bs_ro_details a
  left join ads.bs_techs b on a.tech_number = b.tech_number
  inner join ads.bs_multiple_ros c on a.ro = c.ro
  left join ads.bs_tech_proficiency d on a.tech_number = d.tech_number
    and a.pay_period_start = d.pay_period_start 
  group by a.vin, b.classification) e 
where e.classification is not null   
order by ro

  select min(a.ro) as ro, a.vin, b.classification, sum(a.flag_hours) as class_flag_hours, sum(d.flag_hours) as pay_per_flag_hours,
    sum(d.clock_hours) as pay_per_clock_hours,
    case sum(clock_hours)
      when 0 then 0
      else round(100 * sum(d.flag_hours)/sum(d.clock_hours), 2)
    end as pay_per_class_proficiency
  from ads.bs_ro_details a
  left join ads.bs_techs b on a.tech_number = b.tech_number
  inner join ads.bs_multiple_ros c on a.ro = c.ro
  left join ads.bs_tech_proficiency d on a.tech_number = d.tech_number
    and a.pay_period_start = d.pay_period_start
  group by a.vin, b.classification

  
-- 11/3/ ----------------------------------------------------------------------------------------------
too many notes
slow down

drop table if exists ads.bs_class_proficiency; 
create table ads.bs_class_proficiency (
  pay_period_start date not null,
  pay_period_end date not null,
  classification citext not null,
  flag_hours numeric(6,2) not null,
  clock_hours numeric(6,2) not null,
  proficiency numeric(6,2) not null,
  constraint bs_class_proficiency_pk unique(pay_period_start,classification));
  
insert into ads.bs_class_proficiency
select a.pay_period_start, a.pay_period_end, b.classification, 
  sum(flag_hours) as flag_hours, sum(clock_hours) as clock_hours, 
  case sum(clock_hours)
    when 0 then 0
    else round( 100 * sum(flag_hours)/sum(clock_hours), 2) 
  end as proficiency
from ads.bs_tech_proficiency a
left join ads.bs_techs b on a.tech_number = b.tech_number
group by a.pay_period_start, a.pay_period_end, b.classification;


Touch Time: flag_hours/class_proficiency
tp/classification: total gross/class_touch_time

-- ok, this looks ok for touch time
select c.ro, 
  string_agg(distinct c.classification || '[' || coalesce(round(100 *c.flag_hours/d.proficiency, 2), 0)::citext ||']', '|') as touch_time
from (
  select a.pay_period_start, a.ro, b.classification, sum(flag_hours) as flag_hours
  from ads.bs_ro_details a
  inner join ads.bs_techs b on a.tech_number = b.tech_number
  -- without multiples
  left join ads.bs_multiple_ros c on a.ro = c.ro
  where c.ro is null
  group by a.pay_period_start, a.ro, b.classification) c
left join ads.bs_class_proficiency d on c.pay_period_start = d.pay_period_start
  and c.classification = d.classification 
  and d.proficiency <> 0
group by ro  

select min(c.ro) as ro, vin,
  string_agg(distinct c.classification || '[' || coalesce(round(100 *c.flag_hours/d.proficiency, 2), 0)::citext ||']', '|') as touch_time
from (
  select a.pay_period_start, a.vin, a.ro, b.classification, sum(flag_hours) as flag_hours
  from ads.bs_ro_details a
  inner join ads.bs_techs b on a.tech_number = b.tech_number
  -- multiples
  inner join ads.bs_multiple_ros c on a.ro = c.ro
  group by a.pay_period_start, a.ro, a.vin, b.classification) c
left join ads.bs_class_proficiency d on c.pay_period_start = d.pay_period_start
  and c.classification = d.classification 
  and d.proficiency <> 0
group by vin
order by min(ro)

/*
-- big discrepancy in touch time as illustrated with ro 18037016
-- the orig spreadsheet (tpcu_v3) used individual tech proficiencies
-- in this case swanson, prof = 7.94
-- as opposed to this using classification proficiency, metal:C prof = .562
select *
from (
  select a.pay_period_start, a.ro, b.classification, sum(flag_hours) as flag_hours
  from ads.bs_ro_details a
  inner join ads.bs_techs b on a.tech_number = b.tech_number
  where a.ro in ('18037016','18038782')
  group by a.pay_period_start, a.ro, b.classification)c
left join ads.bs_class_proficiency d on c.pay_period_start = d.pay_period_start
  and c.classification = d.classification   
*/ 

ok, generate the gross stuff from tpcu_v3 w/out limiting to metal touch time 

drop table if exists gross;
create temp table gross as
  select a.ro, a.open_date, a.close_date, a.flag_hours, round(a.ro_labor_sales, 0) as labor_sales, a.labor_cogs, a.labor_gross, 
    b.sales as parts_sales, b.cogs as parts_cogs, b.gross as parts_gross, 
    round(a.ro_shop_supplies, 0) as shop_supplies, round(ro_hazardous_materials, 0) as hazardous_materials, 
    round(ro_paint_materials, 0) as paint_materials,
    a.labor_gross + b.gross as total_gross, payment_type,
    round(a.ro_labor_sales + b.sales) as total_sales, ''::citext as multiple_ros,
    a.vin
  from ( -- ro level
    select ro, open_date, close_date, sum(flag_hours) as flag_hours, ro_labor_sales,
      round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
      round(ro_labor_sales - sum(flag_hours*tech_labor_cost), 0) as labor_gross,
      ro_shop_supplies, ro_hazardous_materials, ro_paint_materials, 
      string_agg(distinct payment_type, ',') as payment_type, a.vin   
    from ads.bs_ro_details a
    where not exists (
      select 1
      from ads.bs_multiple_ros
      where ro = a.ro)
    group by ro, open_date, close_date, ro_labor_sales, ro_shop_supplies, ro_hazardous_materials, ro_paint_materials, a.vin) a
  left join ( -- parts
    select ro, sum(sales) as sales, sum(cogs) as cogs, sum(gross) as gross
    from ads.bs_ro_parts
    group by ro) b on a.ro = b.ro  
union
  select a.ro, a.open_date, a.close_date, a.flag_hours, round(a.ro_labor_sales, 0) as labor_sales, a.labor_cogs, a.labor_gross, 
    b.sales as parts_sales, b.cogs as parts_cogs, b.gross as parts_gross, 
    round(a.ro_shop_supplies, 0) as shop_supplies, round(ro_hazardous_materials, 0) as hazardous_materials, 
    round(ro_paint_materials, 0) as paint_materials,
    a.labor_gross + b.gross as total_gross, payment_type,
    round(a.ro_labor_sales + b.sales) as total_sales,
    multiple_ros, a.vin
  from ( -- ro level
    select min(a.ro) || '*' as ro, 
      min(open_date) as open_date, max(close_date) as close_date, sum(flag_hours) as flag_hours, 
      sum(ro_labor_sales) as ro_labor_sales,
      round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
      round(sum(ro_labor_sales) - sum(flag_hours*tech_labor_cost), 0) as labor_gross,
      sum(ro_shop_supplies) as ro_shop_supplies, 
      sum(ro_hazardous_materials) as ro_hazardous_materials, 
      sum(ro_paint_materials) as ro_paint_materials, 
      string_agg(distinct payment_type, ',') as payment_type,
      string_agg(distinct a.ro, '|') as multiple_ros, a.vin   
    from ads.bs_ro_details a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    group by a.vin) a
  left join ( -- parts
    select aa.vin, sum(a.sales) as sales, sum(a.cogs) as cogs, sum(a.gross) as gross
    from ads.bs_ro_parts a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    group by aa.vin) b on a.vin = b.vin;  

select * from gross

drop table if exists tp_class;
create temp table tp_class as        
select g.*, h.tp 
from gross g
left join (
  select ro, 
    string_agg(distinct classification || '[' || class_touch_time::citext || '][' || tp::citext || ']', '|') as tp
  from (
      select c.ro, c.classification, c.flag_hours, d.proficiency, e.total_gross,
        round(100 * c.flag_hours/d.proficiency, 2) as class_touch_time,
        case c.flag_hours/d.proficiency
          when 0 then 0
          else round(total_gross/(100 * c.flag_hours/d.proficiency), 2)
        end as tp
      from (
        select a.pay_period_start, a.ro, b.classification, sum(flag_hours) as flag_hours
        from ads.bs_ro_details a
        inner join ads.bs_techs b on a.tech_number = b.tech_number
        -- without multiples
        left join ads.bs_multiple_ros c on a.ro = c.ro
        where c.ro is null
        group by a.pay_period_start, a.ro, b.classification) c
      left join ads.bs_class_proficiency d on c.pay_period_start = d.pay_period_start
        and c.classification = d.classification 
        and d.proficiency <> 0
      left join gross e on c.ro = e.ro      
      where d.proficiency is not null) f
  group by ro) h on g.ro = h.ro 
where g.ro not like '%*'     

union

select g.*, h.tp 
from gross g
left join (
  select vin, 
    string_agg(distinct classification || '[' || class_touch_time::citext || '][' || tp::citext || ']', '|') as tp
  from (  
    select c.vin, c.classification, c.flag_hours, d.proficiency,
      round(100 * c.flag_hours/d.proficiency, 2) as class_touch_time,
      case c.flag_hours/d.proficiency
        when 0 then 0
        else round(total_gross/round((100 * c.flag_hours/d.proficiency), 2), 2)
      end as tp,    
      e.total_gross
    from ( -- 1 row per ro/classification
      select a.vin, b.classification, 
        sum(flag_hours) as flag_hours,
        max(close_date) as close_date
      from ads.bs_ro_details a
      inner join ads.bs_techs b on a.tech_number = b.tech_number
      -- multiples
      inner join ads.bs_multiple_ros c on a.ro = c.ro
      group by a.vin, b.classification) c
    left join ads.bs_class_proficiency d on c.close_date between d.pay_period_start and d.pay_period_end
      and c.classification = d.classification 
      and d.proficiency <> 0 
    left join gross e on c.vin = e.vin
    -- VIN alone join is too general, it is including other visits of the same vin
    where e.ro like '%*') f
  group by vin) h on g.vin = h.vin
where g.ro like '%*'


select * from tp_class 










  select vin, 
    string_agg(distinct classification || '[' || class_touch_time::citext || '][' || tp::citext || ']', '|') as tp
  from (  
    select c.vin, c.classification, c.flag_hours, d.proficiency,
      round(100 * c.flag_hours/d.proficiency, 2) as class_touch_time,
      case c.flag_hours/d.proficiency
        when 0 then 0
        else round(total_gross/round((100 * c.flag_hours/d.proficiency), 2), 2)
      end as tp,    
      e.total_gross
    from ( -- 1 row per ro/classification
      select a.vin, b.classification,  ----------------remove grouping by ro
        sum(flag_hours) as flag_hours,
        max(close_date) as close_Date
      from ads.bs_ro_details a
      inner join ads.bs_techs b on a.tech_number = b.tech_number
      -- multiples
      inner join ads.bs_multiple_ros c on a.ro = c.ro
      where a.vin = 'KNMAT2MV3GP595778'
      group by a.vin, b.classification) c
    left join ads.bs_class_proficiency d on c.close_date between d.pay_period_start and d.pay_period_end
      and c.classification = d.classification 
      and d.proficiency <> 0 
    left join gross e on c.vin = e.vin) f
  where vin = 'KNMAT2MV3GP595778'
  group by vin

-- remove ro altogether
-- possibility of multiple proficiencies for same cat ????
-- which makes me think join on pay period start no good, has to be close date
    select c.vin, c.classification, c.flag_hours, d.proficiency,
      round(100 * c.flag_hours/d.proficiency, 2) as class_touch_time,
      case c.flag_hours/d.proficiency
        when 0 then 0
        else round(total_gross/(100 * c.flag_hours/d.proficiency), 2)
      end as tp,    
      e.total_gross
    from ( -- 1 row per ro/classification
      select a.vin, b.classification,  ----------------remove grouping by ro
        sum(flag_hours) as flag_hours,
        max(close_date) as close_Date
      from ads.bs_ro_details a
      inner join ads.bs_techs b on a.tech_number = b.tech_number
      -- multiples
      inner join ads.bs_multiple_ros c on a.ro = c.ro
      where a.vin = '1N6AD0EV5AC444601'
      group by a.vin, b.classification) c
    left join ads.bs_class_proficiency d on c.close_date between d.pay_period_start and d.pay_period_end
      and c.classification = d.classification 
      and d.proficiency <> 0 
    left join gross e on c.vin = e.vin

select * from ads.bs_ro_details where ro = '18044970'

select * from gross where vin = '1GKFK16Z72J105264'

VIN join is too general, it is including other visits of the same vin
limit the mult

select * from ads.bs_multiple_ros where vin = '1GKFK16Z72J105264'




    select min(ro) || '*' as ro, 
      min(open_date) as open_date, max(close_date) as close_date, sum(flag_hours) as flag_hours, 
      sum(ro_labor_sales) as ro_labor_sales,
      round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
      round(sum(ro_labor_sales) - sum(flag_hours*tech_labor_cost), 0) as labor_gross,
      sum(ro_shop_supplies) as ro_shop_supplies, 
      sum(ro_hazardous_materials) as ro_hazardous_materials, 
      sum(ro_paint_materials) as ro_paint_materials, 
      string_agg(distinct payment_type, ',') as payment_type,
      string_agg(distinct ro, '|') as multiple_ros, vin 
    from ads.bs_ro_details a
    where exists (
      select 1
      from ads.bs_multiple_ros
      where ro = a.ro)
    group by vin