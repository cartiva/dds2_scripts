SELECT b.vin, aa.thedate AS saleDate, 
  CASE a.storecode
    WHEN 'RY1' THEN 'Rydell Auto Center'
    WHEN 'RY2' THEN 'Honda Nissan of Grand Forks'
  END AS sellingDealerName,
  CASE a.storecode
    WHEN 'RY1' THEN '2700 South Washington St Grand Forks, ND 58201'
    WHEN 'RY2' THEN '3220 South Washington St Grand Forks, ND 58201'
  END AS sellingDealerAddress,
  c.saleType,  e.bnstcd AS purchaserState, e.bnzip AS purchaserZip
FROM factVehicleSale a
INNER JOIN day aa on a.cappedDateKey = aa.datekey
INNER JOIN dimVehicle b on a.vehicleKey = b.vehicleKEy
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
INNER JOIN dimCustomer d on a.buyerKey = d.customerKey
INNER JOIN stgArkonaBOPNAME e on d.bnkey = e.bnkey
WHERE aa.thedate BETWEEN '04/01/2014' AND '06/30/2014'