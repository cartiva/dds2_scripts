SELECT b.thedate, c.bnemail, a.*
SELECT yearmonth
FROM factRO a
LEFT JOIN day b ON a.opendatekey = b.datekey
LEFT JOIN stgArkonaBOPNAME c ON a.customerkey = c.bnkey
WHERE a.void = false
  AND LEFT(ro,2) = '18'
  AND b.thedate BETWEEN '01/01/2013' AND '04/30/2013'
  AND NOT EXISTS (
    SELECT 1
    FROM factroline
    WHERE ro = a.ro 
      AND paytype = 'i')
  AND a.customerkey <> 0
  
SELECT b.yearmonth,
  SUM(CASE WHEN bnemail = '' THEN 0 ELSE 1 END) AS hasEmail, 
  COUNT(*) AS rosOpened
FROM factRO a
LEFT JOIN day b ON a.opendatekey = b.datekey
LEFT JOIN stgArkonaBOPNAME c ON a.customerkey = c.bnkey
WHERE a.void = false
  AND LEFT(ro,2) = '18'
  AND b.thedate BETWEEN '01/01/2013' AND '04/30/2013'
  AND NOT EXISTS (
    SELECT 1
    FROM factroline
    WHERE ro = a.ro 
      AND paytype = 'i')
  AND a.customerkey <> 0
GROUP BY b.yearmonth   

SELECT monthname, rosopened, hasemail, round((hasemail*1.0/rosopened)*100, 1) AS [Percent With Email] 
FROM (
  SELECT b.monthname, monthofyear,
    SUM(CASE WHEN bnemail = '' THEN 0 ELSE 1 END) AS hasEmail, 
    COUNT(*) AS rosOpened
  FROM factRO a
  LEFT JOIN day b ON a.opendatekey = b.datekey
  LEFT JOIN stgArkonaBOPNAME c ON a.customerkey = c.bnkey
  WHERE a.void = false
    AND LEFT(ro,2) = '18'
    AND b.thedate BETWEEN '01/01/2013' AND '04/30/2013'
    AND NOT EXISTS (
      SELECT 1
      FROM factroline
      WHERE ro = a.ro 
        AND paytype = 'i')
    AND a.customerkey <> 0
  GROUP BY b.monthname, b.monthofyear) x
ORDER BY monthofyear  
  
  
    
    
