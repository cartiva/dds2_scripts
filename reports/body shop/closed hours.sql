SELECT * FROM rptBodyShopTechProd

-- so hours closed BY tech BY day

SELECT a.thedate, b.*, c.*
FROM day a
LEFT JOIN factro b ON a.datekey = b.closedatekey
LEFT JOIN factTechLineFlagDateHours c ON b.ro = c.ro
WHERE a.yearmonth = 201211

SELECT *
FROM tmpBSTechs a
LEFT JOIN dimtech b ON a.technumber = b.technumber
LEFT JOIN BridgeTechGroup c ON b.techkey = c.techkey

SELECT a.ro, a.line, c.technumber, a.flaghours, b.weightfactor
-- SELECT *
FROM factTechlineflagdatehours a
INNER JOIN BridgeTechGroup b ON a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c ON b.techkey = c.techkey
INNER JOIN tmpBSTechs d ON c.technumber = d.technumber
WHERE a.ro = '18001467'
ORDER BY ro

SELECT a.ro, c.technumber, SUM(flaghours) AS rohours
-- SELECT *
FROM factTechlineflagdatehours a
INNER JOIN BridgeTechGroup b ON a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c ON b.techkey = c.techkey
INNER JOIN tmpBSTechs d ON c.technumber = d.technumber
-- WHERE ro = '18001467'
GROUP BY a.ro, c.technumber


SELECT a.thedate, b.ro, c.*
FROM day a
INNER JOIN factro b ON a.datekey = b.closedatekey
INNER JOIN (
  SELECT a.ro, c.technumber, SUM(flaghours) AS rohours
  FROM factTechlineflagdatehours a
  INNER JOIN BridgeTechGroup b ON a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c ON b.techkey = c.techkey
  INNER JOIN tmpBSTechs d ON c.technumber = d.technumber
  GROUP BY a.ro, c.technumber) c ON b.ro = c.ro
WHERE a.thedate BETWEEN '12/01/2011' AND curdate() - 1

SELECT a.thedate, technumber, SUM(rohours) AS closedhours
FROM day a
INNER JOIN factro b ON a.datekey = b.closedatekey
INNER JOIN (
  SELECT a.ro, c.technumber, SUM(flaghours) AS rohours
  FROM factTechlineflagdatehours a
  INNER JOIN BridgeTechGroup b ON a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c ON b.techkey = c.techkey
  INNER JOIN tmpBSTechs d ON c.technumber = d.technumber
  GROUP BY a.ro, c.technumber) c ON b.ro = c.ro
WHERE a.thedate BETWEEN '12/01/2011' AND curdate() - 1
GROUP BY thedate, technumber

-- this IS it
SELECT g.*, coalesce(h.closedhours, 0) AS ClosedHours 
-- SELECT *
FROM rptBodyShopTechProd g
LEFT JOIN ( -- date, technumber, closedhours
  SELECT a.thedate, technumber, SUM(rohours) AS closedhours
  FROM day a
  INNER JOIN factro b ON a.datekey = b.closedatekey
  INNER JOIN (
    SELECT a.ro, c.technumber, SUM(flaghours) AS rohours
    FROM factTechlineflagdatehours a
    INNER JOIN BridgeTechGroup b ON a.techgroupkey = b.techgroupkey
    INNER JOIN dimtech c ON b.techkey = c.techkey
    INNER JOIN tmpBSTechs d ON c.technumber = d.technumber
    GROUP BY a.ro, c.technumber) c ON b.ro = c.ro
  WHERE a.thedate BETWEEN '12/01/2011' AND curdate() - 1
  GROUP BY thedate, technumber) h ON g.thedate = h.thedate AND g.technumber = h.technumber
  
  
SELECT thedate, technumber
FROM rptBodyShopTechProd 
GROUP BY thedate, technumber
HAVING COUNT(*) > 1 

