SELECT employeekey
FROM edwEmployeeDim 
WHERE pydept = 'body shop'


SELECT b.monthname, c.distribution, sum(a.vacationhours) AS vacationhours
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  AND c.pydept = 'body shop'
WHERE year(b.thedate) = 2014
  AND a.vacationhours <> 0
  AND c.distribution <> 'ROMAN BRIER'
GROUP BY b.monthname, c.distribution, b.monthofyear  
ORDER BY monthofyear



SELECT *
INTO #wtf
FROM (
SELECT distinct(monthname), monthofyear
FROM day
WHERE theyear = 2014) a, 
  (
SELECT distinct distribution
FROM edwEmployeeDim 
WHERE employeekeyfromdate BETWEEN '01/01/2014' AND curdate()
  AND pydept = 'body shop'
  AND distribution <> 'ROMAN BRIER') b  
ORDER BY monthofyear, distribution  


SELECT c.monthofyear, c.monthname, c.distribution, coalesce(d.vacationhours, 0) AS vacationhours
FROM #wtf c
LEFT JOIN (
  SELECT b.monthname, c.distribution, sum(a.vacationhours) AS vacationhours
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
    AND c.pydept = 'body shop'
  WHERE year(b.thedate) = 2014
    AND a.vacationhours <> 0
    AND c.distribution <> 'ROMAN BRIER'
  GROUP BY b.monthname, c.distribution, b.monthofyear) d on c.monthname = d.monthname AND c.distribution = d.distribution
ORDER BY c.distribution, c.monthofyear