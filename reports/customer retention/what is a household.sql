SELECT *
FROM stgArkonaBOPNAME

SELECT a.bnkey, a.bnsnam, a.bnphon, a.bncity, b.*
FROM stgArkonaBOPNAME a
INNER JOIN (
  SELECT bnadr1, bnzip
  FROM stgArkonaBOPNAME
  WHERE bntype = 'I'
    AND length(trim(bnadr1)) > 6
  GROUP BY bnadr1, bnzip
  HAVING COUNT(*) > 1) b on a.bnadr1 = b.bnadr1
  AND a.bnzip = b.bnzip
ORDER BY a.bnadr1, b.bnzip  


-- the previous notion of phone number AS household

SELECT bnkey, bnphon
-- INTO #phone
FROM stgarkonabopname
WHERE bnphon <> '0' -- must have a homephone value
  AND length(bnphon) > 6 -- phone must be reasonable
  AND LEFT(bnphon, 1) <> '1' -- these are junk
  AND bnkey NOT IN ( -- eliminate dup bnkey rows FROM bopname
    SELECT bnkey
    FROM (
      SELECT bnphon, bnkey
      FROM stgarkonabopname
      WHERE bnphon <> '0'
        AND length(bnphon) > 6
        AND LEFT(bnphon, 1) <> '1'
      GROUP BY bnphon, bnkey) a  
    GROUP BY bnkey 
    HAVING COUNT(*) > 1)     
GROUP BY bnphon, bnkey   

-- unhappy with the notion of phone AS the determining factor
-- too many cell phones IN a single household, AND those cell phone numbers
-- are listed AS the customer phone number

-- some combination of multiple customers with the same phone number
-- IN addition to multiple customers with the same address

-- multiple customers with same address
SELECT a.bnkey, a.bnsnam, a.bnphon, a.bncity, a.bnzip
FROM stgArkonaBOPNAME a
INNER JOIN ( -- multiple instances of address
  SELECT bnadr1, bnzip
  FROM stgArkonaBOPNAME
  WHERE bntype = 'I' -- ikndividuals, NOT companies
    AND length(trim(bnadr1)) > 6 -- eliminate garbage addresses
  GROUP BY bnadr1, bnzip
  HAVING COUNT(*) > 1) b on a.bnadr1 = b.bnadr1
  AND a.bnzip = b.bnzip
ORDER BY a.bnadr1, b.bnzip  

SELECT a.bnkey, a.bnsnam, a.bnphon, a.bncity, a.bnzip
FROM stgArkonaBOPNAME a
INNER JOIN (
  SELECT bnkey, bnphon
  -- INTO #phone
  FROM stgarkonabopname
  WHERE bnphon <> '0' -- must have a homephone value
    AND length(bnphon) > 6 -- phone must be reasonable
    AND LEFT(bnphon, 1) <> '1' -- these are junk
    AND bnkey NOT IN ( -- eliminate dup bnkey rows FROM bopname
      SELECT bnkey
      FROM (
        SELECT bnphon, bnkey
        FROM stgarkonabopname
        WHERE bnphon <> '0'
          AND length(bnphon) > 6
          AND LEFT(bnphon, 1) <> '1'
        GROUP BY bnphon, bnkey) a  
      GROUP BY bnkey 
      HAVING COUNT(*) > 1)     
  GROUP BY bnphon, bnkey) b on a.bnphon = b.bnphon
ORDER BY a.bnphon  


-- what the fuck, i am zoning on retrieving customers that have the same phone number
SELECT a.bnkey, a.bnsnam, a.bnphon, a.bnadr1, a.bncity, a.bnzip
FROM stgArkonaBOPNAME a
WHERE bnphon IN (
  SELECT bnphon
  FROM stgArkonaBOPNAME
  WHERE bnphon <> '0'
    AND length(bnphon) > 6
    AND LEFT(bnphon, 1) NOT IN ('-','1')
    AND bntype = 'I'
      AND bnkey NOT IN ( -- eliminate dup bnkey rows FROM bopname
        SELECT bnkey
        FROM (
          SELECT bnphon, bnkey
          FROM stgarkonabopname
          WHERE bnphon <> '0'
            AND length(bnphon) > 6
            AND LEFT(bnphon, 1) NOT IN ('-','1')
          GROUP BY bnphon, bnkey) a  
        GROUP BY bnkey 
        HAVING COUNT(*) > 1)   
  GROUP BY bnphon 
  HAVING COUNT(*) > 1)
ORDER BY bnphon      


SELECT a.bnkey, a.bnsnam, a.bnphon, a.bnadr1, a.bncity, a.bnzip
FROM stgArkonaBOPNAME a