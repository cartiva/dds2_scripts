  select TheDate, Name, a.RO, 
      sum(case when #StoreCode = 'RY1' and OpCode in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDEL1', 'PDQDP') then 1
        when #StoreCode = 'RY2' and OpCode in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S') then 1
        else 0 end) as OilChange,
    sum(case when #StoreCode = 'RY1' and OpCode in ('PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQDP') then 1
      when #StoreCode = 'RY2' and OpCode in ('LOFD', 'LOF0205', 'MSS', 'M1S') then 1
      else 0 end) as BOC,
      
      
'MS', 'MSHM', 'MSS', 'M1S'

-- 3/13/ just need to get something to ned
-- ALL vehicles with an oil change
SELECT * FROM factro
DROP TABLE #wtf;
SELECT a.storecode, a.vin, a.customerkey, b.thedate, b.yearmonth
INTO #wtf
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%' OR c.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))  
WHERE customerkey <> 0  
GROUP BY a.storecode, a.vin, a.customerkey, b.thedate, b.yearmonth 

-- good enuf?
SELECT *
-- SELECT COUNT(*)
FROM #wtf a
WHERE yearmonth BETWEEN 201102 AND 201202
  AND NOT EXISTS (
    SELECT 1
    FROM #wtf
    WHERE vin = a.vin
      AND customerkey = a.customerkey
      AND yearmonth BETWEEN 201202 AND 201302)     

SELECT COUNT(*) FROM ( -- 11237     
SELECT storecode, customerkey, MAX(thedate)
-- SELECT COUNT(*)
FROM #wtf a
WHERE yearmonth BETWEEN 201102 AND 201202
  AND NOT EXISTS (
    SELECT 1
    FROM #wtf
    WHERE vin = a.vin
      AND customerkey = a.customerkey
      AND yearmonth BETWEEN 201202 AND 201302)   
GROUP BY storecode, customerkey 
-- ) x     

SELECT COUNT(*) FROM ( -- 282
SELECT a.storecode, MAX(thedate) AS thedate, b.bnsnam, bncity, bnstcd, bnzip, bnphon AS homephone, bnbphn AS busphone
FROM (
  SELECT storecode, customerkey, MAX(thedate) AS thedate
  FROM #wtf a
  WHERE yearmonth BETWEEN 201102 AND 201202
    AND NOT EXISTS (
      SELECT 1
      FROM #wtf
      WHERE vin = a.vin
        AND customerkey = a.customerkey
        AND yearmonth BETWEEN 201202 AND 201302)   
  GROUP BY storecode, customerkey ) a
INNER JOIN stgarkonabopname b ON a.customerkey = b.bnkey  
GROUP BY a.storecode, b.bnsnam, bncity, bnstcd, bnzip, bnphon, bnbphn
) x            

SELECT COUNT(*) FROM ( --9431
SELECT a.storecode, MAX(thedate) AS thedate, b.bnsnam, bncity, bnstcd, bnzip, bnphon AS homephone, bnbphn AS busphone
FROM (
  SELECT storecode, customerkey, MAX(thedate) AS thedate
  FROM #wtf a
  WHERE yearmonth BETWEEN 201102 AND 201202
    AND NOT EXISTS (
      SELECT 1
      FROM #wtf
      WHERE vin = a.vin
        AND customerkey = a.customerkey
        AND yearmonth BETWEEN 201202 AND 201302)   
  GROUP BY storecode, customerkey ) a
INNER JOIN stgarkonabopname b ON a.customerkey = b.bnkey  
WHERE bnphon <> '0'
GROUP BY a.storecode, b.bnsnam, bncity, bnstcd, bnzip, bnphon, bnbphn
) x

-- ok, make it INTO a temp TABLE
SELECT a.storecode, MAX(thedate) AS thedate, b.bnsnam, bncity, bnstcd, bnzip, bnphon AS homephone, bnbphn AS busphone
INTO #wtf1
FROM (
  SELECT storecode, customerkey, MAX(thedate) AS thedate
  FROM #wtf a
  WHERE yearmonth BETWEEN 201102 AND 201202
    AND NOT EXISTS (
      SELECT 1
      FROM #wtf
      WHERE vin = a.vin
        AND customerkey = a.customerkey
        AND yearmonth BETWEEN 201202 AND 201302)   
  GROUP BY storecode, customerkey ) a
INNER JOIN stgarkonabopname b ON a.customerkey = b.bnkey  
WHERE bnphon <> '0'
GROUP BY a.storecode, b.bnsnam, bncity, bnstcd, bnzip, bnphon, bnbphn

-- same last name & homephone
-- SELECT * FROM #wtf1
SELECT LEFT(bnsnam, position(',' IN bnsnam) -1) AS lastname, homephone
FROM #wtf1
WHERE position (',' IN bnsnam) <> 0
GROUP BY LEFT(bnsnam, position(',' IN bnsnam) -1), homephone
HAVING COUNT(*) > 1

SELECT *
FROM #wtf1 a
INNER JOIN (
  SELECT LEFT(bnsnam, position(',' IN bnsnam) -1) AS lastname, homephone
  FROM #wtf1
  WHERE position (',' IN bnsnam) <> 0
  GROUP BY LEFT(bnsnam, position(',' IN bnsnam) -1), homephone
  HAVING COUNT(*) > 1) b ON LEFT(a.bnsnam, position(',' IN a.bnsnam) -1) = b.lastname
    AND a.homephone = b.homephone
WHERE position (',' IN a.bnsnam) <> 0    
ORDER BY a.homephone

-- min/MAX names
SELECT LEFT(bnsnam, position(',' IN bnsnam) -1) AS lastname, min(bnsnam) AS name1, 
  max(bnsnam) AS name2, homephone
FROM #wtf1
WHERE position (',' IN bnsnam) <> 0
GROUP BY LEFT(bnsnam, position(',' IN bnsnam) -1), homephone

SELECT  lastname, 
  CASE
    WHEN name1 = name2 THEN name1
    ELSE trim(name1) + ' --- ' + name2
  END AS name,
  homephone
FROM (
  SELECT LEFT(bnsnam, position(',' IN bnsnam) -1) AS lastname, min(bnsnam) AS name1, 
    max(bnsnam) AS name2, homephone
  FROM #wtf1
  WHERE position (',' IN bnsnam) <> 0
  GROUP BY LEFT(bnsnam, position(',' IN bnsnam) -1), homephone) a
  
-- don't GROUP ON lastname, just ON homephone
SELECT 
  CASE
    WHEN name1 = name2 THEN name1
    ELSE TRIM(name1) + ' --- ' + name2
  END AS name,
  homephone
FROM (   
  SELECT MIN(bnsnam) AS name1, MAX(bnsnam) AS name2, homephone 
  FROM #wtf1  
  GROUP BY homephone) a
ORDER BY name  

ok, back up the aggregation path, verify that BAKKEN, CARRIE L - BAKKEN, PAUL  7017465422  makes sense

#wtf  any vehicle that has had an oil change
      grouped by storecode, vin, customerkey, thedate, yearmonth 
      
SELECT * FROM #wtf a
INNER JOIN stgarkonabopname b ON a.customerkey = b.bnkey
WHERE thedate BETWEEN curdate() - 760 AND curdate() - 365
  AND NOT EXISTS (
    SELECT 1
    FROM #wtf
    WHERE vin = a.vin
      AND customerkey = a.customerkey
      AND thedate BETWEEN curdate() - 364 AND curdate())     
  AND (bnsnam LIKE 'BAKKEN, CAR%' OR bnsnam LIKE 'BAKKEN, PA%')

ok, carrie''s car IS no longer owned BY her neither IS paul''s

so does starting with vehicles w/oc events make sense, OR should it be customers w/oc events 

-- customers with oil change events
DROP TABLE #wtf;
SELECT a.customerkey, b.thedate
INTO #wtf
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%' OR c.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))  
WHERE customerkey <> 0  
GROUP BY a.customerkey, b.thedate

--SELECT COUNT(*) FROM ( -- 8620
-- dist custkeys w/oc event 12-24 mos ago, but NOT IN past year AND the MAX oc event date
SELECT -- dist custkeys w/oc event 12-24 mos ago, but NOT IN past year AND the MAX oc event date
  customerkey, max(thedate) AS thedate
FROM #wtf a
WHERE thedate BETWEEN curdate() - 760 AND curdate() - 365
  AND NOT EXISTS (
    SELECT 1
    FROM #wtf
    WHERE customerkey = a.customerkey
      AND thedate BETWEEN curdate() - 364 AND curdate())
GROUP BY customerkey      

SELECT a.*, b.bnsnam, bncity, bnstcd, bnzip, bnphon AS homephone, bnbphn AS busphone
FROM ( -- dist custkeys w/oc event 12-24 mos ago, but NOT IN past year AND the MAX oc event date
  SELECT customerkey, max(thedate) AS thedate
  FROM #wtf a
  WHERE thedate BETWEEN curdate() - 760 AND curdate() - 365
    AND NOT EXISTS (
      SELECT 1
      FROM #wtf
      WHERE customerkey = a.customerkey
        AND thedate BETWEEN curdate() - 364 AND curdate())
  GROUP BY customerkey) a
LEFT JOIN stgarkonabopname b ON a.customerkey = b.bnkey  
WHERE b.bnphon <> '0'
ORDER BY bnphon

repeat people (based ON name (eg COURTNEY PULKRABEK - 4 diff custkeys) with diff custkey, use the key
FROM the most recent visit

thinking GROUP BY homephone, with min/MAX names

wait, first eliminate 12-24 ocevent custkeys that have a non-ocevent service visit IN 
the past 12 months
want customers who have had ON oc event 12-24 AND HAVE NOT BEEN BACK TO SERVICE FOR ANYTHING IN the past year
-- DROP TABLE #wtf1;
SELECT * -- custkey, date w/ocevent 12-24 but no ocevent IN the past year
INTO #wtf1
FROM #wtf a
WHERE thedate BETWEEN curdate() - 760 AND curdate() - 365
  AND NOT EXISTS (
    SELECT 1
    FROM #wtf
    WHERE customerkey = a.customerkey
      AND thedate BETWEEN curdate() - 364 AND curdate())


SELECT -- customers who have had ON oc event 12-24 AND HAVE NOT BEEN BACK TO SERVICE FOR ANYTHING IN the past year
  customerkey, MAX(thedate) AS thedate
INTO #wtf2  
FROM #wtf1 a
WHERE NOT EXISTS (
  SELECT 1 
  FROM factro c
  INNER JOIN day b ON c.opendatekey = b.datekey
  WHERE c.customerkey <> 0
    AND c.customerkey = a.customerkey
    AND b.thedate BETWEEN curdate() - 364 AND curdate())
GROUP BY customerkey    

SELECT * FROM stgarkonabopname
DROP TABLE #wtf3;
SELECT a.*, b.bnsnam, bnadr1, bncity, bnstcd, bnzip, bnphon AS homephone
INTO #wtf3
FROM #wtf2 a
INNER JOIN stgarkonabopname b ON a.customerkey = b.bnkey 
WHERE bnphon <> '0'


SELECT *
FROM #wtf3
WHERE homephone IN (
  SELECT homephone
  FROM #wtf3
  GROUP BY homephone
  HAVING COUNT(*) > 1)
ORDER BY homephone

interesting results
  7017920320 - 3 people, 3 versions of the same address
  7017461881 - 2 people
  7017755331, 2182811219 - 1 person, 3 customer keys
  2182812234 - 1 person, 4 custkeys, 2 names, 4 addresses
  

the question now IS GROUP ON homephone?
i am thinking good enuf

picking the correct custkey

-- 3/14 thinking DO the grouping BY phone number before limiting to lost customers
-- what does that look LIKE
-- #wtf any custkey with an oc event AND the date of that oc event
SELECT a.*, b.bnsnam, bncity, bnstcd, bnzip, bnphon AS homephone, bnbphn AS busphone
FROM #wtf a
LEFT JOIN stgarkonabopname b ON a.customerkey = b.bnkey

OR maybe somekind of grouping ON bopname first

SELECT bnkey, bnsnam, bnadr1, bncity, bnstcd, bnzip, bnphon
FROM stgarkonabopname
WHERE bnphon <> '0' 
  AND bnsnam LIKE 'pearson%'
ORDER BY bnphon

ok, grouping BY address will NOT WORK because of shit LIKE:
319 N 2ND ST
319 2 ST N

so grouping ON phone means any custkey with that phone number that has had ON oc event 12-24 but no service visit IN past year
phone implies household

SELECT bnphon, bnkey
FROM stgarkonabopname
--WHERE bnphon <> '0' 
GROUP BY bnphon, bnkey
HAVING COUNT(*) > 1

SELECT bnkey, bnsnam, bnadr1, bncity, bnstcd, bnzip, bnphon
FROM stgarkonabopname
WHERE bnkey IN (1047618,1058108,1008771,1012685,1036816)

SELECT bnkey, bnsnam, bnadr1, bncity, bnstcd, bnzip, bnphon
FROM stgarkonabopname
WHERE bnphon IN ('6123609121','7012004949','7012326496','2187736454','2187736454')

SELECT bnphon, bnsnam, bnadr1, bncity, bnstcd, bnzip, bnphon
FROM stgarkonabopname
-- ok, i think i am sufficiently happy to use phone AS the household grouping  
-- ALL bopname rows with a homephone
SELECT bnphon, bnkey, bnsnam, bnadr1, bncity, bnstcd, bnzip
INTO #wtf1
FROM stgarkonabopname a
WHERE bnphon <>'0'
  AND bnkey NOT IN ( -- less than a dozen dups
    SELECT bnkey
    FROM stgarkonabopname
    GROUP BY bnphon, bnkey
    HAVING COUNT(*) > 1)
    
-- DO any of the resulting custkeys have multiple phone#s?  
SELECT * 
--SELECT COUNT(*)
FROM #wtf1 WHERE bnkey IN (
SELECT bnkey FROM #wtf1 GROUP BY bnkey HAVING COUNT(*) > 1)
ORDER BY bnkey  

SELECT * FROM stgarkonabopname WHERE bnkey =  261555

SELECT * 
INTO #wtf2 -- ALL custkeys with a single homephone
FROM #wtf1
WHERE bnkey NOT IN (
  SELECT bnkey
  FROM #wtf1
  GROUP BY bnkey
  HAVING COUNT(*) > 1)
  
SELECT bnphon, bnkey FROM #wtf2 GROUP BY bnphon, bnkey HAVING COUNT(*) > 1 
SELECT bnkey FROM #wtf2 GROUP BY bnkey HAVING COUNT(*) > 1

SELECT * FROM #wtf2 WHERE bnphon IN (SELECT bnphon FROM #wtf2 GROUP BY bnphon HAVING COUNT(*) > 2)
ORDER BY bnphon
-- ok
display idea
phone  
       customer
       customer
phone
       customer
phone
       customer
       customer
       customer              

think #wtf2 data IS good enough, use it to assign a phone number to oc events
SELECT * FROM #wtf2
SELECT * -- 175131
FROM #wtf a
INNER JOIN #wtf2 b ON a.customerkey = b.bnkey

need to assign phone to factro AS well
so , one TABLE IS factro w/phone for the past year
-- include customer name/vin FROM ro

SELECT -- service events for the past year
  b.thedate, c.bnphon, a.*
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN #wtf2 c ON a.customerkey = c.bnkey
WHERE b.thedate BETWEEN curdate() - 365 AND curdate()


SELECT -- oc change events 12-24 months ago
    b.thedate, d.bnphon, a.*
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%' OR c.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))  
INNER JOIN #wtf2 d ON a.customerkey = d.bnkey  
WHERE a.customerkey <> 0  
  AND b.thedate BETWEEN curdate() -730 AND curdate() - 366

-- golly, i sure hope this IS it  
need the cust info
SELECT *
-- SELECT thedate, bnphon, ro, vin, customername AS roname, customerkey as rocustkey
FROM (
  SELECT -- oc change events 12-24 months ago
      b.thedate, d.bnphon, a.*
  FROM factro a
  INNER JOIN day b ON a.opendatekey = b.datekey
  INNER JOIN factroline c ON a.ro = c.ro
    AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%' OR c.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))  
  INNER JOIN #wtf2 d ON a.customerkey = d.bnkey  
  WHERE a.customerkey <> 0  
    AND b.thedate BETWEEN curdate() -730 AND curdate() - 366) a  
WHERE NOT EXISTS (
  SELECT 1
  FROM (
    SELECT -- service events for the past year
      b.thedate, c.bnphon, a.*
    FROM factro a
    INNER JOIN day b ON a.opendatekey = b.datekey
    INNER JOIN #wtf2 c ON a.customerkey = c.bnkey
    WHERE b.thedate BETWEEN curdate() - 365 AND curdate()) b
  WHERE b.bnphon = a.bnphon)         
ORDER BY thedate desc

-- golly, i sure hope this IS it  
-- need the cust info, so this looks LIKE yet another JOIN to custinfo

SELECT a.*, b.bnsnam, b.bnadr1, b.bncity, b.bnstcd, b.bnzip
FROM (
  SELECT thedate, bnphon, ro, vin, customername AS roname, customerkey as rocustkey
  FROM (
    SELECT -- oc change events 12-24 months ago
        b.thedate, d.bnphon, a.*
    FROM factro a
    INNER JOIN day b ON a.opendatekey = b.datekey
    INNER JOIN factroline c ON a.ro = c.ro
      AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%' OR c.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))  
    INNER JOIN #wtf2 d ON a.customerkey = d.bnkey  
    WHERE a.customerkey <> 0  
      AND b.thedate BETWEEN curdate() -730 AND curdate() - 366) a  
  WHERE NOT EXISTS (
    SELECT 1
    FROM (
      SELECT -- service events for the past year
        b.thedate, c.bnphon, a.*
      FROM factro a
      INNER JOIN day b ON a.opendatekey = b.datekey
      INNER JOIN #wtf2 c ON a.customerkey = c.bnkey
      WHERE b.thedate BETWEEN curdate() - 365 AND curdate()) b
    WHERE b.bnphon = a.bnphon)) a
LEFT JOIN stgarkonabopname b ON a.rocustkey = b.bnkey
ORDER BY a.bnphon

SELECT * FROM #wtf2
SELECT * FROM #wtf1

SELECT -- oc event 12-24
  a.ro, a.customerkey, a.vin
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%' OR c.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))  
WHERE a.customerkey <> 0  
  AND b.thedate BETWEEN curdate() -730 AND curdate() - 366
  
SELECT -- service event past year
  a.ro, a.customerkey, a.vin
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%' OR c.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))  
WHERE a.customerkey <> 0  
  AND b.thedate BETWEEN curdate() - 365 AND curdate()
  
SELECT bnkey, bnphon
FROM stgarkonabopname 
WHERE bnphon <> '0' 
GROUP BY bnkey, bnphon

define a household
  !! each custkey IS part of a single household
  a phone AND one OR more custkeys
-- so, custkey IS unique
-- phone IS NOT
-- each phone can be for mult custkeys (duh, a household)
-- each custkey can only be part of a single household

DROP TABLE zHousehold; 
CREATE TABLE zHousehold ( 
      customerkey Integer, -- PK
      phone CIChar( 12 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'zHousehold',
   'zHousehold.adi',
   'CUSTOMERKEY',
   'customerkey',
   '',
   2051,
   512,
   '' ); 

-- a household IS one OR more rows FROM bopname that share a homephone number 
INSERT INTO zhousehold
SELECT bnkey, bnphon
FROM stgarkonabopname
WHERE bnphon <> '0' -- must have a homephone value
  AND length(bnphon) > 6 -- phone must be reasonable
  AND LEFT(bnphon, 1) <> '1' -- these are junk
  AND bnkey NOT IN ( -- eliminate dup bnkey rows FROM bopname
    SELECT bnkey
    FROM (
      SELECT bnphon, bnkey
      FROM stgarkonabopname
      WHERE bnphon <> '0'
        AND length(bnphon) > 6
        AND LEFT(bnphon, 1) <> '1'
      GROUP BY bnphon, bnkey) a  
    GROUP BY bnkey 
    HAVING COUNT(*) > 1)     
GROUP BY bnphon, bnkey;   

SELECT * FROM zhousehold
SELECT customerkey FROM zhousehold GROUP BY customerkey HAVING COUNT(*) > 1

SELECT bnkey FROM stgarkonabopname GROUP BY bnkey HAVING COUNT(*) > 1
SELECT *
SELECT COUNT(*)
FROM stgarkonabopname
WHERE bnkey IN (SELECT bnkey FROM stgarkonabopname GROUP BY bnkey HAVING COUNT(*) > 1) 
ORDER BY bnkey

SELECT ro FROM factro GROUP BY ro HAVING COUNT(*) > 1
SELECT * FROM factro

SELECT ro FROM (
SELECT 
  z.*, b.thedate, a.ro, a.vin, c.opcode, d.bnsnam, d.bnadr1, d.bncity, d.bnstcd, d.bnzip
-- SELECT COUNT(*) -- 44799
FROM zhousehold z
INNER JOIN factro a ON z.customerkey = a.customerkey
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%' OR c.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))  
LEFT JOIN (
  SELECT *
  FROM stgarkonabopname
  WHERE bnphon <> '0') d ON z.customerkey = d.bnkey
WHERE b.thedate BETWEEN curdate() -730 AND curdate() - 366  
) x GROUP BY ro HAVING COUNT(*) > 1 ORDER BY ro

-- how the fuck IS this generating dup ros
-- absolutely there should be one row per rw
SELECT 
  z.*, b.thedate, a.ro, a.vin, c.opcode, d.bnsnam, d.bnadr1, d.bncity, d.bnstcd, d.bnzip
-- SELECT COUNT(*) -- 44799
FROM zhousehold z
INNER JOIN factro a ON z.customerkey = a.customerkey
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%' OR c.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))  
LEFT JOIN (
  SELECT *
  FROM stgarkonabopname
  WHERE bnphon <> '0') d ON z.customerkey = d.bnkey
WHERE b.thedate BETWEEN curdate() -730 AND curdate() - 366 
  AND a.ro IN (
    SELECT ro FROM (
      SELECT 
        z.*, b.thedate, a.ro, a.vin, c.opcode, d.bnsnam, d.bnadr1, d.bncity, d.bnstcd, d.bnzip
      -- SELECT COUNT(*) -- 44799
      FROM zhousehold z
      INNER JOIN factro a ON z.customerkey = a.customerkey
      INNER JOIN day b ON a.opendatekey = b.datekey
      INNER JOIN factroline c ON a.ro = c.ro
        AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%' OR c.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))  
      LEFT JOIN (
        SELECT *
        FROM stgarkonabopname
        WHERE bnphon <> '0') d ON z.customerkey = d.bnkey
      WHERE b.thedate BETWEEN curdate() -730 AND curdate() - 366  
    ) x GROUP BY ro HAVING COUNT(*) > 1)  
    
think it IS dup bnkey IN stgarkonabopname, need to exclude dups IN bopname subquery
well, that IS some but NOT all
SELECT * FROM stgarkonabopname WHERE bnkey IN (1001413,1024800,1001884,1047618,281127,1001413,1026246,1052082,267853,1054965)    
-- what about factroline
yep, that IS definitely generating dups
SELECT a.ro
FROM factro a
INNER JOIN factroline b ON a.ro = b.ro
  AND (b.opcode LIKE '%lof%' OR b.opcode LIKE '%pdq%' OR b.opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))
GROUP BY a.ro HAVING COUNT(*) > 1  

-- move factroline to EXISTS
-- exclude dup bnkey
-- SELECT ro FROM ( -- ro now unique IN result set
SELECT 
  z.*, b.thedate, a.ro, a.vin, d.bnsnam, d.bnadr1, d.bncity, d.bnstcd, d.bnzip
-- SELECT COUNT(*) -- 44799
INTO #wtf
FROM zhousehold z
INNER JOIN factro a ON z.customerkey = a.customerkey
INNER JOIN day b ON a.opendatekey = b.datekey
LEFT JOIN ( 
  SELECT *
  FROM stgarkonabopname
  WHERE bnphon <> '0'
    AND bnkey NOT IN ( -- no dup bnkey
      SELECT bnkey
      FROM stgarkonabopname
      WHERE bnphon <> '0'
      GROUP BY bnkey 
      HAVING COUNT(*) > 1)) d ON z.customerkey = d.bnkey
WHERE b.thedate BETWEEN curdate() -730 AND curdate() - 366 -- 12-24 mos ago
  AND EXISTS ( -- an oilchange event
    SELECT 1 
    FROM factroline
    WHERE ro = a.ro
      AND (opcode LIKE '%lof%' OR opcode LIKE '%pdq%' OR opcode IN ('MS', 'MSHM', 'MSS', 'M1S'))) 
--) x GROUP BY ro HAVING COUNT(*) > 1      

SELECT * FROM #wtf a
WHERE EXISTS (
  SELECT 1 
  FROM (
    SELECT customerkey, phone
    FROM  #wtf
    GROUP BY customerkey, phone
    HAVING COUNT(*) > 2) b
  WHERE b.customerkey = a.customerkey 
    AND b.phone = b.phone)
ORDER BY phone    

SELECT phone, customerkey, MAX(thedate) AS maxDate, vin, bnsnam, bnadr1, bncity, bnstcd, bnzip
FROM #wtf
GROUP BY phone, customerkey, vin, bnsnam, bnadr1, bncity, bnstcd, bnzip

i DO NOT care about which car, any car IN the household IS what i am looking for
so take vin out of the grouping
ALL i need FROM the ro IS whether one EXISTS AND the date

SELECT phone, customerkey, MAX(thedate) AS maxDate, bnsnam, bnadr1, bncity, bnstcd, bnzip
FROM #wtf
GROUP BY phone, customerkey, bnsnam, bnadr1, bncity, bnstcd, bnzip

SELECT thedate, customerkey FROM #wtf GROUP BY thedate, customerkey HAVING COUNT(*) > 1

SELECT * FROM #wtf WHERE thedate = '03/08/2012' AND customerkey  = 216999

SELECT * FROM #wtf WHERE thedate = '12/26/2011' AND phone = '2187732053'

SELECT phone FROM (
select phone,thedate FROM #wtf GROUP BY phone, thedate) a GROUP BY phone HAVING COUNT(*) > 1
---- the fucking question here IS what IS the granularity of #WTF
-- compared to what DO i want FROM #wtf
1. ALL households that had an ocevent 12-24 mos ago
2. that did NOT have service event IN the past year
3. phone -|---|< name, addr, city, zip
-- DROP TABLE #wtf;   
SELECT -- households (phone) with oc event 12-24 mos ago
  z.phone, z.customerkey, d.bnsnam, d.bnadr1, d.bncity, d.bnstcd, d.bnzip
INTO #wtf
FROM zhousehold z
INNER JOIN factro a ON z.customerkey = a.customerkey
INNER JOIN day b ON a.opendatekey = b.datekey
LEFT JOIN ( 
  SELECT *
  FROM stgarkonabopname
  WHERE bnphon <> '0'
    AND bnkey NOT IN ( -- no dup bnkey
      SELECT bnkey
      FROM stgarkonabopname
      WHERE bnphon <> '0'
      GROUP BY bnkey 
      HAVING COUNT(*) > 1)) d ON z.customerkey = d.bnkey
WHERE b.thedate BETWEEN curdate() -730 AND curdate() - 366 -- 12-24 mos ago
  AND EXISTS ( -- an oilchange event
    SELECT 1 
    FROM factroline
    WHERE ro = a.ro
      AND (opcode LIKE '%lof%' OR opcode LIKE '%pdq%' OR opcode IN ('MS', 'MSHM', 'MSS', 'M1S')))  
GROUP BY z.phone, z.customerkey, d.bnsnam, d.bnadr1, d.bncity, d.bnstcd, d.bnzip       


SELECT -- households (phone) with service event past year
  z.phone
FROM zhousehold z  
INNER JOIN factro a ON z.customerkey = a.customerkey
INNER JOIN day b ON a.opendatekey = b.datekey
  AND b.thedate BETWEEN curdate() - 365 AND curdate()
  
  
SELECT a.*
--INTO #wtf1
FROM (
  SELECT -- households (phone) with oilchange event 12-24 mos ago
    z.phone, z.customerkey, d.bnsnam, d.bnadr1, d.bncity, d.bnstcd, d.bnzip
  FROM zhousehold z
  INNER JOIN factro a ON z.customerkey = a.customerkey
  INNER JOIN day b ON a.opendatekey = b.datekey
  LEFT JOIN ( 
    SELECT *
    FROM stgarkonabopname
    WHERE bnphon <> '0'
      AND bnkey NOT IN ( -- no dup bnkey
        SELECT bnkey
        FROM stgarkonabopname
        WHERE bnphon <> '0'
        GROUP BY bnkey 
        HAVING COUNT(*) > 1)) d ON z.customerkey = d.bnkey
  WHERE b.thedate BETWEEN curdate() -730 AND curdate() - 366 -- 12-24 mos ago
    AND EXISTS ( -- an oilchange event
      SELECT 1 
      FROM factroline
      WHERE ro = a.ro
        AND (opcode LIKE '%lof%' OR opcode LIKE '%pdq%' OR opcode IN ('MS', 'MSHM', 'MSS', 'M1S')))  
  GROUP BY z.phone, z.customerkey, d.bnsnam, d.bnadr1, d.bncity, d.bnstcd, d.bnzip) a
LEFT JOIN (
  SELECT -- households (phone) with any service event past year
    z.phone
  FROM zhousehold z  
  INNER JOIN factro a ON z.customerkey = a.customerkey
  INNER JOIN day b ON a.opendatekey = b.datekey
    AND b.thedate BETWEEN curdate() - 365 AND curdate()) b ON a.phone = b.phone    
WHERE b.phone IS NULL    
ORDER BY a.phone 

SELECT *
FROM #wtf1 a   
INNER JOIN (
  SELECT -- households (phone) with any service event past year
    z.phone
  FROM zhousehold z  
  INNER JOIN factro a ON z.customerkey = a.customerkey
  INNER JOIN day b ON a.opendatekey = b.datekey
    AND b.thedate BETWEEN curdate() - 365 AND curdate()) b ON a.phone = b.phone      