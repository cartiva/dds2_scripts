select 
  CASE 
    WHEN a.storecode = 'RY1' THEN 'GM'
	ELSE 'Honda/Nissan'
  END AS store, b.thedate, a.stocknumber, 
  round(a.vehicleprice, 0) AS price, round(a.vehiclecost, 0) AS cost,
  round(a.vehicleprice, 0) - round(a.vehiclecost, 0),
  c.saletype, 
  CASE 
    WHEN d.homephone <> '0' THEN homephone
    ELSE 
      CASE 
	    WHEN cellphone <> '0' THEN cellphone
        ELSE 
          CASE 
		    WHEN businessphone <> '0' THEN businessphone
    	    ELSE 'None'
    	  END
      END
  END AS phone,
  CASE
    WHEN emailvalid = true THEN email
	ELSE
	  CASE 
	    WHEN email2valid = true THEN email2
		ELSE 'None'
	  END
  END AS email
--INTO #sales
FROM factvehiclesale a
INNER JOIN day b on a.cappeddatekey = b.datekey
  AND b.yearmonth BETWEEN 201610 AND 201611
INNER JOIN dimcardealinfo c on a.cardealinfokey = c.cardealinfokey
  AND c.saletype <> 'wholesale'
INNER JOIN dimcustomer d on a.buyerkey = d.customerkey
ORDER BY a.storecode, thedate, stocknumber

select * FROM #sales ORDER BY price

SELECT stocknumber, SUM(gttamt) AS amount
FROM #sales a
LEFT JOIN stgarkonaglptrns b on a.stocknumber = b.gtctl#
  AND b.gtdate BETWEEN '10/01/2016' AND '11/01/2016'
INNER JOIN #accounts c on trim(b.gtacct) = trim(c.account)
GROUP BY stocknumber

FROM postgres
select bopmast_company_number, record_key, sale_type, bopmast_stock_number, bopmast_vin, buyer_number, date_capped, 
  vehicle_cost, retail_price, lease_price, house_gross, saved_price,
  CASE 
    WHEN b.homephone <> '0' THEN homephone
    ELSE 
      CASE 
	    WHEN cellphone <> '0' THEN cellphone
        ELSE 
          CASE 
		    WHEN businessphone <> '0' THEN businessphone
    	    ELSE 'None'
    	  END
      END
  END AS phone,
  CASE
    WHEN emailvalid = true THEN email
	ELSE
	  CASE 
	    WHEN email2valid = true THEN email2
		ELSE 'None'
	  END
  END AS email
-- select *
from dds.ext_bopmast a
left join ads.ext_dds_dimcustomer b on a.buyer_number = b.bnkey
where date_capped between '10/01/2016' and '11/30/2016'  
  and sale_type <> 'W'
order by bopmast_company_number, date_capped, bopmast_stock_number  
  
  
SELECT * FROM #accounts
/*
CREATE TABLE #accounts (account cichar(12));
insert into #accounts values ('242600');
insert into #accounts values ('265110');
insert into #accounts values ('263700');
insert into #accounts values ('240500');
insert into #accounts values ('241000');
insert into #accounts values ('1436001');
insert into #accounts values ('264800');
insert into #accounts values ('240700');
insert into #accounts values ('265500');
insert into #accounts values ('260700');
insert into #accounts values ('1404001');
insert into #accounts values ('244010');
insert into #accounts values ('165000');
insert into #accounts values ('285730');
insert into #accounts values ('185600');
insert into #accounts values ('280601');
insert into #accounts values ('1634001');
insert into #accounts values ('244300');
insert into #accounts values ('262700');
insert into #accounts values ('264301');
insert into #accounts values ('1629301');
insert into #accounts values ('242210');
insert into #accounts values ('1426004');
insert into #accounts values ('264710');
insert into #accounts values ('164300');
insert into #accounts values ('1432001');
insert into #accounts values ('1434001');
insert into #accounts values ('265502');
insert into #accounts values ('264601');
insert into #accounts values ('1411001');
insert into #accounts values ('261900');
insert into #accounts values ('1641001');
insert into #accounts values ('260800');
insert into #accounts values ('244800');
insert into #accounts values ('1440012');
insert into #accounts values ('165101');
insert into #accounts values ('244301');
insert into #accounts values ('1629001');
insert into #accounts values ('1627006');
insert into #accounts values ('245500');
insert into #accounts values ('265001');
insert into #accounts values ('245502');
insert into #accounts values ('1427004');
insert into #accounts values ('1629012');
insert into #accounts values ('243210');
insert into #accounts values ('264400');
insert into #accounts values ('242900');
insert into #accounts values ('185003');
insert into #accounts values ('185900');
insert into #accounts values ('185000');
insert into #accounts values ('185001');
insert into #accounts values ('185700');
insert into #accounts values ('1457001');
insert into #accounts values ('260200');
insert into #accounts values ('1431006');
insert into #accounts values ('144800');
insert into #accounts values ('164800');
insert into #accounts values ('264000');
insert into #accounts values ('264620');
insert into #accounts values ('145000');
insert into #accounts values ('1429301');
insert into #accounts values ('165500');
insert into #accounts values ('1421001');
insert into #accounts values ('243100');
insert into #accounts values ('261100');
insert into #accounts values ('264700');
insert into #accounts values ('265400');
insert into #accounts values ('265200');
insert into #accounts values ('1429312');
insert into #accounts values ('1602004');
insert into #accounts values ('1624012');
insert into #accounts values ('180800');
insert into #accounts values ('265100');
insert into #accounts values ('240410');
insert into #accounts values ('262600');
insert into #accounts values ('240800');
insert into #accounts values ('245020');
insert into #accounts values ('145001');
insert into #accounts values ('1429001');
insert into #accounts values ('265101');
insert into #accounts values ('264402');
insert into #accounts values ('244610');
insert into #accounts values ('1611001');
insert into #accounts values ('1640012');
insert into #accounts values ('280800');
insert into #accounts values ('244620');
insert into #accounts values ('165200');
insert into #accounts values ('1604001');
insert into #accounts values ('1631001');
insert into #accounts values ('165100');
insert into #accounts values ('244000');
insert into #accounts values ('1429012');
insert into #accounts values ('1631012');
insert into #accounts values ('1425001');
insert into #accounts values ('1636001');
insert into #accounts values ('1428001');
insert into #accounts values ('1625001');
insert into #accounts values ('164701');
insert into #accounts values ('263000');
insert into #accounts values ('185002');
insert into #accounts values ('280600');
insert into #accounts values ('1632012');
insert into #accounts values ('242610');
insert into #accounts values ('1402004');
insert into #accounts values ('244402');
insert into #accounts values ('264602');
insert into #accounts values ('185101');
insert into #accounts values ('144601');
insert into #accounts values ('1632001');
insert into #accounts values ('240200');
insert into #accounts values ('185100');
insert into #accounts values ('1433007');
insert into #accounts values ('264600');
insert into #accounts values ('144300');
insert into #accounts values ('185102');
insert into #accounts values ('1441004');
insert into #accounts values ('262500');
insert into #accounts values ('242700');
insert into #accounts values ('1635001');
insert into #accounts values ('165001');
insert into #accounts values ('1428012');
insert into #accounts values ('1628001');
insert into #accounts values ('280802');
insert into #accounts values ('1626004');
insert into #accounts values ('280801');
insert into #accounts values ('1600001');
insert into #accounts values ('1424012');
insert into #accounts values ('1632006');
insert into #accounts values ('1641004');
insert into #accounts values ('1627004');
insert into #accounts values ('242500');
insert into #accounts values ('242710');
insert into #accounts values ('245200');
insert into #accounts values ('245000');
insert into #accounts values ('1640001');
insert into #accounts values ('245400');
insert into #accounts values ('2457001');
insert into #accounts values ('244600');
insert into #accounts values ('285011');
insert into #accounts values ('264401');
insert into #accounts values ('164601');
insert into #accounts values ('1440001');
insert into #accounts values ('164600');
insert into #accounts values ('244400');
insert into #accounts values ('241110');
insert into #accounts values ('285010');
insert into #accounts values ('244602');
insert into #accounts values ('265501');
insert into #accounts values ('264701');
insert into #accounts values ('263200');
insert into #accounts values ('265002');
insert into #accounts values ('180900');
insert into #accounts values ('1628012');
insert into #accounts values ('1441012');
insert into #accounts values ('242510');
insert into #accounts values ('144600');
insert into #accounts values ('285100');
insert into #accounts values ('241100');
insert into #accounts values ('263100');
insert into #accounts values ('243110');
insert into #accounts values ('1432012');
insert into #accounts values ('1623012');
insert into #accounts values ('285720');
insert into #accounts values ('245501');
insert into #accounts values ('1431001');
insert into #accounts values ('1641012');
insert into #accounts values ('1605001');
insert into #accounts values ('264300');
insert into #accounts values ('180600');
insert into #accounts values ('241010');
insert into #accounts values ('1405001');
insert into #accounts values ('285740');
insert into #accounts values ('1607001');
insert into #accounts values ('1441001');
insert into #accounts values ('1629312');
insert into #accounts values ('2657001');
insert into #accounts values ('243300');
insert into #accounts values ('1657001');
insert into #accounts values ('264720');
insert into #accounts values ('243010');
insert into #accounts values ('1625004');
insert into #accounts values ('1633007');
insert into #accounts values ('164700');
insert into #accounts values ('262200');
insert into #accounts values ('1603006');
insert into #accounts values ('1610006');
insert into #accounts values ('265020');
insert into #accounts values ('243000');
insert into #accounts values ('1410006');
insert into #accounts values ('185501');
insert into #accounts values ('260400');
insert into #accounts values ('145500');
insert into #accounts values ('242200');
insert into #accounts values ('144400');
insert into #accounts values ('245010');
insert into #accounts values ('1621001');
insert into #accounts values ('240400');
insert into #accounts values ('180700');
insert into #accounts values ('244401');
insert into #accounts values ('240710');
insert into #accounts values ('241900');
insert into #accounts values ('240210');
insert into #accounts values ('240510');
insert into #accounts values ('1425004');
insert into #accounts values ('1631006');
insert into #accounts values ('1427006');
insert into #accounts values ('264610');
insert into #accounts values ('1435001');
insert into #accounts values ('265000');
insert into #accounts values ('245002');
insert into #accounts values ('265010');
insert into #accounts values ('263300');
insert into #accounts values ('243200');
insert into #accounts values ('1432006');
insert into #accounts values ('1400001');
insert into #accounts values ('1431012');
insert into #accounts values ('260500');
insert into #accounts values ('1407001');
insert into #accounts values ('145200');
insert into #accounts values ('261000');
insert into #accounts values ('1423012');
insert into #accounts values ('1403006');
insert into #accounts values ('265120');
insert into #accounts values ('1606004');
insert into #accounts values ('240810');
insert into #accounts values ('164400');
insert into #accounts values ('262900');
insert into #accounts values ('244601');
*/