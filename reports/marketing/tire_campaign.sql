/*
Hi Jon,
I was wondering if you could pull a list for me?
This would be for anyone that has purchased a car from us in the last 3 � 7 years, 
both new and used, and are we able to scrub that against a list of anyone that 
has purchased tires from us in the last 3 years.  We are going to send out an 
email about a tire rebate special GM is running and we don�t want to send to 
anyone that has purchased tires from us in the past 3 years.
Thank You,
Morgan
Morgan Mason
Marketing Director
*/

-- SELECT COUNT(*) FROM (
select c.lastname, c.firstname, c.middlename,
  CASE
    WHEN c.emailvalid THEN c.email
	ELSE c.email2
  END AS email
-- SELECT b.thedate, c.*
FROM factvehiclesale a
INNER JOIN day b on a.cappeddatekey = b.datekey
  AND b.thedate BETWEEN '09/01/2009' AND '09/01/2013'
INNER JOIN dimcustomer c on a.buyerkey = c.customerkey  
WHERE customertype = 'Person'
  AND hasvalidemail = true
  AND length(lastname) > 1
  AND firstname <> ''
  AND NOT EXISTS (
    --select e.thedate, c.*
    SELECT 1
    FROM factrepairorder c
    inner join dimopcode d on c.corcodekey = d.opcodekey
      AND d.opcode IN ('1nt','2nt','3nt','4nt','5nt','6nt')  
    INNER JOIN day e on c.finalclosedatekey = e.datekey
      AND e.thedate BETWEEN '09/01/2013' AND curdate()
    WHERE customerkey = a.buyerkey)  
GROUP BY c.lastname, c.firstname, c.middlename,
  CASE
    WHEN c.emailvalid THEN c.email
	ELSE c.email2
  END
) x  