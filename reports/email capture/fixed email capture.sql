
-- ro OPEN date
SELECT c.yearmonth, 
  sum(case when b.hasValidEmail = true THEN 1 ELSE 0 END) AS validEmail,
  COUNT(*) AS howmany
FROM (
  SELECT ro, opendatekey, customerkey  
  FROM factRepairOrder
  GROUP BY ro, opendatekey, customerkey) a 
INNER JOIN dimCustomer b on a.customerkey = b.customerkey
INNER JOIN day c on a.opendatekey = c.datekey
WHERE c.yearmonth > 201212
GROUP BY c.yearmonth

SELECT yearmonth, validEmail, howMany, round(100.0 * validEmail/howMany, 2)
FROM (
  SELECT c.yearmonth, 
    sum(case when b.hasValidEmail = true THEN 1 ELSE 0 END) AS validEmail,
    COUNT(*) AS howmany
  FROM (
    SELECT ro, opendatekey, customerkey  
    FROM factRepairOrder
    GROUP BY ro, opendatekey, customerkey) a 
  INNER JOIN dimCustomer b on a.customerkey = b.customerkey
  INNER JOIN day c on a.opendatekey = c.datekey
  WHERE c.yearmonth > 201212
  GROUP BY c.yearmonth) x 

-- narrow it down to service writer census dept
SELECT yearmonth, validEmail, howMany, round(100.0 * validEmail/howMany, 2)
FROM (
  SELECT c.yearmonth, 
    sum(case when b.hasValidEmail = true THEN 1 ELSE 0 END) AS validEmail,
    COUNT(*) AS howmany
  FROM (
    SELECT a.storecode, c.censusdept, ro, opendatekey, customerkey  
    FROM factRepairOrder a
    INNER JOIN dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
    WHERE c.censusdept IN ('BS','MR','QL')
    GROUP BY a.storecode, c.censusdept, ro, opendatekey, customerkey) a 
  INNER JOIN dimCustomer b on a.customerkey = b.customerkey
  INNER JOIN day c on a.opendatekey = c.datekey
  WHERE c.yearmonth > 201212
  GROUP BY c.yearmonth) x 

-- BY store, dept  
SELECT yearmonth, storecode, censusdept, validEmail, howMany, 
  round(1.00 * validEmail/howMany, 2) AS Percentage
FROM (
  SELECT c.yearmonth, a.storecode, a.censusdept,  
    sum(case when b.hasValidEmail = true THEN 1 ELSE 0 END) AS validEmail,
    COUNT(*) AS howmany
  FROM (
    SELECT a.storecode, c.censusdept, ro, opendatekey, customerkey  
    FROM factRepairOrder a
    INNER JOIN dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
    WHERE c.censusdept IN ('BS','MR','QL')
    GROUP BY a.storecode, c.censusdept, ro, opendatekey, customerkey) a 
  INNER JOIN dimCustomer b on a.customerkey = b.customerkey
  INNER JOIN day c on a.opendatekey = c.datekey
  WHERE c.yearmonth > 201212
  GROUP BY c.yearmonth, a.storecode, a.censusdept) x 
  
-- eliminate internal only work  
SELECT yearmonth, storecode, censusdept, validEmail, howMany, 
  round(1.00 * validEmail/howMany, 2) AS Percentage
FROM (
  SELECT c.yearmonth, a.storecode, a.censusdept,  
    sum(case when b.hasValidEmail = true THEN 1 ELSE 0 END) AS validEmail,
    COUNT(*) AS howmany
  FROM (
    SELECT a.storecode, c.censusdept, ro, opendatekey, customerkey  
    FROM factRepairOrder a
    INNER JOIN dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
    WHERE c.censusdept IN ('BS','MR','QL')
      AND EXISTS (
        SELECT 1
        FROM factRepairOrder
        WHERE ro = a.ro
          AND paymenttypekey IN (
            SELECT paymenttypekey
            FROM dimPaymentType
            WHERE paymenttypeCode IN ('C','S','W')))         
    GROUP BY a.storecode, c.censusdept, ro, opendatekey, customerkey) a 
  INNER JOIN dimCustomer b on a.customerkey = b.customerkey
  INNER JOIN day c on a.opendatekey = c.datekey
  WHERE c.yearmonth > 201212
  GROUP BY c.yearmonth, a.storecode, a.censusdept) x 
  
  
-- include the writer IN the breakdown
SELECT yearmonth, storecode, censusdept, name, validEmail, howMany, 
  round(1.00 * validEmail/howMany, 2) AS Percentage
FROM (
  SELECT c.yearmonth, a.storecode, a.censusdept, a.name, 
    sum(case when b.hasValidEmail = true THEN 1 ELSE 0 END) AS validEmail,
    COUNT(*) AS howmany
  FROM (
    SELECT a.storecode, c.censusdept, ro, opendatekey, customerkey, 
      c.writernumber, c.name
    FROM factRepairOrder a
    INNER JOIN dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
    WHERE c.censusdept IN ('BS','MR','QL')
      AND EXISTS (
        SELECT 1
        FROM factRepairOrder
        WHERE ro = a.ro
          AND paymenttypekey IN (
            SELECT paymenttypekey
            FROM dimPaymentType
            WHERE paymenttypeCode IN ('C','S','W')))         
    GROUP BY a.storecode, c.censusdept, ro, opendatekey, customerkey,
      c.writernumber, c.name) a 
  INNER JOIN dimCustomer b on a.customerkey = b.customerkey
  INNER JOIN day c on a.opendatekey = c.datekey
  WHERE c.yearmonth > 201305
  GROUP BY c.yearmonth, a.storecode, a.censusdept, name) x 
  
  
  -- non grouped raw data for intranet email capture app
SELECT ro, thedate, c.name, c.writernumber, d.fullName, d.HasValidEmail
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
INNER JOIN dimCustomer d on a.customerKey = d.CustomerKey
WHERE c.censusdept IN ('BS','MR','QL')
  AND b.thedate BETWEEN curdate() - 90 AND curdatE()
  AND EXISTS (
    SELECT 1
    FROM factRepairORder 
    WHERE ro = a.ro
      AND paymentTypeKey IN (
        SELECT paymentTypeKey
        FROM dimPAymentType
        WHERE paymentTypeCode IN ('C','S','W'))) 
GROUP BY ro, thedate, c.name, c.writernumber, d.fullName, d.HasValidEmail      

