SELECT dl2, al3, glaccount, b.gtjrnl, gtctl#, gtdoc#, gtdoc#, gtdesc, gttamt
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
WHERE name = 'gmfs expenses'
  AND al3 LIKE 'E-%'or al3 LIKE '%vertis%'
  AND glaccount <> 'NA'
  
  
SELECT dl2, al3, gtdesc, glaccount, SUM(gttamt)
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
WHERE name = 'gmfs expenses'
  AND al3 LIKE 'E-%'or al3 LIKE '%vertis%'
  AND glaccount <> 'NA'  
GROUP BY dl2, al3, gtdesc, glaccount 

SELECT dl2, al3, glaccount, SUM(gttamt), COUNT(*)
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
WHERE name = 'gmfs expenses'
  AND al3 LIKE 'E-%'or al3 LIKE '%vertis%'
  AND glaccount <> 'NA'  
GROUP BY dl2, al3, glaccount 

SELECT type, COUNT(*)
FROM allcontrols
GROUP BY type

SELECT type, COUNT(*)
FROM edwcontroldim
GROUP BY type

-- greg db2 expenditure report
Select trns.gtco# as Company, trns.gtacct as Account, mast.gmdesc as Description, cust.gcsnam as VendorName, name.bnadr1 as Address1, name.bnadr2 as Address2, name.bncity as City, name.bnstcd as State, name.bnzip as Zip, sum(trns.gttamt) as Amount
from rydedata.glptrns trns
  left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
  left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
  left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(gtvnd#) <> ''
  and gtdate between '2012-11-01' and '2012-11-30'
  and gtacct in (select gmacct from rydedata.glpmast where gmtype = '8')
  and not gcsnam is null
group by trns.gtco#, trns.gtacct, mast.gmdesc, cust.gcsnam, name.bnadr1, name.bnadr2, name.bncity, name.bnstcd, name.bnzip 
having sum(trns.gttamt) <> 0
order by cust.gcsnam



-- ok, these are the relevant accounts with expenditures
SELECT dl2, al3, gtdesc, glaccount, gtvnd#, SUM(gttamt)
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
WHERE name = 'gmfs expenses'
  AND al3 LIKE 'E-%'or al3 LIKE '%vertis%'
  AND glaccount <> 'NA'  
GROUP BY dl2, al3, gtdesc, gtvnd#, glaccount 

-- ok, these are the relevant accounts with expenditures
-- IN some greg shit
-- assumes a vendor value (gtvnd#) IN glptrns AND that IS NOT the case
SELECT dl2, al3, gtdesc, glaccount, SUM(gttamt), gmdesc
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
LEFT JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND c.gmyear = 2012  
WHERE name = 'gmfs expenses'
  AND al3 LIKE 'E-%'or al3 LIKE '%vertis%'
  AND glaccount <> 'NA'  
  AND gmdesc IS NOT NULL 
GROUP BY dl2, al3, gtdesc, glaccount, gmdesc 

-- dept & acct description
SELECT dl2,dl4,SUM(gttamt), gmdesc
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
LEFT JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND c.gmyear = 2012  
WHERE name = 'gmfs expenses'
  AND (al3 LIKE 'E-%'or al3 LIKE '%vertis%')
  AND glaccount <> 'NA'  
  AND gmdesc IS NOT NULL 
GROUP BY dl2,dl4,gmdesc 

-- what constitutes a vendor
-- let's narrow it down to ry1, sales, advertising new
SELECT dl2,dl4,al3, gmdesc, gtdesc, glaccount, SUM(gttamt)
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
LEFT JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND c.gmyear = 2012  
WHERE name = 'gmfs expenses'
  AND (al3 LIKE 'E-%'or al3 LIKE '%vertis%')
  AND glaccount <> 'NA'  
  AND gmdesc IS NOT NULL 
  AND dl2 = 'ry1'
  AND dl4 = 'sales'
GROUP BY dl2,dl4,al3,gmdesc, gtdesc, glaccount

-- what constitutes a vendor
-- let's narrow it down to ry1, sales, advertising new
-- ADD glpcust
SELECT dl2,dl4,al3, gmdesc, gtdesc, glaccount, gcsnam, SUM(gttamt)
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
LEFT JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND c.gmyear = 2012  
LEFT JOIN stgarkonaglpcust d ON dl2 = gcco# 
  AND b.gtctl# = d.gcvnd#  
WHERE name = 'gmfs expenses'
  AND (al3 LIKE 'E-%'or al3 LIKE '%vertis%')
  AND glaccount <> 'NA'  
  AND gmdesc IS NOT NULL 
  AND dl2 = 'ry1'
  AND dl4 = 'sales'
GROUP BY dl2,dl4,al3,gmdesc, gtdesc, glaccount, gcsnam

-- what constitutes a vendor
-- let's narrow it down to ry1, sales, advertising new
-- ADD glpcust, GROUP BY "vendor"
SELECT dl2,dl4,al3,
  CASE 
    WHEN gttamt = 4 THEN 'PDQ'
    ELSE coalesce(gcsnam, 'XXXXXX') 
  END AS gcsnam, gttamt--, SUM(gttamt)
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
LEFT JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND c.gmyear = 2012  
LEFT JOIN stgarkonaglpcust d ON dl2 = gcco# 
  AND b.gtctl# = d.gcvnd#  
WHERE name = 'gmfs expenses'
  AND al3 IN ('Advertising','E-Commerce Advertising/Fees','Advertising Rebates')
  AND glaccount <> 'NA'  
  AND gmdesc IS NOT NULL 
  AND dl2 = 'ry1'
ORDER BY gcsnam  
--GROUP BY dl2,dl4,al3,gcsnam

SELECT dl2,al3,
  CASE 
    WHEN gttamt = 4 THEN 'PDQ'
    ELSE coalesce(gcsnam, 'XXXXXX') 
  END AS gcsnam, SUM(gttamt)
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
LEFT JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND c.gmyear = 2012  
LEFT JOIN stgarkonaglpcust d ON dl2 = gcco# 
  AND b.gtctl# = d.gcvnd#  
WHERE name = 'gmfs expenses'
  AND al3 IN ('Advertising','E-Commerce Advertising/Fees','Advertising Rebates')
  AND glaccount <> 'NA'  
  AND gmdesc IS NOT NULL 
--  AND dl2 = 'ry1'
GROUP BY dl2,al3,gcsnam
ORDER BY dl2, al3, gcsnam

-- why no detail gcsnam for ry2,ry3
SELECT dl2,al3,
  CASE 
    WHEN gttamt = 4 THEN 'PDQ'
    ELSE coalesce(gcsnam, 'XXXXXX') 
  END AS gcsnam, SUM(gttamt)
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
LEFT JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND c.gmyear = 2012  
LEFT JOIN stgarkonaglpcust d ON  b.gtctl# = d.gcvnd#  
WHERE name = 'gmfs expenses'
  AND al3 IN ('Advertising','E-Commerce Advertising/Fees','Advertising Rebates')
  AND glaccount <> 'NA'  
  AND gmdesc IS NOT NULL 
--  AND dl2 = 'ry1'
GROUP BY dl2,al3,gcsnam
ORDER BY dl2, al3, gcsnam


-- so, what IS IN ALL the XXXXXXXXXXX

SELECT dl2,al3,
  CASE 
    WHEN gttamt = 4 THEN 'PDQ'
    ELSE coalesce(gcsnam, 'XXXXXX') 
  END AS gcsnam,  gttamt,
  gtdesc
FROM zcb a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
LEFT JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND c.gmyear = 2012  
LEFT JOIN stgarkonaglpcust d ON  b.gtctl# = d.gcvnd#  
WHERE name = 'gmfs expenses'
  AND al3 IN ('Advertising','E-Commerce Advertising/Fees','Advertising Rebates')
  AND glaccount <> 'NA'  
  AND gmdesc IS NOT NULL 
--  AND dl2 = 'ry1'
  AND gcsnam IS NULL
  AND gttamt <> 4

ORDER BY dl2, al3, gcsnam

-- RY3 Vendors
Select a.gcvnd# AS "Vendor #", a.gcsnam AS Name, min(gttamt) AS "Min Trans", max(gttamt) AS "Max Trans", 
  MIN(gtdate) AS "Min Trans Date", MAX(gtdate) AS "Max Trans Date", COUNT(*) AS "Trans Count"
from stgArkonaGLPCUST a
LEFT JOIN stgArkonaGLPTRNS b ON a.gcvnd# = b.gtctl#
  AND gtdate BETWEEN '11/01/2011' AND curdate()
where a.gcactive in ('V','B') 
  and left(trim(a.gcvnd#),1) = '3'
  AND b.gtctl# IS NOT NULL 
GROUP BY a.gcvnd#, a.gcsnam, gcvtyp


Select a.gcvnd#, a.gcsnam, gcvtyp, sum(gttamt), min(gttamt), max(gttamt), MIN(gtdate), MAX(gtdate), COUNT(*)
SELECT a.gcvnd#, gcsnam, b.*
from stgArkonaGLPCUST a
LEFT JOIN stgArkonaGLPTRNS b ON a.gcvnd# = b.gtctl#
  AND gtdate BETWEEN '11/01/2011' AND curdate()
where a.gcactive in ('V','B') 
  and trim(a.gcvnd#) = '3FACT001'
  AND b.gtctl# IS NOT NULL 

