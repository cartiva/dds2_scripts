SELECT * FROM edwEmployeeDim
WHERE (
   lastname LIKE 'olso%'
   OR lastname LIKE 'schw%'
   OR lastname LIKE 'smoo%'
   OR lastname LIKE 'boh%'
   OR lastname LIKE 'heff%')
 AND currentrow = true
 AND active = 'active'
 AND storecode = 'ry1'
 AND distcode = 'stec'
 
 
SELECT a.employeekey, a.employeenumber, a.name, a.hourlyrate, a.employeekeyfromdate, a.employeekeythrudate
FROM edwEmployeeDim a
WHERE a.employeenumber IN ('168400','1124436','116185','164015','1106399') 
ORDER BY employeenumber


SELECT yearmonthshort, name, sum(regularhours) as reghours, round(SUM(regularhours*hourlyrate),2) AS regpay, 
  sum(overtimehours) as othours, round(SUM(overtimehours*1.5*hourlyrate),2) AS otpay, 
  SUM(vacationhours + ptohours + holidayhours) as otherhours, round(SUM(vacationhours*hourlyrate + ptohours*hourlyrate + holidayhours*hourlyrate),2) AS otherpay,
  round(SUM(regularhours*hourlyrate) + SUM(overtimehours*1.5*hourlyrate) 
    + SUM(vacationhours + ptohours + holidayhours*hourlyrate),2) AS Totalpay
FROM (
  SELECT a.yearmonthshort, a.thedate, b.*, c.name, c.employeenumber, c.hourlyrate
  FROM day a
  INNER JOIN edwClockHoursFact b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on b.employeekey = c.employeekey
    AND a.thedate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
    AND c.employeenumber IN ('1124436','116185','164015','1106399') 
  WHERE a.yearmonth = 201308
  UNION 
  SELECT a.yearmonthshort, a.thedate, b.*, c.name, c.employeenumber, c.hourlyrate
  FROM day a
  INNER JOIN edwClockHoursFact b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on b.employeekey = c.employeekey
    AND a.thedate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
    AND c.employeenumber = '168400'
  WHERE a.yearmonth = 201307) x
GROUP BY yearmonthshort, name
UNION
SELECT '', 'TEAM', sum(regularhours) as reghours, round(SUM(regularhours*hourlyrate),2) AS regpay, 
  sum(overtimehours) as othours, round(SUM(overtimehours*1.5*hourlyrate),2) AS otpay, 
  SUM(vacationhours + ptohours + holidayhours) as otherhours, round(SUM(vacationhours*hourlyrate + ptohours*hourlyrate + holidayhours*hourlyrate),2) AS otherpay,
  round(SUM(regularhours*hourlyrate) + SUM(overtimehours*1.5*hourlyrate) 
    + SUM(vacationhours + ptohours + holidayhours*hourlyrate),2) AS Totalpay
FROM (
  SELECT a.yearmonthshort, a.thedate, b.*, c.name, c.employeenumber, c.hourlyrate
  FROM day a
  INNER JOIN edwClockHoursFact b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on b.employeekey = c.employeekey
    AND a.thedate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
    AND c.employeenumber IN ('1124436','116185','164015','1106399') 
  WHERE a.yearmonth = 201308
  UNION 
  SELECT a.yearmonthshort, a.thedate, b.*, c.name, c.employeenumber, c.hourlyrate
  FROM day a
  INNER JOIN edwClockHoursFact b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on b.employeekey = c.employeekey
    AND a.thedate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
    AND c.employeenumber = '168400'
  WHERE a.yearmonth = 201307) x