
-- policy accounts
SELECT *
FROM zcb
WHERE gmcoa = '067'
  AND name = 'cb2'
  AND dl4 = 'mechanical'
  AND dl2 = 'ry1'
  
SELECT a.thedate, c.dl5, c.glaccount, b.gtdesc, d.gmdesc, b.gttamt, b.*
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN (
  SELECT *
  FROM zcb
  WHERE gmcoa = '067
    AND name = 'cb2'
    AND dl4 = 'mechanical'
    AND dl2 = 'ry1') c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON b.gtacct = d.gmacct
  AND d.gmyear = 2012    
WHERE a.yearmonth = 201210  

SELECT dl5, gmdesc, SUM(gttamt)
FROM (  
  SELECT a.thedate, c.dl5, c.glaccount, b.gtdesc, d.gmdesc, b.gttamt
  FROM day a
  INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
  INNER JOIN (
    SELECT *
    FROM zcb
    WHERE gmcoa = '067'
      AND name = 'cb2'
      AND dl4 = 'mechanical'
      AND dl2 = 'ry1') c ON b.gtacct = c.glaccount
  LEFT JOIN stgArkonaGLPMAST d ON b.gtacct = d.gmacct
    AND d.gmyear = 2012    
  WHERE a.yearmonth = 201210) x
GROUP BY dl5, gmdesc  