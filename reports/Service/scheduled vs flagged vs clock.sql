SELECT dayname(appointmentts), cast(appointmentts as sql_date), SUM(heavyhours) AS heavy, 
  SUM(mainthours) AS maint, round(SUM(driveHours), 1) AS drive
FROM stgRydellServiceAppointments a
WHERE cast(appointmentts as sql_date) BETWEEN '12/01/2012' AND curdate()
GROUP BY dayname(appointmentts),cast(appointmentts as sql_date)
ORDER BY cast(appointmentts as sql_date)

SELECT * 
FROM stgRydellServiceAppointments
--WHERE LEFT(ro,2) = '16'
ORDER BY ro, appointmentts

SELECT a.thedate, b.ro, SUM(lineflaghours)
FROM day a
LEFT JOIN factroline b ON a.datekey = b.linedatekey
WHERE a.yearmonth = 201212
GROUP BY a.thedate, b.ro

SELECT advisorstatus, COUNT(*)
FROM stgRydellServiceAppointments
GROUP BY advisorstatus

SELECT thedate, SUM(clock-training-other) AS clock, SUM(flag) AS flag,
  round(100 * SUM(flag)/SUM(clock-training-other),0)
FROM rptRY1TechProd
GROUP BY thedate

SELECT 
FROM factro a
LEFT JOIN day b ON 


SELECT dl2, al2, al3, line, SUM(gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
WHERE name = 'gmfs expenses'
  AND dl2 = 'ry3'
GROUP BY dl2, al2, al3, line
ORDER BY line asc, al2 desc




SELECT dl2, dl4, al4, SUM(gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
WHERE name = 'cb2'
  AND dl4 = 'mechanical'
GROUP BY dl2, dl4, al4

-- why is sales 150k hi at ry1

SELECT dl2, dl4, al4, al5, line, SUM(gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
WHERE name = 'cb2'
  AND dl4 = 'mechanical'
  AND dl2 = 'ry1'
  AND al4 = 'sales'
GROUP BY dl2, dl4, al4, al5, line
ORDER BY line

SELECT dl2, dl4, al4, al5, line, SUM(gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '11/01/2012' AND '11/30/2012'
WHERE name = 'cb2'
  AND dl4 = 'mechanical'
  AND al4 = 'sales'
  AND dl2 = 'ry1'
GROUP BY dl2, dl4, al4, al5, line
ORDER BY line



select count(*) as ROs, 
  case PayTypeNum
    when    0 then 'N' 
    when    1 then 'W' 
    when   10 then 'S' 
    when   11 then 'SW' 
    when  100 then 'I' 
    when  101 then 'IW' 
    when  110 then 'IS'
    when  111 then 'ISW' 
    when 1000 then 'C' 
    when 1001 then 'CW' 
    when 1010 then 'CS' 
    when 1011 then 'CSW' 
    when 1100 then 'CI' 
    when 1101 then 'CIW' 
    when 1110 then 'CIS' 
    when 1111 then 'CISW' else 'X' 
  end as PayTypes
from (
  select RO, 
    sum(case PayType when 'C' then 1000 when 'I' then 100 when 'S' then 10 when 'W' then 1 else 0 end) as PayTypeNum
  from (
    select RO, PayType
    from factROLine
    group by RO, PayType) a
  group by RO) b
group by PayTypes
order by ROs desc

SELECT * FROM factroline

SELECT DISTINCT servicetype FROM factroline

SELECT servicetype, paytype, COUNT(*)
FROM factroline
GROUP BY servicetype, paytype

SELECT ro, COUNT(*)
FROM (
  SELECT ro, servicetype
  FROM factroline
  GROUP BY ro, servicetype) a
GROUP BY ro
HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC 


SELECT ro, servicetype, sum(lineflaghours), COUNT(*)
FROM factroline
GROUP BY ro, servicetype



-- flag hours BY servicetyp

SELECT *
FROM factroline



-- 12/13/12
SELECT dayname(appointmentts), cast(appointmentts as sql_date), SUM(heavyhours) AS heavy, 
  SUM(mainthours) AS maint, round(SUM(driveHours), 1) AS drive
FROM stgRydellServiceAppointments a
WHERE cast(appointmentts as sql_date) BETWEEN '11/01/2012' AND curdate()
GROUP BY dayname(appointmentts),cast(appointmentts as sql_date)
ORDER BY cast(appointmentts as sql_date)


SELECT a.thedate, a.theday, a.heavy, a.maint, a.drive, a.heavy+a.maint+a.drive AS Total,
  b.flag, b.wrench
FROM (
SELECT dayname(appointmentts) theday, cast(appointmentts as sql_date) AS thedate, SUM(heavyhours) AS heavy, 
  SUM(mainthours) AS maint, round(SUM(driveHours), 1) AS drive
FROM stgRydellServiceAppointments a
WHERE cast(appointmentts as sql_date) BETWEEN '11/01/2012' AND curdate()
GROUP BY dayname(appointmentts),cast(appointmentts as sql_date)) a
LEFT JOIN (
  SELECT thedate, SUM(flag) AS flag, SUM(clock-training-other) AS wrench
  FROM rptRY1TechProd
  GROUP BY thedate) b ON a.thedate = b.thedate
WHERE wrench <> 0  
ORDER BY a.thedate  


SELECT * FROM factro
SELECT * FROM factroline
SELECT DISTINCT servicetype FROM factroline

SELECT a.thedate, dayname(a.thedate)
FROM day a
WHERE a.thedate BETWEEN '11/01/2012' AND curdate()


SELECT a.ro, b.thedate AS OpenDate, c.thedate AS CloseDate, d.thedate AS FinalClose,
  coalesce(e.flaghours, 0) AS TotalFlagHours, f.*
FROM factro a
LEFT JOIN day b ON a.opendatekey = b.datekey
LEFT JOIN day c ON a.closedatekey = c.datekey
LEFT JOIN day d ON a.finalclosedatekey = d.datekey 
LEFT JOIN (
  SELECT ro, SUM(lineflaghours) AS flaghours
  FROM factroline
  GROUP BY ro) e ON a.ro = e.ro
LEFT JOIN ( 
  SELECT ro,
    SUM(CASE WHEN servicetype = 'AM' THEN LineFlagHours ELSE 0 END) AS "AM Flag Hours",
    SUM(CASE WHEN servicetype = 'BS' THEN LineFlagHours ELSE 0 END) AS "BS Flag Hours",
    SUM(CASE WHEN servicetype = 'CM' THEN LineFlagHours ELSE 0 END) AS "CM Flag Hours",
    SUM(CASE WHEN servicetype = 'CW' THEN LineFlagHours ELSE 0 END) AS "CW Flag Hours",
    SUM(CASE WHEN servicetype = 'EM' THEN LineFlagHours ELSE 0 END) AS "EM Flag Hours",
    SUM(CASE WHEN servicetype = 'MR' THEN LineFlagHours ELSE 0 END) AS "MR Flag Hours",
    SUM(CASE WHEN servicetype = 'QL' THEN LineFlagHours ELSE 0 END) AS "QL Flag Hours",
    SUM(CASE WHEN servicetype = 'RE' THEN LineFlagHours ELSE 0 END) AS "RE Flag Hours"
  FROM factroline 
  GROUP BY ro) f ON a.ro = f.ro 
WHERE b.thedate BETWEEN '10/01/2012' AND curdate()
  AND coalesce(e.flaghours, 0) <> 0 


SELECT a.ro, b.thedate AS OpenDate, c.thedate AS CloseDate, d.thedate AS FinalClose
FROM factro a
LEFT JOIN day b ON a.opendatekey = b.datekey
LEFT JOIN day c ON a.closedatekey = c.datekey
LEFT JOIN day d ON a.finalclosedatekey = d.datekey 


-- detail
SELECT thedate, 
  SUM(CASE WHEN expr = 'clock' THEN flaghours ELSE 0 END) AS clock,
  SUM(CASE WHEN expr = 'flag' THEN flaghours ELSE 0 END) AS flag
FROM (  
SELECT a.*, 'flag'
FROM rptFlagHours a
where a.thedate BETWEEN '12/01/2012' AND curdate()
  AND a.technumber IN ('d06','d11')
UNION all  
SELECT b.*, 'clock'
FROM rptClockHours b
where b.thedate BETWEEN '12/01/2012' AND curdate()
  AND b.technumber IN ('d06','d11')) x
GROUP BY thedate

-- greg's request FROM after the pre-planning meeting meeting
gross, elr, etc 
based ON the techs, ie, main shop are mikes techs, pdq, etc

so, the notion IS anyone, NOT anyone, but any tech#,  with flagged hours IS the determining factor

SELECT storecode, pydept, 
FROM rptFlagHours

SELECT distcode, distribution, pydept, COUNT(*)
FROM edwEmployeeDim
WHERE active = 'active'
  AND currentrow = true
GROUP BY distcode, distribution, pydept

-- look at tech 621: Recon Tech3
SELECT *
FROM rptflaghours
WHERE employeekey = -1
  AND thedate > '01/01/2011'
  
SELECT year(thedate), month(thedate), technumber, SUM(flaghours)
FROM rptflaghours
WHERE employeekey = -1
  AND thedate > '01/01/2011'
GROUP BY year(thedate), month(thedate), technumber  

SELECT *
FROM bridgetechgroup a
INNER JOIN dimtech b ON a.techkey = b.techkey
  AND b.employeenumber IS NULL 
  
SELECT a.storecode, a.ro, a.line, a.flaghours, weightfactor, c.name, c.description, 
  d.pydept, d.distcode, d.distribution
FROM factTechLineFlagDateHours a
INNER JOIN bridgetechgroup b ON a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c ON b.techkey = c.techkey 
  AND c.employeenumber IS NULL 
LEFT JOIN (
  SELECT storecode, employeekey, name, pydept, distcode, distribution, technumber
  FROM rptFlagHours 
  GROUP BY storecode, employeekey, name, pydept, distcode, distribution, technumber) d ON a.storecode = d.storecode
    AND c.technumber = d.technumber
  
-- so WHERE the fuck DO i store dept: rptFlagHours
-- IS dept good enuf for grouping the diff fixed sub departments

-- 12/16
--flag hours be dept
-- BY month, BY payperiod
-- 1. empkey = -1: techs IN dimTech DO NOT have a dept

right, that was what i was wondering
GROUP BY pydept OR BY distcode
start with dept
so, first have to assign pydept
dimTech does NOT have a dept COLUMN
AND that will certainly be a type2 attribute
SELECT * FROM dimtech
this ALL sems to matter because greg said we determine flagged hours for a dept
BY the total of flagged hours BY techs assigned to that dept

SELECT storecode, pydept, pydeptcode, distcode, distribution, technumber
FROM rptFlagHours
GROUP BY storecode, pydept, pydeptcode, distcode, distribution, technumber


SELECT *
FROM dimTech a
LEFT JOIN (
  SELECT storecode, pydept, pydeptcode, distcode, distribution, technumber
  FROM rptFlagHours
  WHERE employeekey <> -1
  GROUP BY storecode, pydept, pydeptcode, distcode, distribution, technumber) b ON a.storecode = b.storecode
    AND a.technumber = b.technumber
ORDER BY name    
    
ok, here IS  the deal, in rptFlagHours, ry1 tech 627 has hours IN dept PDQ AND hours IN Service
LIKE i said before, dept becomes a type 2 attribute of dimtech
AND i can NOT even automate fucking adding a new tech  
    
    
SELECT *
FROM (
  SELECT storecode, pydept, pydeptcode, distcode, distribution, technumber
  FROM rptFlagHours
  GROUP BY storecode, pydept, pydeptcode, distcode, distribution, technumber) b     
WHERE b.storecode = 'ry2' AND b.technumber = '22'


SELECT name, pydept, distcode, MIN(thedate), MAX(thedate)
FROM rptFlagHours
WHERE storecode = 'ry1' AND technumber = '627'
GROUP BY name, pydept, distcode


SELECT storecode, technumber, MIN(thedate), MAX(thedate), COUNT(*)
FROM rptFlagHours
WHERE employeekey = -1
  AND thedate > '01/01/2012'
GROUP BY storecode, technumber
HAVING max(thedate) > '01/01/2012'

-- 12/17
-- thinking, AS fucking usual, short cut, limit the history
-- AND dub IN depts for non human techs
-- but DO it IN rptFlaghours OR IN dimTech

-- unhelpful attempt to determine IF there were ever any departments that EXISTS
-- IN glpmast that are currently NOT IN gldept
-- ALL this turns up are accounts IN glpmast without a dept
SELECT *
FROM stgarkonaglpmast a
LEFT JOIN stgArkonaGLPDEPT b ON a.gmco# = b.gdco#
  AND a.gmdept = b.gddept
WHERE b.gddept IS NULL   

SELECT a.gmacct, a.gmdesc, a.gmdept, c.*
FROM stgarkonaglpmast a
LEFT JOIN stgArkonaGLPDEPT b ON a.gmco# = b.gdco#
  AND a.gmdept = b.gddept
LEFT JOIN stgArkonaGLPTRNS c ON a.gmacct = c.gtacct
  AND c.gtdate > '01/01/2012'
WHERE b.gddept IS NULL  
  AND a.gmyear = 2012

SELECT MIN(b.thedate), MAX(thedate)
FROM factroline a
LEFT JOIN day b ON a.linedatekey = b.datekey
WHERE opcode = 'pic'

-- 12/18
-- fuck it, go with rptFlagHours, only need to UPDATE the empkey records

-- ok, here are the departmental tech #s since 1/1/11
SELECT storecode, technumber, MIN(thedate), MAX(thedate), COUNT(*)
FROM rptFlagHours
WHERE employeekey = -1
  AND thedate > '01/01/2011'
GROUP BY storecode, technumber
HAVING max(thedate) > '01/01/2011'

--what kind of WORK did they DO:
SELECT a.storecode, a.technumber, a.ro
FROM rptFlagHours a
LEFT JOIN dimtechgroup b ON a.techgroupkey = b.techgroupkey

WHERE a.employeekey = -1
  AND a.thedate > '01/01/2011'
  
-- fuck why am i making this so hard
-- how many ry1 tech 103 flagged what kind of WORK

SELECT * 
FROM factTechLineFlagDateHours a
LEFT JOIN bridgetechgroup b ON a.techgroupkey = b.techgroupkey
LEFT JOIN factRoLine c ON a.storecode = c.storecode
  AND a.ro = c.ro
  AND a.line = c.line
LEFT JOIN dimtech d ON b.techkey = d.techkey  
INNER JOIN (
  SELECT storecode, technumber
  FROM rptFlagHours
  WHERE employeekey = -1
    AND thedate > '01/01/2011'
  GROUP BY storecode, technumber
  HAVING max(thedate) > '01/01/2011') e ON a.storecode = e.storecode
    AND d.technumber = e.technumber 
WHERE flagdatekey IN (
  SELECT datekey
  FROM day
  WHERE thedate = '12/17/2012')
ORDER BY a.ro  
-- well, that didn't WORK, returns ro 16105185 line 3 tech 621 emp# 119519 Ted Brown Recon Tech3
-- think the problem must be IN the list of relevant tech numbers
-- ok, here are the departmental tech #s since 1/1/11
SELECT storecode, technumber, MIN(thedate), MAX(thedate), COUNT(*)
FROM rptFlagHours
WHERE employeekey = -1
  AND thedate > '01/01/2011'
GROUP BY storecode, technumber
HAVING max(thedate) > '01/01/2011'

SELECT * FROM rptflaghours WHERE employeekey <> -1 AND technumber = '621'
-- looks LIKE problem with the generation of rptFlagHours

-- oh shit,    

SELECT a.storecode, a.technumber, a.employeenumber, b.pydept, MIN(thedate), MAX(thedate), COUNT(*)
FROM (
  SELECT storecode, technumber, coalesce(employeenumber, '000') AS employeenumber
  FROM rptflaghours
--  WHERE thedate > '01/01/2011'
  GROUP BY storecode, technumber, coalesce(employeenumber, '000')) a
LEFT JOIN rptFlagHours b ON a.storecode = b.storecode
  AND a.technumber = b.technumber
  AND a.employeenumber = coalesce(b.employeenumber, '000')
GROUP BY a.storecode, a.technumber, a.employeenumber, b.pydept  

-- techs BY dept based ON rptFlagHours  
SELECT storecode, pydept, COUNT(*)
FROM (
  SELECT a.storecode, a.technumber, a.employeenumber, b.pydept, MIN(thedate), MAX(thedate), COUNT(*)
  FROM (
    SELECT storecode, technumber, coalesce(employeenumber, '000') AS employeenumber
    FROM rptflaghours
    WHERE thedate > '01/01/2011'
    GROUP BY storecode, technumber, coalesce(employeenumber, '000')) a
  LEFT JOIN rptFlagHours b ON a.storecode = b.storecode
    AND a.technumber = b.technumber
    AND a.employeenumber = coalesce(b.employeenumber, '000')
  GROUP BY a.storecode, a.technumber, a.employeenumber, b.pydept) x    
GROUP BY storecode, pydept  
  
-- so why doesn't this include 621
-- because i am basing it ON dimtech IN which, 621 IS defined
SELECT a.storecode, d.technumber, d.description, e.servicetype, f.stdftsvct, MIN(thedate), MAX(thedate), COUNT(*)
FROM factTechLineFlagDateHours a
INNER JOIN day b ON a.flagdatekey = b.datekey
LEFT JOIN bridgetechgroup c ON a.techgroupkey = c.techgroupkey
LEFT JOIN dimtech d ON c.techkey = d.techkey
LEFT JOIN factroline e ON a.ro = e.ro
  AND a.line = e.line
LEFT JOIN stgArkonaSDPTECH f ON a.storecode = f.stco#
  AND d.technumber = f.sttech  
WHERE b.thedate > '01/01/2011'
  AND d.employeenumber IS NULL   
GROUP BY a.storecode, d.technumber, d.description, e.servicetype, f.stdftsvct  

-- goddamnit,  give me list of ro, line, servicetype, tech(s)

-- 12/19 
-- so why doesn't this include 621
-- because i am basing it ON dimtech IN which, 621 IS defined
-- AS opposed to basing it ON rptFlagHours, which derives dept FROM 
-- the current state of employment

-- 621 assigned to Ted Brown, but also used outside the range of Ted Brown's 
-- employment
-- so, 2 instances of 621, one AS employee one AS role
-- OR should 621 never be assigned to a specific employee

-- i don't want to answer these fucking unanswerable questions right now, too
-- entwined with administrative control: payroll NOT acccurate to the day,
-- who cruds techs IN the system, who decides to use the same tech # for multiple
-- situations -- i don' fucking want to care about that shit now

-- ALL i want to know IS, IF a tech# flags hours, for which dept are those hours flagged?

-- for ALL techs
SELECT a.storecode, d.technumber, d.description, e.servicetype, f.stdftsvct, MIN(thedate), MAX(thedate), COUNT(*)
FROM factTechLineFlagDateHours a
INNER JOIN day b ON a.flagdatekey = b.datekey
LEFT JOIN bridgetechgroup c ON a.techgroupkey = c.techgroupkey
LEFT JOIN dimtech d ON c.techkey = d.techkey
LEFT JOIN factroline e ON a.ro = e.ro
  AND a.line = e.line
LEFT JOIN stgArkonaSDPTECH f ON a.storecode = f.stco#
  AND d.technumber = f.sttech  
WHERE b.thedate > '01/01/2011'
--  AND d.employeenumber IS NULL   
GROUP BY a.storecode, d.technumber, d.description, e.servicetype, f.stdftsvct

-- detail
AS of 9/4/12 we have the D tech numbers,
how was it flagged before
SELECT * FROM stgARkonaSDPRDET
--WHERE ptltyp = 'l'
--  AND ptcode = 'tt'
WHERE ptlhrs <> 0
  AND ptdate BETWEEN '01/01/2012' AND '08/31/2012'
  AND ptlopc IN ('EXT','NDEL','PREP','TDW')
ORDER BY ptro#, ptline  
  
  
SELECT DISTINCT ptlopc FROM stgARkonaSDPRDET 
WHERE ptro# in (
  select ptro#
  from stgArkonaSDPRDET
  where pttech LIKE 'd%')  
  
what are the opcodes under servicetype RE  

-- wtf, distracted BY my own stupidity, of course detail ro's have tech #s
SELECT * FROM stgARkonaSDPRDET WHERE ptro# = '16078109'

every ro with flag hours has a tech
a new rptFlagHours that includes dept
we DO NOT fucking DO ros for car washes
so the depts are main shop, pdq, detail, body shop

SELECT * FROM stgArkonaSDPLOPC WHERE sodes1 LIKE '%wash%'

SELECT * FROM factroline WHERE opcode LIKE 'cw%' AND lineflaghours <> 0

so the depts are main shop, pdq, detail, body shop

date dept flaghours

-- hmmm, factTechflagHoursByDay
SELECT b.thedate, a.*, c.storecode, c.employeenumber, c.name, c.description, c.technumber
FROM factTechFlagHoursByDay a
INNER JOIN day b ON a.flagdatekey = b.datekey
LEFT JOIN dimTech c ON a.techkey = c.techkey

/*
snipped out the WORK i did ON generating dimtech.flagdept
see etl-dimtechFlagDept
*/


SELECT *
INTO #wtf
FROM (
  SELECT c.storecode, b.thedate, b.dayname, 
    SUM(CASE WHEN flagdeptcode = 'SD' THEN a.flaghours ELSE 0 END) AS "SD Flagged",
    SUM(CASE WHEN flagdeptcode = 'BS' THEN a.flaghours ELSE 0 END) AS "BS Flagged",
    SUM(CASE WHEN flagdeptcode = 'QL' THEN a.flaghours ELSE 0 END) AS "QL Flagged",
    SUM(CASE WHEN flagdeptcode = 'RE' THEN a.flaghours ELSE 0 END) AS "RE Flagged"
  FROM day b  
  INNER JOIN factTechFlagHoursByDay a ON b.datekey = a.flagdatekey
  INNER JOIN dimtech c ON a.techkey = c.techkey
  WHERE b.thedate BETWEEN '01/01/2011' AND curdate()
  GROUP BY c.storecode, b.thedate, b.dayname) x
LEFT JOIN (
  SELECT b.gtdate, 
    round(SUM(CASE WHEN dl5 = 'Main Shop' then gttamt ELSE 0 END),0) AS "SD Sales",
    round(SUM(CASE WHEN dl5 = 'Body Shop' then gttamt ELSE 0 END),0) AS "BS Sales",
    round(SUM(CASE WHEN dl5 = 'PDQ' then gttamt ELSE 0 END),0) AS "QL Sales",
    round(SUM(CASE WHEN dl5 = 'Detail' then gttamt ELSE 0 END),0) AS "RE Sales"
  FROM day a  
  INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate 
  INNER JOIN zcb c ON b.gtacct = c.glaccount
  WHERE a.thedate BETWEEN '01/01/2011' AND curdate()
    AND name = 'cb2'
    AND dl3 = 'fixed'
    AND al4 = 'sales'
    AND dl4 <> 'parts'
  GROUP BY b.gtdate) y ON x.thedate = y.gtdate
  
SELECT * FROM #wtf  

SELECT *
FROM zcb
WHERE name = 'cb2'
    AND dl3 = 'fixed'
    AND al4 = 'sales'
    AND dl4 <> 'parts'
  

DROP TABLE #wtf;
SELECT *
--INTO #wtf
FROM (
  SELECT c.storecode, b.yearmonth,
    SUM(CASE WHEN flagdeptcode = 'SD' THEN a.flaghours ELSE 0 END) AS "SD Flagged",
    SUM(CASE WHEN flagdeptcode = 'BS' THEN a.flaghours ELSE 0 END) AS "BS Flagged",
    SUM(CASE WHEN flagdeptcode = 'QL' THEN a.flaghours ELSE 0 END) AS "QL Flagged",
    SUM(CASE WHEN flagdeptcode = 'RE' THEN a.flaghours ELSE 0 END) AS "RE Flagged"
  FROM day b  
  INNER JOIN factTechFlagHoursByDay a ON b.datekey = a.flagdatekey
  INNER JOIN dimtech c ON a.techkey = c.techkey
  WHERE b.thedate BETWEEN '01/01/2011' AND curdate()
  GROUP BY c.storecode, b.yearmonth) x
LEFT JOIN (
  SELECT c.dl2, a.yearmonth, 
    round(SUM(CASE WHEN dl5 = 'Main Shop' then gttamt*split ELSE 0 END),0) AS "SD Sales",
    round(SUM(CASE WHEN dl5 = 'Body Shop' then gttamt*split ELSE 0 END),0) AS "BS Sales",
    round(SUM(CASE WHEN dl5 = 'PDQ' then gttamt*split ELSE 0 END),0) AS "QL Sales",
    round(SUM(CASE WHEN dl5 = 'Detail' then gttamt*split ELSE 0 END),0) AS "RE Sales"
  FROM day a  
  INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate 
  INNER JOIN zcb c ON b.gtacct = c.glaccount
  WHERE a.thedate BETWEEN '01/01/2011' AND curdate()
    AND name = 'cb2'
    AND dl3 = 'fixed'
    AND al4 = 'sales'
    AND dl4 <> 'parts'
  GROUP BY c.dl2, a.yearmonth) y ON x.yearmonth = y.yearmonth
    AND x.storecode = y.dl2
    
SELECT * FROM #wtf WHERE storecode = 'ry1'  


  SELECT c.dl2, al3, a.yearmonth, 
    round(SUM(CASE WHEN dl5 = 'Main Shop' then gttamt*split ELSE 0 END),0) AS "SD Sales",
    round(SUM(CASE WHEN dl5 = 'Body Shop' then gttamt*split ELSE 0 END),0) AS "BS Sales",
    round(SUM(CASE WHEN dl5 = 'PDQ' then gttamt*split ELSE 0 END),0) AS "QL Sales",
    round(SUM(CASE WHEN dl5 = 'Detail' then gttamt*split ELSE 0 END),0) AS "RE Sales"
  FROM day a  
  INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate 
  INNER JOIN zcb c ON b.gtacct = c.glaccount
  WHERE a.thedate BETWEEN '01/01/2011' AND curdate()
    AND name = 'cb2'
    AND dl3 = 'fixed'
    AND al3 = 'gross profit'
--    AND dl4 <> 'parts'
  GROUP BY c.dl2, al3, a.yearmonth
  
  
SELECT al5, glaccount, gmcoa, split, b.*
FROM zcb a
LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
WHERE name = 'cb2'
    AND dl3 = 'fixed'
    AND al4 = 'sales'
    AND dl4 <> 'parts'
    
-- 12/30
WHERE i LEFT off
dept IN dim tech for flag hours
labor sales needs to come FROM glptrns
effective labor rate 30 day rolling avg
  need to segregate out labor accounts    
  coa type of sales includes shop supplies, parts, etc
  nothing IN glpmast will separate out labor only accounts
-- what about IN glptrns  
-- was hoping journal might be some help
SELECT al5, glaccount, gmcoa, split, b.gtjrnl, SUM(gttamt), COUNT(*)
FROM zcb a
LEFT JOIN (
  SELECT gtacct, gtjrnl, SUM(gttamt) AS gttamt
  FROM stgArkonaGLPTRNS
  WHERE gtdate BETWEEN '12/01/2012' AND '12/15/2012'
  GROUP BY gtacct, gtjrnl) b ON a.glaccount = b.gtacct
WHERE name = 'cb2'
    AND dl3 = 'fixed'
    AND al4 = 'sales'
    AND dl4 <> 'parts'
GROUP BY al5, glaccount, gmcoa, split, b.gtjrnl
-- shit, leaving labor sales for now
-- back to flag hour
SELECT a.thedate, b.*, c.*
FROM day a
INNER JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
INNER JOIN dimtech c ON b.techkey = c.techkey
WHERE a.thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND c.flagdeptcode = 'sd'
  AND c.storecode = 'ry1'
-- compare this to labor profit analysis IN arkona
-- NOT even close
SELECT technumber, SUM(flaghours)
FROM day a
INNER JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
INNER JOIN dimtech c ON b.techkey = c.techkey
WHERE a.thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND c.flagdeptcode = 'sd'
  AND c.storecode = 'ry1'  
GROUP BY technumber  
-- so break it down
SELECT a.thedate, b.*, c.*
--SELECT SUM(flaghours)
FROM day a
INNER JOIN factTechLineFlagDateHours b ON a.datekey = b.flagdatekey
INNER JOIN bridgeTechGroup c ON b.techgroupkey = c.techgroupkey
INNER JOIN dimtech d ON c.techkey = d.techkey
WHERE a.thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND d.technumber = '502'
ORDER BY ro, line  

SELECT ro, SUM(flaghours)
--SELECT SUM(flaghours)
FROM day a
INNER JOIN factTechLineFlagDateHours b ON a.datekey = b.flagdatekey
INNER JOIN bridgeTechGroup c ON b.techgroupkey = c.techgroupkey
INNER JOIN dimtech d ON c.techkey = d.techkey
WHERE a.thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND d.technumber = '502'
GROUP BY ro

-- factTechFHbyDay IS short, factTechLFDHours IS 1 hour high(query correct, arkona wrong)
-- so what's fucked up about fhbyday?


SELECT a.thedate, b.*
INTO #wtf1
FROM day a
INNER JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
INNER JOIN dimtech c ON b.techkey = c.techkey
WHERE a.thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND c.flagdeptcode = 'sd'
  AND c.storecode = 'ry1'  
  AND c.technumber = '502'
ORDER BY thedate;  
-- DROP TABLE #wtf2
SELECT a.thedate, ro, SUM(flaghours)
INTO #wtf2
FROM day a
INNER JOIN factTechLineFlagDateHours b ON a.datekey = b.flagdatekey
INNER JOIN bridgeTechGroup c ON b.techgroupkey = c.techgroupkey
INNER JOIN dimtech d ON c.techkey = d.techkey
WHERE a.thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND d.technumber = '502'
GROUP BY thedate, ro;  

SELECT *
FROM #wtf1 a
FULL OUTER JOIN #wtf2 b ON a.thedate = b.thedate


-- key issue why IS FHbyDay wrong
-- yikes, rptFlagHours IS wrong too, but of course, it IS based ON fhByDay
SELECT *
FROM rptFlagHours
WHERE thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND technumber = '502'
-- of course, this IS wrong too  
SELECT SUM(flag)
FROM rptry1techprod
WHERE thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND technumber = '502'
  
TechLineFDHours seems ok, should that be the basis for factTechFlagHoursByDay?

SELECT d.thedate, a.*, b.*, c.storecode, c.technumber, c.techkey, c.flagdeptcode, c.flagdept
-- SELECT SUM(flaghours) -- yep, this IS ok
FROM factTechLineFlagDateHours a
INNER JOIN bridgetechgroup b ON a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c ON b.techkey = c.techkey
INNER JOIN day d ON a.flagdatekey = d.datekey
WHERE thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND technumber = '502'
-- generate flaghoursbyday FROM factTechLineFlagDateHours
SELECT d.thedate, d.datekey, c.techkey, c.technumber, round(SUM(a.flaghours),2)
FROM factTechLineFlagDateHours a
INNER JOIN bridgetechgroup b ON a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c ON b.techkey = c.techkey
INNER JOIN day d ON a.flagdatekey = d.datekey
GROUP BY d.thedate, d.datekey, c.techkey, c.technumber

-- check against arkona
-- i am guessing my numbers aren't exact because i am using flagdate consistently
SELECT c.technumber, round(SUM(a.flaghours),2)
FROM factTechLineFlagDateHours a
INNER JOIN bridgetechgroup b ON a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c ON b.techkey = c.techkey
INNER JOIN day d ON a.flagdatekey = d.datekey
WHERE thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND technumber IN ('502','506','511','519')
GROUP BY c.technumber

-- looks LIKE my query IS leaving out 16100455 with 1 flag hour 11/2
-- nope, my query IS correct those flag hours are for tech 631
-- ro 16101204 arkona shows 1.8, everything ELSE shows 1.5, guessing flag hours were changed
-- arkona includes 16101207: hours flagged 10/27, ro closed 11/15
-- arkona excludes 16101893, don't know why, ro shows 1 hr ON 11/2 for 506
SELECT ro, round(SUM(flaghours),2)
FROM factTechLineFlagDateHours a
INNER JOIN bridgetechgroup b ON a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c ON b.techkey = c.techkey
INNER JOIN day d ON a.flagdatekey = d.datekey
WHERE thedate BETWEEN '11/01/2012' AND '11/15/2012'
  AND technumber = '506'
GROUP BY ro
ORDER BY ro

SELECT d.thedate, a.*, c.technumber 
FROM factTechLineFlagDateHours a
INNER JOIN bridgetechgroup b ON a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c ON b.techkey = c.techkey
INNER JOIN day d ON a.flagdatekey = d.datekey
WHERE a.ro = '16100455'


-- 1/2/13
-- labor sales based ON pricing tables

SELECT dl5, al5, glaccount, gmcoa, split 
FROM zcb
WHERE name = 'cb2'
  AND dl4 = 'mechanical'
  AND al4 = 'sales'
  AND dl2 = 'ry1'
--  AND dl5 = 'main shop'
ORDER BY glaccount  
-- these are the priced labor ops
what abt accts (IN zcb, NOT arkona sdpprice)p
               146031 - 146036
               146101, 146102, 146201, 146202, 
               146301, 146320,
               146401, 146402, 146404, 146404 (pdi)
               
               body shop 147000, 147102, 147400

SELECT dl5, al5, glaccount, gmcoa, split 
FROM zcb
WHERE name = 'cb2'
  AND dl4 = 'body shop'
  AND al4 = 'sales'
  AND dl2 = 'ry1'
--  AND dl5 = 'main shop'
ORDER BY glaccount        

SELECT gtacct, SUM(gttamt)
FROM stgarkonaglptrns
WHERE gtdate BETWEEN '11/01/2012' AND '11/30/2012'  
  AND gtacct IN ('146031','146032','146033','146034','146035','146036','146101','146102',
    '146201','146202','146301','146320','146401','146402','146404')   
GROUP by gtacct    
    
SELECT gmacct, gmdesc, gmdept, coalesce(b.amt, 0)
FROM stgarkonaGLPMAST a
LEFT JOIN (
  SELECT gtacct, SUM(gttamt) AS amt
  FROM stgarkonaglptrns
  WHERE gtdate BETWEEN '11/01/2012' AND '11/30/2012'  
    AND gtacct IN ('146031','146032','146033','146034','146035','146036','146101','146102',
      '146201','146202','146301','146320','146401','146402','146404')   
  GROUP by gtacct ) b ON a.gmacct = b.gtacct 
WHERE a.gmyear = 2012
  AND gmacct IN ('146031','146032','146033','146034','146035','146036','146101','146102',
    '146201','146202','146301','146320','146401','146402','146404')  
ORDER BY a.gmacct       

SELECT gtacct, MIN(gtctl#), MAX(gtctl#)
  FROM stgarkonaglptrns
  WHERE gtdate BETWEEN '11/01/2012' AND '11/30/2012'  
    AND gtacct IN ('146031','146032','146033','146034','146035','146036','146101','146102',
      '146201','146202','146301','146320','146401','146402','146404') 
GROUP BY gtacct      

-- 01/08/13
-- labor sales for flagged hours based ON pricing IS potential labor sales
-- AS opposed to labor sales FROM accounting IS actual labor sales