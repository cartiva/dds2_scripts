-- 11/18
-- to produce the spreadsheet for mike
-- 1/20, ADD nathan
/*
INSERT INTO dimServiceWriter 
SELECT distinct 'RY1','426', name
FROM edwEmployeeDim
WHERE employeenumber = '139920'

3/4/13
  discrepancy BETWEEN COUNT * COUNT of cashiered
  eg Flikka 4 opened, 3 cashiered
*/
-- DROP TABLE #wtf;
SELECT distinct a.storecode, b.yearmonth, a.ro, a.writerid, d.name AS OpenedBy, 
  f.gquser AS CashieredBy
INTO #wtf
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
LEFT JOIN factroline c ON a.ro = c.ro 
LEFT JOIN dimServiceWriter d ON a.storecode = d.storecode
  AND a.writerid = d.writernumber
LEFT JOIN stgArkonaGLPTRNS e ON a.ro = e.gtdoc#  
LEFT JOIN stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
-- WHERE b.yearmonth > 201206
WHERE b.yearmonth = 201303 --BETWEEN (year(curdate())*100 + month(curdate())-3) AND year(curdate())*100 + month(curdate())
  AND a.storecode = 'ry1'
  AND a.void = false
  AND NOT EXISTS (
    SELECT 1
    FROM factroline
    WHERE ro = a.ro
      AND servicetype <> 'mr')
  AND NOT EXISTS (
    SELECT 1
    FROM factroline
    WHERE ro = a.ro
      AND paytype IN ('w','i'))  
  AND writerid IN ('402','403','645','704','705','714','720','426','721','428','429')
  AND void = false
  AND finalclosedatekey <> 7306
-- 3/4  
  AND e.gtdoc# IS NOT NULL;

DROP TABLE rptWriterCashiering;
CREATE TABLE rptWriterCashiering (
  yearmonth integer,
  OpenedBy cichar(25),
  Opened integer,
  CashieredBy cichar(25),
  Cashiered integer) IN database;
INSERT INTO rptWriterCashiering
SELECT yearmonth, openedby, opened, cashieredby, SUM(cashiered) AS cashiered
FROM (    
  SELECT yearmonth, openedby, opened, cashiered, cashieredby
--    CASE 
--      WHEN c2 = 'Writer' THEN cashieredby
--      ELSE 'Cashier'
--    END AS cashieredby  
  FROM(
    SELECT c.*, d.opened, e.c2
    FROM (
      SELECT a.yearmonth, a.openedby, coalesce(a.cashieredby, 'xxx') AS cashieredby, 
        COUNT(*) AS cashiered
      FROM #wtf a
      WHERE cashieredby IS NOT NULL               
      GROUP BY a.yearmonth, a.openedby, coalesce(a.cashieredby, 'xxx')) c
    LEFT JOIN (
      SELECT yearmonth, openedby, COUNT(*) AS Opened
      FROM #wtf  
      GROUP BY yearmonth, openedby) d ON c.yearmonth = d.yearmonth
        AND c.openedby = d.openedby
    LEFT JOIN (
      SELECT DISTINCT coalesce(cashieredby, 'xxx') AS cashieredby, 
        CASE 
          WHEN cashieredby IN ('RYDETRAVIS','RYDESHANEA','RYDERODNEY','RYDEKENESP',
            'RYDEKENCAR','RYDEJOHNTY','RYDEDANEVA') THEN 'Writer'
          ELSE 'Not Writer'
        END AS c2
      FROM #wtf) e ON c.cashieredby = e.cashieredby) g) f      
GROUP BY yearmonth, openedby, opened, cashieredby;  

SELECT *
FROM rptWriterCashiering 
ORDER BY openedby asc, cashiered desc