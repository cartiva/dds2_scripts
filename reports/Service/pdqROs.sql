
SELECT dayname(thedate), dayofweek(thedate), COUNT(*)
FROM (
  SELECT b.thedate, a.ro
  FROM factro a
  INNER JOIN day b ON a.opendatekey = b.datekey
    AND b.thedate BETWEEN curdate() - 365 AND curdate() -1
  WHERE EXISTS (
    SELECT 1
    FROM factroline
    WHERE storecode = a.storecode
      AND ro = a.ro
      AND opcode LIKE 'pdq%')) x
GROUP BY dayname(thedate), dayofweek(thedate) 
ORDER BY dayofweek(thedate)


SELECT ptro#, ptdate, CAST(ptcreate AS sql_date)
FROM stgarkonasdprhdr
WHERE LEFT(ptro#, 2) = '19'
  AND length(ptro#) = 8
  AND CAST(ptcreate AS sql_date) <> '12/31/9999'
  AND ptdate <> CAST(ptcreate AS sql_date)
--  AND ptdate > curdate() - 365
    aND CAST(ptcreate AS sql_date) <> '09/23/2010'
ORDER BY ptdate    

SELECT MIN(ptdate), MAX(ptdate)
FROM stgarkonasdprhdr
WHERE CAST(ptcreate AS sql_date) = '12/31/9999'