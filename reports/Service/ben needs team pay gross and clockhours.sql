CREATE TABLE zBenTeampayGross2013 (theYear integer, emp cichar(9), gross numeric(12,2)) IN database;
Insert into zBenTeampayGross2013 values(2013, '1124436', 15799.28);
Insert into zBenTeampayGross2013 values(2013, '194675', 11755.70);
Insert into zBenTeampayGross2013 values(2013, '1133500', 8452.20);
Insert into zBenTeampayGross2013 values(2013, '152235', 24142.72);
Insert into zBenTeampayGross2013 values(2013, '1107950', 13382.98);
Insert into zBenTeampayGross2013 values(2013, '1137590', 16004.54);
Insert into zBenTeampayGross2013 values(2013, '164015', 17638.49);
Insert into zBenTeampayGross2013 values(2013, '136170', 16983.19);
Insert into zBenTeampayGross2013 values(2013, '161120', 3431.30);
Insert into zBenTeampayGross2013 values(2013, '131370', 15077.69);
Insert into zBenTeampayGross2013 values(2013, '167580', 9157.65);
Insert into zBenTeampayGross2013 values(2013, '152410', 14135.94);
Insert into zBenTeampayGross2013 values(2013, '1134850', 9008.79);
Insert into zBenTeampayGross2013 values(2013, '1149500', 12711.90);
Insert into zBenTeampayGross2013 values(2013, '1106399', 20694.69);
Insert into zBenTeampayGross2013 values(2013, '116185', 14475.35);
Insert into zBenTeampayGross2013 values(2013, '1135410', 12073.77);
Insert into zBenTeampayGross2013 values(2013, '1146989', 14798.99);
Insert into zBenTeampayGross2013 values(2013, '179750', 16874.62);


CREATE TABLE zBenTeampayGross2014 (theYear integer, emp cichar(9), gross numeric(12,2)) IN database;
Insert into zBenTeamPayGross2014 values(2014, '1124436', 22566.77);
Insert into zBenTeamPayGross2014 values(2014, '194675', 17284.68);
Insert into zBenTeamPayGross2014 values(2014, '1133500', 12771.72);
Insert into zBenTeamPayGross2014 values(2014, '152235', 32761.97);
Insert into zBenTeamPayGross2014 values(2014, '1107950', 15167.58);
Insert into zBenTeamPayGross2014 values(2014, '1137590', 18201.75);
Insert into zBenTeamPayGross2014 values(2014, '164015', 24182.02);
Insert into zBenTeamPayGross2014 values(2014, '136170', 18849.60);
Insert into zBenTeamPayGross2014 values(2014, '161120', 17341.11);
Insert into zBenTeamPayGross2014 values(2014, '131370', 15799.35);
Insert into zBenTeamPayGross2014 values(2014, '167580', 10919.52);
Insert into zBenTeamPayGross2014 values(2014, '152410', 17949.73);
Insert into zBenTeamPayGross2014 values(2014, '1134850', 11732.78);
Insert into zBenTeamPayGross2014 values(2014, '1149500', 18413.24);
Insert into zBenTeamPayGross2014 values(2014, '1106399', 30309.06);
Insert into zBenTeamPayGross2014 values(2014, '116185', 20441.36);
Insert into zBenTeamPayGross2014 values(2014, '1135410', 12330.18);
Insert into zBenTeamPayGross2014 values(2014, '1146989', 16215.41);
Insert into zBenTeamPayGross2014 values(2014, '179750', 11697.46);


SELECT 2013, c.employeenumber, SUM(a.clockhours) as clockhours
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
  AND b.thedate BETWEEN '01/01/2013' AND '05/31/2013'
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey  
WHERE c.employeenumber IN (
'116185',
'131370',
'136170',
'152235',
'152410',
'161120',
'164015',
'167580',
'179750',
'194675',
'1106399',
'1107950',
'1124436',
'1133500',
'1134850',
'1135410',
'1137590',
'1146989',
'1149500')   
GROUP BY c.employeenumber

SELECT 2014, c.employeenumber, SUM(a.clockhours) as clockhours
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
  AND b.thedate BETWEEN '01/01/2014' AND '05/31/2014'
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey  
WHERE c.employeenumber IN (
'116185',
'131370',
'136170',
'152235',
'152410',
'161120',
'164015',
'167580',
'179750',
'194675',
'1106399',
'1107950',
'1124436',
'1133500',
'1134850',
'1135410',
'1137590',
'1146989',
'1149500')   
GROUP BY c.employeenumber

DROP TABLE zBenTeampayGross;
CREATE TABLE zBenTeampayGross (
  empNumber cichar(9),
  tech cichar(51)) IN database;
  
INSERT INTO zBenTeampayGross (empNumber, tech)
SELECT employeenumber, TRIM(lastname) + ', ' + TRIM(firstname)
from edwEmployeeDim 
WHERE employeenumber IN (
'116185',
'131370',
'136170',
'152235',
'152410',
'161120',
'164015',
'167580',
'179750',
'194675',
'1106399',
'1107950',
'1124436',
'1133500',
'1134850',
'1135410',
'1137590',
'1146989',
'1149500') 
AND currentrow = true;  

SELECT tech, [gross 2013], [clock 2013], round([gross 2013]/[clock 2013], 2) AS [2013 Rate],
  [gross 2014], [clock 2014], round([gross 2014]/[clock 2014], 2) AS [2014 Rate]
FROM (
  select tech, round(b.gross, 0) AS [Gross 2013], round(e.clockhours, 0) AS [Clock 2013],
    round(c.gross, 0) AS [Gross 2014], round(d.clockhours, 0) AS [Clock 2014]   
  FROM zBenTeampayGross a
  LEFT JOIN zBenTeampayGross2013 b on a.empNumber = b.emp
  LEFT JOIN zBenTeampayGross2014 c on a.empNumber = c.emp
  LEFT JOIN (
    SELECT 2014, c.employeenumber, SUM(a.clockhours) as clockhours
    FROM edwClockHoursFact a
    INNER JOIN day b on a.datekey = b.datekey
      AND b.thedate BETWEEN '01/01/2014' AND '05/31/2014'
    INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey  
    WHERE c.employeenumber IN ('116185','131370','136170','152235','152410',
      '161120','164015','167580','179750','194675','1106399','1107950','1124436',
      '1133500','1134850','1135410','1137590','1146989','1149500')   
    GROUP BY c.employeenumber) d on a.empNumber = d.employeenumber
  LEFT JOIN (
    SELECT 2013, c.employeenumber, SUM(a.clockhours) as clockhours
    FROM edwClockHoursFact a
    INNER JOIN day b on a.datekey = b.datekey
      AND b.thedate BETWEEN '01/01/2013' AND '05/31/2013'
    INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey  
    WHERE c.employeenumber IN ('116185','131370','136170','152235','152410',
      '161120','164015','167580','179750','194675','1106399','1107950','1124436',
      '1133500','1134850','1135410','1137590','1146989','1149500')   
    GROUP BY c.employeenumber) e on a.empNumber = e.employeenumber) x  
    
-- 12/31/2014, mark steinke needs same thing for body shop team pay
-- 2013 vs 2014
-- get pay numbers FROM aqt - payroll total gross for a time period   
-- here are the relevant employeenumbers
SELECT a. firstname, a.lastname, a.employeenumber, b.techkey
-- SELECT "'" + TRIM(a.employeenumber) + "',"
-- SELECT '''' + TRIM(a.employeenumber) + '''' + ',' -- list for aqt
FROM scotest.tpEmployees a
INNER JOIN scotest.tpTechs b on a.employeenumber = b.employeenumber
  AND b.departmentkey = 13
INNER JOIN scotest.tpTeamTechs c  on b.techkey = c.techkey
  AND c.thrudate > curdate()
INNER JOIN edwEmployeeDim d on a.employeenumber = d.employeenumber
  AND d.currentrow = true
  AND d.active = 'active'  


CREATE TABLE #bs2013Gross (theYear integer, emp cichar(9), gross numeric(12,2));
insert into #bs2013Gross values(2013,'135770',60456.14);
insert into #bs2013Gross values(2013,'1125565',49818.83);
insert into #bs2013Gross values(2013,'150105',50866.99);
insert into #bs2013Gross values(2013,'1118780',54772.71);
insert into #bs2013Gross values(2013,'1126040',60162.91);
insert into #bs2013Gross values(2013,'1106400',44680.55);
insert into #bs2013Gross values(2013,'174130',48075.85);
insert into #bs2013Gross values(2013,'1114120',21903.03);
insert into #bs2013Gross values(2013,'191350',46074.25);
insert into #bs2013Gross values(2013,'1110650',48668.91);
insert into #bs2013Gross values(2013,'171055',62298.71);
insert into #bs2013Gross values(2013,'1109852',5419.85);
insert into #bs2013Gross values(2013,'1118722',61076.31);
insert into #bs2013Gross values(2013,'1147061',48077.16);
insert into #bs2013Gross values(2013,'1146991',53423.51);

CREATE TABLE #bs2014Gross (theYear integer, emp cichar(9), gross numeric(12,2));
insert into #bs2014Gross values(2014,'135770',65203.2);
insert into #bs2014Gross values(2014,'1125565',54142.83);
insert into #bs2014Gross values(2014,'150105',57846.1);
insert into #bs2014Gross values(2014,'1118780',71611.34);
insert into #bs2014Gross values(2014,'1126040',68382.5);
insert into #bs2014Gross values(2014,'1106400',53309.88);
insert into #bs2014Gross values(2014,'174130',43424.92);
insert into #bs2014Gross values(2014,'1114120',39014.96);
insert into #bs2014Gross values(2014,'191350',60105.53);
insert into #bs2014Gross values(2014,'1110650',62069.55);
insert into #bs2014Gross values(2014,'186210',18255.95);
insert into #bs2014Gross values(2014,'171055',80338.44);
insert into #bs2014Gross values(2014,'1109852',36666.63);
insert into #bs2014Gross values(2014,'1118722',67889.37);
insert into #bs2014Gross values(2014,'1147061',55498.36);
insert into #bs2014Gross values(2014,'1146991',67453.8);

CREATE TABLE #bsGross (empNumber cichar(9), tech cichar(51));
INSERT INTO #bsGross
SELECT a.employeenumber, TRIM(d.firstname) + ' ' + d.lastname
FROM scotest.tpEmployees a
INNER JOIN scotest.tpTechs b on a.employeenumber = b.employeenumber
  AND b.departmentkey = 13
INNER JOIN scotest.tpTeamTechs c  on b.techkey = c.techkey
  AND c.thrudate > curdate()
INNER JOIN edwEmployeeDim d on a.employeenumber = d.employeenumber
  AND d.currentrow = true
  AND d.active = 'active' 
  
SELECT * FROM #bsGross  


SELECT tech, [gross 2013], [clock 2013], round([gross 2013]/[clock 2013], 2) AS [2013 Rate],
  [gross 2014], [clock 2014], round([gross 2014]/[clock 2014], 2) AS [2014 Rate]
FROM (
  select tech, round(b.gross, 0) AS [Gross 2013], round(e.clockhours, 0) AS [Clock 2013],
    round(c.gross, 0) AS [Gross 2014], round(d.clockhours, 0) AS [Clock 2014]   
  FROM #bsGross a
  LEFT JOIN #bs2013Gross b on a.empNumber = b.emp
  LEFT JOIN #bs2014Gross c on a.empNumber = c.emp
  LEFT JOIN (
    SELECT 2014, c.employeenumber, SUM(a.clockhours) as clockhours
    FROM edwClockHoursFact a
    INNER JOIN day b on a.datekey = b.datekey
      AND b.thedate BETWEEN '01/01/2014' AND '12/31/2014'
    INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey  
    INNER JOIN #bsgross cc on c.employeenumber = cc.empNumber   
    GROUP BY c.employeenumber) d on a.empNumber = d.employeenumber
  LEFT JOIN (
    SELECT 2013, c.employeenumber, SUM(a.clockhours) as clockhours
    FROM edwClockHoursFact a
    INNER JOIN day b on a.datekey = b.datekey
      AND b.thedate BETWEEN '01/01/2013' AND '12/31/2013'
    INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey  
    INNER JOIN #bsGross cc on c.employeenumber = cc.empnumber   
    GROUP BY c.employeenumber) e on a.empNumber = e.employeenumber) x    
ORDER BY tech