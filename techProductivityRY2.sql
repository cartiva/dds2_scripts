SELECT a.thedate, b.employeekey, b.flaghours
-- DROP TABLE #flag
INTO #flag
FROM day a 
LEFT JOIN factTechFlagHoursByDay b on a.datekey = b.flagdatekey 
  AND b.employeekey > 0
  AND b.flaghours > 0
WHERE a.thedate BETWEEN curdate() - 90 AND curdate(); 
-- let's restrict #clock based ON the existence of empkey IN #flag 
SELECT a.thedate, b.employeekey, b.clockhours
-- DROP TABLE #clock
INTO #clock
FROM day a
LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  AND b.clockhours > 0
WHERE a.thedate BETWEEN curdate() - 90 AND curdate()
  AND b.employeekey IN (
    SELECT employeekey
    FROM #flag);
    
-- DO a JOIN with date to each #TABLE, but wait, why don't i just generate 1 row
-- per date IN the base #tables?      

SELECT a.thedate, b.employeekey, b.flaghours
-- DROP TABLE #flag
-- INTO #flag
FROM day a 
LEFT JOIN factTechFlagHoursByDay b on a.datekey = b.flagdatekey 
  AND b.employeekey > 0
WHERE a.thedate BETWEEN curdate() - 90 AND curdate(); 

-- 1 row for each day/empkey
-- empkey FROM factTechFlagHoursByDay
SELECT a.thedate, a.datekey, b.employeekey
FROM day a, (
  SELECT DISTINCT employeekey
  FROM factTechFlagHoursByDay
  WHERE employeekey > 0) b
WHERE a.thedate BETWEEN curdate() - 90 AND curdate();   

SELECT c.thedate, c.employeekey, coalesce(d.flaghours, 0) AS flaghours
INTO #flag
FROM (
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM factTechFlagHoursByDay
    WHERE employeekey > 0) b
  WHERE a.thedate BETWEEN curdate() - 90 AND curdate()) c
LEFT JOIN factTechFlagHoursByDay d ON c.datekey = d.flagdatekey
  AND c.employeekey = d.employeekey
  
-- ok AND now #clock
-- 1 row for each day/empkey WHERE empkey has ever flagged hours
 
SELECT c.thedate, c.employeekey, coalesce(d.clockhours, 0) AS clockhours   
INTO #clock
FROM (
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM edwClockHoursFact 
    WHERE employeekey IN (
      SELECT DISTINCT employeekey
      FROM factTechFlagHoursByDay 
      WHERE employeekey > 0)) b
  WHERE a.thedate BETWEEN curdate() - 90 AND curdate()) c
LEFT JOIN edwClockHoursFact d ON c.datekey = d.datekey
  AND c.employeekey = d.employeekey; 

SELECT thedate, SUM(clockhours) AS clockhours, SUM(flaghours) AS flaghours, 
  round(SUM(flaghours)/SUM(clockhours), 2) AS Prod
FROM (
  SELECT x.*, z.name, zz.technumber
  FROM (
    SELECT a.thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
      coalesce(b.clockhours, 0) AS clockhours,
      coalesce(a.flaghours, 0) AS flaghours
    -- SELECT *
    FROM #flag a
    full OUTER JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) x
  LEFT JOIN edwEmployeeDim z ON x.employeekey = z.employeekey
    AND (z.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
    AND z.storecode = 'ry2'
  LEFT JOIN dimTech zz ON z.employeenumber = zz.employeenumber  
  WHERE zz.technumber IS NOT NULL) r 
WHERE thedate IS NOT NULL   
GROUP BY thedate  


SELECT s.thedate, t.name, t.technumber, SUM(clockhours) AS clockhours, 
  SUM(flaghours) AS flaghours--, round(SUM(flaghours)/SUM(clockhours), 2)*100
FROM (
  SELECT thedate, thedate - 14 as prev14Begin, 
    thedate - 1 AS prev14End
  FROM day 
  WHERE thedate BETWEEN curdate() - 90 and curdate()) s
LEFT JOIN (  
  SELECT x.*, z.name, zz.technumber
  FROM (
    SELECT a.thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
      coalesce(b.clockhours, 0) AS clockhours,
      coalesce(a.flaghours, 0) AS flaghours
    -- SELECT *
    FROM #flag a
    full OUTER JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) x
  LEFT JOIN edwEmployeeDim z ON x.employeekey = z.employeekey
    AND (z.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
    AND z.storecode = 'ry2'
  LEFT JOIN dimTech zz ON z.employeenumber = zz.employeenumber  
  WHERE zz.technumber IS NOT NULL) t ON t.thedate BETWEEN s.prev14Begin AND s.prev14End
WHERE t.thedate IS NOT NULL   
  AND s.thedate BETWEEN curdate() - 76 AND curdate()
GROUP BY s.thedate, t.name, t.technumber



-- guys with flag hours but no clockhours IN prev14
-- wtf IS up w/tech 613
SELECT *
FROM (
SELECT s.thedate, t.name, t.technumber, SUM(clockhours) AS clockhours, 
  SUM(flaghours) AS flaghours-- , round(SUM(flaghours)/SUM(clockhours), 2)*100
FROM (
  SELECT thedate, thedate - 14 as prev14Begin, 
    thedate - 1 AS prev14End
  FROM day 
  WHERE thedate BETWEEN curdate() - 90 and curdate()) s
LEFT JOIN (  
  SELECT x.*, z.name, zz.technumber
  FROM (
    SELECT a.thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
      coalesce(b.clockhours, 0) AS clockhours,
      coalesce(a.flaghours, 0) AS flaghours
    -- SELECT *
    FROM #flag a
    full OUTER JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) x
  LEFT JOIN edwEmployeeDim z ON x.employeekey = z.employeekey
    AND (z.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
    AND z.storecode = 'ry2'
  LEFT JOIN dimTech zz ON z.employeenumber = zz.employeenumber  
  WHERE zz.technumber IS NOT NULL) t ON t.thedate BETWEEN s.prev14Begin AND s.prev14End
WHERE t.thedate IS NOT NULL   
GROUP BY s.thedate, t.name, t.technumber) x
WHERE clockhours = 0

-- shop
SELECT s.thedate, SUM(clockhours) AS clockhours, 
  SUM(flaghours) AS flaghours , round(SUM(flaghours)/SUM(clockhours), 2)*100
FROM (
  SELECT thedate, thedate - 14 as prev14Begin, 
    thedate - 1 AS prev14End
  FROM day 
  WHERE thedate BETWEEN curdate() - 90 and curdate()) s
LEFT JOIN (  
  SELECT x.*, z.name, zz.technumber
  FROM (
    SELECT a.thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
      coalesce(b.clockhours, 0) AS clockhours,
      coalesce(a.flaghours, 0) AS flaghours
    -- SELECT *
    FROM #flag a
    full OUTER JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) x
  LEFT JOIN edwEmployeeDim z ON x.employeekey = z.employeekey
    AND (z.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
    AND z.storecode = 'ry2'
  LEFT JOIN dimTech zz ON z.employeenumber = zz.employeenumber  
  WHERE zz.technumber IS NOT NULL) t ON t.thedate BETWEEN s.prev14Begin AND s.prev14End
WHERE t.thedate IS NOT NULL   
GROUP BY s.thedate

SELECT pttech, SUM(ptlhrs)
FROM xfmro
WHERE ptco# = 'ry2'
  AND ptltyp = 'l'
  AND ptcode = 'tt'
GROUP BY pttech  
  
SELECT flagdate, SUM(ptlhrs)
FROM xfmro
WHERE ptco# = 'ry2'
  AND ptltyp = 'l'
  AND ptcode = 'tt'

 
SELECT a.thedate, SUM(flaghours)
FROM day a 
LEFT JOIN factTechFlagHoursByDay b on a.datekey = b.flagdatekey 
  AND b.employeekey > 0
  AND b.flaghours > 0
INNER JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
  AND c.storecode = 'ry2'  
WHERE a.thedate BETWEEN curdate() - 90 AND curdate() 
GROUP BY a.thedate

-- trying to figure out wtf IS up with tech 613
SELECT *
FROM xfmro
WHERE flagdate BETWEEN '04/17/2012' AND '05/31/2012'
  AND pttech = '613'
  
SELECT *
FROM factroline
WHERE ro = '2661937'  

SELECT *
FROM factro
WHERE ro = '2661937' 

-- ok covell, tech# 613, emp# 222810, techkey 129
SELECT *
FROM dimtech
WHERE technumber = '613'
-- empkey 824
select *
FROM edwEmployeeDim
WHERE employeenumber = '222810'
-- so, the last flagdate here IS 5/15
SELECT b.thedate, a.*
FROM factTechFlagHoursByDay a
LEFT JOIN day b ON a.flagdatekey = b.datekey
WHERE a.employeekey = 824
-- hmm, this looks ok
SELECT a.thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
  coalesce(b.clockhours, 0) AS clockhours,
  coalesce(a.flaghours, 0) AS flaghours
-- SELECT *
FROM #flag a
full OUTER JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey
WHERE a.employeekey = 824  

-- hmmm, IS the problem

  SELECT x.*, z.name, zz.technumber
  FROM (
    SELECT a.thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
      coalesce(b.clockhours, 0) AS clockhours,
      coalesce(a.flaghours, 0) AS flaghours
    -- SELECT *
    FROM #flag a
    full OUTER JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) x
  LEFT JOIN edwEmployeeDim z ON x.employeekey = z.employeekey
    AND (z.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
    AND z.storecode = 'ry2'
  LEFT JOIN dimTech zz ON z.employeenumber = zz.employeenumber  
  WHERE zz.technumber IS NOT NULL
    AND x.employeekey = 824

SELECT x.*-- , z.employeenumber--, zz.technumber
FROM (  
  SELECT a.thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
    coalesce(b.clockhours, 0) AS clockhours,
    coalesce(a.flaghours, 0) AS flaghours
  -- SELECT *
  FROM #flag a
  full OUTER JOIN #clock b ON a.thedate = b.thedate
    AND a.employeekey = b.employeekey) x   
WHERE employeekey = 824     
inner JOIN edwEmployeeDim z ON x.employeekey = z.employeekey
  AND (z.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
  AND z.storecode = 'ry2' 
WHERE x.employeekey = 824  
LEFT JOIN dimtech zz ON z.employeenumber = zz.employeenumber  

SELECT * FROM edwClockHoursFact 
WHERE employeekey = 824
  AND clockhours = 4.82
-- this isn't giving me what i thought it was
-- it's excluding data that shouldn't be
-- though i don't know why
SELECT a.thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
  coalesce(b.clockhours, 0) AS clockhours,
  coalesce(a.flaghours, 0) AS flaghours
-- SELECT *
FROM #flag a
full OUTER JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey

SELECT * -- includes 4/19
FROM #clock
WHERE employeekey = 824
SELECT * -- no 4/19
FROM #flag
WHERE employeekey = 824

-- aha, maybe it IS this
-- wa ithink hoo
SELECT *
FROM (
  SELECT coalesce(a.thedate, b.thedate) AS thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
    coalesce(b.clockhours, 0) AS clockhours,
    coalesce(a.flaghours, 0) AS flaghours
  -- SELECT *
  FROM #flag a
  full OUTER JOIN #clock b ON a.thedate = b.thedate
    AND a.employeekey = b.employeekey) x
WHERE employeekey = 824  
ORDER BY thedate
 
-- so now
--SELECT s.thedate, SUM(clockhours) AS clockhours, 
  SUM(flaghours) AS flaghours , round(SUM(flaghours)/SUM(clockhours), 2)*100
FROM (
  SELECT thedate, thedate - 14 as prev14Begin, 
    thedate - 1 AS prev14End
  FROM day 
  WHERE thedate BETWEEN curdate() - 90 and curdate()) s
LEFT JOIN (  
  SELECT x.*, z.name, zz.technumber
  FROM (
    SELECT coalesce(a.thedate, b.thedate) AS thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
      coalesce(b.clockhours, 0) AS clockhours,
      coalesce(a.flaghours, 0) AS flaghours
    -- SELECT *
    FROM #flag a
    full OUTER JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) x
  LEFT JOIN edwEmployeeDim z ON x.employeekey = z.employeekey
    AND (z.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
    AND z.storecode = 'ry2'
  LEFT JOIN dimTech zz ON z.employeenumber = zz.employeenumber  
  WHERE zz.technumber IS NOT NULL) t ON t.thedate BETWEEN s.prev14Begin AND s.prev14End
WHERE t.thedate BETWEEN curdate() - 76 AND curdate() - 1
GROUP BY s.thedate
-- techs w/0 clockhours
-- god damnit, still getting flag hours for 613 ON days WHERE there aren't any
SELECT *
FROM (
SELECT s.thedate, t.name, t.technumber, SUM(clockhours) AS clockhours, 
  SUM(flaghours) AS flaghours-- , round(SUM(flaghours)/SUM(clockhours), 2)*100
FROM (
  SELECT thedate, thedate - 14 as prev14Begin, 
    thedate - 1 AS prev14End
  FROM day 
  WHERE thedate BETWEEN curdate() - 90 and curdate()) s
LEFT JOIN (  
  SELECT x.*, z.name, zz.technumber
  FROM (
    SELECT coalesce(a.thedate, b.thedate) AS thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
      coalesce(b.clockhours, 0) AS clockhours,
      coalesce(a.flaghours, 0) AS flaghours
    -- SELECT *
    FROM #flag a
    full OUTER JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) x
  LEFT JOIN edwEmployeeDim z ON x.employeekey = z.employeekey
    AND (z.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
    AND z.storecode = 'ry2'
  LEFT JOIN dimTech zz ON z.employeenumber = zz.employeenumber  
  WHERE zz.technumber IS NOT NULL) t ON t.thedate BETWEEN s.prev14Begin AND s.prev14End
WHERE t.thedate IS NOT NULL   
GROUP BY s.thedate, t.name, t.technumber) x
WHERE clockhours = 0
-- so break it down
613 shows flaghours ON 
5/27 4.5
5/28 4.5
5/29 1.5
SELECT * FROM #flag
SELECT *FROM #flag a WHERE employeekey = 824
-- hmm, no records for 613 ON 5/27
SELECT *
FROM (
    SELECT coalesce(a.thedate, b.thedate) AS thedate, coalesce(a.employeekey, b.employeekey) AS employeekey, 
      coalesce(b.clockhours, 0) AS clockhours,
      coalesce(a.flaghours, 0) AS flaghours
    -- SELECT *
    FROM #flag a
    full OUTER JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) Z
WHERE thedate BETWEEN '05/27/2012' AND '05/29/2012'
ORDER BY thedate      
--  hmmm IS full JOIN AND coalesce actually the answer?       
SELECT *
FROM (      
  SELECT a.thedate AS fDate, b.thedate AS cDate, a.employeekey AS fEmpKey, b.employeekey AS cEmpKey, 
    coalesce(b.clockhours, 0) AS clockhours,
    coalesce(a.flaghours, 0) AS flaghours
  -- SELECT *
  FROM #flag a
  full OUTER JOIN #clock b ON a.thedate = b.thedate
    AND a.employeekey = b.employeekey
    WHERE (a.employeekey = 824 OR b.employeekey = 824)) z
-- this appears to be good for the base data
-- how to transform it to what i want
-- what DO i want
-- for a tech with either clock hours OR flag hours ON a date
-- one row for that tech AND that date
SELECT a.thedate, b.*, c.*
FROM day a
LEFT JOIN #flag b ON a.thedate = b.thedate AND b.employeekey = 824
LEFT JOIN #clock c ON a.thedate = c.thedate AND c.employeekey = 824
WHERE a.thedate BETWEEN '04/16/2012' AND '05/15/2012'

-- adding second empkey gives me shit LIKE
4/16      824    6.5    795   7.93
SELECT a.thedate, b.employeekey, b.flaghours, c.employeekey, c.clockhours
FROM day a
LEFT JOIN #flag b ON a.thedate = b.thedate AND b.employeekey = 824 --IN( 824,795)
LEFT JOIN #clock c ON a.thedate = c.thedate AND c.employeekey = 824 --IN( 824,795)
WHERE a.thedate BETWEEN '04/16/2012' AND '05/15/2012'

-- this IS interesting, but does it include days for clockhours with no flaghours
-- i'm thinking not
SELECT a.thedate, b.employeekey, b.flaghours, c.employeekey, c.clockhours
FROM day a
LEFT JOIN #flag b ON a.thedate = b.thedate AND b.employeekey IN( 824,795)
full JOIN #clock c ON b.thedate = c.thedate AND b.employeekey = c.employeekey and c.employeekey IN( 824,795) 
WHERE a.thedate BETWEEN '04/16/2012' AND '05/15/2012'

SELECT a.thedate, b.employeekey, b.flaghours, c.employeekey, c.clockhours
FROM day a
LEFT JOIN #flag b ON a.thedate = b.thedate AND b.employeekey IN( 824,795)
full JOIN #clock c ON a.thedate = c.thedate AND b.employeekey = c.employeekey and c.employeekey IN( 824,795) 
WHERE a.thedate BETWEEN '04/16/2012' AND '05/15/2012'

-- this IS interesting, but does it include days for clockhours with no flaghours
-- i'm thinking NOT
-- eg : day w/clockhour and no flaghour : 824 4/19, 4/26, 4/28, 795 4/20, 5/7
SELECT *
FROM #clock a
WHERE employeekey IN( 824,795) 
  AND NOT EXISTS (
  SELECT 1
  FROM #flag
  WHERE thedate = a.thedate
    AND employeekey = a.employeekey)

-- ok, day w/flaghours AND no clockhours   
-- 795 4/21, 4/24, 5/5  842 4/23, 5/2, 5/9 
SELECT *
FROM #flag a
WHERE employeekey IN( 824,795) 
  AND NOT EXISTS (
  SELECT 1
  FROM #clock
  WHERE thedate = a.thedate
    AND employeekey = a.employeekey)    

SELECT *
FROM day a, #flag
WHERE a.thedate BETWEEN '04/16/2012' AND '05/15/2012'

SELECT *
FROM (
SELECT a.thedate, b.thedate, a.employeekey, b.employeekey, 
  coalesce(b.clockhours, 0) AS clockhours,
  coalesce(a.flaghours, 0) AS flaghours
-- SELECT *
FROM #flag a
full OUTER JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey) z
WHERE flaghours = 0
  AND clockhours <> 0  
 
      
--= date AND empkey are unique IN each
SELECT thedate, employeekey 
FROM #flag
GROUP BY thedate, employeekey
HAVING COUNT(*) > 1

SELECT thedate, employeekey 
FROM #clock
GROUP BY thedate, employeekey
HAVING COUNT(*) > 1

limit #flag AND clock BY the same criteria
SELECT *
FROM #flag a
WHERE a.thedate BETWEEN '04/16/2012' AND '05/15/2012'

SELECT *
FROM #clock
WHERE thedate BETWEEN '04/16/2012' AND '05/15/2012'


SELECT *
FROM #clock a
full JOIN #flag b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey
  AND a.thedate BETWEEN '04/16/2012' AND '05/15/2012'
  AND b.thedate BETWEEN '04/16/2012' AND '05/15/2012'
  AND a.employeekey = 824
  AND b.employeekey = 824
WHERE a.thedate IS NULL  
 
-- i can't fucking figure out how to combine the 2
SELECT w.thedate,x.*, y.*
FROM day w
LEFT JOIN (
  SELECT a.*, b.name, c.technumber 
  FROM #flag a
  LEFT JOIN edwEmployeeDim b ON a.employeekey = b.employeekey
    AND (b.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
    AND b.storecode = 'ry2'
  LEFT JOIN dimTech c ON b.employeenumber = c.employeenumber  
  WHERE c.technumber IS NOT NULL) x ON w.thedate = x.thedate
LEFT JOIN (
  SELECT a.*, b.name, c.technumber 
  FROM #clock a
  LEFT JOIN edwEmployeeDim b ON a.employeekey = b.employeekey
    AND (b.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
    AND b.storecode = 'ry2'
  LEFT JOIN dimTech c ON b.employeenumber = c.employeenumber  
  WHERE c.technumber IS NOT NULL) y ON w.thedate = y.thedate
WHERE w.thedate BETWEEN '04/16/2012' AND '05/15/2012'  

-- checked for clock w/no flag
-- ok, day w/flaghours AND no clockhours   
-- 795 4/21, 4/24, 5/5  842 4/23, 5/2, 5/9 
SELECT *
FROM (
SELECT *
FROM #flag a
full OUTER JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey) x
WHERE x.thedate = '04/23/2012'  
  AND x.employeekey = 824

-- let's restrict #clock based ON the existence of empkey IN #flag  
SELECT x.thedate
FROM day x

FROM (
SELECT *
FROM #flag a
full OUTER JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey) x  
  
  
-- DO a JOIN with date to each #TABLE, but wait, why don't i just generate 1 row
-- per date IN the base #tables?  
  
SELECT *
FROM #flag a
LEFT JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey 
/*
the 2 #tables look complete
SELECT COUNT(*) FROM #flag  -- 13559
SELECT COUNT(*) FROM #clock -- 13559

SELECT *
FROM #flag a
WHERE NOT EXISTS (
  SELECT 1
  FROM #clock
  WHERE thedate = a.thedate
    AND employeekey = a.employeekey)  
    
SELECT *
FROM #clock a
WHERE NOT EXISTS (
  SELECT 1
  FROM #flag
  WHERE thedate = a.thedate
    AND employeekey = a.employeekey)      
*/ 
-- ok, NOT sure
-- back to previous problems
-- this shows ek824 with flag hours ON 5/27 - 5/29
-- of course it will, flaghours are for the previous 14 days
-- so flaghours ON 5/29 of 1.5 comes FROM 5/15
-- so, this will be a prod snag, what DO i DO with rows with 0 clockhours (div BY 0)
-- initially thinking
SELECT s.thedate, employeekey, SUM(flaghours) AS flaghours, SUM(clockhours) AS clockhours
FROM (
  SELECT thedate, thedate - 14 as prev14Begin, 
    thedate - 1 AS prev14End
  FROM day 
  WHERE thedate BETWEEN curdate() - 90 and curdate()) s
LEFT JOIN (  
  SELECT a.thedate, a.employeekey, a.flaghours, b.clockhours
  FROM #flag a
  LEFT JOIN #clock b ON a.thedate = b.thedate
    AND a.employeekey = b.employeekey) t ON t.thedate BETWEEN s.prev14begin AND s.prev14end
WHERE employeekey = 824 
GROUP BY s.thedate, employeekey  
 
-- but this shows no flag hours past 5/15 
SELECT b.thedate, a.*
from factTechFlagHoursByDay a
LEFT JOIN day b ON a.flagdatekey = b.datekey
WHERE employeekey = 824
 
 
-- the 0's may NOT be a problem at shoplevel
SELECT e.thedate, round(SUM(clockhours), 2) AS clockhours, 
  round(SUM(flaghours), 2) AS flaghours 
FROM (
  SELECT s.thedate, employeekey, SUM(flaghours) AS flaghours, SUM(clockhours) AS clockhours
  FROM (
    SELECT thedate, thedate - 14 as prev14Begin, 
      thedate - 1 AS prev14End
    FROM day 
    WHERE thedate BETWEEN curdate() - 90 and curdate()) s
  LEFT JOIN (  
    SELECT a.thedate, a.employeekey, a.flaghours, b.clockhours
    FROM #flag a
    LEFT JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) t ON t.thedate BETWEEN s.prev14begin AND s.prev14end
  GROUP BY s.thedate, employeekey) e   
LEFT JOIN edwEmployeeDim f ON e.employeekey = f.employeekey
  AND (f.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
  AND f.storecode = 'ry2'
LEFT JOIN dimTech g ON f.employeenumber = f.employeenumber  
WHERE f.technumber IS NOT NULL
GROUP BY e.thedate

-- the effect of hiring AND terming
-- watch one tech
SELECT *
FROM (
  SELECT s.thedate, employeekey, SUM(flaghours) AS flaghours, SUM(clockhours) AS clockhours
  FROM (
    SELECT thedate, thedate - 14 as prev14Begin, 
      thedate - 1 AS prev14End
    FROM day 
    WHERE thedate BETWEEN curdate() - 90 and curdate()) s
  LEFT JOIN (  
    SELECT a.thedate, a.employeekey, a.flaghours, b.clockhours
    FROM #flag a
    LEFT JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) t ON t.thedate BETWEEN s.prev14begin AND s.prev14end
  GROUP BY s.thedate, employeekey) h  
WHERE employeekey = 690  
  AND flaghours + clockhours <> 0
  
SELECT *
FROM #flag
WHERE employeekey = 690  
  AND thedate BETWEEN (
    SELECT MIN(thedate)
    FROM #flag
    WHERE employeekey = 690
      AND flaghours > 0)
    AND (
      SELECT max(thedate)
      FROM #flag
      WHERE employeekey = 690
        AND flaghours > 0)    

SELECT *
FROM #clock
WHERE employeekey = 690  
  AND thedate BETWEEN (
    SELECT MIN(thedate)
    FROM #flag
    WHERE employeekey = 690
      AND flaghours > 0)
    AND (
      SELECT max(thedate)
      FROM #flag
      WHERE employeekey = 690
        AND flaghours > 0)    
        
-- doug bohm

SELECT *
FROM (
  SELECT s.thedate, employeekey, SUM(flaghours) AS flaghours, SUM(clockhours) AS clockhours
  FROM (
    SELECT thedate, thedate - 14 as prev14Begin, 
      thedate - 1 AS prev14End
    FROM day 
    WHERE thedate BETWEEN curdate() - 90 and curdate()) s
  LEFT JOIN (  
    SELECT a.thedate, a.employeekey, a.flaghours, b.clockhours
    FROM #flag a
    LEFT JOIN #clock b ON a.thedate = b.thedate
      AND a.employeekey = b.employeekey) t ON t.thedate BETWEEN s.prev14begin AND s.prev14end
  GROUP BY s.thedate, employeekey) h  
WHERE employeekey = 1325  
  AND flaghours + clockhours <> 0
  
SELECT *
FROM #flag
WHERE employeekey = 1325  
  AND thedate BETWEEN (
    SELECT MIN(thedate)
    FROM #flag
    WHERE employeekey = 1325
      AND flaghours > 0)
    AND (
      SELECT max(thedate)
      FROM #flag
      WHERE employeekey = 1325
        AND flaghours > 0)  
  AND flaghours <> 0          

SELECT *
FROM #clock
WHERE employeekey = 1325  
  AND thedate BETWEEN (
    SELECT MIN(thedate)
    FROM #clock
    WHERE employeekey = 1325
      AND clockhours > 0)
    AND (
      SELECT max(thedate)
      FROM #clock
      WHERE employeekey = 1325
        AND clockhours > 0) 
  AND clockhours <> 0         
-- vacation time fucks it up big time       
SELECT b.thedate, a.*
FROM edwClockHoursFact a
inner JOIN day b ON a.datekey = b.datekey   
WHERE a.employeekey = 1325
  AND b.thedate BETWEEN ('05/15/2012') AND curdate()
  
need to exempt days
but how does affect the fucking rolling average shit
  
the previous 14 IS the kicker
IF it includes vacation time the graph IS fucked  
should it only include the days the tech worked

but this fucks up the shop view big time

-- ok, let's TRY it for a tech, previous 14 days worked
-- exempt vacation, pto, holiday AND no clockhours
-- but what afuckingbout days with flaghours but no clockhours???
-- so the vacation effect
-- vacation for a week, prev14 for the days subseqent to returning are fucked,
-- so something LIKE the previous 14 days that a dude worked?

-- out of this, i want the totals for the previous 14 records
SELECT a.thedate, a.employeekey, a.flaghours, b.clockhours
FROM #flag a
LEFT JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey
WHERE a.employeekey = 1325  
  AND a.flaghours + b.clockhours > 0

want to GROUP ON the range of 14 dates that indicate the previous 14 days that the dude clocked/flagged 
5/3: 4/16 - 5/2  


-- can't get away FROM the notion of needing a sequence    
SELECT a.thedate, a.employeekey, a.flaghours, b.clockhours
INTO #wtf1
FROM #flag a
LEFT JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey
WHERE a.employeekey = 1325  
  AND a.flaghours + b.clockhours > 0      
    
SELECT n
INTO #wtf2
FROM tally a 
WHERE n BETWEEN 1 AND (  
  SELECT COUNT(*)
  FROM #flag a
  LEFT JOIN #clock b ON a.thedate = b.thedate
    AND a.employeekey = b.employeekey
  WHERE a.employeekey = 1325  
    AND a.flaghours + b.clockhours > 0)    
    
SELECT *
FROM #wtf1 a
full JOIN #wtf2 b ON dayofyear(a.thedate) = b.n 

SELECT dayofyear(thedate) 
FROM #wtf1

-- ok, i know this IS kludgish, but it fucking works 
-- TRY it for a shop  
DROP TABLE zProd;   
CREATE TABLE zProd (
  seq autoinc, 
  thedate date, 
  employeekey integer,
  flaghours double,
  clockhours double) IN database;
INSERT INTO zProd (thedate, employeekey, flaghours, clockhours)      
SELECT a.thedate, a.employeekey, a.flaghours, b.clockhours
FROM #flag a
LEFT JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey
WHERE a.employeekey = 1325  
  AND a.flaghours + b.clockhours > 0 
   
SELECT * FROM zprod

SELECT f.thedate, g.employeekey, SUM(g.flaghours) AS flaghours, SUM(g.clockhours) AS clockhours
FROM (  
  SELECT a.thedate, a.seq - 14 AS begSeq, a.seq - 1 AS endSeq
  FROM zProd a
  WHERE a.seq > 14) f
LEFT JOIN zPRod g ON g.seq BETWEEN f.begSeq AND f.EndSeq  
GROUP BY f.thedate, g.employeekey


-- TRY it for a shop  
-- ry2
DROP TABLE zProd;   
CREATE TABLE zProd (
  seq autoinc, 
  thedate date, 
  employeekey integer,
  flaghours double,
  clockhours double) IN database;
INSERT INTO zProd (thedate, flaghours, clockhours)      
SELECT a.thedate, a.flaghours, b.clockhours
FROM (
  SELECT *
    FROM (
      SELECT thedate, SUM(flaghours) AS flaghours
      FROM #flag
      WHERE employeekey IN (
        SELECT employeekey
        FROM edwEmployeeDim f 
        LEFT JOIN dimTech g ON f.employeenumber = f.employeenumber  
        WHERE f.technumber IS NOT NULL
          AND (f.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
          AND f.storecode = 'ry2') 
      GROUP BY thedate) h     
    WHERE flaghours <> 0) a
LEFT JOIN (
  SELECT *
  FROM (
    SELECT thedate, SUM(clockhours) AS clockhours
    FROM #clock
    WHERE employeekey IN (
      SELECT employeekey
      FROM edwEmployeeDim f 
      LEFT JOIN dimTech g ON f.employeenumber = f.employeenumber  
      WHERE f.technumber IS NOT NULL
        AND (f.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
        AND f.storecode = 'ry2') 
    GROUP BY thedate) h     
  WHERE clockhours <> 0) b ON a.thedate = b.thedate
WHERE  a.flaghours + b.clockhours > 0;

   
SELECT * FROM zprod

SELECT f.thedate, SUM(g.flaghours) AS flaghours, SUM(g.clockhours) AS clockhours
FROM (  
  SELECT a.thedate, a.seq - 14 AS begSeq, a.seq - 1 AS endSeq
  FROM zProd a
  WHERE a.seq > 14) f
LEFT JOIN zPRod g ON g.seq BETWEEN f.begSeq AND f.EndSeq  
GROUP BY f.thedate

-- ry1
DROP TABLE zProd;   
CREATE TABLE zProd (
  seq autoinc, 
  thedate date, 
  employeekey integer,
  flaghours double,
  clockhours double) IN database;
INSERT INTO zProd (thedate, flaghours, clockhours)      
SELECT a.thedate, a.flaghours, b.clockhours
FROM (
  SELECT *
    FROM (
      SELECT thedate, SUM(flaghours) AS flaghours
      FROM #flag
      WHERE employeekey IN (
        SELECT employeekey
        FROM edwEmployeeDim f 
        LEFT JOIN dimTech g ON f.employeenumber = f.employeenumber  
        WHERE f.technumber IS NOT NULL
          AND (f.distcode IN ('STEC','SRVM') OR f.employeenumber = '167580') 
          AND f.storecode = 'ry1') 
      GROUP BY thedate) h     
    WHERE flaghours <> 0) a
LEFT JOIN (
  SELECT *
  FROM (
    SELECT thedate, SUM(clockhours) AS clockhours
    FROM #clock
    WHERE employeekey IN (
      SELECT employeekey
      FROM edwEmployeeDim f 
      LEFT JOIN dimTech g ON f.employeenumber = f.employeenumber  
      WHERE f.technumber IS NOT NULL
        AND (f.distcode IN ('STEC','SRVM') OR f.employeenumber = '167580')
        AND f.storecode = 'ry1') 
    GROUP BY thedate) h     
  WHERE clockhours <> 0) b ON a.thedate = b.thedate
WHERE  a.flaghours + b.clockhours > 0;

   
SELECT * FROM zprod

SELECT f.thedate, SUM(g.flaghours) AS flaghours, SUM(g.clockhours) AS clockhours,
  round(SUM(g.flaghours)/SUM(g.clockhours), 2) * 100 
FROM (  
  SELECT a.thedate, a.seq - 14 AS begSeq, a.seq - 1 AS endSeq
  FROM zProd a
  WHERE a.seq > 14) f
LEFT JOIN zPRod g ON g.seq BETWEEN f.begSeq AND f.EndSeq  
GROUP BY f.thedate


-- try it for a GROUP of techs
DROP TABLE zProd;   
CREATE TABLE zProd (
  seq autoinc, 
  thedate date, 
  employeekey integer,
  flaghours double,
  clockhours double) IN database;
INSERT INTO zProd (thedate, employeekey, flaghours, clockhours)      
SELECT a.thedate, a.employeekey, sum(a.flaghours), sum(b.clockhours)
FROM #flag a
LEFT JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey
WHERE a.employeekey IN (1219, 1325, 882, 545, 1064)  
  AND a.flaghours + b.clockhours > 0 
GROUP BY a.employeekey, a.thedate; -- for sort order  
   
SELECT * FROM zprod WHERE employeekey = 882

SELECT f.thedate, g.employeekey, SUM(g.flaghours) AS flaghours, SUM(g.clockhours) AS clockhours
FROM (  
  SELECT a.thedate, a.employeekey, a.seq - 14 AS begSeq, a.seq - 1 AS endSeq
  FROM zProd a
  WHERE a.seq > 14) f
LEFT JOIN zPRod g ON f.employeekey = g.employeekey
  AND g.seq BETWEEN f.begSeq AND f.EndSeq  
GROUP BY f.thedate, g.employeekey
ORDER BY g.employeekey, f.thedate

-- 7-16
-- revisiting the mind fuck of the vacation effect
-- why the fuck IS this giving me such big numbers
-- the JOIN BETWEEN #clock & #flag, include thedate
-- so this IS looking better
-- use straight numbers, OR fuck around LIKE zProd
-- look closer at vacation effect - turns out to be a red herring
-- this seems to be good
-- need to generalize AND ADD techtimeadjustments
SELECT a.thedate, a.employeekey, a.flaghours, b.clockhours, c.flag14, c.clock14,
  round(c.flag14/c.clock14, 2) * 100
FROM #flag a
LEFT JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey
LEFT JOIN (
  SELECT m.thedate, round(SUM(n.flaghours), 2) AS flag14, round(SUM(o.clockhours), 2) AS clock14
  FROM day m
  LEFT JOIN #flag n ON n.thedate BETWEEN m.thedate-14 AND m.thedate-1 
    AND n.employeekey = 1219
  LEFT JOIN #clock o ON o.thedate BETWEEN m.thedate-14 AND m.thedate-1 
    AND o.employeekey = 1219
    AND o.thedate = n.thedate
  WHERE m.thedate BETWEEN curdate() - 90 AND curdate()
  GROUP BY  m.thedate) c ON a.thedate = c.thedate
WHERE a.employeekey = 1219
  AND a.thedate > '05/01/2012'
  
SELECT m.thedate, n.*, o.*  
  FROM day m
  LEFT JOIN #flag n ON n.thedate BETWEEN m.thedate-14 AND m.thedate-1 
    AND n.employeekey = 1325
  LEFT JOIN #clock o ON o.thedate BETWEEN m.thedate-14 AND m.thedate-1 
    AND o.employeekey = 1325
    AND o.thedate = n.thedate
  WHERE m.thedate BETWEEN curdate() - 90 AND curdate()  
  
SELECT *
FROM #flag  
 