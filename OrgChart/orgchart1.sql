SELECT a.storecode, a.name, a.firstname, a.lastname, a.employeenumber, a.employeekey, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, ManagerName
FROM edwEmployeeDim a
WHERE a.currentrow = true
  AND a.Activecode = 'A'
ORDER BY storecode, pydeptcode, name
  
select distinct pym.ymname as "Name", pym.ymdept as "Department", jobd.yrtext as "Job Title",  pym.ymsex as "Gender"
from pymast pym
left join pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn) and phd.yrco# = 'RY1' -- job description (code)
left join pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) and jobd.yrco# = 'RY1' -- job description in plain english
where pym.ymco# = 'RY1'
and pym.ymactv <> 'T'
order by pym.ymname

SELECT *
FROM stgArkonaPYPRHEAD a
LEFT JOIN stgArkonaPYPRJOBD b on a.yrjobd = b.yrjobd 

--SELECT storecode, employeenumber FROM (
SELECT a.storecode, a.employeenumber,a.name, a.pydeptcode, a.pydept, a.distcode, 
  ManagerName, c.yrtext
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
LEFT JOIN stgArkonaPYPRJOBD c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
  AND c.yrtext <> '' -- gets rid of dup
WHERE a.currentrow = true 
  AND a.Activecode = 'A'
--) x GROUP BY storecode, employeenumber HAVING COUNT(*) > 1  
ORDER BY storecode, pydeptcode, yrtext, name


SELECT storecode, pydeptcode, pydept, yrtext AS job, max(managername) AS manager, COUNT(*) AS [Count]
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
LEFT JOIN stgArkonaPYPRJOBD c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
  AND c.yrtext <> '' -- gets rid of dup
WHERE a.currentrow = true 
  AND a.Activecode = 'A' 
GROUP BY storecode, pydeptcode, pydept, yrtext  
ORDER BY storecode, pydeptcode, yrtext  
