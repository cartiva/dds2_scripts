
SELECT a.ro, a.customername, a.vin, b.servicetype, SUM(coalesce(lineflaghours, 0)) AS flaghours
FROM factro a
LEFT JOIN factroline b ON a.ro = b.ro
  AND b.paytype = 'i'
WHERE CAST(createdts AS sql_date) BETWEEN '01/01/2012' AND curdate()  
GROUP BY a.ro, a.customername, a.vin, b.servicetype
HAVING SUM(coalesce(lineflaghours, 0)) <> 0


SELECT servicetype, COUNT(*)
FROM (
SELECT a.ro, a.customername, a.vin, b.servicetype, SUM(coalesce(lineflaghours, 0)) AS flaghours
FROM factro a
LEFT JOIN factroline b ON a.ro = b.ro
  AND b.paytype = 'i'
WHERE CAST(createdts AS sql_date) BETWEEN '01/01/2012' AND curdate()  
GROUP BY a.ro, a.customername, a.vin, b.servicetype
HAVING SUM(coalesce(lineflaghours, 0)) <> 0) x
GROUP BY servicetype


SELECT c.*, d.laborsales, d.partssales
FROM (
  SELECT max(a.ro) as ro, a.customername, a.vin, b.servicetype, SUM(coalesce(lineflaghours, 0)) AS flaghours
  FROM factro a
  LEFT JOIN factroline b ON a.ro = b.ro
    AND b.paytype = 'i'
    AND b.servicetype IN ('BS','MR','QL','RE')
  WHERE CAST(createdts AS sql_date) BETWEEN '01/01/2012' AND curdate()  
  GROUP BY a.customername, a.vin, b.servicetype
  HAVING SUM(coalesce(lineflaghours, 0)) <> 0) c
LEFT JOIN factro d ON c.ro = d.ro
ORDER BY d.vin