-- some basic opcode usage
DROP TABLE #opcode;
SELECT c.opcode AS opcode, d.opcode AS corcode, COUNT(*) AS theCount
INTO #opcode
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND year(thedate) = 2014
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey  
WHERE a.storecode = 'ry1'
  AND a.servicetypekey = (
  SELECT servicetypekey
  FROM dimservicetype
  WHERE servicetypecode = 'mr')
AND a.paymenttypekey = (
  SELECT paymenttypekey
  FROM dimPaymenttype
  WHERE paymenttypecode = 'c')  
GROUP BY c.opcode, d.opcode
HAVING COUNT(*) > 10

-- ADD IN retail/cost method/amount
SELECT a.*, b.sormth, b.soramt,c.sormth, c.soramt
FROM #opcode a
LEFT JOIN stgArkonaSDPLOPC b on a.opcode = b.solopc
LEFT JOIN stgarkonasdplopc c on a.corcode = c.solopc


-- labor sales on presumed menu item
SELECT ro, line, flaghours,laborsales
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND year(thedate) = 2014
WHERE a.storecode = 'ry1'
  AND a.servicetypekey = (
  SELECT servicetypekey
  FROM dimservicetype
  WHERE servicetypecode = 'mr')
AND a.paymenttypekey = (
  SELECT paymenttypekey
  FROM dimPaymenttype
  WHERE paymenttypecode = 'c')     
AND a.opcodekey = (
  SELECT opcodekey
  FROM dimopcode
  WHERE opcode = '10B')

-- look at flaghours * tech costing vs laborsales
SELECT a.ro, a.line, a.flaghours, a.laborsales, c.opcode, d.opcode, 
  e.name, e.laborcost, flaghours * laborcost, roDiscount
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
  AND year(thedate) = 2014
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN dimtech e on a.techkey = e.techkey  
WHERE a.storecode = 'ry1'
  AND coalesce(a.laborsales, 0) > 0
  AND a.servicetypekey = (
  SELECT servicetypekey
  FROM dimservicetype
  WHERE servicetypecode = 'mr')
AND a.paymenttypekey = (
  SELECT paymenttypekey
  FROM dimPaymenttype
  WHERE paymenttypecode = 'c')   

-- need to look at flag hours * pricing  
-- need pricing TABLE data SDPPRICW, shit, this IS something that i should have
-- been keeping ALL along AS type 2 fuck fuck fuck
-- for now i can fake it, before 10/5 cust pay MR = 107.91, 10/5 on cust pay MR = 119.1

SELECT a.ro, a.line, a.flaghours, a.laborsales, c.opcode, d.opcode, 
  e.name, 
  CASE 
    WHEN b.thedate < '10/05/2014' THEN round(a.flaghours * 107.1,2)
    ELSE round(a.flaghours * 119.1, 2)
  END AS compLaborSales, roDiscount
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
  AND year(thedate) = 2014
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN dimtech e on a.techkey = e.techkey  
WHERE a.storecode = 'ry1'
  AND coalesce(a.laborsales, 0) > 0
  AND a.servicetypekey = (
  SELECT servicetypekey
  FROM dimservicetype
  WHERE servicetypecode = 'mr')
AND a.paymenttypekey = (
  SELECT paymenttypekey
  FROM dimPaymenttype
  WHERE paymenttypecode = 'c');
  
-- labor sales <> computed labor sales  
select *
FROM (
  SELECT a.ro, a.line, a.flaghours, a.laborsales, c.opcode, d.opcode, 
    e.name, 
    CASE 
      WHEN b.thedate < '10/05/2014' THEN round(a.flaghours * 107.1,2)
      ELSE round(a.flaghours * 119.1, 2)
    END AS compLaborSales, rodiscount
  FROM factrepairorder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
    AND year(thedate) = 2014
  INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
  INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
  INNER JOIN dimtech e on a.techkey = e.techkey  
  WHERE a.storecode = 'ry1'
    AND coalesce(a.laborsales, 0) > 0
    AND a.servicetypekey = (
    SELECT servicetypekey
    FROM dimservicetype
    WHERE servicetypecode = 'mr')
  AND a.paymenttypekey = (
    SELECT paymenttypekey
    FROM dimPaymenttype
    WHERE paymenttypecode = 'c')) x
WHERE laborsales <> compLaborSales  