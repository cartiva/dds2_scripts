zalltheros has the data thru 5/26/12
tmps have data thru 6/1/12
so this IS a good enuf collection of data with which to work
use that data to generate:
DO a zalltheros FROM the tmp tables

dup structure of zalltheors: xfmRO

INSERT INTO xfmRO
SELECT sh.ptco#, sh.ptro#, sh.ptfran, sd.ptline, sd.ptseq#, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, sd.ptsvctyp, sd.ptlpym,
  CASE WHEN ptltyp = 'A' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS LineDate, 
  CASE WHEN ptltyp = 'L' AND ptcode = 'TT' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS FlagDate,
  sh.ptdate AS OpenDate, sh.ptcdat AS CloseDate, sh.ptfcdt AS FinalCloseDate,
  sd.ptlamt, sd.ptlopc, sd.ptcrlo, 'SDET' AS source, sh.ptswid, 'Closed' AS status

FROM tmpSDPRHDR sh
LEFT JOIN tmpSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
    AND gtpost = 'Y')  
  AND sh.ptfran <> '' 
AND sd.ptline IS NOT NULL; -- ros with no lines  )  

INSERT INTO xfmRO
SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, pd.ptline, pd.ptseq#, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, 
--  min(pd.ptdate) AS LineDate, 
  min(CASE WHEN ptltyp = 'A' THEN pd.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS LineDate, 
  min(CASE WHEN ptltyp = 'L' AND ptcode = 'TT' THEN pd.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS FlagDate,
  max(ph.ptdate) AS OpenDate, '12/31/9999' AS CloseDate, '12/31/9999' AS FinalCloseDate,
  0 as ptlamt, pd.ptlopc,
  pd.ptcrlo, 'PDET' AS source, ph.ptswid, ph.ptrsts
FROM tmpPDPPHDR ph
LEFT JOIN tmpPDPPDET pd ON ph.ptco# = pd.ptco#
  AND ph.ptpkey = pd.ptpkey
WHERE ph.ptdtyp = 'RO' 
  AND ph.ptdoc# <> ''
  AND ph.ptco# IN ('RY1','RY2','RY3')
  AND pd.ptdate <> '12/31/9999' -- this could be done IN the scrape: ptdate <> 0 
  AND NOT EXISTS (
    SELECT 1
    FROM xfmRO
    WHERE ptco# = ph.ptco#
      AND ptro# = ph.ptdoc#
      AND ptline = pd.ptline)
GROUP BY ph.ptco#, ph.ptdoc#, ph.ptfran, pd. ptline, pd.ptseq#, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptlopc, pd.ptcrlo, ph.ptswid, ph.ptrsts;

-- SELECT * FROM xfmRO
-- the kind of clean up i did to zalltheros IN etl - servicelinefact3.sql  
UPDATE xfmRO
SET pttech = '251'
WHERE pttech = '521'; 

--xfmro field list
SELECT ptco#, ptro#, ptfran, ptline, ptseq#, ptltyp, ptcode, 
  pttech, ptlhrs, ptsvctyp, ptlpym,
  LineDate, FlagDate, OpenDate, CloseDate, FinalCloseDate,
  ptlamt, ptlopc, ptcrlo, source, ptswid, status  
FROM xfmRO
  
i'm thinking i don't care about data integrity at the level of xfmro
that is simply the base for generating other facts
so what DO i get FROM xfmro

  
 
so flagged hour fact looks LIKE
 

SELECT * FROM xfmRO WHERE pttech IN ('m17','21','521','28'); 

SELECT ptco#, ptro#, ptline, ptltyp, ptcode, source
FROM xfmro
GROUP BY ptco#, ptro#, ptline, ptltyp, ptcode, source
HAVING COUNT(*) > 1
  
SELECT * 
FROM #jon  
WHERE status <> 'Closed'

-- does every line have an A row
SELECT *
FROM #jon z
WHERE NOT EXISTS (
    SELECT 1
    FROM #jon
    WHERE ptco# = z.ptco#
      AND ptro# = z.ptro#
      AND ptline = z.ptline
      AND ptltyp = 'A')

-- most are line 900, 950, 990
SELECT ptline, COUNT(*)
FROM #jon z
WHERE NOT EXISTS (
    SELECT 1
    FROM #jon
    WHERE ptco# = z.ptco#
      AND ptro# = z.ptro#
      AND ptline = z.ptline
      AND ptltyp = 'A')
GROUP BY ptline    

-- non 9xx lines
SELECT *
FROM #jon z
WHERE NOT EXISTS (
    SELECT 1
    FROM #jon
    WHERE ptco# = z.ptco#
      AND ptro# = z.ptro#
      AND ptline = z.ptline
      AND ptltyp = 'A')
  AND ptline IN (1,2,3,4)      

DO i even care about ros without an A line
i simply will NOT have the detail provided BY a lines ON those records

--------------------------------------------------------------------------------
Type Code  Yields                     Grain (store/ro)             
 A    CP||IS||SC||WS
            line creation date        line
            service type              line
            payment method            line 
            opcode                    line        
                                         
 L    TT    tech                      tech
            flag hours                tech, tech/line/flagdate (?)
 L    CR    
 M
 N
 Z       
    

-- zBaseROLine co ro line hours
-- DROP table zLine
--DELETE from zline
DROP TABLE zLine;
CREATE TABLE zLine (
  ptco# cichar(3),
  ptro# cichar(9),
  ptline integer,
  ptlhrs double,
  LineDate date) IN database;

INSERT INTO zLine
-- problem here, including pdet hours which are est NOT ptlhrs
SELECT ptco#, ptro#, ptline, 
  SUM(case when ptltyp = 'L' and ptcode = 'TT' then ptlhrs ELSE 0 END) AS LineHours, 
  MIN(LineDate) AS LineDate
FROM zAlltheRos
GROUP BY ptco#, ptro#, ptline;

-- oooh IS this the zline TABLE, yes, actually
-- except #jon IS zalltheros without the clean updone ON it
SELECT ptco#, ptro#, ptfran, ptline,
  max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
  MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
  MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
  MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
  SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours,
  MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode,
  status
-- SELECT *  
-- FROM xfmRO
FROM zalltheros
-- WHERE ptro# = '16088900'
GROUP BY ptco#, ptro#, ptfran, ptline, status

SELECT ptco#, ptro#, ptfran
FROM (
  SELECT ptco#, ptro#, ptfran, status
  FROM xfmRO
  GROUP BY ptco#, ptro#, ptfran, status) a
GROUP BY ptco#, ptro#, ptfran
HAVING COUNT(*) > 1  

SELECT *
FROM xfmro 
WHERE ptro# IN ('16086893','16090065','16090153','18016178','19104068')
ORDER BY ptro#, ptline

SELECT *
FROM #jon
WHERE ptro# = '16073492'
  AND ptline = 4

SELECT COUNT(*) -- 10697
FROM (
  select ptco#, ptro#
  from tmpsdprhdr
  GROUP BY ptco#, ptro#) a
SELECT COUNT(*) -- 1386
FROM (
  select ptco#, ptro#
  from tmpsdprhdr
  GROUP BY ptco#, ptro#) a
LEFT JOIN (
  select ptco#, ptro#
  from zalltheros
  GROUP BY ptco#, ptro#) b ON a.ptco# = b.ptco# AND a.ptro# = b.ptro#
WHERE b.ptco# IS NULL
ORDER BY a.

  
factRO
SELECT ptco#, ptro#, ptfran, opendate, closedate, finalclosedate, ptswid 
FROM zalltheros
GROUP BY ptco#, ptro#, ptfran, opendate, closedate, finalclosedate, ptswid 

SELECT ptco#, ptro#
FROM zalltheros
GROUP BY ptco#, ptro#

-- duplicate co/ro IN base ro grouping
SELECT ptco#, ptro#
FROM (
  SELECT ptco#, ptro#, ptfran, opendate, closedate, finalclosedate, ptswid 
  FROM zalltheros
  GROUP BY ptco#, ptro#, ptfran, opendate, closedate, finalclosedate, ptswid) a
GROUP BY ptco#, ptro#
HAVING COUNT(*) > 1 
-- results FROM the above dups
SELECT ptco#, ptro#, ptfran, opendate, closedate, finalclosedate, ptswid 
FROM zalltheros
WHERE ptro# IN ('16086893','16088101','16088509','16088864','16089023','16089121',
  '16089172','16089194','16089195','16089352',
  '18015035','19102795','19102795','19102795','19102795','19103051')
GROUP BY ptco#, ptro#, ptfran, opendate, closedate, finalclosedate, ptswid  

SELECT *
FROM zalltheros
WHERE ptro# IN ('16086893','16088101','16088509','16088864','16089023','16089121',
  '16089172','16089194','16089195','16089352',
  '18015035','19102795','19102795','19102795','19102795','19103051')
ORDER BY ptro#, ptline  
  
-- ALL appear to be IN the line 950 category
SELECT *
FROM zalltheros
WHERE ptltyp = 'N'

-- shit, thinking what are the absolute sources IN terms of typ/code for the RO level info
-- hazardous materials, shop supplies, sublet: line OR ro

-- duplicate co/ro IN base ro grouping
-- now there's only 1
SELECT ptco#, ptro#
FROM (
  SELECT ptco#, ptro#, ptfran, opendate, closedate, finalclosedate, ptswid 
  FROM zalltheros
  WHERE ptltyp IN ('A','L')
  GROUP BY ptco#, ptro#, ptfran, opendate, closedate, finalclosedate, ptswid) a
GROUP BY ptco#, ptro#
HAVING COUNT(*) > 1 

SELECT ptco#, ptro#, ptfran, opendate, closedate, finalclosedate, ptswid
SELECT *
FROM zalltheros
WHERE ptro# = '18015035'
  AND ptltyp IN ('A','L')

-- it brings back the questions of phdr vs shdr, currently inserts phdr records
-- IF the LINE does NOT exist IN SHDR
SELECT ptco#, ptro#
FROM (
  SELECT ptco#, ptro#, ptfran, opendate, min(closedate) AS CloseDate, finalclosedate, ptswid 
  FROM zalltheros
  WHERE ptltyp IN ('A','L')
  GROUP BY ptco#, ptro#, ptfran, opendate, finalclosedate, ptswid) a
GROUP BY ptco#, ptro#
HAVING COUNT(*) > 1 

CREATE TABLE factRO1 (
  storecode cichar(3),
  RO cichar(9),
  Franchise cichar(3),
  OpenDate date,
  CloseDate date,
  FinalCloseDate date,
  WriterID cichar(3));
  
INSERT INTO factRO1
SELECT ptco#, ptro#, ptfran, opendate, min(closedate) AS CloseDate, finalclosedate, ptswid 
FROM zalltheros
WHERE ptltyp IN ('A','L')
GROUP BY ptco#, ptro#, ptfran, opendate, finalclosedate, ptswid;

SELECT * FROM factRO1

ALTER TABLE factRO1
ADD COLUMN OpenDateKey integer
ADD COLUMN CloseDateKey integer
ADD COLUMN FinalCloseDateKey integer;

UPDATE factRO1
SET opendatekey = (SELECT datekey FROM day WHERE thedate = factRO1.opendate),
    closedatekey = (SELECT datekey FROM day WHERE thedate = factRO1.closedate),
    finalclosedatekey = (SELECT datekey FROM day WHERE thedate = factRO1.finalclosedate);
    
ALTER TABLE factRO1
DROP COLUMN opendate
DROP COLUMN closedate
DROP COLUMN finalclosedate;    

-- shit only, about 8 goofy honda ros FROM march 2012 that have NOT been closed yet
-- good enough
SELECT *
FROM zalltheros a
WHERE NOT EXISTS (
  SELECT 1
  FROM factRO1
  WHERE ro = a.ptro#)  
  
-- bingo  
SELECT storecode, ro 
FROM factRO1
GROUP BY storecode, ro
HAVING COUNT(*) > 1

SELECT * 
FROM factRO1

-- ros opened per day
SELECT thedate, theCount
FROM day a
LEFT JOIN  (
  SELECT opendatekey, COUNT(*) AS theCount
  FROM factRO1
  WHERE storecode = 'RY1'
  GROUP BY opendatekey) b ON a.datekey = b.opendatekey  
WHERE a.thedate BETWEEN '05/01/2011' AND curdate()  
  AND a.dayofweek BETWEEN 2 AND 6
  AND theCount IS NOT NULL 
  
  
-- oops, need any new techgroup stuff first
-- techgroup
-- this looks LIKE gold for updating BridgeTechGroup
-- done 6/11
INSERT INTO BridgeTechGroup (TechKey, WeightFactor)
SELECT DISTINCT e.techkey, e.weight
FROM (
  SELECT a.ptco#, a.ptro#, a.ptline, a.techkey, sum(round(a.techlinehours/b.linehours, 2)) AS weight 
  FROM (
    SELECT ptco#, ptro#, ptline, techkey, SUM(ptlhrs) AS techlinehours
    FROM xfmro c
    LEFT JOIN dimTech d ON c.ptco# = d.storecode
      AND c.pttech = d.technumber
    WHERE ptltyp = 'L'
      AND ptcode = 'tt'
      AND ptlhrs <> 0
      AND pttech <> ''
    GROUP BY ptco#, ptro#, ptline, techkey) a
  LEFT JOIN (
      SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS linehours
      FROM xfmro  
      WHERE ptltyp = 'L'
        AND ptcode = 'tt'
        AND ptlhrs <> 0
      GROUP BY ptco#, ptro#, ptline) b ON a.ptco# = b.ptco#  
    AND a.ptro# = b.ptro# 
    AND a.ptline = b.ptline
  WHERE linehours <> 0  
  GROUP BY a.ptco#, a.ptro#, a.ptline, a.techkey) e   
LEFT JOIN bridgetechgroup f ON e.techkey = f.techkey
  AND e.weight = f.weightfactor  
WHERE f.techkey IS NULL;   

-- a date has changed - UPDATE   
SELECT * -- 308
FROM (
  SELECT ptco#, ptro#, 
    (SELECT datekey FROM day WHERE thedate = opendate) AS opendatekey,
    (SELECT datekey FROM day WHERE thedate = closedate) AS closedatekey,
    (SELECT datekey FROM day WHERE thedate = finalclosedate) AS finalclosedatekey
  FROM (    
    SELECT ptco#, ptro#, opendate, min(closedate) AS CloseDate, finalclosedate
    FROM xfmro
    WHERE ptltyp IN ('A','L')
    GROUP BY ptco#, ptro#, ptfran, opendate, finalclosedate, ptswid) a) b
LEFT JOIN factRO1 c ON b.ptco# = c.storecode
  AND b.ptro# = c.ro 
WHERE (
    b.opendatekey <> c.opendatekey OR
    b.closedatekey <> c.closedatekey OR
    b.finalclosedatekey <> c.finalclosedatekey) 
-- done 6/11    
UPDATE c
SET opendatekey = b.opendatekey,
    closedatekey = b.closedatekey,
    finalclosedatekey = b.finalclosedatekey
FROM (
  SELECT ptco#, ptro#, ptfran,
    (SELECT datekey FROM day WHERE thedate = opendate) AS opendatekey,
    (SELECT datekey FROM day WHERE thedate = closedate) AS closedatekey,
    (SELECT datekey FROM day WHERE thedate = finalclosedate) AS finalclosedatekey,
    ptswid
  FROM (    
    SELECT ptco#, ptro#, ptfran, opendate, min(closedate) AS CloseDate, finalclosedate, ptswid 
    FROM xfmro
    WHERE ptltyp IN ('A','L')
    GROUP BY ptco#, ptro#, ptfran, opendate, finalclosedate, ptswid) a) b
LEFT JOIN factRO1 c ON b.ptco# = c.storecode
  AND b.ptro# = c.ro 
WHERE (
    b.opendatekey <> c.opendatekey OR
    b.closedatekey <> c.closedatekey OR
    b.finalclosedatekey <> c.finalclosedatekey) 
    
-- new records  
INSERT INTO factRO1 (storecode, ro, franchise, writerid, opendatekey, 
  closedatekey, finalclosedatekey)
SELECT b.ptco#, b.ptro#, b.ptfran, b.ptswid, b.opendatekey, b.closedatekey, 
  b.finalclosedatekey
--SELECT * -- 2520
FROM (
  SELECT ptco#, ptro#, ptfran,
    (SELECT datekey FROM day WHERE thedate = opendate) AS opendatekey,
    (SELECT datekey FROM day WHERE thedate = closedate) AS closedatekey,
    (SELECT datekey FROM day WHERE thedate = finalclosedate) AS finalclosedatekey,
    ptswid
  FROM (    
    SELECT ptco#, ptro#, ptfran, opendate, min(closedate) AS CloseDate, finalclosedate, ptswid 
    FROM xfmro
    WHERE ptltyp IN ('A','L')
    GROUP BY ptco#, ptro#, ptfran, opendate, finalclosedate, ptswid) a) b
LEFT JOIN factRO1 c ON b.ptco# = c.storecode
  AND b.ptro# = c.ro 
WHERE c.storecode IS NULL    

--6/12
these are the tests that will need to be run against xfmro nightly
line
flaghours

-- of course this should give results: fact has been updated, line has not
SELECT *
FROM factRO1 a
LEFT JOIN zline b ON a.storecode = b.ptco#
  AND a.ro = b.ptro#
WHERE b.ptco# is NULL   
/*
-- expected no results FROM this
-- got 1 2659010
SELECT *
from zline a
LEFT JOIN factRO1 b ON a.ptco# = b.storecode
  AND a.ptro# = b.ro
WHERE b.storecode IS NULL   

SELECT *
FROM factro1
WHERE ro = '2659010'

SELECT *
FROM stgArkonaSDPRHDR
WHERE ptro# = '2659010'

SELECT *
FROM stgArkonaPDPPHDR
WHERE ptdoc# = '2659010'

SELECT *
FROM stgArkonaPDPPDET
WHERE ptpkey = 112444

-- go ahead an whack it
delete
FROM zline
WHERE ptro# = '2659010'
*/

-- proposed generator sql:
SELECT ptco#, ptro#, ptfran, ptline,
  max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
  MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
  MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
  MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
  SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours,
  MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode,
  status
-- SELECT *  
--FROM xfmRO
INTO #wtf
FROM zalltheros
-- WHERE ptro# = '16088900'
GROUP BY ptco#, ptro#, ptfran, ptline, status

-- Line date
-- quite a few lines with multiple linedates
SELECT ptco#, ptro#, ptline
INTO #jon
FROM (
  SELECT ptco#, ptro#, ptline, linedate
  FROM zalltheros
  WHERE ptltyp = 'A'
  GROUP BY ptco#, ptro#, ptline, linedate) a
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

SELECT * FROM #jon
-- hmm, A lines with multiple linedates
-- fuck, they are ALL PDET generated, MAX that linedate takes care of it
SELECT *
FROM zalltheros a
WHERE EXISTS (
  SELECT 1
  FROM #jon
  WHERE ptro# = a.ptro# 
    AND ptline = a.ptline)
    
-- Line date

--Service Type
-- lines with multiple serv type
-- 1: 16058003 line 3
SELECT ptco#, ptro#, ptline
FROM (
  SELECT ptco#, ptro#, ptline, ptsvctyp
  FROM zalltheros
  WHERE ptltyp = 'A'
  GROUP BY ptco#, ptro#, ptline, ptsvctyp) a
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

SELECT *
FROM zalltheros
WHERE ptro# = '16058003'

DELETE FROM zalltheros WHERE ptro# = '16058003' AND ptline = 3 AND ptcode = 'WS'
-- Service Type

-- pay type
SELECT ptco#, ptro#, ptline
FROM (
  SELECT ptco#, ptro#, ptline, ptlpym
  FROM zalltheros
  WHERE ptltyp = 'A'
  GROUP BY ptco#, ptro#, ptline, ptlpym) a
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

-- shit, only one
-- of course, it's pdet, MAX takes care of it
SELECT *
FROM zalltheros
WHERE ptro# = '16088669'
-- pay type

-- opcode
-- ha, the same one FROM paytype, MAX to the rescue
SELECT ptco#, ptro#, ptline
FROM (
  SELECT ptco#, ptro#, ptline, ptlopc
  FROM zalltheros
  WHERE ptltyp = 'A'
  GROUP BY ptco#, ptro#, ptline, ptlopc) a
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1
-- opcode

-- corcode
-- 0
SELECT ptco#, ptro#, ptline
FROM (
  SELECT ptco#, ptro#, ptline, ptcrlo
  FROM zalltheros
  WHERE ptltyp = 'A'
  GROUP BY ptco#, ptro#, ptline, ptcrlo) a
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1
-- corcode

-- we are fucking good to go

SELECT ptco#, ptro#, ptfran, ptline,
  max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
  MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
  MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
  MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
  SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours,
  MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode
-- SELECT *  
--FROM xfmRO
INTO #wtf
FROM zalltheros
-- WHERE ptro# = '16088900'
GROUP BY ptco#, ptro#, ptfran, ptline

SELECT * FROM #wtf WHERE linedate IS NULL -- ALL the 900s, NOT a problem 
i''m building a fact, no place for ALL those NULL fields
so does it need to be
SELECT COUNT(*) FROM #wtf --740371
SELECT COUNT(*) -- 221443
FROM #wtf
WHERE linedate IS NOT NULL
  AND servtype IS NOT NULL 
  AND paytype IS NOT NULL
  AND opcode IS NOT NULL
  AND corcode IS NOT NULL 
  
SELECT *
FROM #wtf
WHERE (
  linedate IS NULL OR
  servtype IS NULL OR
  paytype IS NULL OR
  opcode IS NULL) OR
  corcode IS NULL)  
ORDER BY linedate desc  

-- nonononono don't fucking exclude data,
-- coalesce to N/A
-- no goddamnit, those are NULL fields IN the line fact
-- so fucking go ahead AND exclude the data for now, let's see what happens

SELECT *
FROM (
  SELECT ptco#, ptro#, ptfran, ptline,
    max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
    MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
    MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode,
    SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours
  FROM zalltheros
  GROUP BY ptco#, ptro#, ptfran, ptline) a
WHERE linedate IS NOT NULL
  AND servtype IS NOT NULL 
  AND paytype IS NOT NULL
  AND opcode IS NOT NULL
  AND corcode IS NOT NULL   

DROP TABLE factROLine;
CREATE TABLE factROLine ( 
      StoreCode CIChar( 3 ),
      RO CIChar( 9 ),
      Line Integer,
      LineDateKey Integer,
      ServiceType CIChar( 2 ),
      PayType CIChar( 1 ),
      OpCode CIChar( 10 ),
      CorCode CIChar( 10 ),
      LineFlagHours Double( 15 )) IN DATABASE;
  
INSERT INTO factROLine
SELECT ptco#, ptro#, ptline,
  (SELECT datekey FROM day WHERE thedate = linedate),
  ServType, PayType, OpCode, coalesce(CorCode, 'N/A'), LineHours
FROM (
  SELECT ptco#, ptro#, ptline,
    max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
    MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
    MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode,
    SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours
  FROM zalltheros
  GROUP BY ptco#, ptro#, ptline) a
WHERE linedate IS NOT NULL
  AND servtype IS NOT NULL 
  AND paytype IS NOT NULL
  AND opcode IS NOT NULL;  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factROLine',
   'factROLine.adi',
   'NK',
   'StoreCode;RO;Line',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'factROLine',
   'factROLine.adi',
   'RO',
   'RO',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'factROLine',
   'factROLine.adi',
   'LINE',
   'Line',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'factROLine', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'factROLinefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'factROLine', 
   'Table_Primary_Key', 
   'NK', 'APPEND_FAIL', 'factROLinefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'factROLine', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'factROLinefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'factROLine', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'factROLinefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'factROLine', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'factROLinefail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factROLine', 
      'StoreCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROLinefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factROLine', 
      'RO', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROLinefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factROLine', 
      'Line', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROLinefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factROLine', 
      'LineDateKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROLinefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factROLine', 
      'ServiceType', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROLinefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factROLine', 
      'PayType', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROLinefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factROLine', 
      'OpCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROLinefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factROLine', 
      'CorCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROLinefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factROLine', 
      'LineFlagHours', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROLinefail' );   
  
SELECT *
FROM factROLine a
LEFT JOIN factRO1 b ON a.storecode = b.storecode
  AND a.ro = b.ro
WHERE b.ro IS NULL   

-- whole fucking bunch of ros with no line records
-- coalescing corcode helped 
-- but there are more
SELECT *
FROM factRO1 a
LEFT JOIN factROLine b ON a.storecode = b.storecode
  AND a.ro = b.ro
WHERE b.ro IS NULL   

-- maybe it's the data updated recently
-- that's the vast majority, maybe even ALL of it, 9 suspect records opened IN mar 2012
-- go ahead AND UPDATE factROLine with xfmro
yep, that''s the bulk of it
SELECT thedate, COUNT(*)
FROM factRO1 a
LEFT JOIN day c ON a.opendatekey = c.datekey
LEFT JOIN factROLine b ON a.storecode = b.storecode
  AND a.ro = b.ro
WHERE b.ro IS NULL  
GROUP BY thedate

-- an issue IS corrections codes, many lines won't have one, sometimes the correction
-- IS freeform text FROM SDPRTXT
so, coalesce corcode to 'N/A'

-- go ahead AND UPDATE factROLine with xfmro
-- added NULL constraints AND unique indexes to xfmROLine
-- should probably DO the same for xfmRO
-- fail early fail hard
INSERT INTO xfmROLine
SELECT ptco#, ptro#, ptline,
  (SELECT datekey FROM day WHERE thedate = linedate) AS linedatekey,
  ServType, PayType, OpCode, coalesce(CorCode, 'N/A') AS CorCode, LineHours
FROM (
  SELECT ptco#, ptro#, ptline,
    max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
    MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
    MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode,
    SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours
  FROM xfmRO
  GROUP BY ptco#, ptro#, ptline) a
WHERE linedate IS NOT NULL
  AND servtype IS NOT NULL 
  AND paytype IS NOT NULL
  AND opcode IS NOT NULL; 
-- new records 

INSERT INTO factROLine 
SELECT a.*
FROM xfmroline a
LEFT JOIN factroline b ON a.storecode = b.storecode
  AND a.ro = b.ro
  AND a.line = b.line
WHERE b.storecode IS NULL;   

-- updates
UPDATE b
SET linedatekey = a.linedatekey, 
    servicetype = a.servicetype, 
    paytype = a.paytype,
    opcode = a.opcode,
    corcode = a.corcode,
    lineflaghours = a.lineflaghours
 -- SELECT a.*
  FROM xfmroline a
  LEFT JOIN factroline b ON a.storecode = b.storecode
    AND a.ro = b.ro
    AND a.line = b.line
  WHERE (
    a.linedatekey <> b.linedatekey OR 
    a.servicetype <> b.servicetype OR
    a.paytype <> b.paytype OR
    a.opcode <> b.opcode OR
    a.corcode <> b.corcode OR
    a.lineflaghours <> b.lineflaghours);  

-- this IS ok
  
SELECT *
FROM factROLine a
LEFT JOIN factRO1 b ON a.storecode = b.storecode
  AND a.ro = b.ro
WHERE b.ro IS NULL   

-- this IS not    
SELECT *
FROM factRO1 a
LEFT JOIN factROLine b ON a.storecode = b.storecode
  AND a.ro = b.ro
WHERE b.ro IS NULL  
 
-- IN RO, NOT IN ROLine
-- how the fuck did these get IN factRO IF they are NOT IN zlltheros OR xfmro?
SELECT *
FROM stgarkonasdprhdr
WHERE ptro# IN ('16089912','16090127','16090191','16090201','16090225','18016575','18016685','16090346')
SELECT *
FROM stgarkonasdprdet
WHERE ptro# IN ('16089912','16090127','16090191','16090201','16090225','18016575','18016685','16090346')

i''m going to go ahead and whack these
will check it again 6/13 after a synchronized UPDATE runs
/*
fRO              fROLine                                                                                  
store |        | store   |
ro    |-|----o<| ro      |
      |        | line    |
*/      


SELECT storecode, ro
FROM factROLine a

-- 6/13 
-- just the same ones AS yesterday
SELECT *
FROM factro1 a
WHERE NOT EXISTS (
   SELECT 1
   FROM factroline
   WHERE storecode = a.storecode
     AND ro = a.ro)

SELECT *
FROM stgArkonaSDPRHDR
WHERE ptro# IN ('16089912','16090127','16090191','16090201','16090225','18016575','18016685','16090346') 

voided: 16090225, 16090201, 18016685, 18016575, 16090191
leaving 16090127, 16089912, 16090346

SELECT *
FROM xfmro
WHERE ptro# IN ('16090127', '16089912', '16090346')

SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# IN ('16090127', '16089912', '16090346')

-- aha, NOT void ros, but no charge ros
SELECT *
FROM stgArkonaSDPRHDR sh
LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
--  AND EXISTS ( --
--    SELECT 1
--    FROM stgArkonaGLPTRNS
--    WHERE gtdoc# = sh.ptro#
--    AND gtpost = 'Y')  
  AND sh.ptfran <> '' 
AND sd.ptline IS NOT NULL
and sh.ptro# IN ('16090127', '16089912', '16090346')

SELECT *
FROM stgArkonaSDPRHDR a
WHERE ptdate > '07/01/2009'
  AND ptcnam <> '*VOIDED REPAIR ORDER*'
  AND NOT EXISTS (
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = a.ptro#
      AND gtpost = 'Y')
      
-- uh oh, void ro, according to 5250   
-- no record IN glptrns
-- but ptcnam <> '*VOIDED REPAIR ORDER*'   
SELECT *
FROM stgArkonaSDPRHDR  
WHERE ptro# = '16056628'    

-- ptchk# ??
-- maybe, but NOT conclusive
SELECT ptco#, ptro#, ptrtch, ptcnam, ptdate, ptcdat, ptfcdt, pttest, ptchk#, ptltot, ptptot, ptstot
FROM stgArkonaSDPRHDR
WHERE ptchk# LIKE 'V%'
ORDER BY ptro#

SELECT ptro#, ptchk#, b.*
FROM stgArkonaSDPRHDR a
LEFT JOIN stgArkonaGLPTRNS b ON a.ptro# = b.gtdoc#
WHERE a.ptchk# LIKE 'V%'

-- quick grab of some labor sales acct#
SELECT *
FROM stgARkonaglptrns
WHERE gtdoc# like '1604938%'
  AND gtacct IN (
    SELECT account
    FROM edwAccountDim
    WHERE storecode = 'ry1'
      AND accounttype = '4'
      AND gldepartment = 'service')

SELECT DISTINCT ptchk# FROM stgArkonaSDPRHDR
-- 2 issues 
--   void ros
--   ros with no charge

-- < void ros
-- 1. ptcnam = *VOIDED REPAIR ORDER*, OR ptchk# LIKE V%
-- 2. ro becomes void after initial scrape  
-- 3. "void" ros with glptrns entries

-- 2. ro becomes void after initial scrape  
-- ALL ro fact tables will have to be updated, including aggregates
SELECT *
FROM stgArkonaSDPRHDR
WHERE ptcnam = '*VOIDED REPAIR ORDER*'  

SELECT *
FROM factro1 a
INNER JOIN (
  SELECT *
  FROM stgArkonaSDPRHDR
  WHERE ptcnam = '*VOIDED REPAIR ORDER*') b ON a.ro = b.ptro#
  
SELECT *
FROM factroline a
INNER JOIN (
  SELECT *
  FROM stgArkonaSDPRHDR
  WHERE ptcnam = '*VOIDED REPAIR ORDER*') b ON a.ro = b.ptro#  
-- 2.ro becomes void after initial scrape    
-- /> void ros