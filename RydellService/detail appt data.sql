SELECT created, customer, promisetime,
  replace(trim(left(CAST(notes AS sql_char), 500)),'Customer States:','')
FROM appointments
WHERE advisorid = 'Detail'


-- CAST the result of the double replace AS sql_char
-- nested replace to get rid of "Customer States:" AND cr/lf

SELECT a.*, CAST(squawk AS sql_char)
FROM (
  SELECT a.created, createdbyid, customer, promisetime, starttime, 
    modelyear, TRIM(make) + '' + TRIM(model) AS vehicle, 
    vin, licensenumber, 
    replace(trim(replace(trim(notes),CHAR(13)+char(10),'')),'Customer States:','') AS squawk
  FROM appointments a
  WHERE advisorid = 'Detail'
  AND length(trim(cast(notes AS sql_char))) > 100) a

SELECT *  
  FROM appointments a
  WHERE advisorid = 'Detail'
  AND length(trim(cast(notes AS sql_char))) > 100  
  AND CAST(created AS sql_date) > curdate() - 90
  
  
SELECT appointmentid,created,createdbyid,starttime, promisetime, status,ronumber,customer,
  trim(replace(trim(replace(trim(notes),CHAR(13)+char(10),'')),'Customer States:','')) AS squawk,
  modelyear, make, model,vin,comments, techcomments
FROM appointments a  
WHERE advisorid = 'Detail'
AND CAST(created AS sql_date) > curdate() - 90
ORDER BY customer, created

-- NO rows IN AppointmentStatuses
SELECT a.appointmentid,a.created,a.createdbyid,a.customer,a.vin, b.*
FROM appointments a  
LEFT JOIN appointmentstatuses b ON a.appointmentid = b.appointmentid
WHERE advisorid = 'Detail'
AND CAST(created AS sql_date) > curdate() - 360
  AND b.appointmentid IS NOT NULL 

-- there are statuses, but, looks they are NULL OR Advisor_StatusCallMade only
-- but the comments & performedby may be usedful
SELECT a.appointmentid,a.created,a.createdbyid,a.customer,a.vin, a.ronumber,
  b.performedby, b.comments, b.datetime, c.categorystatus
FROM appointments a  
LEFT JOIN statuses b ON a.appointmentid = b.appointmentid
LEFT JOIN categoryStatuses c ON b.categorystatusid = c.categorystatusid
WHERE advisorid = 'Detail'
AND CAST(a.created AS sql_date) > curdate() - 120
ORDER BY created

-- IS this everything i need?
SELECT a.appointmentid,a.created,a.createdbyid,a.starttime, a.promisetime, 
  a.status,a.ronumber,a.customer,
  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')) AS squawk,
  a.modelyear, a.make, a.model,a.vin,a.comments, length(trim(a.comments)), a.techcomments,
  b.performedby, b.comments, b.datetime
FROM appointments a  
LEFT JOIN statuses b ON a.appointmentid = b.appointmentid
WHERE advisorid = 'Detail'
AND CAST(created AS sql_date) > curdate() - 90
ORDER BY created

-- i don't fucking understand what IS an appt for what day
-- looks LIKE starttime IS the appointment day/time
SELECT a.appointmentid,a.created,a.createdbyid,a.starttime, a.promisetime, 
  a.status,a.ronumber,a.customer,
  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')) AS squawk,
  a.modelyear, a.make, a.model,a.vin,a.comments, length(trim(a.comments)), a.techcomments,
  b.performedby, b.comments, b.datetime
FROM appointments a  
LEFT JOIN statuses b ON a.appointmentid = b.appointmentid
WHERE advisorid = 'Detail'
AND CAST(created AS sql_date) > curdate() - 90
AND customer LIKE '%KIM%'

SELECT a.appointmentid,a.created,a.createdbyid,a.starttime, a.promisetime, 
  a.status,a.ronumber,a.customer,
  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')) AS squawk,
  a.modelyear, a.make, a.model,a.vin,a.comments, length(trim(a.comments)), a.techcomments,
  b.performedby, b.comments, b.datetime
FROM appointments a  
LEFT JOIN statuses b ON a.appointmentid = b.appointmentid
WHERE advisorid = 'Detail'
AND CAST(starttime AS sql_date) = curdate() - 1


-- does a new advisorstatus (appointments.status) CREATE a new Appointment (object)?
-- shit, who cares, there IS a created for each new appointmentid
SELECT a.ronumber, a.status, COUNT(*)
FROM appointments a
WHERE CAST(created AS sql_date) > curdate() - 90
  AND ronumber <> ''
GROUP BY a.ronumber, a.status
HAVING COUNT(*) > 1
-- shit, who cares, there IS a created for each new appointmentid
SELECT a.appointmentid,a.created,a.createdbyid,a.starttime, a.promisetime, 
  a.status,a.ronumber,a.customer,
  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')) AS squawk,
  a.modelyear, a.make, a.model,a.vin,a.comments, length(trim(a.comments)), 
  a.techcomments
FROM appointments a 
WHERE ronumber = '16115921'


SELECT a.ronumber, a.status, COUNT(*)
FROM appointments a
WHERE CAST(created AS sql_date) > curdate() - 90
  AND ronumber <> ''
GROUP BY a.ronumber, a.status
HAVING COUNT(*) > 1

-- appointments for today AND the future
SELECT a.appointmentid,a.created,a.createdbyid,a.starttime, a.promisetime, 
  a.status,a.ronumber,a.customer,
  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')) AS squawk,
  a.modelyear, a.make, a.model,a.vin,a.comments, length(trim(a.comments)), a.techcomments,
  b.performedby, b.comments, b.datetime
FROM appointments a  
LEFT JOIN statuses b ON a.appointmentid = b.appointmentid
WHERE advisorid = 'Detail'
AND CAST(starttime AS sql_date) >= curdate() 

SELECT DISTINCT(length(trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')))) AS squawk
FROM appointments a
WHERE advisorid = 'Detail'

-- candidate keys
SELECT created, customer FROM (
SELECT a.appointmentid,a.created,a.createdbyid,a.starttime, a.promisetime, 
  a.status,a.ronumber,a.customer,
  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')) AS squawk,
  a.modelyear, a.make, a.model,a.vin,a.comments, length(trim(a.comments)), a.techcomments
FROM appointments a  
WHERE advisorid = 'Detail'
) x GROUP BY created,customer HAVING COUNT(*) > 1


-- for scrape code (to populate dds.SchedulerDetailAppointments
SELECT a.appointmentid,a.created,a.createdbyid,a.starttime, a.promisetime, 
  a.status,a.ronumber,a.customer,
  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')) AS squawk,
  a.modelyear, a.make, a.model,a.vin,a.comments,  a.techcomments
FROM appointments a  
WHERE advisorid = 'Detail'
  AND CAST(starttime AS sql_date) >= curdate()
