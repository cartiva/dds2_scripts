SELECT appointmentid, status, ronumber, vin, promisetime, starttime, heavyduration, maintenanceduration,
  driveabilityduration, arrivaltime, tocreleasetime, tocfinishtime, technicianassignedid, alignment,
  techstatus
--SELECT *  
FROM appointments


SELECT status, MIN(starttime), MAX(starttime), COUNT(status)  
FROM appointments
GROUP BY status

what IS the natural key of appointments

SELECT techstatus, min(created), max(created),COUNT(*) 
FROM appointments
GROUP BY techstatus


SELECT ronumber, COUNT(*)
FROM appointments
GROUP BY ronumber
HAVING COUNT(*) > 1

-- wtf, it's a start
-- surprisingly few dups here
SELECT status, ronumber, vin, promisetime, starttime, heavyduration, maintenanceduration,
  driveabilityduration, arrivaltime, tocreleasetime, tocfinishtime, technicianassignedid, alignment,
  techstatus
FROM appointments
GROUP BY status, ronumber, vin, promisetime, starttime, heavyduration, maintenanceduration,
  driveabilityduration, arrivaltime, tocreleasetime, tocfinishtime, technicianassignedid, alignment,
  techstatus
HAVING COUNT(*) > 1  
-- deal with/understand the dups
-- THEN cut out some fields
-- rinse AND repeat

SELECT COUNT(*) -- 24599, no help 
FROM appointments
WHERE starttime > promisetime

SELECT status, ronumber, vin, promisetime, starttime, heavyduration, maintenanceduration,
  driveabilityduration, arrivaltime, tocreleasetime, tocfinishtime, technicianassignedid, alignment,
  techstatus
FROM appointments 
WHERE ronumber = ''


-- statuses table
SELECT a.*, b.CategoryStatus
FROM statuses a
LEFT JOIN categoryStatuses b ON a.CategoryStatusID = b.CategoryStatusID

-- datetime needed for unique
SELECT appointmentid, categorystatusid, datetime
from statuses
GROUP BY appointmentid, categorystatusid, datetime
HAVING COUNT(*)>1

-- appointments with multiple records IN Statuses, beaucoup
SELECT a.appointmentid, b.categorystatus, a.performedby, a.comments, a.datetime
FROM statuses a
LEFT JOIN categoryStatuses b ON a.CategoryStatusID = b.CategoryStatusID
WHERE appointmentid IN (
  SELECT appointmentid
  from statuses
  GROUP BY appointmentid, categorystatusid 
  HAVING COUNT(*)>1)
ORDER BY appointmentid, datetime  

-- appointmentstatus
SELECT a.appointmentid, b.categoryStatus, a.fromtimestamp, a.thrutimestamp
FROM AppointmentStatuses a
LEFT JOIN CategoryStatuses b ON a.categorystatusid = b.categorystatusid
WHERE CAST(fromtimestamp AS sql_date) > '12/31/2011'
ORDER BY appointmentid, fromtimestamp

select *
FROM appointments
WHERE appointmentid = '[0eb4581a-e970-034d-af32-019c5527e960]'

SELECT x.appointmentid, x.created, x.status, x.ronumber, x.vin, x.arrivaltime, 
  x.starttime, x.technicianassignedid, x.techstatus, x.heavyduration, 
  x.maintenanceduration, x.driveabilityduration,  
  b.categoryStatus AS asStatus, a.fromtimestamp AS asFromTS, a.thrutimestamp AS asThruTS,

  c.performedby AS sBy, c.comments AS sComments, c.datetime AS sTS, y.categorystatus AS sStatus,
  d.advisorNumber, e.name, f.description
FROM appointments x
LEFT JOIN AppointmentStatuses a ON x.appointmentid = a.appointmentid
LEFT JOIN CategoryStatuses b ON a.categorystatusid = b.categorystatusid
LEFT JOIN Statuses c ON x.appointmentid = c.appointmentid
LEFT JOIN CategoryStatuses y ON c.categorystatusid = y.categorystatusid
LEFT JOIN advisors d ON x.AdvisorID = d.ShortName
LEFT JOIN teams e ON d.teamid = e.teamid
LEFT JOIN departments f ON e.departmentid = f.departmentid
WHERE CAST(fromtimestamp AS sql_date) > '03/31/2012'
ORDER BY a.appointmentid, a.fromtimestamp

SELECT *
FROM advisors a
LEFT JOIN teams b ON a.teamid = b.teamid
LEFT JOIN departments c ON b.departmentid = c.departmentid

-- appointmentstatuses
SELECT z.appointmentid, z.starttime, z.ronumber, z.vin, z.status, z.created, c.categorystatus, c.fromtimestamp, c.thrutimestamp
FROM appointments z
LEFT JOIN (
  SELECT a.AppointmentID, b.CategoryStatus, a.FromTimestamp, a.ThruTimestamp
  FROM appointmentstatuses a
  LEFT JOIN categorystatuses b ON a.CategoryStatusID = b.CategoryStatusID) c ON z.appointmentid = c.appointmentid
WHERE CAST(z.starttime AS sql_date) between '03/31/2012' AND curdate()

SELECT b.AppointmentID, c.CategoryStatus as Status, 
  b.FromTimestamp as FromTS, b.ThruTimestamp as ThruTS
FROM Appointments a  
LEFT JOIN appointmentstatuses b on a.AppointmentID = b.AppointmentID
LEFT JOIN categorystatuses c ON b.CategoryStatusID = c.CategoryStatusID 
WHERE CAST(a.starttime AS sql_date) between '03/31/2012' AND curdate()
  and b.appointmentid is not null 
    
-- statuses
SELECT z.appointmentid, z.starttime, z.ronumber, z.vin, z.status, z.created, 
  c.categorystatus, c.datetime
FROM appointments z
LEFT JOIN (
  SELECT *
  FROM statuses a
  LEFT JOIN categorystatuses b ON a.categorystatusid = b.categorystatusid) c ON z.appointmentid = c.appointmentid
WHERE CAST(z.starttime AS sql_date) between '03/31/2012' AND curdate()

SELECT b.appointmentid, b.performedby, b.comments, 
  b.datetime as TS, c.categorystatus as Status
from Appointments a
LEFT JOIN statuses b on a.AppointmentID = b.AppointmentID
LEFT JOIN categorystatuses c ON b.categorystatusid = c.categorystatusid
WHERE CAST(a.starttime AS sql_date) between '03/31/2012' AND curdate()
  and b.appointmentid is not null 

-- appointments - key fields
SELECT partsstatus, COUNT(*) FROM appointments GROUP BY partsstatus
SELECT * FROM appointments WHERE partsstatus = 'Parts Ordered'
SELECT teamid, COUNT(*) FROM appointments GROUP BY teamid
SELECT * FROM appointments ORDER BY  starttime desc


-- fuck yeah, these are unique
SELECT created, createdbyid,  advisorid, status, ronumber, vin,
  starttime, heavyduration,
  maintenanceduration, driveabilityduration, teamid, arrivaltime, 
  tocreleasetime, tocfinishtime
FROM appointments  
WHERE CAST(starttime AS sql_date) between '03/31/2012' AND curdate()
GROUP BY created, createdbyid,  advisorid, status, ronumber, vin,
  starttime, heavyduration,
  maintenanceduration, driveabilityduration, teamid, arrivaltime, 
  tocreleasetime, tocfinishtime
HAVING COUNT(*) > 1  

-- so this becomes the base extract
SELECT appointmentid, created, createdbyid, a.status, ronumber, vin,
  starttime, heavyduration,
  maintenanceduration, driveabilityduration, a.teamid, arrivaltime, 
  tocreleasetime, tocfinishtime, b.advisornumber
FROM appointments a 
LEFT JOIN advisors b ON a.advisorid = b.shortname
WHERE CAST(starttime AS sql_date) between '03/31/2012' AND curdate()

SELECT DISTINCT teamid FROM appointments

-- do statuses &