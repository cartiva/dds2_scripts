ALTER TABLE day
ADD COLUMN BiWeeklyPayPeriodStartDate date
ADD COLUMN BiWeeklyPayPeriodEndDate date  

UPDATE day
  SET BiWeeklyPayPeriodStartDate = x.startdate,
      BiWeeklyPayPeriodEndDate = x.enddate
FROM (
  SELECT a.thedate, c.startdate, b.enddate
  FROM day a
  LEFT JOIN (
    SELECT thedate AS enddate
    FROM day
    WHERE mod(cast(ABS(timestampdiff(sql_tsi_day, thedate, '01/28/2012')) AS sql_integer), 14) = 0) b ON a.thedate <= b.enddate
      AND a.thedate > b.enddate - 14
  LEFT JOIN (    
    SELECT thedate AS startdate
    FROM day
    WHERE mod(cast(ABS(timestampdiff(sql_tsi_day, thedate, '01/15/2012')) AS sql_integer), 14) = 0) c ON a.thedate >= c.startdate
      AND a.thedate < c.startdate + 14) x 
WHERE day.thedate = x.thedate   