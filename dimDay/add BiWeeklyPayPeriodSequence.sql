ALTER TABLE day
ADD COLUMN BiWeeklyPayPeriodSequence integer


UPDATE day
SET BiWeeklyPayPeriodSequence = x.PayPeriod
from dimBwpayperiod x
WHERE day.BiWeeklyPayPeriodStartDate = x.FromDate

select * FROM day WHERE yearmonth = 201311