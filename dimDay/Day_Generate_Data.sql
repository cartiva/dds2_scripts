-- 12/31/11 added data going back to 1926 t
-- SELECT timestampdiff(sql_tsi_day, '01/01/1926', '01/01/2004') FROM system.iota

DECLARE @i integer;
DECLARE @Date date;
DECLARE @tf logical;
@Date = '12/31/1925'; -- starting date - 1
@i = 1;
TRY 
--  WHILE @i < 7305 DO -- 
  WHILE @i < 28490 DO -- adding dates back to 1926 to accomodate birth dates 
    @tf = iif(month((@Date + @i) + 1) = month(@Date + @i), false, true);
    INSERT INTO Day (TheDate, DayOfWeek, DayName, DayOfMonth, DayOfYear,
      MonthOfYear, MonthName, FirstDayOfMonth, LastDayOfMonth, WeekDay, Weekend,
	  Holiday, TheYear, ISOWeek, DateType)
    values(
      @Date + @i,
      DayOfWeek(@Date + @i),
      DayName(@Date + @i),
      DayOfMonth(@Date + @i),
      DayOfYear(@Date + @i),
      Month(@Date + @i),
      MonthName(@Date + @i),
      case 
        when DayOfMonth(@Date + @i) = 1 THEN true 
        ELSE false
      END,
      @tf,
      case 
        when DayOfWeek NOT IN (1,7) THEN True 
        ELSE false
      end,
      case 
        when DayOfWeek IN (1,7) THEN true 
        ELSE false
      END,
	  NULL, 
	  Year(@Date + @i),
	  ISOWeek(@Date),
	  'DATE');
    @i = @i + 1;
  END WHILE; 
CATCH ALL
  RAISE;
END TRY;      
/*
SELECT MAX(dayofmonth(curdate())) FROM system.iota

@Date = '12/31/2009'; -- starting date - 1
select 

SELECT dayofweek(curdate()) FROM system.iota

SELECT DayOfMonth(DATEADD(DayOfMonth(curdate(),-1,DATEADD(MONTH(curdate()),1,DATEADD(DayOfMonth(curdate()),1-DayOfMonth((curdate()),curdate())))) FROM system.iota

Add one to the month, change the day to 1, subtract 1 day

@Date = '12/31/2009';
SELECT month(curdate()) FROM system.iota
SELECT (month(curdate()) + 1) FROM system.iota

-- first day of next month
select createtimestamp(year(curdate()), (month(curdate()) + 1), 1, 0,0,0,0) FROM system.iota

SELECT DayOfMonth(TimeStampAdd(sql_tsi_day, -1, createtimestamp(year(curdate()), (month(curdate()) + 1), 1, 0,0,0,0))) FROM system.iota

SELECT timestampadd(sql_tsi_day, -1, (month(curdate()) + 1)) FROM system.iota

IF curdate() + 1

ADD one day, IF the month IS different it IS the last day of the month

*/