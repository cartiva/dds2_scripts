/* 12/31/11
-- ADD DateType COLUMN
-- had to make most of the columns nullable
ALTER TABLE Day
ADD COLUMN DateType cichar(12)
UPDATE Day
SET DateType = 'DATE'

INSERT INTO Day (TheDate, DateType) values('12/31/9999', 'N/A')

1/5/12 -- addcolumn SundayToSaturdayWeek

CREATE TABLE __zSundays (
  thedate date,
  xx autoinc) IN database;
  
INSERT INTO __zSundays (thedate)
SELECT thedate
FROM day
WHERE dayofweek = 1; 
  
ALTER TABLE day
ADD COLUMN SundayToSaturdayWeek integer;

UPDATE c
SET SundayToSaturdayWeek = w.xx
FROM day c 
LEFT JOIN __zSundays w ON c.thedate >= w.thedate
  AND c.thedate < w.thedate + 7
  
DROP TABLE __zSundays;  

*/
CREATE TABLE Day ( 
      DateKey AutoInc,
      TheDate Date,
      DayOfWeek Integer,
      DayName CIChar( 12 ),
      DayOfMonth Integer,
      DayOfYear Integer,
      MonthOfYear Integer,
      MonthName CIChar( 12 ),
      FirstDayOfMonth Logical,
      LastDayOfMonth Logical,
      WeekDay Logical,
      Weekend Logical,
      Holiday Logical,
      TheYear Integer,
      ISOWeek Integer,
      DateType CIChar( 12 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Day',
   'Day.adi',
   'DATEKEY',
   'DateKey',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Day',
   'Day.adi',
   'DATE',
   'TheDate',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Day',
   'Day.adi',
   'DAYOFWEEK',
   'DayOfWeek',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Day',
   'Day.adi',
   'THEYEAR',
   'TheYear',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Day',
   'Day.adi',
   'MONTHNAME',
   'MonthName',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Day', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Dayfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Day', 
   'Table_Primary_Key', 
   'DATEKEY', 'APPEND_FAIL', 'Dayfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Day', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Dayfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Day', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'Dayfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Day', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Dayfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Day', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'Dayfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Day', 
      'DateKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Dayfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Day', 
      'TheDate', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Dayfail' ); 

