SELECT a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate,
  abs(timestampdiff(sql_tsi_day, a.thedate, b.biweeklypayperiodstartdate)) + 1 AS dayinpp
FROM day a
INNER JOIN (
SELECT distinct biweeklypayperiodstartdate, biweeklypayperiodenddate
FROM day) b on a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate



ALTER TABLE day
ADD COLUMN DayInBiWeeklyPayPeriod integer


UPDATE day
SET DayInBiWeeklyPayPeriod = x.dayinpp
FROM (
  SELECT a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate,
    abs(timestampdiff(sql_tsi_day, a.thedate, b.biweeklypayperiodstartdate)) + 1 AS dayinpp
  FROM day a
  INNER JOIN (
  SELECT distinct biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day) b on a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate) x
WHERE day.thedate = x.thedate   


SELECT *
FROM day 
WHERE yearmonth = 201311