DROP TABLE zDepxfmRO;
CREATE TABLE zDepxfmRO (
  TableName cichar(30),
  lft integer,
  rgt integer
) IN database;

INSERT INTO zDepxfmRO values('xfmRO',1,10);
INSERT INTO zDepxfmRO values('tmpSDPRHDR',2,3);
INSERT INTO zDepxfmRO values('tmpSDPRDET',4,5);
INSERT INTO zDepxfmRO values('tmpPDPPHDR',6,7);
INSERT INTO zDepxfmRO values('tmpPDPPDET',8,9);

SELECT * FROM zDepxfmRO a
WHERE NOT EXISTS (
  SELECT 1
  FROM system.tables
  WHERE name = a.tablename);
  
  
CREATE TABLE zDepfactRO (
  TableName cichar(30),
  lft integer,
  rgt integer
) IN database;

INSERT INTO zDepfactRO values('factRO',1,6);
INSERT INTO zDepfactRO values('xfmRO',2,3);
INSERT INTO zDepfactRO values('stgArkonaGLPTRNS',4,5);


SELECT * FROM zDepfactRO a
WHERE NOT EXISTS (
  SELECT 1
  FROM system.tables
  WHERE name = a.tablename)  

CREATE TABLE zDependencies (
  Deliverable cichar(30),
  TableName cichar(30),
  lft integer,
  rgt integer) IN database;

INSERT INTO zDependencies values('xfmRO','xfmRO',1,10);
INSERT INTO zDependencies values('xfmRO','tmpSDPRHDR',2,3);
INSERT INTO zDependencies values('xfmRO','tmpSDPRDET',4,5);
INSERT INTO zDependencies values('xfmRO','tmpPDPPHDR',6,7);
INSERT INTO zDependencies values('xfmRO','tmpPDPPDET',8,9);
INSERT INTO zDependencies values('factRO','factRO',1,6);
INSERT INTO zDependencies values('factRO','xfmRO',2,3);
INSERT INTO zDependencies values('factRO','stgArkonaGLPTRNS',4,5);
 
SELECT *
FROM ( 
  SELECT deliverable
  FROM zDependencies
  GROUP BY deliverable) a
LEFT JOIN zDependencies b ON   
  
SELECT a.*, b.*, a.lft + b.lft AS blft, a.rgt + b.rgt AS brgt
FROM zDependencies a
LEFT JOIN zDependencies b ON a.tablename = b.deliverable
  AND b.tablename <> a.tablename
--WHERE b.deliverable IS NOT null 


-- 9/18
-- getting closer

SELECT a.*, b.*, a.lft + b.lft AS blft, a.rgt + b.rgt AS brgt
FROM zDependencies a
LEFT JOIN zDependencies b ON a.tablename = b.deliverable
  AND b.tablename <> a.tablename
--WHERE b.deliverable IS NOT null 

SELECT a.*, b.*, a.lft + b.lft AS blft, a.rgt + b.rgt AS brgt
FROM zDependencies a
LEFT JOIN zDependencies b ON a.tablename = b.deliverable
  AND b.tablename <> a.tablename
ORDER BY a.deliverable, blft asc

-- which deliverables include other deliverable
SELECT *
FROM zDependencies a
WHERE EXISTS (
  SELECT 1
  FROM zDependencies
  WHERE deliverable = a.tablename
    AND tablename <> deliverable)
  AND tablename <> deliverable    

SELECT * FROM zDepxfmRO
SELECT * FROM zDepfactRO
CREATE TABLE 
