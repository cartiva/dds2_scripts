/*
going with composite keys rather than a surrogate
*/
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dObject-Dependency1');
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dObject-Dependency2');  
--DROP TABLE DependencyObjects;
-- 9/1/13 DROP unique index on dObject (NK), change it to just a plain index
-- 10/13/13 what this results IN IS multiple sources for a dObject
-- eg dObject dimTechGroup IS populated BY more than one exe/proc
--    same thing with dimCustomer & dimVehicle
--    eg dimVehicle: dimVehicle/INPMAST & todayFactRepairOrder/todayINPMAST both
--       populate dimVehicle
-- DO NOT know IF this will scale OR not     
CREATE TABLE DependencyObjects (
  dExecutable cichar(50),
  dProcedure cichar(50),
  dObject cichar(50)) IN database;
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'DependencyObjects','DependencyObjects.adi','PK',
   'dExecutable;dProcedure;dObject','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('DependencyObjects','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'DependencyObjectsfail');  
-- index: FK 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'DependencyObjects','DependencyObjects.adi','FK',
   'dExecutable;dProcedure','',2,512,'' );   
-- index
EXECUTE PROCEDURE sp_CreateIndex90( 
   'DependencyObjects','DependencyObjects.adi','dObject',
   'dObject','',2,512,'' );    
-- non-nullable  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'DependencyObjects','dExecutable', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'DependencyObjects','dProcedure', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'DependencyObjects','dObject', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
-- RI
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
   'zProcProcedures-dObjects','zProcProcedures','DependencyObjects','FK',2,2,NULL,'',''); 
--DROP TABLE Dependencies; 
CREATE TABLE Dependencies (
  dObjExecutable cichar(50),
  dObjProcedure cichar (50),
  dObjObject cichar(50),
  DependsOnExecutable cichar(50),
  DependsOnProcedure cichar(50),
  DependsOnObject cichar(50)) IN database;
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Dependencies','Dependencies.adi','PK',
   'dObjExecutable;dObjProcedure;dObjObject;DependsOnExecutable;DependsOnProcedure;DependsOnObject',
   '',2051,1024,'' );   
-- index: FK 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Dependencies','Dependencies.adi','FK1','dObjExecutable;dObjProcedure;dObjObject',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Dependencies','Dependencies.adi','FK2',
   'DependsOnExecutable;DependsOnProcedure;DependsOnObject','',2,512,'' );     
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('Dependencies','Table_Primary_Key','PK', 'APPEND_FAIL', 'Dependenciesfail');  
-- non-nullable  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Dependencies','dObjExecutable', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Dependencies','dObjProcedure', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Dependencies','dObjObject', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Dependencies','DependsOnExecutable', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Dependencies','DependsOnProcedure', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Dependencies','DependsOnObject', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );           
-- RI
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
   'dObject-Dependency1','DependencyObjects','Dependencies','FK1',2,2,NULL,'','');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
   'dObject-Dependency2','DependencyObjects','Dependencies','FK2',2,2,NULL,'','');  

--DELETE FROM DependencyObjects;     
INSERT into DependencyObjects values ('ServiceLine','SDPRHDR','tmpSDPRHDR');
INSERT into DependencyObjects values ('ServiceLine','SDPRDET','tmpSDPRDET');
INSERT into DependencyObjects values ('ServiceLine','PDPPHDR','tmpPDPPHDR');
INSERT into DependencyObjects values ('ServiceLine','PDPPDET','tmpPDPPDET');
INSERT into DependencyObjects values ('ServiceLine','stgRO','xfmRO');
-- DELETE FROM Dependencies;
INSERT INTO Dependencies values('ServiceLine','stgRO','xfmRO','ServiceLine','SDPRHDR','tmpSDPRHDR');
INSERT INTO Dependencies values('ServiceLine','stgRO','xfmRO','ServiceLine','SDPRDET','tmpSDPRDET');
INSERT INTO Dependencies values('ServiceLine','stgRO','xfmRO','ServiceLine','PDPPHDR','tmpPDPPHDR');
INSERT INTO Dependencies values('ServiceLine','stgRO','xfmRO','ServiceLine','PDPPDET','tmpPDPPDET');

-- 0 dependecies
SELECT a.* 
FROM DependencyObjects a
LEFT JOIN Dependencies b ON a.dExecutable = b.dobjExecutable AND a.dProcedure = b.dObjProcedure AND a.dObject = b.dObjObject
WHERE b.dObjExecutable IS NULL; 

-- objects that have dependencies
SELECT DISTINCT dObjExecutable, dObjProcedure, dObjObject
FROM Dependencies


--- NightlyBatch --------------------------------------------------------------
-- defaulting Complete to a date req'd 2 checks IN checkDependency,
-- 1 for NULL (NOT run) AND 1 for '12/31/9999' (run but fail)
DROP TABLE NightlyBatch;
CREATE TABLE NightlyBatch (
  TheDate date,
  dObject cichar(50),
--  Complete Date default '12/31/9999') IN database;
  Complete Date) IN database;

SELECT * FROM nightlybatch
  
alter PROCEDURE UpdateNightlyBatch (
  dObject cichar(50),
  Started logical,
  Completed logical)
BEGIN
/*
*/
DECLARE @dObject string;
DECLARE @Started logical;
DECLARE @Completed logical;
@dObject = (SELECT dObject FROM __input);
@Started = (SELECT Started FROM __input);
@Completed = (SELECT Completed FROM __input);
  IF @Started = true THEN 
    DELETE FROM NightlyBatch
    WHERE TheDate = curdate()
      AND dObject = @dObject; 
    INSERT INTO NightlyBatch (dObject, TheDate)
      values (@dObject, curdate());  
  ENDIF;
  IF @Completed = true THEN
    UPDATE NightlyBatch
    SET Complete = curdate()
    WHERE TheDate = curdate()
      AND dObject = @dObject;
  ENDIF;
END;  
--/ NightlyBatch --------------------------------------------------------------


--- CheckDependency -----------------------------------------------------------
for a given dObject, DO ALL the dependencies have a completed row IN 
NightlyBatch for curdate()
2 tests? 
  1. the DependsOnObject EXISTS IN NightlyBatch for curdate()
       for whatever reason, this one IS giving me the most problem, how DO i know
       what should run every night AND whether it ran OR NOT
       so i guess i am gitchy about the dObjects upon which nothing ELSE depends
       DO i even need to worry about it - IF nothing ELSE depends ON it what DO
       i care IF ran OR NOT
       stuff that IS being run IN zap AND reload that IS NOT yet being used IN 
       anything dimensionally
       leaning towards staying with the second level of dObjects - IF it IS needed for
       something ELSE THEN it will be IN NightlyBatch
       so, back to 2 tests has everything needed for "this" dObject run AND completed
       for the other stuff, zproclog good enough for now
       
       i am thinking that i will only ave to test for adjacent dependencies - 
         immediate parent/children
       
  2. the DependsOnObject IS completed IN NightlyBatch for curdate()
  
so AS usual i am stuck with HAVING to actually implement it to see it AND
figure out IF it works
so, dimRoStatus depends ON tmpSDPRHDR  

DELETE FROM NightlyBatch
SELECT * FROM NightlyBatch

dimRoStatus dependencies:
SELECT DependsOnObject FROM dependencies WHERE dObjObject = 'dimRoStatus'

DECLARE @dObject string;
@dObject = 'dimRoStatus';
-- returns rows for @dObject that have run
SELECT *
FROM NightlyBatch a
INNER JOIN (  
  SELECT DependsOnObject
  FROM Dependencies
  WHERE dObjObject = @dObject) b ON a.dObject = b.DependsOnObject  
WHERE a.thedate = curdate();    
  
DECLARE @dObject string;
@dObject = 'dimRoStatus';
-- returns rows for @dObject that have run AND are complete
SELECT *
FROM NightlyBatch a
INNER JOIN (  
  SELECT DependsOnObject
  FROM Dependencies
  WHERE dObjObject = @dObject) b ON a.dObject = b.DependsOnObject  
WHERE a.thedate = curdate()
  AND a.Complete <> '12/31/9999'     
  
sp has to verify that each dependency has run AND completed before allowing a proc/dObject to run
hmm, this might be good enuf
but i have to check for 2 status, either NULL complete OR complete = '12/31/9999'
which makes me want to change complete so that BY default it IS NULL 
UPDATE nightlybatch SET complete = CAST(NULL AS sql_date) WHERE complete = '12/31/9999'
DECLARE @dObject string;
@dObject = 'tmpPDPPDET';
SELECT a.DependsOnObject, b.Complete
FROM Dependencies a
LEFT JOIN NightlyBatch b ON a.DependsOnObject = b.dObject
  AND b.TheDate = curdate()
WHERE a.dObjObject = @dObject;

this will WORK for zero dependencies AS well

alter PROCEDURE checkDependencies (
  dObject cichar(50),
  PassFail cichar(4) output)
BEGIN 
/*
EXECUTE PROCEDURE checkDependencies('dimRoStatus');
*/  
DECLARE @dObject string;
DECLARE @PassFail integer;
@dObject = (SELECT dObject FROM __input);
@PassFail = (
  SELECT COUNT(*)
  FROM Dependencies a
  LEFT JOIN NightlyBatch b ON a.DependsOnObject = b.dObject
    AND b.TheDate = curdate()
  WHERE a.dObjObject = @dObject
    AND b.Complete IS NULL);
INSERT INTO __output
SELECT 
  CASE
    WHEN @PassFail = 0 THEN 'Pass' 
    ELSE 'Fail'
  END AS PassFail     
FROM system.iota;        
END;  
   
   
ok, this looks ok AS far AS it goes, because NOT ALL dependencies were satisfied 
for dimRoStatus (the dummied up dependency ON tmpPDPPDET), the checkRoStatus proc/dObject
did NOT even run, which IS good, but how DO i know at the END of the day whether
the nightly batch was successfull OR NOT
did everything that needs to run, actually run
thinking a JOIN BETWEEN nightlybatch AND zProc will give it  

what i am looking for IS the master list of what should run each night
which, at least now, feels LIKE more rigorous attention to keeping zProc tables up to date
WHICH FOR NOW, will be good enuf, i am most interested IN  enforcing dependencies right now
SELECT *
FROM zPRocLog
WHERE CAST(startts AS sql_date) = curdate()
  AND executable NOT IN ('hourly','SchedulerDetail')
ORDER BY startts desc  

SELECT * FROM nightlybatch

--/ CheckDependency -----------------------------------------------------------

ok, dummy one up for multiple dependencies
ADD tmpPDPPDET AS a requ for dimRoStatus
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimRoStatus'
  AND b.dObject = 'tmpPDPPDET';
  
DELETE FROM dependencies
-- ELECT * FROM dependencies
WHERE dObjObject = 'dimRoStatus'
AND DependsOnObject = 'tmpPDPPDET'  

  
--< dimTech -------------------------------------------------------------------
-- uh oh
-- depends ON tmpSDPRDET to determine IF there IS a missing technumber IN dimTech
-- which means dimTech has to run after serviceline, but serviceline, sp.stgRO
-- depends ON dimTech for the report tables - this has to be sorted out first
-- also depends ON dimEmployee for type 1 name changes
INSERT INTO DependencyObjects values ('dimTech','SDPTECH','dimTech');
INSERT INTO Dependencies
SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject 
FROM DependencyObjects a
full OUTER JOIN DependencyObjects b ON 1 = 1
WHERE a.dObject = 'dimTech'
AND b.dObject = 'tmpSDPRDET';
--/> dimTech -------------------------------------------------------------------  

--< 
INSERT INTO zProcexecutables values('SDPRHDR','daily',CAST('00:15:00' AS sql_time),10);
INSERT INTO zProcexecutables values('SDPRDET','daily',CAST('00:15:00' AS sql_time),11);
INSERT INTO zProcexecutables values('PDPPHDR','daily',CAST('00:15:00' AS sql_time),12);
INSERT INTO zProcexecutables values('PDPPDET','daily',CAST('00:15:00' AS sql_time),13);

DELETE FROM Dependencies;

delete
FROM DependencyObjects
WHERE dProcedure IN ('SDPRHDR','SDPRDET','PDPPHDR','PDPPDET');

delete
FROM zProcProcedures
WHERE proc IN ('SDPRHDR','SDPRDET','PDPPHDR','PDPPDET');

INSERT INTO zProcProcedures values('SDPRHDR','tmpSDPRHDR',1,'daily');
INSERT INTO zProcProcedures values('SDPRDET','tmpSDPRDET',1,'daily');
INSERT INTO zProcProcedures values('PDPPHDR','tmpPDPPHDR',1,'daily');
INSERT INTO zProcProcedures values('PDPPDET','tmpPDPPDET',1,'daily');

INSERT INTO DependencyObjects values('SDPRHDR','tmpSDPRHDR','tmpSDPRHDR');
INSERT INTO DependencyObjects values('SDPRDET','tmpSDPRDET','tmpSDPRDET');
INSERT INTO DependencyObjects values('PDPPHDR','tmpPDPPHDR','tmpPDPPHDR');
INSERT INTO DependencyObjects values('PDPPDET','tmpPDPPDET','tmpPDPPDET');

-- used 8/23 AS i TRY to see what the fuck IS going ON AS i ADD a new dimension
SELECT a.executable, a.scheduledts, b.proc, c.dobject
FROM zprocexecutables a
LEFT JOIN zprocprocedures b ON a.executable = b.executable
LEFT JOIN dependencyobjects c ON a.executable = c.dExecutable and b.proc = c.dProcedure
WHERE c.dobject is NOT NULL
ORDER BY scheduledts

-- 12/24/13
removing dimTechGroup, dimCorCodeGroup
to accomodate ALL restrict RI
DELETE IN ORDER:
Dependencies
DependencyObjects
zProcProcedures
zProcExecutables


SELECT left(a.executable, 25) AS Exec, left(b.proc, 25) AS proc, 
  left(c.dObject, 25) AS dObject, d.dependsonobject
FROM zProcExecutables a
INNER JOIN zProcProcedures b on a.executable = b.executable
INNER JOIN dependencyobjects c on a.executable = c.dexecutable AND b.proc = c.dprocedure
LEFT JOIN dependencies d on c.dobject = d.dobjObject
WHERE a.Executable IN ('dimTechGroup','dimCorCodeGroup')

DELETE 
FROM dependencies
WHERE dobjobject IN ('dimTechGroup','dimCorCodeGroup')
  OR dependsonobject IN ('dimTechGroup','dimCorCodeGroup');
  
DELETE 
-- SELECT *
FROM dependencyobjects
WHERE dobject IN ('dimTechGroup','dimCorCodeGroup');

DELETE 
--SELECT *
FROM zProcProcedures
WHERE proc IN ('dimTechGroup','dimCorCodeGroup');

DELETE 
--SELECT *
FROM zProcExecutables
WHERE executable IN ('dimTechGroup','dimCorCodeGroup');


-- 12/28/13  tpData
INSERT INTO zProcexecutables values('tpData','daily',CAST('03:30:00' AS sql_time),15);
INSERT INTO zProcProcedures values('tpData','tpData',1,'daily');
INSERT INTO DependencyObjects values('tpData','tpData','tpData');
-- only dependency enforced now IS factRepairOrder
-- also needs factClockHours, dimEmployee ...
INSERT INTO Dependencies
SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, 
  b.dProcedure,b.dObject, 'High' 
FROM DependencyObjects a
full OUTER JOIN DependencyObjects b ON 1 = 1
WHERE a.dObject = 'factRepairOrder'
AND b.dObject = 'tpData';

delete
FROM dependencies
WHERE dependsonExecutable = 'tpdata'

