1/2/14
      roFlagHours IS no longer good enuf, spdrhdr IS NOT being consistently
      updated with labor sales (ptltot) AS revealed BY sco writer labor sale
      month to date being low   
      Have to ADD labor sales at the finest grain (flagdate/tech/corcode) AND
      generate total FROM the SUM   
      
      still struggling with whether labor sales should be generated FROM 
      glptrns OR sdpr/pdppdet
      going with the ro tables for now
      
labor sales (ptlamt) needs to be added to:
  tmpRoTechFlagCor1
  tmpRoTechFlagCor2
  tmpRoTechFlagCorKeys
  tmpFactRepairOrder

ALTER TABLE tmpRoTechFlagCor1
ADD COLUMN ptlamt double(2);

ALTER TABLE tmpRoTechFlagCor2
ADD COLUMN ptlamt double(2);

ALTER TABLE tmpRoTechFlagCorKeys
ADD COLUMN ptlamt double(2);

ALTER TABLE tmpFactRepairOrder
ADD COLUMN laborSales double(2);

ALTER TABLE tmpFactRepairOrder
ALTER COLUMN RoLaborSales roLaborSales double(2);

ALTER TABLE factRepairOrder
ADD COLUMN laborSales double(2);

ALTER TABLE factRepairOrder
ALTER COLUMN RoLaborSales roLaborSales double(2);

ALTER TABLE tmpRoKeys
ALTER COLUMN roLaborSales roLaborSales double(2);

ALTER TABLE todayRoTechFlagCor1
ADD COLUMN ptlamt double(2);

ALTER TABLE todayRoTechFlagCor2
ADD COLUMN ptlamt double(2);

ALTER TABLE todayRoTechFlagCorKeys
ADD COLUMN ptlamt double(2);

ALTER TABLE todayFactRepairOrder
ADD COLUMN laborSales double(2);

ALTER TABLE todayFactRepairOrder
ALTER COLUMN RoLaborSales roLaborSales double(2);


SELECT a.ro, a.rolaborsales, b.* 
FROM tmprokeys a
LEFT JOIN (
  select storecode, ro, sum(ptlhrs) as roFlagHours, sum(ptlamt) as laborSales 
  from tmpRoTechFlagCorKeys 
  GROUP BY storecode, ro) b on a.ro = b.ro
WHERE a.storecode = 'ry1'
  AND a.rolaborsales = 0  

  
  
SELECT a.ro, a.rolaborsales, b.* 
FROM tmprokeys a
LEFT JOIN (
  select storecode, ro, sum(ptlhrs) as roFlagHours, sum(ptlamt) as laborSales 
  from tmpRoTechFlagCorKeys 
  GROUP BY storecode, ro) b on a.ro = b.ro
WHERE a.storecode = 'ry1'
  AND a.rolaborsales <> 0  
  AND round(a.rolaborsales, 2) <> round(b.laborSales, 2)
  