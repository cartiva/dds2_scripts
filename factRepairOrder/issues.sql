--< conflict BETWEEN no row IN pdp AND no closedate IN factrepairorder ---------------------------------------------------------------------------------------
-- OR more succintly 
--    1. no finalclosedate on ros with no ro IN ppdpphdr
--    2. ? don't know IF this happens: a finalclosedate but still has a row IN pdpphdr?
/*
ALL ros should eventually have a finalclosedate
IF there are warranty, service contract OR internal lines, the finalclosedate could be different than the closedate

*/

-- per Jeremy, ALL ros should get a final CLOSE date
-- here are the db2 anomalies
select a.ptro#, a.ptcnam, a.ptdate, a.ptcdat, b.*
from rydedata.sdprhdr a
left join rydedata.pdpphdr b on trim(a.ptro#) = trim(b.ptdoc#)
where a.ptfcdt = 0
  and b.ptco# is null 
order by a.ptdate DESC    

-- 8/29
   
SELECT '''' + trim(ro) + '''' + ',' FROM (
SELECT distinct a.ro, a.closedatekey, a.finalclosedatekey, b.rostatus
FROM factrepairorder a
INNER JOIN dimrostatus b on a.rostatuskey = b.rostatuskey
WHERE a.finalclosedatekey = 7306 -- no fcdate
  AND b.rostatus = 'Closed') c
  
trying to articulate ALL the possible combinations
1. Has fcDate -> roStatus must = Closed
2. No fcDAte -> roStatus must <> Closed
3. Has fcDate -> must NOT exist IN PDP
4. No fcDate -> must exist IN PDP
5. roStatus = Closed -> must NOT exist IN PDP
6. roStatus <> Closed -> must exist IN PDP

-- 1. Has fcDate -> roStatus must = Closed
SELECT * -- 0
FROM factRepairOrder a  
INNER JOIN dimRoStatus b on a.rostatuskey = b.rostatuskey
WHERE a.finalclosedatekey <> 7306
  AND b.rostatus <> 'Closed';
--2. No fcDAte -> roStatus must <> Closed
SELECT COUNT(DISTINCT ro) -- 123
FROM factRepairOrder a
INNER JOIN dimRoStatus b on a.rostatuskey = b.rostatuskey
WHERE a.finalclosedatekey = 7306  
  AND b.rostatus = 'Closed';
--3. Has fcDate -> must NOT exist IN PDP
SELECT * -- 0
FROM factRepairOrder a
INNER JOIN tmpPDPPHDR b on a.ro = b.ptdoc#  
WHERE a.finalclosedatekey <> 7306
-- 4. No fcDate -> must exist IN PDP
SELECT COUNT(DISTINCT ro) -- 123
FROM factRepairOrder a
WHERE a.finalclosedatekey = 7306
  AND NOT EXISTS (
    SELECT 1
    FROM tmpPDPPHDR
    WHERE ptdoc# = a.ro)
-- 5. roStatus = Closed -> must NOT exist IN PDP
SELECT * -- 0
FROM factRepairOrder a
INNER JOIN tmpPDPPHDR b on a.ro = b.ptdoc#
WHERE a.RoStatusKey = (
  SELECT rostatuskey
  FROM dimrostatus
  WHERE rostatus = 'Closed')   
-- 6.roStatus <> Closed -> must exist IN PDP
SELECT * -- 0
FROM factRepairOrder a
INNER JOIN dimRoStatus b on a.rostatuskey = b.rostatuskey
  AND b.rostatuskey <> (
    SELECT rostatuskey
    FROM dimRoStatus
    WHERE rostatus = 'Closed')
LEFT JOIN tmpPDPPHDR c on a.ro = c.ptdoc#
WHERE c.ptdoc# IS NULL    

so, how to fix them & how to test for consistency

--8/30
the ros with a wash line that IS NOT going INTO accounting have been bumped up
to enterprise support, let us see what they DO with it
this IS what i sent to arkona:
select trim(a.ptro#) as ptro#
from rydedata.sdprhdr a
where a.ptfcdt = 0
  and not exists (
    select 1 
    from rydedata.pdpphdr
    where trim(ptdoc#) = trim(a.ptro#))
  and a.ptdate > 20130000;


there IS some subset, though, that are NOT that issue
there IS a finalclosedate IN arkona but NOT IN factRepairOrder
WHERE are they

 
-- FROM Arkona, culling thru those that actually show a finalclosedate IN arkona  
-- show rostatus of closed IN factrepairorder
-- but have no finalclosedate IN factrepairorder  
SELECT b.rostatus, a.* FROM factRepairOrder a INNER JOIN dimrostatus b on a.rostatuskey = b.rostatuskey
WHERE ro IN ('18014654', '18016260', '16098081', '16097944', '16098229', 
'16099970', '18018585', '18018647', '18018913', '16100872', '18018935', '18018966', '18018937', '18019033', 
'18019034', '18019209', '18018983', '18018906', '18019330', '18019521', '18019523', '18019328', '18019522',
'18018967', '18019510', '18019645', '18019670', '16105188', '2667671',  '2668222',  '2668358',  '2668460',  
'2668513',  '2670031',  '2674159',  '2674194',  '2674499',  '2675183',  '2675243',  '2675425',  '2675480') 

UPDATE factRepairOrder
SET finalclosedatekey = closedatekey
WHERE ro IN ('18014654', '18016260', '16098081', '16097944', '16098229', 
'16099970', '18018585', '18018647', '18018913', '16100872', '18018935', '18018966', '18018937', '18019033', 
'18019034', '18019209', '18018983', '18018906', '18019330', '18019521', '18019523', '18019328', '18019522',
'18018967', '18019510', '18019645', '18019670', '16105188', '2667671',  '2668222',  '2668358',  '2668460',  
'2668513',  '2670031',  '2674159',  '2674194',  '2674499',  '2675183',  '2675243',  '2675425',  '2675480')  


--/> conflict BETWEEN no row IN pdp AND no finalclosedate IN factrepairorder ---------------------------------------------------------------------------------

