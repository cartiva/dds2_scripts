shit, ALL the sco shit depends ON factRO
so rather than doing: (which i prefer)
INSERT INTO factROold
SELECT * FROM factRO
i am going to go to the factROnew path
shit
OR maybe factRepairOrder, i LIKE that better

----- fuck my sister: initial load of totallaborsales IS fucked up ----------------------------
-- based ON glptrns, does NOT differentiate BETWEEN sublet AND labor sales eg ro 16033504
-- SELECT * FROM stgArkonaSDPRHDR WHERE ptro# = '16081875'   
-- SELECT * FROM factRepairOrder WHERE ro = '16081875'
-- SELECT * FROM stgArkonaGLPTRNS WHERE gtdoc# = '16081875'

start with trying to populate factRepairOrder with existing factRO/factROLine data
-- implement nullable AND RI AND indexes each step of the way with each dim
--< store,ro, ro level measures, ro level datekeys, line, line measures -------
-- leave out voids AND RY3
-- are there any ros with no lines :: 8, NOT enough to worry about
-- SELECT * FROM factro a where void = false and storecode in ('ry1','ry2') AND NOT EXISTS (SELECT 1 FROM factroline WHERE ro = a.ro)
-- SELECT storecode, ro, line FROM( -- no dups
SELECT a.storecode, a.ro, a.opendatekey, a.closedatekey, a.finalclosedatekey,
  a.laborsales, a.partssales, a.miles, b.line, b.linedatekey, lineflaghours
-- SELECT COUNT(*) -- 1,014,512
FROM factro a
LEFT JOIN factroline b ON a.storecode = b.storecode AND a.ro = b.ro
WHERE a.void = false
  AND a.storecode IN ('ry1','ry2')
  AND b.storecode IS NOT NULL 
-- ) x GROUP BY storecode, ro, line HAVING COUNT(*) > 1  

INSERT INTO factRepairOrder (StoreCode, RO, line,opendatekey,closedatekey,
  finalclosedatekey,linedatekey,rolaborsales,ropartssales, flaghours, miles)
SELECT a.storecode, a.ro, b.line, a.opendatekey, a.closedatekey, a.finalclosedatekey,
  b.linedatekey, a.laborsales, a.partssales, lineflaghours, a.miles  
FROM factro a
LEFT JOIN factroline b ON a.storecode = b.storecode AND a.ro = b.ro
WHERE a.void = false
  AND a.storecode IN ('ry1','ry2')
  AND b.storecode IS NOT NULL;
  
UPDATE factRepairOrder
SET RoFlagHours = x.total 
FROM (    
  SELECT ro, SUM(lineflaghours) AS total 
  FROM factrepairorder
  GROUP BY ro) x  
WHERE factRepairOrder.ro = x.ro  

-- 8/5 ADD CreatedTS
ALTER TABLE factRepairOrder
ADD COLUMN CreatedTS timestamp;

UPDATE factRepairOrder
SET RoCreatedTS = x.CreatedTS
FROM factRO x
WHERE factRepairOrder.RO = x.ro;

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RoCreatedTS', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 

-- 8/6 ADD tag
ALTER TABLE factRepairOrder
ADD COLUMN Tag cichar(5);  

UPDATE factRepairOrder
SET tag = x.pttag#
FROM stgArkonaSDPRHDR x
LEFT JOIN factRepairOrder a ON x.ptco# = a.storecode
  AND x.ptro# = a.ro
WHERE pttag# <> '' 

-- 8/7 ADD the rest of the measures


ALTER TABLE factRepairOrder
ADD COLUMN LaborSales money default '0'
ADD COLUMN PartsSales money default '0'
ADD COLUMN Sublet money default '0'
ADD COLUMN RoShopSupplies money default '0'
ADD COLUMN RoDiscount money default '0'
ADD COLUMN RoHazardousMaterials money default '0'
ADD COLUMN RoPaintMaterials money default '0'; 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'LaborSales', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'PartsSales', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'Sublet', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RoShopSupplies', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RoDiscount', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RoHazardousMaterials', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RoPaintMaterials', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );            
                           
UPDATE factRepairOrder
SET LaborSales = coalesce(x.LaborSales, 0),
    Sublet = coalesce(x.Sublet, 0)
FROM (    
  SELECT a.ro, a.line, 
    SUM(CASE WHEN b.ptcode = 'TT' AND b.ptltyp = 'L' THEN ptlamt END) AS LaborSales,
    SUM(CASE WHEN b.ptcode = 'SL' AND b.ptltyp = 'N' THEN ptlamt END) AS Sublet 
  FROM factRepairOrder a
  LEFT JOIN stgArkonaSDPRDET b ON a.storecode = b.ptco# AND a.ro = b.ptro# AND a.line = b.ptline
  GROUP BY a.ro, a.line) x
WHERE factRepairOrder.ro = x.ro
  AND factRepairOrder.line = x.line;
    
UPDATE factRepairOrder
SET RoShopSupplies = coalesce(b.ptcpss + b.ptwass + b.ptinss + b.ptscss, 0),
    RoDiscount = coalesce(b.pthdsc, 0),
    RoHazardousMaterials = coalesce(b.ptcphz, 0) 
FROM factRepairOrder a
LEFT JOIN stgArkonaSDPRHDR b ON a.storecode = b.ptco# AND a.ro = b.ptro#;
  
SELECT a.ro, sum(b.ptlamt)
FROM factRepairOrder a
LEFT JOIN stgArkonaSDPRDET b ON a.storecode = b.ptco# AND a.ro = b.ptro#
  AND b.ptltyp = 'M'
  AND b.ptcode = 'PM'
GROUP BY ro;    

UPDATE factRepairOrder
SET RoPaintMaterials = coalesce(b.ptlamt, 0)
FROM (
  SELECT storecode, ro
  FROM factRepairOrder
  GROUP BY storecode, ro) a  
LEFT JOIN stgArkonaSDPRDET b ON a.storecode = b.ptco# AND a.ro = b.ptro#
  AND b.ptltyp = 'M'
  AND b.ptcode = 'PM'; 
     
     
SELECT * FROM stgArkonaSDPRDET WHERE ptro# = '16001002' AND ptltyp = 'M' AND ptcode = 'PM'     
-- 8/7 ADD the rest of the measures
change LineStatusKey to StatusKey
change LineFlagHours to FlagHours
     
-- indexes AND nullable
-- PK
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','PK','StoreCode;ro;line',
   '',2051,512, '' ); 
-- TABLE Primary Key
EXECUTE PROCEDURE sp_ModifyTableProperty( 'factRepairOrder', 
   'Table_Primary_Key', 
   'PK', 'RETURN_ERROR', 'factRepairOrderfail');   
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','STORECODE','StoreCode',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','RO','RO',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','Line','Line',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','OpenDateKey','OpenDateKey',
   '',2,512,'' ); 
   EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','CloseDateKey','CloseDateKey',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','FinalCloseDateKey','FinalCloseDateKey',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','LineDateKey','LineDateKey',
   '',2,512,'' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','RoCreatedTS','RoCreatedTS',
   '',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','Tag','Tag',
   '',2,512,'' );         
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'StoreCode', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RO', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'Line', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'OpenDateKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'CloseDateKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'FinalCloseDateKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );      
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'LineDateKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );        
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RoLaborSales', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );     
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RoPartsSales', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'FlagHours', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RoFlagHours', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'Miles', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );             
      
/*      
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimDay-factRepairOrderOpenDate');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimDay-factRepairOrderCloseDate');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimDay-factRepairOrderFinalCloseDate');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimDay-factRepairOrderLineDate');
*/
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimDay-factRepairOrderOpenDate',
     'Day', 'factRepairOrder', 'OpenDateKEY', 
     2, 2, NULL, '', '');
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimDay-factRepairOrderCloseDate',
     'Day', 'factRepairOrder', 'CloseDateKEY', 
     2, 2, NULL, '', '');
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimDay-factRepairOrderFinalCloseDate',
     'Day', 'factRepairOrder', 'FinalCloseDateKEY', 
     2, 2, NULL, '', '');   
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimDay-factRepairOrderLineDate',
     'Day', 'factRepairOrder', 'LineDateKEY', 
     2, 2, NULL, '', '');       

                                                   
--/> store, ro, measures, datekeys, line --------------------------------------

--< service writers -----------------------------------------------------------
-- don't need storecode for update
-- SELECT ro FROM (SELECT storecode, ro FROM factRepairOrder GROUP BY storecode, ro) x GROUP BY ro HAVING COUNT(*) > 1
UPDATE factRepairOrder
SET serviceWriterKey = x.ServiceWriterKey
FROM (
  SELECT a.storecode, a.ro, a.opendatekey, d.*
  FROM (
    select storecode, ro, opendatekey
    from factRepairOrder a
    GROUP BY storecode,ro, opendatekey) a
  LEFT JOIN factRO b ON a.storecode = b.storecode AND a.ro = b.ro
  INNER JOIN day c ON a.opendatekey = c.datekey
  LEFT JOIN dimservicewriter d ON a.storecode = d.storecode AND b.writerid = d.writernumber
    AND c.thedate BETWEEN d.servicewriterkeyfromdate AND d.servicewriterkeythrudate
    AND d.active = true) x 
WHERE factRepairOrder.ro = x.ro;
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','ServiceWriterKey','ServiceWriterKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'ServiceWriterKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimServiceWriter-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimServiceWriter-factRepairOrder',
     'dimServiceWriter', 'factRepairOrder', 'ServiceWriterKey', 
     2, 2, NULL, '', '');
--/> service writers -----------------------------------------------------------

--< Customer ------------------------------------------------------------------
UPDATE factRepairOrder
SET CustomerKey = x.CustomerKey
FROM (
  SELECT a.ro, 
    CASE 
      WHEN d.customerkey IS NOT NULL THEN d.customerkey 
      ELSE e.customerkey 
    END AS CustomerKey
  FROM factro a
  LEFT JOIN dimCustomer d ON a.customerkey = d.bnkey  
  LEFT JOIN (
    select customerkey, fullname 
    from dimCustomer
    WHERE fullname = 'unknown') e ON 1 = 1 
  WHERE EXISTS (
    SELECT 1
    FROM factRepairOrder
    WHERE ro = a.ro)) x
WHERE factRepairOrder.ro = x.ro;    
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','CustomerKey','CustomerKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'CustomerKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimCustomer-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimCustomer-factRepairOrder',
     'dimCustomer', 'factRepairOrder', 'CustomerKey', 
     2, 2, NULL, '', '');
--/> Customer -----------------------------------------------------------------  

--< Vehicle -------------------------------------------------------------------
--SELECT COUNT(*) -- shit, only 58 unknown vehicles
-- 50 seconds for a million row update
UPDATE factRepairOrder
SET VehicleKey = x.VehicleKey
FROM (   
  SELECT a.ro, 
    CASE
      WHEN b.vin IS NOT NULL THEN b.VehicleKey
      ELSE c.VehicleKey
    END AS VehicleKey
  FROM factro a  
  LEFT JOIN dimVehicle b ON a.vin = b.vin
  LEFT JOIN (
    SELECT VehicleKey, Vin
    FROM dimVehicle
    WHERE vin = 'UNKNOWN') c ON 1 = 1
  WHERE EXISTS (
    SELECT 1
    FROM factRepairOrder
    WHERE ro = a.ro)) x 
WHERE factRepairOrder.ro = x.ro;
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','VehicleKey','VehicleKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'VehicleKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimVehicle-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimVehicle-factRepairOrder',
     'dimVehicle', 'factRepairOrder', 'VehicleKey', 
     2, 2, NULL, '', '');    
--/> Vehicle -------------------------------------------------------------------

--< OpCode --------------------------------------------------------------------
/*
-- factRoLine: ro/line are unique
-- SELECT ro,line FROM (SELECT storecode,ro,line FROM factroline GROUP BY storecode,ro,line) x GROUP BY ro,line HAVING COUNT(*) > 1
the difference BETWEEN NULL AND n/a does NOT matter
SELECT a.*
INTO #wtf
FROM factroline a
LEFT JOIN dimOpCode b ON a.opcode = b.opcode
WHERE EXISTS (
  SELECT 1
  FROM factRepairOrder
  WHERE ro = a.ro
    AND line = a.line)
AND b.opcode IS NULL  
-- only 1 with no opcode
-- so fuck it they are ALL N/A
*/
UPDATE factRepairOrder
SET OpCodeKey = x.OpCodeKey
FROM (
  SELECT a.storecode, a.ro, a.line,
    CASE 
      WHEN a.storecode = 'RY1' THEN
        CASE WHEN  b.OpCode IS NOT NULL THEN b.OpCodeKey ELSE c.OpCodeKey END 
      WHEN a.storecode = 'RY2' THEN 
        CASE WHEN  b.OpCode IS NOT NULL THEN b.OpCodeKey ELSE c.OpCodeKey END 
    END AS OpCodeKey
  FROM factroline a
  LEFT JOIN dimOpCode b ON a.storecode = b.storecode AND a.opcode = b.opcode
  LEFT JOIN (
    SELECT storecode, opcodekey
    FROM dimopcode
    WHERE opcode = 'N/A') c ON a.storecode = c.storecode
  WHERE EXISTS (
    SELECT 1
    FROM factRepairOrder
    WHERE ro = a.ro
      AND line = a.line)) x
WHERE factRepairOrder.RO = x.ro
  AND factRepairOrder.Line = x.line;     
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','OpCodeKey','OpCodeKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'OpCodeKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
/* failed nullable     
SELECT * FROM factrepairorder WHERE opcodekey IS NULL   
18024141, 18022651,18022651: now void:  
  DELETE FROM factrepairorder WHERE ro IN ('18024141', '18022651','18022651')
the others have "deleted lines", OR at least lines IN factRepairOrder that no longer exist IN 
  arkona OR factroline
18023752/2, 16126337/6, 16124834/2
SELECT * FROM factroline WHERE ro = '18023752' AND line = 2  
SELECT * FROM factroline WHERE ro = '16126337' AND line = 6 
SELECT * FROM factroline WHERE ro = '16124834' AND line = 2   
DELETE FROM factRepairOrder WHERE ro = '18023752' AND line = 2;
DELETE FROM factRepairOrder WHERE ro = '16126337' AND line = 6;  
DELETE FROM factRepairOrder WHERE ro = '16124834' AND line = 2; 
*/
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimOpCode-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimOpCode-factRepairOrder',
     'dimOpCode', 'factRepairOrder', 'OpCodeKey', 
     2, 2, NULL, '', '');       
--/> OpCode -------------------------------------------------------------------

--< CorCode -------------------------------------------------------------------
UPDATE factRepairOrder
SET CorCodeKey = x.CorCodeKey
FROM (
  SELECT a.storecode, a.ro, a.line,
    CASE 
      WHEN a.storecode = 'RY1' THEN
        CASE WHEN  b.OpCode IS NOT NULL THEN b.OpCodeKey ELSE c.OpCodeKey END 
      WHEN a.storecode = 'RY2' THEN 
        CASE WHEN  b.OpCode IS NOT NULL THEN b.OpCodeKey ELSE c.OpCodeKey END 
    END AS CorCodeKey
  FROM factroline a
  LEFT JOIN dimOpCode b ON a.storecode = b.storecode AND a.corcode = b.opcode
  LEFT JOIN (
    SELECT storecode, opcodekey
    FROM dimopcode
    WHERE opcode = 'N/A') c ON a.storecode = c.storecode
  WHERE EXISTS (
    SELECT 1
    FROM factRepairOrder
    WHERE ro = a.ro
      AND line = a.line)) x
WHERE factRepairOrder.RO = x.ro
  AND factRepairOrder.Line = x.line;  
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','CorCodeKey','CorCodeKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'CorCodeKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimOpCodeCor-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimOpCodeCor-factRepairOrder',
     'dimOpCode', 'factRepairOrder', 'CorCodeKey', 
     2, 2, NULL, '', '');      
--/> CorCode ------------------------------------------------------------------

--< ServiceType ---------------------------------------------------------------
UPDATE factRepairOrder
SET ServiceTypeKey = x.ServiceTypeKey
FROM (
  SELECT a.storecode, a.ro, a.line,
    CASE
      WHEN b.servicetypekey IS NOT NULL THEN b.ServiceTypeKey
      ELSE c.ServiceTypeKey
    END AS ServiceTypeKey
  FROM factroline a
  LEFT JOIN dimServiceType b ON a.servicetype = b.servicetypecode
  LEFT JOIN (
    SELECT serviceTypeKey
    FROM dimServiceType
    WHERE servicetypecode = 'UN') c ON 1 = 1
  WHERE EXISTS (
    SELECT 1
    FROM factRepairOrder
    WHERE ro = a.ro
      AND line = a.line)) x
WHERE factRepairOrder.RO = x.ro
  AND factRepairOrder.Line = x.line;  
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','ServiceTypeKey','ServiceTypeKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'ServiceTypeKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimServiceType-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimServiceType-factRepairOrder',
     'dimServiceType', 'factRepairOrder', 'ServiceTypeKey', 
     2, 2, NULL, '', '');        
--/> ServiceType ---------------------------------------------------------------

--< PaymentType ---------------------------------------------------------------
UPDATE factRepairOrder
SET PaymentTypeKey = x.PaymentTypeKey
FROM (
  SELECT a.storecode, a.ro, a.line,
    CASE
      WHEN b.Paymenttypekey IS NOT NULL THEN b.PaymentTypeKey
      ELSE c.PaymentTypeKey
    END AS PaymentTypeKey
  FROM factroline a
  LEFT JOIN dimPaymentType b ON a.PayType = b.PaymentTypeCode
  LEFT JOIN (
    SELECT PaymentTypeKey
    FROM dimPaymentType
    WHERE Paymenttypecode = 'U') c ON 1 = 1
  WHERE EXISTS (
    SELECT 1
    FROM factRepairOrder
    WHERE ro = a.ro
      AND line = a.line)) x
WHERE factRepairOrder.RO = x.ro
  AND factRepairOrder.Line = x.line;  
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','PaymentTypeKey','PaymentTypeKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'PaymentTypeKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimPaymentType-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimPaymentType-factRepairOrder',
     'dimPaymentType', 'factRepairOrder', 'PaymentTypeKey', 
     2, 2, NULL, '', '');        
--/> PaymentType --------------------------------------------------------------

--< TechGroup -----------------------------------------------------------------
-- ALL the WORK done IN the brdgTechGroup script, just need indexes, nullable & RI
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','TechGroupKey','TechGroupKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'TechGroupKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimPaymentType-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimTechGroup-factRepairOrder',
     'dimTechGroup', 'factRepairOrder', 'TechGroupKey', 
     2, 2, NULL, '', '');   
--/> TechGroup ----------------------------------------------------------------


--< dimRoComment --------------------------------------------------------------
SELECT * FROM dimrocomment WHERE ro = 'n/a'
UPDATE factRepairOrder
SET RoCommentKey = x.RoCommentKey
FROM (
  SELECT storecode, ro, roCommentKey
  FROM dimROComment) x
WHERE factRepairOrder.StoreCode = x.StoreCode
  AND factRepairOrder.RO = x.RO;

UPDATE factRepairOrder
SET RoCommentKey = (
  SELECT RoCommentKey
  FROM dimRoComment
  WHERE storecode = 'RY1'
    AND ro = 'N/A')
WHERE RoCommentKey IS NULL 
  AND storecode = 'RY1';
  
UPDATE factRepairOrder
SET RoCommentKey = (
  SELECT RoCommentKey
  FROM dimRoComment
  WHERE storecode = 'RY2'
    AND ro = 'N/A')
WHERE RoCommentKey IS NULL 
  AND storecode = 'RY2';  
-- index    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','RoCommentKey','RoCommentKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RoCommentKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimPaymentType-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimRoComment-factRepairOrder',
     'dimRoComment', 'factRepairOrder', 'RoCommentKey', 
     2, 2, NULL, '', '');       
--/> dimRoComment -------------------------------------------------------------

--< dimCCC --------------------------------------------------------------------
SELECT * FROM dimccc

UPDATE factRepairOrder
SET CCCKey = x.CCCKey
FROM (
  SELECT storecode, ro, line, CCCKey
  FROM dimCCC) x
WHERE factRepairOrder.StoreCode = x.StoreCode
  AND factRepairOrder.RO = x.RO
  AND factRepairOrder.line = x.line;
  
SELECT * FROM factRepairOrder WHERE ccckey IS null  

UPDATE factRepairOrder
SET CCCKey = (
  SELECT CCCKey
  FROM dimCCC
  WHERE storecode = 'RY1'
    AND ro = 'N/A')
WHERE CCCKey IS NULL 
  AND storecode = 'RY1';
  
UPDATE factRepairOrder
SET CCCKey = (
  SELECT CCCKey
  FROM dimCCC
  WHERE storecode = 'RY2'
    AND ro = 'N/A')
WHERE CCCKey IS NULL 
  AND storecode = 'RY2'; 
-- index    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','CCCKey','CCCKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'CCCKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimPaymentType-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimCCC-factRepairOrder',
     'dimCCC', 'factRepairOrder', 'CCCKey', 
     2, 2, NULL, '', '');       
--/> dimCCC -------------------------------------------------------------------

--< dimLineStatus -------------------------------------------------------------
-- first CREATE the dimension
--DROP TABLE dimLineStatus;
CREATE TABLE dimLineStatus (
  LineStatusKey integer,
  LineStatusCode cichar(1),
  LineStatus cichar(24)) IN database;
  
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimLineStatus','dimLineStatus.adi','PK',
   'LineStatusKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimLineStatus','Table_Primary_Key',
  'PK', 'RETURN_ERROR', NULL);   
  
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimLineStatus','LineStatusKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimLineStatus','LineStatusCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 

INSERT INTO dimLineStatus (LineStatusKey, LineStatusCode, LineStatus) values (1,'I','Open');    
INSERT INTO dimLineStatus (LineStatusKey, LineStatusCode, LineStatus) values (2,'C','Closed');  

-- THEN UPDATE FROM tmpPDPPDET
-- IF it IS NOT IN PDPPDET, it IS closed
UPDATE factRepairOrder
SET StatusKey = coalesce(d.LineStatusKey, e.LineStatusKey)
FROM factRepairOrder a
LEFT JOIN (
  SELECT a.ptdoc#, b.ptline, b.ptlsts, b.ptsvctyp, b.ptlpym 
  FROM tmpPDPPHDR a
  LEFT JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO' 
    AND a.ptdoc# <> ''
    AND b.ptltyp = 'A' AND b.ptseq# = 0) c ON a.ro = c.ptdoc# AND a.line = c.ptline
LEFT JOIN dimLineStatus d ON c.ptlsts = d.LineStatusCode   
LEFT JOIN dimLineStatus e ON 1 = 1
  AND e.linestatus = 'Closed';

-- index    
DROP index factRepairOrder.LineStatusKey;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','StatusKey','StatusKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'StatusKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimLineStatus-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimLineStatus-factRepairOrder',
     'dimLineStatus', 'factRepairOrder', 'StatusKey', 
     2, 2, NULL, '', '');       
     
--< dimLineStatus -------------------------------------------------------------

--< dimRoStatus ---------------------------------------------------------------
UPDATE factRepairOrder
SET RoStatusKey = coalesce(b.RoStatusKey, c.RoStatusKey)
FROM factRepairORder z
LEFT JOIN (
  SELECT ptco#, ptdoc#, ptrsts
  FROM tmpPDPPHDR
  WHERE ptdtyp = 'RO'
    AND ptdoc# <> '')  a ON z.storecode = a.ptco# AND z.ro = a.ptdoc# 
LEFT JOIN dimrostatus b ON a.ptrsts = b.rostatuscode
LEFT JOIN dimrostatus c ON 1 = 1
  AND c.rostatus = 'closed';
-- index    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','RoStatusKey','RoStatusKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'RoStatusKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL ); 
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimRoStatus-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimLineStatus-factRepairOrder',
     'dimRoStatus', 'factRepairOrder', 'RoStatusKey', 
     2, 2, NULL, '', '');    
--/> dimROStatus --------------------------------------------------------------

SELECT COUNT(DISTINCT ro) FROM factRepairOrder a
INNER JOIN dimLineStatus b ON a.linestatuskey = b.linestatuskey
WHERE b.linestatuskey = 1

SELECT a.*
FROM factRepairOrder a
INNER JOIN dimLineStatus b ON a.linestatuskey = b.linestatuskey
WHERE b.linestatuskey = 1
ORDER BY rocreatedts

SELECT a.ptdoc#, b.ptline, b.ptlsts, b.ptsvctyp, b.ptlpym 
FROM tmpPDPPHDR a
LEFT JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
WHERE a.ptdtyp = 'RO' 
  AND a.ptdoc# <> ''
  AND b.ptltyp = 'A' AND b.ptseq# = 0

SELECT rostatus FROM zROHEADER GROUP BY rostatus
SELECT * FROM zrodetail

SELECT *
FROM zroheader
WHERE rostatus <> 'closed'

SELECT a.*, c.* 
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
LEFT JOIN stgArkonaSDPRHDR c ON a.ro = c.ptro#
WHERE a.void = false
  AND finalclosedatekey = 7306
ORDER BY b.thedate

SELECT a.*
FROM tmpPDPPHDR a
WHERE a.ptdtyp = 'RO' 
  AND a.ptdoc# <> ''
ORDER BY ptdate  




detail flag hours for june 2013
SELECT e.technumber, coalesce(e.name,description), round(sum(a.flaghours * d.weightfactor),2)
FROM factRepairOrder a
INNER JOIN day b ON a.closedatekey = b.datekey
  AND b.yearmonth = 201306
INNER JOIN dimTechGroup c ON a.TechGroupKey = c.TechGroupKey  
INNER JOIN brTechGroup d ON c.TechGroupKey = d.TechGroupKey
INNER JOIN dimTech e ON d.TechKey = e.TechKey
  AND e.FlagDeptCode = 'RE'
WHERE a.storecode = 'RY1'
GROUP BY e.technumber, coalesce(e.name,description)


SELECT *
FROM (
  SELECT ro, MAX(rolaborsales) AS gl
  FROM factrepairorder
  GROUP BY ro) a
inner JOIN (  
  SELECT ro, sum(coalesce(laborsales, 0)) AS lines
  FROM factrepairorder
  GROUP BY ro) b ON a.ro = b.ro AND abs(a.gl) <> abs(b.lines)
  
  
