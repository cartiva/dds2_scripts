--fuckety fuck fuck, i am putting linedate IN factRepairOrder
which i am basing on ptltyp = A
but flagdate should be based on ptltyp = L AND ptcode = TT

so it looks LIKE i have to ADD flagdate to factrepairorder
so, change the db connection to a previous daily cut AND WORK it the fuck out

USING sunday 10/27 cut /copy

-- DROP TABLE #wtf;
SELECT a.storecode, aa.thedate as opendate, a.ro, a.line, a.flaghours, coalesce(b.flagdate, cast('12/31/9999' AS sql_date)) AS flagdate
INTO #wtf
FROM factRepairORder a
LEFT JOIN day aa on a.opendatekey = aa.datekey
LEFT JOIN (
  SELECT ptco#, ptro#, ptline, ptdate AS flagdate
  FROM stgArkonaSDPRDET
  WHERE ptltyp = 'L' AND ptcode = 'TT'
    AND ptlhrs > 0) b on a.storecode = b.ptco# AND a.ro = b.ptro# AND a.line = b.ptline

SELECT * FROM #wtf   
the JOIN to sdprdet will return multiple rows for some lines, WHEN there are 
multiple tech time entries per lines
the question IS, DO the separate tech time entries differ within a line
-- the dup lines
SELECT storecode, ro, line
FROM #wtf
GROUP BY storecode, ro, line
HAVING COUNT(*) > 1
-- ADD flagdate to grouping
SELECT storecode, ro, line, flagdate
FROM #wtf
GROUP BY storecode, ro, line, flagdate
HAVING COUNT(*) > 1
-- now, are there multiple lines
--SELECT COUNT(*) FROM (
SELECT storecode, ro, line, MIN(flagdate), MAX(flagdate)
FROM (
  SELECT storecode, ro, line, flagdate
  FROM #wtf
  GROUP BY storecode, ro, line, flagdate
  HAVING COUNT(*) > 1) x 
GROUP BY storecode, ro, line HAVING COUNT(*) > 1  
--) z
-- fuck, of course there are but NOT very many, 19 out of a million 

SINCE THERE ARE SO FEW the trade off IS to use the MAX flag date AS the winner - GOOD ENUF

-- ahh fuck, flaghours with no flag date
-- ooh, these may be OPEN ros, that would be satified with including pdppdet
-- aahh, AND those that were NOT closed on 10/27, remember that's the cut i am using
SELECT *
FROM #wtf
WHERE flaghours <> 0 
  AND flagdate = '12/31/9999'
  
SO, DO A WHOLESALE UPDATE BASED ON SDPRDET, THEN NIGHTLY SHOULD TAKE CARE OF THE OPEN ROS   

--< factRepairOrder ------------------------------------------------------------
ALTER TABLE factRepairOrder
ADD COLUMN FlagDateKey integer CONSTRAINT NOT NULL POSITION 9;

UPDATE factRepairOrder
SET FlagDateKey = (SELECT datekey FROM day WHERE datetype = 'NA');

EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','FlagDateKey',
   'FlagDateKey','',2,512,'' ); 
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimDay-factRepairOrderFlagDate');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimDay-factRepairOrderFlagDate',
     'Day', 'factRepairOrder', 'FlagDateKey', 
     2, 2, NULL, '', ''); 
     
-- DROP TABLE #wtf;
SELECT ptco#, ptro#, ptline, MAX(ptdate) AS flagdate
INTO #wtf
FROM stgArkonaSDPRDET
WHERE ptltyp = 'L' AND ptcode = 'TT'
  AND ptlhrs > 0
GROUP BY ptco#, ptro#, ptline 
     
UPDATE factRepairOrder
SET FlagDateKey = x.datekey
FROM (
  SELECT a.*, b.datekey
  FROM #wtf a
  LEFT JOIN day b on a.flagdate = b.thedate) x
WHERE factRepairOrder.storecode = x.ptco#
  AND factRepairOrder.ro = x.ptro#
  AND factRepairOrder.line = x.ptline;       
  
ALTER PROCEDURE xfmFactRepairOrder    
modify exe/sdprhdr
          /sdprdet
          -- dont have to DO these, they are full scrapes/pdpphdr, /pdppdet        
  
DROP TABLE tmpRoLine;
CREATE TABLE tmpRoLine ( 
  storecode CIChar( 3 ) CONSTRAINT NOT NULL,
  ro CIChar( 9 ) CONSTRAINT NOT NULL,
  line Integer CONSTRAINT NOT NULL,
  linedate Date CONSTRAINT NOT NULL,
  flagdate date CONSTRAINT NOT NULL,
  laborsales Money DEFAULT '0' CONSTRAINT NOT NULL,
  sublet Money DEFAULT '0' CONSTRAINT NOT NULL,
  paintmaterials Money DEFAULT '0' CONSTRAINT NOT NULL,
  servtype CIChar( 2 ),-- CONSTRAINT NOT NULL,
  paytype CIChar( 1 ),-- CONSTRAINT NOT NULL,
  opcode CIChar( 10 ),-- CONSTRAINT NOT NULL,
  linehours Double( 2 ) DEFAULT '0' CONSTRAINT NOT NULL,
  status CIChar( 24 ) CONSTRAINT NOT NULL,
  CONSTRAINT PK PRIMARY KEY (storecode,ro,line)) IN DATABASE;


DROP TABLE tmpRolineKeys;
CREATE TABLE tmpRoLineKeys ( 
  storecode CIChar( 3 ) CONSTRAINT NOT NULL,
  ro CIChar( 9 ) CONSTRAINT NOT NULL,
  line Integer CONSTRAINT NOT NULL,
  linedatekey Integer CONSTRAINT NOT NULL,
  flagdatekey integer CONSTRAINT NOT NULL,
  ServiceTypeKey Integer CONSTRAINT NOT NULL,
  PaymentTypeKey Integer CONSTRAINT NOT NULL,
  cccKey Integer CONSTRAINT NOT NULL,
  OpCodeKey Integer CONSTRAINT NOT NULL,
  CorCodeGroupKey Integer CONSTRAINT NOT NULL,
  TechGroupKey Integer CONSTRAINT NOT NULL,
  StatusKey Integer CONSTRAINT NOT NULL,
  FlagHours Double( 4 ) DEFAULT '0' CONSTRAINT NOT NULL,
  LaborSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  PartsSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  Sublet Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoFlagHours Double( 4 ) DEFAULT '0' CONSTRAINT NOT NULL,
  RoPaintMaterials Money DEFAULT '0' CONSTRAINT NOT NULL,
  CONSTRAINT PK PRIMARY KEY (storecode,ro,line)) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90('tmpRoLineKeys','tmpROLineKeys.adi',
   'RO', 'storecode;ro', '', 2, 512, '' );   

DROP TABLE tmpFactRepairOrder;
CREATE TABLE tmpFactRepairOrder ( 
  StoreCode CIChar( 3 ) CONSTRAINT NOT NULL,
  RO CIChar( 9 ) CONSTRAINT NOT NULL,
  Line Integer CONSTRAINT NOT NULL,
  Tag CIChar( 5 ) CONSTRAINT NOT NULL,
  OpenDateKey Integer CONSTRAINT NOT NULL,
  CloseDateKey Integer CONSTRAINT NOT NULL,
  FinalCloseDateKey Integer CONSTRAINT NOT NULL,
  LineDateKey Integer CONSTRAINT NOT NULL,
  FlagDateKey Integer CONSTRAINT NOT NULL,
  ServiceWriterKey Integer CONSTRAINT NOT NULL,
  CustomerKey Integer CONSTRAINT NOT NULL,
  VehicleKey Integer CONSTRAINT NOT NULL,
  ServiceTypeKey Integer CONSTRAINT NOT NULL,
  PaymentTypeKey Integer CONSTRAINT NOT NULL,
  CCCKey Integer CONSTRAINT NOT NULL,
  RoCommentKey Integer CONSTRAINT NOT NULL,
  OpCodeKey Integer CONSTRAINT NOT NULL,
  CorCodeGroupKey Integer CONSTRAINT NOT NULL,
  TechGroupKey Integer CONSTRAINT NOT NULL,
  RoStatusKey Integer CONSTRAINT NOT NULL,
  StatusKey Integer CONSTRAINT NOT NULL,
  RoLaborSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoPartsSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  FlagHours Double( 2 ) DEFAULT '0' CONSTRAINT NOT NULL,
  RoFlagHours Double( 2 ) DEFAULT '0' CONSTRAINT NOT NULL,
  Miles Integer DEFAULT '0' CONSTRAINT NOT NULL,
  RoCreatedTS TimeStamp CONSTRAINT NOT NULL,
  LaborSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  PartsSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  Sublet Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoShopSupplies Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoDiscount Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoHazardousMaterials Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoPaintMaterials Money DEFAULT '0' CONSTRAINT NOT NULL,
  CONSTRAINT PK PRIMARY KEY (storecode,ro,line)) IN DATABASE;
  
--/> factRepairOrder ------------------------------------------------------------    

--< todayFactRepairOrder ------------------------------------------------------------ 
ALTER TABLE todayFactRepairOrder
ADD COLUMN FlagDateKey integer CONSTRAINT NOT NULL POSITION 9;

UPDATE todayFactRepairOrder
SET FlagDateKey = (SELECT datekey FROM day WHERE datetype = 'NA');
EXECUTE PROCEDURE sp_CreateIndex90( 
   'todayFactRepairOrder','todayFactRepairOrder.adi','FlagDateKey',
   'FlagDateKey','',2,512,'' );      

ALTER TABLE todayROLine 
ADD COLUMN FlagDate date POSITION 5;  

ALTER TABLE todayRoLineKeys
ADD COLUMN FlagDateKey integer POSITION 5;
   
ALTER PROCEDURE todayXfmFactRepairOrder   
modify exe\todayFactRepairOrder
--/> todayFactRepairOrder ------------------------------------------------------------    
   
  
SELECT * FROM todayPDPPDET a
INNER JOIN todayPDPPHDR b on a.ptpkey = b.ptpkey AND b.ptdoc# = '16134747'
WHERE ptltyp = 'L'
  AND ptcode = 'TT'
  
  
-- ok updated AND ran today, pretty good except i am leaving out a shitload
of stuff for today BY limiting to curdate()
CASE IN point
ro 16134379 opened 10/25
lines closed(ie flagdate) 10/29
ro does NOT show up IN today scrape
but the status has changed FROM IN proc to cashier
AND flag hours can be credited
AND of course, today IS reflecting neither of those changes because those ros
are NOT included IN the scrape
fuckety fuck fuck  

initially thinking
there IS no clear indicator of WHEN an ro changes
so, what DO i have to go on
date change: OPEN, CLOSE, finalclose, line, flag
status change: ro, line
but that implies HAVING a current reality against which the new values can be compared

so, the scrape, based on a JOIN of hdr * det WHERE any of the dates are today?
IF i had scraped based on a flagdate of today, that would have given me the missing ros
seems LIKE the scrape will need to include both tables to get data for either TABLE

something LIKE
pdpphdr 
LEFT JOIN pdpppdet
WHERE anydate = curdate()

OR 
sdprhdr WHERE ro IN (
  rhdr JOIN rdet WHERE anydate = curdate())
-- 10/29 END of the night, got today pdphdr & detail done ok
-- will DO the same thing for sdp
-- db2: etl-todayFactRepairOrder

also need to DO the same thing IN nightly, ie, include the flagdate IN the scrape criteria  
-- 10/30
modified ALL the today code AND it works
on to factRepairOrder AND nightly
still ALL against dailycut\sunday\copy



























