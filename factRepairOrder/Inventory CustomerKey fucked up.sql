/*
9/14/13
the problem came up IN trying to eliminate ros for inventory vehicles
should have been able to simply eliminate (OR include) only those ros with a customerkey = 2
realized that the initial assumption IN deriving dimCustomer that ALL inventory
vehicles had bnkey = 0 was incorrect, mostly true but not completely

does the test belong IN dimCustomer OR factRepairOrder
fctRepairOrder because the issue IS NOT that some idiot created OR used the
wrong row from BOPNAME
but IN assigning the correct BOPNAME row to the repair ORDER

leaning toward a stored proc maintFactRepairOrder, i have come across this 
anomaly, i should reasonable expect others to surface
but, i updated factRepairOrder yesterday, after last nights UPDATE, 3 ros with
this problem FROM within the past 45 days resurfaced
which takes it out of the realm of an after the fact maint stored proc, i don't
want to have to fix the fucker every monring for 45 days
AND pushes it back INTO xfmfactRepairOrder
at the tmpRO level
*/


SELECT customerkey
FROM dimcustomer
WHERE fullname LIKE '%inventory%'
GROUP BY customerkey

SELECT distinct b.thedate, a.ro, c.*
--SELECT COUNT(DISTINCT ro)
FROM factRepairORder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimCustomer c on a.customerkey = c.customerkey
LEFT JOIN stgArkonaBOPNAME d on c.bnkey = d.bnkey
WHERE a.customerkey IN (
  SELECT customerkey
  FROM dimcustomer
  WHERE (
    fullname LIKE '%inventory%'
    AND fullname NOT IN ('EINVENTORYNOW.COM INC','INVENTORY CONTROL SYSTEMS')))
AND a.customerkey <> 2    
ORDER BY ro


SELECT distinct a.ro
FROM factRepairORder a
INNER JOIN dimCustomer c on a.customerkey = c.customerkey
WHERE a.customerkey IN (
  SELECT customerkey
  FROM dimcustomer
  WHERE (
    fullname LIKE '%inventory%'
    AND fullname NOT IN ('EINVENTORYNOW.COM INC','INVENTORY CONTROL SYSTEMS')))
AND a.customerkey <> 2    
 

UPDATE factRepairOrder
SET CustomerKey = 2
WHERE ro IN (
  SELECT distinct a.ro
  FROM factRepairORder a
  INNER JOIN dimCustomer c on a.customerkey = c.customerkey
  WHERE a.customerkey IN (
    SELECT customerkey
    FROM dimcustomer
    WHERE (
      fullname LIKE '%inventory%'
      AND fullname NOT IN ('EINVENTORYNOW.COM INC','INVENTORY CONTROL SYSTEMS')))
  AND a.customerkey <> 2)
AND CustomerKey <> 2;  

SELECT distinct b.thedate, a.ro, c.*
--SELECT COUNT(DISTINCT ro)
FROM factRepairORder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimCustomer c on a.customerkey = c.customerkey
LEFT JOIN stgArkonaBOPNAME d on c.bnkey = d.bnkey
WHERE a.customerkey IN (
  SELECT customerkey
  FROM dimcustomer
  WHERE (
    fullname LIKE '%inventory%'
    AND fullname NOT IN ('EINVENTORYNOW.COM INC','INVENTORY CONTROL SYSTEMS')))
AND a.customerkey <> 2    
ORDER BY ro

SELECT *
FROM stgArkonaBOPNAME 
WHERE 1 = 1
  AND (
    bnsnam LIKE '%inventory%'
    AND bnsnam NOT IN ('EINVENTORYNOW.COM INC','INVENTORY CONTROL SYSTEMS'))
ORDER BY bnkey    
    
SELECT ptckey, COUNT(*)
FROM stgArkonaSDPRHDR
WHERE ptcnam LIKE '%inventory%'
GROUP BY ptckey    

SELECT * FROM stgArkonaSDPRHDR WHERE ptckey = 1001417

SELECT * 
FROM tmpro a
inner JOIN stgArkonaBOPNAME b on a.customer = b.bnkey
  AND (
  b.bnsnam LIKE '%inventory%'
  AND b.bnsnam NOT IN ('EINVENTORYNOW.COM INC','INVENTORY CONTROL SYSTEMS'))
  
SELECT bnkey
SELECT COUNT(*)
FROM stgArkonaBOPNAME
WHERE (
  bnsnam LIKE '%inventory%'
  AND bnsnam NOT IN ('EINVENTORYNOW.COM INC','INVENTORY CONTROL SYSTEMS'))
  
SELECT * FROM tmpro WHERE customer = 0

-- 9/15 -- this IS what IS deployed IN sp xfmFactRepairOrder AND todayXfmFactRepairOrders
ok, FINALLY, this IS it
UPDATE tmpRO.customer (bnkey) to 0 WHERE the customer name IS LIKE inventory
-- these are the rows that need to be updated  
SELECT a.ro,a.customer, a.opendate, b.* 
FROM tmpro a 
LEFT JOIN stgArkonaBOPNAME b on a.customer = b.bnkey  
WHERE (
  bnsnam LIKE '%inventory%'
  AND bnsnam NOT IN ('EINVENTORYNOW.COM INC','INVENTORY CONTROL SYSTEMS'))
  AND a.customer <> 0
  
UPDATE tmpRO
SET customer = 0
WHERE ro IN (
  SELECT a.ro
  FROM tmpro a 
  LEFT JOIN stgArkonaBOPNAME b on a.customer = b.bnkey  
  WHERE (
    bnsnam LIKE '%inventory%'
    AND bnsnam NOT IN ('EINVENTORYNOW.COM INC','INVENTORY CONTROL SYSTEMS'))
    AND a.customer <> 0); 
    
