--< todayFactRepairOrder ------------------------------------------------------<

CREATE TABLE todayRoTechFlagCorSeqGroup (
  flagcor cichar(4), 
  ptro# cichar(9),
  ptline integer,
  ptseq# integer,
  seqgroup integer) IN database;
-- DROP TABLE todayRoTechFlagCor1;
CREATE TABLE todayRoTechFlagCor1 (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  flagDate date,
  pttech cichar(3),
  ptlhrs double,
  flagSeqGroup integer,
  ptcrlo cichar(10),
  corSeqGroup integer,
  ptseq# integer) IN database; 
-- DROP TABLE todayRoTechFlagCor2;  
CREATE TABLE todayRoTechFlagCor2 (  
  storecode cichar(3),
  ro cichar(9),
  line integer,
  flagDate date,
  pttech cichar(3),
  ptcrlo cichar(10),
  ptlhrs double,
  CONSTRAINT PK PRIMARY KEY (storecode,ro,line,flagdate,pttech,ptcrlo)) IN database; 
-- DROP TABLE todayRoTechFlagCorKeys;  
CREATE TABLE todayRoTechFlagCorKeys (  
  storecode cichar(3),
  ro cichar(9),
  line integer,
  flagDateKey integer,
  techKey integer,
  corCodeKey integer,
  ptlhrs double,
  CONSTRAINT PK PRIMARY KEY (storecode,ro,line,flagdatekey,techkey,corcodekey)) IN database; 
  
ALTER TABLE todayRoLine
DROP COLUMN flagdate
DROP COLUMN laborsales
DROP COLUMN linehours;

ALTER TABLE todayRoLineKeys
DROP COLUMN roflaghours
DROP COLUMN flagdatekey
DROP COLUMN corcodegroupkey
DROP COLUMN techgroupkey
DROP COLUMN flaghours
DROP COLUMN laborsales
DROP COLUMN PartsSales;

EXECUTE PROCEDURE sp_RenameDDObject( 'todayfactRepairOrder', 'zUnused_todayfactRepairOrder', 1, 0);
CREATE TABLE todayFactRepairOrder ( 
  StoreCode CIChar( 3 ) CONSTRAINT NOT NULL,
  RO CIChar( 9 ) CONSTRAINT NOT NULL,
  Line Integer CONSTRAINT NOT NULL,
  Tag CIChar( 5 ) CONSTRAINT NOT NULL,
  OpenDateKey Integer CONSTRAINT NOT NULL,
  CloseDateKey Integer CONSTRAINT NOT NULL,
  FinalCloseDateKey Integer CONSTRAINT NOT NULL,
  LineDateKey Integer CONSTRAINT NOT NULL,
  FlagDateKey Integer CONSTRAINT NOT NULL,
  ServiceWriterKey Integer CONSTRAINT NOT NULL,
  CustomerKey Integer CONSTRAINT NOT NULL,
  VehicleKey Integer CONSTRAINT NOT NULL,
  ServiceTypeKey Integer CONSTRAINT NOT NULL,
  PaymentTypeKey Integer CONSTRAINT NOT NULL,
  CCCKey Integer CONSTRAINT NOT NULL,
  RoCommentKey Integer CONSTRAINT NOT NULL,
  OpCodeKey Integer CONSTRAINT NOT NULL,
  RoStatusKey Integer CONSTRAINT NOT NULL,
  LineStatusKey Integer CONSTRAINT NOT NULL,
  RoLaborSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoPartsSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  FlagHours Double( 2 ) DEFAULT '0' CONSTRAINT NOT NULL,
  RoFlagHours Double( 2 ) DEFAULT '0' CONSTRAINT NOT NULL,
  Miles Integer DEFAULT '0' CONSTRAINT NOT NULL,
  RoCreatedTS TimeStamp CONSTRAINT NOT NULL,
  Sublet Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoShopSupplies Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoDiscount Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoHazardousMaterials Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoPaintMaterials Money DEFAULT '0' CONSTRAINT NOT NULL,
  CorCodeKey integer constraint NOT NULL,
  TechKey integer constraint NOT NULL,
  CONSTRAINT PK PRIMARY KEY (storecode,ro,line,flagdatekey,techkey,corcodekey)) IN DATABASE;  

-- it takes longer to DO the INSERT, but i did ALL indexes AND ri prior to inserting new data  
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','StoreCode','StoreCode','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','RO','RO','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','Line','Line','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','Tag','Tag','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','OpenDateKey','OpenDateKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','CloseDateKey','CloseDateKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','FinalCloseDateKey','FinalCloseDateKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','LineDateKey','LineDateKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','FlagDateKey','FlagDateKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','ServiceWriterKey','ServiceWriterKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','CustomerKey','CustomerKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','VehicleKey','VehicleKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','ServiceTypeKey','ServiceTypeKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','PaymentTypeKey','PaymentTypeKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','CCCKey','CCCKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','RoCommentKey','RoCommentKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','OpCodeKey','OpCodeKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','RoStatusKey','RoStatusKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','LineStatusKey','LineStatusKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','RoCreatedTS','RoCreatedTS','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','CorCodeKey','CorCodeKey','',2,512,'');
execute procedure sp_createindex90('todayFactRepairOrder','todayFactRepairOrder.adi','TechKey','TechKey','',2,512,'');


--/> todayFactRepairOrder -----------------------------------------------------/>
-- 12/25/13
remove columns LaborSales AND PartsSales
sales at ro level only
ALTER TABLE tmpFactRepairOrder
DROP COLUMN LaborSales
DROP COLUMN PartsSales;

ALTER TABLE factRepairOrder
DROP COLUMN LaborSales
DROP COLUMN PartsSales;

ALTER TABLE tmpRoLineKeys
DROP COLUMN PartsSales;

ALTER TABLE tmpRoTechFlagCor1
DROP COLUMN ptlamt;
ALTER TABLE tmpRoTechFlagCor2
DROP COLUMN ptlamt;

ALTER TABLE tmpRoTechFlagCorKeys
DROP COLUMN ptlamt;

ALTER TABLE tmpRoTechFlagCor1 ADD COLUMN ptseq# integer;    
--< interim tables ------------------------------------------------------------<
ALTER TABLE tmpRoLine
DROP COLUMN flagdate
DROP COLUMN laborsales
DROP COLUMN linehours;

ALTER TABLE tmpRoLineKeys
DROP COLUMN roflaghours
DROP COLUMN flagdatekey
DROP COLUMN corcodegroupkey
DROP COLUMN techgroupkey
DROP COLUMN flaghours
DROP COLUMN laborsales;


CREATE TABLE tmpRoTechFlagCorSeqGroup (
  flagcor cichar(4), 
  ptro# cichar(9),
  ptline integer,
  ptseq# integer,
  seqgroup integer) IN database;
-- DROP TABLE tmpRoTechFlagCor1;
CREATE TABLE tmpRoTechFlagCor1 (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  flagDate date,
  pttech cichar(3),
  ptlhrs double,
  ptlamt double,
  flagSeqGroup integer,
  ptcrlo cichar(10),
  corSeqGroup integer) IN database; 
-- DROP TABLE tmpRoTechFlagCor2;  
CREATE TABLE tmpRoTechFlagCor2 (  
  storecode cichar(3),
  ro cichar(9),
  line integer,
  flagDate date,
  pttech cichar(3),
  ptcrlo cichar(10),
  ptlhrs double,
  ptlamt double,
  CONSTRAINT PK PRIMARY KEY (storecode,ro,line,flagdate,pttech,ptcrlo)) IN database; 
-- DROP TABLE tmpRoTechFlagCorKeys;  
CREATE TABLE tmpRoTechFlagCorKeys (  
  storecode cichar(3),
  ro cichar(9),
  line integer,
  flagDateKey integer,
  techKey integer,
  corCodeKey integer,
  ptlhrs double,
  CONSTRAINT PK PRIMARY KEY (storecode,ro,line,flagdatekey,techkey,corcodekey)) IN database; 
  
DROP TABLE tmpFactRepairOrder;
CREATE TABLE tmpFactRepairOrder ( 
  StoreCode CIChar( 3 ) CONSTRAINT NOT NULL,
  RO CIChar( 9 ) CONSTRAINT NOT NULL,
  Line Integer CONSTRAINT NOT NULL,
  Tag CIChar( 5 ) CONSTRAINT NOT NULL,
  OpenDateKey Integer CONSTRAINT NOT NULL,
  CloseDateKey Integer CONSTRAINT NOT NULL,
  FinalCloseDateKey Integer CONSTRAINT NOT NULL,
  LineDateKey Integer CONSTRAINT NOT NULL,
  FlagDateKey Integer CONSTRAINT NOT NULL,
  ServiceWriterKey Integer CONSTRAINT NOT NULL,
  CustomerKey Integer CONSTRAINT NOT NULL,
  VehicleKey Integer CONSTRAINT NOT NULL,
  ServiceTypeKey Integer CONSTRAINT NOT NULL,
  PaymentTypeKey Integer CONSTRAINT NOT NULL,
  CCCKey Integer CONSTRAINT NOT NULL,
  RoCommentKey Integer CONSTRAINT NOT NULL,
  OpCodeKey Integer CONSTRAINT NOT NULL,
  RoStatusKey Integer CONSTRAINT NOT NULL,
  LineStatusKey Integer CONSTRAINT NOT NULL,
  RoLaborSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoPartsSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  FlagHours Double( 2 ) DEFAULT '0' CONSTRAINT NOT NULL,
  RoFlagHours Double( 2 ) DEFAULT '0' CONSTRAINT NOT NULL,
  Miles Integer DEFAULT '0' CONSTRAINT NOT NULL,
  RoCreatedTS TimeStamp CONSTRAINT NOT NULL,
  Sublet Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoShopSupplies Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoDiscount Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoHazardousMaterials Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoPaintMaterials Money DEFAULT '0' CONSTRAINT NOT NULL,
  CorCodeKey integer constraint NOT NULL,
  TechKey integer constraint NOT NULL,
  CONSTRAINT PK PRIMARY KEY (storecode,ro,line,flagdatekey,techkey,corcodekey)) IN DATABASE;  
--/> interim tables -----------------------------------------------------------/>



--< factRepairOrder -----------------------------------------------------------<
-- to DO, this IS after adding the data ?
-- need to ADD ri for new key columns
-- ADD indexes for new columns
/*
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimTech-brTechGroup');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimTechGroup-brTechGroup');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimTechGroup-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimOpCodeCor-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimLineStatus-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimCorCodeGroup-brCorCodeGroup');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimOpCode-brCorCodeGroup');

-- these are the correct indexes, they got named incorrectly
DROP index factRepairOrder.ROCREATEDTS;
DROP index factRepairOrder.rostatuskey;
DROP index factRepairOrder.statuskey;

ALTER TABLE factRepairOrder
DROP COLUMN CorCodeGroupKey
DROP COLUMN TechGroupKey;

ALTER TABLE factRepairOrder
ADD COLUMN CorCodeKey integer default '0' constraint NOT NULL
ADD COLUMN TechKey integer default '0' constraint NOT NULL;

-- rename columns
ALTER TABLE factRepairOrder
ALTER COLUMN StatusKey LineStatusKey integer CONSTRAINT NOT NULL;

-- to enter the new data, have to DROP the unique indexes
-- DROP primarykey -- ADD with sp_ModifyTableProperty
ALTER TABLE factRepairOrder
DROP constraint primary key;
DROP index factRepairOrder.PK;

-- should now be ok to to DO the data, fuck, think i will have  DELETE ALL the 
-- data first, the old data will NOT comply with the new unique indexes
OR DO i just want to DROP the TABLE THEN recreate it LIKE tmp
that would mean a script with ALL the indexes AND ALL the RI
yep, let us DO it, first save the old TABLE AS zunused
*/

EXECUTE PROCEDURE sp_RenameDDObject( 'factRepairOrder', 'zUnused_factRepairOrder', 1, 0);

-- these are NOT removed BY renaming the TABLE, DROP them ALL AND recreate 
-- fresh AND clean after recreating the table
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimServiceType-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimPaymentType-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimOpCodeCor-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimOpCode-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimTechGroup-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimDay-factRepairOrderOpenDate');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimDay-factRepairOrderFinalCloseDate');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimCustomer-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimDay-factRepairOrderLineDate');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimDay-factRepairOrderCloseDate');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimDay-factRepairOrderFlagDate');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimServiceWriter-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimRoComment-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimCCC-factRepairOrder');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimLineStatus-factRepairOrder');


CREATE TABLE FactRepairOrder ( 
  StoreCode CIChar( 3 ) CONSTRAINT NOT NULL,
  RO CIChar( 9 ) CONSTRAINT NOT NULL,
  Line Integer CONSTRAINT NOT NULL,
  Tag CIChar( 5 ) CONSTRAINT NOT NULL,
  OpenDateKey Integer CONSTRAINT NOT NULL,
  CloseDateKey Integer CONSTRAINT NOT NULL,
  FinalCloseDateKey Integer CONSTRAINT NOT NULL,
  LineDateKey Integer CONSTRAINT NOT NULL,
  FlagDateKey Integer CONSTRAINT NOT NULL,
  ServiceWriterKey Integer CONSTRAINT NOT NULL,
  CustomerKey Integer CONSTRAINT NOT NULL,
  VehicleKey Integer CONSTRAINT NOT NULL,
  ServiceTypeKey Integer CONSTRAINT NOT NULL,
  PaymentTypeKey Integer CONSTRAINT NOT NULL,
  CCCKey Integer CONSTRAINT NOT NULL,
  RoCommentKey Integer CONSTRAINT NOT NULL,
  OpCodeKey Integer CONSTRAINT NOT NULL,
  RoStatusKey Integer CONSTRAINT NOT NULL,
  LineStatusKey Integer CONSTRAINT NOT NULL,
  RoLaborSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoPartsSales Money DEFAULT '0' CONSTRAINT NOT NULL,
  FlagHours Double( 2 ) DEFAULT '0' CONSTRAINT NOT NULL,
  RoFlagHours Double( 2 ) DEFAULT '0' CONSTRAINT NOT NULL,
  Miles Integer DEFAULT '0' CONSTRAINT NOT NULL,
  RoCreatedTS TimeStamp CONSTRAINT NOT NULL,
  Sublet Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoShopSupplies Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoDiscount Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoHazardousMaterials Money DEFAULT '0' CONSTRAINT NOT NULL,
  RoPaintMaterials Money DEFAULT '0' CONSTRAINT NOT NULL,
  CorCodeKey integer constraint NOT NULL,
  TechKey integer constraint NOT NULL,
  CONSTRAINT PK PRIMARY KEY (storecode,ro,line,flagdatekey,techkey,corcodekey)) IN DATABASE;  

-- it takes longer to DO the INSERT, but i did ALL indexes AND ri prior to inserting new data  
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','StoreCode','StoreCode','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','RO','RO','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','Line','Line','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','Tag','Tag','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','OpenDateKey','OpenDateKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','CloseDateKey','CloseDateKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','FinalCloseDateKey','FinalCloseDateKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','LineDateKey','LineDateKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','FlagDateKey','FlagDateKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','ServiceWriterKey','ServiceWriterKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','CustomerKey','CustomerKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','VehicleKey','VehicleKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','ServiceTypeKey','ServiceTypeKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','PaymentTypeKey','PaymentTypeKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','CCCKey','CCCKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','RoCommentKey','RoCommentKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','OpCodeKey','OpCodeKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','RoStatusKey','RoStatusKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','LineStatusKey','LineStatusKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','RoCreatedTS','RoCreatedTS','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','CorCodeKey','CorCodeKey','',2,512,'');
execute procedure sp_createindex90('factrepairorder','factrepairorder.adi','TechKey','TechKey','',2,512,'');

EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimDay-factRepairOrderOpenDate', 'day', 'factRepairOrder', 'OpenDatekey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimDay-factRepairOrderCloseDate', 'day', 'factRepairOrder', 'CloseDateKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimDay-factRepairOrderFinalCloseDate', 'day', 'factRepairOrder', 'FinalCloseDateKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimDay-factRepairOrderLineDate', 'day', 'factRepairOrder', 'LineDateKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimDay-factRepairOrderFlagDate', 'day', 'factRepairOrder', 'FlagDateKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimServiceWriter-factRepairOrder', 'dimServiceWriter', 'factRepairOrder', 'ServiceWriterKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimCustomer-factRepairOrder', 'dimCustomer', 'factRepairOrder', 'CustomerKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimVehicle-factRepairOrder', 'dimVehicle', 'factRepairOrder', 'VehicleKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimServiceType-factRepairOrder', 'dimServiceType', 'factRepairOrder', 'ServiceTypeKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimCCC-factRepairOrder', 'dimCCC', 'factRepairOrder', 'CCCKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimOpCode-factRepairOrderOpCode', 'dimOpCode', 'factRepairOrder', 'OpCodeKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimRoStatus-factRepairOrder', 'dimRoStatus', 'factRepairOrder', 'RoStatusKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimLineStatus-factRepairOrder', 'dimLineStatus', 'factRepairOrder', 'LineStatusKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimOpCode-factRepairOrderCorCode', 'dimOpCode', 'factRepairOrder', 'CorCodeKey', 2, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('dimTech-factRepairOrder', 'dimTech', 'factRepairOrder', 'TechKey', 2, 2, NULL, '', '');




--/> factRepairOrder ----------------------------------------------------------/>


--< no longer used ------------------------------------------------------------<
EXECUTE PROCEDURE sp_RenameDDObject( 'brCorCodeGroup', 'zUnused_brCorCodeGroup', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'bridgeTechGroup', 'zUnused_bridgeTechGroup', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'brTechGroup', 'zUnused_brTechGroup', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'dimCorCodeGroup', 'zUnused_dimCorCodeGroup', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'dimTechGroup', 'zUnused_dimTechGroup', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'keymapBridgeTechGroup', 'zUnused_keymapBridgeTechGroup', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'tmpBrTechGroup', 'zUnused_tmpBrTechGroup', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'todayXfmDimCorCodeGroup1', 'zUnused_todayXfmDimCorCodeGroup1', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'todayXfmDimCorCodeGroup2', 'zUnused_todayXfmDimCorCodeGroup2', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'todayXfmDimCorCodeGroup3', 'zUnused_todayXfmDimCorCodeGroup3', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'todayXfmDimTechGroup1', 'zUnused_todayXfmDimTechGroup1', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'todayXfmDimTechGroup2', 'zUnused_todayXfmDimTechGroup2', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'todayXfmDimTechGroup3', 'zUnused_todayXfmDimTechGroup3', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'XfmDimCorCodeGroup1', 'zUnused_XfmDimCorCodeGroup1', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'XfmDimCorCodeGroup2', 'zUnused_XfmDimCorCodeGroup2', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'XfmDimCorCodeGroup3', 'zUnused_XfmDimCorCodeGroup3', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'XfmDimCorCodeGroup4', 'zUnused_XfmDimCorCodeGroup4', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'XfmDimTechGroup1', 'zUnused_XfmDimTechGroup1', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'XfmDimTechGroup2', 'zUnused_XfmDimTechGroup2', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'XfmDimTechGroup3', 'zUnused_XfmDimTechGroup3', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'factRO', 'zUnused_factRo', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'factRoLine', 'zUnused_factRoLine', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'xfmRO', 'zUnused_xfmRo', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'xfmRoLine', 'zUnused_xfmRoLine', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'xfmDimTechGroup', 'zUnused_xfmDimTechGroup', 10 , 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'xfmDimCorCodeGroup', 'zUnused_xfmDimCorCodeGroup', 10 , 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'todayxfmDimCorCodeGroup', 'zUnused_todayxfmDimCorCodeGroup', 10 , 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'todayxfmDimTechGroup', 'zUnused_todayxfmDimTechGroup', 10 , 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'factTechFlagHoursByDay', 'zUnused_factTechFlagHoursByDay', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'factTechFlagHoursByDay', 'zUnused_factTechFlagHoursByDay', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'factTechLineFlagDateHours', 'zUnused_factTechLineFlagDateHours', 1, 0);
--/> no longer used -----------------------------------------------------------/>

  
alter PROCEDURE xfmOpCode
   ( 
   ) 
BEGIN 
/*
EXECUTE PROCEDURE xfmOpCode();
*/
DELETE FROM xfmDimOpCode;
INSERT INTO xfmDimOpCode
SELECT soco#, solopc, trim(TRIM(sodes1) + ' ' + TRIM(sodes2) + ' ' + TRIM(sodes3) + ' ' + TRIM(sodes4)) 
FROM stgarkonasdplopc 
WHERE solopc collate ads_default_cs NOT IN('d1999','e9425','j7507','j7921','v9665','3105h4','r06161') 
  AND solopc <> '';

merge dimOpCode a
using xfmDimOpCode b ON a.storecode = b.storecode AND a.opcode = b.opcode
WHEN matched THEN 
UPDATE 
SET a.storecode = b.storecode,
    a.opcode = b.opcode,
    a.description = b.description
WHEN NOT matched THEN 
INSERT (storecode, opcode, description)
values(b.storecode,b.opcode,b.description);

-- need unique keys for invalid corcodes 
INSERT INTO dimOpCode(storecode, opcode, description)
SELECT ptco#, ptcrlo, 'INVALID OPCODE'
FROM (
  SELECT distinct ptco#, ptcrlo FROM tmpsdprdet WHERE ptcrlo <> '' AND ptcode = 'cr' AND ptltyp = 'l'
  UNION 
  SELECT DISTINCT ptco#, ptcrlo FROM tmppdppdet  WHERE ptcrlo <> '' AND ptcode = 'cr' AND ptltyp = 'l') a
WHERE NOT EXISTS (
  SELECT 1
  FROM dimOpcode
  WHERE storecode = a.ptco# 
    AND opcode = a.ptcrlo);

INSERT INTO keyMapDimOpCode
SELECT OpCodeKey, StoreCode, OpCode
FROM dimOpCode a
WHERE NOT EXISTS (
  SELECT 1
  FROM keyMapDimOpCode
  WHERE OpCodeKey = a.OpCodeKey
    AND OpCode = a.OpCode);

END;

  

alter PROCEDURE stgArkSDPRDET
   ( 
   ) 
BEGIN 
/*
EXECUTE PROCEDURE stgArkSDPRDET();
12/24/13
  DO NOT DELETE anything
*/

IF ( -- dup ro#s across stores, IF this ok, can DO stgArk updates based ON RO only (AS opposed to co/ro) which IS MUCH faster
  SELECT COUNT(*)  
  FROM tmpSDPRDET s1
  INNER JOIN tmpSDPRDET s2 ON s1.ptro# = s2.ptro#
  WHERE s1.ptco# <> s2.ptco#) <> 0 THEN
  RAISE stgArkSDPRDET (99, ' uhoh, dup ro#s across stores');    
END IF;
BEGIN TRANSACTION;
TRY 
--  DELETE
--  FROM tmpSDPRDET
--  WHERE ptdbas = 'V'
--    AND ptcode = 'TT';
  DELETE
  FROM stgArkonaSDPRDET
  WHERE ptro# IN (
    SELECT DISTINCT ptro#
    FROM tmpSDPRDET);
  INSERT INTO stgArkonaSDPRDET
  SELECT *
  FROM tmpSDPRDET;   
  COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;
EXECUTE PROCEDURE sp_packtable('stgArkonaSDPRDET');

END;
 