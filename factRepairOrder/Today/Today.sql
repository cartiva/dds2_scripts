/*
9/1 decide to use same proc names IN todayFactRepairOrder
  remove unique index on DependencyObjects.dObject
  remove unique index zProcProcedures.NK
  DROP index zProcProcedures.Proc
EXECUTE PROCEDURE sp_CreateIndex90( 
   'zProcProcedures','zProcProcedures.adi','Proc',
   'Proc','',2,512,'' );  
*/

CREATE TABLE RealTimeBatch ( 
      TheDate Date,
      dObject CIChar( 50 ),
      Complete Date) IN DATABASE;
      
CREATE PROCEDURE todayClearRealTimeBatch()
BEGIN
/*
*/
DELETE FROM RealTimeBatch;
END;      
      
CREATE PROCEDURE todayCheckDependencies
   ( 
      dExecutable CICHAR ( 50 ),
      dObject CICHAR ( 50 ),
      PassFail CICHAR ( 4 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE todayCheckDependencies('dimRoStatus');
*/  
DECLARE @cur CURSOR;
OPEN @cur AS 
  SELECT *
  FROM Dependencies a
  LEFT JOIN RealTimeBatch b ON a.DependsOnObject = b.dObject
    AND b.thedate = curdate()
  WHERE a.dObjObject = (SELECT dObject FROM __input)
    AND a.dObjExecutable = (SELECT dExecutable FROM __input)
    AND b.Complete IS NULL
    AND a.Severity = 'High';
TRY    
  IF FETCH @cur THEN 
    INSERT INTO __output values('Fail');
  ELSE
    INSERT INTO __output values('Pass');
  END IF;    
FINALLY
  CLOSE @cur;
END TRY;
END; 

CREATE PROCEDURE TodayUpdateRealTimeBatch
   ( 
      dObject CICHAR ( 50 ),
      Started LOGICAL,
      Completed LOGICAL
   ) 
BEGIN 
/*
*/
DECLARE @dObject string;
DECLARE @Started logical;
DECLARE @Completed logical;
@dObject = (SELECT dObject FROM __input);
@Started = (SELECT Started FROM __input);
@Completed = (SELECT Completed FROM __input);
  IF @Started = true THEN 
    DELETE FROM RealTimeBatch
    WHERE TheDate = curdate()
      AND dObject = @dObject; 
    INSERT INTO RealTimeBatch (dObject, TheDate)
      values (@dObject, curdate());  
  ENDIF;
  IF @Completed = true THEN
    UPDATE RealTimeBatch
    SET Complete = curdate()
    WHERE TheDate = curdate()
      AND dObject = @dObject;
  ENDIF;

END;  

CREATE PROCEDURE todayCheckRoStatus
   ( 
      PassFail CICHAR ( 4 ) OUTPUT
   ) 
BEGIN 
/*
verify that there are no new OR previously undisclosed ro status IN PDPPHDR
EXECUTE PROCEDURE checkRoStatus();  
*/   
DECLARE @cur CURSOR;
OPEN @cur AS 
  SELECT ptrsts
  FROM todayPDPPHDR a
  LEFT JOIN dimRoStatus b on a.ptrsts = b.RoStatusCode
  WHERE a.ptdtyp = 'RO' 
    AND a.ptdoc# <> ''
    AND b.RoStatus IS NULL;
TRY
  IF FETCH @cur THEN 
    INSERT INTO __output values('Fail');
  ELSE 
    INSERT INTO __output values('Pass');
  END IF;
FINALLY
  CLOSE @cur;
END TRY;      
      
END;



-- new tables: todayPDPPHDR, todayPDPPDET, todaySDPRHDR, todaySDPRDET, 
--             todayRO, todayRoKeys, todayRoLine, todayRoLineKeys
--             todayFactRepairOrder
      
--- Dependencies & zProc ------------------------------------------------------
INSERT INTO zProcExecutables values('todayFactRepairOrder','realtime',CAST('06:10:15' AS sql_time),0);

INSERT INTO zProcProcedures values('todayFactRepairOrder','todayPDPPDET',0,'realtime');
INSERT INTO DependencyObjects values('todayFactRepairOrder','todayPDPPDET','todayPDPPDET');

INSERT INTO zProcProcedures values('todayFactRepairOrder','todayPDPPHDR',0,'realtime');
INSERT INTO DependencyObjects values('todayFactRepairOrder','todayPDPPHDR','todayPDPPHDR');

INSERT INTO zProcProcedures values('todayFactRepairOrder','todaySDPRDET',0,'realtime');
INSERT INTO DependencyObjects values('todayFactRepairOrder','todaySDPRDET','todaySDPRDET');

INSERT INTO zProcProcedures values('todayFactRepairOrder','todaySDPRHDR',0,'realtime');
INSERT INTO DependencyObjects values('todayFactRepairOrder','todaySDPRHDR','todaySDPRHDR');

INSERT INTO zProcProcedures values('todayFactRepairOrder','checkRoStatus',0,'realtime');
INSERT INTO DependencyObjects values('todayFactRepairOrder','checkRoStatus','dimRoStatus');

INSERT INTO zProcProcedures values('todayFactRepairOrder','dimCorCodeGroup',0,'realtime');
INSERT INTO DependencyObjects values('todayFactRepairOrder','dimCorCodeGroup','dimCorCodeGroup');

INSERT INTO zProcProcedures values('todayFactRepairOrder','dimTechGroup',0,'realtime');
INSERT INTO DependencyObjects values('todayFactRepairOrder','dimTechGroup','dimTechGroup');

INSERT INTO zProcProcedures values('todayFactRepairOrder','todayFactRepairOrder',0,'realtime');
INSERT INTO DependencyObjects values('todayFactRepairOrder','todayFactRepairOrder','todayFactRepairOrder');



INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'dimRoStatus'
  AND b.dObject = 'todayPDPPHDR';   
  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'dimCorCodeGroup'
  AND b.dObject = 'todayPDPPHDR';     
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'dimCorCodeGroup'
  AND b.dObject = 'todayPDPPDET';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'dimCorCodeGroup'
  AND b.dObject = 'todaySDPRHDR';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'dimCorCodeGroup'
  AND b.dObject = 'todaySDPRDET';        
  
  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'dimTechGroup'
  AND b.dObject = 'todayPDPPHDR';     
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'dimTechGroup'
  AND b.dObject = 'todayPDPPDET';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'dimTechGroup'
  AND b.dObject = 'todaySDPRHDR';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'dimTechGroup'
  AND b.dObject = 'todaySDPRDET'; 
  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'todayFactRepairOrder'
  AND b.dObject = 'todayPDPPHDR';     
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'todayFactRepairOrder'
  AND b.dObject = 'todayPDPPDET';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'todayFactRepairOrder'
  AND b.dObject = 'todaySDPRHDR';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON a.dExecutable = b.dExecutable
  WHERE a.dObject = 'todayFactRepairOrder'
  AND b.dObject = 'todaySDPRDET';         
    
--/ Dependencies & zProc ------------------------------------------------------   


select *
FROM dependencyobjects

select * FROM dependencies WHERE dobjobject = 'dimTechGroup'

SELECT * FROM realtimebatch;
SELECT * FROM todayPDPPHDR;
SELECT * FROM todayPDPPDET;
SELECT * FROM todaySDPRHDR;
SELECT * FROM todaySDPRDET;
SELECT * FROM todayFactRepairOrder;

SELECT *
FROM dependencies a
LEFT JOIN realtimebatch b on a.dependsonobject = b.dobject
WHERE a.dobjobject = 'dimrostatus'

SELECT *
FROM zprocExecutables a
LEFT JOIN zprocprocedures b on a.executable = b.executable