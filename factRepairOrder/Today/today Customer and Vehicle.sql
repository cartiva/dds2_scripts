including these partial scrapes INTO the populating of dimVehicle & dimCustomer
dimCustomer IS comprehensive; any new customer today
dimVehicle IS limited; only new vehicles FROM ROs today

-- oh shit, DO i need a todayXfmDimCustomer table ??, yep
 
CREATE TABLE todayBOPNAME ()
  same AS tmpBOPNAME
  scrape of BOPNAME WHERE updts = curdate()
CREATE TABLE todayINPMAST () 
  same AS stgArkonaINPMAST    
  scrape of INPMAST WHERE vin IN (PDPPHDR OR SDPRHDR WHERE ptdate = curdate())
CREATE TABLE todayXfmDimCustomer ()  
  
SELECT b.*, c.dobject
FROM zprocexecutables a
LEFT JOIN zprocprocedures b on a.executable = b.executable
LEFT JOIN dependencyobjects c on a.executable = c.dexecutable
  AND b.proc = c.dprocedure
WHERE a.executable = 'todayFactRepairOrder'

INSERT INTO zProcProcedures values('todayFactRepairOrder','todayBOPNAME',1,'realtime');
INSERT INTO zProcProcedures values('todayFactRepairOrder','todayINPMAST',1,'realtime');
INSERT INTO DependencyObjects values('todayFactRepairOrder','todayBOPNAME','dimCustomer');
INSERT INTO DependencyObjects values('todayFactRepairOrder','todayINPMAST','dimVehicle');


-- bumped the sched task to every 5 minutes
SELECT LEFT(executable, 25) AS exec, LEFT(proc,25) AS proc, 
  startts, endts, error, timestampdiff(sql_tsi_second, startts, case when cast(endts AS sql_date) = '12/31/9999' THEN startts ELSE endts end)
FROM zproclog
WHERE executable = 'todayFactRepairOrder'
   AND proc = 'none'
--  AND proc = 'dimOpCode'
--  AND executable = 'dimOpCode'
--AND executable = 'dimVehicle' AND proc = 'none' AND error IS NULL AND hour(startts) BETWEEN 0 AND 5
--  AND CAST(startts AS sql_date) > curdate() -7
  AND CAST(startts AS sql_date) = curdate()
--ORDER BY timestampdiff(sql_tsi_second, startts, case when cast(endts AS sql_date) = '12/31/9999' THEN startts ELSE endts end) desc  
ORDER BY startts DESC, exec, proc
