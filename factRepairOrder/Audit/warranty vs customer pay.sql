DROP TABLE #w;
SELECT c.thedate, d.make, d.model, d.vin, a.ro, a.line, a.flaghours, b.opcode, left(b.description, 50)
INTO #w
FROM factrepairorder a
INNER JOIN dimOpCode b on a.corCodeKey = b.opcodekey
INNER JOIN day c on a.closedatekey = c.datekey
INNER JOIN dimVehicle d on a.vehicleKey = d.vehicleKey
WHERE a.flaghours <> 0
  AND a.paymenttypekey = (
    SELECT paymentTypeKey
    FROM dimPaymentType
    WHERE paymentTypeCode = 'W')
  AND b.opcode NOT IN ('OL', 'DIA', 'N/A', 'LOF')
  AND c.thedate > '05/31/2013';    

DROP TABLE #c;    
SELECT c.thedate, d.make, d.model, d.vin, a.ro, a.line, a.flaghours, b.opcode, left(b.description, 50)
INTO #c
FROM factrepairorder a
INNER JOIN dimOpCode b on a.corCodeKey = b.opcodekey
INNER JOIN day c on a.closedatekey = c.datekey
INNER JOIN dimVehicle d on a.vehicleKey = d.vehiclekey
WHERE a.flaghours <> 0
  AND a.paymenttypekey = (
    SELECT paymentTypeKey
    FROM dimPaymentType
    WHERE paymentTypeCode = 'C')
  AND b.opcode NOT IN ('OL', 'DIA', 'N/A', 'LOF')
  AND c.thedate > '05/31/2013';          
    
    
SELECT a.thedate, a.ro, a.line, a.opcode, a.expr, a.flaghours, 
  b.thedate, b.ro, b.line, b.opcode, b.flaghours, round(b.flaghours/a.flaghours, 2)
FROM #w a
full OUTER JOIN #c b 
  on a.opcode = b.opcode
    AND abs(a.thedate - b.thedate) < 90
    AND b.flaghours > 2 * a.flaghours
    AND a.make = b.make  
    AND a.model = b.model
    AND substring(a.vin, 8, 1) = substring(b.vin, 8, 1)
WHERE a.ro IS NOT NULL
  and b.ro IS NOT NULL 
ORDER BY a.opcode    

-- ry1, servicetype MR, with flaghours
DROP TABLE #a;
SELECT c.thedate, d.modelyear, d.make, d.model, d.vin, 
  substring(d.vin, 8, 1) AS engCode, a.ro, a.line, a.flaghours, b.opcode, 
  left(b.description, 50) AS description, e.paymentTypeCode
INTO #a  
FROM factrepairorder a
INNER JOIN dimOpCode b on a.corCodeKey = b.opcodekey AND a.storecode = b.storecode
INNER JOIN day c on a.closedatekey = c.datekey
INNER JOIN dimVehicle d on a.vehicleKey = d.vehiclekey
INNER JOIN dimPaymentType e on a.PaymentTypeKey = e.paymentTypeKey
WHERE a.flaghours > 0
  AND a.storecode = 'RY1'
  AND b.opcode NOT IN ('DIA', 'N/A', 'LOF', 'SHOP', '0590032', 'MFPDI',
    '0590072', '13A','13C','1010030','10B','23I','24E','24F','24M','2NT','4NT',
    '3439929','3K','4029929','4041510','4041512','48M','48R','1NT','6B','ACC',
    'ALIGN','ASIS','BULB','CERT','CKCHG','LOFD','Z7000','Z6998','WTFF','WTF',
    'WPSF','WFIF','WCFD','WBN','WBFF','WB','ROT','NITROM','NICE','FREEROT',
    'FREE10B')
  AND c.theyear > 2012
  AND d.make <> 'UNKNOWN'
  AND b.description NOT LIKE 'INVALID%'
  AND a.serviceTypeKey IN (
    SELECT serviceTypeKey
    from dimServiceType
    WHERE serviceTypeCode = 'MR');
    

select * FROM #a WHERE opcode = 'OL' ORDER BY flaghours DESC 
  
SELECT a.*, b.thedate, b.ro, b.line, b.flaghours, b.paymenttypecode
FROM (  
  SELECT modelyear, make, model, engCode, opcode, description
  FROM #a
  GROUP BY modelyear, make, model, engCode, opcode, description) a 
LEFT JOIN #a b on a.modelyear = b.modelyear AND a.make = b.make
  AND a.model = b.model AND a.engCode = b.engcode AND a.opcode = b.opcode
  AND a.description = b.description   
ORDER BY a.opcode  

-- what are the most frequent vehicles seen
SELECT modelyear, make, model, engcode, COUNT(*)
FROM (  
  SELECT modelyear, make, model, engcode, ro
  FROM #a  
  GROUP BY modelyear, make, model, engcode, ro) a
GROUP BY modelyear, make, model, engcode  
ORDER BY COUNT(*) DESC 