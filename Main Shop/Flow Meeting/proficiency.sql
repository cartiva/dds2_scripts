SELECT a.storecode, a.name, a.firstname, a.lastname, a.employeenumber, a.employeekey, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, a.payrollclass
FROM edwEmployeeDim a
WHERE a.currentrow = true
  AND a.Active = 'Active'
GROUP BY a.storecode, a.name, a.firstname, a.lastname, a.employeenumber, a.employeekey, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, a.payrollclass
  
  
SELECT a.storecode, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, a.payrollclass, COUNT(*), MIN(name), MAX(name), fullparttime
FROM edwEmployeeDim a
WHERE a.currentrow = true
  AND a.Active = 'Active'
GROUP BY a.storecode, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, a.payrollclass, fullparttime
  
SELECT * FROM stgArkonaPYPRJOBD

SELECT * FROM stgArkonaPYPRHEAD
SELECT COUNT(*) FROM stgArkonaPYPRHEAD      


SELECT a.storecode, a.name, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, a.payrollclass, fullparttime, b.yrjobd, c.yrtext
-- SELECT COUNT(*)
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
  AND b.yrjobd <> '' -- takes care of dups
LEFT JOIN ( -- takes care of dups
  select yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD 
  WHERE yrtext <> ''
  group by yrco#, yrjobd, yrtext) c on a.storecode = c.yrco# AND b.yrjobd = c.yrjobd
WHERE a.currentrow = true
  AND a.Active = 'Active'
ORDER BY name  


SELECT a.storecode, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, a.payrollclass, COUNT(*), MIN(name), MAX(name), fullparttime, b.yrjobd, c.yrtext
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
  AND b.yrjobd <> '' -- takes care of dups
LEFT JOIN ( -- takes care of dups
  select yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD 
  WHERE yrtext <> ''
  group by yrco#, yrjobd, yrtext) c on a.storecode = c.yrco# AND b.yrjobd = c.yrjobd
WHERE a.currentrow = true
  AND a.Active = 'Active'
GROUP BY a.storecode, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, a.payrollclass, fullparttime, b.yrjobd, c.yrtext
  
-- spreadsheets for ben foster
SELECT a.storecode, a.name, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, a.payrollclass, a.fullparttime, b.yrjobd, c.yrtext
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
  AND b.yrjobd <> '' -- takes care of dups
LEFT JOIN ( -- takes care of dups
  select yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD 
  WHERE yrtext <> ''
  group by yrco#, yrjobd, yrtext) c on a.storecode = c.yrco# AND b.yrjobd = c.yrjobd
WHERE a.currentrow = true
  AND a.Active = 'Active'
ORDER BY storecode, name  

SELECT *
FROM stgArkonaPYPRHEAD a
WHERE a.yrco# <> 'RY3'
  AND yrjobd <> ''  
  
SELECT yrco#, yrjobd, yrtext
-- SELECT *
FROM stgArkonaPYPRJOBD  
WHERE yrjobd <> ''
GROUP BY yrco#, yrjobd, yrtext

-- leave out the distcode  
SELECT a.storecode, a.pydeptcode, a.pydept, 
  a.payrollclass, COUNT(*) AS HowMany, fullparttime, b.yrjobd, c.yrtext
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
  AND b.yrjobd <> '' -- takes care of dups
LEFT JOIN ( -- takes care of dups
  select yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD 
  WHERE yrtext <> ''
  group by yrco#, yrjobd, yrtext) c on a.storecode = c.yrco# AND b.yrjobd = c.yrjobd
WHERE a.currentrow = true
  AND a.Active = 'Active'
GROUP BY a.storecode, a.pydeptcode, a.pydept, 
  a.payrollclass, fullparttime, b.yrjobd, c.yrtext  
ORDER BY storecode, pydeptcode, b.yrjobd


-- Detail 24: Service Recon
SELECT a.storecode, a.name, a.pydeptcode, a.pydept, 
  a.payrollclass, fullparttime, b.yrjobd, c.yrtext
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
  AND b.yrjobd <> '' -- takes care of dups
LEFT JOIN ( -- takes care of dups
  select yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD 
  WHERE yrtext <> ''
  group by yrco#, yrjobd, yrtext) c on a.storecode = c.yrco# AND b.yrjobd = c.yrjobd
WHERE a.currentrow = true
  AND a.Active = 'Active'
  AND a.pydeptcode = '24'
ORDER BY a.payrollclass, a.name

select *
FROM dimtech
WHERE flagdeptcode = 're'
ORDER BY technumber
  
SELECT employeenumber, technumber
FROM dimtech
WHERE flagdeptcode = 're'
  AND employeenumber <> 'NA'
GROUP BY  employeenumber, technumber  
ORDER BY technumber

-- ok, this matches bev's holy graille
-- only exception IS this includes Reed Otto
SELECT a.storecode, a.name, employeenumber, employeekey
FROM edwEmployeeDim a
WHERE a.currentrow = true
  AND a.Active = 'Active'
  AND a.pydeptcode = '04'
  AND a.storecode = 'RY1'
ORDER BY storecode, name 

-- Friday Oct 25
-- clockhours
SELECT a.name, aa.technumber, SUM(b.ClockHours) AS Total, 
  SUM(b.RegularHours) AS Straight, SUM(b.OVertimeHours) AS OT
INTO #clock  
FROM edwEmployeeDim a
LEFT JOIN dimTech aa on a.employeenumber = aa.employeenumber
  AND aa.currentrow = true
LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
  AND b.datekey IN (SELECT datekey FROM day WHERE thedate = '10/25/2013')
WHERE a.currentrow = true
  AND a.Active = 'Active'
  AND a.pydeptcode = '04'
  AND a.storecode = 'RY1'
GROUP BY a.name, aa.technumber  
UNION
SELECT ' OCT 25', 'SHOP', SUM(b.ClockHours) AS Total, 
  SUM(b.RegularHours) AS Straight, SUM(b.OVertimeHours) AS OT
FROM edwEmployeeDim a
LEFT JOIN dimTech aa on a.employeenumber = aa.employeenumber
  AND aa.currentrow = true
LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
  AND b.datekey IN (SELECT datekey FROM day WHERE thedate = '10/25/2013')
WHERE a.currentrow = true
  AND a.Active = 'Active'
  AND a.pydeptcode = '04'
  AND a.storecode = 'RY1'

-- DROP TABLE #flag;
SELECT name, x.technumber, coalesce(flaghours, 0) AS Flag
INTO #flag
FROM (  
  SELECT a.name, aa.technumber
  FROM edwEmployeeDim a
  LEFT JOIN dimTech aa on a.employeenumber = aa.employeenumber
    AND aa.currentrow = true
  WHERE a.currentrow = true
    AND a.Active = 'Active'
    AND a.pydeptcode = '04'
    AND a.storecode = 'RY1'
  GROUP BY a.name, aa.technumber) x
LEFT JOIN (   
  SELECT c.technumber, round(SUM(a.flaghours*b.weightfactor), 1) AS flaghours
  FROM factRepairOrder a
  INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c on b.techkey = c.techkey
  WHERE closedatekey = (SELECT datekey FROM day WHERE thedate = '10/25/2013')  
    AND a.storecode = 'ry1'
  GROUP BY c.technumber) y on x.technumber = y.technumber
UNION   
SELECT ' OCT 25', 'SHOP', sum(flaghours)
FROM (  
  SELECT a.name, aa.technumber
  FROM edwEmployeeDim a
  LEFT JOIN dimTech aa on a.employeenumber = aa.employeenumber
    AND aa.currentrow = true
  WHERE a.currentrow = true
    AND a.Active = 'Active'
    AND a.pydeptcode = '04'
    AND a.storecode = 'RY1'
  GROUP BY a.name, aa.technumber) x
LEFT JOIN (   
  SELECT c.technumber, round(SUM(a.flaghours*b.weightfactor), 1) AS flaghours
  FROM factRepairOrder a
  INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c on b.techkey = c.techkey
  WHERE closedatekey = (SELECT datekey FROM day WHERE thedate = '10/25/2013')  
    AND a.storecode = 'ry1'
  GROUP BY c.technumber) y on x.technumber = y.technumber  
  
SELECT a.*, b.Flag, 
  CASE
    WHEN total = 0 OR flag = 0 THEN 0
    ELSE round(100*flag/total, 1)
  END AS Prof
FROM #clock a
full OUTER JOIN #flag b on a.name = b.name
ORDER BY a.technumber


-- ok, what the fuck am  i doing
mind fucking on WHEN to capture flaghours
CLOSE date - final CLOSE date
IN the old factro/factroline i had a flag date
i am NOT currently extracting that
DO NOT know/remember why
so lets look at it (again)

IN the  old system, flagdate was extracted FROM sdprdet AS such:
-- DROP TABLE #wtf;
SELECT ptro#, ptline, ptdate AS flagdate, pttech, ptlhrs AS flaghours
INTO #wtf
FROM stgArkonaSDPRDET
WHERE ptdate BETWEEN '10/01/2013' AND curdate()
  AND ptco# = 'RY1'
  AND ptltyp = 'L'  
  AND ptcode = 'TT'
  AND LEFT(ptro#, 2) = '16'
  
SELECT a.ro, a.line, b.thedate AS opendate, c.thedate AS closedate,
  d.thedate AS finalclosedate, f.technumber, f.name, a.flaghours*e.weightfactor AS flaghours
FROM factRepairOrder a
LEFT JOIN day b on a.opendatekey = b.datekey
LEFT JOIN day c on a.closedatekey = c.datekey
LEFT JOIN day d on a.finalclosedatekey = d.datekey
INNER JOIN brTechGroup e on a.techgroupkey = e.techgroupkey
INNER JOIN dimtech f on e.techkey = f.techkey
WHERE opendatekey IN (SELECT datekey FROM day WHERE thedate BETWEEN '10/01/2013' AND curdate())  
  AND a.storecode = 'ry1'
  AND LEFT(ro,2) = '16'
  
  
SELECT a.*, b.*,
  CASE
     WHEN a.opendate = a.closedate AND a.opendate = a.finalclosedate THEN 'All the Same'
     WHEN b.flagdate = a.opendate THEN 'Open'
     WHEN b.flagdate = a.closedate THEN 'Close'
     WHEN b.flagdate = a.finalclosedate THEN 'FinalClose'
     ELSE 'BINGO'
   END AS flagdatematch
FROM (
  SELECT a.ro, a.line, b.thedate AS opendate, c.thedate AS closedate,
    d.thedate AS finalclosedate, f.technumber, f.name, a.flaghours*e.weightfactor AS flaghours
  FROM factRepairOrder a
  LEFT JOIN day b on a.opendatekey = b.datekey
  LEFT JOIN day c on a.closedatekey = c.datekey
  LEFT JOIN day d on a.finalclosedatekey = d.datekey
  INNER JOIN brTechGroup e on a.techgroupkey = e.techgroupkey
  INNER JOIN dimtech f on e.techkey = f.techkey
    AND f.flagdeptcode = 'MR'
  WHERE opendatekey IN (SELECT datekey FROM day WHERE thedate BETWEEN '10/01/2013' AND curdate())  
    AND a.storecode = 'ry1'
    AND LEFT(ro,2) = '16') a  
LEFT JOIN #wtf b on a.ro = b.ptro# AND a.line = b.ptline  
WHERE a.technumber = '627' AND flagdate = '10/25/2013'

--fuckety fuck fuck, i am putting linedate IN factRepairOrder
which i am basing on ptltyp = A
but flagdate should be based on ptltyp = L AND ptcode = TT

so it looks LIKE i have to ADD flagdate to factrepairorder
so, change the db connection to a previous daily cut AND WORK it the fuck out
