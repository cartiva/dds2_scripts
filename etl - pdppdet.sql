-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	  WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
  	WHEN data_type = 'numeric' THEN 'integer,'
  	WHEN data_type = 'timestmp' THEN 'timestamp,'
  	WHEN data_type = 'smallint' THEN 'integer,'
  	WHEN data_type = 'integer' THEN 'double,'
  	WHEN data_type = 'decimal' THEN 'double,'
  	WHEN data_type = 'date' THEN 'date,'
  	WHEN data_type = 'time' THEN 'cichar(8),'
  	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'PDPPDET' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'PDPPDET'  
AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'PDPPDET'  
AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaPDPPDET
-- 3. revise DDL based ON step 2, CREATE table
CREATE TABLE stgArkonaPDPPDET (
ptco#      cichar(3),          
ptpkey     integer,             
ptline     integer,             
ptltyp     cichar(1),          
ptseq#     integer,             
ptcpid     cichar(3),          
ptcode     cichar(2),          
ptsoep     cichar(1),          
ptdate     date,             
ptmanf     cichar(3),          
ptpart     cichar(25),         
ptplng     integer,            
ptsgrp     cichar(3),          
ptbinl     cichar(7),          
ptqty      integer,             
ptcost     money,             
ptlist     money,             
pttrad     money,             
ptfprc     money,             
ptnet      money,             
ptblist    money,             
ptmeth     cichar(1),          
ptpct      integer,             
ptbase     cichar(1),          
ptspcd     cichar(1),          
ptpovr     cichar(1),          
ptgprc     cichar(1),          
ptcore     cichar(1),          
ptaddp     cichar(1),          
ptsppc     integer,             
ptorso     cichar(1),          
ptxcld     cichar(1),          
ptcmnt     cichar(50),         
ptlsts     cichar(1),          
ptsvctyp   cichar(2),          
ptlpym     cichar(1),          
ptpadj     cichar(1),          
pttech     cichar(3),          
ptlopc     cichar(10),         
ptcrlo     cichar(10),         
ptlhrs     double,             
ptlchr     double,             
ptarla     money,             
ptfail     cichar(6),          
ptplvl     integer,            
ptpsas     integer,             
ptslv#     cichar(9),          
ptsli#     cichar(9),          
ptdcpn     integer,             
ptfact     double,             
ptacode    cichar(15),         
ptbawc     cichar(15),         
ptdtlc     double,             
ptinta     cichar(10),         
ptlauth    cichar(7),          
ptntot     double,             
ptoamt     double,             
ptohrs     double,             
ptpmcs     double,             
ptpmrt     double,             
ptrprp     cichar(1),          
pttxin     cichar(1),          
ptupsl     cichar(1),          
ptwctp     cichar(1),          
ptvatcode  cichar(1),          
ptvatamt   double,             
ptrtninvf  cichar(1),          
ptskllvl   cichar(1),          
ptoemrplf  cichar(1),          
ptcstdif$  double,             
ptoptseq#  integer,             
ptdisstrz  cichar(12),       
ptdsclbr$  double,             
ptdscprt$  double,             
ptpickzne  cichar(5),          
ptdispty1  cichar(5),          
ptdispty2  cichar(5),          
ptdisrank1 integer,             
ptdisrank2 integer,             
ptdspadate cichar(12),                
ptdspatime cichar(8),          
ptdspmdate cichar(12),               
ptdspmtime cichar(8)) IN database;     

CREATE TABLE tmpPDPPDET (
) IN database;        

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PDPPDET';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PDPPDET';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaPDPPDET';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
 
