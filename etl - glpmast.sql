SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'integer,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'money,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'glpmast' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'glpmast'  
AND data_type <> 'CHAR'

-- non char fields for spreadsheet
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'glpmast'  
AND data_type <> 'CHAR'

CREATE TABLE stgArkonaGLPMAST (
gmco#      cichar(3),          
gmmode     cichar(1),          
gmyear     integer,              
gmacct     cichar(10),         
gmtype     cichar(1),          
gmdesc     cichar(30),         
gmstyp     cichar(1),          
gmctyp     cichar(1),          
gmdept     cichar(2),          
gmtypb     cichar(1),          
gmdboa     cichar(10),         
gmcroa     cichar(10),         
gmopct     double,              
gmaclr     money,              
gmwoac     cichar(10),         
gmschd     cichar(1),          
gmrecon    cichar(1),          
gmcntu     cichar(1),          
gmopt1     cichar(1),          
gmopt2     cichar(1),          
gmopt3     cichar(1),          
gmopt4     cichar(1),          
gmopt5     cichar(1),          
gmatbb     money,              
gmat01     money,              
gmat02     money,              
gmat03     money,              
gmat04     money,              
gmat05     money,              
gmat06     money,              
gmat07     money,              
gmat08     money,              
gmat09     money,              
gmat10     money,              
gmat11     money,              
gmat12     money,              
gmat13     money,              
gmutbb     integer,              
gmut01     integer,              
gmut02     integer,              
gmut03     integer,              
gmut04     integer,              
gmut05     integer,              
gmut06     integer,              
gmut07     integer,              
gmut08     integer,              
gmut09     integer,              
gmut10     integer,              
gmut11     integer,              
gmut12     integer,              
gmut13     integer,              
gmactive   cichar(1)) IN database;            

-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'GLPMAST';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = coalesce((SELECT '[' + TRIM(column_name) + '], '  FROM #pymast WHERE ordinal_position = @j), '');
-- just COLUMN names
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
-- parameter names  
--  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM #pymast WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'GLPMAST';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaGLPMAST';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
 
