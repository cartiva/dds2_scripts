SELECT sh.ptco#, sh.ptro#, sh.ptfran, sd.ptline, sd.ptseq#, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, sd.ptsvctyp, sd.ptlpym,
  CASE WHEN ptltyp = 'A' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS LineDate, 
  CASE WHEN ptltyp = 'L' AND ptcode = 'TT' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS FlagDate,
  sh.ptdate AS OpenDate, sh.ptcdat AS CloseDate, sh.ptfcdt AS FinalCloseDate,
  sd.ptlamt, sd.ptlopc, sd.ptcrlo, 'SDET' AS source, sh.ptswid, 'Closed' AS status

SELECT COUNT(*)  -- 75226 / 78124
FROM tmpSDPRHDR sh
LEFT JOIN tmpSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
--  AND EXISTS ( --
--    SELECT 1
--    FROM stgArkonaGLPTRNS
--    WHERE gtdoc# = sh.ptro#
--    AND gtpost = 'Y')  
  AND sh.ptfran <> '' 
AND sd.ptline IS NOT NULL;

SELECT COUNT(*)  -- 1907975 / 1919253
FROM stgArkonaSDPRHDR sh
LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
    AND gtpost = 'Y')  
  AND sh.ptfran <> '' 
AND sd.ptline IS NOT NULL;

-- those ros w/out accounting entry
SELECT COUNT(*)  -- 11278
FROM stgArkonaSDPRHDR sh
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
--  AND sh.ptco# IN ('RY1', 'RY2','RY3') -- don't need this, done IN scrape
  AND NOT EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
    AND gtpost = 'Y')  
  AND sh.ptfran <> '' 

-- #wtf are the records to be added after getting rid of the GL filter
SELECT *
INTO #wtf
FROM stgArkonaSDPRHDR sh
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND NOT EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
    AND gtpost = 'Y')  
  AND sh.ptfran <> '' 

-- bingo, they conform
SELECT ptco#, ptro#
FROM #wtf
GROUP BY ptco#, ptro#
HAVING COUNT(*) > 1

-- which ones are void
SELECT ptco#, ptro#, ptcnam, ptdate, ptcdat, ptfcdt, pttest,ptptot, ptltot, ptchk#
FROM #wtf
WHERE ptcnam <> '*VOIDED REPAIR ORDER*'
  AND ptro# LIKE '1608%'
  
-- are ALL ptchk# LIKE V% voids?
-- CLOSE enough
SELECT ptco#, ptro#, ptcnam, ptdate, ptcdat, ptfcdt, pttest,ptptot, ptltot, ptchk#
FROM #wtf
WHERE ptchk# LIKE 'v%'
-- ALL are void IN 5250
SELECT MIN(ptro#), MAX(ptro#), ptchk#, COUNT(*)
FROM #wtf
WHERE ptchk# LIKE 'v%'
GROUP BY ptchk#

SELECT ptco#, ptro#, ptcnam, ptdate, ptcdat, ptfcdt, pttest,ptptot, ptltot, ptchk#
SELECT COUNT(*) -- 5120
FROM stgArkonaSDPRHDR 
WHERE (
  ptcnam = '*VOIDED REPAIR ORDER*' OR 
  ptchk# LIKE 'v%')