just LIKE with dimtech, what i want to DO IS implement this structure
type1: name
type2: active  
       dept
       
currently thinking the type2 dept will be censusDept, defServType has no
relevance outside of census, but unlike dimTech the relationship BETWEEN
defServType AND censusDept IS ambiguous, at best
       
WHERE active/dept determines census of active writers for a dept
one problem, there are a shitload of writers that are NOT IN fact writers  
eg, parts guys need a writer# but are NOT writers    
none of whom are relevant to the writer census

are there departmental writers? cashier

individuals who are active writers IN both stores and/OR multiple depts

leaning toward adding some COLUMN LIKE census dept, which, of course, means that
maintenance of dimServiceWriter IS completely manual

anomalies
  wiebusch 2 overlapping writer numbers
what DO i know for sure about SDPSWTR
-- just LIKE dimTech, Arkona will NOT allow the reuse of a store/writer#
-- store/writer unique
SELECT swco#, swswid
FROM stgArkonaSDPSWTR
GROUP BY swco#, swswid
HAVING COUNT(*) > 1
               
SELECT swco#, swmisc, COUNT(*)
FROM stgArkonaSDPSWTR
WHERE swco# <> 'ry3'
  AND swactive = 'y'
GROUP BY swco#, swmisc
ORDER BY swco#, swmisc

SELECT *
FROM stgArkonaSDPSWTR a
LEFT JOIN (
  SELECT storecode, writerid, MIN(b.thedate), MAX(b.thedate)
  FROM factro a
  INNER JOIN day b ON a.opendatekey = b.datekey
  WHERE void = false 
  GROUP BY storecode, writerid) c ON a.swco# = c.storecode AND a.swswid = c.writerid
WHERE a.swco# <> 'ry3'
  AND a.swactive = 'y'
ORDER BY a.swco#, a.swmisc, a.swswid  

-- writers with no ros
SELECT *
FROM (
  SELECT *
  FROM stgArkonaSDPSWTR a
  LEFT JOIN (
    SELECT storecode, writerid, MIN(b.thedate) AS minDate, MAX(b.thedate) AS maxDate
    FROM factro a
    INNER JOIN day b ON a.opendatekey = b.datekey
    WHERE void = false 
    GROUP BY storecode, writerid) c ON a.swco# = c.storecode AND a.swswid = c.writerid
  WHERE a.swco# <> 'ry3'
    AND a.swactive = 'y') x
WHERE minDate IS NULL     
ORDER BY swco#, swmisc, swswid


-- inactive writers w/dates
SELECT *
FROM (
  SELECT *
  FROM stgArkonaSDPSWTR a
  LEFT JOIN (
    SELECT storecode, writerid, MIN(b.thedate) AS minDate, MAX(b.thedate) AS maxDate
    FROM factro a
    INNER JOIN day b ON a.opendatekey = b.datekey
    WHERE void = false 
    GROUP BY storecode, writerid) c ON a.swco# = c.storecode AND a.swswid = c.writerid
  WHERE a.swco# <> 'ry3'
    AND a.swactive = 'n') x
ORDER BY swco#, swmisc, swswid

-- exclude
-- inactive with no ro's
SELECT *
FROM (
  SELECT *
  FROM stgArkonaSDPSWTR a
  LEFT JOIN (
    SELECT storecode, writerid, MIN(b.thedate) AS minDate, MAX(b.thedate) AS maxDate
    FROM factro a
    INNER JOIN day b ON a.opendatekey = b.datekey
    WHERE void = false 
    GROUP BY storecode, writerid) c ON a.swco# = c.storecode AND a.swswid = c.writerid
  WHERE a.swco# <> 'ry3'
    AND a.swactive = 'n') x
WHERE mindate IS NULL     


-- Script BEGIN ---------------------------------------------------------------
ALTER TABLE dimServiceWriter
ADD COLUMN RowChangeReason cichar(100)
ALTER COLUMN Department CensusDept cichar(3)
ADD COLUMN ServiceWriterKey  autoinc
ADD COLUMN Description cichar(20) -- raw SDPSWTR name, name comes FROM edwEmployeeDim 
ADD COLUMN CurrentRow logical
ADD COLUMN RowChangeDate date
ADD COLUMN RowChangeDateKey integer
ADD COLUMN RowFromTS timestamp
ADD COLUMN RowThruTS timestamp
ADD COLUMN ServiceWriterKeyFromDate date
ADD COLUMN ServiceWriterKeyFromDateKey integer
ADD COLUMN ServiceWriterKeyThruDate date
ADD COLUMN ServiceWriterKeyThruDateKey integer;

SELECT a.*, c.mindate, c.maxdate
INTO #wtf
FROM stgArkonaSDPSWTR a
LEFT JOIN (
  SELECT storecode, writerid, MIN(b.thedate) AS minDate, MAX(b.thedate) AS maxDate
  FROM factro a
  INNER JOIN day b ON a.opendatekey = b.datekey
  WHERE void = false 
  GROUP BY storecode, writerid) c ON a.swco# = c.storecode AND a.swswid = c.writerid
WHERE a.swco# <> 'ry3';

-- excludes inactive/no ros
SELECT a.*
INTO #wtf1
FROM #wtf a
WHERE not EXISTS (
  SELECT 1
  FROM #wtf
  WHERE swactive = 'n'
    AND mindate IS NULL 
    AND swco# = a.swco#
    AND swswid = a.swswid);

/*    
-- make some attempt to determine which of these fucks are active employee
-- would LIKE this JOIN to be ON employeenumber, but IF that IS NULL THEN ON name
-- why no edwEmployeeDim return for lueker, tyrrell, vacura, euliss ...
SELECT a.*, b.*
FROM #wtf1 a
LEFT JOIN (
  SELECT storecode, employeenumber, name, hiredate, termdate
  FROM edwEmployeeDim 
  WHERE currentrow = true) b ON (a.swco# = b.storecode AND a.swemp# = b.employeenumber)
    OR (a.swco# = b.storecode AND 
     TRIM(substring(swname,position(' ' IN swname)+1, 20)) = 
      trim(CASE position(',' IN b.name) -- one name with no commas
        WHEN 0 THEN LEFT(b.name, position (' ' IN b.name)- 1) 
        ELSE  LEFT(b.name, position (',' IN b.name)- 1) 
      END))    
*/
-- thinking fuck it, this IS going to be a manual deal
-- #wtf1 are the writers that are going IN, just start whittling away      
-- start with a subset WHERE employeenumber works
-- ADD those, THEN move ON
-- SELECT * FROM #wtf1

-- active with matching emp# IN edwEmployeeDim ------------------ 
DELETE FROM dimServiceWriter;  
INSERT INTO dimservicewriter (storecode, employeenumber, name, description, 
  writernumber, active, defaultservicetype, censusdept, currentrow, 
  servicewriterkeyfromdate, servicewriterkeyfromdatekey,
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT a.swco#, b.employeenumber, b.name, a.swname, 
  a.swswid, true, swmisc, 
  CASE 
    WHEN swmisc = 'bs' THEN 'BS'
    WHEN swmisc = 'mr' AND pydept = 'Service Team Leaders' THEN 'MR'
    WHEN swmisc = 'ql' THEN 'QL'
    WHEN swmisc = 're' THEN 'RE'
    ELSE 'XX'
  END, true,
  coalesce(mindate, hiredate), (SELECT datekey FROM day WHERE thedate = coalesce(mindate, hiredate)),
  (SELECT thedate FROM day WHERE datetype <> 'date'),
  (SELECT datekey FROM day WHERE datetype <> 'date')
FROM #wtf1 a
INNER JOIN (
  SELECT storecode, employeenumber, name, hiredate, termdate, pydept
  FROM edwEmployeeDim 
  WHERE currentrow = true) b ON a.swco# = b.storecode AND a.swemp# = b.employeenumber
WHERE swactive = 'y'  
  AND termdate > curdate();  
// active with matching emp# IN edwEmployeeDim ------------------  

-- active with no/ros, no emp#, no dates ------------------------
INSERT INTO dimServiceWriter (storecode, description, writernumber, active,
  defaultservicetype, censusdept, currentrow)
SELECT swco#, swname, swswid, true,
  swmisc, 'XX', true
FROM #wtf1 a
WHERE swactive = 'y'
  AND swco# <> 'ry3'
  AND NOT EXISTS (
    SELECT 1
    FROM dimServiceWriter
    WHERE storecode = a.swco#
      AND writernumber = a.swswid)  
  AND mindate IS NULL; 
// active with no/ros, no emp#, no dates ------------------------  

-- active with ros, no emp#, swkeyfrom/thru ---------------------
-- go ahead AND DO these partial inserts, will have to go back AND fill IN the
-- necessary stuff ON stuff with a real censusDept (name, employeenumber, dtusername)
-- DO it IN chunks
INSERT INTO dimservicewriter (storecode, description, 
  writernumber, active, defaultservicetype, censusdept, currentrow, 
  servicewriterkeyfromdate, servicewriterkeyfromdatekey,
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT a.swco#, a.swname, 
  a.swswid, true, swmisc, swmisc, true,
  mindate, (SELECT datekey FROM day WHERE thedate = mindate),
  (SELECT thedate FROM day WHERE datetype <> 'date'),
  (SELECT datekey FROM day WHERE datetype <> 'date')
--SELECT *  
FROM #wtf1 a
WHERE  swco# <> 'ry3'
  AND swactive = 'y'
  AND NOT EXISTS (
    SELECT 1
    FROM dimServiceWriter
    WHERE storecode = a.swco#
      AND writernumber = a.swswid) 
  AND mindate IS NOT NULL  
  AND swco# = 'ry1' -- chunks
  AND swmisc IN ('AM','BS'); -- chunks
--ORDER BY swco#, swmisc, swname    
-- DO it IN chunks
INSERT INTO dimservicewriter (storecode, description, 
  writernumber, active, defaultservicetype, censusdept, currentrow, 
  servicewriterkeyfromdate, servicewriterkeyfromdatekey,
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT a.swco#, a.swname, 
  a.swswid, true, swmisc, 
  CASE 
    WHEN swswid IN ('999','717','48','44','526','734','700','19','600','802') THEN 'XX'
    ELSE 'MR'
  END, true,
  mindate, (SELECT datekey FROM day WHERE thedate = mindate),
  (SELECT thedate FROM day WHERE datetype <> 'date'),
  (SELECT datekey FROM day WHERE datetype <> 'date')  
-- SELECT *  
FROM #wtf1 a
WHERE  swco# <> 'ry3'
  AND swactive = 'y'
  AND NOT EXISTS (
    SELECT 1
    FROM dimServiceWriter 
    WHERE storecode = a.swco#
      AND writernumber = a.swswid) 
  AND mindate IS NOT NULL  
  AND swco# = 'ry1'
  AND swmisc = 'mr';
-- ORDER BY swco#, swmisc, swname    
-- DO it IN chunks
INSERT INTO dimservicewriter (storecode, description, 
  writernumber, active, defaultservicetype, censusdept, currentrow, 
  servicewriterkeyfromdate, servicewriterkeyfromdatekey,
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT a.swco#, a.swname, 
  a.swswid, true, swmisc, 
  CASE 
    WHEN swswid IN ('740','644','401','728','410') THEN 'QL'
    ELSE 'XX'
  END, true,
  mindate, (SELECT datekey FROM day WHERE thedate = mindate),
  (SELECT thedate FROM day WHERE datetype <> 'date'),
  (SELECT datekey FROM day WHERE datetype <> 'date')  
--SELECT *  
FROM #wtf1 a
WHERE  swco# <> 'ry3'
  AND swactive = 'y'
  AND NOT EXISTS (
    SELECT 1
    FROM dimServiceWriter
    WHERE storecode = a.swco#
      AND writernumber = a.swswid) 
  AND mindate IS NOT NULL  
  AND swco# = 'ry1'
  AND swmisc = 'ql';  
--ORDER BY swco#, swmisc, swname  
-- DO it IN chunks
INSERT INTO dimservicewriter (storecode, description, 
  writernumber, active, defaultservicetype, censusdept, currentrow, 
  servicewriterkeyfromdate, servicewriterkeyfromdatekey,
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT a.swco#, a.swname, 
  a.swswid, true, swmisc, 
  CASE 
    WHEN swswid IN ('124','623','658') THEN 'MR'
    WHEN swswid = '127' THEN 'RE'
    WHEN swswid IN ('424','644','728') THEN 'QL'
    ELSE 'XX'
  END, true,
  mindate, (SELECT datekey FROM day WHERE thedate = mindate),
  (SELECT thedate FROM day WHERE datetype <> 'date'),
  (SELECT datekey FROM day WHERE datetype <> 'date')  
--SELECT *  
FROM #wtf1 a
WHERE  swco# <> 'ry3'
  AND swactive = 'y'
  AND NOT EXISTS (
    SELECT 1
    FROM dimServiceWriter
    WHERE storecode = a.swco#
      AND writernumber = a.swswid) 
  AND mindate IS NOT NULL  
  AND swco# = 'ry2';
-- DO it IN chunks
-- at this point, ALL that IS LEFT IS inactive tech numbers
SELECT * 
INTO #type2 
FROM #wtf1 a
WHERE  swco# <> 'ry3'
  AND NOT EXISTS (
    SELECT 1
    FROM dimServiceWriter
    WHERE storecode = a.swco#
      AND writernumber = a.swswid); 

-- non current rows
DECLARE @NowTS timestamp;
@NowTS = (SELECT now() FROM system.iota);
INSERT INTO dimServiceWriter (storecode, description, writernumber, active, 
  defaultservicetype, censusdept, currentrow, rowchangedate, rowchangedatekey, 
  rowthruts, rowchangereason, servicewriterkeyfromdate, servicewriterkeyfromdatekey,
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT swco#, swname, swswid, true, 
  swmisc, swmisc, false, curdate(), (SELECT datekey FROM day WHERE thedate = curdate()),
  @NowTS, 'active to inactive', mindate, (SELECT datekey FROM day WHERE thedate = mindate),
  maxDate, (SELECT datekey FROM day WHERE thedate = maxDate)
FROM #type2;     
-- current rows
INSERT INTO dimServiceWriter (storecode, description, writernumber, active, 
  defaultservicetype, censusdept, currentrow, 
  rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey,
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT swco#, swname, swswid, false,
  swmisc, swmisc, true,
  @NowTS, mindate, (SELECT datekey FROM day WHERE thedate = mindate),
  maxDate, (SELECT datekey FROM day WHERE thedate = maxDate)
FROM #type2;    
// active with ros, no emp#, swkeyfrom/thru ---------------------   
// Script BEGIN ---------------------------------------------------------------

CREATE TABLE xfmServiceWriterDim ( 
      [swco#] CIChar( 3 ),
      swswid CIChar( 3 ),
      swname CIChar( 20 ),
      [swemp#] CIChar( 9 ),
      swactive CIChar( 1 ),
      swmisc CIChar( 2 )) IN DATABASE;

DELETE FROM xfmServiceWriterDim;      
INSERT INTO xfmServiceWriterDim
SELECT *
FROM stgArkonaSDPSWTR
WHERE ((swco# = 'ry1' AND swswid NOT IN ('110','124','34','36','38','404','422','45','46','800','801','803','809','813','933','N07'))
  OR (swco# = 'ry2' AND swswid NOT IN ('12','27','685','688','720','75','803','FO','KLB','LA','LIN','NEW','SEY')))
  AND swco# <> 'ry3';      
  

/*    
-- get the names and emp#s for current writers
-- done
SELECT *
FROM dimServiceWriter
WHERE currentrow = true
  AND active = true
  AND censusDept <> 'XX'   
ORDER BY storecode, censusdept, description  
*/

/*
-- cleanup, current census looks ok except for
-- michelle cochran (ry1 bs) & al berry (ry1 mr)
-- change them both to XX for now, AS their writer numbers are still both active IN Arkona
SELECT c.*, a.*
FROM dimServiceWriter a
LEFT JOIN (
  SELECT storecode, writerid, MIN(b.thedate) AS minDate, MAX(b.thedate) AS maxDate
  FROM factro a
  INNER JOIN day b ON a.opendatekey = b.datekey
  WHERE void = false 
  GROUP BY storecode, writerid) c ON a.storecode = c.storecode AND a.writernumber = c.writerid
WHERE a.currentrow = true
  AND a.active = true
  AND a.censusDept <> 'XX'   
ORDER BY a.storecode, a.censusdept, a.description   

UPDATE dimServiceWriter
SET censusDept = 'XX'
WHERE storecode = 'ry1' 
  AND writernumber IN ('91','721')
*/
-- current census
SELECT *
FROM dimServiceWriter
WHERE currentrow = true
  AND active = true
  AND censusDept <> 'XX'   
ORDER BY storecode, censusdept, description  

 
  
  
-- type1 name changes
SELECT *
FROM dimServiceWriter a
LEFT JOIN (
  SELECT storecode, employeenumber, name
  FROM edwEmployeeDim
  WHERE currentrow = true
    AND active = 'active') b ON a.storecode = b.storecode
    AND a.employeenumber = b.employeenumber
WHERE a.currentrow = true
  AND a.active = true
  AND a.censusDept <> 'XX'   
  AND a.name <> b.name
ORDER BY a.storecode, a.censusdept, a.description     

-- earliest ro date: 6/12/13
-- 636 earliest ro date: none
SELECT ptswid, MIN(ptdate)
FROM stgArkonaSDPRHDR
WHERE ptswid = '440'
  AND year(ptdate) = 2014
GROUP BY ptswid


/*
12/27
Kyle Bragunier has been an ry1 pdq writer
starting 12/20 he IS now an ry2 pdq writer, altho his IS still an ry1 employee
IN arkona, per Ned, he IS now an ry2 employee

1/7/14
  new writer Ben Dalen, 723 RY1 MR

3/3/14
  Daniel Wiebusch, a pdq writer for RY1, IS now also a pdq writer for RY1 
  with a diff writer number (430)
  
    
SELECT ptco#, COUNT(*), MIN(ptdate), MAX(ptdate)
FROM stgarkonasdprhdr
WHERE year(ptdate)= 2014
and ptswid = '443'
GROUP BY ptco#

SELECT * FROM tmpsdprhdr WHERE ptswid = '445'

*/
/*
-- new writer  
SELECT * 
FROM xfmServiceWriterDim a
WHERE NOT EXISTS (
  SELECT 1
  FROM dimServiceWriter
  WHERE storecode = a.swco#
    AND writernumber = a.swswid)
*/    
DECLARE @Swswid cichar(3);
DECLARE @swco# cichar(3);
DECLARE @EmpNo cichar(7);
DECLARE @Name cichar(25);
DECLARE @dtUserName cichar(20);
DECLARE @FromDate date;
@swswid = '445';
@swco# = 'RY1';
@EmpNo = '162795';
@Name = 'HARRIS, SEAN';
@dtUserName = '';
@FromDate = '12/15/2014';
/**/
INSERT INTO dimServiceWriter (storecode, employeenumber, name, description,
  writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
  servicewriterkeyfromdate,servicewriterkeyfromdatekey,
  servicewriterkeythrudate,servicewriterkeythrudatekey)
/**/  
SELECT a.swco#, @EmpNo, @Name, a.swname,
  a.swswid, @dtUserName, true, swmisc,
  -- don't need to test for pydept, this IS being done manually, AND i will NOT
  -- be adding shit LIKE parts guys OR bus off staff  
--  CASE 
--    WHEN swmisc = 'bs' THEN 'BS'
--    WHEN swmisc = 'mr' AND pydept = 'Service Team Leaders' THEN 'MR'
--    WHEN swmisc = 'ql' THEN 'QL'
--    WHEN swmisc = 're' THEN 'RE'
--    ELSE 'XX'
--  END, 
  swmisc, true,
  @FromDate, (SELECT datekey FROM day WHERE thedate = @FromDate),
  (SELECT thedate FROM day WHERE datetype <> 'date'),  
  (SELECT datekey FROM day WHERE datetype <> 'date')
FROM xfmServiceWriterDim a  
WHERE swswid = @swswid
  AND swco# = @swco#;
   
--< active changes ------------------------------------------------------------<

-- type2, generate an email for now  
-- active   
--@activeChange = (   
--  SELECT COUNT(*)
SELECT a.servicewriterkey, a.storecode, a.employeenumber, a.name, 
  a.description, a.writernumber, a.censusdept, c.termdate
FROM dimServiceWriter a
INNER JOIN xfmServiceWriterDim b ON a.storecode = b.swco#
  AND a.writernumber = b.swswid
  AND b.swactive = 'N'
LEFT JOIN edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true	
WHERE a.currentrow = true
  AND a.active = true
  AND a.censusDept <> 'XX'
    
	
11/11/16
just LIKE with terms, i no longer LIKE the way i have been modelling the data
so, i am going to just UPDATE the current row to active = false
ADD a rowchangedate, AND rowchangereason
am NOT going to fuck with servicewriterkey at ALL
these are the writers i need to deactivate
ALL except rodriguez have actually been termed a long time ago, i DO NOT know
why they are just now showing up IN inactive writers,
use their termdate for rowchange date

-- turns out rodriguez was a mistake, NOT de-activated

servicewriterkey	storecode	employeenumber	name	  	   		 	 description		  writernumber	censusdept
556					RY1			1122320			"SATTLER, NICHOLAS G"	 Nic Sattler		  92			BS
524					RY1			128120			"CRANE, AUBREY LYNN"	 AUBREY CRANE		  127			RE
779					RY1			123925			"CESPEDES, TREVOR D"	 Trevor Cespedes	  135			RE
813					RY1			178200			"KJENSTAD, CHRIS T"		 Chris Kjenstad		  455	  		QL
-- 839					RY1			1117842			"RODRIGUEZ, JUSTIN"		 Justin Rodriguez	  473			QL


UPDATE dimServiceWriter
SET rowchangedate = x.termdate,
    rowchangereason = 'active to inactive',
	active = false
FROM (
  SELECT a.servicewriterkey, a.storecode, a.employeenumber, a.name, 
    a.description, a.writernumber, a.censusdept, c.termdate
  FROM dimServiceWriter a
  INNER JOIN xfmServiceWriterDim b ON a.storecode = b.swco#
    AND a.writernumber = b.swswid
    AND b.swactive = 'N'
  LEFT JOIN edwEmployeeDim c on a.employeenumber = c.employeenumber
    AND c.currentrow = true	
  WHERE a.currentrow = true
    AND a.active = true
    AND a.censusDept <> 'XX') x	
where dimServiceWriter.serviceWriterKey = x.serviceWriterKey;

-- employeenumbers with multiple currentrows 
SELECT storecode, employeenumber, currentrow
FROM dimserviceWriter
WHERE currentrow = true
  AND employeenumber IS NOT NULL 
GROUP BY storecode, employeenumber, currentrow
HAVING COUNT(*) > 1

select * FROM dimservicewriter WHERE employeenumber = '1122320'


-- 8/12/14, i have been doing these on a one to one basis, well, today, i have
/*
--   4 writers that have been deactivated, might be a good time to generalize
--   the process a bit, at least SET variables rather than hard coding ALL
--   values ALL the time   
servicewr- storecode  employeen- name                      description          writernum- censusdept
-----------------------------------------------------------------------------------------------------
       761 RY1            188420 LURWIG, DAVID             David Lurwig                441 QL        
       728 RY1            118310 BRAGUNIER, KYLE D         Kyle Bragunier              433 QL        
       597 RY1           1143865 VACURA, SCOTT             Scott Vacura     728        728 QL        
       518 RY1           1106420 OLSON, MATTHEW JOHN       Matt Olson                  431 QL        
       
IN addition, some of these writers are currently valid writer numbers at ry2 AS well

SELECT a.servicewriterkey, a.storecode, a.employeenumber, a.name, a.description, 
  a.writernumber, a.censusdept, c.termdate
FROM dimServiceWriter a
INNER JOIN xfmServiceWriterDim b ON a.storecode = b.swco#
  AND a.writernumber = b.swswid
  AND b.swactive = 'N'
LEFT JOIN edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true  
WHERE a.currentrow = true
  AND a.active = true
  AND a.censusDept <> 'XX'    
  
dates of interest:
  last clock date
  last ro date
  term date (payroll)      
  
active writers that have been termed
SELECT a.*, b.termdate
FROM dimservicewriter a
LEFT JOIN edwEmployeeDim b on a.storecode = b.storecode
  AND a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE a.active = true
  AND a.currentrow = true
  AND a.censusDept <> 'xx'
  AND coalesce(b.termdate, cast('12/31/9999' AS sql_date)) < curdate()    
*/  

*** relevant values query
SELECT k.*, m.maxRoDate
FROM (
  SELECT c.storecode, c.employeenumber, c.name, c.writernumber, c.censusdept, 
    max(d.termdate) AS maxTermDate, MAX(f.thedate) AS maxClockDate
  FROM ( -- active state has changed
    SELECT a.servicewriterkey, a.storecode, a.employeenumber, a.name, a.description, 
      a.writernumber, a.censusdept
    FROM dimServiceWriter a
    INNER JOIN xfmServiceWriterDim b ON a.storecode = b.swco#
      AND a.writernumber = b.swswid
      AND b.swactive = 'N'
    WHERE a.currentrow = true
      AND a.active = true
      AND a.censusDept <> 'XX') c
  LEFT JOIN edwEmployeeDim d on c.employeenumber = d.employeenumber
  LEFT JOIN edwClockHoursFact e on d.employeekey = e.employeekey  
    AND clockhours + vacationhours + ptohours + holidayhours > 0
  LEFT JOIN day f on e.datekey = f.datekey  
  GROUP BY c.storecode, c.employeenumber, c.name, c.writernumber, c.censusdept) k
LEFT JOIN (   
  SELECT c.storecode, c.employeenumber, c.name, c.writernumber, c.censusdept,
    MAX(e.thedate) AS maxRoDate
  FROM ( -- active state has changed
    SELECT a.servicewriterkey, a.storecode, a.employeenumber, a.name, a.description, 
      a.writernumber, a.censusdept
    FROM dimServiceWriter a
    INNER JOIN xfmServiceWriterDim b ON a.storecode = b.swco#
      AND a.writernumber = b.swswid
      AND b.swactive = 'N'
    WHERE a.currentrow = true
      AND a.active = true
      AND a.censusDept <> 'XX') c
  LEFT JOIN factRepairOrder d on c.serviceWriterKey = d.serviceWriterKey   
  LEFT JOIN day e on d.opendatekey = e.datekey 
  GROUP BY c.storecode, c.employeenumber, c.name, c.writernumber, c.censusdept) m on k.storecode = m.storecode
    AND k.writernumber = m.writernumber

-- ok, IN spite of my uneasiness about this whole business, these writer numbers
--   have been flagged AS inactive (BY somebody) IN arkona 
/* 8/12/14
  ry1 431 olson
  ry1 728 vacura
  ry1 433 bragunier
  ry1 441 lurwig
IN addition, i will deactivate John Kazmierczak AND UPDATE accordingly 
which i fucked up, need to include censusDept

4/23/15:
  brian collins, ben dalen, mat flikka, jason hunter
  used the above query to get the relevant values
  wrapped the UPDATE AND INSERT INTO a transaction
*/  

/*
5/2/15
select * FROM dimservicewriter 
WHERE writernumber IN ('724','442','526','433') AND storecode = 'ry1'
ORDER BY writernumber, servicewriterkey;

HDDan (i am assuming) took it upon himself to deactivate some ry1 writers yesterday
so, need to make the type 2 change
ry1 724  scholand  10/16/14
ry1 442  perez  02/09/15
ry1 526  evenson 05/21/14
ry1 433  bragunier 04/23/15

-- need last ro date for each of these guys
SELECT c.writernumber, MAX(b.thedate), MAX(a.ro)
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimservicewriter c on a.servicewriterkey = c.servicewriterkey
  AND c.storecode = 'ry1'
  AND c.writernumber IN ('724','442','526','433')
GROUP BY c.writernumber 
*/
DECLARE @rowChangeReason string;
DECLARE @thruDate date;
DECLARE @thruDateKey integer;
DECLARE @storeCode string;
DECLARE @writerNumber string;
DECLARE @fromDate date;
DECLARE @fromDateKey integer;
DECLARE @censusDept string;
@rowChangeReason = 'active to inactive';
@thruDate = '07/20/2015'; -----------------------------------------------**
@thruDateKey = (SELECT datekey FROM day WHERE thedate = @thruDate);
@storeCode = 'ry1'; -----------------------------------------------------**
@writerNumber = '705'; --------------------------------------------------**
@fromDate = @thruDate + 1;
@fromDateKey = (SELECT datekey FROM day WHERE thedate = @fromDate);
@censusDept = 'MR'; -----------------------------------------------------**
--UPDATE old record  
BEGIN TRANSACTION;
TRY 
  UPDATE dimServiceWriter
  SET currentrow = false,
      rowchangedate = curdate(),
      rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
      RowThruTS = now(),
      rowchangereason = @rowChangeReason,
      ServiceWriterKeyThruDate = @thruDate,
      ServiceWriterKeyThruDateKey = @thruDateKey
  WHERE storeCode = @storeCode 
    and writernumber = @writerNumber
    AND censusDept = @censusDept
    AND currentrow = true; -- added this 5/2/15, only updating the current row
  --INSERT new record 
  INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
    writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
    rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
    servicewriterkeythrudate, servicewriterkeythrudatekey)
  SELECT storecode, employeenumber, name, description, 
    writernumber, dtusername, false, defaultservicetype, censusdept, true,
    now(), @fromDate, @fromDateKey,
    (SELECT thedate FROM day WHERE datetype = 'NA'),
    (SELECT datekey FROM day WHERE datetype = 'NA')
  FROM dimServiceWriter
  WHERE storeCode = @storeCode
    AND writernumber = @writerNumber
    AND censusDept = @censusDept; 
  COMMIT WORK;  
CATCH ALL
  ROLLBACK;
  RAISE;
END;




SELECT ptswid, MIN(ptdate), MAX(ptdate), COUNT(*)
FROM stgarkonasdprhdr
WHERE ptswid IN ('430','431')
GROUP BY ptswid
-- use the last ro ptdate for serviceWriterKeyThruDate
-- 430: 5/31
-- 431: 5/25
--UPDATE old record  
UPDATE dimServiceWriter
SET currentrow = false,
    rowchangedate = curdate(),
    rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
    RowThruTS = now(),
    rowchangereason = 'active to inactive',
    ServiceWriterKeyThruDate = '05/31/2013',
    ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = '05/31/2013')    
WHERE writernumber = '430';  
--INSERT new record 
INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
  writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
  rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT storecode, employeenumber, name, description, 
  writernumber, dtusername, false, defaultservicetype, censusdept, true,
  now(), '06/01/2013', (SELECT datekey FROM day WHERE thedate = '06/01/2013'),
  (SELECT thedate FROM day WHERE datetype = 'NA'),
  (SELECT datekey FROM day WHERE datetype = 'NA')
FROM dimServiceWriter
WHERE writernumber = '430';  

--UPDATE old record  
UPDATE dimServiceWriter
SET currentrow = false,
    rowchangedate = curdate(),
    rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
    RowThruTS = now(),
    rowchangereason = 'active to inactive',
    ServiceWriterKeyThruDate = '05/25/2013',
    ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = '05/25/2013')    
WHERE writernumber = '431';  
--INSERT new record 
INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
  writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
  rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT storecode, employeenumber, name, description, 
  writernumber, dtusername, false, defaultservicetype, censusdept, true,
  now(), '05/26/2013', (SELECT datekey FROM day WHERE thedate = '05/26/2013'),
  (SELECT thedate FROM day WHERE datetype = 'NA'),
  (SELECT datekey FROM day WHERE datetype = 'NA')
FROM dimServiceWriter
WHERE writernumber = '431'; 

-- ethan collings ry1 writer# 740, termed 11-15, 
--UPDATE old record  
UPDATE dimServiceWriter
SET currentrow = false,
    rowchangedate = curdate(),
    rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
    RowThruTS = now(),
    rowchangereason = 'active to inactive',
    ServiceWriterKeyThruDate = '11/15/2013',
    ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = '11/15/2013')    
WHERE writernumber = '740';  
--INSERT new record 
INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
  writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
  rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT storecode, employeenumber, name, description, 
  writernumber, dtusername, false, defaultservicetype, censusdept, true,
  now(), '11/16/2013', (SELECT datekey FROM day WHERE thedate = '11/16/2013'),
  (SELECT thedate FROM day WHERE datetype = 'NA'),
  (SELECT datekey FROM day WHERE datetype = 'NA')
FROM dimServiceWriter
WHERE writernumber = '740'; 


    
-- 4/4/14
  kim Bestul (424) has been deactivated IN arkona
  termed 2/28/14
last ro opened was on 2/28/14  
SELECT b.thedate, a.*
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE serviceWriterKey = 601
ORDER BY thedate DESC

shit, IN dimServiceWriter, she also shows AS an active ry1 writer (424)
hmm, the only ry1 ros are FROM 11/23/2012
SELECT b.thedate, a.*
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE serviceWriterKey = 583
ORDER BY thedate DESC

so, for key 583 (ry1) deactivate on 11/24/12
for key 601 (ry2) deactivate on 02/29/14

--UPDATE old record  
-- ry1
UPDATE dimServiceWriter
SET currentrow = false,
    rowchangedate = curdate(),
    rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
    RowThruTS = now(),
    rowchangereason = 'active to inactive',
    ServiceWriterKeyThruDate = '11/23/2012',
    ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = '11/23/2012')    
WHERE writernumber = '424'
  AND storeCode = 'RY1';  
--INSERT new record 
INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
  writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
  rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT storecode, employeenumber, name, description, 
  writernumber, dtusername, false, defaultservicetype, censusdept, true,
  now(), '11/24/2012', (SELECT datekey FROM day WHERE thedate = '11/24/2012'),
  (SELECT thedate FROM day WHERE datetype = 'NA'),
  (SELECT datekey FROM day WHERE datetype = 'NA')
FROM dimServiceWriter
WHERE writernumber = '424'
  AND storeCode = 'RY1'; 
  
--UPDATE old record  
-- ry2
UPDATE dimServiceWriter
SET currentrow = false,
    rowchangedate = curdate(),
    rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
    RowThruTS = now(),
    rowchangereason = 'active to inactive',
    ServiceWriterKeyThruDate = '02/28/2014',
    ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = '02/28/2014')    
WHERE writernumber = '424'
  AND storeCode = 'RY2';  
--INSERT new record 
INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
  writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
  rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT storecode, employeenumber, name, description, 
  writernumber, dtusername, false, defaultservicetype, censusdept, true,
  now(), '03/01/2014', (SELECT datekey FROM day WHERE thedate = '03/01/2014'),
  (SELECT thedate FROM day WHERE datetype = 'NA'),
  (SELECT datekey FROM day WHERE datetype = 'NA')
FROM dimServiceWriter
WHERE writernumber = '424'
  AND storeCode = 'RY2';  
  
-- 12/2/15 Justice Bauer, termed 11/30
--UPDATE old record  
-- ry1 pdq
UPDATE dimServiceWriter
SET currentrow = false,
    rowchangedate = curdate(),
    rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
    RowThruTS = now(),
    rowchangereason = 'active to inactive',
    ServiceWriterKeyThruDate = '11/30/2015',
    ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = '11/30/2015')    
WHERE writernumber = '452'
  AND storeCode = 'RY1';  
--INSERT new record 
INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
  writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
  rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT storecode, employeenumber, name, description, 
  writernumber, dtusername, false, defaultservicetype, censusdept, true,
  now(), '12/01/2015', (SELECT datekey FROM day WHERE thedate = '12/01/2015'),
  (SELECT thedate FROM day WHERE datetype = 'NA'),
  (SELECT datekey FROM day WHERE datetype = 'NA')
FROM dimServiceWriter
WHERE writernumber = '452'
  AND storeCode = 'RY1';  
  
-- FROM *** relevant values query    
-- these 2 started showing up AS no longer active IN dec 2015
storecode	employeenumber	name	                  writernumber	censusdept	maxTermDate	maxClockDate	maxRoDate
RY1	      174870	        "JOHNSTON, NICHOLAS E"	432	          QL	        08/30/2015	08/29/2015	  08/29/2015
RY1	      184867	        "LEMAR, BRENNAN J"	    440	          QL	        07/29/2015	07/29/2015	  07/28/2015

--UPDATE old record  
-- ry1 pdq
DECLARE @term_date date;
DECLARE @new_date date;
DECLARE @row_change_reason string;
DECLARE @writer_number string;
DECLARE @store_code string;
@term_date = '07/29/2015';
@new_date = '07/30/2015';
@row_change_reason = 'active to inactive';
@writer_number = '440';
@store_code = 'RY1';
BEGIN TRANSACTION;
TRY 
  UPDATE dimServiceWriter
  SET currentrow = false,
      rowchangedate = curdate(),
      rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
      RowThruTS = now(),
      rowchangereason = @row_change_reason,
      ServiceWriterKeyThruDate = @term_date,
      ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = @term_date)    
  WHERE writernumber = @writer_number
    AND storeCode = @store_code;  
  --INSERT new record 
  INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
    writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
    rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
    servicewriterkeythrudate, servicewriterkeythrudatekey)
  SELECT storecode, employeenumber, name, description, 
    writernumber, dtusername, false, defaultservicetype, censusdept, true,
    now(), @new_date, (SELECT datekey FROM day WHERE thedate = @new_date),
    (SELECT thedate FROM day WHERE datetype = 'NA'),
    (SELECT datekey FROM day WHERE datetype = 'NA')
  FROM dimServiceWriter
  WHERE writernumber = @writer_number
    AND storeCode = @store_code; 
  COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;    
  
--/> active changes -----------------------------------------------------------/> 


-- defServiceType   

  SELECT b.*, a.*
  FROM dimServiceWriter a
  INNER JOIN xfmServiceWriterDim b ON a.storecode = b.swco#
    AND a.writernumber = b.swswid
    AND a.DefaultServiceType <> b.swmisc
  WHERE a.currentrow = true
    AND a.active = true
    AND a.censusDept <> 'XX';
-- 8/25, effective 8/5/13
--         wiebusch/428 went FROM MR to QL
--         kazmierczak/423 went FROM QL to MR
-- so, what changes: DefaultServiceTyp, CensusDept, currentrow   
-- 11/6/13 gary ramberg, old writer number still active IN system, 734
--         has moved FROM call center to writing on 11/4/13
-- 2/7/14
--       added a few variables to make this a little better
--       ry2, adam lindquist FROM QL to MR
-- 10/30/14 joel changed garret to MR FROM QL, because "he IS mostly writing service"
-- 9/1/15 AND now he wants him relassified AS QL per zendesk request 1026, 
--        his concern i believe IS with email AND np
/********************************************************************************************************************/
-- *a*
except this violates NK uniquenes, currently: StoreCode;WriterNumber;active;CensusDept
AND the non current row for garrett IS the same AS the new row, id QL
which once again raises the stupid question of what does active mean
i DO NOT fucking know
so, since i am unsure, let me change shit up
FROM now on, a non current row can NOT be active, so there
/********************************************************************************************************************/

--UPDATE old record  
DECLARE @now timestamp;
DECLARE @store string;
DECLARE @writerNumber string;
DECLARE @thruDate date;
DECLARE @fromDate date;
@store = 'RY2';
@writerNumber = '526';
@thruDate = '08/31/2015';
@fromDate = '09/01/2015';
@now = (SELECT now() FROM system.iota);
BEGIN TRANSACTION;
TRY 
/* 
  TRY 
    UPDATE dimServiceWriter
    SET currentrow = false,
        rowchangedate = curdate(),
        rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
        RowThruTS = @now,
        rowchangereason = 'change CensusDept',
        ServiceWriterKeyThruDate = @thruDate,
        ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = @thruDate),    
    -- *a*
        Active = false    
    WHERE storecode = @store
      and writernumber = @writerNumber  
      AND currentrow = true;
  CATCH ALL
    RAISE failUpdate(1, 'Update Failed');
  END TRY;
*/ 
  INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
    writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
    rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
    servicewriterkeythrudate, servicewriterkeythrudatekey)  
    
  SELECT b.swco#, a.employeenumber, a.name, a.description, 
    a.writernumber, a.dtusername, true, b.swmisc AS st, b.swmisc AS census, true,
    now(), @fromDate, (SELECT datekey FROM day WHERE thedate = @fromDate),
    (SELECT thedate FROM day WHERE datetype = 'NA'),
    (SELECT datekey FROM day WHERE datetype = 'NA')
  -- this IS dodgy, but i have other shit to be doing now  
  --  SELECT b.*, a.*
    FROM dimServiceWriter a
    INNER JOIN xfmServiceWriterDim b ON a.storecode = b.swco#
      AND a.writernumber = b.swswid
      AND a.DefaultServiceType <> b.swmisc
  --  WHERE a.currentrow = true
  --    AND a.active = true
  --    AND a.censusDept <> 'XX';
      AND storecode = @store
      AND writernumber = @writernumber;
      
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;    

  
-- new ramberg row
INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
  writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
  rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
  servicewriterkeythrudate, servicewriterkeythrudatekey)  
SELECT a.storecode, '1114105', 'RAMBERG,GARY', a.description, 
  a.writernumber, 'RYDEGARYRA', a.active, 'MR', 'MR', true,
  now(), '11/04/2013', (SELECT datekey FROM day WHERE thedate = '11/04/2013'),
  (SELECT thedate FROM day WHERE datetype = 'NA'),
  (SELECT datekey FROM day WHERE datetype = 'NA')
FROM dimServiceWriter a   
WHERE writernumber = '734'   

--< 8/25/13 fixed -------------------------------------------------------------
-- shit, noticed that the servicewriterkeythrudate IS NOT SET to 12/31/9999 ON the 
-- new type2 rows
-- fuck fuck fuck, ALL the type2 new rows have the same from/thru dates AS the orig rows
-- should probably figure out IF i did that ON purpose, AND IF so, for what reason
SELECT servicewriterkey, storecode, name, description, writernumber, servicewriterkeyfromdate, 
  servicewriterkeythrudate, rowchangedate, rowchangereason
FROM dimservicewriter a
WHERE EXISTS (
  SELECT 1
  FROM dimservicewriter
  WHERE writernumber = a.writernumber
    AND storecode = a.storecode
    AND description = a.description
    AND rowchangereason IS NOT NULL)
ORDER BY storecode, writernumber, servicewriterkey  
-- 8/25
-- take a whack at cleaning this up
-- noticed another problem AS well, 
-- the swkfromdate IS the same for both of the type2 rows
go ahead AND fix them, one at a time

DECLARE @key1 integer;
DECLARE @key2 integer;
DECLARE @fromdate date;
DECLARE @fromdatekey integer;
@key1 = 624;
@key2 = 6775;
@fromdate = (SELECT servicewriterkeythrudate + 1 FROM dimservicewriter WHERE servicewriterkey = @key1);
@fromdatekey = (SELECT datekey FROM day WHERE thedate = @fromdate);
UPDATE dimServiceWriter
SET servicewriterkeyfromdate = @fromdate,
    servicewriterkeyfromdatekey = @fromdatekey,
    servicewriterkeythrudate = '12/31/9999',
    servicewriterkeythrudatekey = 7306
WHERE servicewriterkey = @key2;    
--/> 8/25/13 fixed -------------------------------------------------------------


--< anomalies  wiebusch 2 overlapping writer numbers --------------------------<
-- fixed 8/26
428 MIN 2/11/13 MAX 8/25/13
722 MIN 3/15/08 MAX 7/11/13  

SELECT ptswid, MIN(ptdate), MAX(ptdate), COUNT(*)
FROM stgArkonaSDPRHDR  
WHERE ptswid IN ('722','428')
  AND ptco# = 'ry1'
GROUP BY ptswid  

SELECT * FROM stgArkonaSDPRHDR WHERE ptco# = 'ry1' AND ptswid = '722'

ok, only a few ros of significance here, 
  16124718 7/11/13 - 8/12/13
  16117151 4/18/13 - 4/27/13
  16111254 2/17/13 - 2/25/13
  16111194 2/15/13 - 2/15/13
  
change the techkeys ON ALL to be for tech 428, the old ones will be stable,
but 16124718 will be IN the scrape for another month

DELETE tech 722 with techkey = 566
change factrepairorder swkey to 565 WHERE it IS currently 566

SELECT * FROM factRepairOrder WHERE servicewriterkey = 566

so, what i need to DO IS type2 tech 722 INTO inactive
AND change it to some unknown tech, the only ros are FROM 2008


SELECT * FROM factRepairOrder WHERE servicewriterkey = 566

UPDATE factRepairOrder
SET servicewriterkey = 565
WHERE servicewriterkey = 566;


DELETE FROM dimservicewriter WHERE servicewriterkey = 566;
--/> anomalies  wiebusch 2 overlapping writer numbers -------------------------/>


--< 1/8/14 changes made but NOT reflected IN arkona ---------------------------<

1. garret
/*
  ahh fuck, garrett IS now a pdq writer
  garrett became a pdq writer 12/11/13 (arkona edwEmployeeDim dep/distcode change )   
  but his default service type was NOT changed IN service:application envirionment:service writers
  so ETL did NOT pick it up
so i am thinking just DO a retroactive type 2 change to dimServiceWriters for garrett,
but factRepairOrder will have the wrong writerkey for ros written after 12/11 
fuckit
just DO the type2 change to garrett for default service type AND census
*/

ALSO CHANGED THE DEF SERV TYPE IN ARKONA

-- UPDATE old row
DECLARE @date date;
@date = '12/11/2013';
--SELECT @date -1, timestampadd(sql_tsi_minute, 1, CAST(@date -1 AS sql_timestamp)) FROM system.iota;
UPDATE dimServiceWriter
  SET CurrentRow = false,
      RowChangeDate = @date - 1,
      RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = @date - 1),
      RowThruTS = timestampadd(sql_tsi_minute, 1, CAST(@date -1 AS sql_timestamp)), 
      RowChangeReason = 'change DefaultServiceType, CensusDept',
      serviceWriterKeyThruDate = @date - 1, 
      serviceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = @date - 1)
WHERE serviceWriterKey = 577;
-- new row
DECLARE @date date;
@date = '12/11/2013';
INSERT INTO dimServiceWriter (StoreCode,Employeenumber, name, description, 
  writernumber, active, DefaultserviceType, CensusDept, CurrentRow, RowFromTS,
  ServiceWriterKeyFromDate, ServiceWriterKeyFromDateKey, 
  ServiceWriterKeyThruDate, ServiceWriterKeyThruDateKey)
SELECT storecode, employeenumber, name, description, writernumber, true, 
  'QL','QL', true, timestampadd(sql_tsi_minute, 1, CAST(@date -1 AS sql_timestamp)),
  @date, (SELECT datekey FROM day WHERE thedate = @date),
  '12/31/9999', 7306   
FROM dimServiceWriter
WHERE servicewriterkey = 577;

2. matt flikka
changed FROM MR to QL on 1/6/14

-- UPDATE old row
DECLARE @date date;
@date = '01/06/2014';
--SELECT @date -1, timestampadd(sql_tsi_minute, 1, CAST(@date -1 AS sql_timestamp)) FROM system.iota;
UPDATE dimServiceWriter
  SET CurrentRow = false,
      RowChangeDate = @date - 1,
      RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = @date - 1),
      RowThruTS = timestampadd(sql_tsi_minute, 1, CAST(@date -1 AS sql_timestamp)), 
      RowChangeReason = 'change DefaultServiceType, CensusDept',
      serviceWriterKeyThruDate = @date - 1, 
      serviceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = @date - 1)
WHERE serviceWriterKey = 523;
-- new row
INSERT INTO dimServiceWriter (StoreCode,Employeenumber, name, description, 
  writernumber, active, DefaultserviceType, CensusDept, CurrentRow, RowFromTS,
  ServiceWriterKeyFromDate, ServiceWriterKeyFromDateKey, 
  ServiceWriterKeyThruDate, ServiceWriterKeyThruDateKey)
SELECT storecode, employeenumber, name, description, writernumber, true, 
  'QL','QL', true, timestampadd(sql_tsi_minute, 1, CAST(@date -1 AS sql_timestamp)),
  @date, (SELECT datekey FROM day WHERE thedate = @date),
  '12/31/9999', 7306   
FROM dimServiceWriter
WHERE servicewriterkey = 523;

--/> 1/8/14 changes made but NOT reflected IN arkona --------------------------/>

-- zachary sieracki
-- UPDATE old row
-- SELECT * FROM dimservicewriter WHERE name LIKE 'sier%';
DECLARE @date date;
DECLARE @swkey integer;
@date = '04/06/2015';
@swkey = 772;
-- SELECT @date -1, timestampadd(sql_tsi_minute, 1, CAST(@date -1 AS sql_timestamp)) FROM system.iota;
UPDATE dimServiceWriter
  SET CurrentRow = false,
      RowChangeDate = @date - 1,
      RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = @date - 1),
      RowThruTS = timestampadd(sql_tsi_minute, 1, CAST(@date -1 AS sql_timestamp)), 
      RowChangeReason = 'change DefaultServiceType, CensusDept',
      serviceWriterKeyThruDate = @date - 1, 
      serviceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = @date - 1)
WHERE serviceWriterKey = @swkey;
-- new row
INSERT INTO dimServiceWriter (StoreCode,Employeenumber, name, description, 
  writernumber, active, DefaultserviceType, CensusDept, CurrentRow, RowFromTS,
  ServiceWriterKeyFromDate, ServiceWriterKeyFromDateKey, 
  ServiceWriterKeyThruDate, ServiceWriterKeyThruDateKey)
SELECT storecode, employeenumber, name, description, writernumber, true, 
  'MR','MR', true, timestampadd(sql_tsi_minute, 1, CAST(@date -1 AS sql_timestamp)),
  @date, (SELECT datekey FROM day WHERE thedate = @date),
  '12/31/9999', 7306   
FROM dimServiceWriter
WHERE servicewriterkey = @swkey;

--< Matthew Olson rehire/reuse employee, rehired 2/10/2014 AS a pdq writer ----<

-- no ros with the
SELECT a.servicewriterkey, MIN(theDate), MAX(theDate), COUNT(*)
from factRepairORder a
INNER JOIN day c on a.opendatekey = c.datekey
WHERE a.servicewriterkey IN (724,518)
GROUP BY a.serviceWriterKEy

-- ALL his current ros show servicewriterkey of 726 (RY1 UNKNOWN)
-- ALL the ros FROM his previous stint (last one on 5/25/13) show servicewriterkey of 517
SELECT a.ptro#, a.ptdate, a.ptcdat, b.servicewriterkey, b.servicewriterkey 
FROM stgArkonasdprhdr a 
INNER JOIN factRepairOrder b on a.ptro# = b.ro
WHERE ptswid = '431' ORDER BY ptdate DESC 

SELECT a.ptro#, a.ptdate, a.ptcdat, b.servicewriterkey, b.servicewriterkey 
FROM tmpsdprhdr a 
INNER JOIN factRepairOrder b on a.ptro# = b.ro
WHERE ptswid = '431' ORDER BY ptdate DESC 

SELECT a.ptdoc#, a.ptdate, b.servicewriterkey, b.servicewriterkey 
FROM stgArkonapdpphdr a 
INNER JOIN factRepairOrder b on a.ptdoc# = b.ro
WHERE ptswid = '431' ORDER BY ptdate DESC 

his writer number, 431, was never closed out IN arkona after he was termed
since dimServiceWriter PK IS storecode/writernumber/active/censusdept, that 
does NOT lead to a new row IN dimServiceWriter, already have an active = true AND
an active = false row

leaning toward deleting the type2 row with key 724, UPDATE the original row to 
currentrow, so, even though his employment was NOT continuous, the dimServiceWriter
row IS FROM original hire date thru today
UPDATE factRepairOrder with writernmumber 518 WHERE it IS currently 726 (unknown)
NOT great, but functional

-- also bad distcode, notified ned & ben
SELECT name, distcode, distribution
FROM edwEmployeeDim
WHERE currentrow = true
  AND pydeptcode = '03'
  AND active = 'active'
ORDER BY distcode, firstname
  
BEGIN TRANSACTION;
TRY  
DELETE 
FROM dimServiceWriter
WHERE serviceWriterKey = 724; 
 
UPDATE dimServiceWriter
  SET currentRow = true,
      rowChangeDate = CAST(NULL AS sql_date),
      rowChangeDateKey = CAST(NULL AS sql_integer),
      rowThruTS = CAST(NULL AS sql_timestamp),
      rowChangeReason = NULL,
      serviceWriterKeyThruDate = '12/31/9999',
      serviceWriterKeyThruDateKey = 7306
WHERE serviceWriterKey = 518;

UPDATE factRepairOrder
  SET serviceWriterKey = 518
WHERE ro IN (
SELECT a.ptdoc#
FROM stgArkonapdpphdr a 
INNER JOIN factRepairOrder b on a.ptdoc# = b.ro
WHERE ptswid = '431'
  AND serviceWriterKey = 726);
  
UPDATE factRepairOrder
  SET serviceWriterKey = 518
WHERE ro IN (
  SELECT a.ptro#
  FROM stgArkonasdprhdr a 
  INNER JOIN factRepairOrder b on a.ptro# = b.ro
  WHERE ptswid = '431'
    AND serviceWriterKey = 726);  
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;

--/> Matthew Olson rehire/reuse employee, rehired 2/10/2014 AS a pdq writer ---/>




--< 9/7/14 changes made but NOT reflected IN arkona ---------------------------<

daniel scholand

hired 6/9/14 AS a main shop writer
compli: change to pdq writer 8/19/14
payroll: 8/28/14 dept/dest changed to pdq writer
service writer default service type never changed

 
last 16 series RO: 8/18 16162475
first 19 series ro: 8/21 19178384
SELECT distinct b.thedate, a.ro, c.serviceType
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimServiceType c on a.serviceTypeKey = c.serviceTypeKey
WHERE a.serviceWriterKey = (
  SELECT serviceWriterKey
  FROM dimServiceWriter
  WHERE writernumber = '724')
ORDER BY thedate DESC   


1. CHANGE THE DEF SERV TYPE IN ARKONA
2. retroactive ADD row to dimServiceWriter AS pdq writer, 
     8/20 last day AS mr
     8/21 first day AS ql
-- UPDATE old row
DECLARE @thruDate date;
@thruDate = '08/20/2014';
--SELECT @date -1, timestampadd(sql_tsi_minute, 1, CAST(@date -1 AS sql_timestamp)) FROM system.iota;
UPDATE dimServiceWriter
  SET CurrentRow = false,
      RowChangeDate = curdate(),
      RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = curdate()),
      RowThruTS = now(), 
      RowChangeReason = 'change DefaultServiceType, CensusDept',
      serviceWriterKeyThruDate = @thruDate,
      serviceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = @thruDate)
WHERE serviceWriterKey = 753;
-- new row
DECLARE @fromDate date;
@fromDate = '08/21/2014';
INSERT INTO dimServiceWriter (StoreCode,Employeenumber, name, description, 
  writernumber, active, DefaultserviceType, CensusDept, CurrentRow, RowFromTS,
  ServiceWriterKeyFromDate, ServiceWriterKeyFromDateKey, 
  ServiceWriterKeyThruDate, ServiceWriterKeyThruDateKey)
SELECT storecode, employeenumber, name, description, writernumber, true, 
  'QL','QL', true, timestampadd(sql_tsi_second, 1, rowThruTS),
  @fromDate, (SELECT datekey FROM day WHERE thedate = @fromDate),
  '12/31/9999', 7306   
FROM dimServiceWriter
WHERE servicewriterkey = 753;

3. UPDATE factRepairOrder with the new writer key
UPDATE factRepairOrder
SET serviceWriterKey = 773
WHERE serviceWriterKey = 753
  AND LEFT(ro, 2) = '19'


--/> 9/7/14 changes made but NOT reflected IN arkona --------------------------/>

-- jama jama
--UPDATE old record  
-- ry1
UPDATE dimServiceWriter
SET currentrow = false,
    rowchangedate = curdate(),
    rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
    RowThruTS = now(),
    rowchangereason = 'active to inactive',
    ServiceWriterKeyThruDate = '01/29/2016',
    ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = '01/29/2016')    
WHERE writernumber = '314'
  AND storeCode = 'RY1';  
--INSERT new record 
INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
  writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
  rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
  servicewriterkeythrudate, servicewriterkeythrudatekey)
SELECT storecode, employeenumber, name, description, 
  writernumber, dtusername, false, defaultservicetype, censusdept, true,
  now(), '01/30/2016', (SELECT datekey FROM day WHERE thedate = '01/30/2016'),
  (SELECT thedate FROM day WHERE datetype = 'NA'),
  (SELECT datekey FROM day WHERE datetype = 'NA')
FROM dimServiceWriter
WHERE writernumber = '314'
  AND storeCode = 'RY1'; 
  
-- kalissa kruse: 128 IN ry1 & ry2
--UPDATE old record  
-- ry1
BEGIN TRANSACTION;
try
  UPDATE dimServiceWriter
  SET currentrow = false,
      rowchangedate = curdate(),
      rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
      RowThruTS = now(),
      rowchangereason = 'active to inactive',
      ServiceWriterKeyThruDate = '03/04/2016',
      ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = '03/04/2016')    
  WHERE writernumber = '128'
    AND storeCode = 'RY1';  
  --INSERT new record 
  INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
    writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
    rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
    servicewriterkeythrudate, servicewriterkeythrudatekey)
  SELECT storecode, employeenumber, name, description, 
    writernumber, dtusername, false, defaultservicetype, censusdept, true,
    now(), '03/05/2016', (SELECT datekey FROM day WHERE thedate = '03/05/2016'),
    (SELECT thedate FROM day WHERE datetype = 'NA'),
    (SELECT datekey FROM day WHERE datetype = 'NA')
  FROM dimServiceWriter
  WHERE writernumber = '128'
    AND storeCode = 'RY1';   
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;   
-- ry2
-- 3/25/17 Daniel Nice
BEGIN TRANSACTION;
try
  UPDATE dimServiceWriter
  SET currentrow = false,
      rowchangedate = curdate(),
      rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
      RowThruTS = now(),
      rowchangereason = 'active to inactive',
      ServiceWriterKeyThruDate = '03/04/2016',
      ServiceWriterKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = '03/04/2016')    
  WHERE writernumber = '603'
    AND storeCode = 'RY2';  
  --INSERT new record 
  INSERT INTO dimServiceWriter (storecode, employeenumber, name, description, 
    writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
    rowfromts, servicewriterkeyfromdate, servicewriterkeyfromdatekey, 
    servicewriterkeythrudate, servicewriterkeythrudatekey)
  SELECT storecode, employeenumber, name, description, 
    writernumber, dtusername, false, defaultservicetype, censusdept, true,
    now(), '03/05/2016', (SELECT datekey FROM day WHERE thedate = '03/05/2016'),
    (SELECT thedate FROM day WHERE datetype = 'NA'),
    (SELECT datekey FROM day WHERE datetype = 'NA')
  FROM dimServiceWriter
  WHERE writernumber = '603'
    AND storeCode = 'RY2';   
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

-- 6/3/16
just LIKE i did with alex chaput, josh munter has been termed
i have lost confidence IN dimServiceWriter structure
so ALL i am doing IS changing the currentrow: active -> false, reason: term
AS for dates, that IS ALL IN edwEmployeeDim 

same thing nick ohman

UPDATE dimSErviceWriter
  SET active = false,
      rowchangereason = 'term'
WHERE servicewriterkey = 786;

UPDATE dimSErviceWriter
  SET active = false,
      rowchangereason = 'term'
WHERE servicewriterkey IN (812,829);

-- 7/31/16 same thing timoth granados
UPDATE dimSErviceWriter
  SET active = false,
      rowchangereason = 'term'
WHERE servicewriterkey = 827;

-- 8/10/16 same thing james kerzmann
-- SELECT * FROM dimservicewriter WHERE description LIKE '%kerzmann%'
UPDATE dimSErviceWriter
  SET active = false,
      rowchangereason = 'term'
WHERE servicewriterkey = 831;

-- 8/24/16 same thing nicholas perez
UPDATE dimSErviceWriter
  SET active = false,
      rowchangereason = 'term'
WHERE servicewriterkey = 833;

-- 10/18/16
ned euliss FROM pdq mgr -> ms advisor
pdq writer number deactivated
new row AS mr writer

UPDATE dimSErviceWriter
  SET active = false,
      rowchangereason = 'changed position'
WHERE servicewriterkey = 598;

-- type2, generate an email for now  
-- active   
--@activeChange = (   
--  SELECT COUNT(*)
select *
  FROM dimServiceWriter a
  INNER JOIN xfmServiceWriterDim b ON a.storecode = b.swco#
    AND a.writernumber = b.swswid
    AND b.swactive = 'N'
  WHERE a.currentrow = true
    AND a.active = true
    AND a.censusDept <> 'XX'
    
10/26/16 deactivate kevin hanson	

UPDATE dimSErviceWriter
  SET active = false,
      rowchangereason = 'term'
WHERE servicewriterkey = 771;

11/4/16 cory stinar 

UPDATE dimSErviceWriter
  SET active = false,
      rowchangereason = 'changed position'
WHERE servicewriterkey = 741;

-- 5/2/17 somebody IN IT woke up
UPDATE dimServiceWriter
  SET active = false,
      rowchangereason = 'inactivated by IT'
WHERE servicewriterkey in (744,743,610,599,778,832,834,838,842);
  
  