
/*
-- new writer  
SELECT * 
FROM xfmServiceWriterDim a
WHERE NOT EXISTS (
  SELECT 1
  FROM dimServiceWriter
  WHERE storecode = a.swco#
    AND writernumber = a.swswid)
AND swactive = 'Y'    

SELECT * FROM dimservicewriter ORDER BY writernumber
    
SELECT * FROM tmpSDPRHDR WHERE ptswid = '459' ORDER BY ptdate

3/25/15 For some unknown reason, joel dangerfield has been given an RY1
        writer number with def serv type of MR
        whatever, ADD him, but make census XX
          
10/28/15: Jeff Bear writer # 574, make him census XX   
10/29/15: Shelly Trombley #457, body shop, census XX 

*/    
DECLARE @swswid cichar(3);
DECLARE @swco# cichar(3);
DECLARE @EmpNo cichar(7);
DECLARE @Name cichar(25);
DECLARE @dtUserName cichar(20);
DECLARE @FromDate date;
@swswid = '477';
@swco# = 'RY1';
@EmpNo = '1130326';
@Name = 'SOLIE, TANNER';
@dtUserName = '';
@FromDate = '04/10/2017';
/**/
INSERT INTO dimServiceWriter (storecode, employeenumber, name, description,
  writernumber, dtusername, active, defaultservicetype, censusdept, currentrow,
  servicewriterkeyfromdate,servicewriterkeyfromdatekey,
  servicewriterkeythrudate,servicewriterkeythrudatekey)
/**/  
SELECT a.swco#, @EmpNo, @Name, a.swname,
  a.swswid, @dtUserName, true, swmisc,
  -- don't need to test for pydept, this IS being done manually, AND i will NOT
  -- be adding shit LIKE parts guys OR bus off staff  
--  CASE 
--    WHEN swmisc = 'bs' THEN 'BS'
--    WHEN swmisc = 'mr' AND pydept = 'Service Team Leaders' THEN 'MR'
--    WHEN swmisc = 'ql' THEN 'QL'
--    WHEN swmisc = 're' THEN 'RE'
--    ELSE 'XX'
--  END, 
--  'XX', -- censusdept
  swmisc, -- censusdept
  true,
  @FromDate, (SELECT datekey FROM day WHERE thedate = @FromDate),
  (SELECT thedate FROM day WHERE datetype <> 'date'),  
  (SELECT datekey FROM day WHERE datetype <> 'date')
FROM xfmServiceWriterDim a  
WHERE swswid = @swswid
  AND swco# = @swco#;
   
