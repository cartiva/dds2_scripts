SELECT *
FROM tmpaisdpsacq a
full OUTER JOIN tmpaisarkacq b on a.stocknumber = b.stocknumber

-- complete list of stocknumbers
-- DROP TABLE #stock;
SELECT *
INTO #stock
FROM (
  SELECT stocknumbernoy
  FROM tmpAisDpsAcq
  UNION 
  SELECT stocknumbernoy
  FROM tmpAisArkAcq)  x

SELECT * FROM #stock  
SELECT COUNT(*) FROM #stock  -- 10994

-- acq date differences
SELECT a.stocknumber, timestampdiff(sql_tsi_day, b.dateacq, c.dateacq)
FROM #stock a
LEFT JOIN tmpAisDpsAcq b on a.stocknumbernoy = b.stocknumbernoy
LEFT JOIN tmpAisArkAcq c on a.stocknumbernoy = c.stocknumbernoy
WHERE b.dateacq <> c.dateacq
ORDER BY timestampdiff(sql_tsi_day, b.dateacq, c.dateacq)

-- acq date diff > 100 days
SELECT timestampdiff(sql_tsi_day, b.dateacq, c.dateacq),b.*,c.*
FROM #stock a
LEFT JOIN tmpAisDpsAcq b on a.stocknumbernoy= b.stocknumbernoy
LEFT JOIN tmpAisArkAcq c on a.stocknumbernoy = c.stocknumbernoy
WHERE ABS(timestampdiff(sql_tsi_day, b.dateacq, c.dateacq)) > 100

-- a lot of these are the intra market ws, arkona uses the orig acq date
SELECT b.acqCategory, b.acqCategoryClassification, COUNT(*)
FROM #stock a
LEFT JOIN tmpAisDpsAcq b on a.stocknumber= b.stocknumber
LEFT JOIN tmpAisArkAcq c on a.stocknumber = c.stocknumber
WHERE b.dateacq <> c.dateacq
--WHERE ABS(timestampdiff(sql_tsi_day, b.dateacq, c.dateacq)) > 30
GROUP BY b.acqCategory, b.acqCategoryClassification

-- vins are different for same stocknumber
SELECT *
-- SELECT COUNT(*) -- 112
FROM #stock a
LEFT JOIN tmpAisDpsAcq b on a.stocknumber= b.stocknumber
LEFT JOIN tmpAisArkAcq c on a.stocknumber = c.stocknumber
WHERE b.vin <> c.vin

--< NOT IN dps -----------------------------------------------------------------
/*
21241A: eval finished NOT booked
21349: new car, IN ark AS new car
*/
SELECT a.*, c.*
-- SELECT COUNT(*) -- 720
FROM #stock a
LEFT JOIN tmpAisDpsAcq b on a.stocknumber= b.stocknumber
LEFT JOIN tmpAisArkAcq c on a.stocknumber = c.stocknumber
WHERE b.stocknumber IS NULL 

select *
FROM (
  SELECT a.*, c.*
  -- SELECT COUNT(*) -- 720
  FROM #stock a
  LEFT JOIN tmpAisDpsAcq b on a.stocknumber= b.stocknumber
  LEFT JOIN tmpAisArkAcq c on a.stocknumber = c.stocknumber
  WHERE b.stocknumber IS NULL) d
LEFT JOIN tmpaisdpsacq e on d.vin = e.vin 
WHERE e.vin IS NOT NULL 


--/> NOT IN dps -----------------------------------------------------------------

-- NOT IN ark
/*
wtf 10687XXN (AND many LIKE it) come up IN arkona, so why NOT IN arkAcq?
this IS showing some promise
SELECT COUNT(*)
--SELECT imstk#, imdinv
FROM stgArkonaINPMAST a
WHERE imstk# <> ''
--  AND imstat = 'i'
  AND imtype = 'u'
  AND imdinv BETWEEN '01/01/2012' AND curdate()
--AND imstk# = '10687XXN'  
  AND NOT EXISTS (
    SELECT 1
    FROM tmpAisArkAcq
    WHERE stocknumber = a.imstk#);
    
*/
SELECT a.*, b.*, d.*
-- SELECT COUNT(*) -- 270
FROM #stock a
LEFT JOIN tmpAisDpsAcq b on a.stocknumber= b.stocknumber
LEFT JOIN tmpAisArkAcq c on a.stocknumber = c.stocknumber
LEFT JOIN (
  SELECT imstk#, imdinv
  FROM stgArkonaINPMAST a
  WHERE imstk# <> ''
  --  AND imstat = 'i'
    AND imtype = 'u'
    AND imdinv BETWEEN '01/01/2012' AND curdate()
  --AND imstk# = '10687XXN'  
    AND NOT EXISTS (
      SELECT 1
      FROM tmpAisArkAcq
      WHERE stocknumber = a.imstk#)) d on a.stocknumber = d.imstk#
WHERE c.stocknumber IS NULL 



-- stocknumber matches
SELECT *
-- SELECT COUNT(*) -- 14517
FROM tmpaisarkacq a
INNER JOIN tmpaisdpsacq b on a.stocknumbernoy = b.stocknumbernoy
-- IN dps NOT ark
SELECT a.*
-- SELECT COUNT(*) -- 206
FROM tmpAisDpsAcq a
LEFT JOIN tmpaisarkacq b on a.stocknumbernoy = b.stocknumbernoy
WHERE b.stocknumber IS NULL 
  AND a.storecode <> 'xxx'
-- IN ark NOT dps
SELECT *
-- SELECT COUNT(*) -- 697
FROM tmpaisarkacq a
LEFT JOIN tmpAisDpsAcq b on a.stocknumbernoy = b.stocknumbernoy
WHERE b.stocknumber IS NULL 


-- arkAcq WHERE stocknumber IS different that what IS IN INPMAST for the same vin
-- AND INPMAST stocknumber matches dpsAcq
-- corrections to stocknumbers show up IN inpmast, but NOT IN BOPTRAD OR BOPMAST, IN my opinion
SELECT a.stocknumber, a.dateacq, b.imstk#, b.imdinv, c.*
-- SELECT COUNT(*) -- 1000 !!
FROM tmpAisArkAcq a
INNER JOIN stgArkonaINPMAST b on a.vin = b.imvin
  AND abs(timestampdiff(sql_tsi_day, a.dateacq, b.imdinv)) < 10
  AND a.stocknumber <> b.imstk#
LEFT JOIN tmpaisdpsacq c on b.imstk# = c.stocknumber  
WHERE c.acqcategoryclassification = 'IntraMarketPurchase'


-- arkAcq with no acquisition date
SELECT COUNT(*),
  SUM(CASE WHEN dateacq = '12/31/9999' THEN 1 ELSE 0 END) AS noDate,
  SUM(CASE WHEN dateacq <> '12/31/9999' THEN 1 ELSE 0 END) AS hasDate
FROM tmpaisarkacq


SELECT *
FROM tmpaisarkacq a
LEFT JOIN tmpAisDpsAcq b on a.stocknumber = b.stocknumber
WHERE a.dateacq > curdate()


select *
FROM tmpaisdpsacq a
LEFT JOIN tmpaisarkacq b on a.stocknumber = b.stocknumber
WHERE b.stocknumber IS NULL 

-- IN ark, NOT dps
-- a lot of these are due to the 2011 limitation
select *
-- SELECT COUNT(*)
FROM tmpaisarkacq a
LEFT JOIN tmpaisdpsacq b on a.stocknumber = b.stocknumber
WHERE b.stocknumber IS NULL 


-- IN ark, NOT dps
-- a lot of these are due to the 2011 limitation
select *
-- SELECT COUNT(*) -- 734
FROM tmpaisarkacq a
LEFT JOIN tmpaisdpsacq b on a.stocknumber = b.stocknumber
WHERE b.stocknumber IS NULL 
-- so
SELECT *
-- SELECT COUNT(*) -- 176
FROM (
  select a.*
  FROM tmpaisarkacq a
  LEFT JOIN tmpaisdpsacq b on a.stocknumber = b.stocknumber
  WHERE b.stocknumber IS NULL) c
LEFT JOIN (
  select 
    CASE c.name 
      WHEN 'rydells' THEN 'RY1'
      WHEN 'honda cartiva' THEN 'RY2'
      ELSE 'XXX'
    END,
    left(trim(a.stocknumber), 10), left(trim(b.vin), 17), cast(a.fromts as sql_date)AS dateAcq, 
    CASE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
      WHEN 'Trade' THEN 'Trade'
      ELSE 'Purchase'
    END AS acqCategory,
    CASE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
      WHEN 'Trade' THEN ''
      ELSE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
    END AS acqCategoryClassification,
  --  left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20) AS acqCategory,
    left(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9',''), 'Y',''), 9) AS suffix,
    trim(replace(a.stocknumber, 'y', '')) AS stockNumberNoY
  FROM dps.VehicleInventoryItems a
  INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID 
  INNER JOIN dps.organizations c on a.owningLocationID = c.partyid
  LEFT JOIN dps.vehicleAcquisitions d on a.VehicleInventoryItemID = d.VehicleInventoryItemID  
  LEFT JOIN dps.organizations e on d.acquiredFromPartyID = e.partyid
  WHERE d.typ IS NOT NULL
    AND a.stocknumber IS NOT NULL) d on c.stocknumbernoy = d.stocknumbernoy
WHERE d.expr_1 IS NULL    




-- tmpAisArkAcq with no dateAcquired
SELECT *
-- SELECT COUNT(*) -- 693
FROM tmpAisArkAcq a
WHERE a.dateAcq > curdate()
-- ALL are ark-sale
SELECT datasource, COUNT(*)
FROM tmpAisArkAcq a
WHERE a.dateAcq > curdate()
GROUP BY datasource

SELECT *
FROM tmpAisArkAcq a
LEFT JOIN tmpAisDpsAcq b on a.stocknumberNOY = b.stocknumberNOY
WHERE a.dateAcq > curdate()


-- rows IN factVehicleSale with no corresponding acquisition  
-- every one has a blank stocknumber IN factVehicleSale
SELECT c.thedate, a.*, e.vin 
-- SELECT COUNT(*) -- 19
FROM factVehicleSale a
LEFT JOIN #stock b on replace(a.stocknumber, 'Y','') = b.stocknumbernoy
LEFT JOIN day c on a.approvedDateKey = c.datekey
LEFT JOIN dimCarDealInfo d on a.cardealinfokey = d.cardealinfokey
LEFT JOIN dimvehicle e on a.vehiclekey = e.vehiclekey
WHERE b.stocknumbernoy IS NULL 
  AND year(thedate) > 2011
  AND d.vehicletypecode = 'u'  
  
  
  
  
SELECT * FROM #stock WHERE stocknumbernoy LIKE '23367%'

SELECT *
FROM stgArkonaBOPMAST
WHERE bmkey IN (
SELECT bmkey
-- SELECT COUNT(*) -- 19
FROM factVehicleSale a
LEFT JOIN #stock b on replace(a.stocknumber, 'Y','') = b.stocknumbernoy
LEFT JOIN day c on a.approvedDateKey = c.datekey
LEFT JOIN dimCarDealInfo d on a.cardealinfokey = d.cardealinfokey
LEFT JOIN dimvehicle e on a.vehiclekey = e.vehiclekey
WHERE b.stocknumbernoy IS NULL 
  AND year(thedate) > 2011
  AND d.vehicletypecode = 'u')  
  
SELECT a.stocknumber, c.*
-- SELECT COUNT(*)
FROM dps.VehicleInventoryItems a
LEFT JOIN factVehicleSale b on a.stocknumber = b.stocknumber
LEFT JOIN dps.vehicleAcquisitions c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
WHERE cast(coalesce(a.thruts, CAST('12/31/9999 00:00:00' AS sql_timestamp)) AS sql_date) < curdate()
  AND b.stocknumber IS NULL 

-- intra market ws  
SELECT 100*year(a.soldts) + month(a.soldts), b.fullname, COUNT(*)
FROM dps.vehicleSales a
LEFT JOIN dps.organizations b on a.soldto = b.partyid collate ads_default_ci
WHERE a.typ = 'VehicleSale_Wholesale'
  AND a.status = 'VehicleSale_Sold'
  AND a.soldto IS NOT NULL 
GROUP BY 100*year(a.soldts) + month(a.soldts), b.fullname  
  
back on, status = 'VehicleSale_Cancelled'  