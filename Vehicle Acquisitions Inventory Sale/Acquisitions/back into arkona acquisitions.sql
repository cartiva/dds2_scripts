SELECT a.imstat, a.imstk#, a.imvin, a.imdinv, a.imddlv,
  b.bmdtaprv, b.bmdtcap, b.bmstat, b.bmsnam
FROM stgArkonaINPMAST a
LEFT JOIN stgArkonaBOPMAST b on a.imstk# = b.bmstk#
WHERE a.imstk# <> ''


SELECT a.imstat, a.imstk#, a.imvin, a.imdinv, a.imddlv,
  b.bmdtaprv, b.bmdtcap, b.bmstat, b.bmsnam, b.bmstk#, b.bmvin
FROM stgArkonaBOPMAST b
LEFT JOIN stgArkonaINPMAST a on a.imstk# = b.bmstk#
WHERE a.imstk# IS NULL 


SELECT b.bmdtaprv, b.bmdtcap, b.bmstat, b.bmsnam, b.bmstk#, b.bmvin
FROM stgArkonaBOPMAST b
WHERE bmdtcap = '12/31/9999'

SELECT b.bmdtaprv, b.bmdtcap, b.bmstat, b.bmsnam, b.bmstk#, b.bmvin
FROM stgArkonaBOPMAST b
WHERE bmdtcap <> bmdtaprv

SELECT b.bmdtaprv, b.bmdtcap, b.bmstat, b.bmsnam, b.bmstk#, b.bmvin,
  bmdtcap - bmdtaprv
FROM stgArkonaBOPMAST b
WHERE bmdtcap <> bmdtaprv

SELECT b.bmdtaprv, b.bmdtcap, b.bmstat, b.bmsnam, b.bmstk#, b.bmvin
FROM stgArkonaBOPMAST b
WHERE b.bmvin IN (
  SELECT bmvin
  FROM stgArkonaBOPMAST
  WHERE bmvin <> ''
  GROUP BY bmvin
  HAVING COUNT(*) > 1)
ORDER BY bmvin  

SELECT bmvin, COUNT(*)
FROM stgArkonaBOPMAST
WHERE bmvin <> ''
  AND bmstk# <> ''
GROUP BY bmvin
ORDER BY COUNT(*) DESC 


SELECT b.bmdtaprv, b.bmdtcap, b.bmstat, b.bmsnam, b.bmstk#, b.bmvin
FROM stgArkonaBOPMAST b
WHERE b.bmvin = '3GTP2VE72DG108908'
ORDER BY bmdtaprv

-- the number of sales of a particular vehicle
SELECT sales, COUNT(*)
FROM (
  SELECT bmvin, COUNT(*) AS sales
  FROM stgArkonaBOPMAST
  WHERE bmvin <> ''
    AND bmstk# <> ''
  GROUP BY bmvin) x
GROUP BY sales

-- the number of sales of a particular vehicle
-- eliminate trade ins
SELECT sales, COUNT(*)
FROM (
  SELECT bmvin, COUNT(*) AS sales
  FROM stgArkonaBOPMAST a
  WHERE bmvin <> ''
    AND bmstk# <> ''
    AND NOT EXISTS (
      SELECT 1
      FROM extArkonaBOPTRAD
      WHERE stocknumber = a.bmstk#)
  GROUP BY bmvin) x
GROUP BY sales

so i need to be able to WORK back FROM a sale AND determine the acquisition 
attributes of that instance of that vehicle
ALL of the above IS for ALL vehicles, narrow it down to used only
inpmast.imtype = 'U'
bopmast.bmvtyp = 'U'

-- current inventory
SELECT a.imstat, a.imstk#, a.imvin, a.imdinv, a.imddlv
FROM stgArkonaINPMAST a
WHERE a.imstk# <> ''
  AND imstat = 'I'
  AND imtype = 'U'
  
-- current inventory anomalies
SELECT a.imstat, a.imstk#, a.imvin, a.imdinv, a.imddlv
FROM stgArkonaINPMAST a
WHERE a.imstk# <> ''
  AND imstat = 'I'
  AND imtype = 'U'
  AND imddlv <= curdate()
/*
mistaken assumption #1
  IF the vehicle IS IN current inventory, imddlv will be 12/31/9999
     H7482A: imdinv: 1/10/15
             imddlv: 12/11/14, vehicle was sold AS H7344P on 12/11/14, but 
               was traded IN 1/10/15 AS H7482A
*/


-- 24706K shows AS current inventory AND sold  
SELECT a.imstat, a.imstk#, a.imvin, a.imdinv, a.imddlv,
  b.bmdtaprv, b.bmdtcap, b.bmstat, b.bmsnam, b.bmstk#, b.bmvin
FROM stgArkonaINPMAST a
LEFT JOIN stgArkonaBOPMAST b on a.imstk# = b.bmstk#
  AND b.bmstat <> ''
WHERE a.imstk# <> ''
  AND imstat = 'I'
  AND imtype = 'U'
  AND b.bmstk# IS NOT NULL 

  
SELECT a.imstat, a.imstk#, a.imvin, a.imdinv, a.imddlv
INTO #inp
FROM stgArkonaINPMAST a
WHERE a.imstk# <> ''
  AND imstat = 'I'
  AND imtype = 'U'
  
SELECT bmstat, bmstk#, bmvin, CAST(NULL AS sql_date) AS acqDate, bmdtaprv
INTO #bop
FROM stgArkonaBOPMAST
WHERE bmstk# <> ''  
  AND bmvtyp = 'U'
  AND bmstat <> ''
 
SELECT imstk#
FROM ( 
SELECT a.*, 'inp' FROM #inp a
UNION
SELECT b.*, 'bop' FROM #bop b) c
GROUP BY imstk# HAVING COUNT(*) > 1

SELECT *
FROM (
  SELECT a.*, 'inp' FROM #inp a
  UNION
  SELECT b.*, 'bop' FROM #bop b) c
WHERE imstk# IN (
  SELECT imstk#
  FROM ( 
  SELECT a.*, 'inp' FROM #inp a
  UNION
  SELECT b.*, 'bop' FROM #bop b) c
  GROUP BY imstk# HAVING COUNT(*) > 1)  
ORDER BY imstk#  
  
  
  
SELECT a.*, 'inp' FROM #inp a
UNION
SELECT b.*, 'bop' FROM #bop b  

!! 1. !!
  acq date for inventory derived FROM sales
  
  does it matter IN #bop whether (bmwhsl) sale was retail OR ws ?
  exclude crookston sales ?
  
DROP TABLE #bop;  
SELECT bmstat, bmstk#, bmvin, CAST(NULL AS sql_date) AS acqDate, bmdtaprv
INTO #bop
FROM stgArkonaBOPMAST
WHERE bmstk# <> ''  
  AND bmvtyp = 'U'
  AND bmstat <> ''  
  AND bmstk# NOT LIKE '3%'
  AND bmstk# NOT LIKE 'c%'
  AND bmdtaprv > '12/31/2011'

-- bop# with inpmast JOIN BY stock number with del date diffs  
SELECT a.*, b.imvin, b.imdinv, b.imddlv, a.bmdtaprv - b.imddlv
FROM #bop a
LEFT JOIN stgArkonaINPMAST b on a.bmstk# = b.imstk#
ORDER BY a.bmdtaprv - b.imddlv DESC

-- bop# with inpmast WHERE vins don't match
SELECT a.*, b.imvin, b.imdinv, b.imddlv, a.bmdtaprv - b.imddlv
FROM #bop a
LEFT JOIN stgArkonaINPMAST b on a.bmstk# = b.imstk#
WHERE a.bmvin <> b.imvin

-- dup stock numbers IN #bop
SELECT *
FROM #bop a
WHERE bmstk# IN (
  SELECT bmstk#
  FROM #bop
  GROUP BY bmstk# 
  HAVING COUNT(*) > 1)
ORDER BY bmstk#  

-- throw IN dps
SELECT a.*, b.imvin, b.imdinv, b.imddlv, d.typ, CAST(c.fromts AS sql_date) AS dpsAcqDate
FROM #bop a
LEFT JOIN stgArkonaINPMAST b on a.bmstk# = b.imstk#
LEFT JOIN dps.VehicleInventoryItems c on a.bmstk# = c.stocknumber
LEFT JOIN dps.vehicleAcquisitions d on c.VehicleInventoryItemID = d.VehicleInventoryItemID 

looking pretty good, what this does NOT show are the intra market ws sales
eg, h7713G was IN ry1 inventory prior to being IN ry2 inventory, WHERE IS that info
-- 1/31/15
it may be IN accounting but it sure AS fuck IS nowhere ELSE IN arkona
here IS a juicy one: 3N1AB6AP5CL699908
  09/20 traded INTO RY1: 23233PB
  11/29 ws to RY2: H7560G
  11/29 retailed
  12/05 unwound
  01/08 ws to RY1 24566X
  01/08 retailed
  
SELECT b.stocknumber, cast(b.fromts as sql_date) as fromdate, 
  CAST(a.soldts AS sql_date) AS soldDate, a.typ, c.typ, d.vin, a.status  
FROM dps.vehiclesales a
LEFT JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN dps.vehicleAcquisitions c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN dps.VehicleItems d on b.VehicleItemID = d.VehicleItemID 
WHERE d.vin = '3N1AB6AP5CL699908'  

bopmast shows only the most recent retail deal  
SELECT * FROM stgArkonaBOPMAST WHERE bmvin = '3N1AB6AP5CL699908'  

SELECT * FROM stgarkonaINPMAST WHERE imvin = '3N1AB6AP5CL699908'

select * FROM extarkonaboptrad WHERE vin = '3N1AB6AP5CL699908'

select * FROM stgarkonabopvref WHERE bvvin = '3N1AB6AP5CL699908'