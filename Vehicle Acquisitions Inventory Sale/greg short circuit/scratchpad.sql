SELECT a.storeCode, a.stocknumber, b.thedate, b.yearMonth, c.saleType
FROM factVehicleSale a
INNER JOIN day b on a.originationDateKey = b.datekey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
WHERE storecode = 'ry2'
  AND vehicleTypeCode = 'U'
  AND b.yearmonth BETWEEN 201401 AND 201407

-- retail uc sales BY month
-- ry2 agrees with statement, ry1 IS of BY a couple
SELECT storecode, yearmonth, COUNT(*)
FROM ( -- ALL uc sales 2014
  SELECT a.storeCode, a.stocknumber, b.thedate, b.yearMonth, c.saleType
  FROM factVehicleSale a
  INNER JOIN day b on a.originationDateKey = b.datekey
  INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  WHERE vehicleTypeCode = 'U'
    AND b.yearmonth BETWEEN 201401 AND 201407) d 
WHERE d.saleType = 'retail'    
GROUP BY storecode, yearmonth, saletype    

    
SELECT e.gtctl#, e.gtacct, e.gttamt, e.gtjrnl, f.dl4, f.dl6, f.dl7, 
  f.al2, f.al3, f.gmcoa, f.page, f.line 
FROM stgArkonaGLPTRNS e
INNER JOIN (
--  SELECT al2, al3, gmcoa, glAccount, page, line
  select *
  FROM zcb
  WHERE name = 'gmfs gross'
    AND storecode = 'ry2'
    AND dl3 = 'variable'
    AND dl5 = 'used') f on e.gtacct = f.glaccount
WHERE e.gtdate BETWEEN '07/01/2014' AND '07/31/2014'


SELECT dl4, dl6, dl7, al2, al3, gtacct, SUM(gttamt)
--SELECT SUM(gttamt)
--SELECT gtacct, SUM(gttamt)
FROM ( -- ALL uc sales 2014
  SELECT a.storeCode, a.stocknumber, b.thedate, b.yearMonth, c.saleType
  FROM factVehicleSale a
  INNER JOIN day b on a.originationDateKey = b.datekey
  INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  WHERE vehicleTypeCode = 'U'
    AND b.yearmonth BETWEEN 201401 AND 201407) d 
LEFT JOIN (  
  SELECT e.gtctl#, e.gtacct, e.gttamt, e.gtjrnl, f.dl4, f.dl6, f.dl7, 
    f.al2, f.al3, f.gmcoa, f.page, f.line 
  FROM stgArkonaGLPTRNS e
  INNER JOIN (
  --  SELECT al2, al3, gmcoa, glAccount, page, line
    select *
    FROM zcb
    WHERE name = 'gmfs gross'
      AND storecode = 'ry2'
      AND dl3 = 'variable'
      AND dl5 = 'used') f on e.gtacct = f.glaccount
  WHERE e.gtdate BETWEEN '07/01/2014' AND '07/31/2014') g on d.stocknumber = g.gtctl#  
WHERE d.storeCode = 'ry2' 
  AND d.saleType = 'retail'
  AND d.yearmonth = 201407  
  AND g.dl4 = 'F&I'
--GROUP BY gtacct
GROUP BY dl4, dl6, dl7, al2, al3, gtacct  

SELECT * FROM zcb WHERE name = 'gmfs gross' 
AND glaccount IN ('285100', '285101')
ORDER BY glaccount

285100/101 NOT tied to vehicle, so
SELECT dl4, dl6, dl7, al2, al3, SUM(gttamt)
--SELECT SUM(gttamt)
FROM (
  SELECT e.gtctl#, e.gtacct, e.gttamt, e.gtjrnl, f.dl4, f.dl6, f.dl7, 
    f.al2, f.al3, f.gmcoa, f.page, f.line 
  FROM stgArkonaGLPTRNS e
  INNER JOIN (
  --  SELECT al2, al3, gmcoa, glAccount, page, line
    select *
    FROM zcb
    WHERE name = 'gmfs gross'
      AND storecode = 'ry2'
      AND dl3 = 'variable'
      AND dl5 = 'used') f on e.gtacct = f.glaccount
  WHERE e.gtdate BETWEEN '07/01/2014' AND '07/31/2014') x
WHERE dl4 = 'F&I'  
GROUP BY dl4, dl6, dl7, al2, al3  

ok, the basic issue IS me freaking out about numbers NOT matching financial statement
but NOT realizing, fs page 7 includes chargebacks that are at a grain of dept, 
NOT vehicle


select COUNT(DISTINCT stocknumber)
FROM #2014
WHERE gmcoa = '455'
  AND storecode = 'ry1'
  AND yearmonth = 201407
 
