-- FROM: backfil 3-11-15.sql
-- DROP TABLE #wtf;
-- DROP TABLE #sales;
--< arkona --------------------------------------------------------------------<
-- trades
DELETE FROM tmpAisArkAcq;
INSERT INTO tmpAisArkAcq
SELECT a.storeCode, a.stockNumber, a.vin, b.bmdtaprv, 'Trade',
  CASE b.bmvtyp
    WHEN 'N' THEN 'New'
    WHEN 'U' THEN 'Used'
    ELSE 'Unknown'
  END,
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
  replace(a.stocknumber, 'y', '') AS stockNumberNoY, 'ark-trade'
FROM extArkonaBOPTRAD a
LEFT JOIN stgArkonaBOPMAST b on a.storeCode = b.bmco#
  AND a.bopmastKey = b.bmkey
WHERE b.bmstat <> '' -- eliminates pending deals
  AND right(TRIM(a.stocknumber), 6) <> right(TRIM(a.vin), 6); -- eliminates temp stocknumbers
  
-- trades with bad stocknumbers  
INSERT INTO tmpAisArkAcq
SELECT storecode, imstk, vin, bmdtor, 'Trade', 
  CASE bmvtyp
    WHEN 'N' THEN 'New'
    WHEN 'U' THEN 'Used'
    ELSE 'Unknown'
  END,
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(imstk, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
  replace(imstk, 'y', '') AS stockNumberNoY, 'ark-trade'
FROM (  
  SELECT a.storeCode, a.stocknumber, c.imstk#, b.bmvtyp,
    CASE
      WHEN right(TRIM(a.stocknumber), 6) <> right(TRIM(a.vin), 6) THEN a.stocknumber
      WHEN c.imstk# <> '' AND c.imstk# <> a.stocknumber AND c.imdinv = bmdtor THEN imstk#
      ELSE 'NO'
    END AS imstk, 
    a.stockNumber, a.vin, b.bmdtor, c.imdinv
  FROM extArkonaBOPTRAD a
  LEFT JOIN stgArkonaBOPMAST b on a.storeCode = b.bmco#
    AND a.bopmastKey = b.bmkey
  LEFT JOIN stgArkonaINPMAST c on a.vin = c.imvin  
-- *a*  
--  WHERE year(bmdtor) > 2010
--    AND right(TRIM(a.stocknumber), 6) = right(TRIM(a.vin), 6)
  WHERE right(TRIM(a.stocknumber), 6) = right(TRIM(a.vin), 6)
    AND
      CASE
        WHEN right(TRIM(a.stocknumber), 6) <> right(TRIM(a.vin), 6) THEN a.stocknumber
        WHEN c.imstk# <> '' AND c.imstk# <> a.stocknumber AND c.imdinv = bmdtor THEN imstk#
        ELSE 'NO'
      END <> 'NO') x
WHERE NOT EXISTS (
  SELECT 1
  FROM tmpAisArkAcq
  WHERE vin = x.vin);  
  
-- sales  
-- stk# NOT already IN tmpAisArkAcq, acqDate FROM INPMAST
INSERT INTO tmpAisArkAcq
SELECT bmco#, bmstk#, bmvin, acqDate,
  CASE 
    WHEN right(TRIM(bmstk#), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
    ELSE 'Purchase'
  END AS category,
  CASE 
    WHEN right(TRIM(bmstk#), 1) IN ('a','b','c','d','e','f') THEN
      CASE 
        WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
        ELSE 'Used'
      END
    ELSE ''
  END, 
  suffix, stockNumberNoY, 'ark-sale'
FROM (  
  SELECT distinct a.bmco#, a.bmstk#, a.bmvin, 
--    coalesce(c.imdinv, d.gtdate) AS acqDate, 'Purchase', '',
    coalesce(c.imdinv, CAST('12/31/9999' AS sql_date)) AS acqDate, 'Purchase', '',    
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.bmstk#, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
    replace(a.bmstk#, 'y', '') AS stockNumberNoY    
  FROM stgArkonaBOPMAST a
  LEFT JOIN tmpAisArkAcq b on a.bmstk# = b.stocknumber 
  LEFT JOIN stgArkonaINPMAST c on a.bmstk# = c.imstk#
  WHERE a.bmstk# <> ''
    AND a.bmstat <> '' -- finished deals
    AND a.bmvtyp = 'u' -- used car sale
    AND b.storecode IS NULL) x;  
    
-- current inventory --------------------------------------------
-- 6/9 i don't think this IS actually ever used
INSERT INTO tmpAisArkAcq
SELECT imco#, imstk#, imvin, imdinv,
  CASE 
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
    ELSE 'Purchase'
  END AS category,
  CASE 
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN
      CASE 
        WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
        ELSE 'Used'
      END
    ELSE ''
  END, 
  suffix, stockNumberNoY, 'ark-inv'
FROM (  
  SELECT imco#, imstk#, imvin, imdinv, 'Purchase', '',
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.imstk#, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix, 
    replace(a.imstk#, 'y', '') AS stockNumberNoY
  FROM stgArkonaINPMAST a
  WHERE imstk# <> ''
    AND imstat = 'i'
    AND imtype = 'u'
    AND NOT EXISTS (
      SELECT 1
      FROM tmpAisArkAcq
      WHERE stocknumber = a.imstk#)) x;  
--/> arkona -------------------------------------------------------------------/>     

--< dps -----------------------------------------------------------------------<
DELETE FROM tmpAisDpsAcq;
INSERT INTO tmpAisDpsAcq
select 
  CASE c.name 
    WHEN 'rydells' THEN 'RY1'
    WHEN 'honda cartiva' THEN 'RY2'
    ELSE 'XXX'
  END,
  left(trim(a.stocknumber), 10), left(trim(b.vin), 17), cast(a.fromts as sql_date)AS dateAcq, 
  CASE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
    WHEN 'Trade' THEN 'Trade'
    ELSE 'Purchase'
  END AS acqCategory,
  CASE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
    WHEN 'Trade' THEN ''
    ELSE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
  END AS acqCategoryClassification,
  left(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9',''), 'Y',''), 9) AS suffix,
  trim(replace(a.stocknumber, 'y', '')) AS stockNumberNoY, 'dps'
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID 
INNER JOIN dps.organizations c on a.owningLocationID = c.partyid
LEFT JOIN dps.vehicleAcquisitions d on a.VehicleInventoryItemID = d.VehicleInventoryItemID  
LEFT JOIN dps.organizations e on d.acquiredFromPartyID = e.partyid
WHERE d.typ IS NOT NULL  
  AND a.stocknumber IS NOT NULL;  

UPDATE tmpAisDpsAcq
  SET acqCategoryClassification = 
    CASE 
      WHEN stocknumberSuffix IN ('a','aa','aaa','aaaa','aaaaa') THEN 'New'
      ELSE 'Used'
    END    
WHERE acqCategory = 'trade';

-- 3/5/15 to comply with dds.dimVehicleAcquisitionInfo (come at it FROM dps 2-2-15.sql
UPDATE tmpAisDpsAcq
SET acqCategoryClassification = 
  CASE acqCategoryClassification
    WHEN 'DealerPurchase' THEN 'Dealer Purchase'
    WHEN 'InAndOut' THEN 'In And Out'
    WHEN 'IntraMarketPurchase' THEN 'Intra Market Wholesale'
    WHEN 'LeasePurcha' THEN 'Lease Purchase'
    WHEN 'ServiceLoan' THEN 'Service Loaner'
    ELSE acqCategoryClassification
  END;
--/> dps ----------------------------------------------------------------------/>  

-- III. A) INSERT IMWS INTO factVehicleSale      
INSERT INTO factVehicleSale (storecode,bmkey,stockNumber,carDealInfoKey,
    vehicleKey,originationDateKey,approvedDateKey,cappedDateKey,buyerKey,
    coBuyerKey,consultantKey,managerKey,vehicleCost,vehiclePrice,noY)
SELECT * 
FROM (
  SELECT 
    case d.fullname
      WHEN 'GF-Crookston' THEN 'RY3'
      WHEN 'GF-Honda Cartiva' THEN 'RY2'
      WHEN 'GF-Rydells' THEN 'RY1'
    END AS storeCode, 
    -1 AS bmkey, -- there IS no arkona car deal for IMWS
    left(c.stocknumber, 9) AS stocknumber, 
    (select carDealInfoKey from dimCarDealInfo where saleTypeCode = 'I') as carDealInfoKey,
    e.vehicleKey, 
    f.datekey AS originationDateKey, f.datekey AS approvedDateKey, f.datekey AS cappedDateKey,
    case b.fullname
      WHEN 'GF-Crookston' THEN 175
      WHEN 'GF-Honda Cartiva' THEN 12
      WHEN 'GF-Rydells' THEN 9019
    END AS buyerKey,
    1084332 AS coBuyerKey,
    246 AS consultantKey, 246 AS managerKey, 
    0 AS vehicleCost,
    a.soldAmount AS vehiclePrice,
    replace(left(c.stocknumber, 9),'Y','') AS noY
  FROM dps.vehicleSales a
  INNER JOIN dps.organizations b on a.soldto = b.partyid collate ads_Default_ci
    AND b.fullname IN ('GF-Honda Cartiva','GF-Rydells')
  INNER JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  INNER JOIN dps.VehicleItems cc on c.VehicleItemID = cc.VehicleItemID 
  INNER JOIN dps.organizations d on c.owninglocationid = d.partyid
  LEFT JOIN dimVehicle e on cc.vin = e.vin
  LEFT JOIN day f on CAST(a.soldts AS sql_date) = f.thedate
  WHERE b.fullname IS NOT NULL   
    AND a.status = 'VehicleSale_Sold') x -- eliminate canceled sales   
WHERE vehicleKey IS NOT NULL 
  AND NOT EXISTS (
    SELECT 1
    FROM factVehicleSale
    WHERE stocknumber = x.stocknumber);  

-- III. B)    
-- base rows for ALL sales
-- no sourceVehicle, acqInfoKey, acqDate info, yet
-- 6/6/15 executive decision: used cars only
DELETE FROM factVehicleAcquisition;
INSERT INTO factVehicleAcquisition (storecode,stocknumber,noY,vehicleKey)
SELECT a.storecode, a.stocknumber, a.noy, a.vehicleKey
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey
  AND b.vehicleTypeCode = 'U'
LEFT JOIN stgArkonaINPMAST c on a.stocknumber = c.imstk#
WHERE a.stocknumber <> '';     

-- III. C)
-- #wtf: ALL used car factVehicleAcquisition rows with acq info FROM tmpAisArkAcq & tmpAisDpsAcq
-- DROP TABLE #wtf;
SELECT a.*, b.datasource, b.dateAcq, b.acqCategory, b.acqCategoryClassification, 
   c.datasource, c.dateAcq, c.acqCategory, c.acqCategoryClassification
INTO #wtf   
FROM factVehicleAcquisition a
LEFT JOIN tmpAisArkAcq b on a.noy = b.stocknumbernoy
LEFT JOIN tmpAisDpsAcq c on a.noy = c.stocknumbernoy;

-- III. D) -- used car, acqCat:dps=ark, acqDate:dps-ark w/IN 30 days,
--              acqCatClass: IF trade: ark=dps 
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (    
  SELECT g.storecode, g.stocknumber, g.noy, 
    h.vehicleAcquisitionInfoKey, g.vehicleKey, g.datekey, 
    g.sourceVehicleKey
  FROM (  
    SELECT a.storecode, a.stocknumber, a.noY, 
      a.vehiclekey,
      a.acqCategory, 
      CASE acqCategory
        WHEN 'Trade' THEN acqCategoryClassification
        WHEN 'Purchase' THEN acqCategoryClassification_1
        ELSE 'XXX'
      END AS acqCategoryClassification,
      (select datekey from day where thedate = a.dateAcq) AS datekey, 
      CASE acqCategory
        WHEN 'Trade' THEN d.vehicleKey
        WHEN 'Purchase' THEN e.vehicleKey
        ELSE -666
      END AS sourceVehicleKey
    FROM #wtf a -- #wtf IS only used cars
    LEFT JOIN extArkonaBOPTRAD b on a.stocknumber = b.stocknumber
    LEFT JOIN factVehicleSale c on b.bopmastkey = c.bmkey AND b.storecode = c.storecode
    LEFT JOIN dimVehicle d on c.vehicleKey = d.vehicleKey -- decode source vehicle
    LEFT JOIN dimVehicle e on e.vin = 'NA'
    WHERE NOT(a.dateacq IS NULL AND a.dateacq_1 IS NULL) -- eliminate those rows with no acq info
      AND a.acqCategory = a.acqCategory_1 -- arkona = dps
      AND CASE WHEN acqCategory = 'Trade' THEN a.acqCategoryClassification = a.acqCategoryClassification_1 ELSE 1 = 1 END -- arkona = dps for trades only
      AND ABS(a.dateAcq - a.dateAcq_1) < 30) g -- arkona - dps < 30
  LEFT JOIN dimVehicleAcquisitionInfo h on g.acqCategory = h.acquisitionType
    AND g.acqCategoryClassification = h.acquisitionTypeCategory) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;

-- III. E)
-- ok, i will pick thru, ie find substantial groups that can be updated at once
-- dpsAcq =  IMWS
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (    
  SELECT a.stocknumber, e.vehicleAcquisitionInfoKey, c.datekey, d.vehicleKey AS sourceVehicleKey
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  LEFT JOIN #wtf b on a.stocknumber = b.stocknumber  -- #wtf IS only used cars
  LEFT JOIN day c on b.dateAcq_1 = c.thedate
  LEFT JOIN dimVehicle d on d.vin = 'NA'
  LEFT JOIN dimVehicleAcquisitionInfo e on e.acquisitionTypeCategory = 'Intra market wholesale'
  WHERE a.vehicleAcquisitionInfoKey IS NULL   
    AND acqCategoryClassification_1 = 'Intra market wholesale') x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;  

-- III. F) 
--   acqCategory: ark=dps, acqCategory = purchase  
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (  
  SELECT b.stocknumber, c.vehicleAcquisitionInfoKey, d.datekey, 
    e.vehicleKey AS sourceVehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  INNER JOIN #wtf b on a.stocknumber = b.stocknumber
  LEFT JOIN dimVehicleAcquisitionInfo c on b.acqCategory_1 = c.acquisitionType
    AND b.acqCategoryClassification_1 = c.acquisitionTypeCategory
  LEFT JOIN day d on b.dateAcq_1 = d.thedate  
  LEFT JOIN dimVehicle e on vin = 'NA'
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
--    AND aaa.theyear > 2011   
    AND b.acqCategory = 'Purchase'
    AND b.acqCategory = b.acqCategory_1) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;   
  
-- III. G) 
--   acqCategory: ark=dps, acqCategory = trade, stk EXISTS IN BOPTRAD
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.VehicleKey
FROM (  
  SELECT b.stocknumber, f.vehicleAcquisitionInfoKey, g.datekey, e.vehicleKey
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  INNER JOIN #wtf b on a.stocknumber = b.stocknumber
  LEFT JOIN extArkonaBOPTRAD c on b.stocknumber = c.stocknumber
  LEFT JOIN factVehicleSale d on c.storecode = d.storecode AND c.bopmastkey = d.bmkey
  LEFT JOIN dimVehicle e on d.vehicleKey = e.vehicleKey
  LEFT JOIN dimVehicleAcquisitionInfo f on b.acqCategory = f.acquisitionType
    AND b.acqCategoryClassification = f.acquisitionTypeCategory
  LEFT JOIN day g on b.dateAcq = g.thedate  
  WHERE a.vehicleAcquisitionInfoKey IS NULL    
    AND b.acqCategory = 'Trade'
    AND b.acqCategory = b.acqCategory_1
    AND e.vehicleKey IS NOT NULL 
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;     
  
--- III. H) 
--   acqCategory: ark=purchase dps=trade
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.VehicleKey
FROM (    
  SELECT d.stocknumber, f.vehicleAcquisitionInfoKey, e.datekey, g.vehiclekey 
  FROM (  
    SELECT c.*,
      CASE -- use dps dateAcq
        WHEN dateAcq_1 > saleDate THEN saleDate
        ELSE dateAcq_1
      END AS acqDate,
      CASE right(TRIM(stockNumber),1)
        WHEN 'R' THEN 'Service Loaner'
        WHEN 'X' THEN 'Auction'
        WHEN 'L' THEN 'Lease Purchase'
        WHEN 'P' THEN 'Street'
        ELSE 'Unknown'
      END AS acqTypeCat   
    FROM (
      SELECT b.*, aaa.thedate AS saledate
      FROM factVehicleAcquisition a
      INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
      INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
      INNER JOIN #wtf b on a.stocknumber = b.stocknumber
      WHERE a.vehicleAcquisitionInfoKey IS NULL) c 
    WHERE c.acqCategory = 'purchase'
    AND c.acqCategory_1 = 'trade'
    AND c.acqCategoryClassification = '' 
    AND c.acqCategoryClassification_1 = 'used') d
  LEFT JOIN day e on d.acqDate = e.thedate 
  LEFT JOIN dimVehicleAcquisitionInfo f on f.acquisitionType = 'Purchase'
    AND d.acqTypeCat = f.acquisitionTypeCategory collate ads_default_ci
  LEFT JOIN dimVehicle g on g.vin = 'NA'
 ) x  
 WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;     
   
--- III. I)
--   acqCategory/_1 = trade, acqCatClass/_1 = new, 
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (   
  SELECT stocknumber, e.vehicleAcquisitionInfoKey, d.datekey, c.sourceVehicleKey
  FROM (  
    SELECT b.stocknumber, dateAcq_1 AS acqDate,
      (SELECT vehicleKey 
        FROM factVehicleSale 
        WHERE stocknumber = replace(b.stocknumber,'a','')) AS sourceVehicleKey
    FROM factVehicleAcquisition a
    INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
    INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
    INNER JOIN #wtf b on a.stocknumber = b.stocknumber
    WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND acqCategory = 'trade'
    AND acqCategory_1 = 'trade'
    AND acqCategoryClassification = 'new' 
    AND acqCategoryClassification_1 = 'new') c
  LEFT JOIN day d on c.acqDate = d.thedate  
  LEFT JOIN dimVehicleAcquisitionInfo e on e.acquisitionType = 'Trade'
    AND e.acquisitionTypeCategory = 'New'
  WHERE sourceVehicleKey IS NOT NULL
) x    
 WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;      
  
-- III. J-1
-- new car trades, derive acqDate FROM new car sale WHEN the nc stocknumber
-- EXISTS IN factVehicleSale
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = 13, -- used-trade-new
      datekey = x.datekey, -- date of new car sale
      sourceVehicleKey =  x.vehiclekey -- new car sale on which this was traded
FROM (     
  SELECT a.stocknumber, c.datekey, b.vehiclekey 
  FROM (
    SELECT a.*, replace(replace(replace(replace(replace(replace(replace(replace(replace
          (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
            '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
          replace(a.stocknumber, 'a', '') AS ncStock
      FROM factVehicleAcquisition a
      WHERE vehicleAcquisitionInfoKey IS NULL) a
    LEFT JOIN factVehicleSale b on replace(a.stocknumber, 'a', '') = b.stocknumber
    LEFT JOIN day c on b.approveddatekey = c.datekey
    WHERE a.suffix IN ('a','aa')  
      AND b.stocknumber IS NOT NULL 
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;  

-- III. J-2
-- x & xx vehicles, use dps for values, 219 rows updated  
--   first time mixing IN the notion of old stocknumbers, 1988266XX -> 988266X
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey, -- used-purchase ...
      datekey = x.datekey, -- dps.VehicleInventoryItems.fromts
      sourceVehicleKey =  x.vehiclekey -- n/a these are ALL purchases
FROM (  
SELECT d.stocknumber, 
  CASE f.typ
    WHEN 'VehicleAcquisition_Auction' THEN 2
    WHEN 'VehicleAcquisition_DealerPurchase' THEN 3
    WHEN 'VehicleAcquisition_IntraMarketPurchase' THEN 5
    WHEN 'VehicleAcquisition_LeasePurcha' THEN 6
    WHEN 'VehicleAcquisition_Street' THEN 11
    WHEN 'VehicleAcquisition_Trade' THEN 13
  END AS vehicleAcquisitionInfoKey,
  (SELECT datekey FROM day WHERE thedate = CAST(e.fromts AS sql_date)) AS dateKey, 
  (SELECT vehicleKey FROM dimVehicle WHERE vin = 'NA') AS vehicleKey
FROM (
  SELECT a.*, 
    substring(a.stocknumber, 2,9) AS oldStock          
  FROM factVehicleAcquisition a
  WHERE a.vehicleAcquisitionInfoKey IS NULL 
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
          (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
            '4',''),'5',''),'6',''),'7',''),'8',''),'9','') IN ('x','xx')) d
LEFT JOIN dps.VehicleInventoryItems e on d.stocknumber = e.stocknumber
  OR d.oldstock = e.stocknumber   
LEFT JOIN dps.vehicleAcquisitions f on e.VehicleInventoryItemID = f.VehicleInventoryItemID  
WHERE e.stocknumber IS NOT NULL 
) x  
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;

-- III. J-3
-- x vehicles that were intra market purchases
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = 5, -- intra market purchase
      datekey = x.datekey, -- factVehicleSale sale date
      sourceVehicleKey =  x.vehiclekey -- n/a these are ALL purchases
FROM (  
  SELECT a.stocknumber, aaaa.datekey,
    (SELECT vehicleKey FROM dimVehicle WHERE vin = 'NA') AS vehicleKey    
  FROM factVehicleAcquisition a
  LEFT JOIN dimVehicle aa on a.vehicleKey = aa.vehicleKey
  LEFT JOIN factVehicleSale aaa on a.stocknumber = aaa.stocknumber
  LEFT JOIN day aaaa on aaa.approveddatekey = aaaa.datekey
  LEFT JOIN dps.VehicleItems b on aa.vin = b.vin
  LEFT JOIN dps.VehicleInventoryItems c on b.VehicleItemID = c.VehicleItemID 
  WHERE vehicleAcquisitionInfoKey IS NULL 
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
          (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
            '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'x'
    AND abs(aaaa.thedate - CAST(c.thruts AS sql_date)) < 10    
    AND LEFT(a.stocknumber,1) <> LEFT(c.stocknumber, 1)
) x        
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;

-- III. J-4          
-- b vehicles - trade IN on "a" used cars 103 rows updated    
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = 14, -- used trades
      datekey = x.datekey, -- factVehicleSale sale date for "A" vehicle
      sourceVehicleKey =  x.vehiclekey -- "A" vehicle vehicleKey
FROM (         
  SELECT a.stocknumber, c.datekey, b.vehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  LEFT JOIN factVehicleSale b  on replace(a.stocknumber, 'b','a') = b.stocknumber
  LEFT JOIN day c on b.approveddatekey = c.datekey
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'b'  
    AND b.storecode IS NOT NULL                
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;

-- III. J-5         
-- "A" vehicles - deleted 140 rows
-- there are no factVehicleSale records for these new car stocknumbers
-- so just whack them FROM factVehicleAcquisition
DELETE 
FROM factVehicleAcquisition
WHERE stocknumber IN (
  SELECT stocknumber
  -- SELECT count(*)
  FROM factVehicleAcquisition 
  WHERE vehicleAcquisitionInfoKey IS NULL   
      AND replace(replace(replace(replace(replace(replace(replace(replace(replace
              (replace(substring(stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
                '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'a');
-- III. J-6          
-- XXA vehicles - trade IN on "XX" used cars 
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = 14, -- used trades
      datekey = x.datekey, -- factVehicleSale sale date for "XX" vehicle
      sourceVehicleKey =  x.vehiclekey -- "XX" vehicle vehicleKey
FROM (
  SELECT a.stocknumber, c.datekey, b.vehicleKey
  FROM factVehicleAcquisition a
  LEFT JOIN factVehicleSale b  on replace(a.stocknumber, 'a','') = b.stocknumber
  LEFT JOIN day c on b.approveddatekey = c.datekey
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'xxa'  
    AND b.storecode IS NOT NULL 
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;     
           
-- III. J-7          
-- X vehicles - these are old AND unresolveable, whack 'em -- 89
DELETE 
FROM factVehicleAcquisition
WHERE stocknumber IN (
  SELECT a.stocknumber
  FROM factVehicleAcquisition a
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'x');
              
-- III. J-8
-- XA vehicles - trade IN on "X" used cars
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = 14, -- used trades
      datekey = x.datekey, -- factVehicleSale sale date for "X" vehicle
      sourceVehicleKey =  x.vehiclekey -- "X" vehicle vehicleKey
FROM (
  SELECT a.stocknumber, c.datekey, b.vehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  LEFT JOIN factVehicleSale b  on replace(a.stocknumber, 'a','') = b.stocknumber
  LEFT JOIN day c on b.approveddatekey = c.datekey
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'xa'  
    AND b.storecode IS NOT NULL
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;           

--- IV. 
-- current inventory, need to fashion factVehicleAcquisition rows
-- here is current inventory FROM arkona   

DROP TABLE tmpAisArkInventory;
CREATE TABLE tmpAisArkInventory (
  stocknumber cichar(9),
  vin cichar(17),
  theDate date,
  acquisitionType cichar(10),
  acquisitionTypeCategory cichar(24),
  suffix cichar(9),
  noY cichar(9),
  sourceStock cichar(9)) IN database;
  
DELETE FROM tmpAisArkInventory;
INSERT INTO  tmpAisArkInventory 
SELECT imstk#, imvin, imdinv,
  CASE 
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
    ELSE 'Purchase'
  END AS category,
  CASE 
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN
      CASE 
        WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
        ELSE 'Used'
      END
    ELSE ''
  END, 
  suffix, stockNumberNoY,
  CASE
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN
-- need to handle BB, CC DD, AC, AD etc
      CASE
        WHEN suffix IN ('A','AA','AAA','AAAA') THEN replace(imstk#, 'A', '')
        WHEN suffix IN ('AB','B') THEN replace(imstk#, 'B', 'A')
        WHEN suffix = 'C' THEN replace(imstk#, 'C', 'B')
        WHEN suffix = 'D' THEN replace(imstk#, 'D', 'C')
        WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'A' THEN replace(imstk#, 'A', '')
        WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'B' THEN replace(imstk#, 'B', 'A')
        WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'C' THEN replace(imstk#, 'C', 'B')
        WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'D' THEN replace(imstk#, 'D', 'C')
        WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'A' THEN replace(imstk#, 'A', '')
        WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'B' THEN replace(imstk#, 'B', 'A')
        WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(imstk#, 'C', 'B')
        WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(imstk#, 'D', 'C')
      END 
    ELSE 'NA'
  END AS sourceStock
-- SELECT COUNT(*)
FROM (  
  SELECT imco#, imstk#, imvin, imdinv, 'Purchase', '',
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.imstk#, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix, 
    replace(a.imstk#, 'y', '') AS stockNumberNoY
  FROM stgArkonaINPMAST a
  WHERE imstk# <> ''
    AND imstat = 'i'
    AND imtype = 'u') x;     
    
-- IV.A
-- ALL trades
INSERT INTO factVehicleAcquisition (storecode,stocknumber,noY,
  vehicleAcquisitionInfoKey, vehicleKey, dateKey, sourceVehicleKey)
SELECT b.storecode, a.stocknumber, a.noy, c.vehicleAcquisitionInfoKey, 
  d.vehiclekey, b.approvedDateKey, b.vehicleKey
FROM tmpAisArkInventory a
INNER JOIN factVehicleSale b on a.sourceStock = b.stocknumber
LEFT JOIN dimVehicleAcquisitionInfo c on a.acquisitionType = c.acquisitionType
  AND a.acquisitionTypeCategory = c.acquisitionTypeCategory
LEFT JOIN dimVehicle d on a.vin = d.vin  
WHERE a.acquisitionType = 'Trade';    

-- IV.B
-- purchases
INSERT INTO factVehicleAcquisition (storecode,stocknumber,noY,
  vehicleAcquisitionInfoKey, vehicleKey, dateKey, sourceVehicleKey)
-- SELECT *
SELECT CASE LEFT(a.stocknumber,1) WHEN 'H' THEN 'RY2' ELSE 'RY1' END AS storecode,
  a.stocknumber, a.noy, d.vehicleAcquisitionInfoKey, aa.vehicleKey, 
  (SELECT datekey FROM day WHERE thedate = a.thedate),
  (SELECT vehiclekey FROM dimVehicle WHERE vin = 'NA')
from tmpAisArkInventory a
INNER JOIN dimVehicle aa on a.vin = aa.vin
LEFT JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
LEFT JOIN dps.vehicleAcquisitions c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN dimVehicleAcquisitionInfo d on a.acquisitionType = d.acquisitionType
  AND d.acquisitionTypeCategory = 
    CASE
      WHEN right(trim(suffix), 1) IN ('a','b','c','d','e','f','s') THEN 'Unknown'
      WHEN suffix = 'G' THEN 'Intra Market Wholesale'
      WHEN suffix = 'K' THEN 'Unknown'
      WHEN suffix = 'L' THEN 
        CASE
          WHEN c.typ IN ('VehicleAcquisition_LeasePurcha', 'VehicleAcquisition_Trade') THEN 'Lease Purchase'
          WHEN c.typ = 'VehicleAcquisition_InAndOut' THEN 'In And Out'
          ELSE 'Unknown'
        END
      WHEN right(trim(suffix), 1) = 'N' THEN 'National' 
      WHEN suffix = 'P' THEN 'Street'
      WHEN right(trim(suffix), 1) = 'R' THEN 'Rental'
      WHEN right(TRIM(suffix), 1) = 'T' THEN
        CASE
          WHEN right(TRIM(suffix), 2) = 'DT' THEN 'Driver Training'
          ELSE 'Unknown'
        END 
      WHEN right(TRIM(suffix), 1) = 'X' THEN
        CASE
          WHEN length(TRIM(suffix)) = 1 THEN
            CASE
              WHEN c.typ = 'VehicleAcquisition_Auction' THEN 'Auction'
              WHEN c.typ = 'VehicleAcquisition_IntraMarketPurchase' THEN 'Intra Market Wholesale'
              ELSE 'Unknown'
            END 
          WHEN suffix = 'XX' THEN 'Auction'
          ELSE 'Unknown'
        END 
      
      WHEN right(TRIM(suffix), 1) = 'Z' THEN 'Unknown'
    END
WHERE a.acquisitionType = 'Purchase';

-- V.
-- factVehicleInventory
-- some WORK done IN come at it FROM dps 2-2-15.sql
/* 6/9/15
IS a vehicle IN inventory on 6/5/15 IF it IS sold on 6/5/15, i am going to say
  yes, makes no sense to sell a vehicle that IS NOT IN inventory, whatever
  talk to greg
this might have some relevance
IN postgres with interval types, open-closed intervals
*/
DROP TABLE factVehicleInventory;
CREATE TABLE factVehicleInventory (
  storeCode cichar(3),
  stockNumber cichar(9),  
  noY cichar(9),
  vehicleKey integer,
  fromDateKey integer,
  thruDateKey integer) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'storeCode', 'storeCode', 
  '', 2, 512, NULL );  
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'stockNumber', 'stockNumber', 
  '', 2, 512, NULL );  
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'vehicleKey', 'vehicleKey', 
  '', 2, 512, NULL );   
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'fromDateKey', 'fromDateKey', 
  '', 2, 512, NULL ); 
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'thruDateKey', 'thruDateKey', 
  '', 2, 512, NULL );   
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'noY', 'noY', 
  '', 2, 512, NULL );   

DELETE FROM factVehicleInventory;
INSERT INTO factVehicleInventory
SELECT a.storecode, a.stocknumber, a.noy, a.vehiclekey, b.datekey,
  coalesce(d.datekey, (SELECT datekey FROM day WHERE datetype <> 'DATE'))
FROM factVehicleAcquisition a
INNER JOIN day b on a.datekey = b.datekey
LEFT JOIN factVehicleSale c on a.noy = c.noy
LEFT JOIN day d on c.approveddatekey = d.datekey;


-- V1.  anomaly repair AND culling
-- VI.A:  NULL source vehiclekey on trade -----------------------**************
/*
SELECT *
-- SELECT COUNT(*) -- 256
FROM factVehicleAcquisition
WHERE datekey IS NOT NULL
  AND sourcevehiclekey IS NULL 
  AND vehicleAcquisitionInfoKey IN (13,14)
*/  
-- new car trades
UPDATE factVehicleAcquisition
SET sourceVehicleKey = x.vehicleKey
FROM (
  SELECT e.stocknumber, f.vehiclekey
  -- SELECT *
  FROM (
    SELECT a.stocknumber, b.thedate, 
      replace(replace(replace(replace(replace(replace(replace(replace(replace
        (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
         '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix
    -- SELECT COUNT(*)
    FROM factVehicleAcquisition a
    INNER JOIN day b on a.datekey = b.datekey
    WHERE a.datekey IS NOT NULL
      AND a.sourcevehiclekey IS NULL) e  
  LEFT JOIN factVehicleSale f on replace(e.stocknumber, 'A', '') = f.stocknumber     
  WHERE e.suffix = 'A'    
    AND f.storecode IS NOT NULL 
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;
-- whack the rest
DELETE 
FROM factVehicleAcquisition
WHERE datekey IS NOT NULL 
  AND sourcevehiclekey IS NULL 
  AND vehicleAcquisitionInfoKey IN (13,14);

-- VI.B vehicle sale with no stocknumber -------------------------*************
/*
select *
-- SELECT COUNT(*)
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.cardealinfokey = b.cardealinfokey
  AND b.vehicleTypeCode = 'U'
WHERE stocknumber = ''
*/
-- ok here are uc sales to be updated with the stocknumber
--DROP TABLE #sales;
BEGIN TRANSACTION;
TRY 
  select a.storecode, e.imstk# AS stocknumber, replace(e.imstk#,'y','') AS noy, 
  b.vehicleKey, d.datekey,
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(e.imstk#, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix 
  -- SELECT COUNT(*)
  INTO #sales
  FROM factVehicleSale a
  INNER JOIN dimVehicle b on a.vehicleKey = b.vehicleKey
  INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
    AND c.vehicleTypeCode = 'U'
  INNER JOIN day d on a.approvedDateKey = d.datekey
  LEFT JOIN stgArkonaINPMAST e on b.vin = e.imvin
  WHERE a.stocknumber = ''
    AND abs(d.thedate - e.imddlv) < 10;
    
  -- update vehicleSales with the stocknumber
  UPDATE factVehicleSale
  SET stocknumber = x.stocknumber,
      noy = x.noy
  FROM (    
    select *
    FROM #sales a
    LEFT JOIN factVehicleSale b on a.vehicleKEy = b.vehicleKey 
      AND a.datekey = b.approveddatekey
      AND a.storecode = b.storecode) x  
  WHERE factVehicleSale.stocknumber = ''
    AND factVehicleSale.vehicleKey = x.vehicleKey
    AND factVehicleSale.approvedDateKey = x.approvedDateKey
    AND factVehicleSale.storecode = x.storecode;     
    
-- factVehicleAcquisition: trades
  INSERT INTO factVehicleAcquisition (storecode, stocknumber, noy,
     vehicleAcquisitionInfoKey, vehicleKey, dateKey, sourceVehicleKey)
  SELECT y.storecode, y.stocknumber, y.noy, z.vehicleAcquisitionInfoKey, 
    y.vehicleKey, 
    (select datekey from day where thedate = zz.imdinv), y.sourceVehicleKey
  FROM (
    select c.*,
      CASE c.sourcestock
        WHEN 'NA' THEN e.vehicleKey
        ELSE d.vehiclekey
      END AS sourceVehicleKey
    FROM (
      SELECT a.*,
        CASE 
          WHEN right(TRIM(a.stocknumber), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
          ELSE 'Purchase'
        END AS acquisitionType,
        CASE 
          WHEN right(TRIM(a.stocknumber), 1) IN ('a','b','c','d','e','f') THEN
            CASE 
              WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
              ELSE 'Used'
            END
          ELSE ''
        END AS acquisitionTypeCategory,
        CASE
          WHEN right(TRIM(a.stocknumber), 1) IN ('a','b','c','d','e','f') THEN
      -- need to handle BB, CC DD, AC, AD etc
            CASE
              WHEN suffix IN ('A','AA','AAA','AAAA') THEN replace(a.stocknumber, 'A', '')
              WHEN suffix IN ('AB','B') THEN replace(a.stocknumber, 'B', 'A')
              WHEN suffix = 'C' THEN replace(a.stocknumber, 'C', 'B')
              WHEN suffix = 'D' THEN replace(a.stocknumber, 'D', 'C')
              WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'A' THEN replace(a.stocknumber, 'A', '')
              WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'B' THEN replace(a.stocknumber, 'B', 'A')
              WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'C' THEN replace(a.stocknumber, 'C', 'B')
              WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'D' THEN replace(a.stocknumber, 'D', 'C')
              WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'A' THEN replace(a.stocknumber, 'A', '')
              WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'B' THEN replace(a.stocknumber, 'B', 'A')
              WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(a.stocknumber, 'C', 'B')
              WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(a.stocknumber, 'D', 'C')
            END 
          ELSE 'NA'
        END AS sourceStock  
      FROM #sales a
      LEFT JOIN factVehicleAcquisition b on a.stocknumber = b.stocknumber 
      WHERE b.stocknumber IS NULL) c -- just making sure
    LEFT JOIN factVehicleSale d on c.sourceStock = d.stocknumber  
    LEFT JOIN dimVehicle e on 1 = 1
      AND vin = 'NA') y
  LEFT JOIN dimVehicleAcquisitionInfo z on y.acquisitionType = z.acquisitionType collate ads_Default_ci
    AND y.acquisitionTypeCategory = z.acquisitionTypeCategory collate ads_Default_ci
  LEFT JOIN stgArkonaINPMAST zz on y.stocknumber = zz.imstk#  
  WHERE y.acquisitionType = 'Trade';

-- factVehicleAcquisition purchases
  INSERT INTO factVehicleAcquisition (storecode, stocknumber, noy,
    vehicleAcquisitionInfoKey, vehicleKey, dateKey, sourceVehicleKey)
  SELECT a.storecode, a.stocknumber, a.noy, d.vehicleAcquisitionInfoKey, 
    a.vehicleKey,
    (SELECT datekey FROM day WHERE thedate = e.imdinv),
    (SELECT vehicleKey FROM dimvehicle WHERE vin = 'NA')
  FROM #sales a
  INNER JOIN dimVehicle aa on a.vehicleKey = aa.vehicleKey
  LEFT JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
  LEFT JOIN dps.vehicleAcquisitions c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
  LEFT JOIN dimVehicleAcquisitionInfo d on d.acquisitionType = 'Purchase'
    AND d.acquisitionTypeCategory = 
      CASE
        WHEN right(trim(suffix), 1) IN ('a','b','c','d','e','f','s') THEN 'Unknown'
        WHEN suffix = 'G' THEN 'Intra Market Wholesale'
        WHEN suffix = 'K' THEN 'Unknown'
        WHEN suffix = 'L' THEN 
          CASE
            WHEN c.typ IN ('VehicleAcquisition_LeasePurcha', 'VehicleAcquisition_Trade') THEN 'Lease Purchase'
            WHEN c.typ = 'VehicleAcquisition_InAndOut' THEN 'In And Out'
            ELSE 'Unknown'
          END
        WHEN right(trim(suffix), 1) = 'N' THEN 'National' 
        WHEN suffix = 'P' THEN 'Street'
        WHEN right(trim(suffix), 1) = 'R' THEN 'Rental'
        WHEN right(TRIM(suffix), 1) = 'T' THEN
          CASE
            WHEN right(TRIM(suffix), 2) = 'DT' THEN 'Driver Training'
            ELSE 'Unknown'
          END 
        WHEN right(TRIM(suffix), 1) = 'X' THEN
          CASE
            WHEN length(TRIM(suffix)) = 1 THEN
              CASE
                WHEN c.typ = 'VehicleAcquisition_Auction' THEN 'Auction'
                WHEN c.typ = 'VehicleAcquisition_IntraMarketPurchase' THEN 'Intra Market Wholesale'
                ELSE 'Unknown'
              END 
            WHEN suffix = 'XX' THEN 'Auction'
            ELSE 'Unknown'
          END 
        
        WHEN right(TRIM(suffix), 1) = 'Z' THEN 'Unknown'
      END
  LEFT JOIN stgArkonaINPMAST e on a.stocknumber = e.imstk#    
  WHERE right(trim(a.suffix) , 1) NOT IN ('a','b','c','d','e','f');
  
-- factVehicleInventory
  INSERT INTO factVehicleInventory (storecode,stocknumber,noy,vehiclekey, 
    fromdatekey,thrudatekey)
  SELECT a.storecode, a.stocknumber, a.noy, a.vehiclekey, d.datekey, c.approveddatekey
  FROM #sales a
  LEFT JOIN factVehicleInventory b on a.stocknumber = b.stocknumber
  LEFT JOIN factVehicleSale c on a.stocknumber = c.stocknumber
  LEFT JOIN factVehicleAcquisition d on a.stocknumber = d.stocknumber
  WHERE b.stocknumber IS NULL;   
            
  -- DELETE the remainders
  UPDATE factVehicleSale
    SET stocknumber = 'DELETE'
  FROM (  
    SELECT *
    FROM factVehicleSale a
    INNER JOIN dimCarDealInfo c on a .carDealInfoKEy = c.carDealINfoKey
      AND c.vehicleTypeCode = 'U'
    WHERE a.stocknumber = '') x
  WHERE factVehicleSale.bmkey = x.bmkey
    AND factVehicleSale.vehicleKey = x.VehicleKey
    AND factVehicleSale.approvedDateKey = x.approvedDateKey;
  DELETE 
  FROM factVehicleSale
  WHERE stocknumber = 'DELETE';                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;
    
-- VI.C dup stocknumbers IN factVehicleAcquisition -------------***************    
-- actually IN every fucking thing
-- 6/14/15 fuck it, whack em ALL

DELETE 
FROM factVehicleSale
WHERE stocknumber IN (
  SELECT stocknumber
  FROM factVehicleSale a
  INNER JOIN dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey
    AND b.vehicleTypeCode = 'U'
  GROUP BY stocknumber
  HAVING COUNT(*) > 1);

-- this IS a goofy anomaly, IN twice once AS new, once AS used  
DELETE FROM factVehicleSale WHERE stocknumber = '10187x';  
DELETE FROM factVehicleAcquisition WHERE stocknumber = '10187X';
DELETE FROM factVehicleInventory WHERE stocknumber = '10187X';
 
DELETE 
FROM factVehicleAcquisition
WHERE stocknumber IN (
  SELECT stocknumber
  FROM factVehicleAcquisition
  GROUP BY stocknumber
  HAVING COUNT(*) > 1);
  
DELETE 
FROM factVehicleInventory 
WHERE stocknumber IN (
  SELECT stocknumber
  FROM factVehicleInventory
  GROUP BY stocknumber
  HAVING COUNT(*) > 1); 
  
-- VI.D veh acq with no acqInfoKey -------------------------*******************
-- SELECT COUNT(*) FROM factVehicleAcquisition WHERE vehicleAcquisitionInfoKey IS NULL;
SELECT a.*,   
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
  substring(a.stocknumber, 2,9) AS oldStock   
INTO #noAcqKey    
-- SELECT COUNT(*)           
FROM factVehicleAcquisition a
WHERE vehicleAcquisitionInfoKey IS NULL;   

-- DROP TABLE #noAcqKey_1;
SELECT f.storecode, f.stocknumber, f.noy, f.vehiclekey, f.fromdate, f.category, f.expr_1,
  g.vehicleKey AS sourceVehicleKey, g.approveddatekey, f.typ, f.suffix
-- SELECT COUNT(*)
INTO #noAcqKey_1
FROM (
  SELECT e.*,
    CASE 
      WHEN right(TRIM(stocknumber), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
      ELSE 'Purchase'
    END AS category,
    CASE 
      WHEN right(TRIM(stocknumber), 1) IN ('a','b','c','d','e','f') THEN
        CASE 
          WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
          ELSE 'Used'
        END
      ELSE ''
    END, 
    CASE
      WHEN right(TRIM(stocknumber), 1) IN ('a','b','c','d','e','f') THEN
  -- need to handle BB, CC DD, AC, AD etc
        CASE
          WHEN suffix IN ('A','AA','AAA','AAAA') THEN replace(stocknumber, 'A', '')
          WHEN suffix IN ('AB','B') THEN replace(stocknumber, 'B', 'A')
          WHEN suffix = 'C' THEN replace(stocknumber, 'C', 'B')
          WHEN suffix = 'D' THEN replace(stocknumber, 'D', 'C')
          WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'A' THEN replace(stocknumber, 'A', '')
          WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'B' THEN replace(stocknumber, 'B', 'A')
          WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'C' THEN replace(stocknumber, 'C', 'B')
          WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'D' THEN replace(stocknumber, 'D', 'C')
          WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'A' THEN replace(stocknumber, 'A', '')
          WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'B' THEN replace(stocknumber, 'B', 'A')
          WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(stocknumber, 'C', 'B')
          WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(stocknumber, 'D', 'C')
        END 
      ELSE 'NA'
    END AS sourceStock 
  FROM (
    SELECT c.*, d.typ
    FROM (
      SELECT 'stock', a.*, b.stocknumber AS dpsStock, b.VehicleInventoryItemID, cast(b.fromts as sql_date) as fromDate 
      -- SELECT COUNT(*)
      FROM #noAcqKey a
      INNER JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
      union
      SELECT 'oldstock', a.*, b.stocknumber, b.VehicleInventoryItemID, cast(b.fromts as sql_date) as fromDate 
      -- SELECT COUNT(*)
      FROM #noAcqKey a
      INNER JOIN dps.VehicleInventoryItems b on a.oldstock = b.stocknumber) c
    LEFT JOIN dps.vehicleacquisitions d on c.VehicleInventoryItemID  = d.VehicleInventoryItemID) e) f  
LEFT JOIN factVehicleSale g on f.sourceStock = g.stocknumber
WHERE (f.category = 'Purchase' OR g.storecode IS NOT NULL);   

-- trades 
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey 
FROM (    
  SELECT a.*,b.thedate, c.*, b.datekey
  FROM #noAcqKey_1 a
  LEFT JOIN day b on a.approveddatekey = b.datekey
  LEFT JOIN dimVehicleAcquisitionInfo c on a.category = c.acquisitionType collate ads_Default_ci
    AND a.expr_1 = c.acquisitiontypecategory collate ads_Default_ci
  WHERE category = 'Trade') x  
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;  

-- purchases
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = (SELECT vehiclekey FROM dimVehicle WHERE vin = 'NA')
FROM (
--SELECT * FROM (
  SELECT a.*,b.thedate, d.*, b.datekey
  FROM #noAcqKey_1 a
  LEFT JOIN day b on a.fromdate = b.thedate
  LEFT JOIN dimVehicleAcquisitionInfo d on a.category = d.acquisitionType collate ads_default_ci
    AND d.acquisitionTypeCategory = 
      CASE
        WHEN right(trim(suffix), 1) IN ('a','b','c','d','e','f','s') THEN 'Unknown'
        WHEN suffix = 'G' THEN 'Intra Market Wholesale'
        WHEN suffix = 'K' THEN 
          CASE 
            WHEN a.typ = 'VehicleAcquisition_InAndOut' THEN 'In And Out'
            WHEN a.typ = 'VehicleAcquisition_Auction' THEN 'Auction'
            WHEN a.typ = 'VehicleAcquisition_Street' THEN 'Street'
            ELSE 'Unknown' 
          END 
        WHEN suffix = 'L' THEN 
          CASE
            WHEN a.typ IN ('VehicleAcquisition_LeasePurcha', 'VehicleAcquisition_Trade') THEN 'Lease Purchase'
            WHEN a.typ = 'VehicleAcquisition_InAndOut' THEN 'In And Out'
            ELSE 'Unknown'
          END
        WHEN right(trim(suffix), 1) = 'N' THEN 'National' 
        WHEN suffix = 'P' THEN 'Street'
        WHEN right(trim(suffix), 1) = 'R' THEN 'Rental'
        WHEN right(TRIM(suffix), 1) = 'T' THEN
          CASE
            WHEN right(TRIM(suffix), 2) = 'DT' THEN 'Driver Training'
            ELSE 'Unknown'
          END 
        WHEN right(TRIM(suffix), 1) = 'X' THEN
          CASE
            WHEN length(TRIM(suffix)) = 1 THEN
              CASE
                WHEN a.typ = 'VehicleAcquisition_Auction' THEN 'Auction'
                WHEN a.typ = 'VehicleAcquisition_IntraMarketPurchase' THEN 'Intra Market Wholesale'
                ELSE 'Unknown'
              END 
            WHEN suffix = 'XX' THEN 'Auction'
            ELSE 'Unknown'
          END         
        WHEN right(TRIM(suffix), 1) = 'Z' THEN 'Unknown'
      END
  WHERE a.category = 'Purchase') x 
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;

-- AND whack the rest
DELETE 
FROM factVehicleAcquisition
WHERE stocknumber IN (
  SELECT stocknumber
  -- SELECT COUNT(*)           
  FROM factVehicleAcquisition a
  WHERE vehicleAcquisitionInfoKey IS NULL);

-- VI.E sale without acquisition --------------------**************************  
-- sales with no acquisition
/*
SELECT * -- SELECT COUNT(*)
FROM (
  SELECT c.thedate, a.storecode, a.stocknumber, a.noy
  FROM factVehicleSale a
  INNER JOIN dimcardealinfo b on a.cardealinfokey = b.cardealinfokey
    AND b.vehicletypecode = 'U'
  INNER JOIN day c on a.approveddatekey = c.datekey) c
LEFT JOIN factVehicleAcquisition d on c.noy = d.noy  
WHERE d.noy is NULL;  
*/
-- DROP TABLE #saleNoAcq;
SELECT c.*
INTO #saleNoAcq
FROM (
  SELECT c.thedate, a.storecode, a.stocknumber, a.noy, 
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.noy, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
  substring(a.stocknumber, 2, 9) AS oldStock  
  FROM factVehicleSale a
  INNER JOIN dimcardealinfo b on a.cardealinfokey = b.cardealinfokey
    AND b.vehicletypecode = 'U'
  INNER JOIN day c on a.approveddatekey = c.datekey) c
LEFT JOIN factVehicleAcquisition d on c.noy = d.noy  
WHERE d.noy is NULL;  


-- ALL IN one
INSERT INTO factVehicleAcquisition (storecode, stocknumber, noy, 
 vehicleAcquisitionInfoKey, vehiclekey, datekey, sourceVehicleKey)

SELECT g.storecode, g.stocknumber, g.noy, h.vehicleAcquisitionInfoKey,
  (SELECT vehicleKey FROM factVehicleSale WHERE stocknumber = g.stocknumber),
  CASE g.acquisitionType
    WHEN 'Purchase' THEN (SELECT datekey FROM day WHERE thedate = g.dpsFromDate)
    ELSE (SELECT datekey FROM day WHERE thedate = g.sourceSaleDate)
  END,
  CASE g.sourceStock
    WHEN 'NA' THEN (SELECT vehicleKey FROM dimvehicle WHERE vin = 'NA')
    ELSE (SELECT vehicleKey FROM factVehicleSale WHERE stocknumber = g.sourceStock)
  END 
-- select * -- SELECT COUNT(*)  
FROM (
  SELECT b.*,
    CASE 
      WHEN right(TRIM(b.noy), 1) IN ('a','b','c','d','e','f') THEN
        CASE 
          WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
          ELSE 'Used'
        END
      ELSE ''
    END AS tradeAcquisitionTypeCategory, 
    CASE  
      WHEN right(trim(b.suffix), 1) in ('a','b','c','d','e','f') then 'Trade'
      ELSE 'Purchase'
    END AS acquisitionType,
    CASE  
      WHEN right(trim(b.suffix), 1) in ('a','b','c','d','e','f') then c.stocknumber 
    END AS sourceVehicle,
    CASE  
      WHEN right(trim(b.suffix), 1) not in ('a','b','c','d','e','f') then e.typ
    END AS purchaseAcquisitionTypeCategory ,
    CAST(d.fromts AS sql_date) AS dpsFromDate,
    cc.thedate AS sourceSaleDate
  FROM (
    SELECT a.*,
        CASE
          WHEN right(TRIM(noy), 1) IN ('a','b','c','d','e','f') THEN
      -- need to handle BB, CC DD, AC, AD etc
            CASE
              WHEN suffix IN ('A','AA','AAA','AAAA') THEN replace(stocknumber, 'A', '')
              WHEN suffix IN ('AB','B') THEN replace(stocknumber, 'B', 'A')
              WHEN suffix = 'C' THEN replace(stocknumber, 'C', 'B')
              WHEN suffix = 'D' THEN replace(stocknumber, 'D', 'C')
              WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'A' THEN replace(noy, 'A', '')
              WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'B' THEN replace(noy, 'B', 'A')
              WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'C' THEN replace(noy, 'C', 'B')
              WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'D' THEN replace(noy, 'D', 'C')
              WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'A' THEN replace(noy, 'A', '')
              WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'B' THEN replace(noy, 'B', 'A')
              WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(noy, 'C', 'B')
              WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(noy, 'D', 'C')
            END 
          ELSE 'NA'
        END AS sourceStock 
    FROM #saleNoAcq a) b
  -- WHERE suffix IN ('a','b','c','d','e','f')) b
  LEFT JOIN factVehicleSale c on b.sourceStock = c.noy
  LEFT JOIN day cc on c.approveddatekey = cc.datekey 
  LEFT JOIN dps.VehicleInventoryItems d on b.stocknumber = d.stocknumber
  LEFT JOIN dps.vehicleacquisitions e on d.VehicleInventoryItemID = e.VehicleInventoryItemID) g
LEFT JOIN dimVehicleAcquisitionInfo h on h.vehicleType = 'Used'  
  AND 
    CASE
      WHEN g.sourceVehicle IS NOT NULL THEN h.acquisitionType = 'Trade' collate ads_default_ci
        AND h.acquisitionTypeCategory = g.tradeAcquisitionTypeCategory collate ads_default_ci
      WHEN g.purchaseAcquisitionTypeCategory IS NOT NULL THEN 
        h.acquisitionType = 'Purchase' collate ads_default_ci
          AND h.acquisitionTypeCategory = 
            CASE g.purchaseAcquisitionTypeCategory 
              WHEN 'VehicleAcquisition_IntraMarketPurchase' THEN 'Intra Market Wholesale'
              WHEN 'VehicleAcquisition_InAndOut' THEN 'In AND Out'
            END     
    END 
WHERE (sourcevehicle IS NOT NULL OR purchaseAcquisitionTypeCategory IS NOT NULL);

-- AND whack the rest
DELETE FROM factVehicleSale
WHERE stocknumber IN (
  SELECT c.stocknumber
  FROM (
    SELECT c.thedate, a.storecode, a.stocknumber, a.noy
    FROM factVehicleSale a
    INNER JOIN dimcardealinfo b on a.cardealinfokey = b.cardealinfokey
      AND b.vehicletypecode = 'U'
    INNER JOIN day c on a.approveddatekey = c.datekey) c
  LEFT JOIN factVehicleAcquisition d on c.noy = d.noy  
  WHERE d.noy is NULL);  

-- VI.F acquisition with no inventory ------------------------------***********
/*
SELECT COUNT(*) 
FROM factVehicleAcquisition a
LEFT JOIN factVehicleInventory b on a.noy = b.noy
WHERE b.noy IS NULL;
*/
INSERT INTO factVehicleInventory (storecode, stocknumber, noy, vehiclekey,
  fromdatekey, thrudatekey)
select a.storecode, a.stocknumber, a.noy, a.vehiclekey, 
  a.datekey, c.approveddatekey
FROM factVehicleAcquisition a
LEFT JOIN factVehicleInventory b on a.noy = b.noy
LEFT JOIN factVehicleSale c on a.stocknumber = c.stocknumber
WHERE b.noy IS NULL; 

-- VI.G inv with no acq ------------------------------------------*************
/*
SELECT * -- SELECT COUNT(*) -- 94
FROM factVehicleInventory a
LEFT JOIN factVehicleAcquisition b on a.stocknumber = b.stocknumber
WHERE b.stocknumber IS NULL 
*/
-- 6/17 looks LIKE it IS going to be pointless to salvage any of these, no 
--      factVehicleSale record for any of them, mostly old old deals, mostly
--      crookston
-- whack em
DELETE 
FROM factVehicleInventory
WHERE stocknumber IN (
SELECT a.stocknumber
FROM factVehicleInventory a
LEFT JOIN factVehicleAcquisition b on a.stocknumber = b.stocknumber
WHERE b.stocknumber IS NULL);

 
-- VI. closed inv with no sale ------------------------------------------------
-- 6/18: inv with no acq seems to have taken care of these AS well
/*
SELECT a.*, b.thedate -- SELECT COUNT(*) -- 93 of them
FROM factVehicleInventory a
INNER JOIN day b on a.thrudatekey = b.datekey
LEFT JOIN factVehicleSale c on a.stocknumber = c.stocknumber
WHERE b.thedate <= curdate()
  AND c.stocknumber IS NULL 
*/

SELECT 'null source vehiclekey on trade', COUNT(*) AS theCount
FROM factVehicleAcquisition
WHERE datekey IS NOT NULL
  AND sourcevehiclekey IS NULL 
  AND vehicleAcquisitionInfoKey IN (13,14)
UNION
SELECT 'sale with no stocknumber', COUNT(*)
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.cardealinfokey = b.cardealinfokey
  AND b.vehicleTypeCode = 'U'
WHERE stocknumber = ''
UNION 
SELECT 'dup stocknumber in acquisition', COUNT(*)
FROM (
  SELECT stocknumber
  FROM factVehicleAcquisition
  GROUP BY stocknumber
  HAVING COUNT(*) > 1) x
UNION 
SELECT 'dup stocknumber in inventory', COUNT(*)
FROM (
  SELECT stocknumber
  FROM factVehicleInventory
  GROUP BY stocknumber
  HAVING COUNT(*) > 1) x
UNION 
SELECT 'dup stocknumber in sale', COUNT(*)
FROM (
  SELECT stocknumber
  FROM factVehicleSale a
  INNER JOIN dimCarDealInfo b on a.cardealinfokey = b.cardealinfokey
    AND b.vehicletypecode = 'U'
  GROUP BY stocknumber
  HAVING COUNT(*) > 1) x
UNION
SELECT 'acq with no acq key',COUNT(*) 
FROM factVehicleAcquisition 
WHERE vehicleAcquisitionInfoKey IS NULL
UNION
SELECT 'sale with no acq', COUNT(*)
FROM (
  SELECT c.thedate, a.storecode, a.stocknumber, a.noy
  FROM factVehicleSale a
  INNER JOIN dimcardealinfo b on a.cardealinfokey = b.cardealinfokey
    AND b.vehicletypecode = 'U'
  INNER JOIN day c on a.approveddatekey = c.datekey) c
LEFT JOIN factVehicleAcquisition d on c.noy = d.noy  
WHERE d.noy is NULL 
UNION
SELECT 'acq with no inv',COUNT(*) 
FROM factVehicleAcquisition a
LEFT JOIN factVehicleInventory b on a.noy = b.noy
WHERE b.noy IS NULL
UNION
SELECT 'inv with no acq', COUNT(*) 
FROM factVehicleInventory a
LEFT JOIN factVehicleAcquisition b on a.stocknumber = b.stocknumber
WHERE b.stocknumber IS NULL 
UNION
SELECT 'closed inv with no sale', COUNT(*) 
FROM factVehicleInventory a
INNER JOIN day b on a.thrudatekey = b.datekey
LEFT JOIN factVehicleSale c on a.stocknumber = c.stocknumber
WHERE b.thedate <= curdate()
  AND c.stocknumber IS NULL;
  

  
/*
-- arkona inventory NOT IN dps
SELECT a.*, c.stocknumber, d.vin -- SELECT COUNT(*)
FROM factVehicleInventory a
INNER JOIN day b on a.thrudatekey = b.datekey
LEFT JOIN dps.VehicleInventoryItems c on a.stocknumber = c.stocknumber
LEFT JOIN dimvehicle d on a.vehiclekey = d.vehiclekey
WHERE b.thedate > curdate()
  AND a.storecode = 'ry1'
  AND c.stocknumber IS NULL 
  
-- dps inventory NOT IN factVehicleInventory
SELECT a.stocknumber, aa.vin
FROM dps.VehicleInventoryItems a
LEFT JOIN dps.VehicleItems aa on a.VehicleItemID = aa.VehicleItemID 
LEFT JOIN factVehicleinventory b on a.stocknumber = b.stocknumber
WHERE a.thruts IS NULL   
  AND b.stocknumber IS NULL 


-- compare inpmast to factVehicleInventory
SELECT *
FROM factVehicleInventory a
INNER JOIN day aa on a.thrudatekey = aa.datekey
LEFT JOIN stgArkonaINPMAST b on a.stocknumber = b.imstk#
  AND b.imtype = 'u'
  AND b.imstat = 'i'
WHERE b.imstk# IS NULL 
  AND aa.thedate > curdate()

-- arkona NOT factVehicleInventory  
SELECT *
FROM stgArkonaINPMAST a 
LEFT JOIN factVehicleInventory b on a.imstk# = b.stocknumber
WHERE a.imtype = 'u'
  AND a.imstat = 'i'  
  AND b.stocknumber IS NULL 
*/  