17179: new car
17179A: trade on sale of new car
17179AA: second trade on sale of new car
17179B: trade on sale of 17179A
17179C: trade on sale of 17179B
??
25164AB: trade on sale of 25164AA

ALL trades on the purchase of a new car END IN A, NOT ALL stocknumbers that 
  END IN A are trades on new car purchases
  
ADD two rows to dimVehicleAcquisitionInfo: Purchase/Rental, Purchase/National

INSERT INTO dimVehicleAcquisitionInfo (vehicleType, acquisitionType, acquisitionTypeCategory)
values('Used','Purchase','Rental');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType, acquisitionType, acquisitionTypeCategory)
values('Used','Purchase','National');
  
Y: flag to indicate a damaged title
R: rental
DT: driver training
N: National
K: early trade, customer IS purchasing a vehicle for future delivery, we are
  taking their trade now, DO NOT know what the stock number of the future 
  delivery will be, also used for owner (Rydell) cars put into inventory
P: street purchase, including at RY1 the purchase of a lease return    
X: auction purchase, NOT floorable,
     also, RY2 vehicle ws to RY1 for retail
XX: auction purchase, floorable (program car)
L: RY2 only, lease buy out
G: RY2 only, wholesale purchase of RY1 vehicle

used car acquisitions
  trade
    new car sale
    used car sale
  purchase
    auction
    street
    lease
    in/out
    back on
    intramarket ws
    purchase of rental/national
    purchase of owner/coach/...
    other
??
CREATE TABLE dimVehicleAcquisitionCategory (
  category cichar(10) constraint NOT NULL,
  classification cichar(60) constraint NOT NULL,
  constraint PK primary key (category, classification)) IN database;
  
  