-- ok here uc sales to be updated with the stocknumber
DROP TABLE #sales;
select a.storecode, e.imstk# AS stocknumber, replace(e.imstk#,'y','') AS noy, 
b.vehicleKey, d.datekey,
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(e.imstk#, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix 
-- SELECT COUNT(*)
INTO #sales
FROM factVehicleSale a
INNER JOIN dimVehicle b on a.vehicleKey = b.vehicleKey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  AND c.vehicleTypeCode = 'U'
INNER JOIN day d on a.approvedDateKey = d.datekey
LEFT JOIN stgArkonaINPMAST e on b.vin = e.imvin
WHERE a.stocknumber = ''
  AND abs(d.thedate - e.imddlv) < 10
  
-- for updating vehicleSales
UPDATE factVehicleSales
SET stocknumber = x.stocknumber,
    noy = x.noy
FROM (    
  select *
  FROM #sales a
  LEFT JOIN factVehicleSale b on a.vehicleKEy = b.vehicleKey 
    AND a.datekey = b.approveddatekey
    AND a.storecode = b.storecode) x  
WHERE factVehicleSales = ''
  AND factVehicleSales.vehicleKey = x.vehicleKey
  AND factVehicleSales.datekey = x.approvedDateKey
  AND factVehicleSales.storecode = x.storecode; 
  
-- need an acquisition row for each of these
-- trades
-- INSERT INTO factVehicleAcquisition (storecode, stocknumber, noy,
--   vehicleAcquisitionInfoKey, vehicleKey, dateKey, sourceVehicleKey)
SELECT y.storecode, y.stocknumber, y.noy, z.vehicleAcquisitionInfoKey, 
  y.vehicleKey, 
  (select datekey from day where thedate = zz.imdinv), y.sourceVehicleKey
FROM (
  select c.*,
    CASE c.sourcestock
      WHEN 'NA' THEN e.vehicleKey
      ELSE d.vehiclekey
    END AS sourceVehicleKey
  FROM (
    SELECT a.*,
      CASE 
        WHEN right(TRIM(a.stocknumber), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
        ELSE 'Purchase'
      END AS acquisitionType,
      CASE 
        WHEN right(TRIM(a.stocknumber), 1) IN ('a','b','c','d','e','f') THEN
          CASE 
            WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
            ELSE 'Used'
          END
        ELSE ''
      END AS acquisitionTypeCategory,
      CASE
        WHEN right(TRIM(a.stocknumber), 1) IN ('a','b','c','d','e','f') THEN
    -- need to handle BB, CC DD, AC, AD etc
          CASE
            WHEN suffix IN ('A','AA','AAA','AAAA') THEN replace(a.stocknumber, 'A', '')
            WHEN suffix IN ('AB','B') THEN replace(a.stocknumber, 'B', 'A')
            WHEN suffix = 'C' THEN replace(a.stocknumber, 'C', 'B')
            WHEN suffix = 'D' THEN replace(a.stocknumber, 'D', 'C')
            WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'A' THEN replace(a.stocknumber, 'A', '')
            WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'B' THEN replace(a.stocknumber, 'B', 'A')
            WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'C' THEN replace(a.stocknumber, 'C', 'B')
            WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'D' THEN replace(a.stocknumber, 'D', 'C')
            WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'A' THEN replace(a.stocknumber, 'A', '')
            WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'B' THEN replace(a.stocknumber, 'B', 'A')
            WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(a.stocknumber, 'C', 'B')
            WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(a.stocknumber, 'D', 'C')
          END 
        ELSE 'NA'
      END AS sourceStock  
    FROM #sales a
    LEFT JOIN factVehicleAcquisition b on a.stocknumber = b.stocknumber 
    WHERE b.stocknumber IS NULL) c -- just making sure
  LEFT JOIN factVehicleSale d on c.sourceStock = d.stocknumber  
  LEFT JOIN dimVehicle e on 1 = 1
    AND vin = 'NA') y
LEFT JOIN dimVehicleAcquisitionInfo z on y.acquisitionType = z.acquisitionType collate ads_Default_ci
  AND y.acquisitionTypeCategory = z.acquisitionTypeCategory collate ads_Default_ci
LEFT JOIN stgArkonaINPMAST zz on y.stocknumber = zz.imstk#  
WHERE y.acquisitionType = 'Trade'


-- purchases
-- INSERT INTO factVehicleAcquisition (storecode, stocknumber, noy,
--   vehicleAcquisitionInfoKey, vehicleKey, dateKey, sourceVehicleKey)
SELECT a.storecode, a.stocknumber, a.noy, d.vehicleAcquisitionInfoKey, 
  a.vehicleKey, 
  (SELECT datekey FROM day WHERE thedate = e.imdinv),
  (SELECT vehicleKey FROM dimvehicle WHERE vin = 'NA')
FROM #sales a
INNER JOIN dimVehicle aa on a.vehicleKey = aa.vehicleKey
LEFT JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
LEFT JOIN dps.vehicleAcquisitions c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN dimVehicleAcquisitionInfo d on d.acquisitionType = 'Purchase'
  AND d.acquisitionTypeCategory = 
    CASE
      WHEN right(trim(suffix), 1) IN ('a','b','c','d','e','f','s') THEN 'Unknown'
      WHEN suffix = 'G' THEN 'Intra Market Wholesale'
      WHEN suffix = 'K' THEN 'Unknown'
      WHEN suffix = 'L' THEN 
        CASE
          WHEN c.typ IN ('VehicleAcquisition_LeasePurcha', 'VehicleAcquisition_Trade') THEN 'Lease Purchase'
          WHEN c.typ = 'VehicleAcquisition_InAndOut' THEN 'In And Out'
          ELSE 'Unknown'
        END
      WHEN right(trim(suffix), 1) = 'N' THEN 'National' 
      WHEN suffix = 'P' THEN 'Street'
      WHEN right(trim(suffix), 1) = 'R' THEN 'Rental'
      WHEN right(TRIM(suffix), 1) = 'T' THEN
        CASE
          WHEN right(TRIM(suffix), 2) = 'DT' THEN 'Driver Training'
          ELSE 'Unknown'
        END 
      WHEN right(TRIM(suffix), 1) = 'X' THEN
        CASE
          WHEN length(TRIM(suffix)) = 1 THEN
            CASE
              WHEN c.typ = 'VehicleAcquisition_Auction' THEN 'Auction'
              WHEN c.typ = 'VehicleAcquisition_IntraMarketPurchase' THEN 'Intra Market Wholesale'
              ELSE 'Unknown'
            END 
          WHEN suffix = 'XX' THEN 'Auction'
          ELSE 'Unknown'
        END 
      
      WHEN right(TRIM(suffix), 1) = 'Z' THEN 'Unknown'
    END
LEFT JOIN stgArkonaINPMAST e on a.stocknumber = e.imstk#    
WHERE right(trim(a.suffix) , 1) NOT IN ('a','b','c','d','e','f');

-- factVehicleInventory
INSERT INTO factVehicleInventory (storecode,stocknumber,noy,vehiclekey, 
  fromdatekey,thrudatekey)
SELECT a.storecode, a.stocknumber, a.noy, a.vehiclekey, d.datekey, c.approveddatekey
FROM #sales a
LEFT JOIN factVehicleInventory b on a.stocknumber = b.stocknumber
LEFT JOIN factVehicleSale c on a.stocknumber = c.stocknumber
LEFT JOIN factVehicleAcquisition d on a.stocknumber = d.stocknumber
WHERE b.stocknumber IS NULL 


UPDATE factVehicleSale
  SET stocknumber = 'DELETE'
FROM (  
  SELECT *
  FROM factVehicleSale a
  INNER JOIN dimCarDealInfo c on a .carDealInfoKEy = c.carDealINfoKey
    AND c.vehicleTypeCode = 'U'
  WHERE a.stocknumber = '') x
WHERE factVehicleSale.bmkey = x.bmkey
  AND factVehicleSale.vehicleKey = x.VehicleKey
  AND factVehicleSale.approvedDateKey = x.approvedDateKey;
DELETE 
FROM factVehicleSale
WHERE stocknumber = 'DELETE';  


SELECT *
FROM factVEhicleInventory
WHERE stocknumber IN ('21214x','17078a','h6111a','h6752g')
-- need an inventory row for each of these
SELECT storecode, bmkey, vehiclekey, approveddatekey
-- SELECT *
FROM factVehicleSale
GROUP BY storecode, bmkey, vehiclekey, approveddatekey
HAVING COUNT(*) > 1
  
