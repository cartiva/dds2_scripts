used car acquisitions
  trade
    new car sale
    used car sale
  purchase
    auction
    street
    lease
    in/out
    back on
    intramarket ws
    purchase of rental/national
    purchase of owner/coach/...
    other

basic assumption: 
  arkona IS reality, tool IS decoration, except for shit LIKE intra market,  ...
  
  from the perspective of arkona, a vehicle has either been sold OR IS IN current inventory
  
of course, the challenge IS to mashup those 2 resources INTO one acquisition table  

SELECT *
FROM tmpAisDpsAcq 

SELECT *
FROM tmpAisArkAcq a
full OUTER JOIN tmpAisDpsAcq b on a.stocknumberNoY = b.stocknumberNoY
WHERE a.storecode IS NULL OR b.storecode IS NULL 




-- 8/2 some basic stats
-- stocknumber IN ark NOT dps
SELECT a.*
-- SELECT COUNT(*) -- 80
FROM tmpAisArkAcq a
LEFT JOIN tmpAisDpsAcq b on a.stocknumberNoY = b.stocknumberNoY
WHERE b.storecode IS NULL 
-- stocknumber IN ark NOT dps, of those which are IN dps BY vin
SELECT a.*, c.*
FROM tmpAisArkAcq a
LEFT JOIN tmpAisDpsAcq b on a.stocknumberNoY = b.stocknumberNoY
LEFT JOIN tmpAisDpsAcq c on a.vin = c.vin
WHERE b.storecode IS NULL 
ORDER BY a.vin

-- this givves me some low hanging fruit of stocknumber typo,
-- WHEN the stocknumber IS wrong IN the tool, EZ fix,
-- of course, it IS NOT always EZ to determine which IS correct
-- but IF the stocknumber IS wrong IN arkona, it can be for a couple of reasons
-- 1. 15679B (s/b 15879B), H7013GA: stk# IS wrong IN BOPTRAD, but IS correct IN BOPMAST
--      they simply changed the stocknumber IN arkona for the deal, but that did
--      NOT UPDATE BOPTRAD, so every time i download BOPTRAD, this one IS going
--      to be fucked up
-- 2. 21349, looks LIKE a nc sold AS a used car for no discernible reason
-- 3. fucking crookston, don't know what i am going to DO about these
-- 4. fucking intra market ws
SELECT *
FROM tmpAisDpsAcq a
WHERE vin IN ( -- stocknumber IN ark NOT dps
  SELECT a.vin
  FROM tmpAisArkAcq a
  LEFT JOIN tmpAisDpsAcq b on a.stocknumberNoY = b.stocknumberNoY
  WHERE b.storecode IS NULL)
  
  
-- stocknumber IN ark NOT dps
-- VehicleItems, AND most recent VehicleItemStatus
-- dup rows FROM o  
SELECT m.*, n.VehicleItemID, o.*
FROM (
  SELECT a.*
  FROM tmpAisArkAcq a
  LEFT JOIN tmpAisDpsAcq b on a.stocknumber = b.stocknumber
  WHERE b.storecode IS NULL) m
LEFT JOIN dps.VehicleItems n on m.vin = n.vin  
LEFT JOIN (  
  SELECT a.VehicleItemID, a.status, a.fromts
  FROM dps.VehicleItemStatuses a
  INNER JOIN (
    SELECT VehicleItemID, max(fromTS) AS fromTs
    FROM dps.VehicleItemStatuses
    GROUP BY VehicleItemID) b on a.VehicleItemID = b.VehicleItemID AND a.fromts = b.fromts) o on n.VehicleItemID = o.VehicleItemID 

-- arkAcq NOT IN dpsAcq BY stocknumber with VehicleItemStatus based on vin 
SELECT m.stocknumber, m.vin, m.dateAcq, m.acqCategory, n.VehicleItemID, 
  MAX(CASE WHEN status LIKE 'Inv%' THEN status END) AS invStatus, 
  MAX(CASE WHEN status NOT LIKE 'Inv%' THEN status END) AS evalStatus, 
  MAX(o.fromTS)
FROM (
  SELECT a.*
  FROM tmpAisArkAcq a
  LEFT JOIN tmpAisDpsAcq b on a.stocknumber = b.stocknumber
  WHERE b.storecode IS NULL) m
LEFT JOIN dps.VehicleItems n on m.vin = n.vin  
LEFT JOIN (  
  SELECT a.VehicleItemID, a.status, a.fromts
  FROM dps.VehicleItemStatuses a
  INNER JOIN (
    SELECT VehicleItemID, max(fromTS) AS fromTs
    FROM dps.VehicleItemStatuses
    GROUP BY VehicleItemID) b on a.VehicleItemID = b.VehicleItemID AND a.fromts = b.fromts) o on n.VehicleItemID = o.VehicleItemID 
GROUP BY m.stocknumber, m.vin, m.dateAcq, m.acqCategory, n.VehicleItemID  


-- the issue IS that dpsAcq prior to 2011
select *
FROM (
  SELECT a.*
  FROM tmpAisArkAcq a
  LEFT JOIN tmpAisDpsAcq b on a.stocknumber = b.stocknumber
  WHERE b.storecode IS NULL) m
LEFT JOIN dps.VehicleInventoryItems n on m.stocknumber = n.stocknumber
ORDER BY dateAcq


-- sales with no arkAcq based on stocknumber
SELECT b.thedate, a.stocknumber, d.*
FROM factVehicleSale a
INNER JOIN day b on a.originationDateKey = b.datekey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  AND c.vehicleType = 'used'
LEFT JOIN tmpAisArkAcq d on a.stocknumber = d.stocknumber
WHERE b.thedate > '12/31/2012'
  AND d.storecode IS NULL 

-- ALL current inventory have an arkAcq   
SELECT a.imstk#, b.*
FROM stgArkonaINPMAST a
LEFT JOIN tmpAisArkAcq b on a.imstk# = b.stocknumber
WHERE a.imstat = 'i'
  AND a.imtype = 'u'  
  AND b.storecode IS NULL 
  
SELECT *
FROM extArkonaBOPTRAD
WHERE vin = '1G1ZB5EB0AF278235'  

much LIKE the VehicleItems vs VehicleInventoryItems issue
acquisition vs inventory
how to tie a vehicle (vin) to a point IN time

-- after adding acqCategoryClassification
select *
-- SELECT COUNT(*)
FROM tmpAisArkAcq a
INNER JOIN tmpAisDpsAcq b on a.stocknumber = b.stocknumber
  AND a.acqCategory <> b.acqCategory
  
  
select *
FROM tmpAisDpsAcq a
LEFT JOIN tmpAisArkAcq b on a.stocknumber = b.stocknumber
WHERE a.acqCategoryClassification =  'InAndOut'  

SELECT acqCategory, acqCategoryClassification, COUNT(*)
FROM tmpAisArkAcq
GROUP BY acqCategory, acqCategoryClassification

SELECT acqCategory, acqCategoryClassification, COUNT(*)
FROM tmpAisDpsAcq
GROUP BY acqCategory, acqCategoryClassification


--8/19
-- stocknumber IN ark NOT IN dps
-- DO these stocknumbers exist IN bopmast OR inpmast?
--SELECT a.*, c.bmstk#, d.imstk#
SELECT a.storecode, a.stocknumber, a.vin, a.dateacq, a.acqCategory,
  a.acqCategoryClassification, c.bmstk#, d.imstk#
-- SELECT COUNT(*) -- only 75
FROM tmpAisArkAcq a
LEFT JOIN tmpAisDpsAcq b on a.stocknumberNoY = b.stocknumberNoY
LEFT JOIN stgArkonaBOPMAST c on a.stocknumber = c.bmstk#
LEFT JOIN stgArkonaINPMAST d on a.stocknumber = d.imstk#
WHERE b.storecode IS NULL 
ORDER BY a.dateacq DESC
