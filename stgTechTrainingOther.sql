DROP TABLE stgTechTrainingOther;
CREATE TABLE stgTechTrainingOther (
  theDate date,
  technumber cichar(3),
  storecode cichar(3),
  hours double(2),
  reason cichar(25)) IN database;

DELETE FROM stgTechTrainingOther;  
INSERT INTO stgTechTrainingOther
SELECT a.thedate, c.technumber, c.storecode, a.hours, a.reason
FROM stgTechclocktimeAdjustments a
LEFT JOIN edwEmployeeDim b ON a.employeekey = b.employeekey
LEFT JOIN dimtech c ON b.employeenumber = c.employeenumber
  AND a.thedate BETWEEN c.techkeyfromdate AND techkeythrudate
WHERE c.technumber IS NOT NULL   
  
SELECT * FROM (
SELECT a.thedate, c.technumber, a.hours, a.reason
FROM stgTechclocktimeAdjustments a
LEFT JOIN edwEmployeeDim b ON a.employeekey = b.employeekey
LEFT JOIN dimtech c ON b.employeenumber = c.employeenumber
  AND a.thedate BETWEEN c.techkeyfromdate AND techkeythrudate) a 
WHERE technumber IS NULL   



DECLARE @d1 date;
DECLARE @d2 date;
DECLARE @yearmonth integer;
@d1 = '12/01/2013';
@d2 = '12/13/2013';
SELECT b.technumber, b.name, 
  coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other,
  coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training
FROM (
  SELECT technumber, employeenumber, name
  FROM dimtech a
  WHERE a.flagdeptcode = 'MR'
    AND a.storecode = 'ry1'
    AND a.employeenumber <> 'na'
    AND a.active = true 
    AND a.techkeyfromdate < @d2
    AND a.techkeythrudate > @d1
  GROUP BY a.technumber, a.employeenumber, a.name) b 
LEFT JOIN stgTechTrainingOther c ON b.technumber = c.technumber  
  AND thedate BETWEEN @d1 AND @d2
GROUP BY b.technumber, b.name  
UNION
SELECT '666', 'zTotal', 
  SUM(coalesce(other, 0)) AS otherhours, SUM(coalesce(Training, 0)) AS traininghours
FROM (
  SELECT b.technumber, b.name, 
    coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
    coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
  FROM (
    SELECT technumber, employeenumber, name
    FROM dimtech a
    WHERE a.flagdeptcode = 'MR'
      AND a.storecode = 'ry1'
      AND a.employeenumber <> 'na'
      AND a.active = true 
      AND a.techkeyfromdate < @d2
      AND a.techkeythrudate > @d1
    GROUP BY a.technumber, a.employeenumber, a.name) b 
  LEFT JOIN stgTechTrainingOther c ON b.technumber = c.technumber  
    AND thedate BETWEEN @d1 AND @d2
  GROUP BY b.technumber, b.name) x  
ORDER BY b.name  

/*
SELECT *
FROM stgTechTrainingOther a
WHERE NOT EXISTS (
  SELECT 1
  FROM dimtech
  WHERE technumber = a.technumber
    AND storecode = a.storecode
    AND active = true
    AND a.thedate BETWEEN techkeyfromdate AND techkeythrudate)
*/  