alter function Utilities.LEVENSHTEIN( 
  @s cichar(50), 
  @t cichar(50) ) 
--Returns the Levenshtein Distance between strings s1 and s2.
--Original developer: Michael Gilleland   
-- http://www.merriampark.com/ld.htm
--Translated to TSQL by Joseph Gama


returns integer

BEGIN
/*

 The SQL Server STUFF function inserts a string into another string. It deletes a 
 specified length of characters in the first string at the start position 
 and then inserts the second string into the first string at the start position.
 STUFF ( character_expression , start , length , replaceWith_expression )
 
 ads equivalent IS INSERT(str1, start, length, str2 )
 
Returns a character string where length characters have been deleted from str1, 
beginning at start, 
and where str2 has been inserted into str1, beginning at start.

12/7/14 fails with  INSERT Starting position is after the end of the string

12/7/14 fails with  INSERT Starting position is after the end of the string

SELECT utilities.levenshtein('HUTZOL,SEAN M','HUTZOL-JOHNSON, SEAN M') FROM system.iota
*/
DECLARE @d cichar(100); 
DECLARE @LD integer; 
DECLARE @m integer; 
DECLARE @n integer; 
DECLARE @i integer; 
DECLARE @j integer;
DECLARE @s_i cichar(1); 
DECLARE @t_j cichar(1);
DECLARE @cost integer;
--Step 1
@n=length(@s);
@m=length(@t);
@d = repeat(CHAR(0),100);
If @n = 0 THEN 
  @LD = @m;
ELSE 
  IF @m = 0 THEN
    @LD = @n;
  ELSE
    --Step 2
    @i=0;
    WHILE @i<=@n DO
      @d = INSERT(@d,@i+1,1,CHAR(@i));--d(i, 0) = i
      @i=@i+1;
    END WHILE;
    
    @i=0;
    WHILE @i<=@m DO
      @d = INSERT(@d,@i*(@n+1)+1,1,CHAR(@i));--d(0, j) = j
      @i = @i+1;
    END WHILE;

    --Step 3
    @i=1;
    WHILE @i<=@n DO 
      @s_i=(substring(@s,@i,1));
    --Step 4
      @j=1;
      WHILE @j<=@m DO
        @t_j=(substring(@t,@j,1));
    --Step 5
        If @s_i = @t_j THEN 
    	  @cost=0;
    	ELSE
    	  @cost=1;
    	END IF;
    --Step 6
    -- here's the problem, dbo.min3
    -- which may be solvable IF ads functions can call functions
    /*
    CREATE FUNCTION fnMin3(@a int, @b int,  @c int )
    RETURNS int
    AS
    BEGIN
        DECLARE @m INT
        SET @m = @a
    
        IF @b < @m SET @m = @b
        IF @c < @m SET @m = @c
        
        RETURN @m
    END
    */
--    		@d=INSERT(@d,@j*(@n+1)+@i+1,1,CHAR(dbo.MIN3(ASCII(substring(@d,@j*(@n+1)+@i-1+1,1))+1,ASCII(substring(@d,(@j-1)*(@n+1)+@i+1,1))+1,ASCII(substring(@d,(@j-1)*(@n+1)+@i-1+1,1))+@cost)));
			
          @d = INSERT(@d,@j*(@n+1)+@i+1,1, CHAR((SELECT utilities.min3(ASCII(substring(@d,@j*(@n+1)+@i-1+1,1))+1, ASCII(substring(@d,(@j-1)*(@n+1)+@i+1,1))+1, ASCII(substring(@d,(@j-1)*(@n+1)+@i-1+1,1))+@cost) FROM system.iota)));
		  
		  
		  			
    	  @j=@j+1;
    	END WHILE; // @j<=@m
      @i=@i+1;
    END WHILE; // @i<=@n 
  END IF; // @LD = @n; 
END IF; //@m = 0 THEN
--Step 7
@LD = ASCII(substring(@d,@n*(@m+1)+@m+1,1));
RETURN @LD;
END

/*
-- original TSQL
Levenshtein Distance Algorithm: TSQL Implementation
by Joseph Gama
 
CREATE function LEVENSHTEIN( @s varchar(50), @t
varchar(50) ) 
--Returns the Levenshtein Distance between strings s1
and s2.
--Original developer: Michael Gilleland   
http://www.merriampark.com/ld.htm
--Translated to TSQL by Joseph Gama
returns varchar(50)
as
BEGIN
DECLARE @d varchar(100), @LD int, @m int, @n int, @i
int, @j int,
@s_i char(1), @t_j char(1),@cost int
--Step 1
SET @n=LEN(@s)
SET @m=LEN(@t)
SET @d=replicate(CHAR(0),100)
If @n = 0
	BEGIN
	SET @LD = @m
	GOTO done
	END
If @m = 0
	BEGIN
	SET @LD = @n
	GOTO done
	END
--Step 2
SET @i=0
WHILE @i<=@n
	BEGIN
	SET @d=STUFF(@d,@i+1,1,CHAR(@i))--d(i, 0) = i
	SET @i=@i+1
	END

SET @i=0
WHILE @i<=@m
	BEGIN
	SET @d=STUFF(@d,@i*(@n+1)+1,1,CHAR(@i))--d(0, j) = j
	SET @i=@i+1
	END
--goto done
--Step 3
	SET @i=1
	WHILE @i<=@n
		BEGIN
		SET @s_i=(substring(@s,@i,1))
--Step 4
	SET @j=1
	WHILE @j<=@m
		BEGIN
		SET @t_j=(substring(@t,@j,1))
		--Step 5
		If @s_i = @t_j
			SET @cost=0
		ELSE
			SET @cost=1
--Step 6
		SET @d=STUFF(@d,@j*(@n+1)+@i+1,1,CHAR(dbo.MIN3(
		ASCII(substring(@d,@j*(@n+1)+@i-1+1,1))+1,
		ASCII(substring(@d,(@j-1)*(@n+1)+@i+1,1))+1,
		ASCII(substring(@d,(@j-1)*(@n+1)+@i-1+1,1))+@cost)
		))
		SET @j=@j+1
		END
	SET @i=@i+1
	END      
--Step 7
SET @LD = ASCII(substring(@d,@n*(@m+1)+@m+1,1))
done:
--RETURN @LD
--I kept this code that can be used to display the
matrix with all calculated values
--From Query Analyser it provides a nice way to check
the algorithm in action
--
RETURN @LD
--declare @z varchar(255)
--set @z=''
--SET @i=0
--WHILE @i<=@n
--	BEGIN
--	SET @j=0
--	WHILE @j<=@m
--		BEGIN
--		set
@z=@z+CONVERT(char(3),ASCII(substring(@d,@i*(@m+1
)+@j+1 ,1)))
--		SET @j=@j+1 
--		END
--	SET @i=@i+1
--	END
--print dbo.wrap(@z,3*(@n+1))
END

*/