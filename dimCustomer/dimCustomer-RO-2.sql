--- CAVEAT EMPTOR -------------------------------------------------------------
very small (21) subset with dups, so exclude these  
start out with old CustomerKeyFromDates = 7/31/2009 (bncrtts = 1/1/0001)
custType <> '' AND fullname<> ''
inventory: bnkey = 0 
no match IN dimCustomer: (select customerkey from dimDumstomer where bnkey = 2)
Smartdraw: SPDRHDR
-- 2/13/14
factRepairOrder-dimCustomer
  BOPMAST uses cobuyer bmcbyr = 0 WHEN there IS none
  need to ADD an N/A row
  use bnkey = 2
--/ CAVEAT EMPTOR -------------------------------------------------------------
--- DDL -----------------------------------------------------------------------
-- structure accomodates Type 2 metadata, but for now, the TABLE will be Type 1
-- unique index ON bnkey AS the NK
DROP TABLE dimCustomer;
CREATE TABLE dimCustomer (
  CustomerKey autoinc,
  StoreCode cichar(3),
  BNKEY integer,
  CustomerTypeCode cichar(1),
  CustomerType cichar(12),
  FullName cichar(50),
  LastName cichar(50),
  FirstName cichar(15),
  MiddleName cichar(25),
  HomePhone cichar(12),
  BusinessPhone cichar(20),
  CellPhone cichar(12),
  Email cichar(60),
  City cichar(20),
  County cichar(20),
  State cichar(2),
  Zip cichar(9),
  CurrentRow logical,
  RowChangeDate date,
  RowChangeDateKey integer,
  RowFromTS timestamp,
  RowThruTS timestamp,
  RowChangeReason cichar(100),
  CustomerKeyFromDate date,
  CustomerKeyFromDateKey integer,
  CustomerKeyThruDate date,
  CustomerKeyThruDateKey integer) IN database;
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimCustomer','dimCustomer.adi','PK',
   'CustomerKey','',2051,512,'' ); 
-- index: NK   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimCustomer','dimCustomer.adi','NK',
   'BNKEY','',2051,512,'' );    
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimCustomer','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'dimCustomerObjectsfail');   
-- regular index  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimCustomer','dimCustomer.adi','FullName',
   'FullName','',2,512,'' );    
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','StoreCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','BNKEY', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CustomerKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CurrentRow', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );       
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CustomerKeyFromDate', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CustomerKeyThruDate', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CustomerKeyThruDateKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );     

DROP TABLE keyMapDimCustomer;     
CREATE TABLE keyMapDimCustomer (
  CustomerKey integer,
  StoreCode cichar(3),
  BNKEY integer) IN database;         
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'keyMapDimCustomer','keyMapDimCustomer.adi','PK',
   'CustomerKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('keyMapDimCustomer','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'keyMapDimCustomerfail');   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimCustomer','StoreCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimCustomer','BNKEY', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimCustomer','CustomerKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );
  
--RI  
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimCustomer-KeyMap');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimCustomer-KeyMap','dimCustomer','keyMapDimCustomer','PK',2,2,NULL,'','');  
     

DROP TABLE xfmDimCustomer; 
CREATE TABLE xfmDimCustomer(
  StoreCode cichar(3),
  BNKEY integer,
  CustomerTypeCode cichar(1),
  CustomerType cichar(12),
  FullName cichar(50),
  LastName cichar(50),
  FirstName cichar(15),
  MiddleName cichar(25),
  HomePhone cichar(12),
  BusinessPhone cichar(20),
  CellPhone cichar(12),
  Email cichar(60),
  City cichar(20),
  County cichar(20),
  State cichar(2),
  Zip cichar(9)) IN database;
-- index: NK unique
EXECUTE PROCEDURE sp_CreateIndex90( 
  'xfmDimCustomer','xfmDimCustomer.adi','NK','BNKEY','',2051,512,'' );   
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('xfmDimCustomer','Table_Primary_Key',
  'NK', 'APPEND_FAIL', 'dimCustomerObjectsfail');   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimCustomer','StoreCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimCustomer','BNKEY', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );          
--/ DDL -----------------------------------------------------------------------  

--- Initial Load  -------------------------------------------------------------
DELETE FROM dimCustomer;   
-- unknown 
INSERT INTO dimCustomer (StoreCode,BNKEY,CustomerTypeCode,CustomerType,
  FullName,LastName,FirstName,MiddleName,HomePhone,BusinessPhone,CellPhone,
  Email,City,County,State,Zip, CurrentRow, CustomerKeyFromDate, CustomerKeyFromDateKey,
  CustomerKeyThruDate, CustomerKeyThruDateKey)
values('RY1',1,'X','UNKNOWN', 'UNKNOWN','UNKNOWN','UNKNOWN','UNKNOWN','UNKNOWN','UNKNOWN','UNKNOWN',
  'UNKNOWN','UNKNOWN','UNKNOWN','UN','UNKNOWN',true,'07/31/2009',
  (SELECT datekey FROM day WHERE thedate = '07/31/2009'),
  '12/31/9999', 
  (SELECT datekey FROM day WHERE thedate = '12/31/9999'));
-- inventory 
INSERT INTO dimCustomer (StoreCode,BNKEY,CustomerTypeCode,CustomerType,
  FullName,CurrentRow, CustomerKeyFromDate, CustomerKeyFromDateKey,
  CustomerKeyThruDate, CustomerKeyThruDateKey)
values('RY1',0,'C','Company','INVENTORY', 
  true,'07/31/2009',
  (SELECT datekey FROM day WHERE thedate = '07/31/2009'),
  '12/31/9999', 
  (SELECT datekey FROM day WHERE thedate = '12/31/9999')); 
-- N/A
INSERT INTO dimCustomer (StoreCode,BNKEY,CustomerTypeCode,CustomerType,
  FullName,LastName,FirstName,MiddleName,HomePhone,BusinessPhone,CellPhone,
  Email,City,County,State,Zip, CurrentRow, CustomerKeyFromDate, CustomerKeyFromDateKey,
  CustomerKeyThruDate, CustomerKeyThruDateKey)
values('RY1',2,'X','N/A', 'N/A','N/A','N/A','N/A','N/A','N/A','N/A',
  'N/A','N/A','N/A','NA','N/A',true,'07/31/2009',
  (SELECT datekey FROM day WHERE thedate = '07/31/2009'),
  '12/31/9999', 
  (SELECT datekey FROM day WHERE thedate = '12/31/9999'), false);
INSERT INTO dimCustomer (StoreCode,BNKEY,CustomerTypeCode,CustomerType,
  FullName,LastName,FirstName,MiddleName,HomePhone,BusinessPhone,CellPhone,
  Email,City,County,State,Zip, CurrentRow, CustomerKeyFromDate, CustomerKeyFromDateKey,
  CustomerKeyThruDate, CustomerKeyThruDateKey)
SELECT bnco#,bnkey,bntype,
  CASE bntype
    WHEN 'C' THEN 'Company'
    WHEN 'I' THEN 'Person'
    ELSE 'Unknown'
  END,
  bnsnam,bnlnam, bnfnam,bnmidi,bnphon,bnbphn, 
  bncphon,bnemail,bncity,bncnty,bnstcd,bnzip, true,
  CAST(LEFT(bncrtts,10) AS sql_date),
  (SELECT datekey FROM day WHERE thedate = CAST(LEFT(bncrtts,10) AS sql_date)) AS BadColumn,
  (SELECT thedate FROM day WHERE datetype = 'NA'),
  (SELECT datekey FROM day WHERE datetype = 'NA')
-- SELECT COUNT(*)   
FROM stgArkonaBOPNAME a
WHERE bnco# IN ('RY1','RY2','RY3')
  AND NOT EXISTS (
    SELECT 1
    FROM (
      SELECT bnco#, bnkey
      FROM stgArkonaBOPNAME
      GROUP BY bnco#, bnkey HAVING COUNT(*) > 1) z  
    WHERE z.bnco# = a.bnco#
      AND z.bnkey = a.bnkey)
  AND bntype <> ''
  AND bnsnam <> '';   
  
UPDATE dimCustomer
SET CustomerKeyFromDate = '07/31/2009',
    CustomerKeyFromDateKey = (SELECT datekey FROM day WHERE thedate = '07/31/2009')
WHERE CustomerKeyFromDate = '01/01/0001'; 
-- nullable
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CustomerKeyFromDateKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   

INSERT INTO keyMapDimCustomer (CustomerKey,StoreCode,BNKEY)
SELECT CustomerKey,StoreCode,BNKEY
FROM dimCustomer a ; 

-- goofy "unknown" FROM BOPNAME
DELETE FROM keyMapDimCustomer WHERE bnkey = 297316;
DELETE FROM dimcustomer WHERE bnkey = 297316;
--/ Initial Load  -------------------------------------------------------------
--- Dependencies --------------------------------------------------------------
INSERT INTO zProcExecutables values('dimCustomer','daily',CAST('00:15:15' AS sql_time),5);
INSERT INTO zProcProcedures values('dimCustomer','BOPNAME',1,'Daily');
INSERT INTO DependencyObjects values('dimCustomer','BOPNAME','dimCustomer');
--/ Dependencies --------------------------------------------------------------

-- ADD HasValidEmail ----------------------------------------------------------
ALTER TABLE dimCustomer 
ADD COLUMN HasValidEmail logical;
ALTER TABLE xfmDimCustomer
ADD COLUMN HasValidEmail logical;

UPDATE dimCustomer
SET HasValidEmail = x.HasValidEmail
FROM (
  SELECT customerkey, email, 
    CASE 
      WHEN length(TRIM(coalesce(email, ''))) = 0 THEN False
  	  WHEN position('@' IN email) = 0 THEN False
      WHEN position('wng' IN email) > 0 THEN False
    	WHEN position('dnh' IN email) > 0 THEN False
    	WHEN position('noemail' IN email) > 0 THEN False
    	WHEN position('none' IN email) > 0 THEN False
    	WHEN position('dng' IN email) > 0 THEN False
    	ELSE True
    END AS HasValidEmail
  FROM dimcustomer) x
WHERE dimCustomer.CustomerKey = x.CustomerKey;

-- ADD HasValidEmail ----------------------------------------------------------