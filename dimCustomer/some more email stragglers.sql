SELECT *
FROM factRepairOrder
WHERE ro IN ('16148368','16148354')

SELECT *
FROM dimCustomer
WHERE customerkey IN (104960,170156)

SELECT *
FROM stgArkonaBOPNAME
WHERE bnkey IN (320426,1072857)

SELECT *
FROM stgArkonaBOPNAME 
WHERE bnsnam LIKE '%HANSON, BROOKE%' or bnsnam like '%HASBROUCK, DENNIS%'

-- those 2 are john tyrell's customers, they have valid email addresses IN arkona
-- for whatever reason, stgArkonaBOPNAME does NOT have those vales
-- populate the following TABLE with
/*
select bnkey, bnsnam, bnemail, bneml2
from rydedata.bopname
where bnemail <> '' or bneml2 <> ''

3/27 new arkona query to generate email addresses

select *
from (
  select bnkey, bnsnam, bnemail,
    CASE 
      WHEN length(TRIM(coalesce(bnemail, ''))) = 0 THEN 0
      WHEN position('@' IN bnemail) = 0 THEN 0
      WHEN position ('.' IN bnemail) = 0 THEN 0
      WHEN position('WNG' IN upper(bnemail)) > 0 THEN 0
      WHEN position('DNH' IN upper(bnemail)) > 0 THEN 0
      WHEN position('NOEMAIL' IN upper(bnemail)) > 0 THEN 0
      WHEN position('NONE' IN upper(bnemail)) > 0 THEN 0
      WHEN position('DNG' IN upper(bnemail)) > 0 THEN 0
      ELSE 1
    END as emailValid,
    bneml2,
    CASE 
      WHEN length(TRIM(coalesce(bneml2, ''))) = 0 THEN 0
      WHEN position('@' IN bneml2) = 0 THEN 0
      WHEN position ('.' IN bneml2) = 0 THEN 0
      WHEN position('WNG' IN upper(bneml2)) > 0 THEN 0
      WHEN position('DNH' IN upper(bneml2)) > 0 THEN 0
      WHEN position('NOEMAIL' IN upper(bneml2)) > 0 THEN 0
      WHEN position('NONE' IN upper(bneml2)) > 0 THEN 0
      WHEN position('DNG' IN upper(bneml2)) > 0 THEN 0    
      ELSE 1
    END as email2Valid  
  from rydedata.bopname
  where (bnemail <> '' or bneml2 <> '')) x
where emailValid = 1 or email2Valid = 1  
*/
-- FROM db2 ~38000 rows
DROP TABLE zEmailUpdateFromArkona;
CREATE TABLE zEmailUpdateFromArkona (
bnkey integer,
name cichar(50),
email cichar(60),
emailValid logical,
email2 cichar(60),
email2Valid logical) IN database

SELECT * FROM zEmailUpdateFromArkona

SELECT a.bnkey, a.fullname, a.email, a.email2, a.hasValidEmail, b.*
FROM dimCustomer a
LEFT JOIN zEmailUpdateFromArkona b on a.bnkey = b.bnkey
WHERE a.hasValidEmail = false
  AND b.bnkey IS NOT NULL
  
SELECT a.bnkey, a.fullname, left(a.email,25), left(a.email2,25), b.*
--SELECT COUNT(*)
FROM dimCustomer a
LEFT JOIN zEmailUpdateFromArkona b on a.bnkey = b.bnkey
WHERE a.hasValidEmail = false
  AND (b.emailValid = true OR b.email2Valid = true)
  
-- dimCustomer with false valid email joined with scrape FROM above
DROP TABLE #wtf;  
SELECT a.bnkey, a.fullname, a.email, a.email2, a.hasValidEmail, b.*
INTO #wtf
FROM dimCustomer a
LEFT JOIN zEmailUpdateFromArkona b on a.bnkey = b.bnkey
  AND (coalesce(a.email, 'x') <> coalesce(b.email,'x') OR coalesce(a.email2, 'x') <> coalesce(b.email2, 'x')) 
WHERE a.hasValidEmail = false
  AND b.bnkey IS NOT NULL
 
-- only 114 rows, but of course, johnny t's customers are IN the list  
SELECT bnkey, fullname, email_1, email2_1
FROM #wtf  
ORDER BY fullname

-- benchmark the change this will CREATE
SELECT COUNT(*) FROM dimCustomer WHERE hasValidEmail = true -- 34430, after the UPDATE: 34523

-- get the newly scraped values INTO dimCustomer
UPDATE dimCustomer
SET email = x.email_1,
    email2 = x.email2_1
FROM #wtf x
WHERE dimCustomer.bnkey = x.bnkey    

UPDATE dimCustomer
SET emailValid = 
  CASE 
    WHEN length(TRIM(coalesce(email, ''))) = 0 THEN False
	  WHEN position('@' IN email) = 0 THEN False
    WHEN position ('.' IN email) = 0 THEN False
    WHEN position('wng' IN email) > 0 THEN False
  	WHEN position('dnh' IN email) > 0 THEN False
  	WHEN position('noemail' IN email) > 0 THEN False
  	WHEN position('none' IN email) > 0 THEN False
  	WHEN position('dng' IN email) > 0 THEN False
  	ELSE True
  END
WHERE bnkey IN (
  SELECT bnkey
  FROM #wtf);

UPDATE dimcustomer
SET email2Valid = 
  CASE 
    WHEN length(TRIM(coalesce(email2, ''))) = 0 THEN False
	  WHEN position('@' IN email2) = 0 THEN False
    WHEN position ('.' IN email2) = 0 THEN False
    WHEN position('wng' IN email2) > 0 THEN False
  	WHEN position('dnh' IN email2) > 0 THEN False
  	WHEN position('noemail' IN email2) > 0 THEN False
  	WHEN position('none' IN email2) > 0 THEN False
  	WHEN position('dng' IN email2) > 0 THEN False
  	ELSE True
  END
WHERE bnkey IN (
  SELECT bnkey
  FROM #wtf);  
  
UPDATE dimCustomer
SET hasValidEmail = 
  CASE
    WHEN emailValid = true OR email2Valid = true THEN true
    WHEN emailValid = false AND email2Valid = false THEN false
  END
WHERE bnkey IN (
  SELECT bnkey
  FROM #wtf);     
  
  
-- 3/27
-- these are the rows IN dimCustomer that currently show false for hasValidEmail
-- LEFT joined to the latest scrape FROM arkona that include a VALID FUCKING EMAIL
SELECT a.bnkey, b.*
FROM dimCustomer a
LEFT JOIN zEmailUpdateFromArkona b on a.bnkey = b.bnkey
WHERE a.hasValidEmail = false
  AND (b.emailValid = true OR b.email2Valid = true)  
  
UPDATE dimCustomer
SET email = x.email,
    emailValid = x.emailValid,
    email2 = x.email2,
    email2Valid = x.email2Valid,
    hasValidEmail = true
FROM (  
  SELECT a.bnkey, b.*
  FROM dimCustomer a
  LEFT JOIN zEmailUpdateFromArkona b on a.bnkey = b.bnkey
  WHERE a.hasValidEmail = false
    AND (b.emailValid = true OR b.email2Valid = true)) x  
WHERE dimCustomer.bnkey = x.bnkey;  

-- 4/9
ADD this ALL to dimCustomer overnight scrape
after doing the basic dimCustomer (proc BOPNAME)
proc: emailUpdate

CREATE TABLE tmpDimCustomerEmailUpdate ( 
      bnkey Integer,
      name CIChar( 50 ),
      email CIChar( 60 ),
      emailValid Logical,
      email2 CIChar( 60 ),
      email2Valid Logical) IN DATABASE;
      
SELECT a.bnkey, b.*
FROM dimCustomer a
LEFT JOIN tmpDimCustomerEmailUpdate b on a.bnkey = b.bnkey
WHERE a.hasValidEmail = false
  AND (b.emailValid = true OR b.email2Valid = true);
  
UPDATE dimCustomer
SET email = x.email,
    emailValid = x.emailValid,
    email2 = x.email2,
    email2Valid = x.email2Valid,
    hasValidEmail = true
FROM (  
  SELECT a.bnkey, b.*
  FROM dimCustomer a
  LEFT JOIN tmpDimCustomerEmailUpdate b on a.bnkey = b.bnkey
  WHERE a.hasValidEmail = false
    AND (b.emailValid = true OR b.email2Valid = true)) x  
WHERE dimCustomer.bnkey = x.bnkey;    
