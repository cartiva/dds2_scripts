
SELECT employeekey, employeenumber, name, pydeptcode, pydeptcodedesc, c.code, c.codedesc
FROM edwEmployeeDim e
LEFT JOIN resarkonacodes c ON e.pydeptcode = c.code
  AND e.storecode = c.storecode
WHERE pydeptcodedesc <> codedesc

-- ok, went to res - CodesTypes.sql, zapped & reloaded pyclckctl IN resArkoknaCodes
-- but now need to UPDATE the descriptions IN edwEmployeeDim 
SELECT * FROM edwEmployeeDim 


UPDATE edwEmployeeDim
SET PyDeptCodeDesc = x.codeDesc
-- SELECT a.code, a.codedesc, e.name, e.pydeptcode, e.pydeptcodedesc
FROM resArkonaCodes a
LEFT JOIN edwEmployeeDim e ON a.storecode = e.storecode
  AND a.code = e.PyDeptCode
WHERE a.stgTableName = 'stgArkonaPYPCLKCTL'
  AND pydeptcodedesc <> codedesc
--  AND name LIKE '%OLSON%'