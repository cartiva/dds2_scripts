SELECT a.thedate, b.*
FROM day a
LEFT JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
  AND b.employeekey > 1
WHERE a.thedate BETWEEN '06/17/2012' AND curdate()  

SELECT c.name, SUM(coalesce(FlagHours, 0)) AS FlagHours
FROM day a
LEFT JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
--  AND b.employeekey > 1
LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
WHERE b.employeekey IN (1351,1337,1325, 1270, 1261, 1249, 1198, 1170)
  AND a.thedate BETWEEN '06/17/2012' AND curdate()  
GROUP BY c.name

-- the point of the exercise IS to drill down to the differences BETWEEN HolyGraille
-- AND these numbers


/* 1. factTechLineFlagDateHours ***********************************************/
-- to drill down to differences, i need the ros for which the tech has flag time
-- continuation of the conv FROM flaghours.sql
-- the factTechFlagHoursByRO
-- an ro can have more than one techgroupkey

-- 1 row per ro/tech w/flag hours
-- but the lowest grain IS ro/line/flagdate/tech w/flag hours
-- so i deleted factTechLineFlagDateHours but i am going to need it, IF nothing ELSE
-- than AS a source for aggregation


----------------------------------------------------------</  FROM flagHours.sql
-- IS this the path to flaghours per RO, i think so
-- finest grain: ro/line/flagdate/techgroupkey w/flaghours

-- the thing that keeps stopping me IS knowing exactly what the hours represent
-- it should be this:  ro/line/flagdate/techgroupkey w/flaghours

-- remember: generate the finest grain possible, aggregate from that

-- so why DO i need the techgroupkey ::because ro/line/tech/flagdate IS NOT unique
-- weight factor = line/tech/flagdate-hours/line-hours
-- DO i ever have more than one row per ro/line/tech/flagdate
-- absofuckinglutely, a tech can flag hours ON a line ON multiple days
-- so what are the fucking hours IN factTechLineFlagDateHours

-- need techgroupkey for RO, for Line, but DO i need it for flagdate?

-- 6/30
-- still mindfucking ON the hours IN factTechLineFlagDateHours
-- ok, they are line/tech/flagdate hours, but require techGroupKey/weightfactor to get the tech
-- fuck, what hours are they: duh, TABLE name: TechLineFlagDateHours

SELECT * FROM xfmroinitial
DROP TABLE #jon
-- exclude voids
SELECT a.*, b.lineflaghours, d.* 
INTO #jon
FROM ( -- this gives me line/tech/flagdate w/hours
  SELECT ptco#, ptro#, ptline, pttech, flagdate, SUM(ptlhrs) AS TechLineFlagDateHours
  -- INTO #wtf
  FROM xfmroinitial
    WHERE ptltyp = 'l'
    AND ptcode = 'tt'
    AND ptlhrs > 0
    AND status <> 'VOID'
  GROUP BY ptco#, ptro#, ptline, pttech, flagdate) a
LEFT JOIN FactROLine b ON a.ptco# = b.storecode
  AND a.ptro# = b.ro
  AND a.ptline = b.line
  AND b.LineFlagHours > 0
LEFT JOIN dimtech c ON a.ptco# = c.storecode
  and a.pttech = c.technumber
LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
  AND d.weightfactor = round(a.TechLineFlagDateHours/b.LineFlagHours, 2) 
WHERE a.LineTechFlagDateHours > 0
  AND b.LineFlagHours > 0
  
SELECT * FROM #jon  
WHERE ptro# IN (
  SELECT ptro#
  FROM #jon 
  GROUP BY ptco#, ptro#,ptline, pttech
  HAVING COUNT(*) > 1)

--DROP TABLE factTechLineFlagDateHours;  
CREATE TABLE factTechLineFlagDateHours (
  StoreCode CIChar( 3 ), 
  RO CIChar( 9 ), 
  Line Integer, 
  FlagDateKey Integer, 
  TechGroupKey Integer,
  FlagHours Double) IN database;  
  
-- DELETE FROM factTechLineFlagDateHours 
INSERT INTO factTechLineFlagDateHours
SELECT ptco#, ptro#, ptline,
  (SELECT datekey FROM day WHERE thedate = a.flagdate),
  TechGroupKey, TechLineFlagDateHours
--FROM #jon a; 
FROM ( -- this gives me line/tech/flagdate w/hours
  SELECT ptco#, ptro#, ptline, pttech, flagdate, SUM(ptlhrs) AS TechLineFlagDateHours
  -- INTO #wtf
  FROM xfmroinitial
    WHERE ptltyp = 'l'
    AND ptcode = 'tt'
    AND ptlhrs <> 0
    AND status <> 'VOID'
  GROUP BY ptco#, ptro#, ptline, pttech, flagdate) a
LEFT JOIN FactROLine b ON a.ptco# = b.storecode
  AND a.ptro# = b.ro
  AND a.ptline = b.line
  AND b.LineFlagHours > 0
LEFT JOIN dimtech c ON a.ptco# = c.storecode
  and a.pttech = c.technumber
LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
  AND d.weightfactor = round(a.TechLineFlagDateHours/b.LineFlagHours, 2) 
WHERE a.TechLineFlagDateHours > 0
  AND b.LineFlagHours > 0
  AND round(b.LineFlagHours, 2) >= round(a.TechLineFlagDateHours, 2)
  AND round(a.TechLineFlagDateHours/b.LineFlagHours, 2) > 0;  

execute procedure sp_ZapTable('factTechLineFlagDateHours')
  
select n/100.0
FROM tally
WHERE n < 101

-- ok, differences are because factROLine has been updated with xfmro, AND factTechLineFlagDateHours has NOT
-- after UPDATE, 2 differences
-- 16084596, 16084689
-- 5250 shows both have negative flag hours, adjustments
-- factROLine accounts for the adjustment
-- factTechLineFlagDateHours does NOT
-- that's because, IN each CASE the + hours are for one tech, the - hours for another 
-- NOT sure i understand this
-- FINALLY, no discrepancies
SELECT * FROM #jon
SELECT a.*, b.*, round(a.hours,2)-round(b.hours,2)
INTO #jon
FROM (
select storecode, ro, SUM(flaghours) AS hours
FROM factTechLineFlagDateHours
group by storecode, ro) a
LEFT JOIN (
SELECT storecode, ro, SUM(LineFlagHours) AS hours
FROM factroline
GROUP BY storecode, ro)b ON a.storecode = b.storecode
  AND a.ro = b.ro
where round(a.hours, 2) - round(b.hours, 2) <> 0

SELECT * FROM factTechLineFlagDateHours WHERE ro = '16004080'
SELECT * FROM xfmro WHERE ptro# = '16004080'
SELECT * FROM xfmroinitial WHERE ptro# = '16004080'

SELECT * FROM factTechLineFlagDateHours WHERE ro IN ('16084596','16084689')
SELECT * FROM xfmROInitial WHERE ptro# IN ('16084596','16084689')

-- nightly factTechLineFlagDateHours UPDATE
-- good enough
-- now does this go IN sp.stgRO
DELETE 
FROM factTechLineFlagDateHours
WHERE ro IN (
  SELECT a.ptro#
  FROM xfmro a);
INSERT INTO factTechLineFlagDateHours 
SELECT ptco#, ptro#, ptline,
  (SELECT datekey FROM day WHERE thedate = a.flagdate),
  TechGroupKey, TechLineFlagDateHours
--FROM #jon a; 
FROM ( -- this gives me line/tech/flagdate w/hours
  SELECT ptco#, ptro#, ptline, pttech, flagdate, SUM(ptlhrs) AS TechLineFlagDateHours
  -- INTO #wtf
  FROM xfmro
    WHERE ptltyp = 'l'
    AND ptcode = 'tt'
    AND ptlhrs <> 0
    AND flagdate IS NOT NULL -- some conversion stragglers
    AND status <> 'VOID'
  GROUP BY ptco#, ptro#, ptline, pttech, flagdate) a
LEFT JOIN FactROLine b ON a.ptco# = b.storecode
  AND a.ptro# = b.ro
  AND a.ptline = b.line
  AND b.LineFlagHours > 0
LEFT JOIN dimtech c ON a.ptco# = c.storecode
  and a.pttech = c.technumber
LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
  AND d.weightfactor = round(a.TechLineFlagDateHours/b.LineFlagHours, 2) 
WHERE a.TechLineFlagDateHours > 0
  AND b.LineFlagHours > 0; 
  
EXECUTE PROCEDURE sp_packtable('factTechLineFlagDateHours');  

----------------------------------------------------------/>  FROM flagHours.sql

/* 1. factTechLineFlagDateHours ***********************************************/

-- use this to get the service tech list
SELECT a.thedate, b.employeenumber, b.technumber, b.name, coalesce(b.flaghours, 0) AS flaghours, 
  coalesce(c.clockhours) AS clockhours,
  case when c.clockhours <> 0 then round(100*b.flaghours/c.clockhours, 2) ELSE 0 END AS prof
FROM (
  SELECT thedate, datekey
  FROM day
  WHERE thedate BETWEEN '04/01/2012' AND '04/30/2012') a,
  (
  SELECT f.ptco#, t.employeenumber, t.technumber, t.name, f.flagdatekey, round(SUM(f.linehours*g.weightfactor), 2) AS flaghours
  FROM zflaghours f
  INNER JOIN bridgeTechGroup g ON f.techgroupkey = g.techgroupkey
  INNER JOIN dimtech t ON g.techkey = t.techkey
  WHERE f.ptco# = 'ry1'
    AND t.employeenumber IS NOT NULL 
  GROUP BY f.ptco#, t.employeenumber, t.technumber, t.name, f.flagdatekey) b,
 (
    SELECT e.storecode, e.employeenumber, f.datekey, sum(f.ClockHours) AS clockhours 
    -- SELECT *
    FROM edwClockHoursFact f
    INNER JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
      AND e.storecode = 'ry1'
      AND e.distcode in ('STEC', 'SRVM') -- srvm: reed
    GROUP BY e.storecode, e.employeenumber, f.datekey) c
WHERE b.flagdatekey = a.datekey 
  AND c.datekey = a.datekey
  AND b.ptco# = c.storecode
  AND b.employeenumber = c.employeenumber
  AND b.employeenumber = '1106399'