-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
--  WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'integer,'
--	WHEN data_type = 'timestmp' THEN 'timestamp,'
--	WHEN data_type = 'smallint' THEN 'integer,'
--	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'integer,'
--	WHEN data_type = 'date' THEN 'date,'
--	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'GLPFAFD' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'GLPFAFD'  
AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'GLPFAFD'  
AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaPYHSHDTA
-- 3. revise DDL based ON step 2, CREATE TABLE
-- this one IS too fucking much for the spreadsheet, just go thru it here

-- 
CREATE TABLE stgArkonaGLPFAFD (
g2co#      cichar(3),          
g2rpt#     integer,            
g2lky1     integer,            
g2lky2     integer,            
g2lky3     integer,            
g2lky4     integer,            
g2lky5     integer,            
g2lky6     integer,            
g2lky7     integer,            
g2lky8     integer,            
g2lky9     integer,            
g2lky10    integer,            
g2lky11    integer,            
g2lky12    integer,            
g2lvl      cichar(1),          
g2desc     cichar(30),         
g2ltyp     cichar(1),          
g2sign     cichar(1),          
g2ttyp     cichar(1),          
g2pct      cichar(1),          
g2pbrk     cichar(1),          
g2lspa     integer,            
g2undl     cichar(1),          
g2bold     cichar(1),          
g2aunt     cichar(1),          
g2mfac     integer,            
g2addt     cichar(1)) IN database;            

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'GLPFAFD';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'GLPFAFD';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaGLPFAFD';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
