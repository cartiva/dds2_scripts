/*
-- based ON sdet scrape 6/16

-- xfmRO needs to include ALL the fields i anticipate needing
-- ADD ptcnam, ptckey, ptvin
6-17
this has become a reconstruction project
accumlating ALL the bits AND pieces to regenerate the data
xfmRO
factRO
factROLine
bridgeTechGroup - updated, NOT yet regenerated
keymapBridgeTechGroup
factTechFlagHours
*/
xfmROInitial

SELECT sh.ptco#, sh.ptro#, sh.ptfran, sd.ptline, sd.ptseq#, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, sd.ptsvctyp, sd.ptlpym,
  CASE WHEN ptltyp = 'A' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS LineDate, 
  CASE WHEN ptltyp = 'L' AND ptcode = 'TT' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS FlagDate,
  sh.ptdate AS OpenDate, sh.ptcdat AS CloseDate, sh.ptfcdt AS FinalCloseDate,
  sd.ptlamt, sd.ptlopc, sd.ptcrlo, 'SDET' AS source, sh.ptswid, 'Closed' AS status

SELECT COUNT(*) -- 1929847, 
FROM stgArkonaSDPRHDR sh
LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE length(trim(sh.ptro#)) > 6  -- exclude conversion data
  AND sd.ptline IS NOT null
  
-- ros with no lines, lot's of voids
-- need to include them, but limit to post conversion  
SELECT MIN(sh.ptdate), MAX(sh.ptdate) -- 3/24/04 6/14/12
SELECT *
FROM stgArkonaSDPRHDR sh  
LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE length(trim(sh.ptro#)) > 6  -- exclude conversion data
  AND sd.ptline IS NULL -- no line
  AND sh.ptdate > '07/25/2009' -- exclude conversion data
  
INSERT INTO xfmROInitial  
SELECT sh.ptco#, sh.ptro#, sh.ptfran, ptcnam, ptckey, ptvin, sd.ptline, sd.ptseq#, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, sd.ptsvctyp, sd.ptlpym,
  CASE WHEN ptltyp = 'A' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS LineDate, 
  CASE WHEN ptltyp = 'L' AND ptcode = 'TT' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS FlagDate,
  sh.ptdate AS OpenDate, sh.ptcdat AS CloseDate, sh.ptfcdt AS FinalCloseDate,
  sd.ptlamt, sd.ptlopc, sd.ptcrlo, 'SDET' AS source, sh.ptswid, 
  CASE  
    when ptcnam = '*VOIDED REPAIR ORDER*' then 'VOID'
    when ptchk# like 'v%' then 'VOID'
    else 'Closed'
  END AS status
FROM stgArkonaSDPRHDR sh  
LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE length(trim(sh.ptro#)) > 6  -- exclude conversion data
  AND sh.ptdate > '07/25/2009'; -- exclude conversion data  
  
INSERT INTO xfmROInitial
SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, ptcnam, ptckey, ptvin, pd.ptline, pd.ptseq#, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, 
--  min(pd.ptdate) AS LineDate, 
  min(CASE WHEN ptltyp = 'A' THEN pd.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS LineDate, 
  min(CASE WHEN ptltyp = 'L' AND ptcode = 'TT' THEN pd.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS FlagDate,
  max(ph.ptdate) AS OpenDate, '12/31/9999' AS CloseDate, '12/31/9999' AS FinalCloseDate,
  0 as ptlamt, pd.ptlopc,
  pd.ptcrlo, 'PDET' AS source, ph.ptswid, ph.ptrsts
FROM stgArkonaPDPPHDR ph
LEFT JOIN stgArkonaPDPPDET pd ON ph.ptco# = pd.ptco#
  AND ph.ptpkey = pd.ptpkey
WHERE ph.ptdtyp = 'RO' 
  AND ph.ptdoc# <> ''
  AND ph.ptco# IN ('RY1','RY2','RY3')
  AND pd.ptdate <> '12/31/9999' -- this could be done IN the scrape: ptdate <> 0 
  AND NOT EXISTS (
    SELECT 1
    FROM xfmROInitial
    WHERE ptco# = ph.ptco#
      AND ptro# = ph.ptdoc#
      AND ptline = pd.ptline)
GROUP BY ph.ptco#, ph.ptdoc#, ph.ptfran,  ptcnam, ptckey, ptvin,pd. ptline, pd.ptseq#, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptlopc, pd.ptcrlo, ph.ptswid, ph.ptrsts;
  
  
-- historical technumber replacement
-- only needs to be done ON initial load
--SELECT * FROM xfmROInitial WHERE pttech IN ('m17','21','521','28')
UPDATE xfmROInitial
SET pttech = '506'
WHERE pttech = 'm17'; 
UPDATE xfmROInitial
SET pttech = '22'
WHERE pttech = '21';
UPDATE xfmROInitial
SET pttech = '251'
WHERE pttech = '521';
UPDATE xfmROInitial
SET pttech = '17'
WHERE pttech = '28';  

DROP TABLE factRO;
CREATE TABLE factRO (
  StoreCode cichar(3),
  RO cichar(9),
  Franchise cichar(3),
  OpenDate date,
  CloseDate date,
  FinalCloseDate date,
  WriterID cichar(3),
  CustomerName cichar(30),
  CustomerKey integer,
  VIN cichar(17),
  Void logical);

INSERT INTO factRO -- 283394 rows
-- the dups are coming FROM multiple dates WHEN there is a pdet line of 900,950,990 
-- which have c/fcDate of 12/31/9999 
-- could eliminate BY taking dates out of GROUP, LIKE with opendate 
SELECT ptco#, ptro#, ptfran, opendate, MIN(closedate) AS CloseDate, 
  min(finalclosedate) AS FinalCloseDate,
  ptswid, ptcnam, ptckey, ptvin, 
  MAX(CASE WHEN status = 'VOID' THEN true ELSE false END)
FROM xfmROInitial  
GROUP BY ptco#, ptro#, ptfran, opendate, ptswid, ptcnam, ptckey, ptvin;
  
ALTER TABLE factRO
ADD COLUMN OpenDateKey integer
ADD COLUMN CloseDateKey integer
ADD COLUMN FinalCloseDateKey integer;

UPDATE factRO
SET opendatekey = (SELECT datekey FROM day WHERE thedate = factRO.opendate),
    closedatekey = (SELECT datekey FROM day WHERE thedate = factRO.closedate),
    finalclosedatekey = (SELECT datekey FROM day WHERE thedate = factRO.finalclosedate);
    
ALTER TABLE factRO
DROP COLUMN opendate
DROP COLUMN closedate
DROP COLUMN finalclosedate;     


SELECT *
FROM factRO
-- ok, got them all
SELECT *
FROM xfmROInitial a
WHERE NOT EXISTS (
  SELECT 1
  FROM factRO
  WHERE ro = a.ptro#)  
  
-- bingo
SELECT ro 
FROM factRO
GROUP BY storecode, ro
HAVING COUNT(*) > 1  

/*
CREATE TABLE factRO ( 
      StoreCode CIChar( 3 ),
      RO CIChar( 9 ),
      Franchise CIChar( 3 ),
      WriterID CIChar( 3 ),
      CustomerName CIChar( 30 ),
      CustomerKey Integer,
      VIN CIChar( 17 ),
      Void Logical,
      OpenDateKey Integer,
      CloseDateKey Integer,
      FinalCloseDateKey Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRO',
   'factRO.adi',
   'STORECODE',
   'StoreCode',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRO',
   'factRO.adi',
   'RO',
   'RO',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRO',
   'factRO.adi',
   'FRANCHISE',
   'Franchise',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRO',
   'factRO.adi',
   'WRITERID',
   'WriterID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRO',
   'factRO.adi',
   'VOID',
   'Void',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRO',
   'factRO.adi',
   'OPENDATEKEY',
   'OpenDateKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRO',
   'factRO.adi',
   'CLOSEDATEKEY',
   'CloseDateKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRO',
   'factRO.adi',
   'FINALCLOSEDATEKEY',
   'FinalCloseDateKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRO',
   'factRO.adi',
   'PK',
   'StoreCode;RO',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'factRO', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'factROfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'factRO', 
   'Table_Primary_Key', 
   'PK', 'APPEND_FAIL', 'factROfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'factRO', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'factROfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'factRO', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'factROfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'factRO', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'factROfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'StoreCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'RO', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'Franchise', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'WriterID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'CustomerName', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'CustomerKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'VIN', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'Void', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'OpenDateKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'CloseDateKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRO', 
      'FinalCloseDateKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'factROfail' ); 

*/

-- UPDATE BridgeTechGroup  
INSERT INTO BridgeTechGroup (TechKey, WeightFactor)  
--SELECT a.storecode, a.ro, a.line, a.lineflaghours, b.pttech, b.flagdate, b.techlinehours,
--  round(b.techlinehours/a.lineflaghours, 2) AS weight
SELECT DISTINCT c.techkey, round(b.techlinehours/a.lineflaghours, 2)  
FROM factroline a
LEFT JOIN (
    SELECT ptco#, ptro#, ptline, pttech, FlagDate, SUM(ptlhrs) AS TechLineHours
    from xfmROInitial
    WHERE ptltyp = 'L'
      AND ptcode = 'TT'
      AND ptlhrs <> 0
      GROUP BY ptco#, ptro#, ptline, pttech, FlagDate) b ON a.storecode = b.ptco#
  AND a.ro = b.ptro#
  AND a.line = b.ptline
LEFT JOIN dimtech c ON b.ptco# = c.storecode 
  AND b.pttech = c.technumber   
LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
  AND round(b.techlinehours/a.lineflaghours, 2) = d.weightfactor   
WHERE a.lineflaghours > 0
  AND b.techlinehours > 0
  AND d.techkey IS NULL  -- for update 

-- UPDATE keymapBridgeTechGroup
-- wahoo, it fucking works
    TRY
      INSERT INTO keymapBridgeTechGroup
      SELECT TechGroupKey, TechKey, WeightFactor
      FROM BridgeTechGroup a
      WHERE NOT EXISTS (
        SELECT 1
        FROM keymapBridgeTechGroup
        WHERE TechKey = a.TechKey
          AND WeightFactor = a.WeightFactor);
    CATCH ALL
      RAISE SPxfmFactRO1(202, 'insert into keyMapBridgeTechGroup'); 
    END TRY;    
    IF ( -- enforce a mandatory one to one
      SELECT COUNT(*)
        FROM keymapBridgeTechGroup a
        full JOIN BridgeTechGroup b ON a.techkey = b.techkey
          AND a.weightfactor = b.weightfactor
        WHERE (
          a.techkey IS NULL OR 
          b.techkey IS NULL)) <> 0 THEN 
      RAISE SPxfmDimTech(203, 'keymapDimTech discrepancy'); 
    END IF;

-- now factTechFlagHour  

-- don't need total line hours ON each row, but the flag hours for that date
-- so the attribute becomes ROLineTechFlagDateHours
-- weight IS  lineTechFlagDateHours/LineHours
-- duh, which IS really techFlagDateHours
--DROP TABLE #wtf
SELECT ptco#, ptro#, ptline, pttech, flagdate, SUM(ptlhrs) AS ROLineFlagDateHours
INTO #wtf
FROM xfmroinitial
  WHERE ptltyp = 'l'
  AND ptcode = 'tt'
  AND ptlhrs > 0
GROUP BY ptco#, ptro#, ptline, pttech, flagdate;

--DROP TABLE #jon
SELECT a.*, b.lineflaghours, d.* 
INTO #jon
FROM #wtf a 
LEFT JOIN FactROLine b ON a.ptco# = b.storecode
  AND a.ptro# = b.ro
  AND a.ptline = b.line
  AND b.LineFlagHours > 0
LEFT JOIN dimtech c ON a.ptco# = c.storecode
  and a.pttech = c.technumber
LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
  AND d.weightfactor = round(a.ROLineFlagDateHours/b.LineFlagHours, 2) 
WHERE a.ROLineFlagDateHours > 0
  AND b.LineFlagHours > 0;
--WHERE a.ptro# = '16083330'

-- this IS actually techgroup flag hours
-- use this to generate true tech flag hours
CREATE TABLE factTechFlagHours (
-- change this factTechLineFlagDateHours
-- to factTechLineFlagDateHours
  StoreCode CIChar( 3 ), 
  RO CIChar( 9 ), 
  Line Integer, 
  FlagDateKey Integer, 
  TechGroupKey Integer,
  FlagHours Double) IN database;
  
  
-- bingo
SELECT *
FROM #jon
WHERE weightfactor IS NULL 
-- wafuckinghoo
SELECT ptco#, ptro#, ptline, flagdate, techgroupkey
FROM #jon
GROUP BY ptco#, ptro#, ptline, flagdate, techgroupkey
HAVING COUNT(*) > 1

INSERT INTO factTechLineFlagDateHours
SELECT ptco#, ptro#, ptline,
  (SELECT datekey FROM day WHERE thedate = a.flagdate),
  TechGroupKey, ROLineFlagDateHours
FROM #jon a




-- factTechLineFlagDateHours  
-- use this to generate true tech flag hours
-- which would look LIKE
-- hmm factTechFlagHours: include an employeekey, the correct one could be determined based ON the date
DROP TABLE factTechFlagHours;
CREATE TABLE factTechFlagHours (
  TechKey integer,
  EmployeeKey integer,
  FlagDateKey integer,
  FlagHours double) IN database;

-- DROP TABLE #xxx  
SELECT techkey, 
  (SELECT datekey FROM day WHERE thedate = z.flagdate) AS FlagDateKey, Hours
INTO #xxx
FROM (  
SELECT a.techkey, b.technumber, b.name, a.flagdate, round(SUM(a.ROLineFlagDateHours), 2) AS Hours
FROM #jon a
LEFT JOIN dimtech b ON a.ptco# = b.storecode
  AND a.techkey = b.techkey
GROUP BY a.techkey, a.flagdate, b.technumber, b.name) z

-- should be the same
SELECT flagdate, pttech, round(sum(ptlhrs), 2) AS ptlhrs
FROM xfmROInitial
WHERE ptltyp = 'L'
  AND ptcode = 'TT'
  AND ptlhrs > 0
GROUP BY flagdate, pttech  

-- hmm factTechFlagHours: include an employeekey, the correct one could be determined based ON the date
-- which would simplify joining to clockhours, got the datekey, got the emplkey
-- cool
-- problem IS non contiguous datekeys
-- need to SELECT the employeekey based ON date range, NOT datekeyrange
-- CAN NOT DO RANGES BASED ON DATEKEY

INSERT INTO factTechFlagHours  
select a.techkey, coalesce(c.EmployeeKey, -1), a.flagdatekey, a.hours
from #xxx a
LEFT JOIN day z ON a.flagdatekey = z.datekey
LEFT JOIN dimtech b ON a.techkey = b.techkey
LEFT JOIN edwEmployeeDim c ON b.employeenumber = c.employeenumber
  AND z.thedate BETWEEN c.EmployeeKeyFromDate AND c.EmployeeKeyThruDate

SELECT techkey, flagdatekey
FROM factTechFlagHours
GROUP BY techkey, flagdatekey
HAVING COUNT(*) > 1

SELECT employeekey, flagdatekey
FROM factTechFlagHours
WHERE employeekey > 0
GROUP BY employeekey, flagdatekey
HAVING COUNT(*) > 1


select a.thedate, b.*
FROM day a, factTechFlagHours b
WHERE a.thedate BETWEEN curdate()-21 AND curdate()-18
  AND b.employeekey > 0
  AND a.datekey = b.flagdatekey
  
SELECT a.thedate, a.dayname, b.techkey, d.technumber, d.name, coalesce(b.FlagHours, 0) AS flaghours, 
  coalesce(c.ClockHours, 0) AS clockhours
FROM day a
inner JOIN factTechFlagHours b ON a.datekey = b.flagdatekey
  AND b.employeekey > 0
inner JOIN edwClockHoursFact c ON a.datekey = c.datekey
  AND b.employeekey = c.employeekey  
inner JOIN dimTech d ON b.techkey = d.techkey 
  AND (d.technumber LIKE '5%' OR d.technumber LIKE '6%')
  AND length(technumber) = 3
  AND storecode = 'RY1'
WHERE a.thedate BETWEEN curdate()-21 AND curdate()-18



  
SELECT a.thedate, a.dayname, round(sum(coalesce(b.FlagHours, 0)), 2) AS flaghours, 
  round(sum(coalesce(c.ClockHours, 0)), 2) AS clockhours, 
  round(sum(coalesce(b.FlagHours, 0))/sum(coalesce(c.ClockHours, 0)), 2) AS prod
FROM day a
inner JOIN factTechFlagHours b ON a.datekey = b.flagdatekey
  AND b.employeekey > 0
inner JOIN edwClockHoursFact c ON a.datekey = c.datekey
  AND b.employeekey = c.employeekey  
inner JOIN dimTech d ON b.techkey = d.techkey 
  AND (d.technumber LIKE '5%' OR d.technumber LIKE '6%')
  AND length(technumber) = 3
  AND storecode = 'RY1'
WHERE a.thedate BETWEEN curdate()-61 AND curdate()-18
GROUP BY a.thedate, a.dayname
ORDER BY dayname


  
SELECT d.name, min(a.thedate) AS fromdate, max(a.thedate) as thrudate, round(sum(coalesce(b.FlagHours, 0)), 2) AS flaghours, 
  round(sum(coalesce(c.ClockHours, 0)), 2) AS clockhours, 
  round(sum(coalesce(b.FlagHours, 0))/sum(coalesce(c.ClockHours, 0)), 2) AS prod
FROM day a
inner JOIN factTechFlagHours b ON a.datekey = b.flagdatekey
  AND b.employeekey > 0
inner JOIN edwClockHoursFact c ON a.datekey = c.datekey
  AND b.employeekey = c.employeekey  
inner JOIN dimTech d ON b.techkey = d.techkey 
  AND (d.technumber LIKE '5%' OR d.technumber LIKE '6%')
  AND length(technumber) = 3
  AND storecode = 'RY1'
WHERE a.thedate BETWEEN curdate()- 151 AND curdate()-18
GROUP BY d.name, a.sundaytosaturdayweek

  
-- 529  
SELECT *
FROM dimtech
WHERE name = 'THOMPSON,WYATT'  

SELECT *
FROM factTechFlagHours
WHERE TechKey = 55

SELECT a.datekey, thedate, dayname, coalesce(b.flaghours, 0) AS flaghours, 
  coalesce(d.clockhours, 0) AS clockhours
FROM day a
LEFT JOIN factTechFlagHours b ON a.datekey = b.flagdatekey
  AND techkey = 55
LEFT JOIN dimtech c ON b.techkey = c.techkey  
LEFT JOIN edwClockHoursFact d ON a.datekey = d.datekey  
  AND b.employeekey = d.employeekey
WHERE thedate BETWEEN curdate() - 74 AND curdate() - 14
  
  
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
      WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
      WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
      WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
      WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
      WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17 
      WHEN TheDate >= CurDate() - 125 AND TheDate <= CurDate() - 119 THEN 18 
      WHEN TheDate >= CurDate() - 132 AND TheDate <= CurDate() - 120 THEN 19 
      WHEN TheDate >= CurDate() - 139 AND TheDate <= CurDate() - 127 THEN 20 
      WHEN TheDate >= CurDate() - 146 AND TheDate <= CurDate() - 134 THEN 21 
    END AS TheWeek
  FROM day d
  WHERE TheDate >= CurDate() - 146
  AND TheDate <= CurDate()  
  
  previous 2weeks for each date
  so for curdate() (6/18/12) i want 4th = 17th
  
  SELECT datekey, thedate, dayname,
    CASE
      WHEN thedate BETWEEN curdate() - 14 AND curdate() - 1 THEN 1
      WHEN thedate BETWEEN curdate() - 28 AND curdate() - 15 THEN 2
      WHEN thedate BETWEEN curdate() - 42 AND curdate() - 29 THEN 3
    end
      
  FROM day
  WHERE thedate BETWEEN curdate() - 42 AND curdate()
  
  SELECT datekey, thedate, dayname, thedate - 14 AS BeginPrev14 , thedate - 1 as EndPrev14
  FROM day a
  WHERE thedate between '05/01/2012' AND '05/31/2012'
  
  
SELECT d.name, min(a.thedate) AS fromdate, max(a.thedate) as thrudate, round(sum(coalesce(b.FlagHours, 0)), 2) AS flaghours, 
  round(sum(coalesce(c.ClockHours, 0)), 2) AS clockhours, 
  round(sum(coalesce(b.FlagHours, 0))/sum(coalesce(c.ClockHours, 0)), 2) AS prod
SELECT *
-- FROM day a
FROM (
  SELECT datekey, thedate, dayname, thedate - 14 AS BeginPrev14 , thedate - 1 as EndPrev14,
    (SELECT datekey FROM day WHERE thedate = a.thedate - 14) AS BegKey,
    (SELECT datekey FROM day WHERE thedate = a.thedate - 1) AS EndKey
  FROM day a
  WHERE thedate = '05/15/2012') a
--inner JOIN factTechFlagHours b ON a.datekey = b.flagdatekey
--  AND b.employeekey > 0
INNER JOIN factTechFlaghoursByDay b ON b.flagdatekey IN (SELECT datekey FROM day WHERE thedate BETWEEN a.BeginPrev14 and a.EndPrev14)
  AND b.employeekey = 903 
inner JOIN edwClockHoursFact c ON c.datekey  IN (SELECT datekey FROM day WHERE thedate BETWEEN a.BeginPrev14 and a.EndPrev14)
  AND b.employeekey = c.employeekey  
  AND c.employeekey = 903
inner JOIN dimTech d ON b.techkey = d.techkey 
  WHERE d.techkey = 55
GROUP BY d.name, a.sundaytosaturdayweek

fred van heste
empkey = 903
empl# = '1144265'
techkey = 57
tech# = '572'

5/15
SELECT *
FROM factTechFlagHours
WHERE techkey = 57
  AND flagdatekey IN (
    SELECT datekey
    FROM day
    WHERE thedate IN (
      SELECT
      
SELECT a.thedate
FROM day a

WHERE a.thedate = '05/15/2012'  


SELECT a.thedate, b.*
FROM day a, factTechFlagHours b, edwClockHoursFact c
WHERE a.thedate = '05/15/2012'
  AND b.techkey = 57 
  AND b.flagdatekey IN (
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '05/01/2012' AND '05/14/2012')
  AND c.employeekey = 903
  AND c.datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '05/01/2012' AND '05/14/2012')  

  
SELECT employeekey, SUM(clockhours)
FROM edwClockHoursFact
WHERE employeekey = 903  
  AND datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '05/01/2012' AND '05/14/2012')
GROUP BY employeekey    

SELECT techkey, SUM(flaghours)
FROM factTechFlagHoursbyDay
WHERE techkey = 57 
  AND flagdatekey IN (
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '05/01/2012' AND '05/14/2012')
GROUP BY techkey      

SELECT a.thedate, b.*
FROM day a,
(
  SELECT employeekey, SUM(clockhours)
  FROM edwClockHoursFact
  WHERE employeekey = 903  
    AND datekey IN (
      SELECT datekey
      FROM day
      WHERE thedate BETWEEN a.thedate - 14 AND a.thedate - 1)
  GROUP BY employeekey) b
WHERE thedate = '05/15/2012'

-- this works
-- don't know how to generalize it
bohm tk 68 ek 1325
westerhausen tk 61 ek 882
stryker tk 51 ek 1501
SELECT thedate, 100*round(flaghours/clockhours, 2)
FROM (
  SELECT a.thedate,
    (SELECT SUM(clockhours) 
      FROM edwClockHoursFact
      WHERE employeekey = 882
        AND datekey IN (
          SELECT datekey
          FROM day
          WHERE thedate BETWEEN a.thedate - 14 AND a.thedate - 1)
      GROUP BY employeekey) AS clockhours,
    (SELECT SUM(flaghours) 
    FROM factTechFlagHoursByDay
    WHERE techkey = 61
      AND flagdatekey IN (
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN a.thedate - 14 AND a.thedate - 1)
    GROUP BY techkey) AS flaghours      
  FROM day a
  WHERE a.thedate between '05/01/2012' AND '05/31/2012') a

  


