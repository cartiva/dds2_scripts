/*
1/9/13 the al berry (ry3) problem
al has come back to WORK at ry1, hence rehire/reuse, because, of course, he gets the 
same employeenumber AS before (113550)

/*
12/27
alyssa huot, rehired for the 3rd time
issues: 
  hiredate: payroll accomodates only 2 hire dates
  ek FROM & thru dates: script currently have those commented out IN the UPDATE of the old record
    which IS part of the general issue WHERE i have NOT figured out the semantics of 
    ALL those dates, what IS the term of employment, what does the validity of 
    the empkey
  which of course IS causing clockhourstoday to bomb because her emp# has 2 empkeys
  IN the same from/thru range
  FUUUUUUUCK
  going to manually change rows
  
  but IS an employeekey valid thru just the END of the employment stint, OR until replaced
  seems LIKE i have been leaning toward until replaced
  
  back to alyssa
  rehired and started ON the 22nd
  prev:
  curr:
  
  SELECT * FROM day WHERE thedate BETWEEN '12/21/2012' AND '12/28/2012'
  
SELECT employeenumber
FROM edwEmployeeDim
WHERE '12/27/2012' BETWEEN employeekeyfromdate AND employeekeythrudate
GROUP BY employeenumber
HAVING COUNT(*) > 1  

-- this possibly gives me a better view of the history
SELECT b.yearmonth, employeekey, COUNT(*)
FROM edwClockHoursFact a
LEFT JOIN day b ON a.datekey = b.datekey
WHERE employeekey IN (1168,1592,1770,1856)
  AND clockhours <> 0 
GROUP BY b.yearmonth, employeekey  

actual WORK history
         - 8/12/11
5/15/12  - 8/26/12
12/22/12 -          

1/17/14
  got 2 rehire/reuse
  limit #Rehire to one at a time, use top 1

1/28/14
  ADD keyMap AND pretty name  
*/

DECLARE @nowts timestamp;
@nowts = (SELECT Now() FROM system.iota);
BEGIN TRANSACTION;       
// Rehire/Reuse: type 2 changes
    TRY 
//      SELECT Utilities.DropTablesIfExist('#Rehire') FROM system.iota;
      -- DROP TABLE #Rehire;
      SELECT top 1 edwED.EmployeeKey, x.*	
      INTO #Rehire 
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE edwED.ActiveCode = 'T' 
        AND x.ActiveCode IN ('P','A')
        AND edwED.lastname <> 'Lear'
      ORDER BY edwED.employeeKey;
/*
UPDATE to previous empkey
currentrow = false
rowchangereason, rowchangedate, rowfromts, rowthruts
*/    
      IF (SELECT COUNT(*) FROM #Rehire) > 0 THEN 
    -- UPDATE the old record
        UPDATE edwEmployeeDim 
        SET RowChangeDate = CAST(@NowTS AS sql_date),
            RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date)),
            RowThruTS = @NowTS,
            CurrentRow = false,
            RowChangeReason = 'Rehire/Reuse',
-- see fb CASE 170            
--decide now - DO you want to UPDATE the ekthrudate ON the prev employeekey            
            EmployeeKeyThruDate = cast(timestampadd(sql_tsi_day, -1, x.HireDate) AS sql_date),
            EmployeeKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = cast(timestampadd(sql_tsi_day, -1, x.HireDate) AS sql_date))
        -- SELECT *    
        FROM #Rehire x
        LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
          AND x.employeeNumber = e.EmployeeNumber
          AND e.CurrentRow = true;
    -- INSERT new record        
        INSERT INTO edwEmployeeDim(StoreCode, Store, EmployeeNumber, 
            ActiveCode, Active, FullPartTimeCode, FullPartTime,
            PyDeptCode, PyDept, DistCode, Distribution, Name, BirthDate, 
            BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
            RowFromTS, CurrentRow, EmployeeKeyFromDate, EmployeeKeyFromDateKey,
            EmployeeKeyThruDate, EmployeeKeyThruDateKey,
            PayrollClassCode, PayrollClass, Salary, HourlyRate,
            PayPeriodCode, PayPeriod) 
        SELECT StoreCode, Store, EmployeeNumber, 
            ActiveCode, Active, FullPartTimeCode, FullPartTime,
            PyDeptCode, PyDept, DistCode, Distribution,  Name, BirthDate, 
            BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
            @NowTS, TRUE,
            HireDate AS EKFrom, HireDateKey AS EKFromKey,
            (SELECT thedate FROM day WHERE datetype = 'NA'),
            (SELECT datekey FROM day WHERE datetype = 'NA'),
            PayrollClassCode, PayrollClass, Salary, HourlyRate,
            PayPeriodCode, PayPeriod                     
        FROM #Rehire; 
      END IF; // IF (SELECT COUNT(*) FROM #Rehire) > 0		
      
-- pretty name 
    UPDATE edwEmployeeDim
    SET LastName = trim(zz.LastName),
        FirstName = trim(zz.FirstName),
        MiddleName = trim(zz.MiddleName)    
    FROM (
      SELECT name, lastname, LEFT(first,1) + lcase(substring(first, 2, length(first))) AS firstname, middlename
      FROM (
        SELECT name, lastname,
          LEFT(firstandmiddle, position(' ' IN firstandmiddle)) AS first,
          substring(firstandmiddle, position(' ' IN firstandmiddle), length(firstandmiddle)) AS middlename
        FROM (    
          SELECT distinct name, lastname,
              trim(replace(replace(trim(name), trim(lastname), ''),',','')) AS FirstAndMiddle
          FROM (
            SELECT distinct name, 
              LEFT(trim(LEFT(name, position(',' IN name) - 1)), 1) + 
                lcase(substring(trim(LEFT(name, position(',' IN name) - 1)),2,length(trim(LEFT(name, position(',' IN name) - 1))))) AS lastname
            FROM (
              SELECT *
              FROM edwEmployeeDim
              WHERE lastname IS NULL  
                AND position(',' IN name) <> 0
                AND name NOT IN ('AHMED,AHMED O','JAMA,JAMA O','MOHAMED, MOHAMED M')) w) x) y) z) zz   
    WHERE edwEmployeeDim.name = zz.name
      AND edwEmployeeDim.lastname IS NULL;      
      
// keyMap
    INSERT INTO keyMapDimEmp      
    SELECT employeekey, storecode, employeenumber, coalesce(fullparttimecode, 'X'), 
      coalesce(pydeptcode, 'N/A'), coalesce(pydept, 'N/A'), 
      distcode, name, coalesce(RowFromTS, CAST('12/31/9999 00:00:01' AS sql_timestamp)), 
      payrollclasscode, salary, hourlyrate, payperiodcode
    FROM edwEmployeeDim a
    WHERE NOT EXISTS (
      SELECT 1 
      FROM keyMapDimEmp
      WHERE employeekey = a.employeekey);    
        
COMMIT WORK;       
    CATCH ALL
      ROLLBACK WORK;
      RAISE SPxfmEmpDIM(555, 'Rehire Reuse');	  
	  END TRY;       
    
    
