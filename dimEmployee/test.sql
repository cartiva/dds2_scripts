

DECLARE @store string;
DECLARE @department string;

@store = 'RY2';
@department = 'PDQ';

IF @department = 'Service' THEN 
SELECT dealername, censusdept,
  cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  CASE -- divide BY 0 prevention
    WHEN SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) = 0 THEN '0%'
    ELSE 
    trim(CAST( 
      -- Promoters
      SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
        100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
        THEN 1 ELSE 0 END) 
      -    
      -- Detractors
      SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
        THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
        THEN 1 ELSE 0 END) AS sql_char)) + '%' 
   END AS NetPromoterScore
FROM (
  SELECT DealerName, vin, ronumber, TransactionType, 
    MAX(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
    MAX(ReferralLikelihood) AS ReferralLikelihood, c.censusdept
  FROM xfmDigitalAirStrike a
  LEFT JOIN dds.factRepairOrder b on a.ronumber = b.ro
  LEFT JOIN dds.dimServiceWriter c on b.ServiceWriterKey = c.ServiceWriterKey
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90  
    AND TransactionType = @department
    AND Dealername = 
    CASE
      WHEN @store = 'RY1' THEN 'Rydell Chevrolet Buick GMC Cadillac'
      WHEN @store = 'RY2' THEN 'Honda of Grand Forks'
    END    
  GROUP BY DealerName, vin, ronumber, TransactionType, c.censusdept) x   
GROUP BY dealername, censusdept; 
ENDIF;

IF @department IN ('Main Shop', 'PDQ', 'Body Shop') THEN 
  SELECT dealername, censusdept,
    cast(COUNT(*) AS sql_char) AS Sent,
    cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
    trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
    CASE -- divide BY 0 prevention
      WHEN SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) = 0 THEN '0%'
      ELSE 
      trim(CAST( 
        -- Promoters
        SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
          100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
          THEN 1 ELSE 0 END) 
        -    
        -- Detractors
        SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
          THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
          THEN 1 ELSE 0 END) AS sql_char)) + '%' 
     END AS NetPromoterScore
  FROM (
    SELECT DealerName, vin, ronumber, TransactionType, 
      MAX(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
      MAX(ReferralLikelihood) AS ReferralLikelihood, c.censusdept
    FROM xfmDigitalAirStrike a
    LEFT JOIN dds.factRepairOrder b on a.ronumber = b.ro
    LEFT JOIN dds.dimServiceWriter c on b.ServiceWriterKey = c.ServiceWriterKey
    WHERE CAST(surveysentts AS sql_date) > curdate() - 90  
      AND TransactionType = @department
      AND Dealername = 
      CASE
        WHEN @store = 'RY1' THEN 'Rydell Chevrolet Buick GMC Cadillac'
        WHEN @store = 'RY2' THEN 'Honda of Grand Forks'
      END    
    GROUP BY DealerName, vin, ronumber, TransactionType, c.censusdept) x   
  -- what about AM, XX  
  WHERE CensusDept =
    CASE @department
      WHEN 'Main Shop' THEN 'MR'
      WHEN 'PDQ' THEN 'QL'
      WHEN 'Body Shop' THEN 'BS'
    END
  GROUP BY dealername, censusdept;
ENDIF;