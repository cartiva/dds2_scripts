SELECT employeenumber, name, b.yrmgrn,
  (SELECT name FROM edwEmployeeDim WHERE employeenumber = b.yrmgrn)
FROM edwEmployeeDim a
LEFT JOIN stgarkonapyprhead b ON a.employeenumber = b.yrmgrn


SELECT a.yrempn, b.*, c.*
FROM stgArkonaPYPRHEAD a
LEFT JOIN (
  SELECT employeenumber, name
  FROM edwEmployeeDim
  WHERE currentrow = true) b ON a.yrmgrn = b.employeenumber
LEFT JOIN (
  SELECT active, storecode, employeenumber, name, distribution, pydept
  FROM edwEmployeeDim
  WHERE currentrow = true
    AND active = 'Active') c ON a.yrempn = c.employeenumber  
    
SELECT storecode, active, employeenumber, name, yrempn, yrmgrn
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b ON a.employeenumber = b.yrempn
WHERE currentrow = true

SELECT storecode, employeenumber
FROM (
SELECT storecode, active, employeenumber, name, yrempn, yrmgrn
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b ON a.employeenumber = b.yrempn
  AND a.storecode = b.yrco#
WHERE currentrow = true) a
GROUP BY storecode, employeenumber HAVING COUNT(*) > 1

SELECT storecode, active, a.employeenumber, a.name, yrempn, yrmgrn, c.name AS mgr
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b ON a.employeenumber = b.yrempn
  AND a.storecode = b.yrco#
LEFT JOIN (
  SELECT name, employeenumber
  FROM edwEmployeeDim
  WHERE currentrow = true) c ON b.yrmgrn = c.employeenumber 
WHERE currentrow = true
  AND yrmgrn <> ''
  
SELECT employeenumber
FROM (
SELECT storecode, active, a.employeenumber, a.name, yrempn, yrmgrn, c.name AS mgr
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b ON a.employeenumber = b.yrempn
  AND a.storecode = b.yrco#
LEFT JOIN (
  SELECT name, employeenumber
  FROM edwEmployeeDim
  WHERE currentrow = true) c ON b.yrmgrn = c.employeenumber 
WHERE currentrow = true
  AND yrmgrn <> '') x
GROUP BY employeenumber HAVING COUNT(*) > 1    



    SELECT ymco# AS StoreCode,
      store.codeDesc AS Store, ymempn AS EmployeeNumber, p.ymactv AS ActiveCode, 
      CASE 
        WHEN p.ymactv = 'A' THEN 'Active'
        WHEN p.ymactv = 'P' THEN 'Active'
        WHEN p.ymactv = 'T' THEN 'Term'
      END AS Active, 
      p.ymactv AS FullPartTimeCode,  
      CASE 
        WHEN p.ymactv = 'A' THEN 'Full'
        WHEN p.ymactv = 'P' THEN 'Part'
      END AS FullPartTime,      
      pc.yicdept AS PyDeptCode, pc.yicdesc AS PyDept,
      ymdist AS DistCode, d.ytadsc AS Distribution, ymname AS Name, 
      ymbdte AS BirthDate, bd.DateKey AS BirthDateKey, ymhdte AS HireDate, 
      hd.DateKey AS HireDateKey, ymtdte AS TermDate, td.DateKey AS TermDateKey,
      ymclas AS PayrollClassCode, 
      CASE
        WHEN p.ymclas = 'H' THEN 'Hourly' 
        WHEN p.ymclas = 'S' THEN 'Salary'
        WHEN p.ymclas = 'C' THEN 'Commission'
      END AS DescrPayrollClass, ymsaly AS Salary, ymrate AS HourlyRate,
      ympper AS PayPeriodCode, 
      CASE ympper
        WHEN 'B' THEN 'Bi-Weekly'
        WHEN 'S' THEN 'Semi-Monthly'
      END as PayPeriod,
    (select ymname from stgArkonaPYMAST where ymco# = e.yrco# AND ymempn = e.yrmgrn) AS mgr      
    FROM stgArkonaPYMAST p
    LEFT JOIN day bd ON p.ymbdte = bd.thedate
    LEFT JOIN day hd ON p.ymhdte = hd.thedate
    LEFT JOIN day td ON p.ymtdte = td.thedate
    LEFT JOIN resArkonaCodes store ON store.stgTableName = 'stgArkonaPYMAST'
      AND store.stgColumnName = 'ymco#'
      AND store.storeCode = 'All'
      AND p.ymco# = store.code
    LEFT JOIN stgArkonaPYPCLKCTL pc ON p.ymco# = pc.yicco# // pydept
      AND p.ymdept = pc. yicdept
      AND pc.CurrentRow = true
    LEFT JOIN ( -- 1 row per store/code/DESC distcode
        SELECT ytaco#, ytadic, ytadsc
        FROM stgArkonaPYACTGR
        WHERE CurrentRow = true
        GROUP BY ytaco#, ytadic, ytadsc) d 
      ON p.ymco# = d.ytaco# 
      AND p.ymdist = d.ytadic
    LEFT JOIN stgArkonaPYPRHEAD e ON p.ymempn = e.yrempn
      AND p.ymco# = e.yrco#
    
/* 
1. UPDATE current edwEmployeeDim 
   2 tables xfmEmployeeDim & edwEmployeeDim
   
SELECT * FROM xfmEmployeeDim   

ALTER TABLE xfmEmployeeDim
ADD COLUMN ManagerName cichar(25);

ALTER TABLE edwEmployeeDim 
ADD COLUMN ManagerName cichar(25);

UPDATE edwEmployeeDim
SET managername = ''

simply ran sp.xfmEmpDIM to update

   
   
  
  