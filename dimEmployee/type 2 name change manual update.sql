/* IS there a type 2 name change to process?
  -- DROP TABLE #type2;
      SELECT a.*, edwED.EmployeeKey, 'Name' AS rowChangeReason
      INTO #type2
      FROM xfmEmployeeDim a
      LEFT JOIN edwEmployeeDim edwED ON a.StoreCode = edwED.StoreCode
        AND a.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE a.Name <> edwED.name 
        AND edwED.ActiveCode <> 'T'; -- exclude reuse records that could be picked up BY name OR dept
   SELECT * FROM #type2;
*/  
DECLARE @nowTS timestamp;
@nowTS = (SELECT now() FROM system.iota);     
BEGIN TRANSACTION;
TRY 
  -- UPDATE the old record   
      UPDATE edwEmployeeDim
      SET RowChangeDate = CAST(@NowTS AS sql_date),
          RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date)),
          RowThruTS = @NowTS,
          CurrentRow = false, 
          RowChangeReason = x.RowChangeReason,
          EmployeeKeyThruDate = CAST(timestampadd(sql_tsi_day, -1, @NowTS) AS sql_date),
          EmployeeKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST(timestampadd(sql_tsi_day, -1, @NowTS) AS sql_date))
      -- SELECT *   
      FROM #Type2 x     
      LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
        AND x.employeeNumber = e.EmployeeNumber
        AND e.CurrentRow = true;          
  -- INSERT new record        
      INSERT INTO edwEmployeeDim(StoreCode, Store, EmployeeNumber, 
          ActiveCode, Active, FullPartTimeCode, FullPartTime,
          PyDeptCode, PyDept, DistCode, Distribution, Name, BirthDate,  
          BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
          RowFromTS, CurrentRow, EmployeeKeyFromDate, EmployeeKeyFromDateKey,
          EmployeeKeyThruDate, EmployeeKeyThruDateKey, 
          PayrollClassCode, PayrollClass, Salary, HourlyRate,
          PayPeriodCode, PayPeriod) 
      SELECT StoreCode, Store, EmployeeNumber, 
          ActiveCode, Active, FullPartTimeCode, FullPartTime,
          PyDeptCode, PyDept, DistCode, Distribution, Name, BirthDate, 
          BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
          @NowTS, TRUE,     
          CAST(@NowTS AS sql_date),
          (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date)),
          (SELECT thedate FROM day WHERE datetype = 'NA'),
          (SELECT datekey FROM day WHERE datetype = 'NA'),
          PayrollClassCode, PayrollClass, Salary, HourlyRate,
          PayPeriodCode, PayPeriod           
      FROM #Type2 x;    
-- pretty name 
    UPDATE edwEmployeeDim
    SET LastName = trim(zz.LastName),
        FirstName = trim(zz.FirstName),
        MiddleName = trim(zz.MiddleName)    
    FROM (
      SELECT name, lastname, LEFT(first,1) + lcase(substring(first, 2, length(first))) AS firstname, middlename
      FROM (
        SELECT name, lastname,
          LEFT(firstandmiddle, position(' ' IN firstandmiddle)) AS first,
          substring(firstandmiddle, position(' ' IN firstandmiddle), length(firstandmiddle)) AS middlename
        FROM (    
          SELECT distinct name, lastname,
              trim(replace(replace(trim(name), trim(lastname), ''),',','')) AS FirstAndMiddle
          FROM (
            SELECT distinct name, 
              LEFT(trim(LEFT(name, position(',' IN name) - 1)), 1) + 
                lcase(substring(trim(LEFT(name, position(',' IN name) - 1)),2,length(trim(LEFT(name, position(',' IN name) - 1))))) AS lastname
            FROM (
              SELECT *
              FROM edwEmployeeDim
              WHERE lastname IS NULL  
                AND position(',' IN name) <> 0
                AND name NOT IN ('AHMED,AHMED O','JAMA,JAMA O','MOHAMED, MOHAMED M')) w) x) y) z) zz   
    WHERE edwEmployeeDim.name = zz.name
      AND edwEmployeeDim.lastname IS NULL;
                
// keyMap
    INSERT INTO keyMapDimEmp      
    SELECT employeekey, storecode, employeenumber, coalesce(fullparttimecode, 'X'), 
      coalesce(pydeptcode, 'N/A'), coalesce(pydept, 'N/A'), 
      distcode, name, coalesce(RowFromTS, CAST('12/31/9999 00:00:01' AS sql_timestamp)), 
      payrollclasscode, salary, hourlyrate, payperiodcode
    FROM edwEmployeeDim a
    WHERE NOT EXISTS (
      SELECT 1 
      FROM keyMapDimEmp
      WHERE employeekey = a.employeekey);
COMMIT WORK;  	  
CATCH ALL
  ROLLBACK;
  RAISE SPxfmEmpDIM(999, __errtext);   
END TRY;
