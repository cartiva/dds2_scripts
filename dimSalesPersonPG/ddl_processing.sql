﻿drop table if exists dds.xfm_sales_person;
create table if not exists dds.xfm_sales_person (
  row_type citext not null,
  store_code citext not null,
  sales_person_id citext not null,
  sales_person_type_code citext not null,
  employee_number citext default 'N/A',
  active boolean not null,
  run_date date not null,
  constraint xfm_sales_person_pk primary key (store_code,sales_person_id,run_date));
  
  
drop table if exists dds.dim_sales_person;
create table if not exists dds.dim_sales_person (
  sales_person_key serial primary key,
  store_code citext not null,
  sales_person_id citext not null,
  sales_person_type_code citext not null,
  sales_person_type citext not null, 
  employee_number citext default 'N/A',
  active boolean,
  constraint dim_sales_person_nk unique(store_code,sales_person_id));

unknown row: 1 for each store/type combination

select sales_person_type, count(*) from dds.ext_bopslsp group by sales_person_type
BDC Reresentative: 'B';44
Sales Manager: 'M';21
Desk Manager: 'D';2
Closer: 'C';1
Sales Representative: 'S';259
F&I Manager: 'F';35


select a.company_number, a.sales_person_id, a.sales_person_type, 
  coalesce(a.active, 'Y')::boolean, employee_number, firstname, lastname
from dds.ext_bopslsp a
left join dds.edwemployeedim b on a.employee_number = b.employeenumber
  and b.currentrow = true
where coalesce(a.active, 'Y')::boolean
  and company_number in ('RY1','RY2')
  and sales_person_type = 'S'
 order by employee_number 


 