
DECLARE @cur1 CURSOR AS 
  SELECT storecode, ro, line
  FROM xfmDimCorCodeGroup2
  WHERE storecode = 'ry1' 
    AND ro = '16001003'
    AND line = 1
  GROUP BY storecode, ro, line
  ORDER BY storecode, ro, line;
    
DECLARE @cur2 CURSOR AS 
  SELECT corcodekey
  FROM xfmDimCorCodeGroup2
  WHERE storecode = @storecode
    AND ro = @ro
    AND line = @line
  ORDER BY corcodekey;
DECLARE @str string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @corcodekey string;  
OPEN @cur1;
TRY
  WHILE FETCH @cur1 DO
    @storecode = @cur1.storecode;
    @ro = @cur1.ro;
    @line = @cur1.line;
    OPEN @cur2;
    TRY
      @str = '';
      WHILE FETCH @cur2 DO
        @corcodekey = TRIM(CAST(@cur2.corcodekey AS sql_char));
        @str = @str + @corcodekey + '|';  
      END WHILE;
      INSERT INTO xfmDimCorCodeGroup3 values(@storecode, @ro, @line, @str);
      @str = '';
    FINALLY
      CLOSE @cur2;
    END TRY;
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;  