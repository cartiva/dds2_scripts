-- DROP TABLE #flag;
-- DROP TABLE #clock;
-- 1 row for each day/empkey
-- empkey FROM factTechFlagHoursByDay
SELECT c.thedate, c.employeekey, coalesce(d.flaghours, 0) AS flaghours
INTO #flag
FROM (
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM factTechFlagHoursByDay
    WHERE employeekey > 0) b
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate()) c
LEFT JOIN factTechFlagHoursByDay d ON c.datekey = d.flagdatekey
  AND c.employeekey = d.employeekey
  
-- 1 row for each day/empkey WHERE empkey has ever flagged hours
SELECT c.thedate, c.employeekey, coalesce(d.clockhours, 0) AS clockhours   
--INTO #clock
FROM (
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM edwClockHoursFact 
    WHERE employeekey IN (
      SELECT DISTINCT employeekey
      FROM factTechFlagHoursByDay 
      WHERE employeekey > 0)) b
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate()) c
LEFT JOIN edwClockHoursFact d ON c.datekey = d.datekey
  AND c.employeekey = d.employeekey;   
  
-- ry2 shop
  SELECT *
    FROM (
      SELECT thedate, SUM(flaghours) AS flaghours
      FROM #flag
      WHERE employeekey IN (
        SELECT employeekey
        FROM edwEmployeeDim f 
        LEFT JOIN dimTech g ON f.employeenumber = f.employeenumber  
        WHERE f.technumber IS NOT NULL
          AND (f.distcode IN (/*'STEC','SRVM'*/'TECH'))--OR z.employeenumber = '167580') 
          AND f.storecode = 'ry2') 
      GROUP BY thedate) h     
    WHERE flaghours <> 0) a  
    
-- techs
SELECT a.employeekey, a.name, b.technumber
-- SELECT COUNT(*) -- 36, ADD flag 32, ADD clock 32
FROM edwEmployeeDim a
LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
WHERE a.storecode = 'ry2'
  AND a.distcode = 'TECH' 
  AND b.storecode IS NOT NULL  
  AND a.employeekey IN (
    SELECT employeekey
    FROM #flag)  
  AND a.employeekey IN (
    SELECT employeekey
    FROM #clock)    
    
SELECT employeekey, round(SUM(flaghours), 2) AS flaghours
FROM #flag   
GROUP BY employeekey

SELECT employeekey, round(SUM(clockhours), 2) AS clockhours
FROM #clock   
GROUP BY employeekey

-- any ry2 tech (with a name) w/flag hours IN the last 120 days
SELECT a.employeekey, a.name, b.technumber, c.flaghours, d.clockhours
FROM edwEmployeeDim a
LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
LEFT JOIN ( -- total flag hours BY empkey
  SELECT employeekey, round(SUM(flaghours), 2) AS flaghours
  FROM #flag   
  GROUP BY employeekey) c ON a.employeekey = c.employeekey
LEFT JOIN ( -- total clock hours BY empkey 
  SELECT employeekey, round(SUM(clockhours), 2) AS clockhours
  FROM #clock   
  GROUP BY employeekey) d ON a.employeekey = d.employeekey 
WHERE a.storecode = 'ry2'
  AND a.distcode = 'TECH' 
  AND b.storecode IS NOT NULL  
  AND a.employeekey IN (
    SELECT employeekey
    FROM #flag)  
  AND a.employeekey IN (
    SELECT employeekey
    FROM #clock)  
  AND c.flaghours + d.clockhours <> 0

-- ahh, this gives me totals BY tech/day
-- SELECT employeekey, thedate FROM #wtf GROUP BY employeekey, thedate HAVING COUNT(*) > 1  
SELECT *
--INTO #wtf
FROM (
  SELECT a.employeekey, a.name, b.technumber
  FROM edwEmployeeDim a
  LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
  WHERE a.storecode = 'ry2'
    AND a.distcode = 'TECH' 
    AND b.storecode IS NOT NULL) t
LEFT JOIN ( 
  SELECT *
  FROM (
    SELECT s.thedate, employeekey, SUM(flaghours) AS prev14flag, SUM(clockhours) AS prev14clock
    FROM (
      SELECT thedate, thedate - 14 as prev14Begin, 
        thedate - 1 AS prev14End
      FROM day 
      WHERE thedate BETWEEN curdate() - 120 and curdate()) s
    LEFT JOIN (  
      SELECT a.thedate, a.employeekey, a.flaghours, b.clockhours
      FROM #flag a
      LEFT JOIN #clock b ON a.thedate = b.thedate
        AND a.employeekey = b.employeekey) t ON t.thedate BETWEEN s.prev14begin AND s.prev14end
    GROUP BY s.thedate, employeekey) h) u ON t.employeekey = u.employeekey  
WHERE prev14flag + prev14clock <> 0  

SELECT * FROM #wtf
-- shop
SELECT thedate, sum(prev14flag) AS flag, SUM(prev14clock) AS clock, 
  round(sum(prev14flag)/SUM(prev14clock), 2) * 100 AS prod
FROM #wtf
GROUP BY thedate

-- ADD tech productivity
SELECT thedate, max(name) AS name, round(sum(prev14flag)/SUM(prev14clock), 2) * 100 AS prod  
FROM #wtf
WHERE thedate BETWEEN '06/01/2012' AND '06/30/2012'
GROUP BY employeekey, thedate
HAVING SUM(prev14clock) > 0 -- the pat kliner fix: 15 days of no clocktime
-- pat kliner IS an issue
-- flag ON 6/15 & 6/16, but prev14 clock = 0
SELECT *
FROM (
SELECT thedate, name, sum(prev14flag) AS flag, SUM(prev14clock) AS clock 
FROM #wtf
WHERE thedate BETWEEN '06/01/2012' AND '06/30/2012'
GROUP BY name, thedate) x 
WHERE clock = 0

SELECT b.thedate, a.*
FROM factTechFlagHoursByDay a
LEFT JOIN day b ON a.flagdatekey = b.datekey
WHERE a.employeekey = 791
-- no clockhours BETWEEN 06/01 AND 06/15
-- it's legit, checked against pypclockin
SELECT b.thedate, a.*
FROM edwClockHoursFact a
LEFT JOIN day b ON a.datekey = b.datekey
WHERE a.employeekey = 791
  AND a.clockhours > 0
  AND b.thedate > '06/01/2012'
ORDER BY b.thedate  


-- 7/17
-- pdq
SELECT c.thedate, c.employeekey, coalesce(d.flaghours, 0) AS flaghours
--INTO #flag
FROM (
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM factTechFlagHoursByDay
    WHERE employeekey < 0) b
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate()) c
LEFT JOIN factTechFlagHoursByDay d ON c.datekey = d.flagdatekey
  AND c.employeekey = d.employeekey
  
SELECT * FROM dimtech WHERE technumber IN ('619','135') AND storecode = 'ry1'

SELECT *
FROM edwEmployeeDim
WHERE distcode = 'WTEC'

-- 1 row for each day/empkey WHERE distcode = WTEC
-- DROP TABLE #clock;
SELECT c.thedate, sum(coalesce(d.clockhours, 0)) AS clockhours   
--INTO #clock
FROM (
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM edwClockHoursFact 
    WHERE employeekey IN (
      SELECT DISTINCT employeekey
      FROM edwEmployeeDim  
      WHERE distcode = 'WTEC')) b
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1) c
LEFT JOIN edwClockHoursFact d ON c.datekey = d.datekey
  AND c.employeekey = d.employeekey   
GROUP BY c.thedate  


SELECT d.thedate, round(SUM(coalesce(e.flag, 0)), 2) AS flag, -- cum flag FROM prev
  round(sum(coalesce(e.flag, 0))/sum(coalesce(f.clock, 0)), 2) * 100 AS prod 
FROM day d
LEFT JOIN (
  SELECT a.thedate, SUM(coalesce(b.flaghours, 0)) AS flag
  FROM day a
  LEFT JOIN  factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1
    AND b.techkey IN (
      SELECT techkey
      FROM dimtech
      WHERE technumber IN ('619','135'))
  GROUP BY a.thedate) e ON e.thedate BETWEEN d.thedate - 14 AND d.thedate - 1
LEFT JOIN ( 
  SELECT a.thedate, SUM(coalesce(b.clockhours, 0)) AS clock
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1
    AND b.employeekey IN (
      SELECT employeekey 
      FROM edwEmployeeDim 
      WHERE distcode = 'WTEC')
  GROUP BY a.thedate) f ON f.thedate BETWEEN d.thedate - 14 AND d.thedate - 1 
WHERE d.thedate BETWEEN curdate() - 113 AND curdate() - 1  
GROUP BY d.thedate    

-- this gives dailyflag, dailyclock & cum prod (cum range SET BY JOIN BETWEEN day AND e/f
-- for a dept (detail, IN this CASE) SET BY tech# for flaghours AND distcode for clockhours
SELECT d.thedate, coalesce(max(g.flag), 0) AS dailyflag, 
  coalesce(max(h.clock), 0) AS dailyclock,
  round(sum(coalesce(e.flag, 0))/sum(coalesce(f.clock, 0)), 2) * 100 AS prod 
FROM day d
LEFT JOIN (
  SELECT a.thedate, SUM(coalesce(b.flaghours, 0)) AS flag
  FROM day a
  LEFT JOIN  factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1
    AND b.techkey IN (
      SELECT techkey
      FROM dimtech
      WHERE technumber IN ('619','135'))
  GROUP BY a.thedate) e ON e.thedate BETWEEN d.thedate - 3 AND d.thedate - 1
LEFT JOIN ( 
  SELECT a.thedate, SUM(coalesce(b.clockhours, 0)) AS clock
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1
    AND b.employeekey IN (
      SELECT employeekey 
      FROM edwEmployeeDim 
      WHERE distcode = 'WTEC')
  GROUP BY a.thedate) f ON f.thedate BETWEEN d.thedate - 3 AND d.thedate - 1 
LEFT JOIN (
  SELECT a.thedate, SUM(coalesce(b.flaghours, 0)) AS flag
  FROM day a
  LEFT JOIN  factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1
    AND b.techkey IN (
      SELECT techkey
      FROM dimtech
      WHERE technumber IN ('619','135'))
  GROUP BY a.thedate) g ON d.thedate = g.thedate
LEFT JOIN ( 
  SELECT a.thedate, SUM(coalesce(b.clockhours, 0)) AS clock
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1
    AND b.employeekey IN (
      SELECT employeekey 
      FROM edwEmployeeDim 
      WHERE distcode = 'WTEC')
  GROUP BY a.thedate) h ON d.thedate = h.thedate
WHERE d.thedate BETWEEN curdate() - 113 AND curdate() - 1  
GROUP BY d.thedate 


-- Detail productivity for June
SELECT round(a.flag/b.clock, 2) * 100
FROM (-- flag hours for June
  SELECT SUM(coalesce(b.flaghours, 0)) AS flag
  FROM day a
  LEFT JOIN  factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
  WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
    AND b.techkey IN (
      SELECT techkey
      FROM dimtech
      WHERE technumber IN ('619','135'))) a,
  (-- clock hours for June      
  SELECT SUM(coalesce(b.clockhours, 0)) AS clock
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
    AND b.employeekey IN (
      SELECT employeekey 
      FROM edwEmployeeDim 
      WHERE distcode = 'WTEC')) b    

-- ben actually wants to monitor opened ros vs closed ros BY day      
SELECT ptsvctyp, COUNT(*)
FROM xfmro
WHERE ptro# IN (
  SELECT distinct ptro#
  FROM xfmro
  WHERE pttech IN ('619','135')
    AND ptco# = 'RY1')
GROUP BY ptsvctyp        

SELECT * FROM xfmro ORDER BY opendate

SELECT *
FROM xfmro
WHERE ptro# IN (
  SELECT ptro#
  FROM xfmro
  WHERE ptsvctyp = 'RE')

-- ros with flagtime for 619, 135
SELECT c.ro
FROM factTechLineFlagDateHours c
INNER JOIN (
  SELECT techgroupkey 
  FROM bridgeTechGroup a
  INNER JOIN dimtech b ON a.techkey = b.techkey
    AND b.technumber IN ('619', '135')
  GROUP BY techgroupkey) b ON c.techgroupkey = b.techgroupkey
WHERE c.storecode = 'ry1'
GROUP BY c.ro

-- hmm, ro's with flagtime for 619, 135
-- writers ashley: 123, tim: 421
SELECT writerid, COUNT(*)
FROM factro
WHERE storecode = 'ry1'
  AND void = false
  AND ro IN (
    SELECT c.ro
    FROM factTechLineFlagDateHours c
    INNER JOIN (
      SELECT techgroupkey 
      FROM bridgeTechGroup a
      INNER JOIN dimtech b ON a.techkey = b.techkey
        AND b.technumber IN ('619', '135')
      GROUP BY techgroupkey) b ON c.techgroupkey = b.techgroupkey
    WHERE c.storecode = 'ry1'
    GROUP BY c.ro)  
GROUP BY writerid