-- clockhours by day with empkey, emp#, name, store, dept, distcode, technumber
-- daterange SET IN JOIN to day
-- any day with clockhours
-- so IS this returns nRows (n=daterange) for each empkey with
SELECT b.thedate, b.datekey, c.storecode, a.employeekey, c.employeenumber, c.name, c.pydept, 
  c.pydeptcode, c.distcode, distribution, coalesce(d.technumber, 'NA') AS technumber, clockhours
FROM edwClockHoursFact a
INNER JOIN day b ON a.datekey = b.datekey
LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
LEFT JOIN dimtech d ON c.storecode = d.storecode
  AND c.employeenumber = d.employeenumber

CREATE TABLE rptClockHours (
  theDate date,
  DateKey integer, 
  StoreCode cichar(3),
  EmployeeKey integer,
  Employeenumber cichar(7),
  Name cichar(25),
  pyDept cichar(30),
  pyDeptCode cichar(2),
  DistCode cichar(4),
  Distribution cichar(25),
  TechNumber cichar(3),
  ClockHours double) IN database;

-- 7/25 ri IS fucked up, multiple employeenumber/date
-- added ekfrom/thru to edwEmployeeDim JOIN

EXECUTE PROCEDURE sp_zaptable('rptClockHours'); 
INSERT INTO rptClockHours
SELECT b.thedate, b.datekey, c.storecode, a.employeekey, c.employeenumber, c.name, c.pydept, 
  c.pydeptcode, c.distcode, distribution, coalesce(d.technumber, 'NA') AS technumber, clockhours
FROM edwClockHoursFact a
INNER JOIN day b ON a.datekey = b.datekey
LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
  AND b.thedate BETWEEN employeekeyfromdate AND employeekeythrudate
LEFT JOIN dimtech d ON c.storecode = d.storecode
  AND c.employeenumber = d.employeenumber 
WHERE c.storecode IS NOT NULL;   

SELECT thedate, storecode, employeenumber
FROM rptClockHours
GROUP BY thedate, storecode, employeenumber
HAVING COUNT(*) > 1

TABLE rptClockHours ( 
      theDate Date      
  NK  DateKey Integer
  NK  StoreCode CIChar( 3 )
      EmployeeKey Integer
  NK  Employeenumber CIChar( 7 )
      Name CIChar( 25 )
      pyDept CIChar( 30 )
      pyDeptCode CIChar( 2 )
      DistCode CIChar( 4 )
      Distribution CIChar( 25 )
      TechNumber CIChar( 3 )
      ClockHours Double( 15 )

-- BY store BY dept for june 2012
SELECT storecode, pydept, round(SUM(clockhours), 2)
FROM rptClockHours
WHERE month(thedate) = 6
  AND year(thedate) = 2012
GROUP BY storecode, pydept  

-- BY store/dept/distcode/employee for june 2012
SELECT storecode, pydept, distcode, name, SUM(clockhours) AS clockhours
FROM rptClockHours
WHERE month(thedate) = 6
  AND year(thedate) = 2012
GROUP BY storecode, pydept, distcode, name

-- ry1 serv techs for june 2012
SELECT name, technumber, SUM(clockhours)
FROM rptClockhours
WHERE month(thedate) = 6
  AND year(thedate) = 2012
  AND storecode = 'ry1'
  AND technumber <> 'NA'
  AND pydept = 'service'
GROUP BY name, technumber

-- ry2 serv techs for june 2012
SELECT name, technumber, SUM(clockhours)
FROM rptClockhours
WHERE month(thedate) = 6
  AND year(thedate) = 2012
  AND storecode = 'ry2'
  AND technumber <> 'NA'
  AND pydept = 'service'
GROUP BY name, technumber

-- ry1 detail for june 2012
SELECT name, SUM(clockhours)
FROM rptClockhours
WHERE month(thedate) = 6
  AND year(thedate) = 2012
  AND storecode = 'ry1'
  AND distcode = 'WTEC'
GROUP BY name
  
  
-- ry1 serv techs for july 2012
SELECT thedate, SUM(clockhours)
FROM rptClockhours
WHERE month(thedate) = 7
  AND year(thedate) = 2012
  AND storecode = 'ry1'
  AND technumber <> 'NA'
  AND pydept = 'service'
GROUP BY thedate

