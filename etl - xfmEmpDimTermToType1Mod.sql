/*
i believe term should be a type 1 change
need additional edwEmployeeDim COLUMN: PartFullTime
ActiveCode becomes Active/Term
1. ADD & populate PartFull COLUMN -- history will NOT be accurate
-- edit resArkonaCodes to accomodate the change xfmTable/COLUMN values 
2. UPDATE Active with correct value
3. modify orig row IN edwEmployeeDim for emp# with 1 row for active AND 1 row for term
4. DELETE the type2 term rows
5. check AND DELETE rows FROM edwClockHoursFact for the deleted dimension rows
6. change sp to handle term AS type 1 AND A->P, P->A to type 2
*/
SELECT storecode, employeekey, employeenumber, hiredate, termdate, 
  CASE
-- the employeekey 1362 IS NOT currently active, empkey 1391 is  
-- the empkey IS a comb of effdate AND ? expdate ?
    WHEN termdate = '12/31/9999' THEN 'Active'
    ELSE 'Term'
  END AS act, 
  EffTS, currentrow
FROM ( -- 1 row per empkey/effTS
  SELECT storecode, employeekey, employeenumber, hiredate, MAX(termdate) AS termdate,
    CAST(RowEffectiveTS AS sql_date) AS EffTS, currentrow
  FROM edwEmployeeDim  
  GROUP BY storecode, employeekey, employeenumber, hiredate, CAST(RowEffectiveTS AS sql_date), currentrow) x
WHERE employeenumber in (
  '16250', -- name change
  '1111211', -- term
  '171210') -- dept change
ORDER BY employeenumber

SELECT *
FROM edwEmployeeDim 
WHERE employeenumber in (
  '16250', -- name change
  '1111211', -- term
  '171210') 
ORDER BY employeenumber
    
-- already zapped a couple of records
-- emp#s with a record for active AND term
-- move to local to modify sp.xfmEmpDIM so AS NOT to bust tonights scrape
SELECT *
FROM edwEmployeeDim e1
WHERE EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE employeenumber = e1.employeenumber
    AND termdatekey = (
      SELECT datekey
      FROM day 
      WHERE datetype = 'NA'))
AND EXISTS (  
  SELECT 1
  FROM edwEmployeeDim
  WHERE employeenumber = e1.employeenumber
    AND termdatekey in (
      SELECT datekey
      FROM day 
      WHERE datetype <> 'NA'))
ORDER BY employeenumber  

SELECT *
FROM edwClockhoursFact
WHERE employeekey = 1398
delete
FROM edwClockhoursFact
WHERE employeekey = 1382
/*******************************************************************************
1. ADD & populate PartFull COLUMN -- history will NOT be accurate. tables edwEmployeeDim & xfmEmployeeDim
2. Modify sp.xfmEmpDim to decode ymactv values
  -- INSERT INTO xfmEmpDim
  -- never mind, new constraint: IF ymactv <> T THEN FullPartTimeCodeDesc must NOT be NULL
  -- INSERT INTO edwEmployeeDim 
  -- type 1 changes: ADD ymactv
  -- type 2 changes: remove ymactv, termdate
  --                 INSERT INTO edwEmployeeDim 
3. UPDATE Active with correct value
4. modify orig row IN edwEmployeeDim for emp# with 1 row for active AND 1 row for term
5. DELETE the type2 term rows
6. check AND DELETE rows FROM edwClockHoursFact for the deleted dimension rows
7. change sp to handle term AS type 1 AND A->P, P->A to type 2
*******************************************************************************/
ALTER TABLE edwEmployeeDim
ADD COLUMN FullPartTimeCode CIChar(1)
ADD COLUMN FullPartTimeCodeDesc cichar(8)
ALTER COLUMN ActiveCodeDesc ActiveCodeDesc cichar(8);

ALTER TABLE xfmEmployeeDim
ADD COLUMN FullPartTimeCode CIChar(1)
ADD COLUMN FullPartTimeCodeDesc cichar(8);
ALTER COLUMN ActiveCodeDesc ActiveCodeDesc cichar(8);
--resArkonaCodes: thinking fuckit,just decode IN sp.xfmEmpDIM
-- 3.
SELECT *
FROM edwEmployeeDim

UPDATE edwEmployeeDim
SET FullPartTimeCodeDesc = 
  CASE 
    WHEN ActiveCode = 'A' THEN 'Full'
    WHEN ActiveCode = 'P' THEN 'Part'
  END;
  
UPDATE edwEmployeeDim
SET ActiveCodeDesc = 
  CASE 
    WHEN ActiveCode = 'A' THEN 'Active'
    WHEN ActiveCode = 'P' THEN 'Active'
    WHEN ActiveCode = 'T' THEN 'Term'
  END;    
  
UPDATE edwEmployeeDim
SET FullPartTimeCode = ActiveCode
WHERE ActiveCode <> 'T';  

--4.
-- emp#s with a record for active AND term
-- fix these manually
SELECT *
FROM edwEmployeeDim e1
WHERE EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE employeenumber = e1.employeenumber
    AND termdatekey = (
      SELECT datekey
      FROM day 
      WHERE datetype = 'NA'))
AND EXISTS (  
  SELECT 1
  FROM edwEmployeeDim
  WHERE employeenumber = e1.employeenumber
    AND termdatekey in (
      SELECT datekey
      FROM day 
      WHERE datetype <> 'NA'))
ORDER BY employeenumber  

SELECT *
FROM edwClockhoursFact
WHERE employeekey = 1398
delete
FROM edwClockhoursFact
WHERE employeekey = 1397
