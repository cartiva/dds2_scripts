-- wtf, a whole bunch of 99999 minutes
-- aha! clockin times are ON the hour
-- eg  110910 1/2/12, 121101 1/6/12, 185950 1/5/10
-- also check ON the hour clockouts
-- minute 1/2 to DO tmp
-- test against edwClockHoursFact SUM(minutes) BY day
-- this IS only good for WHEN yiclind = yiclkoutd   
-- 7/25, ok this IS it
-- simply DELETE the offending concurrent logins
-- test for recurrence nightly
-- exclude post midnight minutes
-- 2 runs 1. yiclkind = yiclkoutd  2. yiclkind < yiclkoutd
-- how DO i want to store the results
at the fact level, just LIKE edwClockHoursFact with a timeofdaykey
at the rpt level, just LIKE rptClockhours
-- ok, 1 more test
-- compare zminutes with edwClockHoursFact
so xfm zminutes INTO the factlevel AND THEN compare
need the empkey, DO it just LIKE edwClockHoursFact 


-- 7/25 first load 
-- god damnit, DO real AND complete mpClockMinutes first
CREATE TABLE factClockMinutesByHour (
  EmployeeKey integer,
  DateKey integer,
  TimeOfDayKey integer, 
  Minutes integer) IN database;
  
EXECUTE PROCEDURE sp_ZapTable('tmpClockMinutes');
-- 1. yiclkind = yiclkoutd  
INSERT INTO tmpClockMinutes
SELECT a.yico#, a.yiemp#, a.yiclkind, b.thetime, 
  SUM (
    CASE
      WHEN thetime = yiclkint AND thetime = yiclkoutt -- 1
        OR thetime > yiclkint AND thetime = yiclkoutt -- 7
        OR thetime > yiclkint AND thetime > yiclkoutt AND hour(thetime) > hour(yiclkoutt) -- 9a
          THEN 0
      WHEN thetime = yiclkint AND thetime < yiclkoutt AND hour(thetime) = hour(yiclkoutt) -- 2a
        OR thetime < yiclkint AND thetime < yiclkoutt AND hour(thetime) = hour(yiclkoutt) -- 5a
          THEN minute(yiclkoutt) - minute(yiclkint)
      WHEN thetime = yiclkint AND thetime < yiclkoutt AND hour(thetime) < hour(yiclkoutt) -- 2b
        OR thetime > yiclkint and thetime < yiclkoutt AND hour(thetime) < hour(yiclkoutt) -- 8b
          THEN 60 
      WHEN thetime = yiclkint and thetime > yiclkoutt -- 3
        OR thetime < yiclkint and thetime = yiclkoutt -- 4
        OR thetime < yiclkint and thetime > yiclkoutt -- 6
          THEN 666
      WHEN thetime < yiclkint and thetime < yiclkoutt and hour(thetime) < hour(yiclkoutt) THEN 60 - minute(yiclkint) -- 5b
      WHEN thetime > yiclkint AND thetime < yiclkoutt AND hour(thetime) = hour(yiclkoutt) THEN minute(yiclkoutt) -- 8a 
      ELSE 999999
    END) AS minutes
FROM (
  SELECT a.yico#,a. yiemp#, a.yiclkind, a.yiclkoutd, a.yicode,
    a.yiclkint, 
      CASE cast(a.yiclkoutt AS sql_char) -- handle midnight clockout
        WHEN '00:00:00' THEN CAST('23:59:59' AS sql_time)
        ELSE a.yiclkoutt
      END AS yiclkoutt  
  FROM tmpPYPCLOCKIN a
  LEFT JOIN ( -- exclude overlap
    SELECT yico#, yiemp#, yiclkind, min(yiclkint) AS minIn, MAX(yiclkint) AS maxIN,
      MIN(yiclkoutt) AS minOut, MAX(yiclkoutt) AS maxOut
    FROM tmpPYPCLOCKIN  
    GROUP BY yico#, yiemp#, yiclkind
    HAVING COUNT(*) > 1
      AND MAX(yiclkint) < MIN(yiclkoutt)) b ON a.yico# = b.yico#
    AND a.yiemp# = b.yiemp#
    AND a.yiclkind = b.yiclkind
  LEFT JOIN ( -- exclude dup
    SELECT yico#, yiemp#, yiclkind, yiclkint
    FROM tmpPYPCLOCKIN
    GROUP BY yico#, yiemp#, yiclkind, yiclkint
      HAVING COUNT(*) > 1) c ON a.yico# = c.yico#
    AND a.yiemp# = c.yiemp#
    AND a.yiclkind = c.yiclkind
    AND a.yiclkint = c.yiclkint  
  WHERE (b.yico# IS NULL AND c.yico# IS NULL)
    AND a.yiclkind = a.yiclkoutd -- !important yiclkind = yiclkoutd BY one day
    AND yicode = 'O') a
LEFT JOIN (
  SELECT *
  FROM timeofday 
  WHERE minute(thetime) = 0
    AND second(thetime) = 0) b ON hour(b.thetime) BETWEEN hour(yiclkint) AND hour(yiclkoutt)
GROUP BY a.yico#, a.yiemp#, a.yiclkind, b.thetime; 

-- 2. yiclkind < yiclkoutd  
INSERT INTO tmpClockMinutes
SELECT a.yico#, a.yiemp#, a.yiclkind, b.thetime, 
  SUM (
    CASE
      WHEN thetime = yiclkint AND thetime = yiclkoutt -- 1
        OR thetime > yiclkint AND thetime = yiclkoutt -- 7
        OR thetime > yiclkint AND thetime > yiclkoutt AND hour(thetime) > hour(yiclkoutt) -- 9a
          THEN 0
      WHEN thetime = yiclkint AND thetime < yiclkoutt AND hour(thetime) = hour(yiclkoutt) -- 2a
        OR thetime < yiclkint AND thetime < yiclkoutt AND hour(thetime) = hour(yiclkoutt) -- 5a
          THEN minute(yiclkoutt) - minute(yiclkint)
      WHEN thetime = yiclkint AND thetime < yiclkoutt AND hour(thetime) < hour(yiclkoutt) -- 2b
        OR thetime > yiclkint and thetime < yiclkoutt AND hour(thetime) < hour(yiclkoutt) -- 8b
          THEN 60 
      WHEN thetime = yiclkint and thetime > yiclkoutt -- 3
        OR thetime < yiclkint and thetime = yiclkoutt -- 4
        OR thetime < yiclkint and thetime > yiclkoutt -- 6
          THEN 666
      WHEN thetime < yiclkint and thetime < yiclkoutt and hour(thetime) < hour(yiclkoutt) THEN 60 - minute(yiclkint) -- 5b
      WHEN thetime > yiclkint AND thetime < yiclkoutt AND hour(thetime) = hour(yiclkoutt) THEN minute(yiclkoutt) -- 8a 
      ELSE 999999
    END) AS minutes   
FROM (
  SELECT a.yico#, a.yiemp#, a.yiclkind, a.yiclkoutd, a.yicode,
    a.yiclkint,  CAST('23:59:59' AS sql_time) AS yiclkoutt    
--  FROM tmpPYPCLOCKIN
  FROM tmpPYPCLOCKIN a
  LEFT JOIN ( -- exclude overlap
    SELECT yico#, yiemp#, yiclkind, min(yiclkint) AS minIn, MAX(yiclkint) AS maxIN,
      MIN(yiclkoutt) AS minOut, MAX(yiclkoutt) AS maxOut
    FROM tmpPYPCLOCKIN  
    GROUP BY yico#, yiemp#, yiclkind
    HAVING COUNT(*) > 1
      AND MAX(yiclkint) < MIN(yiclkoutt)) b ON a.yico# = b.yico#
    AND a.yiemp# = b.yiemp#
    AND a.yiclkind = b.yiclkind
  LEFT JOIN ( -- exclude dup
    SELECT yico#, yiemp#, yiclkind, yiclkint
    FROM tmpPYPCLOCKIN
    GROUP BY yico#, yiemp#, yiclkind, yiclkint
      HAVING COUNT(*) > 1) c ON a.yico# = c.yico#
    AND a.yiemp# = c.yiemp#
    AND a.yiclkind = c.yiclkind
    AND a.yiclkint = c.yiclkint  
  WHERE (b.yico# IS NULL AND c.yico# IS NULL)
    AND a.yiclkoutd - a.yiclkind = 1 -- yiclkind < yiclkoutd BY one day
    AND a.yicode = 'O') a  -- clockhours only, no vac, pto, etc
LEFT JOIN (
  SELECT *
  FROM timeofday 
  WHERE minute(thetime) = 0
    AND second(thetime) = 0) b ON hour(b.thetime) BETWEEN hour(yiclkint) AND hour(yiclkoutt)   
GROUP BY a.yico#, a.yiemp#, a.yiclkind, b.thetime;         

CREATE TABLE factClockMinutesByHour (
  EmployeeKey integer,
  DateKey integer,
  TimeOfDayKey integer, 
  Minutes integer) IN database;

EXECUTE PROCEDURE sp_ZapTable('factClockMinutesByHour');
  
INSERT INTO factClockMinutesByHour
SELECT b.employeekey, c.datekey, d.TimeOfDayKey, sum(a.minutes)
FROM zminutes a
LEFT JOIN edwEmployeeDim b ON  a.yico = b.storecode
  AND a.yiemp = b.employeenumber
  AND a.yiclkind BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
LEFT JOIN day c ON a.yiclkind = c.thedate 
LEFT JOIN TimeOfDay d ON a.thetime = d.thetime
WHERE a.minutes < 61
  AND a.yico IN ('ry1','ry2','ry3')
  AND b.storecode IS NOT null  -- first fix clockhours outside of empl duration
GROUP BY b.employeekey, c.datekey, d.TimeOfDayKey;  

-- 1 verify RI  
--   NK: EmployeeKey;DateKey;TimeOfDayKey
-- 2 test against edwClockHoursFact
-- looks good, with the comparison, see below for outliers
SELECT d.*, e.clockhours, round(d.minhours - e.clockhours, 2)
FROM (
  SELECT a.thedate, a.datekey, b.employeekey, sum(b.minutes)/60.0 AS minHours
  FROM day a
  LEFT JOIN factClockMinutesByHour b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN '01/01/2012' AND curdate() - 1
  GROUP BY a.thedate, a.datekey, b.employeekey) d
LEFT JOIN edwClockHoursFact e ON d.employeekey = e.employeekey
  AND d.datekey = e.datekey  
WHERE ABS(round(d.minhours - e.clockhours, 2)) > .5  
-- the outliers
-- this one CASE at least, slipped thru AND was paid for the inflated hours
SELECT *
FROM edwClockHoursFact
WHERE employeekey = 1483
  AND datekey = 3112
  
SELECT * FROM edwEmployeeDim WHERE employeekey = 1483
SELECT * FROM day WHERE datekey = 3112 

SELECT *
FROM tmpClockHours  
WHERE yiemp# = '115101'
  AND yiclkind = '07/08/2012'
  
SELECT *
FROM tmpPYPCLOCKIN  
WHERE yiemp# = '115101'
  AND yiclkind = '07/08/2012'
-- thinking a test against nightly edwClockHoursFact, MAX clockhours/day  
SELECT * FROM edwClockHoursFact where clockhours > 13 ORDER BY clockhours desc 

SELECT a.clockhours, a.regularhours, a.overtimehours, b.thedate, c.name, c.pydept 
FROM edwClockHoursFact a
LEFT JOIN day b ON a.datekey = b.datekey
LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey
where a.clockhours > 13
  AND c.pydept <> 'hobby shop'
ORDER BY thedate desc
-- 3 DO nightly
--     include test for mult concurrent login
-- 4 DO rpt
-- 5 verify RI with employeenumber

-- 1
-- yikes
-- 1a
-- 160350: chad olson still clocking ry1 for a few days after going to ry2
-- 110090: sydney bayley post term clockhours
-- 110230: frankie barrios post term clockhours
-- 1114150: jelly pre hire clockhours: fix IN edwEmployeeDim
-- ALL except jelly are clockhours outside of empl duration
-- FUCK 'EM
-- first fix
-- AND of course, the other dups were because i was NOT grouping IN the INSERT
-- 2 good enuf

SELECT a.thedate, b.*, c.clockhours
FROM day a
LEFT JOIN (
  SELECT employeekey, datekey, SUM(minutes), round(SUM(minutes)*1.0/60, 2) AS minHours 
  FROM factClockMinutesByHour
  GROUP BY employeekey, datekey) b ON a.datekey = b.datekey
LEFT JOIN edwClockHoursFact c ON a.datekey = c.datekey  
  AND c.employeekey = b.employeekey
WHERE thedate BETWEEN '04/01/2012' AND '06/30/2012'
  AND ABS(minhours - clockhours) > 1

-- 3
-- this looks LIKE it selects out the overlapping records
-- that account for minutes > 60  
-- include this AS a test IN ETL
SELECT yico#, yiemp#, yiclkind, min(yiclkint) AS minIn, MAX(yiclkint) AS maxIN,
  MIN(yiclkoutt) AS minOut, MAX(yiclkoutt) AS maxOut
-- FROM stgArkonaPYPCLOCKIN  
FROM stgArkonaPYPCLOCKIN  
--WHERE yiemp# = '114205'
--  AND yiclkind = '06/13/2012'
GROUP BY yico#, yiemp#, yiclkind
HAVING COUNT(*) > 1
  AND MAX(yiclkint) < MIN(yiclkoutt)
ORDER BY yico#, yiemp#, yiclkind  


-- 4

SELECT * FROM factClockMinutesByHour
DROP TABLE rptClockMinutesByHour;
CREATE TABLE rptClockMinutesByHour ( 
      theDate Date,
      DateKey Integer,
      StoreCode CIChar( 3 ),
      EmployeeKey Integer,
      Employeenumber CIChar( 7 ),
      Name CIChar( 25 ),
      pyDept CIChar( 30 ),
      pyDeptCode CIChar( 2 ),
      DistCode CIChar( 4 ),
      Distribution CIChar( 25 ),
      TechNumber CIChar( 3 ),
      TimeOfDay Time,
      ClockMinutes integer) IN DATABASE;
-- EXECUTE procedure sp_zaptable('rptClockMinutesByHour');      
INSERT INTO rptClockMinutesByHour
  SELECT b.thedate, b.datekey, c.storecode, a.employeekey, c.employeenumber, c.name, c.pydept, 
    c.pydeptcode, c.distcode, distribution, coalesce(d.technumber, 'NA') AS technumber, 
    (select thetime from timeofday where timeofdaykey = a.timeofdaykey), a.minutes
  FROM factClockMinutesByHour a
  INNER JOIN day b ON a.datekey = b.datekey
  LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
    AND b.thedate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
  LEFT JOIN dimtech d ON c.storecode = d.storecode
    AND c.employeenumber = d.employeenumber; 
    
-- 7/30
-- so the deal with this IS the same emp# clocked IN at the same day/time more than once  
-- because of the fucked up empkey from/thru dates resulting IN what looks LIKE
-- multiple empkeys for the same emp# at the same time  
-- so DO the overlap query against edwEmployeeDim 
-- this returns nothing, why that, obviously my concise analysis IS NOT correct
-- what's wrong IN my assumption
SELECT storecode, employeenumber
FROM edwEmployeeDim a
WHERE EXISTS (
  SELECT 1
GROUP BY storecode, employeenumber
HAVING COUNT(*) > 1
  AND MAX(employeekeyfromdate) < MIN(employeekeythrudate)
  
  
 
-- dup emp#/date/time    
SELECT thedate, storecode, employeenumber, timeofday
--INTO #wtf
FROM rptClockMinutesByHour    
GROUP BY thedate, storecode, employeenumber, timeofday
HAVING COUNT(*) > 1

-- fuck, they are both rehire/reuse 1109110: tim paige, 1147260: mahamed warsame
SELECT employeenumber, COUNT(*)
FROM #wtf
GROUP BY employeenumber

-- this also highlights some other edwEmployeeDim issues
-- looks LIKE terms are getting fucked up, some at least
-- Al Berry, morlock, korsmoe, hays  
SELECT employeekey, storecode, employeenumber, activecode, pydept, distcode, name, 
  hiredate, hiredatekey, termdate, termdatekey, rowchangedate, employeekeyfromdate, 
  employeekeyfromdatekey, employeekeythrudate, employeekeythrudatekey, currentrow, rowchangereason
FROM edwEmployeeDim
WHERE employeenumber IN (
  SELECT employeenumber 
  FROM edwEmployeeDim
  WHERE employeekeyfromdate > employeekeythrudate)
ORDER BY employeenumber, employeekeyfromdate

tables with empkey
edwEmployeeDim 
AccrualCommissions
edwClockHoursFact 
factTechFlagHoursByDay
keyMapDimEmp
factClockHoursToday
zProd
stgTechTimeAdjustments
rptClockHours
rptFlagHours
factClockMinutesByHour
rptClockMinutesByHour

-- another test for edwEmployeeDim
-- any date/datekey mismatch
-- new record: empnumber already used
-- ekfrom > ekthru
-- hiredate > termdate
SELECT parent
FROM system.columns
WHERE name = 'EmployeeKey'
DECLARE @cur CURSOR AS 
  SELECT parent
  FROM system.columns
  WHERE name = 'EmployeeKey';

-- henry morgan
ekfrom 11/9/10
ekthru 1/19/10, sb 1/19/11
UPDATE edwEmployeeDim
  SET termdate = '01/19/2011',
      termdatekey = (SELECT datekey FROM day WHERE thedate = '01/19/2011'),
      employeekeythrudate = '01/19/2011',
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '01/19/2011')
SELECT * FROM edwEmployeeDim 
WHERE employeenumber = '198875'

-- shayne bosma
-- fucked up termdate
UPDATE edwEmployeeDim
  SET termdate = '01/19/2011',
      termdatekey = (SELECT datekey FROM day WHERE thedate = '01/19/2011'),
      employeekeythrudate = '01/19/2011',
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '01/19/2011')
--SELECT * FROM edwEmployeeDim 
WHERE employeenumber = '116580'

-- 7/26 this IS turning INTO an edwEmployeeDim analysis, finding bad data
-- 2 categories rehire/reuse AND term
-- i'm wondering IF the terms are inclusive  to a date range
-- this query IS ekfrom > ekthru
SELECT employeekey, storecode, employeenumber, activecode, pydept, distcode, name, 
  hiredate, hiredatekey, termdate, termdatekey, rowchangedate, employeekeyfromdate, 
  employeekeyfromdatekey, employeekeythrudate, employeekeythrudatekey, currentrow, rowchangereason
FROM edwEmployeeDim
WHERE employeenumber IN (
  SELECT employeenumber 
  FROM edwEmployeeDim
  WHERE employeekeyfromdate > employeekeythrudate)
ORDER BY employeenumber, employeekeyfromdate

-- 7/31 
SELECT thedate, employeenumber, timeofday
FROM rptClockMinutesByHour
GROUP BY  thedate, employeenumber, timeofday
HAVING COUNT(*) > 1






