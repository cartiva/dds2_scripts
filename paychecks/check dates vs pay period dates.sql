
SELECT b.*, c.biweeklypayperiodstartdate, c.biweeklypayperiodenddate
-- SELECT *
FROM (  
  select 
    cast (
      case
        when yhdemm between 1 and 9 then '0' + trim(cast(yhdemm AS sql_char))
        else trim(cast(yhdemm AS sql_char))
      end 
      + '/' +
      case
        when yhdedd between 1 and 9 then '0' + trim(cast(yhdedd AS sql_char))
        else trim(cast(yhdedd AS sql_char))
      end 
      + '/' +
      case
        when yhdeyy between 1 and 9 then '200' + trim(cast(yhdeyy AS sql_char))
        else '20' + trim(cast(yhdeyy AS sql_char))
      END AS sql_date) as PayrollEndDate,
    cast (
      case
        when yhdcmm between 1 and 9 then '0' + trim(cast(yhdcmm AS sql_char))
        else trim(cast(yhdcmm AS sql_char))
      end 
      + '/' +
      case
        when yhdcdd between 1 and 9 then '0' + trim(cast(yhdcdd AS sql_char))
        else trim(cast(yhdcdd AS sql_char))
      end 
      + '/' +
      case
        when yhdcyy between 1 and 9 then '200' + trim(cast(yhdcyy AS sql_char))
        else '20' + trim(cast(yhdcyy AS sql_char))
      END AS sql_date) as CheckDate ,
      a.theCount   
  FROM (  
    SELECT ypbcyy, yhdeyy, yhdemm,yhdedd, yhdcyy, yhdcmm, yhdcdd, COUNT(*) AS theCount
    FROM dds.stgArkonaPYHSHDTA
    WHERE ypbcyy > 109
    GROUP BY ypbcyy, yhdeyy, yhdemm,yhdedd, yhdcyy, yhdcmm, yhdcdd) a) b
LEFT JOIN (
  select biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM dds.day c 
  group by biweeklypayperiodstartdate, biweeklypayperiodenddate) c on b.payrollEndDate = c.BiWeeklyPayPeriodEndDate
ORDER BY payrollenddate desc