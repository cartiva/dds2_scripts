DROP TABLE xfminpmastMake;
CREATE TABLE xfmINPMASTMake (
  vin cichar(17),
  make1 cichar(30),
  make2 cichar(20), 
  make3 cichar(20)) IN database;

INSERT INTO xfminpmastMake (vin, make1, make2)  

SELECT imvin, immake, b.make
FROM stgArkonaInpmast a
LEFT JOIN (
  SELECT DISTINCT make
  FROM dps.MakeModelClassifications) b ON a.immake = b.make
WHERE (
  imstat = 'i'
  or 
  EXISTS (
    SELECT 1
    FROM stgArkonaBOPMAST
    WHERE bmvin = a.imvin))   
  AND length(imvin) = 17
  AND immodl <> ''  
  AND imyear > 1989;

UPDATE xfminpmastMake
SET make3 = 
--SELECT a.*,
  CASE 
    WHEN make2 IS NOT NULL THEN make2 
    WHEN make2 IS NULL AND (LEFT(make1, 4) = 'CHEV' 
      OR LEFT(make1, 4) = 'CEVE'
      OR LEFT(make1, 4) = 'CHEC'
      OR LEFT(make1, 4) = 'CHEF' 
      OR LEFT(make1, 4) = 'CAVI'
      OR LEFT(make1, 4) = 'CHEY'
      OR LEFT(make1, 4) = 'CHWE'
      OR LEFT(make1, 4) = 'CHEC'
      OR LEFT(make1, 4) = 'CORV'
      OR LEFT(make1, 6) = 'SUBURB') THEN 'Chevrolet'
    WHEN make2 IS NULL AND LEFT(make1, 3) = 'CAD' THEN 'Cadillac'
    WHEN make2 IS NULL AND LEFT(make1, 3) = 'PON' THEN 'Pontiac'
    WHEN make2 IS NULL AND make1 LIKE 'v%' THEN 'Volkswagen'
    WHEN make2 IS NULL AND make1 LIKE 'silver%' THEN 'Chevrolet'
    WHEN make2 IS NULL AND (make1 LIKE 'sier%' 
      OR make1 = 'gms'
      OR make1 = 'GMC PICKUP') THEN 'GMC'
    WHEN make2 IS NULL AND (make1 = 'plym' OR make1 = 'polaris') THEN 'Plymouth'
    WHEN make2 IS NULL AND (LEFT(make1, 2) like 'bu%' 
      OR LEFT(make1, 4) = 'park') THEN 'Buick'
    WHEN make2 IS NULL AND (LEFT(make1, 4) = 'ACCO' 
      OR LEFT(make1, 4) = 'ELEM' 
      OR LEFT(make1, 4) = 'HOND') THEN 'Honda'
    WHEN make2 IS NULL AND LEFT(make1, 4) = 'CHRY' THEN 'Chrysler' 
    WHEN make2 IS NULL AND (LEFT(make1, 4) = 'DODG' 
      OR LEFT(make1, 4) = 'DOGE') THEN 'Dodge'
    WHEN make2 IS NULL AND LEFT(make1, 3) = 'FOR' THEN 'Ford'
    WHEN make2 IS NULL AND (LEFT(make1, 5) = 'GRAND' 
      OR LEFT(make1, 5) = 'bonne') THEN 'Pontiac'
    WHEN make2 IS NULL AND LEFT(make1, 3) = 'JEE' THEN 'Jeep'
    WHEN make2 IS NULL AND LEFT(make1, 4) = 'linc' THEN 'Lincoln'
    WHEN make2 IS NULL AND LEFT(make1, 3) = 'maz' THEN 'Mazda'
    WHEN make2 IS NULL AND LEFT(make1, 5) = 'merc ' THEN 'Mercury'
    WHEN make2 IS NULL AND LEFT(make1, 5) = 'merce' THEN 'Mercedes-Benz'
    WHEN make2 IS NULL AND (LEFT(make1, 3) = '2is' 
      OR LEFT(make1, 4) = 'niss' 
      OR LEFT(make1, 4) = 'mura') THEN 'Nissan'   
    WHEN make2 IS NULL AND (LEFT(make1, 1) = 'o' 
      or make1 = 'alero') THEN 'Oldsmobile'    
    WHEN make2 IS NULL AND (LEFT(make1, 7) = 'subarau'
      OR LEFT(make1, 6) = 'SUBERU') THEN 'Subaru'  
    WHEN make2 IS NULL AND LEFT(make1, 5) = 'toyot' THEN 'Toyota' 
    WHEN make2 IS NULL AND make1 = 'INFINITY' THEN 'Infiniti'    
  END;
-- FROM xfminpmastMake a
-- WHERE make2 IS NULL;

DELETE -- only 4
FROM xfminpmastMake
WHERE make3 IS NULL; 

DROP TABLE xfminpmastModel;
CREATE TABLE xfmINPMASTModel (
  vin cichar(17),
  make cichar(20),
  model1 cichar(25), -- FROM inpmast
  model2 cichar(25), -- FROM MakeModelClassifications WHERE = inpmast
  model3 cichar(50),
  chromeModel cichar(50), -- FROM chrome vin
  mmcModel cichar(25), -- FROM MakeModelClassifications WHERE = chrome
  bbMake cichar(20), -- FROM bb vin
  bbModel cichar(20)) -- FROM bb vin
   IN database;

INSERT INTO xfminpmastModel (vin, make, model1, model2, chromeModel, mmcModel, bbmake, bbmodel)  

SELECT distinct imvin, b.make3, a.immodl, c.model, trim(d.vinmodelname) AS chromModel, e.model AS mmcmodelFromChrome,
  f.make AS bbMake, f.model AS bbModel
FROM stgArkonaInpmast a
INNER JOIN xfminpmastMake b ON a.imvin = b.vin -- limit BY those vins FROM xfmMake
LEFT JOIN (
  SELECT DISTINCT model
  FROM dps.MakeModelClassifications) c ON a.immodl = c.model
LEFT JOIN zvinpattern d ON LEFT(VinPattern, 8) = LEFT(imvin, 8)
  AND imvin LIKE REPLACE(vinpattern, '*', '_')  
LEFT JOIN (
  SELECT DISTINCT model
  FROM dps.MakeModelClassifications) e ON d.vinmodelname = e.model
LEFT JOIN bb.blackbookadt f ON LEFT(a.imvin, 8) = f.vin 
  AND substring(a.imVIN, 10, 1) = f.vinyear
  AND f.blackbookpubdate = '10/22/2012';   
  
UPDATE xfminpmastModel 
  SET model3 =
    CASE
      WHEN model2 IS NOT NULL THEN model2
      WHEN (model2 IS NULL AND bbmodel IS NOT NULL) THEN bbmodel
      WHEN (model2 IS NULL AND bbmodel IS NULL AND mmcmodel IS NOT NULL) THEN mmcmodel
      ELSE NULL 
    END;
-- get some older blackbook
UPDATE a
SET model3 = f.model 
FROM xfminpmastModel a
LEFT JOIN bb.blackbookadt f ON LEFT(a.vin, 8) = f.vin 
  AND substring(a.VIN, 10, 1) = f.vinyear
  AND f.blackbookpubdate = '12/29/2008' -- '12/25/2006' --
WHERE model3 IS NULL 
  AND model IS NOT NULL;     
  
-- get some older blackbook
UPDATE a
SET model3 = f.model 
FROM xfminpmastModel a
LEFT JOIN bb.blackbookadt f ON LEFT(a.vin, 8) = f.vin 
  AND substring(a.VIN, 10, 1) = f.vinyear
  AND f.blackbookpubdate = '12/25/2006' --'12/29/2008' -- 
WHERE model3 IS NULL 
  AND model IS NOT NULL;  

-- based ON existing model3 values that are CLOSE enuf
UPDATE xfminpmastmodel
SET model3 =
--SELECT a.*, 
  CASE 
    WHEN model3 IS NULL THEN
    CASE 
      WHEN (make = 'Oldsmobile' AND (model1 LIKE '%regency%' OR model1 LIKE '98 re%')) THEN 'Regency'
      WHEN (make = 'Oldsmobile' AND model1 LIKE '%cutl%') THEN 'Cutlass' 
      WHEN (make = 'Chevrolet' AND chromemodel = 'C/K 3500') THEN 'C/K3500' 
      WHEN (make = 'Chevrolet' AND chromemodel = 'C/K 1500') THEN 'C/K1500' 
      WHEN (make = 'Chevrolet' AND chromemodel = 'Silverado 3500HD') THEN 'Silverado 3500' 
      WHEN (make = 'Chevrolet' AND chromemodel = 'Silverado 2500HD') THEN 'Silverado 2500'
      WHEN (make = 'Chevrolet' AND chromemodel = '1500 Pickups') THEN 'C/K1500'
      WHEN (make = 'Chevrolet' AND chromemodel = '2500 Pickups') THEN 'C/K2500'
      WHEN (make = 'Chevrolet' AND model1 = 's-10') THEN 'S10'
      WHEN model1 = 'windstar' THEN 'Windstar'
      WHEN model1 LIKE 'astro%' THEN 'Astro Vans'
      WHEN model1 = 'SIERRA 3500HD' THEN 'Sierra 3500'
      WHEN model1 = 'SIERRA 2500HD' THEN 'Sierra 2500'
    END 
  END 
--FROM xfminpmastmodel a  
WHERE model3 IS NULL; 
--ORDER BY make, model1

DELETE FROM xfminpmastModel
WHERE model3 IS NULL; 

DELETE FROM xfminpmastMake
WHERE vin NOT IN (
  SELECT vin
  FROM xfminpmastModel);

/******************************************************************************/

SELECT *
FROM (  
SELECT a.vin, a.make3, b.model3, b.vin
FROM xfminpmastMake a
full OUTER JOIN xfminpmastModel b ON a.vin = b.vin) c
WHERE vin IS NULL OR vin_1 IS NULL 

-- these are the makes/models that DO NOT classify   
SELECT make3, model3, COUNT(*)
FROM xfminpmastMake a
INNER JOIN xfminpmastModel b ON a.vin = b.vin 
LEFT JOIN dps.MakeModelClassifications c ON make3 = c.make
  AND model3 = c.model
WHERE c.model IS NULL 
GROUP BY make3, model3
-- UPDATE with values FROM VehicleItems WHERE relevant
SELECT a.vin, make3, model3, c.make, c.model, d.make, d.model
FROM xfminpmastMake a
INNER JOIN xfminpmastModel b ON a.vin = b.vin 
LEFT JOIN dps.MakeModelClassifications c ON make3 = c.make
  AND model3 = c.model
LEFT JOIN dps.VehicleItems d ON a.vin = d.vin   
WHERE c.model IS NULL 
order BY make3, model3

UPDATE a
  SET make3 = coalesce(d.make, make3)
FROM xfminpmastMake a
INNER JOIN xfminpmastModel b ON a.vin = b.vin 
LEFT JOIN dps.MakeModelClassifications c ON make3 = c.make
  AND model3 = c.model
LEFT JOIN dps.VehicleItems d ON a.vin = d.vin   
WHERE c.model IS NULL 

UPDATE b
  SET model3 = coalesce(d.model, model3)
FROM xfminpmastMake a
INNER JOIN xfminpmastModel b ON a.vin = b.vin 
LEFT JOIN dps.MakeModelClassifications c ON make3 = c.make
  AND model3 = c.model
LEFT JOIN dps.VehicleItems d ON a.vin = d.vin   
WHERE c.model IS NULL 

-- these are LEFT
SELECT a.vin, make3, model3, chromemodel
FROM xfminpmastMake a
INNER JOIN xfminpmastModel b ON a.vin = b.vin 
LEFT JOIN dps.MakeModelClassifications c ON make3 = c.make
  AND model3 = c.model
WHERE c.model IS NULL 
order BY make3, model3

UPDATE xfminpmastModel
SET model3 = 'C/K1500'
WHERE vin = '1GCEC14W8VZ132010';
UPDATE xfminpmastModel
SET model3 = 'F250'
WHERE vin = '1FTEF25H8LPA86037';
DELETE FROM xfminpmastModel
WHERE vin IN ('1BAACCSA3MF042890','1BAACCSA5MF042891','1GDJ7H1D9XJ852833','JH2RC50365M101752');
DELETE FROM xfminpmastMake
WHERE vin IN ('1BAACCSA3MF042890','1BAACCSA5MF042891','1GDJ7H1D9XJ852833','JH2RC50365M101752');

INSERT INTO dps.MakeModelClassifications 
values('Cadillac','ATS','VehicleSegment_Midsize','VehicleType_Car',true,false,false, now(), NULL,'MfgOriginTyp_Domestic');
INSERT INTO dps.MakeModelClassifications 
values('Cadillac','XTS','VehicleSegment_Large','VehicleType_Car',true,false,false, now(), NULL,'MfgOriginTyp_Domestic');
INSERT INTO dps.MakeModelClassifications 
values('Chevrolet','Spark','VehicleSegment_Small','VehicleType_Car',false,false,false, now(), NULL,'MfgOriginTyp_Domestic');
INSERT INTO dps.MakeModelClassifications 
values('Chevrolet','Volt','VehicleSegment_Midsize','VehicleType_Car',false,false,false, now(), NULL,'MfgOriginTyp_Domestic');
INSERT INTO dps.MakeModelClassifications 
values('Honda','CR-Z','VehicleSegment_Small','VehicleType_Car',false,false,false, now(), NULL,'MfgOriginTyp_Import');
INSERT INTO dps.MakeModelClassifications 
values('Nissan','370Z','VehicleSegment_Compact','VehicleType_Car',false,false,false, now(), NULL,'MfgOriginTyp_Import');
INSERT INTO dps.MakeModelClassifications 
values('Nissan','Leaf','VehicleSegment_Small','VehicleType_Car',false,false,false, now(), NULL,'MfgOriginTyp_Import');
INSERT INTO dps.MakeModelClassifications 
values('Nissan','Murano CrossCabrlt','VehicleSegment_Compact','VehicleType_SUV',false,false,false, now(), NULL,'MfgOriginTyp_Import');
INSERT INTO dps.MakeModelClassifications 
values('Suzuki','Equator','VehicleSegment_Compact','VehicleType_Pickup',false,false,false, now(), NULL,'MfgOriginTyp_Import');

DELETE FROM xfminpmastModel
WHERE vin = '1GCEC14X77Z142281'
  AND model3 = 'Silverado 1500';

/*****************************************************************************/
 
SELECT vin 
FROM (
  SELECT a.vin, a.make3, b.model3, c.vehicletype, c.vehiclesegment
  FROM xfminpmastMake a
  LEFT JOIN xfminpmastModel b ON a.vin = b.vin 
  LEFT JOIN dps.MakeModelClassifications c ON a.make3 = c.make
    AND b.model3 = c.model) z
GROUP BY vin 
HAVING COUNT(*) > 1    

-- uh oh, adding the other tables results IN lots of dup vin

SELECT vin FROM (
SELECT a.vin, a.make3, b.model3, c.vehicletype, c.vehiclesegment
--  d.imstat, imyear, immcode, imbody, imcolr, imodom, imdinv, imddlv, imlic#, imcost
FROM xfminpmastMake a
LEFT JOIN xfminpmastModel b ON a.vin = b.vin 
LEFT JOIN dps.MakeModelClassifications c ON a.make3 = c.make
  AND b.model3 = c.model
LEFT JOIN stgArkonaINPMAST d ON a.vin = d.imvin 
LEFT JOIN dps.VehicleItems e ON a.vin = e.vin
LEFT JOIN zvinpattern f ON LEFT(f.VinPattern, 8) = LEFT(a.vin, 8)
  AND a.vin LIKE REPLACE(vinpattern, '*', '_') 
LEFT JOIN bb.blackbookadt g ON LEFT(a.vin, 8) = g.vin 
  AND substring(a.VIN, 10, 1) = g.vinyear
  AND g.blackbookpubdate = '10/22/2012'  
) z GROUP BY vin HAVING COUNT(*) > 1 

-- looks LIKE the dups come FROM bbSeries
SELECT a.vin, a.make3, left(b.model3, 12) AS model, 
  left(substring(c.vehicletype, position('_' IN c.vehicletype) + 1, length(TRIM(c.vehicletype)) - position('_' IN c.vehicletype) + 1), 12) AS Shape, 
  left(substring(c.vehiclesegment, position('_' IN c.vehiclesegment) + 1, length(TRIM(c.vehiclesegment)) - position('_' IN c.vehiclesegment) + 1), 12) AS Size,
  imbody, left(imcolr, 12) AS color, imtype AS NU, imstat, imyear, 
  LEFT(e.bodystyle, 20) AS viBody, LEFT(e.TRIM, 12) AS viTrim, LEFT (e.engine, 20) AS viEngine,
  LEFT(e.exteriorColor, 12) AS viColor,
  LEFT(e.interiorColor, 12) AS viIntColor,
  LEFT(f.year, 4) AS chYear, left(f.vinstylename, 16) AS chStyle,
  left(g.year, 4) AS bbYear, LEFT(g.series, 12) AS bbSeries, LEFT(g.bodyStyle, 20) AS bbBodyStyle
FROM xfminpmastMake a
LEFT JOIN xfminpmastModel b ON a.vin = b.vin 
LEFT JOIN dps.MakeModelClassifications c ON a.make3 = c.make
  AND b.model3 = c.model
LEFT JOIN stgArkonaINPMAST d ON a.vin = d.imvin 
LEFT JOIN dps.VehicleItems e ON a.vin = e.vin
LEFT JOIN zvinpattern f ON LEFT(f.VinPattern, 8) = LEFT(a.vin, 8)
  AND a.vin LIKE REPLACE(vinpattern, '*', '_') 
LEFT JOIN bb.blackbookadt g ON LEFT(a.vin, 8) = g.vin 
  AND substring(a.VIN, 10, 1) = g.vinyear
  AND g.blackbookpubdate = '10/22/2012'
ORDER BY left(a.vin, 8)      

LEFT JOIN (
  SELECT DISTINCT model
  FROM dps.MakeModelClassifications) c ON a.immodl = c.model
LEFT JOIN zvinpattern d ON LEFT(VinPattern, 8) = LEFT(imvin, 8)
  AND imvin LIKE REPLACE(vinpattern, '*', '_')  
LEFT JOIN bb.blackbookadt f ON LEFT(a.imvin, 8) = f.vin 
  AND substring(a.imVIN, 10, 1) = f.vinyear
  AND f.blackbookpubdate = '10/22/2012';   

SELECT *
FROM bb.blackbookadt

SELECT *
FROM zvinpattern

SELECT *
FROM dps.VehicleItems 

-- 10/27
-- this IS ALL well AND good
-- DO NOT get distracted BY ALL the possibilities
-- focus ON core dimVehicles
CREATE TABLE dimVehicle (
  VehicleKey autoinc,
  vin cichar(17),
  make cichar(60),
  model cichar(60),
  modelyear integer,
  shape cichar(20),
  size cichar(20));
INSERT INTO dimVehicle (vin, make, model, modelyear, shape, size)
SELECT distinct a.vin, a.make3, b.model3, imyear, 
  left(substring(c.vehicletype, position('_' IN c.vehicletype) + 1, length(TRIM(c.vehicletype)) - position('_' IN c.vehicletype) + 1), 12) AS Shape, 
  left(substring(c.vehiclesegment, position('_' IN c.vehiclesegment) + 1, length(TRIM(c.vehiclesegment)) - position('_' IN c.vehiclesegment) + 1), 12) AS Size
FROM xfminpmastMake a
LEFT JOIN xfminpmastModel b ON a.vin = b.vin 
LEFT JOIN dps.MakeModelClassifications c ON a.make3 = c.make
  AND b.model3 = c.model
LEFT JOIN stgArkonaINPMAST d ON a.vin = d.imvin; 


SELECT *
FROM dimVehicle

 
DROP TABLE factVehicleInventory;
create TABLE factVehicleInventory (
  VehicleKey integer,
  Stocknumber cichar(20), -- degenerate dimension
  NewUsed cichar(1),
  InventoryDate date,
  DeliveryDate date,
  SaleAccount cichar(10),
  InventoryAccount cichar(10)) IN database; 
INSERT INTO factVehicleInventory (VehicleKey, stocknumber, newused, inventoryDate, deliverydate,
  saleaccount, inventoryaccount) 
SELECT a.VehicleKey, b.imstk#, b.imtype, imdinv, imddlv, imsact, imiact  
FROM dimVehicle a
LEFT JOIN stgArkonaINPMAST b ON a.vin = b.imvin
WHERE imstk# <> '';

CREATE TABLE factVehicleSale (
  VehicleKey integer,
  Stocknumber cichar(20), -- degenerate dimension
  NewUsed cichar(1),
  RecordType cichar(1),
  SaleType cichar(1),
  OriginationDate date,
  ApprovedDate date, 
  CappedDate date,
  DeliveryDate date) IN database;
INSERT INTO factVehicleSale (vehiclekey, stocknumber, newused, recordtype,
  saletype, originationdate, approveddate, cappeddate, deliverydate)
SELECT a.vehiclekey, b.bmstk#, b.bmvtyp AS NewUsed, bmtype AS RecordType, bmwhsl AS SaleType,
  bmdtor AS OriginationDate, bmdtaprv AS ApprovedDate, bmdtcap AS CappedDate, 
  bmdelvdt AS DeliveryDate
FROM dimVehicle a
LEFT JOIN stgArkonaBOPMAST b ON a.vin = b.bmvin    
WHERE b.bmstk# <> '';

SELECT *
FROM dimVehicle a
INNER JOIN factVehicleInventory b ON a.vehiclekey = b.vehiclekey
LEFT JOIN factVehicleSale c ON b.stocknumber = c.stocknumber
WHERE b.deliverydate <> c.deliverydate

SELECT *
FROM (
  SELECT *
  FROM factVehicleInventory a
  full OUTER JOIN factVehicleSale b ON a.stocknumber = b.stocknumber) c
WHERE (vehiclekey IS NULL OR vehiclekey_1 IS NULL) 

     