SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptdate, 0 as ptlamt, pd.ptlopc,
  pd.ptcrlo
FROM stgArkonaPDPPHDR ph
LEFT JOIN stgArkonaPDPPDET pd ON ph.ptco# = pd.ptco#
  AND ph.ptpkey = pd.ptpkey
where trim(ph.ptdoc#) in (
  select ptro#
  from (
    SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
      pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptdate, 0 as ptlamt, pd.ptlopc,
      pd.ptcrlo
    FROM stgArkonaPDPPHDR ph
    LEFT JOIN stgArkonaPDPPDET pd ON ph.ptco# = pd.ptco#
      AND ph.ptpkey = pd.ptpkey
    WHERE trim(ph.ptdtyp) = 'RO' 
      AND trim(ph.ptdoc#) <> ''
      AND ph.ptco# IN ('RY1','RY2','RY3')
      AND trim(pd.ptltyp) = 'A'
    GROUP BY ph.ptco#, ph.ptdoc#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
      pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptdate, pd.ptlopc, pd.ptcrlo)x
  group by ptco#, ptro#, ptline
  having count(*) > 1)
AND ph.ptdoc# = '16080603'
order by ph.ptco#, ptro#, ptline, ptltyp

SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16081577'

SELECT *
FROM stgArkonaPDPPHDR
WHERE ptdoc# = '16073492'

SELECT *
FROM stgArkonaPDPPDET
WHERE ptpkey = 428008
  AND ptline = 2
  AND ptltyp = 'A'
ORDER BY ptline, ptltyp

-- multiple A lines, which one to choose
-- A line provides: line date, serv type, pay meth
-- invest because, sometimes diff dates ON the same line
sptro#      ptline    
---------------------
  16061936          1  ptdate <> 0
  16073492          1  ptdate <> 0
  16073492          2  ptdate <> 0
  16080603          1  ptdate <> 0
  16080603          2  either one, dont see a diff, so GROUP AND DO MIN OR MAX ON date?
  16081577          1  ptdate <> 0
  16082833          1  ptdate <> 0
  16082833          2  ptdate <> 0
  16082833          3  ptdate <> 0
  16082833          5  ptdate <> 0
  16086579          7
  16086975          4
  16087374          5
  16087374          9
  16087484          6
  16087514          4
  19096583          3
   2656763          1
   2657818          1
   2658605          1
  36011509          1
  36012081          4