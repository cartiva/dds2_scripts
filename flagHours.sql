
-- now factTechFlagHour  

-- don't need total line hours ON each row, but the flag hours for that date
-- so the attribute becomes ROLineTechFlagDateHours
-- weight IS  lineTechFlagDateHours/LineHours
-- duh, which IS really techFlagDateHours
--DROP TABLE #wtf
-- 1 row ptco#/ptro#/ptline/pttech/flagdate
-- this IS actually line-tech-flagdate hours
SELECT ptco#, ptro#, ptline, pttech, flagdate, SUM(ptlhrs) AS LineTechFlagDateHours
 INTO #wtf
FROM xfmro
  WHERE ptltyp = 'l'
  AND ptcode = 'tt'
  AND ptlhrs > 0
GROUP BY ptco#, ptro#, ptline, pttech, flagdate;

-- why am i joining to factROLine? ahh, to get the LineFlagHours
-- IS weight based ON line-hours OR line-flagdate-hours
-- there IS no such thing AS line-flagdate-hours, because a line can have multiple
-- flagdates, but throw IN tech & we now have line-tech-flagdate-hours
-- line-hours
--DROP TABLE #jon
-- 1 row per ro - line - tech - flagdate
-- DROP TABLE #jon
SELECT a.*, b.lineflaghours, d.* 
INTO #jon
FROM (
  SELECT ptco#, ptro#, ptline, pttech, flagdate, SUM(ptlhrs) AS LineTechFlagDateHours
  -- INTO #wtf
  FROM xfmro
    WHERE ptltyp = 'l'
    AND ptcode = 'tt'
    AND ptlhrs > 0
  GROUP BY ptco#, ptro#, ptline, pttech, flagdate) a
LEFT JOIN FactROLine b ON a.ptco# = b.storecode
  AND a.ptro# = b.ro
  AND a.ptline = b.line
  AND b.LineFlagHours > 0
LEFT JOIN dimtech c ON a.ptco# = c.storecode
  and a.pttech = c.technumber
LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
  AND d.weightfactor = round(a.LineTechFlagDateHours/b.LineFlagHours, 2) 
WHERE a.LineTechFlagDateHours > 0
  AND b.LineFlagHours > 0
--  AND a.ptro# IN ('16080603','16087112','16087381')

-- this IS actually techgroup flag hours
-- use this to generate true tech flag hours
CREATE TABLE factTechFlagHours (
-- change this factTechLineFlagDateHours
-- to factTechLineFlagDateHours
  StoreCode CIChar( 3 ), 
  RO CIChar( 9 ), 
  Line Integer, 
  FlagDateKey Integer, 
  TechGroupKey Integer,
  FlagHours Double) IN database;
  
/* 
-- bingo
SELECT *
FROM #jon
WHERE weightfactor IS NULL 
-- wafuckinghoo
SELECT ptco#, ptro#, ptline, flagdate, techgroupkey
FROM #jon
GROUP BY ptco#, ptro#, ptline, flagdate, techgroupkey
HAVING COUNT(*) > 1
*/

INSERT INTO factTechLineFlagDateHours
SELECT ptco#, ptro#, ptline,
  (SELECT datekey FROM day WHERE thedate = a.flagdate),
  TechGroupKey, ROLineFlagDateHours
FROM #jon a;
-- factTechLineFlagDateHours  
-- use this to generate true tech flag hours
-- which would look LIKE
-- hmm factTechFlagHours: include an employeekey, the correct one could be determined based ON the date
DROP TABLE factTechFlagHours;
CREATE TABLE factTechFlagHours (
  TechKey integer,
  EmployeeKey integer,
  FlagDateKey integer,
  FlagHours double) IN database;


-- DROP TABLE #xxx  
SELECT techkey, 
  (SELECT datekey FROM day WHERE thedate = z.flagdate) AS FlagDateKey, Hours
INTO #xxx
FROM (  
SELECT a.techkey, b.technumber, b.name, a.flagdate, round(SUM(a.LineTechFlagDateHours), 2) AS Hours
FROM #jon a
LEFT JOIN dimtech b ON a.ptco# = b.storecode
  AND a.techkey = b.techkey
GROUP BY a.techkey, a.flagdate, b.technumber, b.name) z;

-- should be the same
SELECT flagdate, pttech, round(sum(ptlhrs), 2) AS ptlhrs
FROM xfmROInitial
WHERE ptltyp = 'L'
  AND ptcode = 'TT'
  AND ptlhrs > 0
GROUP BY flagdate, pttech;  

-- hmm factTechFlagHours: include an employeekey, the correct one could be determined based ON the date
-- which would simplify joining to clockhours, got the datekey, got the emplkey
-- cool
-- problem IS non contiguous datekeys
-- need to SELECT the employeekey based ON date range, NOT datekeyrange
-- CAN NOT DO RANGES BASED ON DATEKEY

INSERT INTO factTechFlagHours  
select a.techkey, coalesce(c.EmployeeKey, -1), a.flagdatekey, a.hours
from #xxx a
LEFT JOIN day z ON a.flagdatekey = z.datekey
LEFT JOIN dimtech b ON a.techkey = b.techkey
LEFT JOIN edwEmployeeDim c ON b.employeenumber = c.employeenumber
  AND z.thedate BETWEEN c.EmployeeKeyFromDate AND c.EmployeeKeyThruDate;
  
tables should be
  TechFlagHoursByDay
the whole factTechLineFlagDateHours seems goofy to me 
DO i need it AS an interim fact?
IF a "fact" TABLE doesn't provide a fact that IS exposed to the END user
does it have to be a physical TABLE?
let's assume NOT
the fact i need to provide IS factTechFlagHoursByDay 
which IS currently called factTechFlagHours
wtf ->jon->factTechLineFlagDateHours
wtf->jon->xxx->factTechFlagHours

/* let's TRY a different way */
SELECT * FROM xfmRO
-- i don't care about ro OR line, just date, tech & hours
-- 6/28, this looks good

SELECT b.datekey, c.techkey, round(sum(ptlhrs), 2) AS ptlhrs
FROM xfmRO a
LEFT JOIN day b ON a.flagdate = b.thedate
LEFT JOIN dimTech c ON a.ptco# = c.storecode
  AND a.pttech = c.technumber
LEFT JOIN edwEmployeeDim d ON c.employeenumber = d.employeenumber
  AND b.thedate BETWEEN d.EmployeeKeyFromDate AND d.EmployeeKeyThruDate  
WHERE ptltyp = 'L'
  AND ptcode = 'TT'
  AND ptlhrs > 0
  AND flagdate IS NOT NULL -- some conversion stragglers
GROUP BY b.datekey, c.techkey

-- so rename the TABLE

-- now the nightly DELETE
-- everything for the date?
-- gettin that gitchy feeling
-- it's ok, because whatever changes IS being replaced
-- IF the time was taken off, it will now be out of the fact TABLE
-- which of course means that whatever facts are derived FROM this
-- will also have to be updated
-- 
DELETE FROM factTechFlagHoursByDay -- del 1486
WHERE EXISTS (
  SELECT 1
  FROM (   
    SELECT techkey, datekey
    FROM xfmRO a
    LEFT JOIN day b ON a.flagdate = b.thedate
    LEFT JOIN dimTech c ON a.ptco# = c.storecode
      AND a.pttech = c.technumber  
    WHERE a.ptltyp = 'L'
      AND a.ptcode = 'TT'
      AND a.ptlhrs > 0
      AND a.flagdate IS NOT NULL     
    GROUP BY techkey, datekey)y
  WHERE y.techkey = factTechFlagHoursByDay.techkey
    AND y.datekey = factTechFlagHoursByDay.flagdatekey);   
    
INSERT INTO factTechFlagHoursByDay -- INSERT 2048 
SELECT c.techkey, coalesce(d.EmployeeKey, -1), b.datekey, round(sum(ptlhrs), 2) AS ptlhrs
FROM xfmRO a
LEFT JOIN day b ON a.flagdate = b.thedate
LEFT JOIN dimTech c ON a.ptco# = c.storecode
  AND a.pttech = c.technumber
LEFT JOIN edwEmployeeDim d ON c.employeenumber = d.employeenumber
  AND b.thedate BETWEEN d.EmployeeKeyFromDate AND d.EmployeeKeyThruDate  
WHERE ptltyp = 'L'
  AND ptcode = 'TT'
  AND ptlhrs > 0
  AND flagdate IS NOT NULL -- some conversion stragglers
GROUP BY c.techkey, coalesce(d.EmployeeKey, -1), b.datekey;

-- uh oh? why would i be deleting 1400 more than inserting  
so repopulate FROM xfmROInitial AND run it again -- 44390

SELECT b.datekey
INTO #DelDates
FROM xfmRO a
LEFT JOIN day b ON a.flagdate = b.thedate
WHERE a.ptltyp = 'L'
  AND a.ptcode = 'TT'
  AND a.ptlhrs > 0
  AND a.flagdate IS NOT NULL
 
SELECT * FROM #DelDates
 
SELECT COUNT(*) -- 3408
FROM factTechFlagHoursByDay
WHERE flagdatekey IN (
  SELECT a.datekey 
  FROM #DelDates a) 
  
SELECT *
INTO #DelRows
FROM factTechFlagHoursByDay
WHERE flagdatekey IN (
  SELECT a.datekey 
  FROM #DelDates a)  

SELECT * FROM #DelRows
  
SELECT techkey, sum(flaghours)
FROM #DelRows
GROUP BY techkey    

DROP TABLE #InsRows

these are the ones generated BY xfmro summed
SELECT techkey, SUM(ptlhrs)
FROM (
SELECT c.techkey, round(sum(ptlhrs), 2) AS ptlhrs
INTO #InsRows
FROM xfmRO a
LEFT JOIN day b ON a.flagdate = b.thedate
LEFT JOIN dimTech c ON a.ptco# = c.storecode
  AND a.pttech = c.technumber
LEFT JOIN edwEmployeeDim d ON c.employeenumber = d.employeenumber
  AND b.thedate BETWEEN d.EmployeeKeyFromDate AND d.EmployeeKeyThruDate  
WHERE ptltyp = 'L'
  AND ptcode = 'TT'
  AND ptlhrs > 0
  AND flagdate IS NOT NULL -- some conversion stragglers
GROUP BY c.techkey, coalesce(d.EmployeeKey, -1), b.datekey)x
GROUP BY techkey
     
     
before i go too far down this rabbit hole
xfmRO does NOT have ALL the transactions for those dates,
only those that have changed, so naturally date IS too wide a swath to cut 

so the records to DELETE become

DELETE FROM factTechFlagHoursByDay -- del 3407
WHERE flagdatekey IN (
  SELECT b.datekey
  FROM xfmRO a
  LEFT JOIN day b ON a.flagdate = b.thedate
  WHERE a.ptltyp = 'L'
    AND a.ptcode = 'TT'
    AND a.ptlhrs > 0
    AND a.flagdate IS NOT NULL);

SELECT *
FROM factTechFlagHoursByDay x
WHERE EXISTS (
  SELECT 1
  FROM xfmRO a
  LEFT JOIN day b ON a.flagdate = b.thedate
  LEFT JOIN dimTech c ON a.ptco# = c.storecode
    AND a.pttech = c.technumber  
  WHERE a.ptltyp = 'L'
    AND a.ptcode = 'TT'
    AND a.ptlhrs > 0
    AND a.flagdate IS NOT NULL
    AND b.datekey = x.flagdatekey
    AND c.techkey = x.techkey)
  

SELECT COUNT(*)
FROM factTechFlagHoursByDay 
WHERE EXISTS (
  SELECT 1
  FROM (   
    SELECT techkey, datekey
    FROM xfmRO a
    LEFT JOIN day b ON a.flagdate = b.thedate
    LEFT JOIN dimTech c ON a.ptco# = c.storecode
      AND a.pttech = c.technumber  
    WHERE a.ptltyp = 'L'
      AND a.ptcode = 'TT'
      AND a.ptlhrs > 0
      AND a.flagdate IS NOT NULL     
    GROUP BY techkey, datekey)y
  WHERE techkey = factTechFlagHoursByDay.techkey
    AND datekey = factTechFlagHoursByDay.flagdatekey)   
    
    
    
