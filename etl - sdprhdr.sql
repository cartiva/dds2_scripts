/*
10/31/12
        added miles
*/        
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'double,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'money,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'sdprhdr' 
-- ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'sdprhdr'  
AND data_type <> 'CHAR'

-- non char field list for spreadsheet
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'sdprhdr'  
AND data_type <> 'CHAR'
      

-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @table_name string;
DECLARE @col string;
DECLARE @cols string;
@table_name = 'sdprhdr';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @table_name);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = (SELECT '[' + TRIM(column_name) + '], '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @table_name);
-- just COLUMN names
  @col = (SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @table_name);
-- parameter names  
--  @col = (SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @table_name);
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;  

-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonasdprhdr';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;


SELECT *
FROM stgArkonaSDPRDET
WHERE ptdate IS NULL 


SELECT * FROM stgArkonaSDPRDET WHERE ptdate IS NULL 

select cast('9999-12-31' as sql_date) FROM system.iota

CREATE TABLE stgArkonaSDPRHDR (
ptco#      cichar(3),          
ptro#      cichar(9),          
ptrtyp     cichar(1),          
ptwro#     integer,              
ptswid     cichar(3),          
ptrtch     cichar(3),          
ptckey     integer,              
ptcnam     cichar(30),         
ptarck     integer,              
ptpmth     cichar(2),          
ptdate     money,              
ptcdat     date,              
ptfcdt     date,              
ptvin      cichar(17),         
pttest     money,              
ptsvco     money,              
ptsvco2    money,              
ptdeda     money,              
ptdeda2    money,              
ptwded     money,              
ptfran     cichar(3),          
ptodom     integer,              
ptmilo     integer,              
ptchk#     cichar(7),          
ptpo#      cichar(12),         
ptrec#     integer,              
ptptot     money,              
ptltot     money,              
ptstot     money,              
ptdedp     money,              
ptsvct     money,              
ptspod     money,              
ptcphz     money,              
ptcpst     money,              
ptcpst2    money,              
ptcpst3    money,              
ptcpst4    money,              
ptwast     money,              
ptwast2    money,              
ptwast3    money,              
ptwast4    money,              
ptinst     money,              
ptinst2    money,              
ptinst3    money,              
ptinst4    money,              
ptscst     money,              
ptscst2    money,              
ptscst3    money,              
ptscst4    money,              
ptcpss     money,              
ptwass     money,              
ptinss     money,              
ptscss     money,              
pthcpn     integer,              
pthdsc     money,              
pttcdc     money,              
ptpbmf     money,              
ptpbdl     money,              
ptauth     cichar(6),          
pttag#     cichar(5),          
ptapdt     date,              
ptdtcm     date,              
ptdtpi     date,              
pttiin     cichar(1),          
ptcreate   timestamp,          
ptdspteam  cichar(10)) IN database;

-- 10/31/12:  miles

-- some Zeros    
SELECT 
  SUM(CASE WHEN ptodom = 0 THEN 1 ELSE 0 END) AS ZeroIn,
  SUM(CASE WHEN ptmilo = 0 THEN 1 ELSE 0 END) AS ZeroOut
-- SELECT COUNT(*)  
FROM stgArkonaSDPRHDR a
WHERE EXISTS (
  SELECT 1
  FROM factro
  WHERE ro = a.ptro#
    AND void = false)     
-- no nulls    
SELECT * FROM stgArkonaSDPRHDR WHERE (ptodom IS NULL OR ptmilo IS NULL)

different cases
ptodom    then
=ptmilo   ptodom
<ptmilo   ptmilo   
>ptmilo   ptodom


SELECT ptro#, ptdate, ptvin, ptodom, ptmilo
INTO #wtf
FROM stgArkonaSDPRHDR a
WHERE EXISTS (
  SELECT 1
  FROM factro
  WHERE ro = a.ptro#
    AND void = false)
AND (ptodom = 0 OR ptmilo = 0)   

-- thinking i need to test for negatives AND nulls
SELECT * FROM #wtf where ptodom = 0
SELECT a.*,
  CASE 
    WHEN ptodom < 0 THEN
      CASE 
        WHEN ptmilo < 0 THEN 0
        ELSE ptmilo
      END
    WHEN ptodom = ptmilo THEN ptodom
    WHEN ptodom < ptmilo THEN ptmilo
    WHEN ptodom > ptmilo THEN ptodom
  END AS miles
FROM #wtf a   
    
SELECT ptro#, ptdate, ptvin, ptodom, ptmilo,
  CASE 
    WHEN (ptodom = 0 AND pt 
FROM stgArkonaSDPRHDR a
WHERE (ptodom < 1 OR ptmilo < 1)  
  AND EXISTS (
  SELECT 1
  FROM factro
  WHERE ro = a.ptro#
    AND void = false)      
  
ALTER TABLE factRO
ADD COLUMN Miles integer;
UPDATE factRO
SET miles = 0;  

ALTER TABLE xfmRO
ADD COLUMN Miles integer;
UPDATE xfmRO
SET miles = 0; 

UPDATE a
  SET Miles = b.miles
-- SELECT *  
FROM factro a
LEFT JOIN (
  SELECT ptro#, 
    CASE 
      WHEN ptodom < 0 THEN
        CASE 
          WHEN ptmilo < 0 THEN 0
          ELSE ptmilo
        END
      WHEN ptodom = ptmilo THEN ptodom
      WHEN ptodom < ptmilo THEN ptmilo
      WHEN ptodom > ptmilo THEN ptodom
    END AS miles 
  FROM stgArkonaSDPRHDR) b ON a.ro = b.ptro# 
WHERE a.void = false 

SELECT ro, miles, ptodom, ptmilo
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
LEFT JOIN stgArkonaSDPRHDR c ON a.ro = c.ptro#
WHERE a.void = false
  AND ptodom <> ptmilo
ORDER BY b.thedate desc  
