need to generate a series of amounts for an employeenumber
FROM the INSERT INTO #jon1
gross amount
  CASE payroll class
       H regularhours * hourlyrate
       S salary/14 * NumDays
overtime amount 
  overtimehours * hourlyrate*1.5
pto amount
  ptohours * hourlyrate
holiday amount
  holidayhours * hourlyrate
fica amount
  CASE payrollclass
       H gross + overtime + vacation + pto + holiday
       S gross
med amount
  CASE payrollclass
       H gross + overtime + vacation + pto + holiday
       S gross
retire amount
  CASE payrollclass
       H gross + overtime + vacation + pto + holiday
       S gross

     
SELECT * -- 1 row per employeekey with total hours of each type for the period with edwEmployeeDim info
FROM ( -- 1 row per employeekey with total hours of each type for the period        
  SELECT a.employeekey, SUM(RegularHours) AS RegularHours, 
    SUM(OvertimeHours) AS OvertimeHours, SUM(VacationHours) AS VacationHours,
    SUM(PTOHours) AS PTOHours, SUM(HolidayHours) AS HolidayHours
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
    AND b.thedate BETWEEN '07/14/2013' AND '07/31/2013'
  GROUP BY a.Employeekey) a
LEFT JOIN edwEmployeeDim b on a.employeekey = b.employeekey -- excluded employees based on emp# & distcode
    AND b.storecode <> 'RY3'
    AND b.employeenumber NOT IN ('190915','1160100')
    AND b.PayPeriodCode = 'B'
    AND b.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')
WHERE b.storecode IS NOT NULL  
 
-- !!! Shane Anderson IS showing up AND should NOT - terminated salary people will NOT be excluded
-- !!! based on clockhours  ::  thinking maybe DO hourly AND salaried separately
--                              so it becomes a UNION of 3: hourly, salaried, commission
      /*
          what the fuck joel dangerfield, bev, & david pederson ALL are salaried, buy have clockhours AND Overtime ????
          ok, ctfd, they are NOT being paid overtime OR hourly, they are salaried, but for whatever reason, they clock IN AND out
      */
-- hourly only, IN the context of which it makes sense that IF there IS clock time, i need that employeekey   
-- beware of premature grouping  
-- 1 row per emp#

--  dist code ?? same employeenumber could have multiple distcodes IN the same period
-- which edwEmployeeDim recognizes but arkona doesn't
--< hourly ------------------------------------------------------------------------------------------------------------------
/*
@hourly1 gives me the employeekey, various pay amounts AND distribution accounts
*/
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @NumDays integer;
@fromDate = '07/14/2013';
@thrudate = '07/27/2013';
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1;     
-- DROP TABLE #hourly1     
SELECT a.employeekey, c.employeenumber, 
  HourlyRate, RegularHours, OvertimeHours, VacationHours, PTOHours, HolidayHours,
  RegularHours*HourlyRate as RegularPay, 
  OvertimeHours*HourlyRate*1.5 AS OvertimePay, VacationHours*HourlyRate AS VacationPay,
  PTOHours*HourlyRate AS PTOPay, HolidayHours*HourlyRate AS HolidayPay, 
  RegularHours*HourlyRate + VacationHours*HourlyRate + PTOHours*HourlyRate + HolidayHours*HourlyRate AS TotalNonOTPay,
  d.DIST_CODE, d.SEQ_NUMBER, d.GROSS_DIST, d.GROSS_EXPENSE_ACT_, d.OVERTIME_ACT_, 
  d.VACATION_EXPENSE_ACT_, d.HOLIDAY_EXPENSE_ACT_, d.SICK_LEAVE_EXPENSE_ACT_,
  d.EMPLR_FICA_EXPENSE, d.EMPLR_MED_EXPENSE, d.EMPLR_CONTRIBUTIONS,
  e.yficmp, e.ymedmp, coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
  coalesce(h.FIXED_DED_AMT, 0) AS FIXED_DED_AMT
INTO #hourly1
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
  AND b.thedate BETWEEN @fromDate AND @thruDate  
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey -- excluded employees based on emp# & distcode
    AND c.storecode <> 'RY3'
    AND c.employeenumber NOT IN ('190915','1160100')
    AND c.PayPeriodCode = 'B'
    AND c.PayRollClassCode = 'H'
    AND c.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')
LEFT JOIN stgArkonaPYACTGR d ON c.StoreCode = d.COMPANY_NUMBER -- distribution
  AND c.DistCode = d.DIST_CODE
  AND d.CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL e ON c.storecode = e.yco#
  AND e.ycyy = 100 + (year(@ThruDate) - 2000) -- 112 -  
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) f ON c.employeenumber = f.EMPLOYEE_NUMBER    
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) g ON c.employeenumber = g.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT h ON c.storecode = h.COMPANY_NUMBER
  AND c.employeenumber = h.EMPLOYEE_NUMBER 
  AND h.DED_PAY_CODE IN ('91', '99');    

-- aha, here we go, mult dist codes for an emp#  
SELECT employeenumber
FROM (
SELECT employeenumber, dist_code
FROM #new1
GROUP BY employeenumber, dist_code) x
GROUP BY employeenumber HAVING COUNT(*) > 1

-- 9/5 so keep working at the empkey level
-- DROP TABLE #hourly2
select EmployeeNumber as Control, Account, round(sum(Amount),2) as Amount
INTO #hourly2
FROM (
  SELECT 'Gross', employeenumber, GROSS_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 * RegularPay AS Amount
  FROM #hourly1
  UNION ALL
  SELECT 'OT', employeenumber, OVERTIME_ACT_ AS Account,
    GROSS_DIST/100.0 * OvertimePay AS Amount
  FROM #hourly1
  UNION ALL 
  SELECT 'VAC', employeenumber, VACATION_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 * VacationPay AS Amount
  FROM #hourly1
  UNION ALL 
  SELECT 'PTO', employeenumber, SICK_LEAVE_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 * PTOPay AS Amount
  FROM #hourly1
  UNION ALL 
  SELECT 'HOL', employeenumber, HOLIDAY_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 * HolidayPay AS Amount
  FROM #hourly1
  UNION ALL
  SELECT 'FICA', employeenumber, EMPLR_FICA_EXPENSE AS Account,
    GROSS_DIST/100.0 * round((TotalNonOTPay + OvertimePay - coalesce(FicaEx, 0)) * yficmp/100.0, 2) AS Amount
  FROM #hourly1
  UNION ALL 
  SELECT 'MEDIC', employeenumber, EMPLR_MED_EXPENSE AS Account,
    GROSS_DIST/100.0 * round((TotalNonOTPay + OvertimePay - coalesce(MedEx, 0)) * ymedmp/100.0, 2) AS Amount
  FROM #hourly1
  UNION ALL 
  SELECT 'RETIRE', employeenumber, EMPLR_CONTRIBUTIONS AS Account,
      CASE 
        WHEN FIXED_DED_AMT IS NULL THEN 0
        ELSE 
        GROSS_DIST/100.0 *
              CASE 
                WHEN (TotalNonOTPay + OvertimePay) * FIXED_DED_AMT/200.0 -- 1/2 of employee contr
                    < .02 * (TotalNonOTPay + OvertimePay) 
                  THEN round((TotalNonOTPay + OvertimePay) * FIXED_DED_AMT/200.0, 2)
                ELSE round(.02 * (TotalNonOTPay + OvertimePay), 2) 
              END  
      END AS Amount     
  FROM #hourly1) x
WHERE Amount > 0
GROUP BY employeenumber, Account;  
--/> hourly ------------------------------------------------------------------------------------------------------------------
--< salaried ------------------------------------------------------------------------------------------------------------------
-- ok, june has 2 rows (changed salary, etc IN the middle of july)
so for what number of days was she each empkey
salary/14 = daily pay
still need a number of days for each employeekey, ie @numdays will be 14 OR less
but will have to somehow ADD ALL those values?    
AGAIN beware of premature grouping, DO ALL the WORK on the empkey THEN GROUP at the END, just LIKE hourly       

-- DROP TABLE #salary1;   
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @NumDays integer;
@fromDate = '07/28/2013';
@thrudate = '08/10/2013';
SELECT a.employeekey, a.employeenumber, a.TotalPay, NumberOfDays, 
  d.DIST_CODE, d.SEQ_NUMBER, d.GROSS_DIST, d.GROSS_EXPENSE_ACT_, d.OVERTIME_ACT_, 
  d.VACATION_EXPENSE_ACT_, d.HOLIDAY_EXPENSE_ACT_, d.SICK_LEAVE_EXPENSE_ACT_,
  d.EMPLR_FICA_EXPENSE, d.EMPLR_MED_EXPENSE, d.EMPLR_CONTRIBUTIONS,
  e.yficmp, e.ymedmp, coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
  coalesce(h.FIXED_DED_AMT, 0) AS FIXED_DED_AMT
INTO #salary1
FROM (
  SELECT a.storecode, a.employeenumber, a.name, a.employeekey, a.payrollclasscode, 
      a.PayPeriodCode, a.DistCode,
      CASE 
        WHEN a.employeekeyfromdate <= @fromDate AND a.employeekeythrudate >= @thrudate THEN round((a.salary/14) * (timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1), 2)
        -- emp key starts before @fromDate AND ends before @thruDate
        WHEN a.employeekeyfromdate < @fromDate AND a.employeekeythrudate < @thrudate THEN round((a.salary/14) * (timestampdiff(sql_tsi_day, @FromDate, a.employeekeythrudate) + 1),2)
        -- emp key starts after @fromDate AND ends ON OR BEFORE @thruDate
        WHEN a.employeekeyfromdate > @fromDate AND a.employeekeythrudate <= @thrudate THEN round((a.salary/14) * (timestampdiff(sql_tsi_day, a.employeekeyFromDate, a.employeekeythrudate) + 1),2)
        -- emp key starts after @fromDate AND ends AFTER @thruDate
        WHEN a.employeekeyfromdate > @fromDate AND a.employeekeythrudate > @thrudate THEN round((a.salary/14) * (timestampdiff(sql_tsi_day, a.employeekeyFromDate, @thruDate) + 1),2)
        ELSE 0
      END AS TotalPay,
      CASE 
        WHEN a.employeekeyfromdate <= @fromDate AND a.employeekeythrudate >= @thrudate THEN timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1
        -- emp key starts before @fromDate AND ends before @thruDate
        WHEN a.employeekeyfromdate < @fromDate AND a.employeekeythrudate < @thrudate THEN timestampdiff(sql_tsi_day, @FromDate, a.employeekeythrudate) + 1
        -- emp key starts after @fromDate AND ends ON OR BEFORE @thruDate
        WHEN a.employeekeyfromdate > @fromDate AND a.employeekeythrudate <= @thrudate THEN timestampdiff(sql_tsi_day, a.employeekeyFromDate, a.employeekeythrudate) + 1
        -- emp key starts after @fromDate AND ends AFTER @thruDate
        WHEN a.employeekeyfromdate > @fromDate AND a.employeekeythrudate > @thrudate THEN timestampdiff(sql_tsi_day, a.employeekeyFromDate, @thruDate) + 1
      END AS NumberOfDays   
  FROM edwEmployeeDim a
  INNER JOIN edwEmployeeDim b on a.employeekey =  b.employeekey -- ALL employeekeys valid for the interval
    AND b.employeekeyFromDate < @thruDate
    AND b.employeekeyThruDate > @FromDate
  WHERE a.hiredate <= @thruDate -- only active folks for the interval
    AND a.termdate >= @fromDate
    AND a.storecode <> 'ry3'  
    AND a.employeenumber NOT IN ('190915','1160100')
    AND a.PayPeriodCode = 'B'
    AND a.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV') 
    AND a.PayRollClassCode = 'S') a 
LEFT JOIN stgArkonaPYACTGR d ON a.StoreCode = d.COMPANY_NUMBER -- distribution
  AND a.DistCode = d.DIST_CODE
  AND d.CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL e ON a.storecode = e.yco#
  AND e.ycyy = 100 + (year(@ThruDate) - 2000) -- 112 -  
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) f ON a.employeenumber = f.EMPLOYEE_NUMBER    
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) g ON a.employeenumber = g.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT h ON a.storecode = h.COMPANY_NUMBER
  AND a.employeenumber = h.EMPLOYEE_NUMBER 
  AND h.DED_PAY_CODE IN ('91', '99');
  
SELECT * FROM #salary1 WHERE numberofdays <> 18  
  
-- DROP TABLE #salary2
select EmployeeNumber as Control, Account, round(sum(Amount),2) as Amount
--INTO #salary2
FROM (
  SELECT 'Gross', employeenumber, GROSS_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 * TotalPay AS Amount
  FROM #salary1
  UNION ALL 
  SELECT 'FICA', employeenumber, EMPLR_FICA_EXPENSE AS Account,
    round((GROSS_DIST/100.0) * (TotalPay - (coalesce(FicaEx, 0)) * (NumberOfDays/14)) * yficmp/100.0, 2) AS Amount
  FROM #salary1
ORDER BY Employeenumber  
) x

  SELECT 'FICA', employeenumber, EMPLR_FICA_EXPENSE AS Account,
   round((GROSS_DIST/100.0) * (TotalPay - coalesce(FicaEx, 0)) * (NumberOfDays/14) * yficmp/100.0, 2)
  FROM #salary1
  UNION ALL 
  SELECT 'MEDIC', employeenumber, EMPLR_MED_EXPENSE AS Account,
    GROSS_DIST/100.0 *  round(((TotalPay * @NumDays) - (coalesce(MedEx, 0)) * @NumDays/14) * ymedmp/100.0, 2) AS Amount
  FROM #salary1
  UNION ALL 
  SELECT 'RETIRE', employeenumber, EMPLR_CONTRIBUTIONS AS Account,
      CASE 
        WHEN FIXED_DED_AMT IS NULL THEN 0
        ELSE 
        GROSS_DIST/100.0 *
          CASE PayrollClassCode
            WHEN 'H' THEN 
              CASE 
                WHEN (((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) * FIXED_DED_AMT/200.0 -- 1/2 of employee contr
                    < .02 * (((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) 
                  THEN round((((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) * FIXED_DED_AMT/200.0, 2)
                ELSE round(.02 * (((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)), 2) 
              END  
            WHEN 'S' THEN 
              CASE  
                WHEN ((salary/14) * @NumDays) * (FIXED_DED_AMT/200.0) < .02 * ((salary/14) * @NumDays) THEN round((salary/14 * @NumDays) * (FIXED_DED_AMT/200.0), 2)
                ELSE round((.02 * (salary/14) * @NumDays), 2)                  
              END 
          END 
      END AS Amount     
      
      
  FROM #salary1 ) x
WHERE Amount > 0
GROUP BY employeenumber, Account;
   
      
      
      
      
      
  FROM #hourly1) x
WHERE Amount > 0
GROUP BY employeenumber, Account;      

--/> salaried ------------------------------------------------------------------------------------------------------------------



/*I
124704 main shop adjusted cost of labor
SELECT *
FROM #hourly2 a
LEFT JOIN #jon1 b on a.control = b.control AND a.account = b.account
WHERE a.amount - b.amount > 1

SELECT a.*, b.*, round(a.amount - b.amount, 2)
FROM (
SELECT control, SUM(amount) AS amount
FROM #hourly2
WHERE account = '124704'
GROUP BY control) a
LEFT JOIN (
SELECT control, SUM(amount) AS amount
FROM #jon1
GROUP BY control) b on a.control = b.control
WHERE a.amount - b.amount > 10
ORDER BY round(a.amount - b.amount, 2)

SELECT * FROM #hourly1 WHERE employeenumber = '1106260'
SELECT * FROM stgArkonaPYACTGR WHERE emplr_fica_expense <> emplr_med_Expense AND company_number = 'ry1'

SELECT SUM(amount)
FROM #hourly2
WHERE account = '124704'

SELECT SUM(amount)
FROM #jon1
WHERE account = '124704'

SELECT SUM(amount) FROM zaccrual WHERE date = '07/31/2013' AND account = '124704'

SELECT a.control, a.amount, b.* FROM zaccrual a left join #jon1 b on a.control = b.control and b.account = '124704' WHERE a.date = '07/31/2013' AND a.account = '124704' AND a.amount <> b.amount 
*/

/* 9/4/2:55      
-- so scrap this, redo it for hourly only      
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @NumDays integer;
@fromDate = '07/14/2013';
@thrudate = '07/31/2013';
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1;     
--SELECT * -- 1 row per employeekey with total hours of each type for the period with edwEmployeeDim info
SELECT a.employeekey, b.employeenumber, b.name, 
  PayRollClass, RegularHours,
  CASE WHEN b.PayRollClassCode = 'H' THEN a.regularHours * b.HourlyRate END AS RegularPay,
  CASE WHEN b.PayRollClassCode = 'S' THEN round((b.Salary*14)/@NumDays, 2) END AS SalaryPay,
  OverTimeHours * HourlyRate * 1.5 AS OvertimePay, VacationHours * HourlyRate AS VacationPay,
  PTOHours * HourlyRate AS PTOPay, HolidayHours * HourlyRate AS HolidayPay,
  (RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate AS HourlyNonOTTotalPay
FROM ( -- 1 row per employeekey with total hours of each type for the period        
  SELECT a.employeekey, SUM(RegularHours) AS RegularHours, 
    SUM(OvertimeHours) AS OvertimeHours, SUM(VacationHours) AS VacationHours,
    SUM(PTOHours) AS PTOHours, SUM(HolidayHours) AS HolidayHours
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
    AND b.thedate BETWEEN @fromDate AND @thruDate
  GROUP BY a.Employeekey) a
LEFT JOIN edwEmployeeDim b on a.employeekey = b.employeekey -- excluded employees based on emp# & distcode
    AND b.storecode <> 'RY3'
    AND b.employeenumber NOT IN ('190915','1160100')
    AND b.PayPeriodCode = 'B'
    AND b.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')
WHERE b.storecode IS NOT NULL     
ORDER BY payrollclass, name    
 */
 
 -- 9/6 starting over -------------------------------------------------------
 
--< Salaried employees ------------------------------------------------------------------------------------------------------
DROP TABLE #salaried;
DROP TABLE #salaried2;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @NumDays integer;
-- actual payperiod, check date: 8/2
@fromDate = '09/08/2013';
@thrudate = '09/21/2013';
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1;
SELECT a.storecode, a.employeenumber, a.name, a.employeekey, a.payrollclasscode, 
  a.DistCode, a.salary, a.salary * @NumDays/14 AS TotalPay,
  d.DIST_CODE, d.SEQ_NUMBER, d.GROSS_DIST, d.GROSS_EXPENSE_ACT_, d.OVERTIME_ACT_, 
  d.VACATION_EXPENSE_ACT_, d.HOLIDAY_EXPENSE_ACT_, d.SICK_LEAVE_EXPENSE_ACT_,
  d.EMPLR_FICA_EXPENSE, d.EMPLR_MED_EXPENSE, d.EMPLR_CONTRIBUTIONS,
  e.yficmp, e.ymedmp, coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
  coalesce(h.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
INTO #salaried  
FROM edwEmployeeDim a
INNER JOIN ( -- the possibly dodgy assumption here IS that employeekeys are sequential, but they are autoinc 
  SELECT a.Employeenumber, MAX(a.employeekey) AS employeekey
  FROM edwEmployeeDim a
  INNER JOIN edwEmployeeDim b on a.employeekey =  b.employeekey -- ALL employeekeys valid for the interval
    AND b.employeekeyFromDate < @thruDate
    AND b.employeekeyThruDate > @FromDate
  WHERE a.hiredate <= @thruDate -- only folks active during the interval
    AND a.termdate >= @fromDate
    AND a.storecode <> 'ry3'  
    AND a.employeenumber NOT IN ('190915','1160100')
    AND a.PayPeriodCode = 'B'
    AND a.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 
      'BSM','SALE','TEAM','USCM','USCD', 'STEV') 
    AND a.PayRollClassCode = 'S' -- salaried only
  GROUP BY a.employeenumber) x on a.employeekey = x.employeekey
LEFT JOIN stgArkonaPYACTGR d ON a.StoreCode = d.COMPANY_NUMBER -- distribution
  AND a.DistCode = d.DIST_CODE
  AND d.CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL e ON a.storecode = e.yco#
  AND e.ycyy = 100 + (year(@ThruDate) - 2000) -- 112 -  
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) f ON a.employeenumber = f.EMPLOYEE_NUMBER    
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) g ON a.employeenumber = g.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT h ON a.storecode = h.COMPANY_NUMBER
  AND a.employeenumber = h.EMPLOYEE_NUMBER 
  AND h.DED_PAY_CODE IN ('91', '99');
  
SELECT *
INTO #salaried2
FROM (
  SELECT 'Gross', employeenumber, GROSS_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 * TotalPay AS Amount
  FROM #salaried
  UNION ALL 
  SELECT 'FICA', employeenumber, EMPLR_FICA_EXPENSE AS Account,
    round((GROSS_DIST/100.0) * (TotalPay - (coalesce(FicaEx, 0)) * (@NumDays/14)) * yficmp/100.0, 2) AS Amount
  FROM #salaried
  UNION ALL 
  SELECT 'MEDIC', employeenumber, EMPLR_MED_EXPENSE AS Account,
    round((GROSS_DIST/100.0) *  (TotalPay - (coalesce(MedEx, 0)) * (@NumDays/14)) * ymedmp/100.0, 2) AS Amount
  FROM #salaried
  UNION ALL 
  SELECT 'RETIRE', employeenumber, EMPLR_CONTRIBUTIONS AS Account,
  CASE 
    WHEN FIXED_DED_AMT IS NULL THEN 0
    ELSE 
    CASE 
      WHEN (TotalPay * FIXED_DED_AMT/200.0) < .02 * TotalPay 
        THEN round((GROSS_DIST/100.0 * TotalPay) * (FIXED_DED_AMT/200.0), 2)
      ELSE round((Gross_DIST/100.0) * TotalPay * .02, 2) 
    END               
  END AS Amount  
  FROM #salaried) x

SELECT * FROM #salaried2  
SELECT b.ymname, a.* FROM #salaried2 a LEFT JOIN stgArkonaPYMAST b on a.employeenumber = b.ymempn ORDER BY ymname
--/> Salaried employees ------------------------------------------------------------------------------------------------------

--< Clock Hours ------------------------------------------------------------------------------------------------------ 
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @NumDays integer;
-- actual payperiod, check date: 8/2
@fromDate = '09/13/2013';
@thrudate = '09/28/2013';
DROP TABLE #clockhours;
SELECT c.employeenumber, c.name, SUM(regularHours) AS regularHours, 
  SUM(OvertimeHours) AS Overtimehours, SUM(VacationHours) AS VacationHours,
  SUM(PTOHours) AS PTOHours, SUM(HolidayHours) AS HolidayHours
INTO #clockhours  
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
  AND b.thedate BETWEEN @fromDate AND @thruDate  
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey  
WHERE regularhours + overtimehours + vacationhours + ptohours + holidayhours > 0
  AND NOT EXISTS ( -- emp# NOT IN #salaried
    SELECT 1
    FROM #salaried
    WHERE employeenumber = c.employeenumber)
GROUP BY c.employeenumber, c.name;
--/> Clock Hours ------------------------------------------------------------------------------------------------------
--< Hourly ------------------------------------------------------------------------------------------------------
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @NumDays integer;
-- actual payperiod, check date: 8/2
@fromDate = '07/14/2013';
@thrudate = '07/27/2013';
--DROP TABLE #hourly;
SELECT b.storecode, b.employeenumber, b.name, b.employeekey, b.payrollclasscode, 
  b.distcode, b.hourlyrate  
INTO #hourly  
FROM #clockhours a
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
INNER JOIN (
  SELECT employeenumber, MAX(employeekey) AS employeekey
  FROM edwEmployeeDim
  WHERE EmployeeKeyFromDate <= @ThruDate
    AND storecode <> 'RY3'
    AND PayPeriodCode = 'B'
    AND PayRollClassCode = 'H'
    AND employeenumber NOT IN ('190915','1160100')
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')   
  GROUP BY EmployeeNumber) c on b.employeekey = c.employeekey;
--/> Hourly ------------------------------------------------------------------------------------------------------

--< Commission ------------------------------------------------------------------------------------------------------
should deductions for commision be doubled because the commission IS for the entire month ???? NO it IS for one biweeky pay period
--/> Commission  ------------------------------------------------------------------------------------------------------