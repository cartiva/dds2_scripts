/*
SELECT e.storecode, e.employeenumber, e.name, e.payrollclasscode, e.hourlyrate, e.salary,
  e.DistCode, coalesce(sum(RegularHours), 0) AS RegularHours, coalesce(SUM(OvertimeHours), 0) AS OvertimeHours,
  coalesce(sum(VacationHours), 0) AS VacationHours, 
  coalesce(sum(PTOHours), 0) AS PTOHours, coalesce(sum(HolidayHours), 0) AS HolidayHours,
  Max(FromDate) AS FromDate, max(ThruDate) AS ThruDate, max(NumDays) AS NumDays
-- SELECT *  
-- DROP TABLE #wtf
INTO #wtf
FROM edwEmployeeDim e
INNER JOIN ( -- h: FromDate, ThruDate, NumDays
  SELECT y.thedate AS FromDate, d.thedate AS ThruDate, timestampdiff(sql_tsi_day, y.thedate, d.thedate) + 1 AS NumDays
  FROM ( -- y 
    SELECT cast(timestampadd(sql_tsi_day, 1, MAX(payrollendingdate)) AS sql_date) AS thedate
    FROM ( -- x
      SELECT payrollendingdate, COUNT(*)
      FROM vpyhshdta
      WHERE payperiod = 'B'
      GROUP BY payrollendingdate
        HAVING COUNT(*) > 3) x
    GROUP BY year(payrollendingdate), month(payrollendingdate)) y  
  LEFT JOIN day d ON year(y.thedate) = year(d.thedate)
    AND month(y.thedate) = month(d.thedate)
    AND d.LastDayofMonth = true
  WHERE d.thedate = '12/31/2011') h ON e.EmployeeKeyFromDate <= h.ThruDate AND employeekeythrudate >= h.FromDate
LEFT JOIN ( -- ch
  SELECT f.*, d.thedate
  FROM edwClockHoursFact f
  LEFT JOIN day d ON f.datekey = d.datekey
  WHERE (clockhours + vacationhours + ptohours + holidayhours) <> 0) ch ON e.EmployeeKey = ch.EmployeeKey AND ch.thedate BETWEEN h.FromDate AND h.ThruDate  
WHERE e.payperiodcode = 'B'  
  and e.DistCode not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD') 
GROUP BY e.storecode, e.employeenumber, e.name, e.payrollclasscode, e.DistCode, e.hourlyrate, e.salary


-- looks ok
SELECT storecode, employeenumber, name, distcode,
  CASE PayrollClassCode
    WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)
    WHEN 'S' THEN salary * (NumDays*1.0/Dayofmonth(thrudate))
  END AS GrossPay,
  CASE PayrollClassCode
    WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5) 
    WHEN 'S' THEN salary * (NumDays*1.0/Dayofmonth(thrudate))
  END * 0.062 AS FICA, 
  CASE PayrollClassCode
    WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5) 
    WHEN 'S' THEN salary * (NumDays*1.0/Dayofmonth(thrudate))
  END * 0.0145 AS Medicare 
FROM #wtf w
LEFT JOIN stgArkonaPYACTGR a ON a.ytaco# = w.storecode
  AND a.ytadic = w.DistCode

SELECT * FROM #wtf
SELECT x.*,   
  ytagpa as GrossAccount, ytaeta as FICAAccount, ytamed as MedicareAccount,
  round(GrossPay * ytagpp / 100, 2) as Gross, round(FICA * ytagpp / 100, 2) as FICA, 
  round(Medicare * ytagpp / 100, 2) as Medicare  
FROM (
  SELECT storecode, employeenumber, name, distcode,
    CASE PayrollClassCode
      WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)
      WHEN 'S' THEN salary * (NumDays*1.0/Dayofmonth(thrudate))
    END AS GrossPay,
    CASE PayrollClassCode
      WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5) 
      WHEN 'S' THEN salary * (NumDays*1.0/Dayofmonth(thrudate))
    END * 0.062 AS FICA, 
    CASE PayrollClassCode
      WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5) 
      WHEN 'S' THEN salary * (NumDays*1.0/Dayofmonth(thrudate))
    END * 0.0145 AS Medicare, a.*
  FROM #wtf w
  LEFT JOIN stgArkonaPYACTGR a ON a.ytaco# = w.storecode
    AND a.ytadic = w.DistCode) x     

WHERE employeenumber = '1120225'

-- 2/8
-- floundering
SELECT name, payrollclasscode, distcode
FROM edwEmployeeDim e
WHERE PayPeriodCode = 'B'
  AND DistCode IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD') 
  
-- the dates, based ON lastday of month
SELECT y.thedate AS FromDate, d.thedate AS ThruDate, timestampdiff(sql_tsi_day, y.thedate, d.thedate) + 1 AS NumDays
FROM ( -- y 
  SELECT cast(timestampadd(sql_tsi_day, 1, MAX(payrollendingdate)) AS sql_date) AS thedate
  FROM ( -- x
    SELECT payrollendingdate, COUNT(*)
    FROM vpyhshdta
    WHERE payperiod = 'B'
    GROUP BY payrollendingdate
      HAVING COUNT(*) > 3) x
  GROUP BY year(payrollendingdate), month(payrollendingdate)) y  
LEFT JOIN day d ON year(y.thedate) = year(d.thedate)
  AND month(y.thedate) = month(d.thedate)
  AND d.LastDayofMonth = true
WHERE d.thedate = '12/31/2011'  

-- except now my assumptions about the last payroll before eof seem to be challenged BY 3/2011
SELECT *
from vpyhshdta
WHERE payrollendingdate = '03/17/2011'

    SELECT yhdco#, payrollendingdate, COUNT(*)
    FROM vpyhshdta
    WHERE payperiod = 'B'
    GROUP BY yhdco#,payrollendingdate
    ORDER BY yhdco#,payrollendingdate DESC 

*/
    
-- conclusion, the data can NOT be used to absolutely determine the last payrollendingdate  
-- across the entire organizaion
-- the date will have to be entered AS a parameter  
-- the last payrollending date IS used to determine the hours worked BETWEEN that date
-- AND the EOM, this IS what need to be accrued
-- temp tables: #emp, #clockhours, #totals
DECLARE @ThruDate date; // Last day of month
DECLARE @FromDate date; // Last payroll ending date prior to EOM
DECLARE @NumDays integer;
@FromDate = '1/14/2012';
@ThruDate = (
  SELECT thedate 
  FROM day WHERE LastDayOfMonth = true 
    AND month(thedate) = month(@FromDate) 
    AND year(thedate) = year(@FromDate));
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate);

-- Employees paid bi-weekly, not IN distcode exclusion list, that were actively employed at
--   any time during the interval: @FromDate + 1 thru @ThruDate 
/* #emp 1 row per employeekey */
-- DROP TABLE #emp
SELECT *
INTO #Emp
--SELECT COUNT(*)
FROM edwEmployeeDim 
WHERE PayPeriodCode = 'B'
  AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
  AND hiredate <= @thrudate
  AND termdate >= @fromdate;
/*
an employeekey interval of validity has nothing to DO with active employment 
what about an employee that IS active for only part of the interval
what determines that an emp was actively employed at any time during any interval  
ActiveCode doesn't WORK because the replaced row gets updated to Inactive
hiredate <= @thrudate
termdate >= @fromdate
so i want ALL the rows (empkeys) WHERE hiredate->termdate overlaps from->thru
*/


-- looking for clock hours for the interval 
-- narrow this down to just those edwClockHoursFact records with time
/* #ClockHours: 1 row per employeekey/datekey 
SELECT f.*
INTO #ClockHours
FROM day d
INNER JOIN edwClockHoursFact f ON d.DateKey = f.DateKey
WHERE d.thedate BETWEEN @FromDate + 1 AND @ThruDate
  AND f.ClockHours + f.VacationHours + f.PTOHours + f.HolidayHours <> 0;
*/

-- for the period store, EmpKey, DistCode, GrossPay + FICA + Medicare
--DROP TABLE #totals
/* #total: 1 row per store/employeekey/distcode */
/*
SELECT storecode, employeekey, distcode, SUM(GrossPay + FICA + Medicare) AS Total
INTO #totals
FROM (
  SELECT e.storecode, e.employeekey, e.distcode,
    SUM(
      CASE PayrollClassCode
        WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)
        WHEN 'S' THEN salary * (@NumDays*1.0/Dayofmonth(@thrudate))
      END) AS GrossPay,
    SUM(  
      CASE PayrollClassCode
        WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5) 
        WHEN 'S' THEN salary * (@NumDays*1.0/Dayofmonth(@thrudate))
      END) * 0.062 AS FICA, 
    SUM(  
      CASE PayrollClassCode
        WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5) 
        WHEN 'S' THEN salary * (@NumDays*1.0/Dayofmonth(@thrudate))
      END) * 0.0145 AS Medicare
  FROM #emp e
  INNER JOIN #ClockHours c ON e.employeekey = c.employeekey    
  GROUP BY e.storecode, e.employeekey, distcode) x
GROUP BY storecode, employeekey, distcode
*/

/* uh oh */
-- SELECT COUNT(*) FROM ( -- emp#: 268, empkey: 270
-- looking LIKE i have to DO distribution ON employeekey rather than employeenumber
-- the same employee could have multiple distcodes within the interval
-- for the interval of 1/5 -> 1/31 , r. hunter changed FROM distcode ptec to pdqw, which have different gross accounts
/*
SELECT storecode, name, e.employeekey, employeenumber, distcode    
FROM #emp e
INNER JOIN #ClockHours c ON e.employeekey = c.employeekey    
WHERE e.employeekey IN (
  SELECT employeekey
  FROM #emp
  WHERE employeenumber IN (
    SELECT employeenumber
    FROM #emp
    GROUP BY employeenumber
    HAVING COUNT(*) > 1))
GROUP BY storecode, e.employeekey, employeenumber, name, distcode
ORDER BY name
*/

-- now what i want IS one row per employeekey/account/store
SELECT t.*, p.ytaco#, ytagpp,  ytagpa as GrossAccount, 
      ytaeta as FICAAccount, ytamed as MedicareAccount
FROM #totals t
LEFT JOIN stgArkonaPYACTGR p ON t.storecode = p.ytaco#
  AND t.DistCode = p.ytadic
ORDER BY distcode

SELECT *
FROM #totals t

SELECT *
FROM stgArkonapyhshdta

SELECT *
FROM edwEmployeeDim
WHERE employeenumber = '146450'

  
SELECT *
FROM #emp

SELECT *
FROM #ClockHours

SELECT *
FROM #totals t
LEFT JOIN stgArkonaPYACTGR p ON t.storecode = p.ytaco#
  AND t.distcode = p.ytadic
WHERE storecode = 'RY2'
  AND distcode = 'OFFI'  

ytagpa: gross
ytaeta: fica
medicare: ytamed

SELECT * FROM stgArkonaPYCNTRL  
-- hmm, separate TABLE for gross, fica, medicare, retirement
SELECT *
FROM stgArkonaPYACTGR
WHERE ytaco# = 'RY2'
AND ytadic = 'OFFI'

/******* 2/12 ******************************************************************/

-- SELECT * FROM stgArkonaPYDEDUCT
-- SELECT * FROM stgArkonaPYACTGR ORDER BY ytaco#, ytadic
DECLARE @ThruDate date; // Last day of month
DECLARE @FromDate date; // Last payroll ending date prior to EOM
DECLARE @NumDays integer;
@FromDate = '1/14/2012';
@ThruDate = (
  SELECT thedate 
  FROM day WHERE LastDayOfMonth = true 
    AND month(thedate) = month(@FromDate) 
    AND year(thedate) = year(@FromDate));
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate);
SELECT name, e.employeekey, storecode, employeenumber,
  payrollclasscodedesc, distcode,
  CASE PayrollClassCode
    WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)
    WHEN 'S' THEN salary * (@NumDays*1.0/DayOfMonth(@thrudate))
  END AS GrossPay,
  CASE PayrollClassCode
    WHEN 'H' THEN (((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * p.yficmp/100.0
    WHEN 'S' THEN (salary * (@NumDays*1.0/DayOfMonth(@thrudate))) * p.yficmp/100.0
  END AS FICA,
  CASE PayrollClassCode
    WHEN 'H' THEN (((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * p.ymedmp/100.0
    WHEN 'S' THEN (salary * (@NumDays*1.0/DayOfMonth(@thrudate))) * p.ymedmp/100.0
  END AS Medicare,
  CASE PayrollClassCode
    WHEN 'H' THEN (((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * d.yddamt/100.0
    WHEN 'S' THEN (salary * (@NumDays*1.0/DayOfMonth(@thrudate))) * d.yddamt/100.0
  END AS Retirement
-- DROP TABLE #wtf  
 INTO #wtf  
FROM ( -- bi-weekly employeekeys employed during the interval
  SELECT *
  FROM edwEmployeeDim 
  WHERE PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
    AND hiredate <= @thrudate
    AND termdate >= @fromdate) e
LEFT JOIN ( -- clock hours per employeekey for the interval
  SELECT employeekey, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
    sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
    sum(holidayhours) AS holidayhours
  FROM day d
  INNER JOIN edwClockHoursFact f ON d.DateKey = f.DateKey
  WHERE d.thedate BETWEEN @FromDate + 1 AND @ThruDate
    AND f.ClockHours + f.VacationHours + f.PTOHours + f.HolidayHours <> 0
  GROUP BY employeekey) c ON e.EmployeeKey = c.EmployeeKey
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL p ON e.storecode = p.yco#
  AND p.ycyy = 100 + (year(@ThruDate) - 2000) 
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT d ON e.storecode = d.ydco#
  AND e.employeenumber = d.ydempn 
  AND d.yddcde IN ('91', '99'); 

SELECT employeekey
FROM #wtf 
GROUP BY employeekey 
  HAVING COUNT(*) > 1
  
SELECT * FROM #wtf  
WHERE employeenumber IN (
  SELECT employeenumber
  FROM #wtf
  GROUP BY employeenumber
    HAVING COUNT(*) > 1)
ORDER BY employeenumber    

SELECT *
FROM #wtf w
LEFT JOIN (
    SELECT ytagpp, ytagpa, ytaeta, ytamed, ytaera, ytaco#, ytadic 
    FROM stgArkonaPYACTGR) p ON w.storecode = p.ytaco#
  AND w.distcode = p.ytadic
WHERE coalesce(w.GrossPay, 0) <> 0
  AND employeenumber = '118032'
ORDER BY distcode

SELECT *
from stgArkonaPYACTGR
WHERE ytadic = 'R B'
SELECT * FROM #wtf

SELECT employeenumber, DistAcct, sum(DistAmt)
FROM (
SELECT w.employeekey, w.employeenumber, gross.ytagpp AS DistPerc, gross.ytagpa AS DistAcct, round(GrossPay * gross.ytagpp/100.0, 2) AS DistAmt
FROM #wtf w
LEFT JOIN ( -- gross
    SELECT ytaco#, ytadic, ytagpp, ytagpa
    FROM stgArkonaPYACTGR) gross ON w.storecode = gross.ytaco#
  AND w.distcode = gross.ytadic
WHERE coalesce(w.GrossPay, 0) <> 0  
--  AND employeenumber = '118032'
UNION ALL   
SELECT w.employeekey, w.employeenumber, fica.ytagpp AS DistPerc, fica.ytaeta AS DistAcct, round(fica * fica.ytagpp/100.0, 2) AS DistAmt
FROM #wtf w
LEFT JOIN ( -- fica
    SELECT ytaco#, ytadic, ytagpp, ytaeta
    FROM stgArkonaPYACTGR) fica ON w.storecode = fica.ytaco#
  AND w.distcode = fica.ytadic
WHERE coalesce(w.GrossPay, 0) <> 0  
--  AND employeenumber = '118032'
UNION ALL   
SELECT w.employeekey, w.employeenumber, med.ytagpp AS DistPerc, med.ytamed AS DistAcct, round(Medicare * med.ytagpp/100.0, 2) AS DistAmt
FROM #wtf w
LEFT JOIN ( -- medicare
    SELECT ytaco#, ytadic, ytagpp, ytamed
    FROM stgArkonaPYACTGR) med ON w.storecode = med.ytaco#
  AND w.distcode = med.ytadic
WHERE coalesce(w.GrossPay, 0) <> 0  
--  AND employeenumber = '118032'  
UNION ALL   
SELECT w.employeekey, w.employeenumber, ret.ytagpp AS DistPerc, ret.ytaera AS DistAcct, coalesce(round(Retirement * ret.ytagpp/100.0, 2), 0) AS DistAmt
FROM #wtf w
LEFT JOIN ( -- retirement
    SELECT ytaco#, ytadic, ytagpp, ytaera
    FROM stgArkonaPYACTGR) ret ON w.storecode = ret.ytaco#
  AND w.distcode = ret.ytadic
WHERE coalesce(w.GrossPay, 0) <> 0
  AND retirement <> 0) x 
--  AND employeenumber = '118032') x
WHERE employeenumber = '11650'
GROUP BY employeenumber, DistAcct   

SELECT *
FROM #wtf
WHERE employeenumber = '11650'
  
-- regualar hours are more than for the entire pay period, should be less
-- this interval 1/15/2012 - 1/31/2012 : 93.21 hours 
-- paycheck interval 1/15/2012 - 2/1/2012 : 79.71 hours

DECLARE @ThruDate date; // Last day of month
DECLARE @FromDate date; // Last payroll ending date prior to EOM
DECLARE @NumDays integer;
@FromDate = '1/14/2012';
@ThruDate = (
  SELECT thedate 
  FROM day WHERE LastDayOfMonth = true 
    AND month(thedate) = month(@FromDate) 
    AND year(thedate) = year(@FromDate));
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate);
SELECT name, c.*
FROM (
  SELECT *
  FROM edwEmployeeDim 
  WHERE PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
    AND hiredate <= @thrudate
    AND termdate >= @fromdate) e
LEFT JOIN (
  SELECT employeekey, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
    sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
    sum(holidayhours) AS holidayhours
  FROM day d
  INNER JOIN edwClockHoursFact f ON d.DateKey = f.DateKey
  WHERE d.thedate BETWEEN @FromDate + 1 AND @ThruDate
    AND f.ClockHours + f.VacationHours + f.PTOHours + f.HolidayHours <> 0
  GROUP BY employeekey) c ON e.EmployeeKey = c.EmployeeKey
  
SELECT d.thedate, f.*
-- SELECT SUM(regularhours), SUM(overtimehours)
FROM edwClockHoursFact f
INNER JOIN day d ON f.datekey = d.datekey
  AND d.thedate BETWEEN '01/15/2012' AND '01/31/2012'
WHERE employeekey = 1372
  
/*/******* 2/12 ******************************************************************/ 

/******* 2/13 ******************************************************************/ 
bi-weekly payroll
  ending date IS always every other saturday
  eg
    payroll 20300 check date = 2/3/12
                  period ending date 2/1/12 (i think this IS WHEN kim actually runs payroll
                  actual END of period IS 1/28/12
                  
SELECT CAST('01/28/2012' AS sql_date) - 13
FROM system.iota     

SELECT thedate, DayName, mod(cast(ABS(timestampdiff(sql_tsi_day, thedate, '01/28/2012')) AS sql_integer), 14)
FROM day
WHERE thedate BETWEEN '01/01/2011' AND curdate()
  AND mod(cast(ABS(timestampdiff(sql_tsi_day, thedate, '01/28/2012')) AS sql_integer), 14) = 0
  
SELECT DISTINCT payrollendingdate
FROM stgArkonaPYHSHDTA p
WHERE payrollendingdate BETWEEN '01/01/2011' AND curdate()


-- this looks pretty good
-- except jan 2011 only shows 1 previouw biw pay end
SELECT payrollendingdate, MAX(x.thedate) AS "Previous biw pay end", MAX(y.thedate) AS EOM

FROM (
  SELECT DISTINCT payrollendingdate
  FROM stgArkonaPYHSHDTA p
  WHERE payrollendingdate BETWEEN '01/01/2011' AND curdate()
    AND yhpper = 'B') pe  
LEFT JOIN (  
  SELECT *
  FROM day
  WHERE thedate BETWEEN cast('01/01/2010' AS sql_date) AND curdate()
    AND mod(cast(ABS(timestampdiff(sql_tsi_day, thedate, '01/28/2012')) AS sql_integer), 14) = 0) x ON x.thedate < pe.payrollendingdate
LEFT JOIN (
  SELECT thedate
  FROM day
  WHERE lastdayofmonth = true
    AND thedate BETWEEN '01/01/2011' AND curdate() + 31) y ON month(pe.payrollendingdate) = month(y.thedate)
    AND year(pe.payrollendingdate) = year(y.thedate)   
GROUP BY payrollendingdate    

                       
/*/****** 2/13 ******************************************************************/

/******* 2/14 ******************************************************************/       


-- pyptbdta: payroll batch, here's WHERE the bi-weekly dates are stored for existing payroll batches
-- i will get to that, first run query against bi-weekly pay periods AND compare the results to pyhshdta for the same pay periods
-- change the dates stuff AND calculations based ON dates
-- include hours IN output for comparison to pyhshdta
/*
DECLARE @ThruDate date; // Last day of month
DECLARE @FromDate date; // Last payroll ending date prior to EOM
DECLARE @NumDays integer;
@FromDate = '1/14/2012';
@ThruDate = (
  SELECT thedate 
  FROM day WHERE LastDayOfMonth = true 
    AND month(thedate) = month(@FromDate) 
    AND year(thedate) = year(@FromDate));
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate);
*/
DECLARE @FromDate date; -- first day of biweekly pay period
DECLARE @ThruDate date; -- last day of biweekly pay period
@FromDate = '01/29/2012';
@ThruDate = '02/11/2012';
SELECT name, e.employeekey, storecode, e.employeenumber,
  payrollclasscodedesc, distcode,
  CASE PayrollClassCode
    WHEN 'H' THEN ((RegularHours + VacationHours + PTOHours + HolidayHours) * s.HourlyRate) + (OvertimeHours * s.HourlyRate * 1.5)
--    WHEN 'S' THEN salary * (@NumDays*1.0/DayOfMonth(@thrudate))
    WHEN 'S' THEN s.salary
  END AS GrossPay,
  CASE PayrollClassCode
    WHEN 'H' THEN round(((((RegularHours + VacationHours + PTOHours + HolidayHours) * s.HourlyRate) + (OvertimeHours * s.HourlyRate * 1.5)) - coalesce(fe.FicaEx, 0)) * p.yficmp/100.0, 2)
--    WHEN 'S' THEN (salary * (@NumDays*1.0/DayOfMonth(@thrudate))) * p.yficmp/100.0
    WHEN 'S' THEN round((s.salary - coalesce(fe.FicaEx, 0)) * p.yficmp/100.0, 2)
  END AS FICA,
  CASE PayrollClassCode
    WHEN 'H' THEN round(((((RegularHours + VacationHours + PTOHours + HolidayHours) * s.HourlyRate) + (OvertimeHours * s.HourlyRate * 1.5)) - coalesce(me.MedEx, 0))* p.ymedmp/100.0, 2)
--    WHEN 'S' THEN (salary * (@NumDays*1.0/DayOfMonth(@thrudate))) * p.ymedmp/100.0
    WHEN 'S' THEN round((s.salary - coalesce(me.MedEx, 0))* p.ymedmp/100.0, 2)
  END AS Medicare,
  CASE 
    WHEN d.yddamt IS NULL THEN 0
    ELSE 
    CASE PayrollClassCode
      WHEN 'H' THEN 
        CASE 
          WHEN (((RegularHours + VacationHours + PTOHours + HolidayHours) * s.HourlyRate) + (OvertimeHours * s.HourlyRate * 1.5)) * d.yddamt/200.0 -- 1/2 of employee contr
              < .02 * (((RegularHours + VacationHours + PTOHours + HolidayHours) * s.HourlyRate) + (OvertimeHours * s.HourlyRate * 1.5)) 
            THEN round((((RegularHours + VacationHours + PTOHours + HolidayHours) * s.HourlyRate) + (OvertimeHours * s.HourlyRate * 1.5)) * d.yddamt/200.0, 2)
          ELSE round(.02 * (((RegularHours + VacationHours + PTOHours + HolidayHours) * s.HourlyRate) + (OvertimeHours * s.HourlyRate * 1.5)), 2) 
        END 
  --    WHEN 'S' THEN (salary * (@NumDays*1.0/DayOfMonth(@thrudate))) * d.yddamt/100.0
      WHEN 'S' THEN 
        CASE  
          WHEN s.salary * d.yddamt/200.0 < .02 * s.salary THEN round((s.salary * d.yddamt/200.0), 2)
          ELSE round(.02 * s.salary, 2) 
        END 
    END 
  END AS Retirement,
  regularhours, overtimehours, vacationhours, ptohours, holidayhours
-- DROP TABLE #wtf  
 INTO #wtf  
FROM ( -- bi-weekly employeekeys employed during the interval
  SELECT *
  FROM edwEmployeeDim 
  WHERE PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
    AND hiredate <= @thrudate
    AND termdate >= @fromdate
-- shane 2 record fix    
    AND EmployeeKeyFromDate <= @thrudate
    AND EmployeeKeyThruDate >= @fromdate) e
LEFT JOIN ( -- clock hours per employeekey for the interval
  SELECT employeekey, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
    sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
    sum(holidayhours) AS holidayhours
  FROM day d
  INNER JOIN edwClockHoursFact f ON d.DateKey = f.DateKey
--  WHERE d.thedate BETWEEN @FromDate + 1 AND @ThruDate
  WHERE d.thedate BETWEEN @FromDate AND @ThruDate
    AND f.ClockHours + f.VacationHours + f.PTOHours + f.HolidayHours <> 0
  GROUP BY employeekey) c ON e.EmployeeKey = c.EmployeeKey
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL p ON e.storecode = p.yco#
  AND p.ycyy = 100 + (year(@ThruDate) - 2000) 
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT d ON e.storecode = d.ydco#
  AND e.employeenumber = d.ydempn 
  AND d.yddcde IN ('91', '99')
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT ydempn, SUM(yddamt) AS FicaEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex2 = 'Y')
  GROUP BY ydempn) fe ON e.employeenumber = fe.ydempn   
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT ydempn, SUM(yddamt) AS MedEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex6 = 'Y')
  GROUP BY ydempn) me ON e.employeenumber = me.ydempn 
LEFT JOIN ( -- wage ON @ThruDate
  SELECT EmployeeNumber, Salary, HourlyRate
  FROM edwEmployeeDim ex
  WHERE EmployeeKeyFromDate = (
    SELECT MAX(EmployeekeyFromDate)
    FROM edwEmployeeDim
    WHERE EmployeeNumber = ex.Employeenumber
      AND EmployeeKeyFromDate <= '02/11/2012'
    GROUP BY employeenumber)) s ON e.employeenumber = s.employeenumber ; 

-- oops, look
-- so, i want to compare ALL hours, gross pay, employer: fica, medicare, retirement  

SELECT 'wtf', (regularhours + vacationhours + ptohours + holidayhours), overtimehours, fica, medicare, retirement
-- SELECT *
FROM #wtf
WHERE employeenumber = '11650'
union
SELECT 'paycheck', yhdhrs AS RegHours, yhdoth AS OTHours, yhcssm AS fica, yhcmdm AS medicare, yhcrtm AS retire401k
-- SELECT *
FROM stgArkonaPYHSHDTA
WHERE yhdemp = '11650'
  AND ypbnum = 217000
  
-- oops, looks LIKE i am pulling the employee contr to retirement  

/*  2/23/12 */  
SELECT 'wtf', regularhours, overtimehours, vacationhours, ptohours, holidayhours, 
  fica, medicare, retirement
-- SELECT *
FROM #wtf
WHERE employeenumber = '17560'
union
SELECT 'paycheck', yhdhrs AS RegHours, yhdoth AS OTHours, yhdvac as VacHours, 
  yhdsck as PTOSickHours, yhdhol as HolHours, yhcssm AS fica, yhcmdm AS medicare, 
  yhcrtm AS retire401k
-- SELECT *
FROM stgArkonaPYHSHDTA
WHERE yhdemp = '17560'
  AND ypbnum = 217000  
  
SELECT w.name, d.*
FROM #wtf w
INNER JOIN stgArkonaPYDEDUCT d ON w.employeenumber = d.ydempn
INNER JOIN stgArkonaPYPCODES c ON d.yddcde = c.ytddcd
  AND c.ytdex2 = 'Y'
WHERE employeenumber = '11650'  

-- FICA exempt amt  ADD it to #wtf
SELECT ydempn, SUM(yddamt)
FROM stgArkonaPYDEDUCT d
WHERE ydempn = '11650'
  AND EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex2 = 'Y')
GROUP BY ydempn    

/************************   2/24/12/  **************************************/            
SELECT 'wtf', regularhours, overtimehours, vacationhours, ptohours, holidayhours, 
  GrossPay, fica, medicare, retirement
-- SELECT *
FROM #wtf
WHERE employeenumber = '118010'
union
SELECT 'paycheck', yhdhrs AS RegHours, yhdoth AS OTHours, yhdvac as VacHours, 
  yhdsck as PTOSickHours, yhdhol as HolHours, 
  yhacm as GrossPay, yhcssm AS fica, yhcmdm AS medicare, 
  yhcrtm AS retire401k
-- SELECT *
FROM stgArkonaPYHSHDTA
WHERE yhdemp = '118010'
  AND ypbnum = 217000  

-- ALL the differences  
SELECT employeenumber, name,
  CASE WHEN w.grosspay <> p.grosspay THEN w.grosspay - p.grosspay END AS "Gross Diff",
  CASE WHEN w.fica <> p.fica THEN w.fica - p.fica END AS "FICA Diff" ,
  CASE WHEN w.medicare <> p.medicare THEN w.medicare - p.medicare END AS "Medicare Diff" ,
  CASE WHEN w.retirement <> p.retirement THEN w.retirement - p.retirement END AS "Retire Diff"  
FROM (
  SELECT employeenumber, name, GrossPay, fica, medicare, retirement
  -- SELECT *
  FROM #wtf) w  
LEFT JOIN (  
  SELECT yhdemp, 'paycheck', yhacm as GrossPay, yhcssm AS fica, yhcmdm AS medicare, 
    yhcrtm AS retirement
  -- SELECT *
  FROM stgArkonaPYHSHDTA
  WHERE  ypbnum = 217000) p ON w.employeenumber = p.yhdemp
WHERE (
    w.grosspay <> p.grosspay OR
    w.fica <> p.fica OR
    w.medicare <> p.medicare OR 
    w.retirement <> p.retirement)
  AND abs(w.grosspay - p.grosspay) > 1   

-- 118010 2 rows IN wtf  
-- date range IS 1/29 - 2/11
-- the type2 change occurred within the interval, both rows are valid
SELECT hiredate, termdate, employeekeyfromdate, employeekeythrudate 
FROM edwEmployeeDim WHERE employeenumber = '118010'  

-- AS one would expect
SELECT employeenumber FROM edwEmployeeDim GROUP BY employeenumber HAVING COUNT(*) > 1
SELECT employeekey FROM edwEmployeeDim GROUP BY employeekey HAVING COUNT(*) > 1

-- actual accrual IS dependent ON distcode, which IS empkey dependent
-- at the paycheck level DO a group/SUM ON employeenumber

  
SELECT employeenumber, max(name),
  SUM(CASE WHEN w.grosspay <> p.grosspay THEN w.grosspay - p.grosspay END) AS "Gross Diff",
  SUM(CASE WHEN w.fica <> p.fica THEN w.fica - p.fica END) AS "FICA Diff" ,
  SUM(CASE WHEN w.medicare <> p.medicare THEN w.medicare - p.medicare END) AS "Medicare Diff" ,
  SUM(CASE WHEN w.retirement <> p.retirement THEN w.retirement - p.retirement END) AS "Retire Diff"  
FROM (
  SELECT employeenumber, name, GrossPay, fica, medicare, retirement
  -- SELECT *
  FROM #wtf
  WHERE employeenumber = '118010') w  
LEFT JOIN (  
  SELECT yhdemp, 'paycheck', yhacm as GrossPay, yhcssm AS fica, yhcmdm AS medicare, 
    yhcrtm AS retirement
  -- SELECT *
  FROM stgArkonaPYHSHDTA
  WHERE  ypbnum = 217000) p ON w.employeenumber = p.yhdemp
WHERE (
    w.grosspay <> p.grosspay OR
    w.fica <> p.fica OR
    w.medicare <> p.medicare OR 
    w.retirement <> p.retirement)
  AND abs(w.grosspay - p.grosspay) > 1   
GROUP BY employeenumber  


-- GROUPing to look at a single employeenumber
SELECT source, SUM(regularhours) AS Reg, SUM(overtimehours)AS OT, SUM(vacationhours) AS Vac, 
  SUM(ptohours) AS PTO, SUM(holidayhours) AS Hol, 
  SUM(GrossPay) AS Gross, SUM(fica) AS FICA, SUM(medicare) AS Medic, SUM(retirement) AS Retire
FROM (  
SELECT 'wtf' AS source, regularhours, overtimehours, vacationhours, ptohours, holidayhours, 
  GrossPay, fica, medicare, retirement
-- SELECT *
FROM #wtf
WHERE employeenumber = '118010'
union
SELECT 'paycheck' AS source, yhdhrs AS RegHours, yhdoth AS OTHours, yhdvac as VacHours, 
  yhdsck as PTOSickHours, yhdhol as HolHours, 
  yhacm as GrossPay, yhcssm AS fica, yhcmdm AS medicare, 
  yhcrtm AS retire401k
-- SELECT *
FROM stgArkonaPYHSHDTA
WHERE yhdemp = '118010'
  AND ypbnum = 217000) x    
GROUP BY source  

SELECT * FROM edwEmployeeDim WHERE employeenumber = '118010'
-- what i'm trying to get IS the salary ON a specific day
-- MAX fromdate < @thruDate
-- OK, salary ON a particular date seems to WORK ok
  SELECT EmployeeNumber, Salary, HourlyRate
  FROM edwEmployeeDim ex
  WHERE EmployeeKeyFromDate = (
    SELECT MAX(EmployeekeyFromDate)
    FROM edwEmployeeDim
    WHERE EmployeeNumber = ex.Employeenumber
      AND EmployeeKeyFromDate <= '02/11/2012'
    GROUP BY employeenumber)
ORDER BY employeenumber;

-- next problem :FICA
paycheck: 46.84
wtf :     43.48
check ON the fica exempt deductions for 118010

SELECT *
FROM stgArkonaPYDEDUCT
WHERE ydempn = '118010'

-- fica exempt
  SELECT ydempn, SUM(yddamt) AS FicaEx
  FROM stgArkonaPYDEDUCT d
  WHERE ydempn = '118010'
    AND EXISTS (
      SELECT 1
      FROM stgArkonaPYPCODES
      WHERE ytddcd = d.yddcde
      AND ytdex2 = 'Y')
  GROUP BY ydempn

