-- WORK days IN accrual period
SELECT COUNT(*)
FROM accrualDates a 
INNER JOIN day b on b.thedate BETWEEN a.fromdate AND a.thrudate
WHERE b.dayofweek BETWEEN 2 AND 6

figure per WORK day pay for tp techs, payroll / 10

workDaysInAccrualPeriod * payPerWorkDay = regPay
/**********/ DO NOT forget brian peterson
generate AccrualHourly1 AS usual
THEN 2 updates WHERE employeeNumber IN teampay
1. regPay = commissionPay (workDaysInAccrualPeriod * payPerWorkDay)
   overtimePay = 0
2. totalNonOtPay = regPay + vacationPay + ptoPay + holidayPay  


-- FROM scotest payroll for kim
select thedate, lastname, firstname, employeenumber,  
  techtfrrate*teamprofpptd*techclockhourspptd/100 AS commissionPay
FROM scotest.tpdata a
INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
  AND a.teamKey = b.teamkey
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 31 AND curdate() -- for now, this IS somewhat arbitrary
ORDER BY lastname asc, thedate desc                -- chose number to generate 3 payperiods


-- this IS the general pattern, MAX of previous 3 pay periods
select lastname, firstname, employeenumber,  
  round(max(techtfrrate*teamprofpptd*techclockhourspptd/100)/10, 2) AS commissionPayPerDay
FROM scotest.tpdata a
INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
  AND a.teamKey = b.teamkey
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 31 AND curdate()
GROUP BY lastname, firstname, employeenumber  
UNION -- brian peterson
select c.lastname, c.firstname, a.employeenumber, 
  round(max(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPayPerDay
FROM scotest.bsFlatRateData a
LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
  AND a.thedate BETWEEN b.fromdate AND b.thrudate
LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 31 AND curdate()
GROUP BY c.lastname, c.firstname, a.employeenumber 


CREATE TABLE accrualTpAdjustment (
  employeenumber cichar(9),
  commissionPay money);
/*
08/01/2014
rather than DELETE, keep some history  
ALTER TABLE accrualTpAdjustment
ADD COLUMN monthEndDate date;
UPDATE accrualTpAdjustment
SET monthEndDate = '06/30/2014';

*/
-- 7/2, for june, no need to use the MAX of previous 3, accruing the entire
--    pay period + 1 day
INSERT INTO accrualTpAdjustment
select employeenumber, 
  round(techtfrrate*teamprofpptd*techclockhourspptd/100, 2) +
  round(techtfrrate*teamprofpptd*techclockhourspptd/100/10, 2) AS commissionPay
FROM scotest.tpdata a
INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
  AND a.teamKey = b.teamkey
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 7 AND curdate()
UNION -- brian peterson
select a.employeenumber, 
  round(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD, 2) +
  round((b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPay
FROM scotest.bsFlatRateData a
LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
  AND a.thedate BETWEEN b.fromdate AND b.thrudate
LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 7 AND curdate(); 
  
-- 8/2/14 
-- accrual period 7/13 - 7/31
-- include actual commision FROM pay period 7/13 - 7/26
-- ADD IN 4 days at the MAX commPayPerDay over the last 3 pay periods  
INSERT INTO accrualTpAdjustment
select u.employeenumber, coalesce(commissionPay, 0) + coalesce(plus4days, 0) AS Accrual, '07/31/2014'
FROM ( -- actual commission pay for pay period
  select teamname, firstname, employeenumber, 
    round(techtfrrate*teamprofpptd*techclockhourspptd/100, 2)  AS commissionPay
  FROM scotest.tpdata a
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 7 AND curdate()
  UNION -- brian peterson
  select 'pdr', b.firstname, a.employeenumber, 
    round(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD, 2) +
    round((b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPay
  FROM scotest.bsFlatRateData a
  LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.fromdate AND b.thrudate
  LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 7 AND curdate()) u
LEFT JOIN ( -- 4 days of the MAX commPerDay over the last 3 pay periods
  SELECT employeenumber, 4 * commissionPayPerDay AS plus4days
  FROM (
    select lastname, firstname, employeenumber,  
      round(max(techtfrrate*teamprofpptd*techclockhourspptd/100)/10, 2) AS commissionPayPerDay
    FROM scotest.tpdata a
    INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey  
    WHERE thedate = payperiodend
      AND thedate BETWEEN curdate() - 45 AND curdate()
    GROUP BY lastname, firstname, employeenumber  
    UNION -- brian peterson
    select c.lastname, c.firstname, a.employeenumber, 
      round(max(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPayPerDay
    FROM scotest.bsFlatRateData a
    LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
      AND a.thedate BETWEEN b.fromdate AND b.thrudate
    LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
    WHERE thedate = payperiodend
      AND thedate BETWEEN curdate() - 45 AND curdate()
    GROUP BY c.lastname, c.firstname, a.employeenumber ) e) v on u.employeenumber = v.employeenumber  
WHERE coalesce(commissionPay, 0) + coalesce(plus4days, 0) > 0    




-- 9/2/14 
-- accrual period 8/24 - 8/31
--INSERT INTO accrualTpAdjustment
-- make the multiplier the number of workdays IN the accrual period
DECLARE @days integer;
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN '08/24/2014' AND '08/31/2014'
    AND holiday = false);
INSERT INTO accrualTpAdjustment  
SELECT employeenumber, @days * commissionPayPerDay AS commissionPay, '08/31/2014'
FROM (
  select lastname, firstname, employeenumber,  
    round(max(techtfrrate*teamprofpptd*techclockhourspptd/100)/10, 2) AS commissionPayPerDay
  FROM scotest.tpdata a
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 45 AND curdate()
  GROUP BY lastname, firstname, employeenumber  
  UNION -- brian peterson
  select c.lastname, c.firstname, a.employeenumber, 
    round(max(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPayPerDay
  FROM scotest.bsFlatRateData a
  LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.fromdate AND b.thrudate
  LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 45 AND curdate()
  GROUP BY c.lastname, c.firstname, a.employeenumber ) e
  
-- 10/1/14 
-- accrual period 9/21 - 9/30
--INSERT INTO accrualTpAdjustment
-- make the multiplier the number of workdays IN the accrual period
-- 11/3 accrual period 10/19 - 10/31
DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '10/19/2014';
@thruDate = '10/31/2014';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);
-- SELECT @days FROM system.iota;        
INSERT INTO accrualTpAdjustment  
SELECT employeenumber, @days * commissionPayPerDay AS commissionPay, @thruDate
FROM (
  select lastname, firstname, employeenumber,  
    round(max(techtfrrate*teamprofpptd*techclockhourspptd/100)/10, 2) AS commissionPayPerDay
  FROM scotest.tpdata a
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 45 AND curdate()
  GROUP BY lastname, firstname, employeenumber  
  UNION -- brian peterson
  select c.lastname, c.firstname, a.employeenumber, 
    round(max(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPayPerDay
  FROM scotest.bsFlatRateData a
  LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.fromdate AND b.thrudate
  LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 45 AND curdate()
  GROUP BY c.lastname, c.firstname, a.employeenumber ) e
  
--12/1/14 just DO the actual payroll (11/16 - 11/29)
-- hmm, does NOT include bonus
-- at least, IN this CASE ADD IN pto/holiday  
-- 6/1/15 just DO the actual payroll
DECLARE @thruDate date;
@thruDate = '05/30/2015';
-- INSERT INTO accrualTpAdjustment
select employeenumber, 
  round(techtfrrate*teamprofpptd*techclockhourspptd/100, 2) --+
  + techhourlyrate * (techvacationhourspptd + techptohourspptd + techholidayhourspptd),
  @thruDate 
--  round(techtfrrate*teamprofpptd*techclockhourspptd/100/10, 2) AS commissionPay
FROM scotest.tpdata a
INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
  AND a.teamKey = b.teamkey
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 7 AND curdate()
UNION -- brian peterson
select a.employeenumber, 
  round(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD, 2), --+
--  round((b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPay
  @thruDate
FROM scotest.bsFlatRateData a
LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
  AND a.thedate BETWEEN b.fromdate AND b.thrudate
LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 7 AND curdate();   
  
  
-- 1/2/15, 2/2/15, 3/2/15, 4/2/15, 5/2/15 i'm dead OR IN the hospital, this IS what i sent to greg
-- make the multiplier the number of workdays IN the accrual period
-- include holiday pay
-- 8/3/15
-- remove the brian peterson UNION, body shop now ALL one team
--DELETE FROM accrualTpAdjustment WHERE monthEndDate = '02/07/2015'
DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '07/26/2015';
@thruDate = '07/31/2015';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);
-- SELECT @days FROM system.iota;        
--INSERT INTO accrualTpAdjustment  
SELECT employeenumber, round((@days * commissionPayPerDay) + holPay, 2) AS commissionPay, @thruDate
FROM (
  select lastname, firstname, employeenumber,  
    round(max(techtfrrate*teamprofpptd*techclockhourspptd/100)/10, 2) AS commissionPayPerDay,
    max(techhourlyrate * (techvacationhourspptd + techptohourspptd + techholidayhourspptd)) AS holPay
  FROM scotest.tpdata a
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 45 AND curdate()
  GROUP BY lastname, firstname, employeenumber) e
/*   
  UNION -- brian peterson
  select c.lastname, c.firstname, a.employeenumber, 
    round(max(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPayPerDay,
    0 AS holPay
  FROM scotest.bsFlatRateData a
  LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.fromdate AND b.thrudate
  LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 45 AND curdate()
  GROUP BY c.lastname, c.firstname, a.employeenumber ) e  
*/