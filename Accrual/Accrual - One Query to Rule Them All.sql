/*
SELECT DISTINCT biweeklypayperiodstartdate, biweeklypayperiodenddate
FROM day
WHERE theyear = 2014
*/
/*
3/30/12
       added variables
       added DistCode STEV to exclusion
       reduced base Greg amount to acct 12101
4/2/12
      added limitations ON JOIN to AccrualCommissions: ThruDate = @Date AND Amount <> 0
      
DROP TABLE #wtf2;
DROP TABLE #jon2;       
DROP TABLE #entries;        
DROP TABLE #wtf1;
DROP TABLE #jon1;   

6/1/12
2/3/13: assume accrual period IS 1/27 - 1/31
  jeri actually wants 13th
  UPDATE TABLE AccrualCommissions first
  these are the base rows, Values have to be added manually
    
      DECLARE @fromdate string;
      DECLARE @thrudate string;
      
      @fromdate = '07/26/2015'; -- last payroll enddate + 1
      @thrudate = '07/31/2015'; -- EOM
      INSERT INTO AccrualCommissions
      SELECT a.storecode, a.employeenumber, a.employeekey, a.name,
        CAST(@fromdate AS sql_Date) AS FromDate, CAST(@thrudate AS sql_date) AS ThruDate, 0 AS Amount
      FROM edwEmployeeDim a
      LEFT JOIN ( -- clock hours for the interval
          SELECT storecode, employeenumber, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
            sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
            sum(holidayhours) AS holidayhours
          FROM day d
          INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
          LEFT JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
          WHERE thedate BETWEEN CAST(@fromdate AS sql_date) AND CAST(@thrudate AS sql_date)
          GROUP BY storecode, employeenumber) b ON a.storecode = b.storecode 
        AND a.employeenumber = b.employeenumber 
      WHERE EmployeeKeyFromDate = (
          SELECT MAX(EmployeekeyFromDate)
          FROM edwEmployeeDim
          WHERE EmployeeNumber = a.Employeenumber
            AND EmployeeKeyFromDate <= CAST(@thrudate AS sql_date)
          GROUP BY employeenumber)
        AND PayPeriodCode = 'B'
        AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV') 
        AND b.storecode IS NOT NULL;-- ORDER BY name;   
8/28/12
  commission IS NOT being accrued to (comm/days IN month) * @NumDays
  commission number IS for the entire month
  ADD variable @DaysInMonth
9/4/12 
  changed greg's accrual account per jeri  
9/5/12
 FFFFFFFFFFFFUUUUUUUUUUUUUUUUCCCCCCCCCCCCCCCCKKKKKKKKKKKKKKKKKKK
 Jeri was just here, the entire commission for august should be included IN the
 accrual, BECAUSE NONE OF IT HAS BEEN PAID YET  
 
 10/1
 9/23 - 9/30
 11/1
 10/21 - 10/31
 12/1
 11/18 - 11/30
 12/16 - 12/31
 2/10/13 - 2/28/13
 3/24/13 - 3/31/13
 
 2/4/13
   no more crookston
2/8/13
  per Jeri, exclude Ben Marotte & Steve Zoellick emp#: 190915, 1160100
  have NOT yet figured out how to get her done
  IF i eliminate them FROM base JOIN IN #wtf1/2 that should take care of it
  they use to be eliminated BY their dist code, but that has been changed to an 
    included dist code (11)
4/3 Jeri says this IS still doing Travis Lashley 283120 AND should NOT be  
  5/2 ADD him AS an exemption for now, still need to fix  
5/11 changed field names for stgArkonaPYACTGR & stgArkonaPYDEDUCT
5/28 reworked #wtf1/#wtf2 to exclude nonactive employees (lashley, et al), jeri has been
     complaining about them, to summarize: include employees whose interval of
     employment (hiredate->termdate) overlaps with the accrual interval
9/4/13  i totally fucked up july accrual for main shop techs, IN the generation of #wtf1  
        i was limiting the employee keys to only the one active at the END of the month  
        thereby eliminating a shit load of clockhours, this came to light because
        a shit load of techs got raises IN the middle of the july accrual interval
9/4/13  another bug, IN the JOIN to stgArkonaPYCNTRL, i have hard coded 112        
*/
/*
may 2013 5/19 - 5/31
june 2013 6/16 - 6/30
july 2013 14 - 31
august 2013 25 - 31
*/
DECLARE @Date string; // last day of the month for which payroll IS being accrued;
DECLARE @FromDate date; // beginning date for the interval to be accrued
DECLARE @ThruDate date; // ending date for the interval to be accrued
DECLARE @NumDays integer;
DECLARE @DaysInMonth integer;
DECLARE @Journal string;
DECLARE @Doc string;
DECLARE @Ref string;
@Journal = 'GLI';
@Doc = 'GLI093013';
@Ref = @Doc;

@Date = '09/30/2013';
@FromDate = '09/22/2013';
@ThruDate = '09/30/2013';
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1;
@DaysInMonth = DayOfMonth(@Date);
-- clock hours
SELECT a.*, b.regularhours, b.overtimehours, b.vacationhours, b.ptohours, b.holidayhours,
  p.DIST_CODE, p.SEQ_NUMBER, p.GROSS_DIST, p.GROSS_EXPENSE_ACT_, p.OVERTIME_ACT_, 
  p.VACATION_EXPENSE_ACT_, p.HOLIDAY_EXPENSE_ACT_, p.SICK_LEAVE_EXPENSE_ACT_,
  p.EMPLR_FICA_EXPENSE, p.EMPLR_MED_EXPENSE, p.EMPLR_CONTRIBUTIONS,
  pc.yficmp, pc.ymedmp, fe.FicaEx, me.MedEx, d.FIXED_DED_AMT 
INTO #wtf1
FROM ( -- a: 1 row per store/emp#  emp IS payed bi-weekly, dist code NOT IN list, 
       -- the empkey that was valid ON last payroll ending date ** this IS SO WRONG
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
      PayPeriodCode, DistCode, salary, hourlyrate
  FROM edwEmployeeDim e
  WHERE hiredate <= @thruDate
    AND termdate >= @fromDate
    AND storecode <> 'ry3'  
    AND employeenumber NOT IN ('190915','1160100')
    AND PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV') 
    AND employeekey = ( -- this IS the problem
      SELECT MAX(employeekey)
      FROM edwEmployeeDim 
      WHERE employeenumber = e.employeenumber
        AND storecode = e.storecode
        AND employeekeyfromdate <=  @thruDate
      GROUP BY storecode, employeenumber)) a     
LEFT JOIN ( -- clock hours for the interval BY empkey
  SELECT employeekey, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
    sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
    sum(holidayhours) AS holidayhours
  FROM day d
  INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
  WHERE thedate BETWEEN @fromDate AND @thruDate
  GROUP BY employeekey) b ON a.employeekey = b.employeekey   
LEFT JOIN stgArkonaPYACTGR p ON a.StoreCode = p.COMPANY_NUMBER -- distribution
  AND a.DistCode = p.DIST_CODE
  AND CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL pc ON a.storecode = pc.yco#
  AND pc.ycyy = 112 -- 100 + (year(@ThruDate) - 2000) 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) fe ON a.employeenumber = fe.EMPLOYEE_NUMBER  
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) me ON a.employeenumber = me.EMPLOYEE_NUMBER    
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT d ON a.storecode = d.COMPANY_NUMBER
  AND a.employeenumber = d.EMPLOYEE_NUMBER 
  AND d.DED_PAY_CODE IN ('91', '99') 
WHERE b.employeekey IS NOT NULL;  
  
select @Journal as Journal, @Date as [Date], Account, EmployeeNumber as Control, 
  @Doc as Document, @Ref as Reference, round(sum(Amount),2) as Amount, 
  'Payroll Accrual' as Description
INTO #jon1
FROM (
  SELECT 'Gross', employeenumber, GROSS_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 *
      CASE PayrollClassCode
        WHEN 'H' THEN (regularhours * HourlyRate)
        WHEN 'S' THEN salary/14 * @NumDays
      END AS Amount
  FROM #wtf1
  UNION ALL
  SELECT 'OT', employeenumber, OVERTIME_ACT_ AS Account,
    GROSS_DIST/100.0 * overtimehours * HourlyRate * 1.5 AS Amount
  FROM #wtf1
  WHERE PayrollClassCode = 'H'
  UNION ALL 
  SELECT 'VAC', employeenumber, VACATION_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 * vacationhours * HourlyRate AS Amount
  FROM #wtf1
  WHERE PayrollClassCode = 'H'
  UNION ALL 
  SELECT 'PTO', employeenumber, SICK_LEAVE_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 * ptohours * HourlyRate AS Amount
  FROM #wtf1
  WHERE PayrollClassCode = 'H'
  UNION ALL 
  SELECT 'HOL', employeenumber, HOLIDAY_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 * holidayhours * HourlyRate AS Amount
  FROM #wtf1
  WHERE PayrollClassCode = 'H'
  UNION ALL
  SELECT 'FICA', employeenumber, EMPLR_FICA_EXPENSE AS Account,
    GROSS_DIST/100.0 *
      CASE PayrollClassCode
        WHEN 'H' THEN round(((((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) 
            + (overtimehours * HourlyRate * 1.5)) - coalesce(FicaEx, 0)) * yficmp/100.0, 2)
        WHEN 'S' THEN round(((salary/14 * @NumDays) - (coalesce(FicaEx, 0)) * @NumDays/14) * yficmp/100.0, 2)
      END AS Amount
  FROM #wtf1
  UNION ALL 
  SELECT 'MEDIC', employeenumber, EMPLR_MED_EXPENSE AS Account,
    GROSS_DIST/100.0 *
      CASE PayrollClassCode
        WHEN 'H' THEN round(((((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) - coalesce(MedEx, 0)) * ymedmp/100.0, 2)
        WHEN 'S' THEN round(((salary/14 * @NumDays) - (coalesce(MedEx, 0)) * @NumDays/14) * ymedmp/100.0, 2)
      END AS Amount
  FROM #wtf1   
  UNION ALL 
  SELECT 'RETIRE', employeenumber, EMPLR_CONTRIBUTIONS AS Account,
      CASE 
        WHEN FIXED_DED_AMT IS NULL THEN 0
        ELSE 
        GROSS_DIST/100.0 *
          CASE PayrollClassCode
            WHEN 'H' THEN 
              CASE 
                WHEN (((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) * FIXED_DED_AMT/200.0 -- 1/2 of employee contr
                    < .02 * (((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) 
                  THEN round((((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) * FIXED_DED_AMT/200.0, 2)
                ELSE round(.02 * (((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)), 2) 
              END  
            WHEN 'S' THEN 
              CASE  
                WHEN ((salary/14) * @NumDays) * (FIXED_DED_AMT/200.0) < .02 * ((salary/14) * @NumDays) THEN round((salary/14 * @NumDays) * (FIXED_DED_AMT/200.0), 2)
                ELSE round((.02 * (salary/14) * @NumDays), 2)                  
              END 
          END 
      END AS Amount     
  FROM #wtf1 ) x
WHERE Amount > 0
GROUP BY employeenumber, Account;

-- commission
SELECT a.*, ac.amount AS comm,
  p.DIST_CODE, p.SEQ_NUMBER, p.GROSS_DIST, p.GROSS_EXPENSE_ACT_, p.OVERTIME_ACT_, 
  p.VACATION_EXPENSE_ACT_, p.HOLIDAY_EXPENSE_ACT_, p.SICK_LEAVE_EXPENSE_ACT_,
  p.EMPLR_FICA_EXPENSE, p.EMPLR_MED_EXPENSE, p.EMPLR_CONTRIBUTIONS,
  pc.yficmp, pc.ymedmp, fe.FicaEx, me.MedEx, d.FIXED_DED_AMT 
-- DROP TABLE #wtf2
INTO #wtf2
FROM ( -- a: 1 row per store/emp#  emp IS payed bi-weekly, dist code NOT IN list, 
       -- the empkey that was valid ON last payroll ending date
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
      PayPeriodCode, DistCode, salary, hourlyrate
  FROM edwEmployeeDim e
  WHERE hiredate <= @thruDate
    AND termdate >= @fromDate
    AND storecode <> 'ry3'  
    AND employeenumber NOT IN ('190915','1160100')
    AND PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV') 
    AND employeekey = (
      SELECT MAX(employeekey)
      FROM edwEmployeeDim 
      WHERE employeenumber = e.employeenumber
        AND storecode = e.storecode
        AND employeekeyfromdate <=  @thruDate
      GROUP BY storecode, employeenumber)) a   
INNER JOIN AccrualCommissions ac ON a.storecode = ac.storecode 
  AND a.employeekey = ac.employeekey
  AND ThruDate = CAST(@Date AS sql_date)
  AND Amount <> 0  
LEFT JOIN ( -- clock hours for the interval BY empkey
  SELECT employeekey, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
    sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
    sum(holidayhours) AS holidayhours
  FROM day d
  INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
  WHERE thedate BETWEEN @fromDate AND @thruDate
  GROUP BY employeekey) b ON a.employeekey = b.employeekey    
LEFT JOIN stgArkonaPYACTGR p ON a.StoreCode = p.COMPANY_NUMBER 
  AND a.DistCode = p.DIST_CODE
  AND CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL pc ON a.storecode = pc.yco#
  AND pc.ycyy = 112 -- 100 + (year(@ThruDate) - 2000) 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) fe ON a.employeenumber = fe.EMPLOYEE_NUMBER  
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) me ON a.employeenumber = me.EMPLOYEE_NUMBER    
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT d ON a.storecode = d.COMPANY_NUMBER
  AND a.employeenumber = d.EMPLOYEE_NUMBER 
  AND d.DED_PAY_CODE IN ('91', '99') 
WHERE b.employeekey IS NOT NULL;

select @Journal as Journal, @Date as [Date], Account, EmployeeNumber as Control, 
  @Doc as Document, @Ref as Reference, round(sum(Amount),2) as Amount, 
  'Payroll Accrual' as Description
-- DROP TABLE #jon2
INTO #jon2
FROM (
  SELECT 'Comm', employeenumber, GROSS_EXPENSE_ACT_ AS Account,
    GROSS_DIST/100.0 * Comm AS Amount
  FROM #wtf2
  UNION ALL
  SELECT 'FICA', employeenumber, EMPLR_FICA_EXPENSE AS Account,
    round(GROSS_DIST/100.0 * Comm * yficmp/100.0, 2) AS Amount
  FROM #wtf2
  UNION ALL   
  SELECT 'MEDIC', employeenumber, EMPLR_MED_EXPENSE AS Account,
    round(GROSS_DIST/100.0 * Comm * ymedmp/100.0, 2) AS Amount
  FROM #wtf2  
  UNION ALL   
  SELECT 'RETIRE', employeenumber, EMPLR_CONTRIBUTIONS AS Account,
  CASE 
    WHEN FIXED_DED_AMT IS NULL THEN 0
    ELSE 
      GROSS_DIST/100.0 *
        CASE 
          WHEN Comm * FIXED_DED_AMT/200.0 < .02 * Comm
            THEN round(Comm * FIXED_DED_AMT/200.0, 2)
          ELSE round(.02 * Comm, 2) 
        END 
    END AS Amount      
  FROM #wtf2 ) x
WHERE Amount <> 0
GROUP BY employeenumber, Account;  
-- greg
--INSERT INTO #jon2 values(@Journal, @Date, '12101', '1130426', @Doc, @Ref, 3415.40 * @NumDays/14, 'Payroll Accrual');
-- changed, per Jeri 3/30/12
--INSERT INTO #jon2 values(@Journal, @Date, '12101', '1130426', @Doc, @Ref, 1415.40 * @NumDays/14, 'Payroll Accrual');
--INSERT INTO #jon2 values(@Journal, @Date, '12102', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');
--INSERT INTO #jon2 values(@Journal, @Date, '12104', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');
--INSERT INTO #jon2 values(@Journal, @Date, '12105', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');
--INSERT INTO #jon2 values(@Journal, @Date, '12106', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');
-- 9/4/12 changed accounts per Jeri
INSERT INTO #jon2 values(@Journal, @Date, '12001', '1130426', @Doc, @Ref, 1415.40 * @NumDays/14, 'Payroll Accrual');
INSERT INTO #jon2 values(@Journal, @Date, '12002', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');
INSERT INTO #jon2 values(@Journal, @Date, '12004', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');
INSERT INTO #jon2 values(@Journal, @Date, '12005', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');
INSERT INTO #jon2 values(@Journal, @Date, '12006', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');

-- output
SELECT journal, date, account, control, document, reference, SUM(amount) AS Amount, description
INTO #entries
FROM (
SELECT * FROM #jon1
UNION ALL 
SELECT * FROM #jon2) x
WHERE amount <> 0
GROUP BY journal, date, account, control, document, reference, description;

-- reversing entries
ALTER TABLE #entries
ALTER COLUMN control control CIChar(9);

INSERT INTO #entries
select @Journal as Journal, @Date as [Date], 
  case
    when LEFT(account, 1) = '1' then '132101'
    when left(Account, 1) = '2' then '232103'
    when left(Account, 1) = '3' then '332100'
  end  as Account, @Doc AS Control, 
  @Doc as Document, @Ref as Reference, round(sum(Amount),2)* -1 as Amount, 
  'Payroll Accrual' as Description
FROM #entries
GROUP BY LEFT(account, 1);

SELECT journal, date, account, control, document, reference, 
round(amount, 2) AS amount, description 
FROM #entries
WHERE LEFT(account,1) <> '3'
  AND LEFT(control,1) <> '3';

/*
-- 6/1 --
-- zAccrual populated Feb, Mar, Apr, May
CREATE TABLE zAccrual (
  journal cichar(3),
  [date] date,
  account cichar(30), NK
  control cichar(9), 
  document cichar(9),
  reference cichar(9),
  amount money,
  description cichar(15)) IN database;
  
INSERT INTO zaccrual
SELECT journal, cast(date AS sql_date), account, control, document, reference, 
round(amount, 2) AS amount, description 
FROM #entries
WHERE LEFT(account,1) <> '3'
  AND LEFT(control,1) <> '3';

9/5
DELETE FROM zaccrual WHERE date = '08/31/2012'

-- 6/1 --

SELECT e.name, Amount 
FROM #entries x
LEFT JOIN (
  select distinct employeenumber, name
  from edwEmployeeDim) e ON x.control = e.employeenumber
WHERE account = '12305'; 

-- service compensation for march
SELECT  name, technumber, employeenumber, sum(amount) AS amount
FROM #entries e
INNER JOIN (
  SELECT distinct employeenumber, name, technumber
  -- SELECT *
  FROM edwEmployeeDim
  WHERE technumber IS NOT NULL 
    AND storecode = 'RY1'
    AND pyDeptCode = '04') t ON e.control = t.employeenumber
WHERE e.amount > 0
GROUP BY name, technumber, employeenumber
ORDER BY technumber

11/1/12
-- accounts, description AND total
SELECT account, gmdesc, SUM(amount)
FROM #entries a
LEFT JOIN stgArkonaGLPMAST b ON a.account = b.gmacct
  AND b.gmyear = 2012
GROUP BY account, gmdesc  

-- 12/4/12
SELECT a.*, gmdesc
FROM zaccrual a 
LEFT JOIN stgArkonaGLPMAST b ON a.account = b.gmacct
  AND b.gmyear = 2012
  
SELECT DISTINCT date FROM zaccrual  
  
SELECT account, gmdesc,
  SUM(CASE WHEN date = '02/29/2012' THEN amount ELSE 0 END) AS feb,
  SUM(CASE WHEN date = '03/31/2012' THEN amount ELSE 0 END) AS mar,
  SUM(CASE WHEN date = '04/30/2012' THEN amount ELSE 0 END) AS apr,
  SUM(CASE WHEN date = '05/31/2012' THEN amount ELSE 0 END) AS may,
  SUM(CASE WHEN date = '06/30/2012' THEN amount ELSE 0 END) AS jun,
  SUM(CASE WHEN date = '07/31/2012' THEN amount ELSE 0 END) AS jul,
  SUM(CASE WHEN date = '08/31/2012' THEN amount ELSE 0 END) AS aug,
--  SUM(CASE WHEN date = '09/30/2012' THEN amount ELSE 0 END) AS sep, fucked up AND didn't INSERT sep INTO zaccrual
  SUM(CASE WHEN date = '10/31/2012' THEN amount ELSE 0 END) AS oct,
  SUM(CASE WHEN date = '11/30/2012' THEN amount ELSE 0 END) AS nov
FROM zaccrual a 
LEFT JOIN stgArkonaGLPMAST b ON a.account = b.gmacct
  AND b.gmyear = 2012  
GROUP BY account, gmdesc

*/

-- SELECT * FROM #entries
