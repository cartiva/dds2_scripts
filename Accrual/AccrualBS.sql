/*
one time shot to handle body shop NOT being IN team pay
*/
DECLARE @thruDate date;
@thruDate = '06/30/2015';
DELETE FROM AccrualBS1;
INSERT INTO AccrualBS1


SELECT e.storecode, d.employeenumber, d.gross, d.pto,
  d.gross + d.pto AS TotalNonOTPay,
  f.GROSS_DIST, f.GROSS_EXPENSE_ACT_, f.SICK_LEAVE_EXPENSE_ACT_,  
  f.EMPLR_FICA_EXPENSE, f.EMPLR_MED_EXPENSE, f.EMPLR_CONTRIBUTIONS, 
  g.yficmp, g.ymedmp,
  coalesce(h.FicaEx, 0) AS FicaEx,
  coalesce(i.MedEx, 0) AS MedEx,
  coalesce(j.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
FROM  accrualbsgross d
    
LEFT JOIN edwEmployeeDim e on d.employeenumber = e.employeenumber
  AND e.currentrow = true    
LEFT JOIN zFixPyactgr f on e.storecode = f.company_number
  AND e.distcode = f.dist_code    
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL g ON e.storecode = g.yco#
  AND g.ycyy = 100 + (year(@thruDate) - 2000) -- 112 -    
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) h ON d.employeenumber = h.EMPLOYEE_NUMBER 
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) i ON d.employeenumber = i.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT j ON e.storecode = j.COMPANY_NUMBER
  AND d.employeenumber = j.EMPLOYEE_NUMBER 
--  AND h.DED_PAY_CODE IN ('91', '99');
-- *666
  AND 
    case 
      when j.employee_number = '11650' then j.ded_pay_code = '99'
      else j.DED_PAY_CODE = '91'
    END;

DELETE FROM AccrualBS2;   
INSERT INTO AccrualBS2
SELECT 'Gross', employeenumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * Gross AS Amount
FROM AccrualBS1
UNION ALL 
SELECT 'PTO', employeenumber, PTOAccount AS Account,
  GrossDistributionPercentage/100.0 * PTO AS Amount
FROM AccrualBS1
WHERE PTO <> 0
UNION ALL
SELECT 'FICA', employeenumber, FicaAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(FicaExempt, 0)) * FicaPercentage/100.0, 2)) AS Amount
FROM AccrualBS1
GROUP BY employeenumber, FicaAccount
UNION ALL 
SELECT 'MEDIC', employeenumber, MedicareAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(MedicareExempt, 0)) * MedicarePercentage/100.0, 2)) AS Amount
FROM AccrualBS1
GROUP BY employeenumber, MedicareAccount
UNION ALL 
SELECT  'RETIRE', employeenumber, RetirementAccount AS Account,
  sum(CASE 
    WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
    
    CASE 
      WHEN TotalNonOTPay * FixedDeductionAmount/200.0  < .02 * TotalNonOTPay
        THEN round(GrossDistributionPercentage/100.0 * TotalNonOTPay * FixedDeductionAmount/200.0, 2)
      ELSE round(GrossDistributionPercentage/100.0 * .02 * TotalNonOTPay, 2) 
    END 
  END) AS Amount     
FROM AccrualBS1
GROUP BY employeenumber, RetirementAccount;


