/*
1/2/15
1. DO AccrualSales, THEN manually enter the numbers FROM Jeri (just LIKE the regualr accruaCommission)
2. Need AccrualDates (beginning of accrual - january 2015 - one time fix.sql_
3. run accrualSales1, accrualSales2 
*/
THEN IN the actual accrual, uncomment out the UNION on accrualSales2 (2 places,
  generating the base AccrualFileForJeri AND the reversing entries)

/*
CREATE TABLE AccrualSales ( 
      StoreCode CIChar( 3 ),
      EmployeeNumber CIChar( 7 ),
      EmployeeKey Integer,
      Name CIChar( 25 ),
      FromDate Date,
      ThruDate Date,
      Amount Money) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualSales',
   'AccrualSales.adi',
   'NAME',
   'Name',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualSales',
   'AccrualSales.adi',
   'EMPLOYEEKEY',
   'EmployeeKey',
   '',
   2,
   512,
   '' ); 
*/   

/* 1/2/15, ADD rj
INSERT INTO accrualSales 
SELECT storecode, employeenumber, employeekey, name, '12/14/2014','12/31/2014', 2000
FROM edwEmployeeDim 
WHERE employeenumber = '140500' 
  AND currentrow = true
  
1/1/15 ADD nick shirek ry1  
ADD nick shirek ry1 , ben foster, ben knudson 
INSERT INTO accrualSales 
SELECT storecode, employeenumber, employeekey, name, '12/27/2015','12/31/2015', 0
FROM edwEmployeeDim 
WHERE employeenumber = '1126300' 
  AND currentrow = true
  
INSERT INTO accrualSales 
SELECT storecode, employeenumber, employeekey, name, '12/27/2015','12/31/2015', 0
FROM edwEmployeeDim 
WHERE employeenumber = '148050' 
  AND currentrow = true
  
INSERT INTO accrualSales 
SELECT storecode, employeenumber, employeekey, name, '12/27/2015','12/31/2015', 0
FROM edwEmployeeDim 
WHERE employeenumber = '179380' 
  AND currentrow = true  
  
-- *c* changed dist codes to include the above dudes  
-- *d* ADD salaried to get the managers

shit, now why IS honda low
think it IS zfixpyactgr --- yep

ended up excluding ben foster 148050, multiple distcodes fucked up pk on accrualsales2

*/
DECLARE @fromdate string;
DECLARE @thrudate string;
@fromdate = '12/25/2016'; -- last payroll enddate + 1
@thrudate = '12/31/2016'; -- EOM
--DELETE FROM AccrualSales;
INSERT INTO AccrualSales
SELECT a.storecode, a.employeenumber, a.employeekey, a.name,
  CAST(@fromdate AS sql_Date) AS FromDate, CAST(@thrudate AS sql_date) AS ThruDate, 0 AS Amount
--INTO #wtf  
FROM edwEmployeeDim a
LEFT JOIN ( -- clock hours for the interval
    SELECT storecode, employeenumber, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
      sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
      sum(holidayhours) AS holidayhours
    FROM day d
    INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
    LEFT JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
    WHERE thedate BETWEEN CAST(@fromdate AS sql_date) AND CAST(@thrudate AS sql_date)
    GROUP BY storecode, employeenumber) b ON a.storecode = b.storecode 
  AND a.employeenumber = b.employeenumber 
WHERE EmployeeKeyFromDate = (
    SELECT MAX(EmployeekeyFromDate)
    FROM edwEmployeeDim
    WHERE EmployeeNumber = a.Employeenumber
      AND EmployeeKeyFromDate <= CAST(@thrudate AS sql_date)
    GROUP BY employeenumber)
  AND PayPeriodCode = 'S'
-- *c*
--  AND DistCode IN ('SALE','TEAM')  
  AND DistCode IN ('SALE','TEAM', 'SALM', 'BEN') 
  AND b.storecode IS NOT NULL;-- ORDER BY name;     



/*
CREATE TABLE AccrualSales1 ( 
      StoreCode CIChar( 3 ),
      EmployeeNumber CIChar( 9 ),
      TotalPay Money,
      GrossDistributionPercentage Double( 2 ),
      GrossAccount CIChar( 10 ),
      FicaAccount CIChar( 10 ),
      MedicareAccount CIChar( 10 ),
      RetirementAccount CIChar( 10 ),
      FicaPercentage Double( 2 ),
      MedicarePercentage Double( 2 ),
      FicaExempt Money,
      MedicareExempt Money,
      FixedDeductionAmount Double( 2 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualSales1',
   'AccrualSales1.adi',
   'PK',
   'EmployeeNumber;GrossAccount',
   '',
   2051,
   512,
   '' ); 
*/
   
DELETE FROM AccrualSales1;
INSERT INTO AccrualSales1
SELECT a.storecode, a.employeenumber, 
  a.salary * aa.NumDays14 AS TotalPay,
  d.GROSS_DIST, d.GROSS_EXPENSE_ACT_, 
  d.EMPLR_FICA_EXPENSE, d.EMPLR_MED_EXPENSE, d.EMPLR_CONTRIBUTIONS,
  e.yficmp, e.ymedmp, coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
  coalesce(h.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
FROM edwEmployeeDim a
INNER JOIN AccrualDates aa on 1 = 1
INNER JOIN ( -- the possibly dodgy assumption here IS that employeekeys are sequential, but they are autoinc 
  SELECT a.Employeenumber, MAX(a.employeekey) AS employeekey
  FROM edwEmployeeDim a
  INNER JOIN AccrualDates aa on 1 = 1
  INNER JOIN edwEmployeeDim b on a.employeekey =  b.employeekey -- ALL employeekeys valid for the interval
    AND b.employeekeyFromDate < aa.thrudate
    AND b.employeekeyThruDate > aa.fromdate
  WHERE a.hiredate <= aa.thrudate -- only folks active during the interval
    AND a.termdate >= aa.fromdate
    AND a.storecode <> 'ry3'  
    AND a.employeenumber NOT IN ('190915','1160100')
    AND a.PayPeriodCode = 'S'
-- *c*
--  AND a.DistCode IN ('SALE','TEAM')  
    AND a.DistCode IN ('SALE','TEAM', 'SALM', 'BEN')
-- *d*    
--    AND a.PayRollClassCode = 'C' -- commissioned only
    AND a.PayRollClassCode IN ('C', 'S') 
  GROUP BY a.employeenumber) x on a.employeekey = x.employeekey
LEFT JOIN zFixPYACTGR/*stgArkonaPYACTGR*/ d ON a.StoreCode = d.COMPANY_NUMBER -- distribution
  AND a.DistCode = d.DIST_CODE
--  AND d.CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL e ON a.storecode = e.yco#
  AND e.ycyy = 100 + (year(aa.ThruDate) - 2000) -- 112 -  
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) f ON a.employeenumber = f.EMPLOYEE_NUMBER    
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) g ON a.employeenumber = g.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT h ON a.storecode = h.COMPANY_NUMBER
  AND a.employeenumber = h.EMPLOYEE_NUMBER 
  AND h.DED_PAY_CODE IN ('91', '99');  

/*  
CREATE TABLE AccrualSales2 ( 
      Category CIChar( 12 ),
      EmployeeNumber CIChar( 9 ),
      Account CIChar( 10 ),
      Amount Money) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualSales2',
   'AccrualSales2.adi',
   'PK',
   'EmployeeNumber;Category;Account',
   '',
   2051,
   512,
   '' );  
*/  


DELETE FROM AccrualSales2;
INSERT INTO AccrualSales2
-- DROP TABLE #wtf;

SELECT *
--INTO #wtf
FROM (
  SELECT 'Gross', EmployeeNumber, GrossAccount AS Account,
    GrossDistributionPercentage/100.0 * TotalPay AS Amount
  FROM (
    SELECT a.storecode, a.amount AS TotalPay, c.*
    FROM AccrualSales a
    INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
    LEFT JOIN (
      SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
        RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
        MedicareExempt, FixedDeductionAmount 
      FROM accrualsales1) c on a.employeenumber = c.employeenumber
    WHERE a.amount <> 0) a 
  UNION ALL 
  SELECT 'FICA', EmployeeNumber, FicaAccount AS Account,
    round((GrossDistributionPercentage/100.0) * (TotalPay - (coalesce(FicaExempt, 0)) * (select NumDays/14 FROM AccrualDates)) * FicaPercentage/100.0, 2) AS Amount
  FROM (
    SELECT a.storecode, a.amount AS TotalPay, c.*
    FROM AccrualSales a
    INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
    LEFT JOIN (
      SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
        RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
        MedicareExempt, FixedDeductionAmount 
      FROM accrualsales1) c on a.employeenumber = c.employeenumber
    WHERE a.amount <> 0) b 
  UNION ALL 
  SELECT 'MEDIC', EmployeeNumber, MedicareAccount AS Account,
  --  round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * (NumDays/14)) * MedicarePercentage/100.0, 2) AS Amount
    round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * (select NumDays/14 FROM AccrualDates)) * MedicarePercentage/100.0, 2) AS Amount
  FROM (
    SELECT a.storecode, a.amount AS TotalPay, c.*
    FROM AccrualSales a
    INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
    LEFT JOIN (
      SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
        RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
        MedicareExempt, FixedDeductionAmount 
      FROM accrualsales1) c on a.employeenumber = c.employeenumber
    WHERE a.amount <> 0) c
  UNION ALL 
  SELECT 'RETIRE', EmployeeNumber, RetirementAccount AS Account,
  CASE 
    WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
    CASE 
      WHEN (TotalPay * FixedDeductionAmount/200.0) < .02 * TotalPay 
        THEN round((GrossDistributionPercentage/100.0 * TotalPay) * (FixedDeductionAmount/200.0), 2)
      ELSE round((GrossDistributionPercentage/100.0) * TotalPay * .02, 2) 
    END               
  END AS Amount  
  FROM (
  SELECT a.storecode, a.amount AS TotalPay, c.*
    FROM AccrualSales a
    INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
    LEFT JOIN (
      SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
        RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
        MedicareExempt, FixedDeductionAmount 
      FROM accrualsales1) c on a.employeenumber = c.employeenumber
    WHERE a.amount <> 0) d) x
WHERE employeenumber IS NOT NULL AND employeenumber <> '148050';    





