/*
DROP TABLE AccrualFlatRateGrossPerDay;
CREATE TABLE AccrualFlatRateGrossPerDay (
  employeenumber cichar(9) constraint NOT NULL,
  lastname cichar(25) constraint NOT NULL,
  firstname cichar(25) constraint NOT NULL,
  fromdate date constraint NOT NULL,
  thrudate date constraint NOT NULL,
  gross_per_day double  constraint NOT NULL default '0',
  constraint pk primary key(employeenumber,fromdate)) IN database;

DROP TABLE AccrualFlatRateGross; 
CREATE TABLE AccrualFlatRateGross (
  storecode cichar(3) constraint NOT NULL,
  employeenumber cichar(9) constraint NOT NULL,
  lastname cichar(25) constraint NOT NULL,
  firstname cichar(25) constraint NOT NULL,
  fromdate date constraint NOT NULL,
  thrudate date constraint NOT NULL,
  gross double  constraint NOT NULL default '0',
  distcode cichar(4) constraint NOT NULL,
  constraint pk primary key(employeenumber,fromdate)) IN database;  
  
CREATE TABLE AccrualFlatRate1 ( 
      StoreCode CIChar( 3 ) constraint NOT NULL,
      EmployeeNumber CIChar( 9 ) constraint NOT NULL,
      Gross Money constraint NOT NULL,
      PTO Money constraint NOT NULL,
      TotalNonOTPay Money constraint NOT NULL,
      GrossDistributionPercentage Double( 2 ) constraint NOT NULL,
      GrossAccount CIChar( 10 ) constraint NOT NULL,
      PTOAccount CIChar( 10 ) constraint NOT NULL,
      FicaAccount CIChar( 10 ) constraint NOT NULL,
      MedicareAccount CIChar( 10 ) constraint NOT NULL,
      RetirementAccount CIChar( 10 ) constraint NOT NULL,
      FicaPercentage Double( 2 ) constraint NOT NULL,
      MedicarePercentage Double( 2 ) constraint NOT NULL,
      FicaExempt Money constraint NOT NULL,
      MedicareExempt Money constraint NOT NULL,
      FixedDeductionAmount Double( 2 ) constraint NOT NULL) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualFlatRate1',
   'AccrualFlatRate1.adi',
   'PK',
   'EmployeeNumber;GrossAccount',
   '',
   2051,
   512,
   '' ); 
      
CREATE TABLE AccrualFlatRate2 ( 
      Category CIChar( 12 ),
      EmployeeNumber CIChar( 9 ),
      Account CIChar( 10 ),
      Amount Money) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualFlatRate2',
   'AccrualFlatRate2.adi',
   'PK',
   'EmployeeNumber;Category;Account',
   '',
   2051,
   512,
   '' );    
*/
/* 
team pay can be the MAX of the previous 3 pay periods FROM tpData
detail will have to be based on previous 3 paychecks 
*/
/*
-- *b* Oct accrual full pay period, no need to average
-- *c* need to include brian peterson
-- *d* Nov full pay period + 2 days
-- *e* 8/1/16 fucked up with a rehire tech IN detail, Jason Olson
-- *f*
  need to separate out gross AND pto
  gross IS estimated, pto IS NOT
  for AccrualFlatRateGross, will need to ADD a pto COLUMN
  
  shit, looks LIKE a separate calculation for gross USING 3 pay periods, vs pto which IS just
  current pay period
  
  pto will just be actual pto pay (hol+vac+pto) for the days IN the accrual range 
  
  ALTER TABLE AccrualFlatRateGross
  ADD COLUMN ptopay double;  
  
3/1/17  
 *x*
Constraint violation while updating column "FicaPercentage".  The column cannot be updated to a NULL value
    AccrualFlatRate1
	    forgot to DO zfixpyactgr, which IS true 
		but fuck me, no 2017 values IN stgArkonaPYNCTRL???
		fixed: inserted 2017 values INTO stgArkonaPYCNTRL
		
*/
DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromdate = '06/25/2017';
@thrudate = '06/30/2017';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);
DELETE FROM AccrualFlatRateGross;    
INSERT INTO AccrualFlatRateGross   
-- *f*
SELECT e.*, coalesce(f.ptopay, 0) AS ptopay
FROM (	
  SELECT storecode, employeenumber, lastname, firstname, @fromDate, @thruDate, 
  -- *f*
  --  @days * max(round((commissionpay + ptopay)/10, 2)), distcode 
    @days * max(round(commissionpay/10, 2)) AS gross, distcode 
  FROM (    
    select aa.storecode, a.employeenumber, a.lastname, a.firstname, 
      round(techtfrrate*teamprofpptd*techclockhourspptd/100, 2) AS commissionPay,
      round(techHourlyRate * (techvacationhourspptd + techptohourspptd + techholidayhourspptd), 2) AS ptoPay,
      aa.distcode
    FROM scotest.tpdata a
    LEFT JOIN edwEmployeeDim aa on a.employeenumber = aa.employeenumber
      AND aa.currentrow = true
    INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey  
    WHERE thedate = payperiodend
      AND payperiodseq IN ( -- last 3 full pay periods
        SELECT top 3 DISTINCT payperiodseq
        FROM scotest.tpdata
        WHERE payperiodend <= @thrudate
        ORDER BY payperiodseq DESC)) x    
  WHERE EXISTS ( -- currently a team pay tech
    SELECT 1
    FROM scotest.tpdata
    WHERE employeenumber = x.employeenumber
      AND thedate = @thrudate)    
  OR (x.lastname = 'peterson' AND x.firstname = 'brian')      
  GROUP BY storecode, employeenumber, lastname, firstname, distcode) e
-- *f*  
LEFT JOIN ( -- pto
  select a.employeenumber, 
    sum(round(techHourlyRate * (techvacationhoursday + techptohoursday + techholidayhoursday), 2)) AS ptoPay
  FROM scotest.tpdata a
  LEFT JOIN edwEmployeeDim aa on a.employeenumber = aa.employeenumber
    AND aa.currentrow = true
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey  
  WHERE thedate BETWEEN @fromdate AND @thrudate
  GROUP BY a.employeenumber) f on e.employeenumber = f.employeenumber;  

/*
detail
prior to 10/1, had to enter detail accrual FROM aubreys spreadsheet
got her ok to use the numbers i generate

INSERT INTO AccrualFlatRateGross
SELECT storecode, employeenumber, lastname, firstname, @fromDate, @thruDate, 
  0 AS gross, distcode
FROM edwEmployeeDim 
WHERE distcode = 'wtec'
  AND payrollclasscode = 'c'
  AND currentrow = true;
  
** manually enter numbers FROM Aubrey's spreadsheet  
SELECT *
FROM AccrualFlatRateGross
WHERE distcode = 'wtec'
  AND thrudate = '08/31/2015'
ORDER BY lastname, firstname;

-- *a*
FROM Aubrey 11/2/15:Wyatt Telken, Ariella Kingbird and Nick Roehrich 
will never have enough hours in a pay period to get bonus pay 
so I would put them at 0
12/1/15: Hunter Nelson, Brian Gonzalez

*/
INSERT INTO AccrualFlatRateGross  
SELECT 'RY1', employeenumber, lastname, firstname, @fromdate, @thrudate,  
-- *f*
--  coalesce(round((12 * flaghours) + ((rate - 12) * flaghours) + (12 * ptohours), 2), 0), 'WTEC'
  coalesce(round((12 * flaghours) + ((rate - 12) * flaghours), 2), 0), 'WTEC',
  coalesce(round(12 * ptohours, 2), 0)  AS ptopay
FROM (
  SELECT m.*, n.flaghours, round(flaghours/clockhours, 2) AS prof,
  -- hard code the prof matrix for now
    CASE
-- *a*  
      WHEN lastname IN ('Telken','Kingbird','Roehrich'/*,'Gonzalez'*/) THEN 12.0
      WHEN lastname = 'Nelson' AND firstname = 'Hunter' THEN 12.0   
      WHEN lastname = 'Horton' AND firstname = 'Eric' THEN 12.0
      WHEN lastname = 'Nelson' AND firstname = 'Kordell' THEN 12.0
      WHEN round(flaghours/clockhours, 2) <= 1.3 THEN 12.0   
      WHEN round(flaghours/clockhours, 2) >= 1.3 AND round(flaghours/clockhours, 2) < 1.4 THEN 12.5
      WHEN round(flaghours/clockhours, 2) >= 1.4 AND round(flaghours/clockhours, 2) < 1.5 THEN 13.5
      WHEN round(flaghours/clockhours, 2) >= 1.5 AND round(flaghours/clockhours, 2) < 1.6 THEN 15
      WHEN round(flaghours/clockhours, 2) >= 1.6 AND round(flaghours/clockhours, 2) < 1.7 THEN 16
      WHEN round(flaghours/clockhours, 2) >= 1.7 AND round(flaghours/clockhours, 2) < 1.8 THEN 17
      WHEN round(flaghours/clockhours, 2) >= 1.8 AND round(flaghours/clockhours, 2) < 1.9 THEN 18
      WHEN round(flaghours/clockhours, 2) >= 1.9 AND round(flaghours/clockhours, 2) < 2 THEN 19
      WHEN round(flaghours/clockhours, 2) > 2 THEN 20.0  
    END AS rate
  FROM (
    SELECT a.firstname, a.lastname, a.employeenumber, a.employeekey, 
      SUM(b.clockhours) AS clockhours, SUM(holidayhours+ptohours+vacationhours) AS ptohours,
      c.technumber, c.techkey
    FROM edwEmployeeDim a 
    LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
      AND b.datekey IN (
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN @fromdate AND @thrudate)
    LEFT JOIN dimtech c on a.employeenumber = c.employeenumber
-- *e*
      AND c.active = true    
      AND c.currentrow = true    
    WHERE a.distcode = 'wtec'
  --    AND a.currentrow = true
      AND a.active = 'active'
      AND a.employeekeyfromdate <= @thrudate
      AND a.employeekeythrudate > @fromdate
      AND a.payrollclass = 'commission'
    GROUP BY a.firstname, a.lastname, a.employeenumber, a.employeekey, c.technumber, c.techkey) m   
  LEFT JOIN (
    SELECT d.technumber, d.description, d.employeenumber, SUM(flaghours) AS flaghours
    FROM factrepairorder a
    INNER JOIN day b on a.closedatekey = b.datekey
    INNER JOIN dimtech d on a.techkey = d.techkey
    WHERE b.thedate between @fromdate AND @thrudate
      AND d.flagdeptcode = 're'
      AND d.employeenumber <> 'NA'
    GROUP BY d.technumber, d.description, d.employeenumber) n on m.employeenumber = n.employeenumber  ) x;  
  
    
DELETE FROM AccrualFlatRate1;
INSERT INTO AccrualFlatRate1
-- *f*
-- SELECT d.storecode, d.employeenumber, d.gross, 0 AS pto,
SELECT d.storecode, d.employeenumber, d.gross, ptopay AS pto,
  d.gross AS TotalNonOTPay,
  f.GROSS_DIST, f.GROSS_EXPENSE_ACT_, f.SICK_LEAVE_EXPENSE_ACT_,  
  f.EMPLR_FICA_EXPENSE, f.EMPLR_MED_EXPENSE, f.EMPLR_CONTRIBUTIONS, 
  g.yficmp, g.ymedmp,
  coalesce(h.FicaEx, 0) AS FicaEx,
  coalesce(i.MedEx, 0) AS MedEx,
  coalesce(j.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
FROM AccrualFlatRateGross d      
LEFT JOIN zFixPyactgr f on d.storecode = f.company_number
  AND d.distcode = f.dist_code    
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL g ON d.storecode = g.yco#
-- *x*  
  AND g.ycyy = 100 + (year(@thruDate) - 2000) -- 112 -   
--  AND g.ycyy = 116 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) h ON d.employeenumber = h.EMPLOYEE_NUMBER 
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) i ON d.employeenumber = i.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT j ON d.storecode = j.COMPANY_NUMBER
  AND d.employeenumber = j.EMPLOYEE_NUMBER 
--  AND h.DED_PAY_CODE IN ('91', '99');
-- *666
  AND 
    case 
      when j.employee_number = '11650' then j.ded_pay_code = '99'
      else j.DED_PAY_CODE = '91'
    END;

DELETE FROM AccrualFlatRate2;   
INSERT INTO AccrualFlatRate2
SELECT 'Gross', employeenumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * Gross AS Amount
FROM AccrualFlatRate1
UNION ALL 
SELECT 'PTO', employeenumber, PTOAccount AS Account,
  GrossDistributionPercentage/100.0 * PTO AS Amount
FROM AccrualFlatRate1
WHERE PTO <> 0
UNION ALL
SELECT 'FICA', employeenumber, FicaAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(FicaExempt, 0)) * FicaPercentage/100.0, 2)) AS Amount
FROM AccrualFlatRate1
GROUP BY employeenumber, FicaAccount
UNION ALL 
SELECT 'MEDIC', employeenumber, MedicareAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(MedicareExempt, 0)) * MedicarePercentage/100.0, 2)) AS Amount
FROM AccrualFlatRate1
GROUP BY employeenumber, MedicareAccount
UNION ALL 
SELECT  'RETIRE', employeenumber, RetirementAccount AS Account,
  sum(
    CASE 
      WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
      CASE 
        WHEN TotalNonOTPay * FixedDeductionAmount/200.0  < .02 * TotalNonOTPay
          THEN round(GrossDistributionPercentage/100.0 * TotalNonOTPay * FixedDeductionAmount/200.0, 2)
        ELSE round(GrossDistributionPercentage/100.0 * .02 * TotalNonOTPay, 2) 
      END 
    END) AS Amount     
FROM AccrualFlatRate1
GROUP BY employeenumber, RetirementAccount;
    