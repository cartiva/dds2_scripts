


SELECT m.gmdept, _201604, _201605, _201606, _201607, _201608, _201609, _201610, 
  _201611, _201702, _201703, _201704, _201705, _201706
FROM (
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201604
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2016
  WHERE a.year_month = 201604  AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) m
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201605
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2016
  WHERE a.year_month = 201605 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) n on m.gmdept = n.gmdept
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201606
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2016
  WHERE a.year_month = 201606 AND LEFT(a.account, 1) = '1' 
  ) x GROUP BY gmdept) o on n.gmdept = o.gmdept
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201607
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2016
  WHERE a.year_month = 201607 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) p on o.gmdept = p.gmdept  
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201608
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2016
  WHERE a.year_month = 201608 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) q on p.gmdept = q.gmdept  
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201609
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2016
  WHERE a.year_month = 201609 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) r on q.gmdept = r.gmdept  
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201610
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2016
  WHERE a.year_month = 201610 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) s on r.gmdept = s.gmdept  
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201611
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2016
  WHERE a.year_month = 201611 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) t on s.gmdept = t.gmdept    
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201702
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2017
  WHERE a.year_month = 201702 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) u on t.gmdept = u.gmdept    
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201703
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2017
  WHERE a.year_month = 201703 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) v on u.gmdept = v.gmdept    
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201704
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2017
  WHERE a.year_month = 201704 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) w on v.gmdept = w.gmdept    
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201705
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2017
  WHERE a.year_month = 201705 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) y on w.gmdept = y.gmdept   
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _201706
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2017
  WHERE a.year_month = 201706 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) yy on w.gmdept = yy.gmdept     