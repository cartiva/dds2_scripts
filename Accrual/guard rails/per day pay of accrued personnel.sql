DROP TABLE ext_arkona_pyptbdta;
CREATE TABLE ext_arkona_pyptbdta(
    COMPANY_NUMBER        cichar(3),       
    PAYROLL_CEN_YEAR      integer,    
    PAYROLL_RUN_NUMBER    integer,    
    SEQUENCE              integer,    
    PAYROLL_START_DATE    integer,    
    PAYROLL_END_DATE      integer,    
    CHECK_DATE            integer,    
    SELECTED_DEPTS        cichar(1000),    
    SELECTED_CLASS        cichar(5),       
    SELECTED_PAY_PER_     cichar(10),      
    DESCRIPTION           cichar(40),      
    BATCH_STATUS          cichar(1),       
    STATUS_DATE           integer,    
    STATUS_TIME           integer,    
    OPERATION             cichar(1),       
    STEP                  cichar(6),       
    STEP_YBLSTEP          cichar(6),       
    PROGRAM               cichar(10),      
    USER_                  cichar(10),      
    WORK_STATION          cichar(10),      
    LDA                   cichar(1024),    
    CHECK_COUNT           integer,    
    GROSS_PAY             numeric(13,2), 
    NET_PAY               numeric(13,2), 
    DD_TRANS_USER         cichar(10),      
    DD_TRANS_WORKSTATION  cichar(10) ,     
    DD_TRANS_DATE         integer,    
    DD_TRANS_TIME         integer,    
    DD_TRANS_STATUS       cichar(1),       
    DD_TRANS_REPORT_FLAGS cichar(10),      
    SELECTED_RPT_CD1      cichar(200),     
    SELECTED_RPT_CD2      cichar(200),     
    PAY_PERIOD_OVERRIDE   cichar(1),       
    FLAG_1                cichar(1),       
    FLAG_2                cichar(1),       
    FLAG_3                cichar(1),       
    FLAG_4                cichar(1),       
    FLAG_5                cichar(1)) IN database;       
    

-- payroll batches that are for biweekly pay periods onl, RY1 only, YTD
-- so, each of these paychecks are for 14 days
-- DROP TABLE #payroll_run_number;
SELECT DISTINCT company_number, payroll_run_number, payroll_start_date, 
  payroll_end_date, check_date
INTO #payroll_run_number
FROM (
  SELECT company_number, payroll_run_number, sequence,
    cast(createTimestamp(  
      CAST(LEFT(CAST(payroll_start_date AS sql_char),4) AS sql_integer),
      CAST(substring(CAST(payroll_start_date AS sql_char), 5, 2) AS sql_integer),
      CAST(substring(CAST(payroll_start_date AS sql_char), 7, 2) AS sql_integer),
      0,0,0,0) AS sql_date) AS payroll_start_date,
    cast(createTimestamp(  
      CAST(LEFT(CAST(payroll_end_date AS sql_char),4) AS sql_integer),
      CAST(substring(CAST(payroll_end_date AS sql_char), 5, 2) AS sql_integer),
      CAST(substring(CAST(payroll_end_date AS sql_char), 7, 2) AS sql_integer),
      0,0,0,0) AS sql_date) AS payroll_end_date,  
    cast(createTimestamp(  
      CAST(LEFT(CAST(check_date AS sql_char),4) AS sql_integer),
      CAST(substring(CAST(check_date AS sql_char), 5, 2) AS sql_integer),
      CAST(substring(CAST(check_date AS sql_char), 7, 2) AS sql_integer),
      0,0,0,0) AS sql_date) AS check_date
  FROM ext_arkona_pyptbdta
  WHERE payroll_start_date > 20140000) a
LEFT JOIN day b on a.payroll_start_date = b.biweeklypayperiodstartdate
WHERE b.thedate IS not NULL 
  AND company_number = 'RY1'
ORDER BY payroll_start_date  

SELECT a.*, b.dayname, check_Date - payroll_end_date
FROM #payroll_run_number a
LEFT JOIN day b on a.check_date = b.thedate


SELECT * 
FROM stgArkonaPYHSHDTA a
LEFT JOIN edwEmployeeDim b on a.yhdemp = b.employeenumber
  AND b.currentrow = true
WHERE a.ypbnum = 531150    

-- employees accrued for may 2015
SELECT a.date, a.account, a.control, a.amount, b.name, b.pydept, b.distcode
FROM accrualfileforjeri a
INNER JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true
  AND b.storecode = 'ry1'


-- employees accrued for may 2015
-- with a bunch of detail FROM glpmast
SELECT a.date, a.account, a.control, a.amount, b.name, b.pydept, b.distcode,
  c.gmdesc, c.gmdept
FROM accrualfileforjeri a
INNER JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true
  AND b.storecode = 'ry1'
LEFT JOIN stgArkonaGLPMAST c on a.account = c.gmacct
  AND c.gmyear = 2015  
LEFT JOIN   
ORDER BY gmdept 

-- total accrual BY employee
SELECT a.date, a.control,  b.name, b.pydept, b.distcode, SUM(a.amount)
FROM accrualfileforjeri a
INNER JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true
  AND b.storecode = 'ry1'
GROUP BY a.date, a.control,  b.name, b.pydept, b.distcode  
ORDER BY name  

-- the ry1 employees accrued for may 2015
SELECT distinct a.control,  b.name, b.pydept
FROM accrualfileforjeri a
INNER JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true
  AND b.storecode = 'ry1'
  
 
SELECT COUNT(*) -- 35 payrolls for 1106399 - jay olson
-- SELECT yhdtgp/14, a.* -- pay per day for each payroll
-- SELECT SUM(yhdtgp)/(35*14) -- avg per day since 1/1/14: 231.62
FROM stgArkonaPYHSHDTA a
WHERE a.yhdco# = 'RY1'
  AND ypbnum IN (
    SELECT payroll_run_number
    FROM #payroll_run_number)
AND yhdemp = '1106399'  

-- but how DO i know IF someone should be accrued
-- hmmm, DO per day BY dept

SELECT a.*, b.gmdesc, b.gmdept
FROM (
  SELECT company_number, dist_code, description, gross_dist, gross_expense_act_
  FROM zFixPyactgr
  WHERE company_number = 'ry1'
  GROUP BY company_number, dist_code, description, gross_dist, gross_expense_act_) a
LEFT JOIN stgArkonaGLPMAST b on a.gross_expense_act_ = b.gmacct
  AND b.gmyear = 2015    
 
-- this may be going down the rabbit hole, with the whole dept/distcode/perc
--   analysis  
SELECT *
FROM (
  -- the ry1 employees accrued for may 2015
  SELECT distinct b.storecode, a.control,  b.name, b.pydept, b.distcode
  FROM accrualfileforjeri a
  INNER JOIN edwEmployeeDim b on a.control = b.employeenumber
    AND b.currentrow = true
    AND b.storecode = 'ry1') m  
LEFT JOIN (
  SELECT a.*, b.gmdesc, b.gmdept
  FROM (
    SELECT company_number, dist_code, description, gross_dist, 
      gross_expense_act_, seq_number
    FROM zFixPyactgr
    WHERE company_number = 'ry1'
    GROUP BY company_number, dist_code, description, gross_dist, 
      gross_expense_act_, seq_number) a
  LEFT JOIN stgArkonaGLPMAST b on a.gross_expense_act_ = b.gmacct
    AND b.gmyear = 2015) n on m.storecode = n.company_number AND m.distcode = n.dist_code
ORDER BY m.name, n.seq_number    

-- keep it simple, for each person IN an accrual, what IS the accrual pay(gross)/day
--  vs sorting out what percentage of whose pay goes to what departments
--  so, the person, the pydept, the accrual/day, the avg payroll/day

 


-- fuck, just want gross out of accrual, maybe, NOT sure

-- avg pay(gross)/day ytd
SELECT b.yhdemp, COUNT(*), SUM(yhdtgp), SUM(yhdtgp)/(COUNT(*) * 14)
-- SELECT yhdemp, count(*), SUM(yhdtgp)/(COUNT(*)*14)
FROM #payroll_run_number a
INNER JOIN stgArkonaPYHSHDTA b on a.payroll_run_number = b.ypbnum
  AND b.yhdco# = 'ry1'
INNER JOIN ( -- limit to employees in current accrual
  SELECT control
  FROM accrualfileforjeri
  GROUP BY control) c on b.yhdemp = c.control  
--ORDER BY b.yhdemp  
GROUP BY yhdemp  
  
 
SELECT e.*, f.name, f.pydept
FROM (  
  SELECT b.yhdemp, SUM(yhdtgp)/(COUNT(*) * 14) AS daily_pay
  FROM #payroll_run_number a
  INNER JOIN stgArkonaPYHSHDTA b on a.payroll_run_number = b.ypbnum
    AND b.yhdco# = 'ry1'
  INNER JOIN ( -- limit to employees in current accrual
    SELECT control
    FROM accrualfileforjeri
    GROUP BY control) c on b.yhdemp = c.control  
  GROUP BY yhdemp) e  
LEFT JOIN edwEmployeeDim f on e.yhdemp = f.employeenumber
  AND f.currentrow = true    
ORDER BY pydept

DROP TABLE #payroll;
SELECT pydept, SUM(daily_pay) AS daily_pay
INTO #payroll
FROM (
  SELECT e.*, f.name, f.pydept
  FROM (  
    SELECT b.yhdemp, cast(round(SUM(yhdtgp)/(COUNT(*) * 14), 0) as sql_integer)AS daily_pay
    FROM #payroll_run_number a
    INNER JOIN stgArkonaPYHSHDTA b on a.payroll_run_number = b.ypbnum
      AND b.yhdco# = 'ry1'
    INNER JOIN ( -- limit to employees in current accrual
      SELECT control
      FROM accrualfileforjeri
      GROUP BY control) c on b.yhdemp = c.control  
    GROUP BY yhdemp) e  
  LEFT JOIN edwEmployeeDim f on e.yhdemp = f.employeenumber
    AND f.currentrow = true) g 
GROUP BY pydept;   

DROP TABLE #accrual;
SELECT b.pydept, round(SUM(a.amount)/15, 0) AS daily_accrual
INTO #accrual
FROM accrualfileforjeri a
INNER JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true
  AND b.storecode = 'ry1'
GROUP BY b.pydept;  

SELECT a.*, b.daily_accrual
FROM #payroll a
full OUTER JOIN #accrual b on a.pydept = b.pydept

SELECT SUM(daily_pay), SUM(daily_accrual)
FROM #payroll a
full OUTER JOIN #accrual b on a.pydept = b.pydept



DROP TABLE accrual_history;
CREATE TABLE accrual_history (
  year_month integer,
  from_date date,
  thru_date date,
  account cichar(10),
  control cichar(9),
  amount numeric(12,2),
  CONSTRAINT PK PRIMARY KEY (year_month,account,control)) IN database;
INSERT INTO accrual_history
SELECT b.yearmonth, '05/17/2015','05/31/2015', a.account, a.control, a.amount
FROM accrualfileforjeri a
INNER JOIN day b on cast(a.date AS sql_date) = b.thedate;

-- excel formula to generate INSERT statement
=CONCATENATE("insert into accrual_history values(201504,'04/19/2015','04/30/2015','",C2,"','",D2,"',",G2,");")
=CONCATENATE("insert into accrual_history values(201506,'06/14/2015','06/30/2015','",C2,"','",D2,"',",G2,");")
=CONCATENATE("insert into accrual_history values(201507,'07/26/2015','07/31/2015','",C2,"','",D2,"',",G2,");")
=CONCATENATE("insert into accrual_history values(201508,'07/26/2015','07/31/2015','",C2,"','",D2,"',",G2,");")

-- script
DECLARE @year_month integer;
DECLARE @payroll_start date;
DECLARE @payroll_end date;
DECLARE @days integer;
@year_month = 201508;
@payroll_end = (
  SELECT MAX(thedate)
  FROM day
  WHERE yearmonth = @year_month);
@payroll_start = (
  SELECT MAX(thedate) - 180
  FROM day
  WHERE yearmonth = @year_month);  
@days = (
  SELECT (thru_date - from_date) + 1
  FROM (
    SELECT distinct thru_date, from_date
    FROM accrual_history
    WHERE year_month = @year_month) z);
  
SELECT @days FROM system.iota;  
/*
SELECT DISTINCT company_number, payroll_run_number
FROM (
  SELECT company_number, payroll_run_number,
    cast(createTimestamp(  
      CAST(LEFT(CAST(payroll_start_date AS sql_char),4) AS sql_integer),
      CAST(substring(CAST(payroll_start_date AS sql_char), 5, 2) AS sql_integer),
      CAST(substring(CAST(payroll_start_date AS sql_char), 7, 2) AS sql_integer),
      0,0,0,0) AS sql_date) AS payroll_start_date
  FROM ext_arkona_pyptbdta
  WHERE  
    cast(createTimestamp(  
      CAST(LEFT(CAST(payroll_start_date AS sql_char),4) AS sql_integer),
      CAST(substring(CAST(payroll_start_date AS sql_char), 5, 2) AS sql_integer),
      CAST(substring(CAST(payroll_start_date AS sql_char), 7, 2) AS sql_integer),
      0,0,0,0) AS sql_date)  BETWEEN @payroll_start AND @payroll_end) u
LEFT JOIN day v on u.payroll_start_date = v.biweeklypayperiodstartdate
WHERE v.thedate IS not NULL;
*/
SELECT x.*, y.daily_pay, x.daily_accrual - y.daily_pay
FROM (
  SELECT b.pydept, round(SUM(a.amount)/@days, 0) AS daily_accrual
  -- INTO #accrual
  FROM accrual_history a
  INNER JOIN edwEmployeeDim b on a.control = b.employeenumber
    AND b.currentrow = true
  --  AND b.storecode = 'ry1'
  WHERE year_month = @year_month
  GROUP BY b.pydept) x
INNER JOIN (
  SELECT pydept, SUM(daily_pay) AS daily_pay
  -- INTO #payroll
  FROM (
    SELECT e.*, f.name, f.pydept
    FROM (  
      SELECT b.yhdemp, cast(round(SUM(yhdtgp)/(COUNT(*) * 14), 0) as sql_integer)AS daily_pay
      FROM (
  SELECT DISTINCT company_number, payroll_run_number
  FROM (
    SELECT company_number, payroll_run_number,
      cast(createTimestamp(  
        CAST(LEFT(CAST(payroll_start_date AS sql_char),4) AS sql_integer),
        CAST(substring(CAST(payroll_start_date AS sql_char), 5, 2) AS sql_integer),
        CAST(substring(CAST(payroll_start_date AS sql_char), 7, 2) AS sql_integer),
        0,0,0,0) AS sql_date) AS payroll_start_date
    FROM ext_arkona_pyptbdta
    WHERE  
      cast(createTimestamp(  
        CAST(LEFT(CAST(payroll_start_date AS sql_char),4) AS sql_integer),
        CAST(substring(CAST(payroll_start_date AS sql_char), 5, 2) AS sql_integer),
        CAST(substring(CAST(payroll_start_date AS sql_char), 7, 2) AS sql_integer),
        0,0,0,0) AS sql_date)  BETWEEN @payroll_start AND @payroll_end) u
  LEFT JOIN day v on u.payroll_start_date = v.biweeklypayperiodstartdate
  WHERE v.thedate IS not NULL    ) a
      INNER JOIN stgArkonaPYHSHDTA b on a.payroll_run_number = b.ypbnum
--        AND b.yhdco# = 'ry1'
      INNER JOIN ( -- limit to employees in current accrual
        SELECT control
        FROM accrualfileforjeri
        GROUP BY control) c on b.yhdemp = c.control  
      GROUP BY yhdemp) e  
    LEFT JOIN edwEmployeeDim f on e.yhdemp = f.employeenumber
      AND f.currentrow = true) g 
  GROUP BY pydept) y on x.pydept = y.pydept;  


  
  
-- 9/1
-- compare months

-- DROP TABLE #july;
SELECT year_month, from_date, thru_date, control, SUM(amount) AS july_amount,
  round(SUM(amount)/((thru_date - from_date) + 1), 2) AS july_day,
  thru_date - from_date  + 1 AS days
INTO #july
FROM accrual_history 
WHERE year_month = 201507
GROUP BY year_month, from_date, thru_date, control;  
-- DROP TABLE #august;
SELECT year_month, from_date, thru_date, control, SUM(amount) AS august_amount,
  round(SUM(amount)/((thru_date - from_date) + 1), 2) AS aug_day,
  thru_date - from_date + 1 AS days
INTO #august
FROM accrual_history 
WHERE year_month = 201508
GROUP BY year_month, from_date, thru_date, control;   

SELECT c.*, d.name
FROM (
  SELECT a.*, b.*, aug_day - july_day AS diff
  FROM #july a
  full OUTER JOIN #august b on a.control = b.control) c
LEFT JOIN edwEmployeeDim d on 
  case 
    when c.control is not null then c.control = d.employeenumber
    ELSE c.control_1 = d.employeenumber
  END 
  AND d.currentrow = true  
ORDER BY name  
ORDER BY diff ; 

SELECT * FROM gmfs_Accounts

SELECT *
FROM accrualfileforjeri a
LEFT JOIN gmfs_accounts b on a.account = b.gl_account
WHERE control = '11687'