SELECT *
FROM edwEmployeeDim
WHERE lastname = 'rogne'

changed FROM hourly to salaried on 7/31
emp#: 1117960

select * -- 103.77 reg, 14.62 ot
FROM accrualClockHours
WHERE employeenumber = '1117960'

select * -- 0
FROM accrualCommissions WHERE employeenumber = '1117960' ORDER BY fromdate DESC 

SELECT * -- null
FROM AccrualCommissions2 WHERE employeenumber = '1117960'

SELECT * -- account 12104: 241731.16
FROM AccrualFileForJeri WHERE control = '1117960'

SELECT * -- TotalNonOTPay: 214,942
FROM AccrualHourly1 WHERE employeenumber = '1117960'

SELECT * -- ALL way too high, orders of magnitude
FROM AccrualHourly2 WHERE employeenumber = '1117960'

SELECT * -- null
FROM AccrualSalaried1 WHERE employeenumber = '1117960'

SELECT * -- null
FROM AccrualSalaried2 WHERE employeenumber = '1117960'

SELECT * -- null
FROM accrualTpAdjustment WHERE employeenumber = '1117960'

so, obviously, the problem IS IN accrualHourly1

-- here's the problem: hourlyRate = 1923.08
SELECT employeekey, name, distribution, LEFT(rowChangeReason, 25), employeekeyfromdate, 
  employeeKeyThruDate, payrollClass, Salary, HourlyRate
FROM edwEmployeeDim
WHERE employeenumber = '1117960'

reran the accrual script AND sent the rogne values to jeri