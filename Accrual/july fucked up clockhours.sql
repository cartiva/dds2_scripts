DECLARE @Date string; // last day of the month for which payroll IS being accrued;
DECLARE @FromDate date; // beginning date for the interval to be accrued
DECLARE @ThruDate date; // ending date for the interval to be accrued
DECLARE @NumDays integer;
DECLARE @DaysInMonth integer;
DECLARE @Journal string;
DECLARE @Doc string;
DECLARE @Ref string;
@Journal = 'GLI';
@Doc = 'GLI083113';
@Ref = @Doc;

@Date = '07/31/2013';
@FromDate = '07/14/2013';
@ThruDate = '07/31/2013';
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1;
@DaysInMonth = DayOfMonth(@Date);

SELECT a.*, b.regularhours, b.overtimehours, b.vacationhours, b.ptohours, b.holidayhours,
  p.DIST_CODE, p.SEQ_NUMBER, p.GROSS_DIST, p.GROSS_EXPENSE_ACT_, p.OVERTIME_ACT_, 
  p.VACATION_EXPENSE_ACT_, p.HOLIDAY_EXPENSE_ACT_, p.SICK_LEAVE_EXPENSE_ACT_,
  p.EMPLR_FICA_EXPENSE, p.EMPLR_MED_EXPENSE, p.EMPLR_CONTRIBUTIONS,
  pc.yficmp, pc.ymedmp, fe.FicaEx, me.MedEx, d.FIXED_DED_AMT 
INTO #wtf1
FROM ( -- a: 1 row per store/emp#  emp IS payed bi-weekly, dist code NOT IN list, 
       -- the empkey that was valid ON last payroll ending date
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
      PayPeriodCode, DistCode, salary, hourlyrate
  FROM edwEmployeeDim e
  WHERE hiredate <= @thruDate
    AND termdate >= @fromDate
    AND storecode <> 'ry3'  
    AND employeenumber NOT IN ('190915','1160100')
    AND PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV') 
    AND employeekey = (
      SELECT MAX(employeekey)
      FROM edwEmployeeDim 
      WHERE employeenumber = e.employeenumber
        AND storecode = e.storecode
        AND employeekeyfromdate <=  @thruDate
      GROUP BY storecode, employeenumber)) a     
LEFT JOIN ( -- clock hours for the interval BY empkey
  SELECT employeekey, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
    sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
    sum(holidayhours) AS holidayhours
  FROM day d
  INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
  WHERE thedate BETWEEN @fromDate AND @thruDate
  GROUP BY employeekey) b ON a.employeekey = b.employeekey   
LEFT JOIN stgArkonaPYACTGR p ON a.StoreCode = p.COMPANY_NUMBER -- distribution
  AND a.DistCode = p.DIST_CODE
  AND CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL pc ON a.storecode = pc.yco#
  AND pc.ycyy = 112 -- 100 + (year(@ThruDate) - 2000) 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) fe ON a.employeenumber = fe.EMPLOYEE_NUMBER  
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) me ON a.employeenumber = me.EMPLOYEE_NUMBER    
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT d ON a.storecode = d.COMPANY_NUMBER
  AND a.employeenumber = d.EMPLOYEE_NUMBER 
  AND d.DED_PAY_CODE IN ('91', '99') 
WHERE b.employeekey IS NOT NULL;  


SELECT * FROM #wtf1 WHERE employeenumber IN  ('1117960','179750','116185','1149500','164015','131370','161120','167580','1133500')

SELECT * FROM #wtf1 WHERE employeenumber IN (
  SELECT control
  FROM zaccrual
  WHERE account = '124704'
  AND date = '07/31/2013')

looks LIKE clockhours are fucked up
SELECT employeekey, employeenumber, name, employeekeyfromdate, employeekeythrudate, rowchangereason 
FROM edwEmployeeDim WHERE employeenumber IN  ('1117960','179750','116185','1149500','164015','131370','161120','167580','1133500')
ORDER BY employeenumber, employeekey

the problem IS for every tech that got a RAISE toward the END of july

AND IN the generation of clockhours (#wtf1) i stupidly DO this:

  FROM edwEmployeeDim e
  WHERE hiredate <= @thruDate
    AND termdate >= @fromDate
    AND storecode <> 'ry3'  
    AND employeenumber NOT IN ('190915','1160100')
    AND PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV') 
    AND employeekey = ( 
      SELECT MAX(employeekey)  -- this IS so wrong, what it did was eliminate the emp key for 7/14 -> 7/29
      FROM edwEmployeeDim 
      WHERE employeenumber = e.employeenumber
        AND storecode = e.storecode
        AND employeekeyfromdate <=  @thruDate
      GROUP BY storecode, employeenumber)) a   
      
-- 9/4/13     

so once again the core issue IS understanding the period of activity AND the associated employeekeys
need to accomodate multiple employeekeys for the period AS well AS excluding non active employees
need any AND ALL employeekeys that had clockhours for the desired period
hmmm, use the clock AS the basis for selecting employeekeys for hourly
still need a way to get the relevant keys for salaried (#wtf2)

use this AS the base TABLE for #wtf1
this gives me multiple rows per employeenumber WHEN there are multiple employeekeys for the period
at what point IN the process DO i GROUP to generate an emp# row?
what values are employeekey dependent,
what values are employeenumber dependent

SELECT *
FROM ( -- use this AS the base TABLE for #wtf1
  SELECT employeekey, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
    sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
    sum(holidayhours) AS holidayhours
  FROM day d
  INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
  WHERE thedate BETWEEN '07/14/2013' AND '07/31/2013'
  GROUP BY employeekey) a
INNER JOIN (
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
      PayPeriodCode, DistCode, salary, hourlyrate
  FROM edwEmployeeDim e
-- may NOT even need this, IF the fucking employeekey EXISTS IN the clock, it needs to 
-- be included  
-- this simply becomes a way of excluding certain folks based on employeenumber OR distcode
  WHERE hiredate <= '07/31/2013'
    AND termdate >= '07/14/2013'
    AND storecode <> 'ry3'  
    AND employeenumber NOT IN ('190915','1160100')
    AND PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')) b on a.employeekey = b.employeekey 
ORDER BY name     