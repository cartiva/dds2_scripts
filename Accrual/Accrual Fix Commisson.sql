WHERE i want to go IS a nightly accrual job
so i''m thinking i need to manifest the #temp tables
but first i have to fix commission accrual
AND THEN out of trying to build a comparison BETWEEN accrual AND actual
figure out what the tables look LIKE
paycheck fact 
empkey
ppstartdate
ppenddate
payrolldate
batch - degenerate dim?
generates:
      
so it''s going to be LIKE a payroll fact sufficient to check the values IN accrual
AS a start   
need FICA, MEDIC, RETIRE
so it will be based ON payperiod ?
any paycheck issued within a payperiod    

store accruals for entire payperiods
THEN the actual fucking check! 

ok, the final output FROM THE accrual query IS jeri''s file
but for comparison, i would rather have
1 row per emp# - what about multiple accounts
roll it up for now
so, for payperiod END date
1 row per emp# with, commission, gross, ot,  pto, vac, hol, fica, medic, retire
so change the UNION INSERT INTO #jonX
to SELECT FROM #wtf

what''s missing FROM #wtfX IS dates
regenerate with payperiods
SELECT storecode
FROM #wtf1
       
DROP TABLE #wtf1;
DROP TABLE #jon1;  

DROP TABLE #wtf2;
DROP TABLE #jon2; 


DROP TABLE #entries; 


SELECT * FROM #wtf2

don''t go off the fucking deep END
just fix the problem
AND the problem IS
commissions are accrued for the entire amount
(comm/days IN month) * @NumDays

the split takes place IN #jonX

  SELECT 'Gross', employeenumber, ytagpa AS Account,
    ytagpp/100.0 *
      CASE PayrollClassCode
        WHEN 'H' THEN (regularhours * HourlyRate)
        WHEN 'S' THEN salary/14 * @NumDays
      END AS Amount
  FROM #wtf1

DECLARE @Date string; // last day of the month for which payroll IS being accrued;
DECLARE @FromDate date; // beginning date for the interval to be accrued
DECLARE @ThruDate date; // ending date for the interval to be accrued
DECLARE @NumDays integer;
DECLARE @Journal string;
DECLARE @Doc string;
DECLARE @Ref string;
DECLARE @DaysInMonth integer;
@Journal = 'GLI';
@Doc = 'GLI073112';
@Ref = @Doc;
@Date = '07/31/2012';
@FromDate = '07/15/2012';
@ThruDate = '07/31/2012';
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1;
@DaysInMonth = DayOfMonth(@Date);
/*
--FROM (
  SELECT 'CorrComm', employeenumber, ytagpa AS Account,
    ytagpp/100.0 * (Comm/@DaysinMonth) * @NumDays AS Amount
  FROM #wtf2
  UNION ALL
  SELECT 'Comm', employeenumber, ytagpa AS Account,
    ytagpp/100.0 * Comm AS Amount
  FROM #wtf2

*/

SELECT employeenumber, name, round(sum(ytagpp/100.0 * (Comm/@DaysinMonth) * @NumDays), 2) AS CorrCom,
  round(sum(ytagpp/100.0 * Comm), 2) AS oldCom,
  round(sum(comm), 2) AS fullcomm
FROM #wtf2
GROUP BY employeenumber, name

SELECT * 
FROM #jon2 a
LEFT JOIN zaccrual b ON cast(a.[date] AS sql_date) = b.[date] 
  AND a.control = b.control 
  AND a.account = b.account
  
  
SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate 
FROM day 
WHERE thedate BETWEEN '12/31/2011' AND curdate()
GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate 

SELECT storecode, employeenumber, name, sum(regularhours) AS regHours, 
  sum(overtimehours) AS otHours, sum(vacationhours) AS vacHours, 
  sum(ptohours) AS ptoHours, sum(holidayhours) AS holidayhours
  
-- 110085/341 basnet  
-- this looks good for one employeenumber/ payperiod
-- dds usage patter: relevant type 2 row for a date
-- IN this CASE, an empkey for each biweeklypayperiodenddate
-- now, ADD the other shit
-- oops, clockhours
-- 1 MIN 7 sec
-- this much IS good day, edwEmployeeDim: 1 row per payperiod/empl# 
--SELECT biweeklypayperiodenddate, storecode, employeenumber FROM (
SELECT a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, b.storecode, 
  b.name, b.employeenumber, b.employeekey
-- INTO #wtf  
--SELECT COUNT(*)   
FROM (
  SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate 
  FROM day 
  WHERE thedate BETWEEN '12/31/2011' AND curdate()
  GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate) a
LEFT JOIN ( -- the relevant empkey for the payperiod
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
    PayPeriodCode, DistCode, salary, hourlyrate,EmployeeKeyFromDate, EmployeeKeyThruDate
  FROM edwEmployeeDim
  WHERE PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', 
      '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')) b ON b.EmployeeKeyFromDate = (
        SELECT MAX(employeekeyFromDate)
        FROM edwEmployeeDim 
        WHERE employeenumber = b.employeenumber
          AND EmployeeKeyFromDate < a.biweeklypayperiodenddate
        GROUP BY employeenumber) 
--) z GROUP BY biweeklypayperiodenddate, storecode, employeenumber HAVING COUNT(*) > 1

-- ADD clockhours
-- might be ok
SELECT a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, b.storecode, 
  b.name, b.employeenumber, b.employeekey
FROM (
  SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate 
  FROM day 
  WHERE thedate BETWEEN '12/31/2011' AND curdate()
  GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate) a
LEFT JOIN ( -- the relevant empkey for the payperiod
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
    PayPeriodCode, DistCode, salary, hourlyrate,EmployeeKeyFromDate, EmployeeKeyThruDate
  FROM edwEmployeeDim
  WHERE PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', 
      '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')) b ON b.EmployeeKeyFromDate = (
        SELECT MAX(employeekeyFromDate)
        FROM edwEmployeeDim 
        WHERE employeenumber = b.employeenumber
          AND EmployeeKeyFromDate < a.biweeklypayperiodenddate
        GROUP BY employeenumber) 
LEFT JOIN (
  select c.thedate, b.*
  from edwClockHoursFact b 
  LEFT JOIN day c ON b.datekey = c.datekey) d ON b.employeekey = d.employeekey
    AND d.thedate BETWEEN a.biweeklypayperiodstartdate AND a.biweeklypayperiodenddate 
 
-- looks ok 
--SELECT biweeklypayperiodenddate, storecode, employeenumber FROM #wtf GROUP BY biweeklypayperiodenddate, storecode, employeenumber HAVING COUNT(*) > 1     
SELECT a.biweeklypayperiodenddate, b.storecode, 
  b.name, b.employeenumber, SUM(coalesce(ClockHours, 0)) AS ClockHours,
    sum(coalesce(RegularHours, 0)) AS RegularHours, sum(coalesce(OvertimeHours, 0)) AS OvertimeHours, 
    sum(coalesce(VacationHours, 0)) AS VacationHours,
    sum(coalesce(PTOHours, 0)) AS PTOHours, sum(coalesce(HolidayHours, 0)) AS HolidayHours
INTO #wtf    
FROM (
  SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate 
  FROM day 
  WHERE thedate BETWEEN '12/31/2011' AND curdate()
  GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate) a
LEFT JOIN ( -- the relevant empkey for the payperiod
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
    PayPeriodCode, DistCode, salary, hourlyrate,EmployeeKeyFromDate, EmployeeKeyThruDate
  FROM edwEmployeeDim
  WHERE PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', 
      '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')) b ON b.EmployeeKeyFromDate = (
        SELECT MAX(employeekeyFromDate)
        FROM edwEmployeeDim 
        WHERE employeenumber = b.employeenumber
          AND EmployeeKeyFromDate < a.biweeklypayperiodenddate
        GROUP BY employeenumber) 
LEFT JOIN ( -- clockhours
  select c.thedate, b.employeekey, b.datekey, coalesce(ClockHours, 0 ) AS ClockHours,
    coalesce(RegularHours, 0 ) AS RegularHours, coalesce(OvertimeHours, 0 ) AS OvertimeHours, 
    coalesce(VacationHours, 0 ) AS VacationHours,
    coalesce(PTOHours, 0 ) AS PTOHours, coalesce(HolidayHours, 0 ) AS HolidayHours
  from edwClockHoursFact b 
  LEFT JOIN day c ON b.datekey = c.datekey) d ON b.employeekey = d.employeekey
    AND d.thedate BETWEEN a.biweeklypayperiodstartdate AND a.biweeklypayperiodenddate  
GROUP BY a.biweeklypayperiodenddate, b.storecode, 
  b.name, b.employeenumber    
  
SELECT * FROM #wtf a  
   


-- 8/31/12
fuck i think i confuse myself ON the 2 deliverables here, one IS a paychech fact, 
the other IS accrual query to compare to paycheck

SELECT * FROM #wtf1
SELECT * FROM #jon1

  
-- don't think i need distribution, this IS NOT about GL but rather simulating a paycheck
--LEFT JOIN stgArkonaPYACTGR p ON a.StoreCode = p.ytaco# -- distribution
--  AND a.DistCode = p.ytadic
--  AND CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL pc ON a.storecode = pc.yco#
  AND pc.ycyy = 112 -- 100 + (year(@ThruDate) - 2000) 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT ydempn, SUM(yddamt) AS FicaEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex2 = 'Y')
  GROUP BY ydempn) fe ON a.employeenumber = fe.ydempn  
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT ydempn, SUM(yddamt) AS MedEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex6 = 'Y')
  GROUP BY ydempn) me ON a.employeenumber = me.ydempn    
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT d ON a.storecode = d.ydco#
  AND a.employeenumber = d.ydempn 
  AND d.yddcde IN ('91', '99') 
WHERE b.storecode IS NOT NULL;   


SELECT 100 + (year('12/10/2010') - 2000) FROM system.iota

-- 9/1 focus ON the paycheck fact
employeekey
employeenumber
batch DegDim
check# degdim
period
dates:
  check date
  period ending date
check
amounts:
  YHDBSP	:	   BASE PAY
  YHDTGP	:	   TOTAL GROSS PAY
  YHDTGA	:	2  TOTAL ADJ GROSS
  YHCFED	:	3  CURR FEDERAL TAX
  YHCSSE	:	4  CURR FICA
  YHCSSM	:	5  CURR EMPLR FICA
  YHCMDE	:	6  CURR EMPLOYEE MEDICARE
  YHCMDM	:	7  CURR EMPLOYER MEDICARE
  YHCEIC	:	8  YTD  EIC PAYMENTS
  YHCST	  :	9  CURR STATE TAX
  YHCSDE	:	10 CURR EMPLOYEE SDI
  YHCSDM	:	11 CURR EMPLOYER SDI
  YHCCNT	:	12 CURR COUNTY TAX
  YHCCTY	:	13 CURR CITY TAX
  YHCFUC	:	14 CURR FUTA
  YHCSUC	:	15 CURR SUTA
  YHCCMP	:	16 WORKMANS COMP
  YHCRTE	:	17 EMPLY CURR RET
  YHCRTM	:	18 EMPLR CURR RET
  YHDOTA	:	19 OVERTIME AMOUNT
  YHDDED	:	20 TOTAL DEDUCTION
  YHCOPY	:	21 TOTAL OTHER PAY
  YHCNET	:	22 CURRENT NET
  YHCTAX	:	23 CURRENT TAX TOT
  YHAFED	:	1  CURR ADJ FED
  YHASS	  :	2  CURR ADJ SS
  YHAFUC	:	3  CURR ADJ FUTA
  YHAST	  :	4  CURR ADJ STATE
  YHACN	  :	5  CURR ADJ CNTY
  YHAMU	  :	6  CURR ADJ CITY
  YHASUC	:	7  CURR ADJ SUTA
  YHACM	  :	8  CURR ADJ COMP
  YHDVAC	:	1  VACATION TAKEN
  YHDHOL	:	2  HOLIDAY TAKEN
  YHDSCK	:	3  SICK LEAVE TAKEN
  YHAVAC	:	4  VACATION ACC
  YHAHOL	:	5  HOLIDAY ACC
  YHASCK	:	6  SICK LEAVE ACC
  YHDHRS	:	7  REG HOURS
  YHDOTH	:	8  OVERTIME HOURS
  YHDAHR	:	9  ALT PAY  HOURS
  YHVACD	:	10 DEF VAC HRS
  YHHOLD	:	11 DEF HOL HRS
  YHSCKD	:	12 DEF SCK HRS
  YHHDTE	:	HIRE DATE
  YHFDEX	:	FEDERAL EXEMPTIONS
  YHFDAD	:	FEDERAL ADD ON AMOUNT
  YHSTEX	:	STATE EXEMPTIONS
  YHSTFX	:	STATE FIXED EXEMPTIONS
  YHSTAD	:	STATE TAX ADD ON AMOUNT
  YHWKCD	:	WKMAN COMP CODE
  YHSALY	:	BASE SALARY
  YHRATE	:	BASE HRLY RATE
  YHSALA	:	ALT  SALARY
  YHRATA	:	ALT  HRLY RATE
  YHRETP	:	RETIREMENT %
  YHRFIX	:	RETIREMENT FIXED AMOUNT
  YMDVYR	:	DEFERRED VAC YEAR
  YMDHYR	:	DEFERRED HOL YEAR
  YMDSYR	:	DEFFERED SIC YEAR
  YMSECG	:	SECURITY GROUP
  YMSS#	  :	SOC SEC NUMBER
  YHVCYY	:	PAYROLL CEN + YEAR
  YHVNUM	:	PAYROLL RUN NUMBER
  YHACTY	:	Adjusted city wages
  YHASDIW	:	Adjusted SDI wages
  YHAOTR	:	Adjusted other wages
  YHFTAP	:	Federal additional tax %
  YHSTAP	:	State additional tax %
  YHCSUE	:	Curr Emplee SUTA
  YHCST2	:	Curr 2nd State Whld
  YHCCN2	:	Curr 2nd Cnty Whld
  YHCCT2	:	Curr 2nd City Whld
  YHTMP08	:	NOTUSED 8
  YHTMP09	:	NOTUSED 9
    
    
SELECT LEFT(name, 7)+ ' : ', field_num
FROM system.columns
WHERE parent = 'stgArkonaPYHSHDTA'    
  AND field_type = 18
ORDER BY field_num    
    


select * from 
stgArkonapyhshdta where ypbcyy = 112 and trim(yhdemp) = '179750'
and ypbnum = 831000 


the only real issue IS picking the correct employeekey
how to verify
employeenumber with multiple empkeys during a payperiod with different salaries
the question IS, what IS the relevant pay date
also, NOT ALL checks are issued 
-- 9/2 TRY to find an example
SELECT *
FROM (
  SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate 
  FROM day 
  WHERE thedate BETWEEN '07/31/2009' AND curdate()
  GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate) a
LEFT JOIN ( -- the relevant empkey for the payperiod
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
    PayPeriodCode, DistCode, salary, hourlyrate,EmployeeKeyFromDate, EmployeeKeyThruDate
  FROM edwEmployeeDim
  WHERE PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', 
      '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')) b ON b.EmployeeKeyFromDate = (
        SELECT MAX(employeekeyFromDate)
        FROM edwEmployeeDim 
        WHERE employeenumber = b.employeenumber
          AND EmployeeKeyFromDate < a.biweeklypayperiodenddate
        GROUP BY employeenumber) 
-- this IS CLOSE, but NOT quite it 
-- it's showing the prechange hourlyrate       
SELECT a.name, a.employeenumber, a.employeekey, a.rowchangedate, left(a.rowchangereason, 20), 
  a.hourlyrate, b.biweeklypayperiodstartdate, b.biweeklypayperiodenddate, c.yhdemp, c.yhsaly, c.yhrate
FROM edwEmployeeDim a
LEFT JOIN (
  SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate 
  FROM day 
  WHERE thedate BETWEEN '07/31/2009' AND curdate()
  GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate) b 
    ON a.rowchangedate > b.biweeklypayperiodstartdate AND a.rowchangedate < b.biweeklypayperiodenddate   
LEFT JOIN stgArkonaPYHSHDTA c ON a.employeenumber = c.yhdemp
  AND year(b.biweeklypayperiodenddate) - 2000 = yhdeyy
  AND month(b.biweeklypayperiodenddate) = yhdemm 
  AND dayofmonth(b.biweeklypayperiodenddate) = yhdedd 
WHERE b.biweeklypayperiodstartdate IS NOT NULL 
  AND a.rowchangereason = 'Salary HourlyRate'
  
  
-- 9/3

SELECT * FROM stgArkonaPYHSHDTA WHERE yhdemp = '171210'

SELECT * from stgarkonapyhshdta WHERE yhvoid = 'V'