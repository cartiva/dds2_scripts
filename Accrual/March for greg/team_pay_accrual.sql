DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '03/22/2015';
@thruDate = '03/31/2015';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);
-- SELECT @days FROM system.iota;        
INSERT INTO accrualTpAdjustment  
SELECT employeenumber, (@days * commissionPayPerDay) + holPay AS commissionPay, @thruDate
FROM (
  select lastname, firstname, employeenumber,  
    round(max(techtfrrate*teamprofpptd*techclockhourspptd/100)/10, 2) AS commissionPayPerDay,
    max(techhourlyrate * (techvacationhourspptd + techptohourspptd + techholidayhourspptd)) AS holPay
  FROM scotest.tpdata a
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 45 AND curdate()
  GROUP BY lastname, firstname, employeenumber  
  UNION -- brian peterson
  select c.lastname, c.firstname, a.employeenumber, 
    round(max(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPayPerDay,
    0 AS holPay
  FROM scotest.bsFlatRateData a
  LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.fromdate AND b.thrudate
  LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 45 AND curdate()
  GROUP BY c.lastname, c.firstname, a.employeenumber ) e; 