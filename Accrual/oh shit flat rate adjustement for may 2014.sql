#wtf: main shop payroll for kim
#wtf1: body shop payroll for kim

-- needed to GROUP AND SUM to get both ines for brian peterson
SELECT employeenumber, name, SUM(totalgross) AS totalgross
INTO #wtf2
FROM (
  SELECT [employee number] AS employeenumber, name, [total gross pay] AS totalgross
  FROM #wtf
  UNION
  SELECT [employee number] AS employeenumber, name, [total gross pay] AS totalgross
  FROM #wtf1) x
GROUP BY employeenumber, name  
ORDER BY employeenumber

-- include only the gross pay accounts
SELECT * 
FROM accrualfileforjeri a
LEFT JOIN #wtf2 b on a.control = b.employeenumber
WHERE account IN ('124703','124704') 
  AND b.employeenumber IS NOT NULL
ORDER BY a.control

-- the difference
SELECT control, account, round(totalgross - amount, 2) AS adjustment
FROM accrualfileforjeri a
LEFT JOIN #wtf2 b on a.control = b.employeenumber
WHERE account IN ('124703','124704') 
  AND b.employeenumber IS NOT NULL
ORDER BY a.control