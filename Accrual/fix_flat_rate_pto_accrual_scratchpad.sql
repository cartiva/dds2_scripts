/*
wtf for employees with pto, i would think that this total would be higher? NOT necessarily
*/

DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromdate = '12/25/2016';
@thrudate = '12/31/2016';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);
-- detail

//INSERT INTO #a_f_r_g 
SELECT 'RY1', employeenumber, lastname, firstname, @fromdate, @thrudate,  
  coalesce(round((12 * flaghours) + ((rate - 12) * flaghours), 2), 0), 'WTEC',
  coalesce(round(12 * ptohours, 2), 0)  AS ptopay
FROM (
  SELECT m.*, n.flaghours, round(flaghours/clockhours, 2) AS prof,
  -- hard code the prof matrix for now
    CASE
-- *a*  
      WHEN lastname IN ('Telken','Kingbird','Roehrich'/*,'Gonzalez'*/) THEN 12.0
      WHEN lastname = 'Nelson' AND firstname = 'Hunter' THEN 12.0   
      WHEN lastname = 'Horton' AND firstname = 'Eric' THEN 12.0
      WHEN lastname = 'Nelson' AND firstname = 'Kordell' THEN 12.0
      WHEN round(flaghours/clockhours, 2) <= 1.3 THEN 12.0   
      WHEN round(flaghours/clockhours, 2) >= 1.3 AND round(flaghours/clockhours, 2) < 1.4 THEN 12.5
      WHEN round(flaghours/clockhours, 2) >= 1.4 AND round(flaghours/clockhours, 2) < 1.5 THEN 13.5
      WHEN round(flaghours/clockhours, 2) >= 1.5 AND round(flaghours/clockhours, 2) < 1.6 THEN 15
      WHEN round(flaghours/clockhours, 2) >= 1.6 AND round(flaghours/clockhours, 2) < 1.7 THEN 16
      WHEN round(flaghours/clockhours, 2) >= 1.7 AND round(flaghours/clockhours, 2) < 1.8 THEN 17
      WHEN round(flaghours/clockhours, 2) >= 1.8 AND round(flaghours/clockhours, 2) < 1.9 THEN 18
      WHEN round(flaghours/clockhours, 2) >= 1.9 AND round(flaghours/clockhours, 2) < 2 THEN 19
      WHEN round(flaghours/clockhours, 2) > 2 THEN 20.0  
    END AS rate
  FROM (
    SELECT a.firstname, a.lastname, a.employeenumber, a.employeekey, 
      SUM(b.clockhours) AS clockhours, SUM(holidayhours+ptohours+vacationhours) AS ptohours,
      c.technumber, c.techkey
    FROM edwEmployeeDim a 
    LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
      AND b.datekey IN (
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN @fromdate AND @thrudate)
    LEFT JOIN dimtech c on a.employeenumber = c.employeenumber
-- *e*
      AND c.active = true    
      AND c.currentrow = true    
    WHERE a.distcode = 'wtec'
  --    AND a.currentrow = true
      AND a.active = 'active'
      AND a.employeekeyfromdate <= @thrudate
      AND a.employeekeythrudate > @fromdate
      AND a.payrollclass = 'commission'
    GROUP BY a.firstname, a.lastname, a.employeenumber, a.employeekey, c.technumber, c.techkey) m   
  LEFT JOIN (
    SELECT d.technumber, d.description, d.employeenumber, SUM(flaghours) AS flaghours
    FROM factrepairorder a
    INNER JOIN day b on a.closedatekey = b.datekey
    INNER JOIN dimtech d on a.techkey = d.techkey
    WHERE b.thedate between @fromdate AND @thrudate
      AND d.flagdeptcode = 're'
      AND d.employeenumber <> 'NA'
    GROUP BY d.technumber, d.description, d.employeenumber) n on m.employeenumber = n.employeenumber  ) x
	
ORDER BY lastname	
