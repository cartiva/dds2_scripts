/*
    a combination of "Accrual - One Query to Rule Them All.sql" AND
    "accrual = january 2015 - one time fix.sql"
    "AccrualFlatRateGross.sql"
*/

/* 
1. 
zFixPYACTGR
   extract FROM db2 AND repopulate zFixPYACTGR
2.   
determine accrual period
per jeri: 11/13/2016 -> 11/30/2016
*/
2a. DO detail spreadsheet for aubrey (detail accrual.sql)
/*
****
  SELECT max(biweeklypayperiodenddate) from day where biweeklypayperiodenddate < curdate() 
  
  SELECT top 2 biweeklypayperiodenddate from (
    select biweeklypayperiodenddate
	from day
	group by biweeklypayperiodenddate) a where biweeklypayperiodenddate < curdate() ORDER BY biweeklypayperiodenddate DESC
  
3.
generate base rows for Accrual Commissions
(this actually should be reworked, generates way too may rows - doesn't really 
  hurt anything, but beaucoup superfluous data)    
 
****
  DECLARE @fromdate string;
  DECLARE @thrudate string;  
  @fromdate = '08/21/2016'; -- last payroll enddate + 1
  @thrudate = '08/31/2016'; -- EOM
  INSERT INTO AccrualCommissions
  SELECT a.storecode, a.employeenumber, a.employeekey, a.name,
    CAST(@fromdate AS sql_Date) AS FromDate, CAST(@thrudate AS sql_date) AS ThruDate, 0 AS Amount
  FROM edwEmployeeDim a
  LEFT JOIN ( -- clock hours for the interval
      SELECT storecode, employeenumber, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
        sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
        sum(holidayhours) AS holidayhours
      FROM day d
      INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
      LEFT JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
      WHERE thedate BETWEEN CAST(@fromdate AS sql_date) AND CAST(@thrudate AS sql_date)
      GROUP BY storecode, employeenumber) b ON a.storecode = b.storecode 
    AND a.employeenumber = b.employeenumber 
  WHERE EmployeeKeyFromDate = (
      SELECT MAX(EmployeekeyFromDate)
      FROM edwEmployeeDim
      WHERE EmployeeNumber = a.Employeenumber
        AND EmployeeKeyFromDate <= CAST(@thrudate AS sql_date)
      GROUP BY employeenumber)
    AND PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV') 
    AND b.storecode IS NOT NULL;
    
-- 6/1/16 this IS better but no done yet
-- 8/1/16 adding active filtered out the superfluous adam lindquist ry1 row
DECLARE @fromdate date;
DECLARE @thrudate date;  
@fromdate = '06/25/2017'; -- last payroll enddate + 1
@thrudate = '06/30/2017'; -- EOM
INSERT INTO AccrualCommissions
SELECT storecode, employeenumber, employeekey, name,
  @fromdate AS FromDate, 
  @thrudate AS ThruDate, 0 AS Amount
FROM edwEmployeeDim 
WHERE currentrow = true
  AND payperiodcode = 'B'
  AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', 
    '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV') 
  AND termdate > @thrudate
  AND active = 'active';

 -- this gets the hourly folks IN digital that have distcode = SALE
INSERT INTO accrualcommissions     
SELECT storecode, employeenumber, employeekey, name,
  @fromdate AS FromDate, 
  @thrudate AS ThruDate, 0 AS Amount
FROM edwEmployeeDim 
WHERE currentrow = true
  AND payperiodcode = 'B'
  AND DistCode ='SALE'
  AND payrollclass = 'Hourly'
  AND termdate > @thrudate
  AND active = 'active';
*/  
  

4.
manually UPDATE AccrualCommisions with info FROM jeri
include the body shop estimator info
python projects\body_shop_pay\sql\payroll.sql	 	 

5.
AccrualFlatRateGross.sql

6.
accrual - january 2015 - one time fix.sql
