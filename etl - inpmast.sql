
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'double,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'money,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'INPMAST' 
  AND column_name IN (
    'imco#', 'imvin', 'imstk#', 'imdoc#', 'imstat', 
    'imgtrn', 'imtype', 'imfran', 'imyear', 'immake', 
    'immcode', 'immodl', 'imbody', 'imcolr', 'imodom', 
    'imdinv', 'imdsvc', 'imddlv', 'imdord', 'imsact', 
    'imiact', 'imlic#', 'imkey', 'imodoma', 'imkey')
ORDER BY ordinal_position



         

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text, ordinal_position
from syscolumns
WHERE table_name = 'INPMAST' 
  AND column_name IN (
    'imco#', 'imvin', 'imstk#', 'imdoc#', 'imstat', 
    'imgtrn', 'imtype', 'imfran', 'imyear', 'immake', 
    'immcode', 'immodl', 'imbody', 'imcolr', 'imodom', 
    'imdinv', 'imdsvc', 'imddlv', 'imdord', 'imsact', 
    'imiact', 'imlic#', 'imkey', 'imodoma', 'imkey')
ORDER BY ordinal_position

-- non char field list for spreadsheet
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'INPMAST'  
  AND data_type <> 'CHAR'
  AND column_name IN (
    'imco#', 'imvin', 'imstk#', 'imdoc#', 'imstat', 
    'imgtrn', 'imtype', 'imfran', 'imyear', 'immake', 
    'immcode', 'immodl', 'imbody', 'imcolr', 'imodom', 
    'imdinv', 'imdsvc', 'imddlv', 'imdord', 'imsact', 
    'imiact', 'imlic#', 'imkey', 'imodoma', 'imkey')
ORDER BY ordinal_position




      
DROP TABLE #INPMAST

SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text, ordinal_position
INTO #INPMAST 
from syscolumns
WHERE table_name = 'INPMAST'  
  AND column_name IN (
    'imco#', 'imvin', 'imstk#', 'imdoc#', 'imstat', 
    'imgtrn', 'imtype', 'imfran', 'imyear', 'immake', 
    'immcode', 'immodl', 'imbody', 'imcolr', 'imodom', 
    'imdinv', 'imdsvc', 'imddlv', 'imdord', 'imsact', 
    'imiact', 'imlic#', 'imkey', 'imodoma', 'imkey')
  
SELECT * FROM #INPMAST

-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
@i = (
  SELECT MAX(ordinal_position)
  FROM #INPMAST);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = coalesce((SELECT '[' + TRIM(column_name) + '], '  FROM #pymast WHERE ordinal_position = @j), '');
-- just COLUMN names
--  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM #INPMAST WHERE ordinal_position = @j), '');
-- parameter names  
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM #INPMAST WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaINPMAST';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;


CREATE TABLE stgArkonaINPMAST (
imco#      cichar(3),          
imvin      cichar(17),         
imstk#     cichar(9),          
imdoc#     cichar(9),          
imstat     cichar(1),          
imgtrn     cichar(1),          
imtype     cichar(1),          
imfran     cichar(3),          
imyear     integer,             
immake     cichar(25),         
immcode    cichar(10),         
immodl     cichar(25),         
imbody     cichar(25),         
imcolr     cichar(25),         
imodom     integer,              
imdinv     date,              
imdsvc     date,              
imddlv     date,              
imdord     date,              
imsact     cichar(10),         
imiact     cichar(10),         
imlic#     cichar(10),         
imodoma    cichar(1),          
imkey      integer) IN database;    

-- 9/28/12 added imcost

