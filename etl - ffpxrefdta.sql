SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'integer,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'double,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'ffpxrefdta' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'ffpxrefdta'  
AND data_type <> 'CHAR'

-- non char fields for spreadsheet
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'ffpxrefdta'  
AND data_type <> 'CHAR'

CREATE TABLE stgArkonaFFPXREFDTA (
fxco#      cichar(3),          
fxconsol   cichar(1),          
fxglco#    cichar(3),          
fxcode     cichar(3),          
fxcyy      integer,            
fxgact     cichar(10),         
fxfact     cichar(10),         
fxfac2     cichar(10),         
fxfac3     cichar(10),         
fxfac4     cichar(10),         
fxfac5     cichar(10),         
fxfac6     cichar(10),         
fxfac7     cichar(10),         
fxfac8     cichar(10),         
fxfac9     cichar(10),         
fxfa10     cichar(10),         
fxfp01     double,             
fxfp02     double,             
fxfp03     double,             
fxfp04     double,             
fxfp05     double,             
fxfp06     double,             
fxfp07     double,             
fxfp08     double,             
fxfp09     double,             
fxfp10     double) IN database;            

-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'FFPXREFDTA';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = coalesce((SELECT '[' + TRIM(column_name) + '], '  FROM #pymast WHERE ordinal_position = @j), '');
-- just COLUMN names
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
-- parameter names  
--  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM #pymast WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'FFPXREFDTA';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaFFPXREFDTA';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
 
