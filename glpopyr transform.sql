alter PROCEDURE OpenAccountingMonthYearByCompany(
  Company CIChar(3),
  Year integer output,
  month integer output)
BEGIN 

/*
EXECUTE PROCEDURE OpenAccountingMonthYearByCompany('RY1');

SELECT * FROM (EXECUTE PROCEDURE OpenAccountingMonthYearByCompany('RY1')) x
*/ 

DECLARE @Company string;
@Company = (SELECT Company FROM __input);
INSERT INTO __output
SELECT goyear AS OpenYear,
  CAST (
    CASE OpenMonth
	  WHEN 'Jan' THEN 1
	  WHEN 'Feb' THEN 2
	  WHEN 'Mar' THEN 3
	  WHEN 'Apr' THEN 4
	  WHEN 'May' THEN 5
	  WHEN 'Jun' THEN 6
	  WHEN 'Jul' THEN 7
	  WHEN 'Aug' THEN 8
	  WHEN 'Sep' THEN 9
	  WHEN 'Oct' THEN 10
	  WHEN 'Nov' THEN 11
	  WHEN 'Dec' THEN 12 
	END AS sql_integer) AS OpenMonth
FROM (
  SELECT goyear, 
    CASE when gomn01 = 'Y' THEN 'Jan' ELSE 'N' END AS OpenMonth
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x
  UNION ALL 
  SELECT goyear, 
    CASE when gomn02 = 'Y' THEN 'Feb' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x  
  UNION ALL 
  SELECT goyear, 
    CASE when gomn03 = 'Y' THEN 'Mar' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x
  UNION ALL 
  SELECT goyear, 
    CASE when gomn04 = 'Y' THEN 'Apr' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x
  UNION ALL 
  SELECT goyear, 
    CASE when gomn05 = 'Y' THEN 'May' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x
  UNION ALL 
  SELECT goyear, 
    CASE when gomn06 = 'Y' THEN 'Jun' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x
  UNION ALL 
  SELECT goyear, 
    CASE when gomn07 = 'Y' THEN 'Jul' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x
  UNION ALL 
  SELECT goyear, 
    CASE when gomn08 = 'Y' THEN 'Aug' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x
  UNION ALL 
  SELECT goyear, 
    CASE when gomn09 = 'Y' THEN 'Sep' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x
  UNION ALL 
  SELECT goyear, 
    CASE when gomn10 = 'Y' THEN 'Oct' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x
  UNION ALL 
  SELECT goyear, 
    CASE when gomn11 = 'Y' THEN 'Nov' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x
  UNION ALL 
  SELECT goyear, 
    CASE when gomn12 = 'Y' THEN 'Dec' ELSE 'N' END 
  FROM (  
    SELECT *
    FROM stgarkonaglpopyr
    WHERE goco# = @Company) x) y
WHERE OpenMonth <> 'N';
END;	                

