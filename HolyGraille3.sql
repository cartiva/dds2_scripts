-- this IS the base
-- 11/1/11 because that's the earliest that i have training & other hours FROM service
  SELECT b.storecode, a.thedate, b.employeekey, technumber, 
    coalesce(b.flaghours, 0) AS flag, 0 AS clock, 0 AS Training, 0 AS Other
  FROM day a
  LEFT JOIN rptFlaghours b ON a.thedate = b.thedate
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  UNION ALL -- adj hours
  SELECT d.storecode, a.thedate, b.employeekey, d.technumber, 0 as flag, 0 as clock, 
    coalesce(sum(CASE WHEN reason = 'Training' THEN b.hours END), 0) AS Training, 
    coalesce(sum(CASE WHEN reason <> 'Training' THEN b.hours END), 0) AS Other
  FROM day a
  LEFT JOIN stgTechClockTimeAdjustments b ON a.thedate = b.thedate
  LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
  LEFT JOIN dimtech d ON c.employeenumber = d.employeenumber
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  GROUP BY d.storecode, a.thedate, b.employeekey, d.technumber
  UNION ALL -- clock hours
  SELECT b.storecode, a.thedate, b.employeekey, technumber, 0 as flag, 
    coalesce(b.clockhours, 0) AS clock, 0 AS training, 0 AS other
  FROM day a
  LEFT JOIN rptClockHours b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  
SELECT a.*, b.name
FROM (
  SELECT b.storecode, a.thedate, b.employeekey, technumber, 
    coalesce(b.flaghours, 0) AS flag, 0 AS clock, 0 AS Training, 0 AS Other
  FROM day a
  LEFT JOIN rptFlaghours b ON a.thedate = b.thedate
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  UNION ALL -- adj hours
  SELECT d.storecode, a.thedate, b.employeekey, d.technumber, 0 as flag, 0 as clock, 
    coalesce(sum(CASE WHEN reason = 'Training' THEN b.hours END), 0) AS Training, 
    coalesce(sum(CASE WHEN reason <> 'Training' THEN b.hours END), 0) AS Other
  FROM day a
  LEFT JOIN stgTechClockTimeAdjustments b ON a.thedate = b.thedate
  LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
  LEFT JOIN dimtech d ON c.employeenumber = d.employeenumber
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  GROUP BY d.storecode, a.thedate, b.employeekey, d.technumber
  UNION ALL -- clock hours
  SELECT b.storecode, a.thedate, b.employeekey, technumber, 0 as flag, 
    coalesce(b.clockhours, 0) AS clock, 0 AS training, 0 AS other
  FROM day a
  LEFT JOIN rptClockHours b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1) a
LEFT JOIN edwEmployeeDim b ON a.employeekey = b.employeekey  
WHERE a.technumber <> 'NA'  
  AND b.storecode = 'RY1' -- it's a human being, NOT a dept
  AND EXISTS ( -- has ever flagged hours
    SELECT 1
    FROM rptFlagHours
    WHERE employeekey = a.employeekey
      AND flaghours <> 0
      AND pydept IN ('Service','PDQ')) -- include holwerda time WHEN distcode was PTEC AND he was flagging hours
  
-- uh oh, no clockhours  
-- duh, come ON, remember this shit
-- premature exclusion
-- OR some such shit
-- need to remember that the base TABLE generates a separate date/empkey/tech# row for
--   each of the 3 time types: clock, flag, train/other
-- so the base needs to be grouped
-- but first, let's ADD some more date info IN the base

  SELECT b.storecode, a.thedate, b.employeekey, technumber, 
    coalesce(b.flaghours, 0) AS flag, 0 AS clock, 0 AS Training, 0 AS Other,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  LEFT JOIN rptFlaghours b ON a.thedate = b.thedate
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  UNION ALL -- adj hours
  SELECT d.storecode, a.thedate, b.employeekey, d.technumber, 0 as flag, 0 as clock, 
    coalesce(sum(CASE WHEN reason = 'Training' THEN b.hours END), 0) AS Training, 
    coalesce(sum(CASE WHEN reason <> 'Training' THEN b.hours END), 0) AS Other,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  LEFT JOIN stgTechClockTimeAdjustments b ON a.thedate = b.thedate
  LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
  LEFT JOIN dimtech d ON c.employeenumber = d.employeenumber
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  GROUP BY d.storecode, a.thedate, b.employeekey, d.technumber,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  UNION ALL -- clock hours
  SELECT b.storecode, a.thedate, b.employeekey, technumber, 0 as flag, 
    coalesce(b.clockhours, 0) AS clock, 0 AS training, 0 AS other,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  LEFT JOIN rptClockHours b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  
-- so, what's the NK of this mishmash
-- 1 row per date/employeekey, non zero value of each of the 3 time types  
SELECT *
INTO #base
FROM (
  SELECT b.storecode, a.thedate, b.employeekey, technumber, 
    coalesce(b.flaghours, 0) AS flag, 0 AS clock, 0 AS Training, 0 AS Other,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  LEFT JOIN rptFlaghours b ON a.thedate = b.thedate
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  UNION ALL -- adj hours
  SELECT d.storecode, a.thedate, b.employeekey, d.technumber, 0 as flag, 0 as clock, 
    coalesce(sum(CASE WHEN reason = 'Training' THEN b.hours END), 0) AS Training, 
    coalesce(sum(CASE WHEN reason <> 'Training' THEN b.hours END), 0) AS Other,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  LEFT JOIN stgTechClockTimeAdjustments b ON a.thedate = b.thedate
  LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
  LEFT JOIN dimtech d ON c.employeenumber = d.employeenumber
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  GROUP BY d.storecode, a.thedate, b.employeekey, d.technumber,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  UNION ALL -- clock hours
  SELECT b.storecode, a.thedate, b.employeekey, technumber, 0 as flag, 
    coalesce(b.clockhours, 0) AS clock, 0 AS training, 0 AS other,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  LEFT JOIN rptClockHours b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1) a
  
-- ? 1 row per date/employeekey, non zero value of each of the 3 time types 
SELECT * FROM #base
SELECT COUNT(*) FROM #base
SELECT * FROM #base WHERE storecode IS NULL 

-- no more than 3 rows for any store,date,empkey,tech#
SELECT *
FROM (
  SELECT storecode, thedate, employeekey, technumber, COUNT(*) AS howmany
  FROM #base 
  WHERE storecode IS NOT NULL 
  group by storecode, thedate, employeekey, technumber) x
WHERE howmany > 3

-- the only empkey with more than one tech# IS -1 (departmental tech#s)
SELECT employeekey
FROM (
  SELECT employeekey, technumber
  FROM #base
  GROUP BY employeekey, technumber) a
GROUP BY employeekey
HAVING COUNT(*) > 1

-- lot's of techs with multiple empkeys
SELECT technumber
FROM (
  SELECT employeekey, technumber
  FROM #base
  GROUP BY employeekey, technumber) a
GROUP BY technumber
HAVING COUNT(*) > 1

-- change #base to grouped
-- DROP TABLE #base
SELECT storecode, thedate, employeekey, technumber, 
    sum(flag) AS flag, SUM(clock) AS clock, SUM(training) AS Training, SUM(other) AS Other,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
INTO #base
FROM (
  SELECT b.storecode, a.thedate, b.employeekey, technumber, 
    coalesce(b.flaghours, 0) AS flag, 0 AS clock, 0 AS Training, 0 AS Other,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  LEFT JOIN rptFlaghours b ON a.thedate = b.thedate
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  UNION ALL -- adj hours
  SELECT d.storecode, a.thedate, b.employeekey, d.technumber, 0 as flag, 0 as clock, 
    coalesce(sum(CASE WHEN reason = 'Training' THEN b.hours END), 0) AS Training, 
    coalesce(sum(CASE WHEN reason <> 'Training' THEN b.hours END), 0) AS Other,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  LEFT JOIN stgTechClockTimeAdjustments b ON a.thedate = b.thedate
  LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
  LEFT JOIN dimtech d ON c.employeenumber = d.employeenumber
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
  GROUP BY d.storecode, a.thedate, b.employeekey, d.technumber,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  UNION ALL -- clock hours
  SELECT b.storecode, a.thedate, b.employeekey, technumber, 0 as flag, 
    coalesce(b.clockhours, 0) AS clock, 0 AS training, 0 AS other,
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  LEFT JOIN rptClockHours b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1) a
WHERE storecode IS NOT NULL -- ?? 
GROUP BY storecode, thedate, employeekey, technumber, 
  yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate;
  
-- now what DO we have
SELECT * FROM #base  
-- bingo
SELECT thedate, employeekey
FROM #base
WHERE employeekey <> -1
GROUP BY thedate, employeekey
HAVING COUNT(*) > 1
-- bango
SELECT thedate,technumber
FROM #base
WHERE employeekey = -1
GROUP BY thedate, technumber
HAVING COUNT(*) > 1

-- ry1 service 
SELECT a.*, b.name
FROM #base a
LEFT JOIN edwEmployeeDim b ON a.employeekey = b.employeekey
WHERE a.employeekey <> -1 
  AND a.storecode = 'ry1'
  AND EXISTS ( -- empkey has ever flagged hours
    SELECT 1
    FROM rptFlagHours
    WHERE employeekey = a.employeekey
      AND flaghours <> 0
      AND pydept IN ('Service','PDQ')) -- include holwerda time WHEN distcode was PTEC AND he was flagging hours  
      
-- DROP TABLE #ry1techs      
SELECT a.thedate, a.technumber, b.name, 
  sum(flag) AS flag, SUM(clock) AS clock, SUM(training) AS Training, SUM(other) AS Other,
  yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
INTO #ry1Techs
FROM (      
  SELECT storecode, thedate, employeekey, technumber, 
      sum(flag) AS flag, SUM(clock) AS clock, SUM(training) AS Training, SUM(other) AS Other,
      yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
  --INTO #base
  FROM (
    SELECT b.storecode, a.thedate, b.employeekey, technumber, 
      coalesce(b.flaghours, 0) AS flag, 0 AS clock, 0 AS Training, 0 AS Other,
      yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
    FROM day a
    LEFT JOIN rptFlaghours b ON a.thedate = b.thedate
    WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
    UNION ALL -- adj hours
    SELECT d.storecode, a.thedate, b.employeekey, d.technumber, 0 as flag, 0 as clock, 
      coalesce(sum(CASE WHEN reason = 'Training' THEN b.hours END), 0) AS Training, 
      coalesce(sum(CASE WHEN reason <> 'Training' THEN b.hours END), 0) AS Other,
      yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
    FROM day a
    LEFT JOIN stgTechClockTimeAdjustments b ON a.thedate = b.thedate
    LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
    LEFT JOIN dimtech d ON c.employeenumber = d.employeenumber
    WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
    GROUP BY d.storecode, a.thedate, b.employeekey, d.technumber,
      yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
    UNION ALL -- clock hours
    SELECT b.storecode, a.thedate, b.employeekey, technumber, 0 as flag, 
      coalesce(b.clockhours, 0) AS clock, 0 AS training, 0 AS other,
      yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate
    FROM day a
    LEFT JOIN rptClockHours b ON a.datekey = b.datekey
    WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1) a
  WHERE storecode IS NOT NULL -- ?? 
  GROUP BY storecode, thedate, employeekey, technumber, 
    yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate) a      
LEFT JOIN edwEmployeeDim b ON a.employeekey = b.employeekey
WHERE a.employeekey <> -1 
  AND a.storecode = 'ry1'
  AND EXISTS ( -- empkey has ever flagged hours
    SELECT 1
    FROM rptFlagHours
    WHERE employeekey = a.employeekey
      AND flaghours <> 0
      AND pydept IN ('Service','PDQ'))
GROUP BY a.thedate, a.technumber, b.name, yearmonth, biweeklypayperiodstartdate, biweeklypayperiodenddate;

-- what have we now
SELECT * FROM #ry1techs
-- bingo
SELECT thedate, technumber
FROM #ry1techs
GROUP BY thedate, technumber
HAVING COUNT(*) > 1

-- monthly totals BY tech
SELECT yearmonth, technumber, name, SUM(flag) AS flag, SUM(clock) AS clock,
  SUM(training) AS training, SUM(other) AS other, 
  SUM(clock) - SUM(training) - SUM(other) AS wrench
FROM #ry1techs  
GROUP BY yearmonth, technumber, name
-- monthly
SELECT yearmonth, SUM(flag) AS flag, SUM(clock) AS clock,
  SUM(training) AS training, SUM(other) AS other, 
  SUM(clock) - SUM(training) - SUM(other) AS wrench
FROM #ry1techs  
GROUP BY yearmonth

-- monthly w/prod
SELECT  yearmonth, SUM(flag) AS flag, 
  SUM(clock) - SUM(training) - SUM(other) AS wrench,
  round(SUM(flag)/(SUM(clock) - SUM(training) - SUM(other)), 2) * 100
FROM #ry1techs  
GROUP BY yearmonth

-- BY payperiod
SELECT biweeklypayperiodstartdate, SUM(flag) AS flag, SUM(clock) AS clock,
  SUM(training) AS training, SUM(other) AS other 
FROM #ry1techs  
GROUP BY biweeklypayperiodstartdate

-- tech totals BY payperiod
SELECT technumber, name, sum(clock) AS clock, sum(flag) AS flag, 
  sum(other) AS other, sum(training) AS training,
  SUM(clock) - SUM(training) - SUM(other) AS wrench
FROM #ry1techs
WHERE biweeklypayperiodstartdate = '07/01/2012'
GROUP BY technumber, name
UNION 
SELECT 'ALL','ALL', sum(clock) AS clock, sum(flag) AS flag, 
  sum(other) AS other, sum(training) AS training,
  SUM(clock) - SUM(training) - SUM(other) AS wrench
FROM #ry1techs
WHERE biweeklypayperiodstartdate = '07/01/2012'
ORDER BY technumber

-- tech totals BY month
SELECT yearmonth, technumber, name, sum(clock) AS clock, sum(flag) AS flag, 
  sum(other) AS other, sum(training) AS training,
  SUM(clock) - SUM(training) - SUM(other) AS wrench
FROM #ry1techs
GROUP BY yearmonth, technumber, name
UNION 
SELECT yearmonth, 'ALL','ALL', sum(clock) AS clock, sum(flag) AS flag, 
  sum(other) AS other, sum(training) AS training,
  SUM(clock) - SUM(training) - SUM(other) AS wrench
FROM #ry1techs
GROUP BY yearmonth
ORDER BY yearmonth, technumber

-- daily numbers, month to date, payperiod to date  
SELECT *
FROM #ry1techs a
LEFT JOIN (
  SELECT technumber, yearmonth, sum(clock) AS clock, sum(flag) AS flag, 
    sum(other) AS other, sum(training) AS training,
    SUM(clock) - SUM(training) - SUM(other) AS wrench
  FROM #ry1techs
  GROUP BY technumber, yearmonth 

-- 1 row per tech/day  
select a.*,
  (SELECT SUM(flag) 
    FROM #ry1techs b
    WHERE yearmonth = a.yearmonth
      AND technumber = a.technumber
      AND thedate <= a.thedate) AS MTDflag,
  (SELECT SUM(clock) - SUM(training) - SUM(other)
    FROM #ry1techs b
    WHERE yearmonth = a.yearmonth
      AND technumber = a.technumber
      AND thedate <= a.thedate) AS MTDwrench,  
  (SELECT SUM(flag) 
    FROM #ry1techs b
    WHERE biweeklypayperiodstartdate = a.biweeklypayperiodstartdate
      AND technumber = a.technumber
      AND thedate <= a.thedate) AS PPTDflag,
  (SELECT SUM(clock) - SUM(training) - SUM(other)
    FROM #ry1techs b
    WHERE biweeklypayperiodstartdate = a.biweeklypayperiodstartdate
      AND technumber = a.technumber
      AND thedate <= a.thedate) AS PPTDwrench,
  (SELECT SUM(flag) 
    FROM #ry1techs b
    WHERE technumber = a.technumber
      AND thedate BETWEEN a.thedate -14 AND a.thedate - 1) AS MPrev14flag, 
  (SELECT SUM(clock) - SUM(training) - SUM(other)
    FROM #ry1techs b
    WHERE technumber = a.technumber
      AND thedate BETWEEN a.thedate -14 AND a.thedate - 1) AS MPrev14wrench                            
FROM #ry1techs a
WHERE thedate BETWEEN '12/01/2011' AND curdate() - 1

SELECT x.*, 
  CASE clock
    WHEN 0 THEN 0
    ELSE round(flag/clock, 2) * 100
  END AS ProdDay,
  CASE mtdwrench
    WHEN 0 THEN 0
    ELSE round(mtdflag/mtdwrench, 2) * 100
  END AS ProdMTD,
  CASE pptdwrench
    WHEN 0 THEN 0
    ELSE round(pptdflag/pptdwrench, 2) * 100
  END AS ProdPPTD,
  CASE prev14wrench
    WHEN 0 THEN 0
    ELSE round(prev14flag/prev14wrench, 2) * 100
  END AS ProdPrev14    
FROM (
  select a.*,
    (SELECT SUM(flag) 
      FROM #ry1techs b
      WHERE yearmonth = a.yearmonth
        AND technumber = a.technumber
        AND thedate <= a.thedate) AS MTDflag,
    (SELECT SUM(clock) - SUM(training) - SUM(other)
      FROM #ry1techs b
      WHERE yearmonth = a.yearmonth
        AND technumber = a.technumber
        AND thedate <= a.thedate) AS MTDwrench,  
    (SELECT SUM(flag) 
      FROM #ry1techs b
      WHERE biweeklypayperiodstartdate = a.biweeklypayperiodstartdate
        AND technumber = a.technumber
        AND thedate <= a.thedate) AS PPTDflag,
    (SELECT SUM(clock) - SUM(training) - SUM(other)
      FROM #ry1techs b
      WHERE biweeklypayperiodstartdate = a.biweeklypayperiodstartdate
        AND technumber = a.technumber
        AND thedate <= a.thedate) AS PPTDwrench,
    coalesce (
      (SELECT SUM(flag) 
        FROM #ry1techs b
        WHERE technumber = a.technumber
          AND thedate BETWEEN a.thedate -14 AND a.thedate - 1), 0) AS Prev14flag, 
    coalesce (
      (SELECT SUM(clock) - SUM(training) - SUM(other)
        FROM #ry1techs b
        WHERE technumber = a.technumber
          AND thedate BETWEEN a.thedate -14 AND a.thedate - 1), 0) AS Prev14wrench                            
  FROM #ry1techs a
  WHERE thedate BETWEEN '12/01/2011' AND curdate() - 1) x

  

CREATE TABLE rptRY1TechProd (
  thedate date, 
  technumber cichar(3),
  name cichar(25),
  flag double,
  clock double, 
  training double,
  other double,
  yearmonth integer, 
  biweeklypayperiodstartdate date,
  biweeklypayperiodenddate date, 
  mtdflag double, 
  mtdwrench double,
  pptdflag double,
  pptdwrench double,
  prev14flag double,
  prev14wrench double,
  prodday double,
  prodmtd double,
  prodpptd double,
  predprev14 double) IN database;
 
EXECUTE PROCEDURE sp_zaptable('rptRY1TechProd');  
INSERT INTO rptRY1TechProd
SELECT x.*, 
  CASE clock
    WHEN 0 THEN 0
    ELSE round(flag/clock, 2) * 100
  END AS ProdDay,
  CASE mtdwrench
    WHEN 0 THEN 0
    ELSE round(mtdflag/mtdwrench, 2) * 100
  END AS ProdMTD,
  CASE pptdwrench
    WHEN 0 THEN 0
    ELSE round(pptdflag/pptdwrench, 2) * 100
  END AS ProdPPTD,
  CASE prev14wrench
    WHEN 0 THEN 0
    ELSE round(prev14flag/prev14wrench, 2) * 100
  END AS ProdPrev14    
FROM (
  select a.*,
    (SELECT SUM(flag) 
      FROM #ry1techs b
      WHERE yearmonth = a.yearmonth
        AND technumber = a.technumber
        AND thedate <= a.thedate) AS MTDflag,
    (SELECT SUM(clock) - SUM(training) - SUM(other)
      FROM #ry1techs b
      WHERE yearmonth = a.yearmonth
        AND technumber = a.technumber
        AND thedate <= a.thedate) AS MTDwrench,  
    (SELECT SUM(flag) 
      FROM #ry1techs b
      WHERE biweeklypayperiodstartdate = a.biweeklypayperiodstartdate
        AND technumber = a.technumber
        AND thedate <= a.thedate) AS PPTDflag,
    (SELECT SUM(clock) - SUM(training) - SUM(other)
      FROM #ry1techs b
      WHERE biweeklypayperiodstartdate = a.biweeklypayperiodstartdate
        AND technumber = a.technumber
        AND thedate <= a.thedate) AS PPTDwrench,
    coalesce (
      (SELECT SUM(flag) 
        FROM #ry1techs b
        WHERE technumber = a.technumber
          AND thedate BETWEEN a.thedate -14 AND a.thedate - 1), 0) AS Prev14flag, 
    coalesce (
      (SELECT SUM(clock) - SUM(training) - SUM(other)
        FROM #ry1techs b
        WHERE technumber = a.technumber
          AND thedate BETWEEN a.thedate -14 AND a.thedate - 1), 0) AS Prev14wrench                            
  FROM #ry1techs a
  WHERE thedate BETWEEN '12/01/2011' AND curdate() - 1) x
  
  
SELECT thedate, prodday, prodmtd, prodpptd, prodprev14 
FROM rptRY1TechProd WHERE technumber = '585'
  
SELECT thedate, flag, 
 clock - training - other AS wrench,
 prodpptd, prodprev14 
FROM rptRY1TechProd 
WHERE technumber = '528' 
  AND thedate BETWEEN '03/01/2012' AND curdate() 

-- what i want IS pptd AND prev14 ON the last day of each payperiod
-- NOT good enuf
SELECT thedate, pptdflag, pptdwrench, prev14flag, prev14wrench, prodpptd, prodprev14
FROM rptry1techprod
WHERE thedate IN (SELECT biweeklypayperiodenddate FROM rptry1techprod)
  AND technumber = '631'
  
-- prodpptd for each ppend
-- AND the prev 7 days of prev14
-- think i need 14 for the graph

SELECT thedate, biweeklypayperiodenddate, prodprev14
FROM rptRy1techprod
WHERE thedate BETWEEN biweeklypayperiodenddate - 14 AND biweeklypayperiodenddate
  AND technumber = '631'
  AND thedate > '05/05/2012'
ORDER BY thedate  
  
SELECT thedate, prodpptd
FROM rptRy1techprod
WHERE thedate = biweeklypayperiodenddate  
  AND technumber = '631'
 
SELECT * FROM rptRY1TechProd  
-- data for the graph  
SELECT thedate, biweeklypayperiodenddate, prodprev14,
  CASE
    WHEN thedate = biweeklypayperiodenddate THEN prodPPTD
    ELSE NULL
  END AS prodPPTD
FROM rptRy1techprod
WHERE thedate BETWEEN biweeklypayperiodenddate - 14 AND biweeklypayperiodenddate
  AND technumber = '537'
  AND thedate > '05/05/2012'
ORDER BY thedate  

 