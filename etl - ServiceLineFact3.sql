INSERT INTO zAllTheRos
-- 5/4/12: pdet type A: mult dates/lin
--         exclude WHERE ptdate = 0
--         exclude ptdate FROM grouping, SELECT MIN ptdate   
-- 5/12: remove the grouping at this level     
-- 5/25: abt 20 minutes  12:02
-- 5/26: included seq: ro 16001654 line 4, tech 511 has 2 lines that are identical IN every way
--       except seq#, so the dupline was being eliminated BY the UNION
--   added ptseq# to zalltheros
-- TRY it without indexes  12:37
-- still taking looong time
-- DO closed first 12:47- 12:52, indexes 12:56
-- 5/26 '16001654'
/*
much faster than UNION, actually probably don't need seq now
DROP TABLE zalltheros
1. sdet
2. pdet
3. indexes 

ADD COLUMN status
5/28 5 dates: line, flag, OPEN, CLOSE, finalclose
*/
DROP TABLE zalltheros;
CREATE TABLE zAllTheRos ( 
      [ptco#] CIChar( 3 ),
      [ptro#] CIChar( 9 ),
      ptfran CIChar( 3 ),
      ptline Integer,
      [ptseq#] Integer,
      ptltyp CIChar( 1 ),
      ptcode CIChar( 2 ),
      pttech CIChar( 3 ),
      ptlhrs Double( 15 ),
      ptsvctyp CIChar( 2 ),
      ptlpym CIChar( 1 ),
      LineDate Date,
      FlagDate date,
      OpenDate date,
      CloseDate date,
      FinalCloseDate date,
      ptlamt Money,
      ptlopc CIChar( 10 ),
      ptcrlo CIChar( 10 ),
      source CIChar( 6 ),
      ptswid CIChar( 3 ),
      status CIChar( 6 )) IN DATABASE;

INSERT INTO zAllTheRos
SELECT sh.ptco#, sh.ptro#, sh.ptfran, sd.ptline, sd.ptseq#, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, sd.ptsvctyp, sd.ptlpym,
  CASE WHEN ptltyp = 'A' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS LineDate, 
  CASE WHEN ptltyp = 'L' AND ptcode = 'TT' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS FlagDate,
  sh.ptdate AS OpenDate, sh.ptcdat AS CloseDate, sh.ptfcdt AS FinalCloseDate,
  sd.ptlamt, sd.ptlopc, sd.ptcrlo, 'SDET' AS source, sh.ptswid, 'Closed'
FROM stgArkonaSDPRHDR sh
LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
    AND gtpost = 'Y')  
  AND sh.ptfran <> '' 
AND sd.ptline IS NOT NULL -- ros with no lines  )  



INSERT INTO zAllTheRos
SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, pd.ptline, pd.ptseq#, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, 
--  min(pd.ptdate) AS LineDate, 
  min(CASE WHEN ptltyp = 'A' THEN pd.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS LineDate, 
  min(CASE WHEN ptltyp = 'L' AND ptcode = 'TT' THEN pd.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS FlagDate,
  max(ph.ptdate) AS OpenDate, '12/31/9999' AS CloseDate, '12/31/9999' AS FinalCloseDate,
  0 as ptlamt, pd.ptlopc,
  pd.ptcrlo, 'PDET' AS source, ph.ptswid, ph.ptrsts
FROM stgArkonaPDPPHDR ph
LEFT JOIN stgArkonaPDPPDET pd ON ph.ptco# = pd.ptco#
  AND ph.ptpkey = pd.ptpkey
WHERE ph.ptdtyp = 'RO' 
  AND ph.ptdoc# <> ''
  AND ph.ptco# IN ('RY1','RY2','RY3')
  AND pd.ptdate <> '12/31/9999' -- this could be done IN the scrape: ptdate <> 0 
  AND NOT EXISTS (
    SELECT 1
    FROM zalltheros
    WHERE ptco# = ph.ptco#
      AND ptro# = ph.ptdoc#
      AND ptline = pd.ptline)
GROUP BY ph.ptco#, ph.ptdoc#, ph.ptfran, pd. ptline, pd.ptseq#, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptlopc, pd.ptcrlo, ph.ptswid, ph.ptrsts
  
  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'zAllTheRos','zAllTheRos.adi','PTCO','ptco#','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'zAllTheRos','zAllTheRos.adi','PTRO','ptro#','',2,512,'' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'zAllTheRos','zAllTheRos.adi','PTLINE','ptline','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'zAllTheRos','zAllTheRos.adi','PTLTYP','ptltyp','',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'zAllTheRos','zAllTheRos.adi','PTCODE','ptcode','',2,512,'' ); 
  
   
-- 5/27
-- historical technumber replacement
-- only needs to be done ON initial load
--SELECT * FROM zalltheros WHERE pttech IN ('m17','21','521','28')
UPDATE zalltheros
SET pttech = '506'
WHERE pttech = 'm17'; 
UPDATE zalltheros
SET pttech = '22'
WHERE pttech = '21';
UPDATE zalltheros
SET pttech = '251'
WHERE pttech = '521';
UPDATE zalltheros
SET pttech = '17'
WHERE pttech = '28';  

-- 5/25, zclosedros = zalltheros WHERE source = sdet  
SELECT *
FROM zclosedros   
ORDER BY ptro#, ptline, ptltyp

SELECT ptco#, ptro#, ptfran, ptline, ptltyp
INTO #lineTyp
FROM zclosedros
GROUP BY ptco#, ptro#, ptfran, ptline, ptltyp

SELECT t.ptco#, t.ptro#, t.ptfran, t.ptline, t.ptltyp 
SELECT z.* 
FROM #lineTyp t
LEFT JOIN zclosedros z ON z.ptco# = t.ptco#
  AND z.ptro# = t.ptro#
  AND z.ptfran = t.ptfran
  AND z.ptline = t.ptline
  AND z.ptltyp = t.ptltyp
WHERE t.ptltyp = 'A'  

-- 5/13 --- 
facts
multiple techs per line
multiple corcode per lin
SELECT *
FROM zclosedros
WHERE ptltyp = 'l'

so the grain IS ptltyp
SELECT ptro#, ptltyp, ptseq#, ptcode, ptdate, ptlpym, pttech, ptlhrs, ptlamt, ptcrlo
--SELECT *
--SELECT SUM(ptlamt)
FROM stgArkonaSDPRDET
WHERE ptro# = '16080001'
  AND ptline = 7
  
SELECT *
FROM zclosedros  
WHERE ptro# = '16080001'
  AND ptline = 7

-- lines with multiple ptcrlo  
-- lost of them
SELECT ptco#, ptro#, ptline
FROM (
  SELECT ptco#, ptro#, ptline, ptcrlo
  FROM  zclosedros
  WHERE (ptcrlo <> '' and ptcrlo IS NOT NULL)
  GROUP BY ptco#, ptro#, ptline, ptcrlo) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

IS there ever more than one tech ON a ptcrlo?
first IS there ever a ptcrlo without a tech

--top level store/ro/fran
-- one fran per ro
SELECT ptco#, ptro#
FROM (
  SELECT ptco#, ptro#, ptfran
  FROM zclosedros
  GROUP BY ptco#, ptro#, ptfran) x
GROUP BY ptco#, ptro#
sHAVING COUNT(*) > 1  

-- single date per line
-- nope, diff dates for same line for A & L lines (at least)
SELECT ptco#, ptro#, ptline
FROM (
  SELECT ptco#, ptro#, ptline, ptdate
  FROM  zclosedros
  WHERE ptdate IS NOT NULL
  GROUP BY ptco#, ptro#, ptline, ptdate) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

-- IS there more than one ptlopc per line?
-- ptlopc EXISTS for both A & L lines (at least)
SELECT ptco#, ptro#, ptline
INTO #wtf
FROM (
  SELECT ptco#, ptro#, ptline, ptlopc
  FROM  zclosedros
  WHERE (ptlopc <> '' and ptlopc IS NOT NULL)
  GROUP BY ptco#, ptro#, ptline, ptlopc) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

SELECT z.ptltyp, COUNT(*)
FROM zclosedros z
INNER JOIN #wtf w ON z.ptco# = w.ptco#
  AND z.ptro# = w.ptro#
  AND z.ptline = w.ptline
WHERE ptlopc <> ''
GROUP BY z.ptltyp
  

A: line creation date
L: flagged hours date
  

-- so circle back around
-- does every line have an A row
SELECT *
FROM zclosedros z
WHERE NOT EXISTS (
  SELECT 1
  FROM zclosedros
  WHERE ptco# = z.ptco#
    AND ptro# = z.ptro#
    AND ptline = z.ptline
    AND ptltyp = 'A')

SELECT *
FROM zclosedros
WHERE ptline IS NULL  

SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# IN (
  SELECT ptro#
  FROM zclosedros
  WHERE ptline IS NULL )

SELECT sh.ptco#, sh.ptro#, sh.ptfran, sh.ptswid
FROM stgArkonaSDPRHDR sh
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
    AND gtpost = 'Y')  
  AND sh.ptfran <> '' 
AND sd.ptline IS NULL -- ros with no lines   

SELECT *
FROM stgArkonaSDPRHDR
WHERE ptfcdt = '12/31/9999'

SELECT *
FROM stgArkonaPDPPHDR
WHERE ptdoc# = '2661694'

-- base closed ros
SELECT sh.ptco#, sh.ptro#, sh.ptfran, sh.ptswid
SELECT *
FROM stgArkonaSDPRHDR sh
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
    AND gtpost = 'Y')  
  AND sh.ptfran <> '' 
  
SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# IN (
SELECT ptro#
FROM stgArkonaSDPRHDR
WHERE ptckey IN (279146,239036))
ORDER BY ptro#, ptline  
  
SELECT DISTINCT ptrsts
FROM stgArkonaPDPPHDR 
WHERE ptdtyp = 'RO' 


 
SELECT ptco#, ptro#, ptfran, ptswid, ptckey, ptcnam, ptdate, ptcdat, ptfcdt, ptvin,
  'Closed' AS status, 'SHDR' AS source
FROM stgArkonaSDPRHDR sh  
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
    AND gtpost = 'Y')  
  AND sh.ptfran <> ''

SELECT ptco#, ptdoc# AS ptro#, ptfran, ptswid, ptckey, ptcnam, ptdate, '12/31/9999','12/31/9999',
  ptvin, ptrsts AS status, 'PHDR' AS source
FROM stgArkonaPDPPHDR ph 
WHERE ptdtyp = 'RO'
  AND ptdoc# <> ''
ORDER BY ptrsts  

--PHDR statuses: 1, 2, 3, 4, 5, 7, 9, A, C, L
SELECT DISTINCT ptrsts
FROM stgArkonaPDPPHDR 
WHERE ptdtyp = 'RO' 

SELECT * FROM stgArkonaPDPPHDR WHERE ptrsts = 'D'

1: Open
2: In-Proc
3: Appr-PD
4: Cashier
5: Cashier/D
6: Pre-Inv
7: Odom Rq
9: Parts-A
A: Apt-SD - no ro#
C: Apt-WP - no ro#
D: Apt-1V - no ro#
L: G/L Error
P: PO Required
SELECT ptco#, ptdoc# AS ptro#, ptfran, ptswid, ptckey, ptcnam, ptdate, ptvin, ptrsts
FROM stgArkonaPDPPHDR ph 
WHERE ptdoc# = '16082833'

SELECT ptco#, ptdoc# AS ptro#, ptfran, ptswid, ptckey, ptcnam, ptdate, ptvin, ptrsts
FROM stgArkonaPDPPHDR ph 
WHERE ptrsts = '9'

SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptrsts, pd.ptpkey, pd.ptline, pd.ptltyp, pd.ptcode,pd.ptsvctyp, 
   pd.ptlsts
FROM stgArkonaPDPPHDR ph 
LEFT JOIN stgArkonaPDPPDET pd ON ph.ptpkey = pd.ptpkey
WHERE ph.ptdtyp = 'RO'
  AND ph.ptdoc# <> ''
ORDER BY ph.ptpkey, pd.ptline  
  
SELECT *
FROM stgArkonaPDPPDET
WHERE ptpkey = 387918
ORDER BY ptline, ptltyp

SELECT *
FROM (
SELECT ptco#, ptro#, ptfran, ptswid, ptckey, ptcnam, ptdate, ptcdat, ptfcdt, ptvin,
  'Closed' AS status, 'SHDR' AS source
FROM stgArkonaSDPRHDR sh  
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
    AND gtpost = 'Y')  
  AND sh.ptfran <> '') a
LEFT JOIN (
  SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptrsts, pd.ptpkey, pd.ptline, pd.ptltyp, pd.ptcode,pd.ptsvctyp, 
     pd.ptlsts
  FROM stgArkonaPDPPHDR ph 
  LEFT JOIN stgArkonaPDPPDET pd ON ph.ptpkey = pd.ptpkey
  WHERE ph.ptdtyp = 'RO'
    AND ph.ptdoc# <> '') b ON a.ptro# = b.ptro#
WHERE b.ptro# IS NOT NULL     


SELECT sh.ptro#, ph.ptrsts
FROM stgArkonaSDPRHDR sh  
LEFT JOIN stgArkonaPDPPHDR ph ON sh.ptro# = ph.ptdoc#
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
    AND gtpost = 'Y')  
  AND sh.ptfran <> ''
  AND ptfcdt ='12/31/9999'
  
SELECT ptco#, ptro#, ptfran, ptswid, ptckey, ptcnam, ptdate, ptvin
FROM stgArkonaSDPRHDR
WHERE ptro# = '16088168'
UNION  
SELECT ptco#, ptdoc#, ptfran, ptswid, ptckey, ptcnam, ptdate, ptvin 
FROM stgArkonaPDPPHDR
WHERE ptdoc# = '16088168' 

SELECT *
  FROM stgArkonaGLPTRNS t
  INNER JOIN (
    SELECT account
    FROM edwAccountDim
    WHERE accounttype = '4'
      AND gldepartmentcode = 'SD') m ON t.gtacct = m.account
WHERE gtdoc# = '16088168'

SELECT *
FROM stgArkonaPDPPHDR
WHERE ptdoc# = '16088168'

SELECT *
FROM stgArkonaPDPPDET
WHERE ptpkey = 468598

--160088168
-- IN cashier status
-- closed lines: 559.29 labor - glptrns
-- OPEN lines: 30.14 labor = pdppdet L/TT ptnet
-- closed lines, 1-7 are NOT IN pdppdet

SELECT COUNT(*) -- 476
FROM stgArkonaPDPPHDR ph 
WHERE ph.ptdtyp = 'RO'
  AND ph.ptdoc# <> ''
  
SELECT COUNT(*) -- 429
FROM stgArkonaPDPPHDR ph 
WHERE ph.ptdtyp = 'RO'
  AND ph.ptdoc# <> ''
  AND NOT EXISTS (
    SELECT 1
    FROM stgArkonaSDPRHDR
    WHERE ptro# = ph.ptdoc#)  
    
-- 3 states
EXISTS IN SHDDR only: status = closed, labor sales: glptrns
EXISTS IN PHDR only: phdr status, labor sales: pdppdet
EXISTS IN both SHDR & PHDR: phdr status: pdppdet + glptrns

SELECT * -- 72
FROM stgArkonaSDPRHDR
WHERE ptfcdt = '12/31/9999'  

SELECT * -- 1
FROM stgArkonaSDPRHDR
WHERE ptcdat = '12/31/9999'

-- why no final CLOSE date
SELECT * -- 25
FROM stgArkonaSDPRHDR sh
WHERE ptfcdt = '12/31/9999' 
  AND NOT EXISTS (
    SELECT 1
    FROM stgArkonaPDPPHDR
    WHERE ptdoc# = sh.ptro#)
    
-- 5/14 back to basics, dimRO, populated BY SHDR & PHDR ------------------------   
-- DROP TABLE #wtf
SELECT *
INTO #wtf 
FROM (  
SELECT ptco#, ptro#, ptfran, ptswid, ptckey, ptcnam, 
  ptdate, ptcdat, ptfcdt, ptvin,
  'Closed' AS status, 'SHDR' AS source
--SELECT COUNT(*) -- 267377, w/ conv
FROM stgArkonaSDPRHDR sh
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
      AND gtpost = 'Y')  
  AND sh.ptfran <> '' 
union
SELECT ph.ptco#, ph.ptdoc# AS ptro#, ptfran, ph.ptswid, ptckey, ptcnam, 
  ptdate, cast('12/31/9999' AS sql_date), cast('12/31/9999' AS sql_date), ptvin, 
  ptrsts, 'PHDR' AS source
FROM stgArkonaPDPPHDR ph 
WHERE ph.ptdtyp = 'RO'
  AND ph.ptdoc# <> '')X  

SELECT *
FROM #wtf
  


-- thinking
SELECT *
FROM (  
  SELECT *  
  FROM #wtf
  WHERE source = 'SHDR') s
INNER JOIN (
  SELECT *
  FROM #wtf
  WHERE source = 'PHDR') p ON s.ptco# = p.ptco# AND s.ptro# = p.ptro#  
ORDER BY ptro#  

-- there are no rows IN SHDR WHERE ptcdat IS null  
SELECT ptco#, ptro#, ptfran, ptswid, ptckey, ptcnam, 
  ptdate, ptcdat, ptfcdt, ptvin,
  'Closed' AS status, 'SHDR' AS source
--SELECT COUNT(*) -- 267377, w/ conv
FROM stgArkonaSDPRHDR sh
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
      AND gtpost = 'Y')  
  AND sh.ptfran <> '' 
  AND ptcdat = '12/31/9999'
  

SELECT ptco#, ptro#, ptfran, ptswid, ptckey, ptcnam, 
  ptdate, ptcdat, ptfcdt, ptvin,
  'Closed' AS status, 'SHDR' AS source
INTO #closed  
--SELECT COUNT(*) -- 267377, w/ conv
FROM stgArkonaSDPRHDR sh
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS ( --
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
      AND gtpost = 'Y')  
  AND sh.ptfran <> '' 

SELECT ph.ptco#, ph.ptdoc# AS ptro#, ptfran, ph.ptswid, ptckey, ptcnam, 
  ptdate, cast('12/31/9999' AS sql_date), cast('12/31/9999' AS sql_date), ptvin, 
  ptrsts, 'PHDR' AS source
INTO #open  
FROM stgArkonaPDPPHDR ph 
WHERE ph.ptdtyp = 'RO'
  AND ph.ptdoc# <> ''

-- this IS CLOSE, but NOT quite
-- ptcdat needs to be FROM SHDR  
SELECT *
INTO #ALL
FROM (  
  SELECT *
  FROM #closed c
  WHERE NOT EXISTS (
    SELECT 1
    FROM #OPEN
    WHERE ptco# = c.ptco#
      AND ptro# = c.ptro#) 
  UNION
  SELECT *
  FROM #OPEN) x
  
SELECT *
FROM #ALL
WHERE source = 'PHDR'
 stgSortOrders so on edw.GMFSDepartment = so.SortOrderValue and so.SortOrderType = 'GMFSDepartment';
  
UPDATE al
  SET  ptcdat = s.ptcdat
  FROM #ALL al
  INNER JOIN #closed s ON al.ptco# = s.ptco# AND al.ptro# = s.ptro#
  WHERE al.source = 'PHDR'

SELECT * FROM #all WHERE source = 'PHDR' ORDER BY ptro#

-- ok this updated the ptcdat ok

SELECT *
FROM stgArkonaPDPPHDR
WHERE ptdoc# IN (
SELECT ptro#
FROM #OPEN)
      
ON OPEN only: labor sales ptltot

SELECT *
FROM #ALL
WHERE source = 'PHDR'
  AND LEFT(ptro#, 1) = '1'
ORDER by status, ptro#

SELECT *
FROM stgArkonaPDPPHDR
WHERE ptdoc# = '16088422'

-- 5/15, pretty comfortable with #ALL AS the basis for dimRO
-- what are the facts that dimRO decorates?
-- factRO: labor(c/s), parts(c/s)
SELECT *
FROM #ALL
WHERE ptco# = 'ry1'
  AND LEFT(ptro#, 2) = '16'
ORDER BY ptdate DESC 

--DROP TABLE zdimRO
CREATE TABLE zdimRO (
  ROKey autoinc,
  StoreCode cichar(3),
  Store cichar(30),
  RO cichar(9),
  Franchise cichar(3),
  ptswid cichar(3),
  ptckey integer, 
  ptcnam cichar(30),
  ptdate date,
  ptcdat date,
  ptfcdt date,
  ptvin cichar(27), 
  Status cichar(12),
  Source cichar(40)) IN database;
Open: 1
In-Proc: 2
Appr-PD: 3
Cashier: 4
Cashier/D: 5
Pre-Inv: 6
Odom Rq: 7
Parts-A: 9
Appt-SD: A - no ro#
Appt-WP: C - no ro#
G/L Error: L    
INSERT INTO zdimRO (StoreCode, store, RO, Franchise, ptswid, ptckey, ptcnam, 
  ptdate, ptcdat, ptfcdt, ptvin, Status, Source) 
SELECT ptco#,
  CASE ptco#
    WHEN 'RY1' THEN 'Rydell'
    WHEN 'RY2' THEN 'Honda'
    WHEN 'RY3' THEN 'Crookston'
  END AS Store, 
  ptro#, ptfran, ptswid, ptckey, ptcnam, ptdate, ptcdat, ptfcdt, ptvin,
  CASE status
    WHEN 'Closed' THEN 'Closed'
    WHEN '1' THEN 'Open'
    WHEN '2' THEN 'In-Proc'
    WHEN '3' THEN 'Appr-PD'
    WHEN '4' THEN 'Cashier'
    WHEN '5' THEN 'Cashier/D'
    WHEN '6' THEN 'Pre-Inv'
    WHEN '7' THEN 'Odom Rq'
    WHEN '9' THEN 'Parts-A'
    WHEN 'L' THEN 'G/L Error'
    ELSE '??????'
  END AS Status, source
FROM #ALL      
  
SELECT store, COUNT(*)
FROM zdimRO
GROUP BY store
/*  
I fact 1  
*/
rokey
datekey
laborcost
laborsales
partscost
partssales  

-- A) parts
-- veering toward pdptdet, but LIKE, labor, isn't glptrns the place to go?

-- DROP TABLE #tdet
SELECT ptco#, ptinv#, SUM(ptcost) AS ptcost, SUM(ptnet) AS ptnet
INTO #tdet
FROM (-- this takes care of negative quantity 
  SELECT ptco#, ptinv#, ptline, ptseq#, ptqty, ptqty*ptcost AS ptcost, ptqty*ptnet AS ptnet
  FROM stgArkonaPDPTDET
  WHERE ptcode IN ('CP','IS','SC','WS','SR')) x -- SR needed for returned parts
GROUP BY ptco#, ptinv#


-- DROP TABLE #glp
SELECT g.gtco#, g.gtdoc#, sum(gttamt)
INTO #gSales
FROM stgArkonaGLPTRNS g
INNER JOIN edwAccountDim a ON g.gtco# = a.storecode
  AND g.gtacct = a.Account
WHERE a.glDepartment = 'Parts'
  AND g.gtjrnl NOT IN ('PAY','PAA')
  AND a.accountType = '4'
GROUP BY g.gtco#, g.gtdoc#

SELECT * FROM #glp

--7846 <>, 142222 = 
SELECT z.ro, z.ptdate, z.status, t.ptcost, t.ptnet, g.expr, t.ptnet + g.expr AS diff
FROM zdimRO z
LEFT JOIN #tdet t ON z.storecode = t.ptco#
  AND z.ro = t.ptinv#  
LEFT JOIN #gSales g ON z.storecode = g.gtco#
  AND z.ro = g.gtdoc#  
--WHERE z.ro = '16007264'  
WHERE ptnet + expr <> 0 
ORDER BY ptnet + expr desc

-- what's missing now IS the discounts FROM glptrns
-- OR rather, it looks LIKE glptrns IS taking adding IN the discount, but tdet doesn't
-- 16001099: diff = 75.08
-- AND that difference IS indeed a +75.08 TRANSACTION to 146700
-- which IS the correct amt for parts sales: with OR without the discount?
SELECT gtacct, gttamt, a.description, a.gldescription, a.accounttype, a.gldepartmentcode
FROM stgArkonaGLPTRNS g
LEFT JOIN edwAccountDim a ON g.gtacct = a.account 
WHERE gtdoc# = '16007264'
--ORDER BY ABS(gttamt)
ORDER BY gldepartmentcode, accounttype

-- 
ro          tdet   glptrns      5250                                                                                                    
16018531   85.95     75.27     85.95 special order
16004650   32.84    135.98     32.84
16007264 2869.17    228.07   2869.17  
16038699  817.35    117.35    117.35 700.00 special ORDER part

SELECT gtacct, gttamt, a.description, a.gldescription, a.accounttype, a.gldepartmentcode
FROM stgArkonaGLPTRNS g
LEFT JOIN edwAccountDim a ON g.gtacct = a.account 
WHERE gtdoc# = '16007264'
--ORDER BY ABS(gttamt)
ORDER BY gldepartmentcode, accounttype

SELECT *
FROM stgArkonaGLPTRNS g
WHERE gtdoc# = '16007264'

SELECT *
FROM stgArkonaPDPTDET
WHERE ptinv# = '16038699'

SELECT *
FROM edwAccountDim
SELECT *
FROM (
SELECT account, description, gldescription 
FROM edwAccountDim
WHERE storecode = 'ry2'
  AND accounttype = '4'
  AND gldepartmentcode = 'sd') a
LEFT JOIN ( 
  SELECT account, description, gldescription
  FROM edwAccountDim
  WHERE storecode = 'ry2'
    AND accounttype = '5') b ON LEFT(a.account,1) = LEFT(b.account, 1) 
--    AND gldepartmentcode = 'sd') b ON LEFT(a.account,1) = LEFT(b.account, 1) 
  AND substring(a.account, 3, 3) = substring(b.account, 3, 3)       

SELECT *
FROM edwaccountdim
WHERE account = '265700'

-- 5/16
i am ALL over the fucking place
does the cost of sales accounts = cost of parts?

SELECT g.gtco#, g.gtdoc#, sum(gttamt)
INTO #gCost
FROM stgArkonaGLPTRNS g
INNER JOIN edwAccountDim a ON g.gtco# = a.storecode
  AND g.gtacct = a.Account
WHERE a.glDepartment = 'Parts'
  AND g.gtjrnl NOT IN ('PAY','PAA')
  AND a.accountType = '5'
  AND g.gtdoc# LIKE '1608%'
GROUP BY g.gtco#, g.gtdoc#

SELECT z.ro, z.ptdate, z.status, t.ptcost, t.ptnet, g.expr AS gSales, c.expr AS Cost
FROM zdimRO z
LEFT JOIN #tdet t ON z.storecode = t.ptco#
  AND z.ro = t.ptinv#  
LEFT JOIN #gSales g ON z.storecode = g.gtco#
  AND z.ro = g.gtdoc# 
LEFT JOIN #gCost c ON z.storecode = c.gtco#
  AND z.ro = c.gtdoc#
WHERE z.ro LIKE '1608%'  
  AND t.ptcost IS NOT NULL 
  AND ptcost <> c.expr
  AND ptnet + g.expr = 0

  
-- 5/18
-- ok, got glpmast & edwAccountDim updated for parts & service sales & cost
SELECT * FROM zDimRO
-- this IS missing at least the date, AND it's slower than shit
SELECT g.gtdoc#, 
  SUM(CASE WHEN a.AccountType = '4' AND a.gldepartment = 'Service' THEN g.gttamt else 0 END) AS "Labor Sales",
  SUM(CASE WHEN a.AccountType = '5' AND a.gldepartment = 'Service' THEN g.gttamt else 0 END) AS "Labor Cost",
  SUM(CASE WHEN a.AccountType = '4' AND a.gldepartment = 'Parts' THEN g.gttamt else 0 END) AS "Parts Sales",
  SUM(CASE WHEN a.AccountType = '5' AND a.gldepartment = 'Parts' THEN g.gttamt else 0 END) AS "Parts Cost"
FROM zDimRO z  
INNER JOIN stgArkonaGLPTRNS g ON z.ro = g.gtdoc#
INNER JOIN edwAccountDim a ON g.gtco# = a.storecode
  AND g.gtacct = a.account
  AND a.glDepartment IN ('Parts','Service')
  AND a.AccountType IN ('4','5')
WHERE z.Status = 'Closed'
  AND z.ptdate > curdate() - 7
GROUP BY g.gtdoc#  
-- look at the details
SELECT gtjrnl, gtacct, gttamt, a.Description, a.gldescription, a.accountType, a.GLDepartmentcode
FROM stgArkonaGLPTRNS g
LEFT JOIN edwAccountDim a ON g.gtacct = a.account
WHERE gtdoc# = '18002689'

  
-- 5/19 
-- so, derive this FROM AcctFact
-- acct fact has data 1/1/12 -4/4/12
SELECT *
FROM edwAccountingFact f
INNER JOIN day d ON f.posteddatekey = d.datekey

-- what we don't have, IS a document number
-- difference BETWEEN doc & control   
SELECT *
FROM edwAccountingFact g
INNER JOIN edwAccountDim a ON g.AccountKey = a.AccountKey
  AND a.AccountType IN ('4','5')
INNER JOIN edwJournalDim j ON g.journalkey = j.journalkey
  AND j.code IN ('SCA','SVI','SWA')
  
  
 -- i know a document IS an ro because ??? of account (includes dept,type) & journal 
 -- ALL transactions

-- these should ALL be ROs 
-- DROP TABLE #wtf
-- added gtdtyp = s
-- switch to GLDepartmentCode
SELECT gtco#, gtjrnl, gldepartmentcode, gtdate, gtacct, gtdoc#, gtctl#, gtdtyp, gttamt 
-- INTO #wtf
-- SELECT *
FROM stgArkonaGLPTRNS g
INNER JOIN edwAccountDim a ON g.gtacct = a.Account -- ON g.gtco# = a.storecode -- uh oh, glptrns co IS ALL ry1
--WHERE g.gtdoc# = '36011538' 
WHERE g.gtdtyp = 's'
  AND a.AccountType = '4'
--  AND a.gldepartmentcode = 'SD'
--  AND a.gmfsdepartment = 'service' -- NOT ALL accounts IN ALL stores IS updated enuf to use this
  AND gldepartmentcode IN ('BS','CW','QL','RE','SD')
  AND g.gtjrnl IN ('SCA','SVI','SWA')
  AND gtdoc# = '16001654'

-- so what are the docs that don't exist IN ros? 
-- including only gtdtyp = s IN #wtf, only one record that IS NOT IN zdimro
-- NOT bad, 2 out of 333795
SELECT *
FROM #wtf w
LEFT JOIN zDimRO z ON w.gtdoc# = z.ro
WHERE z.ro IS NULL 
  AND w.gtdate < curdate() -7
  
-- now the other way
-- there will be beaucoup, lots of ros with 0 labor
-- ros with no records IN #wtf
SELECT *
FROM zdimro z
LEFT JOIN #wtf w ON z.ro = w.gtdoc#
WHERE w.gtdoc# IS NULL  

-- exclude ros with 0 labor
SELECT a.ro, ptltot
-- SELECT COUNT(*) -- only 38, only 4 ry1 ros
FROM (
  SELECT ro
  FROM zdimro z
  LEFT JOIN (
    SELECT gtdoc#, SUM(gttamt) AS gttamt
    FROM #wtf
    GROUP BY gtdoc#) w ON z.ro = w.gtdoc#
  WHERE w.gtdoc# IS NULL) a  
LEFT JOIN stgArkonaSDPRHDR b ON a.ro = b.ptro#
WHERE ptltot <> 0
ORDER BY ro
     
-- oh fuck, i haven't included parts, so of course ros with parts only will NOT be IN #wtf
-- this IS service only
-- so what's up with ALL the body shop ros
SELECT *
FROM stgArkonaGLPTRNS
WHERE gtdoc# = '36011538'

SELECT COUNT(*) FROM #wtf
16022888: there IS no source doc, no RO
  
SELECT account, gldepartment, gmfsaccount, gmfsdepartment
FROM edwAccountDim
WHERE accounttype = '4'
  AND gldepartmentcode = 'sd'
  
SELECT account, gldepartment, gmfsaccount, gmfsdepartment
FROM edwAccountDim
WHERE gldepartment = 'Service'
  AND gldepartment <> gmfsdepartment  
ORDER BY length(account), account 
 
 
 
/*  GLPTRNS */                        
FIELD    Field Def      VALUE      Generated From                
GTDTYP   Doc Type        B          Deals                
GTDTYP   Doc Type        C          Checks                
GTDTYP   Doc Type        D          Deposits                
GTDTYP   Doc Type        I          Inventory Purchase/Stock In            
GTDTYP   Doc Type        J          Conversion or GJE                
GTDTYP   Doc Type        O          Invoice/PO Entry                
GTDTYP   Doc Type        P          Parts Ticket                
GTDTYP   Doc Type        R          Cash Receipts                
GTDTYP   Doc Type        S          Service Tickets                
GTDTYP   Doc Type        W          Handwrittn Checks                
GTDTYP   Doc Type        X          Bank Rec (Fee, Service Charges, etc.)          
GTPOST   Post Status                In Process of Writing to GLPTRNS and GLPMAST (Most Likely)  
GTPOST   Post Status     V          Voided Entry                
GTPOST   Post Status     Y          Posted Entry                   
GTRDTYP  Recon Doc Type             N/A  (It is possible for a document to reconcile with out a Reconciling Doc Type
GTRDTYP  Recon Doc Type  B          Same as Codes for Document Type              
GTRDTYP  Recon Doc Type  C           ""             
GTRDTYP  Recon Doc Type  D           ""            
GTRDTYP  Recon Doc Type  I           ""                
GTRDTYP  Recon Doc Type  J           ""                   
GTRDTYP  Recon Doc Type  O           ""                  
GTRDTYP  Recon Doc Type  P           ""                  
GTRDTYP  Recon Doc Type  R           ""                  
GTRDTYP  Recon Doc Type  S           ""                   
GTRDTYP  Recon Doc Type  X           ""                 
GTTYPE   Record Type                Other*   *Any account that does not require a special reconciliation program.             
GTTYPE   Record Type     $          Entry to a Bank Account              
GTTYPE   Record Type     C          Entry from a Cash Drawer              
GTTYPE   Record Type     M          Other*                
GTTYPE   Record Type     P          Entry to/from Payables              
GTTYPE   Record Type     R          Entry to/from Receiveables    


SELECT gtdtyp, gtjrnl, gtdate, gtdoc#, gtacct, gttamt
FROM stgArkonaGLPTRNS 
WHERE gtdate BETWEEN '05/07/2012' AND '5/12/2012'

SELECT a.*, b.description, b.gldescription, b.accounttype, b.gldepartment,
  gmfsaccount, gmfsdepartment, gmfssection, gmfsarea
FROM (
  SELECT gtacct, gtdtyp, gtjrnl, COUNT(*)
  FROM (
    SELECT gtdtyp, gtjrnl, gtdate, gtdoc#, gtacct, gttamt
    FROM stgArkonaGLPTRNS 
    WHERE gtdate BETWEEN '05/07/2012' AND '5/12/2012') x
  GROUP BY gtacct, gtdtyp, gtjrnl) a  
LEFT JOIN edwAccountDim b ON a.gtacct = b.account  

-- lots of accounts IN glpmast that don't exist IN edwAccountDim
SELECT *
FROM stgArkonaGLPMAST g
WHERE gmyear = 2012
  AND NOT EXISTS (
    SELECT 1
    FROM edwAccountDim
    WHERE Account = g.gmacct)
-- 5/20

-- does ffpxref change year to year
SELECT *
FROM stgarkonaffpxrefdta

SELECT COUNT(*)  -- r109:16646, r110:17115, r111:17830, ry112: 18055
FROM stgarkonaffpxrefdta
WHERE fxcyy = 2009
  AND fxglco# = 'ry1'
  
SELECT fxco#, fxglco#, fxcode, fxcyy, fxgact, fxfact
FROM stgarkonaffpxrefdta
  
SELECT fxco#, COUNT(*)  
FROM stgarkonaffpxrefdta  
GROUP BY fxco#

SELECT *
FROM stgArkonaGLPMAST g
WHERE gmyear = 2012
  AND NOT EXISTS (
    SELECT 1
    FROM stgArkonaFFPXREFDTA
    WHERE fxgact = g.gmacct) 
-- forget ffpxref for now
-- combination of    
glptrns.gtdtype
glptrns.gtjrnl

SELECT gtjrnl, gtdtyp, COUNT(*)
FROM stgArkonaGLPTRNS 
WHERE gtdate BETWEEN '04/01/2012' AND '5/12/2012'
GROUP BY gtjrnl, gtdtyp

SELECT *
FROM stgArkonaGLPTRNS 
WHERE gtdate BETWEEN '04/01/2012' AND '5/12/2012'
  AND gtjrnl = 'PAA'

 
SELECT storecode, gtjrnl, gldepartmentcode, gtacct, gtdtyp, accounttype
-- INTO #wtf
-- SELECT *
FROM stgArkonaGLPTRNS g
INNER JOIN edwAccountDim a ON g.gtacct = a.Account -- ON g.gtco# = a.storecode -- uh oh, glptrns co IS ALL ry1

  
SELECT *
FROM stgArkonaglptrns  
WHERE gtjrnl = 'svi'
  AND gtdtyp = 'j'
  AND month(gtdate) = 5
  AND year(gtdate) = 2012

-- ro with cp AND internal  
SELECT gtdoc#, COUNT(*)
FROM stgArkonaGLPTRNS g
WHERE g.gtdate = '05/10/2012'
  AND EXISTS (
     SELECT 1
     FROM stgArkonaGLPTRNS
     WHERE gtdoc# = g.gtdoc#
       AND gtjrnl = 'svi')
  AND EXISTS (
     SELECT 1
     FROM stgArkonaGLPTRNS
     WHERE gtdoc# = g.gtdoc#
       AND gtjrnl = 'sca')   
GROUP BY gtdoc#           

SELECT gmtype, gmdept, SUM(gttamt)
FROM (       
  SELECT g.gtseq#, g.gtdtyp, g.gtjrnl, g.gtacct, g.gtdesc,
    m.gmtype, m.gmdesc, m.gmdept,
    j.gjdesc, j.gjctlp, j.gjctlp,
    g.gttamt
  FROM stgArkonaGLPTRNS g
  LEFT JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  LEFT JOIN stgArkonaGLPJRND j ON g.gtjrnl = j.gjjrnl
    AND m.gmco# = j.gjco#
  WHERE g.gtdoc# = '16087842'
    AND m.gmyear = 2012  
    AND gmtype IN ('4','5')) x
GROUP BY gmtype, gmdept

SELECT m.gmtype, m.gmdept, g.gtdoc#, g.gtacct, sum(g.gttamt) AS gttamt
FROM stgArkonaGLPTRNS g
LEFT JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
WHERE g.gtdate BETWEEN '04/01/2012' AND '04/30/2012'
  AND g.gtdtyp = 's' -- doc type Service Ticket
  AND m.gmyear = 2012  
  AND gmtype IN ('4','5')
GROUP BY g.gtdoc#, m.gmtype, m.gmdept, g.gtacct
ORDER BY gtdoc#

-- shit not feeling LIKE fucking figuring everyfuckingthing out
-- DO the fucking ro facts

SELECT g.gtdoc#, g.gtdate, SUM(g.gttamt) AS gttamt
FROM stgArkonaGLPTRNS g
LEFT JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
WHERE g.gtdate BETWEEN '04/01/2012' AND '04/30/2012'
  AND g.gtdtyp = 's' -- doc type Service Ticket
  AND m.gmyear = 2012  
  AND m.gmtype IN ('4')
  AND m.gmdept = 'SD'
GROUP BY g.gtdoc#, g.gtdate 
ORDER BY g.gtdoc# 

SELECT g.gtdoc#, g.gtacct, gtjrnl, gtdate, gttamt 
-- SELECT *
-- SELECT DISTINCT g.gtacct
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmyear = 2012
  AND m.gmtype = '4'
  AND m.gmdept = 'SD'
--WHERE gtdoc# = '16082672'
WHERE gtdtyp = 's'

SELECT g.gtdoc#, gtdate, 
  sum(case when m.gmtype = '4' and m.gmdept = 'SD' then gttamt else 0 END) AS "Labor Sales",
  sum(CASE WHEN m.gmtype = '5' and m.gmdept = 'SD' THEN gttamt else 0 END) AS "Labor Cost",
  sum(CASE WHEN m.gmtype = '4' and m.gmdept = 'PD' THEN gttamt else 0 END) AS "Parts Sales",
  sum(CASE WHEN m.gmtype = '5' and m.gmdept = 'PD' THEN gttamt else 0 END) AS "Parts Cost"
-- SELECT *
-- SELECT DISTINCT g.gtacct
INTO #wtf
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmyear = 2012
  AND m.gmtype in ('4', '5')
  AND m.gmdept in ('SD', 'PD')
WHERE gtdtyp = 's'
GROUP BY g.gtdoc#, gtdate
ORDER BY g.gtdoc#

SELECT gtdoc#, SUM([labor sales]) AS "Labor Sales",
  SUM([labor cost]) AS "Labor Cost",
  SUM([parts sales]) AS "Parts Sales",
  SUM([parts cost]) AS "Parts Cost"
FROM #wtf
WHERE gtdate BETWEEN '04/01/2012' AND '04/30/2012'
--WHERE gtdoc# = '36012141'
GROUP BY gtdoc#


SELECT g.gtdoc#, g.gtacct, gtjrnl, gtdate, gttamt , m.gmtype
-- SELECT *
-- SELECT DISTINCT g.gtacct
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmyear = 2012
  AND m.gmtype IN ('4','5')
  AND m.gmdept = 'SD'
WHERE gtdoc# = '36012141'
WHERE gtdtyp = 's'


SELECT *
FROM stgArkonaGLPMAST
WHERE gmyear = 2012
  AND gmtype = '5'
  AND gmstyp = 'A'
  AND gmdept = 'SD'
  
  

SELECT  g.gtdtyp, m.gmtype, m.gmdept, COUNT(*)
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmyear = 2012
  AND m.gmstyp = 'A' -- ry1 only
WHERE g.gtdate BETWEEN '04/01/2012' AND '04/30/2012'  
--  AND gtdtyp = 'p'
--  AND gmtype = '4'
GROUP BY g.gtdtyp, m.gmtype, m.gmdept 

-- Kimball Reader 10.3
SELECT gtdtyp, gtjrnl, gtdate, gtacct, gtdoc#, gtctl#
SELECT *
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmyear = 2012
  AND m.gmstyp = 'A' -- ry1 only
WHERE g.gtdate BETWEEN '04/01/2012' AND '04/30/2012'  
--  AND gtdtyp = 'p'
--  AND gmtype = '4'

SELECT gtdoc#, gtctl#
from (
  SELECT gtdoc#, gtctl#
  FROM stgArkonaGLPTRNS g
  INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
    AND m.gmyear = 2012
    AND m.gmstyp = 'A' -- ry1 only
    
  WHERE g.gtdate BETWEEN '04/01/2011' AND '04/30/2012'  
  --  AND gtdtyp = 'p'
  --  AND gmtype = '4'
  GROUP BY gtdoc#, gtctl#) x
GROUP BY gtdoc#, gtctl# 
HAVING COUNT(*) > 1

-- 5/22
-- inactive accounts
-- there are no transactions ON inactive accounts
SELECT *
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmstyp = 'A' -- ry1 only
WHERE g.gtdate BETWEEN '04/01/2012' AND '04/30/2012'  
AND gmactive = 'N'
--  AND gtdtyp = 'p'
--  AND gmtype = '4'


SELECT *
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmyear = 2012
  AND m.gmstyp = 'A' -- ry1 only
WHERE g.gtdate BETWEEN '04/01/2012' AND '04/30/2012'  
--  AND gtdtyp = 'p'

-- are there transactions that span multiple dates?
SELECT gttrn#, COUNT(*)
FROM stgArkonaGLPTRNs
GROUP BY gttrn#
 
SELECT * FROM stgArkonaGLPTRNS WHERE gttrn# < 0 

-- the combination of acct, jrnl & doc, spans multiple days
SELECT gtacct, gtjrnl, gtdoc#
FROM (
  SELECT gtacct, gtjrnl, gtdoc#, gtdate
  FROM stgArkonaGLPTRNS g
  INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
    AND m.gmyear = 2012
    AND m.gmstyp = 'A' -- ry1 only
  WHERE g.gtdate BETWEEN '04/01/2012' AND '04/30/2012'
  GROUP BY gtacct, gtjrnl, gtdoc#, gtdate) x
GROUP BY gtacct, gtjrnl, gtdoc#
HAVING COUNT(*) > 1  


--hmmm, 
-- does it make sense to eliminate cancelling entries?
SELECT gtacct, gtdoc#, SUM(gttamt)
FROM stgArkonaGLPTRNS g
INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
  AND m.gmyear = 2012
  AND m.gmstyp = 'A' -- ry1 only
WHERE g.gtdate BETWEEN '04/01/2012' AND '04/30/2012'  
GROUP BY gtacct, gtdoc#
HAVING SUM(gttamt) = 0 

SELECT COUNT(*) -- 97010
FROM (
  SELECT gtacct, gtdoc#
  FROM stgArkonaGLPTRNS g
  INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
    AND m.gmyear = 2012
    AND m.gmstyp = 'A' -- ry1 only
  WHERE g.gtdate BETWEEN '04/01/2012' AND '04/30/2012'  
  GROUP BY gtacct, gtdoc#) x
  
SELECT COUNT(*) -- 97010
FROM (
  SELECT gtacct, gtdoc#
  FROM stgArkonaGLPTRNS g
  INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
    AND m.gmyear = 2012
    AND m.gmstyp = 'A' -- ry1 only
  WHERE g.gtdate BETWEEN '04/01/2012' AND '04/30/2012'  
  GROUP BY gtacct, gtdoc#
  HAVING SUM(gttamt) <> 0) x  

-- 5/22
-- back to ro fact
-- IS line:ptlopc 1:1
-- well, yes, IF you look at the A line only
-- the entries IN plopc for the L lines are typos FROM tech entering a value IN Complaint Code
SELECT ptco#, ptro#, ptline
FROM (
  select ptco#, ptro#, ptline, ptlopc
  FROM zalltheros  
  WHERE ptlopc <> ''
    AND LEFT(ptro#, 4) = '1608'
    AND ptltyp = 'a'
  GROUP BY ptco#, ptro#, ptline, ptlopc) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

-- line:ptcrlo 1:M
-- the question IS how to correlate TT line to the individual ptcrlo
-- holy shit, looking at SDPRTXT (16086753), the techs type IN flagged hours next to corr cod
-- 
SELECT ptco#, ptro#, ptline
FROM (
  select ptco#, ptro#, ptline, ptcrlo
  FROM zalltheros  
  WHERE ptcrlo <> ''
    AND LEFT(ptro#, 4) = '1608'
    AND ptltyp = 'l'
  GROUP BY ptco#, ptro#, ptline, ptcrlo) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

-- lines with multiple techs AND corr code per line
SELECT DISTINCT a.ptro#, a.ptline
FROM (
-- ros with mult corr code per line
  SELECT ptco#, ptro#, ptline
  FROM (
    select ptco#, ptro#, ptline, ptcrlo
    FROM zalltheros  
    WHERE ptcrlo <> ''
      AND LEFT(ptro#, 4) = '1608'
      AND ptltyp = 'l'
    GROUP BY ptco#, ptro#, ptline, ptcrlo) x
  GROUP BY ptco#, ptro#, ptline
  HAVING COUNT(*) > 1) a
INNER JOIN (  
-- ros with mult techs per line
  SELECT ptco#, ptro#, ptline
  FROM (
    select ptco#, ptro#, ptline, pttech
    FROM zalltheros  
    WHERE pttech <> ''
      AND LEFT(ptro#, 4) = '1608'
      AND ptltyp = 'l'
    GROUP BY ptco#, ptro#, ptline, pttech) x
  GROUP BY ptco#, ptro#, ptline
  HAVING COUNT(*) > 1) b ON a.ptro# = b.ptro# AND a.ptline = b.ptline

-- DON'T FUCKING WORRY ABOUT CORRELATING FLAG HOURS TO COR CODES

          