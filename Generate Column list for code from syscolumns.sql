-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = 'glptrns');
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
--  @col = (SELECT '[' + TRIM(column_name) + '], '  FROM syscolumns WHERE ordinal_position = @j AND table_name = 'glptrns');
  @col = (SELECT ':' + TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = 'glptrns');
  @cols = @cols + @col;
  @j = @j + 1;  
END WHILE;
SELECT @cols FROM system.iota;  