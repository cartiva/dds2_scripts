SELECT *
FROM edwEmployeeDim
WHERE name IN (
  SELECT name
  FROM edwEmployeeDim
  WHERE currentrow = true
  GROUP BY storecode, name 
  HAVING COUNT(*) > 1)
  
-- 2 cases, 'JOHNSON, JOSEPH', JENKINS,ZACHERY L
-- 6/7/12 Jenkins will keep showing up, the emp# 72519 IS FROM 2009 AND no way to DELETE it IN arkona
-- take care of it IN sp.xfmEmpDIM; DELETE emp# 72519
/* joseph */
bogus empkey: 1439
correct empkey: 1485

-- 1 edwClockHoursFact 
  -- the 2 employeekeys DO have datekey records IN common
  SELECT datekey 
  FROM edwClockHoursFact 
  WHERE employeekey IN (1439,1485)
  GROUP BY datekey 
  HAVING COUNT(*) > 1
-- the days with row IN edwClockHoursFact for both empkey 
SELECT *
FROM edwClockHoursFact
WHERE datekey IN ( 
    SELECT datekey 
    FROM edwClockHoursFact 
    WHERE employeekey IN (1439,1485)
    GROUP BY datekey 
    HAVING COUNT(*) > 1)
  AND employeekey IN (1439,1485)  
-- DO any of those rows have clockhours?
-- Nope
SELECT *
FROM edwClockHoursFact
WHERE datekey IN ( 
    SELECT datekey 
    FROM edwClockHoursFact 
    WHERE employeekey IN (1439,1485)
      AND clockhours <> 0
    GROUP BY datekey 
    HAVING COUNT(*) > 1)
  AND employeekey IN (1439,1485)
-- any bogus key edwClockHoursFact rows with hours?
-- nope
SELECT *
FROM edwClockHoursFact
WHERE employeekey = 1439
  AND clockhours + overtimehours + vacationhours + ptohours + holidayhours > 0
  
-- so IN this CASE, the correct thing to DO IS to DELETE the rows FROM
-- edwClockHoursFact for the bogus employeekey
DELETE FROM edwClockHoursFact 
WHERE employeekey = 1439;  
  
-- 2 edwEmployeeDim 
-- IN this CASE there IS a valid type 2 change (Dept) which needs to be retained
-- this particular one IS goofy: mulitple hiredates but with no term date
-- 5250 shows orig hiredate of 3/21/11 
--  AND last hiredate AS 9/1/11

SELECT year(thedate), month(thedate), employeekey, COUNT(*)
FROM (
  SELECT d.thedate, f.*
  FROM edwClockHoursFact f
  LEFT JOIN day d ON f.datekey = d.datekey
  where employeekey IN (1156,1439,1485)
    AND clockhours <> 0) a
GROUP BY year(thedate), month(thedate), employeekey

-- shows clockhours 3/21/11 thru 5/6/11
-- AND THEN 09/1/2011 thru current
-- edwClockHoursFact does NOT have any of that 3/21 -> 5/6 data
select *
FROM stgArkonaPYPCLOCKIN
WHERE yiemp# = '174150'
ORDER BY yiclkind

SELECT d.thedate, f.* 
FROM day d
INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
  AND employeekey IN (1156,1439,1485)
WHERE d.thedate BETWEEN '03/01/2011' AND '09/01/2011'

-- oh fucking well
-- just DELETE the bogus record FROM edwEmployeeDim 
DELETE FROM edwEmployeeDim
-- SELECT * FROM edwEmployeeDim 
WHERE storecode = 'ry1' and employeenumber = '174'

-- leave the hiredate anomaly alone for now, meaning that the 2 rows IN edwEmployeeDim
-- for emp# 174150 have different hiredates

/* joseph */


/* JENKINS,ZACHERY L */
bogus empkey 
this one IS much more straightforward, plain AND simple a fat finger 
SELECT *
DELETE 
FROM edwClockHoursFact
WHERE employeekey = 1615
--  AND clockhours <> 0

-- just DELETE 
DELETE 
-- SELECT *
FROM edwEmployeeDim
WHERE employeekey = 1615




