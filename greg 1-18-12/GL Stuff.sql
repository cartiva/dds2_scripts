/***** Staging -- Organizations *****/
drop table stgOrganizations;
create table stgOrganizations(
  OrganizationKey AutoInc,
  Level1Code cichar(10),
  Level1Description cichar(30),
  Level2Code cichar(10),
  Level2Description cichar(30),
  Level3Code cichar(10),
  Level3Description cichar(30),
  Level4Code cichar(10),
  Level4Description cichar(30));
insert into stgOrganizations(Level1Code, Level1Description, Level2Code, Level2Description, Level3Code, Level3Description, Level4Code, Level4Description) 
  values('Rydells', 'Rydell AutoCenter', 
           'Variable', 'New, Used, Lease & F&I', 
		     'Sales', 'New, Used & F&I',
			   'New', 'New Vehicles w/F&I');
insert into stgOrganizations(Level1Code, Level1Description, Level2Code, Level2Description, Level3Code, Level3Description, Level4Code, Level4Description) 
  values('Rydells', 'Rydell AutoCenter', 
           'Variable', 'New, Used, Lease & F&I', 
		     'Sales', 'New, Used & F&I',
			   'Used', 'Used Vehicles w/F&I');
insert into stgOrganizations(Level1Code, Level1Description, Level2Code, Level2Description, Level3Code, Level3Description, Level4Code, Level4Description) 
  values('Rydells', 'Rydell AutoCenter', 
           'Variable', 'New, Used, Lease & F&I', 
		     'Sales', 'New, Used & F&I',
			   'Lease', 'Lease & Rental');
insert into stgOrganizations(Level1Code, Level1Description, Level2Code, Level2Description, Level3Code, Level3Description, Level4Code, Level4Description) 
  values('Rydells', 'Rydell AutoCenter', 
           'Fixed', 'Service, Parts & Body', 
		     'Service', 'Main Shop, PDQ & Car Wash',
			   'Main Shop', 'Main Shop');
insert into stgOrganizations(Level1Code, Level1Description, Level2Code, Level2Description, Level3Code, Level3Description, Level4Code, Level4Description) 
  values('Rydells', 'Rydell AutoCenter', 
           'Fixed', 'Service, Parts & Body', 
		     'Service', 'Main Shop, PDQ & Car Wash',
			   'PDQ', 'PDQ');
insert into stgOrganizations(Level1Code, Level1Description, Level2Code, Level2Description, Level3Code, Level3Description, Level4Code, Level4Description) 
  values('Rydells', 'Rydell AutoCenter', 
           'Fixed', 'Service, Parts & Body', 
		     'Service', 'Main Shop, PDQ & Car Wash',
			   'Car Wash', 'Car Wash');
insert into stgOrganizations(Level1Code, Level1Description, Level2Code, Level2Description, Level3Code, Level3Description, Level4Code, Level4Description) 
  values('Rydells', 'Rydell AutoCenter', 
           'Fixed', 'Service, Parts & Body', 
		     'Parts', 'Parts', 
			   'Parts', 'Parts');
insert into stgOrganizations(Level1Code, Level1Description, Level2Code, Level2Description, Level3Code, Level3Description, Level4Code, Level4Description) 
  values('Rydells', 'Rydell AutoCenter', 
           'Fixed', 'Service, Parts & Body', 
		     'Body', 'Body',
			   'Body', 'Body');
			   
/***** Staging -- Sort Orders *****/
drop table stgSortOrders;
create table stgSortOrders(
  SortOrderKey AutoInc,
  SortOrderType cichar(30),
  SortOrderValue cichar(30),
  SortOrder Integer);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSDepartment', 'New', 1);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSDepartment', 'Used', 2);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSDepartment', 'Rental', 3);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSDepartment', 'Service', 4);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSDepartment', 'Body', 5);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSDepartment', 'Parts', 6);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSSection', 'Sales', 1);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSSection', 'CostOfSales', 2);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSSection', 'Variable', 3);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSSection', 'Personnel', 4);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSSection', 'SemiFixed', 5);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GMFSSection', 'Fixed', 6);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('CigarBoxSection', 'Gross', 1);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('CigarBoxSection', 'Variable', 2);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('CigarBoxSection', 'Personnel', 3);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('CigarBoxSection', 'SemiFixed', 4);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('CigarBoxSection', 'Fixed', 5);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'New Vehicle', 1);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'Used Vehicle', 2);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'National', 3);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'Service', 4);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'Quick Lane', 5);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'Detail', 6);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'Car Wash', 7);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'Parts', 8);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'Body Shop', 9);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'Finance', 10);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'ADMIN', 11);
insert into stgSortOrders(SortOrderType, SortOrderValue, SortOrder) values('GLDepartment', 'General', 12);

/***** Account Dimension *****/	  
drop table edwAccountDim; 
create table edwAccountDim(
  AccountKey AutoInc,
  StoreCode cichar(3),
  Store cichar(30),
  Account cichar(12),
  Description cichar(50),
  AccountType cichar(12),
  GLDepartmentCode cichar(2),
  GLDepartment cichar(15),
  GLDepartmentSortOrder Integer,
  GMFSAccount cichar(10),
  GMFSStatementType cichar(30), -- Balance Sheet, Income Statement
  GMFSDepartment cichar(30), -- New Vehicle, Used Vehicle, Lease and Rental, F&I, Service, Body Shop, Parts
  GMFSDepartmentSortOrder Integer,
  GMFSSection cichar(20),  -- Assets, Liabilities, Sales, Gross, Expenses, Additions, Deductions
  GMFSSectionSortOrder Integer,
  GMFSSubSection cichar(30),
  CigarBoxLevel1 cichar(30),
  CigarBoxLevel1SortOrder Integer,
  CigarBoxLevel2 cichar(30),
  CigarBoxLevel2SortOrder Integer,
  CigarBoxLevel3 cichar(30),
  CigarBoxLevel3SortOrder Integer,
  CigarBoxLevel4 cichar(30),
  CigarBoxLevel4SortOrder Integer,
  CigarBoxSection cichar(20), 
  CigarBoxSectionSortOrder Integer,
  Page Integer,
  Line Double);
EXECUTE PROCEDURE sp_CreateIndex90('edwAccountDim','edwAccountDim.adi', 'ACCOUNTKEY', 'AccountKey', '', 2, 512, ''); 
insert into edwAccountDim(StoreCode, Store, Account, Description, GLDepartmentCode, GLDepartment,GMFSAccount, AccountType)
  select 
    case when gmstyp = 'A' then 'RY1' when gmstyp = 'B' then 'RY2' when gmstyp = 'C' then 'RY3' else '???' end,
	case when gmstyp = 'A' then 'Rydell' when gmstyp = 'B' then 'Honda' when gmstyp = 'C' then 'Crookston' else 'None' end, 
	gmacct, gmdesc, gmdept, gddesc, trim(fxfact), gmtype
  from stgArkonaGLPMAST gl
    inner join stgArkonaFFPXREFDTA fs on fs.fxco# = gl.gmco# and fs.fxgact = gl.gmacct and fxconsol = '' and fxcyy = 2011 and fxcode = 'GM'
	left join stgArkonaGLPDEPT gd on gd.gdco# = gl.gmco# and gd.gddept = gl.gmdept
  where gmyear = 2011;
update edw
  set Description = FSDescription, 
      GMFSDepartment = gm.Department,
	    GMFSSection = gm.GMFSSection,
	    CigarBoxSection = gm.CigarBoxSection
  from edwAccountDim edw 
  inner join gmaccounts gm on edw.GMFSAccount = gm.FSAccount;  -- GMAccounts comes from an imported spreadsheet
update edw
  set GMFSDepartmentSortOrder = so.SortOrder
  from edwAccountDim edw 
  inner join stgSortOrders so on edw.GMFSDepartment = so.SortOrderValue and so.SortOrderType = 'GMFSDepartment';	
update edw
  set GMFSSectionSortOrder = so.SortOrder
  from edwAccountDim edw 
  inner join stgSortOrders so on edw.GMFSSection = so.SortOrderValue and so.SortOrderType = 'GMFSSection';	
update edw
  set GLDepartmentSortOrder = so.SortOrder
  from edwAccountDim edw 
  inner join stgSortOrders so on edw.GLDepartment = so.SortOrderValue and so.SortOrderType = 'GLDepartment';	
update edw
  set CigarBoxSectionSortOrder = so.SortOrder
  from edwAccountDim edw 
  inner join stgSortOrders so on edw.CigarBoxSection = so.SortOrderValue and so.SortOrderType = 'CigarBoxSection';	
update edw
  set Page = fxmpge,
      Line = fxmlne  
  from edwAccountDim edw
  inner join stgFSPagesLines pl on edw.GMFSAccount = pl.fxfact;
  
/***** Journal Dimension *****/
drop table edwJournalDim;
create table edwJournalDim(
  JournalKey AutoInc,
  StoreCode cichar(3),
  Store cichar(30),
  Code cichar(3),
  Description cichar(40),
  PrimaryControl cichar(20),
  SecondaryControl cichar(20));
EXECUTE PROCEDURE sp_CreateIndex90('edwJournalDim', 'edwJournalDim.adi', 'STORECODE', 'StoreCode', '', 2, 512, ''); 
EXECUTE PROCEDURE sp_CreateIndex90('edwJournalDim', 'edwJournalDim.adi', 'CODE', 'Code', '', 2, 512, ''); 
EXECUTE PROCEDURE sp_CreateIndex90('edwJournalDim', 'edwJournalDim.adi', 'STORE', 'Store', '', 2, 512, ''); 
insert into edwJournalDim(StoreCode, Store, Code, Description, PrimaryControl, SecondaryControl)
  select gjco#, case gjco# when 'RY1' then 'Rydell' when 'RY2' then 'Honda' when 'RY3' then 'Crookston' else gjco# end, gjjrnl, gjdesc,
    case gjjrnl
	  when 'BTD' then 'Deposit#'
	  when 'CDH' then 'Check#'
	  when 'CDP' then 'Check#'
	  when 'CRC' then 'CashReceipt#'
	  when 'DRI' then 'Vendor#'
	  when 'EFT' then 'Auto'
	  when 'GJE' then 'Auto'
	  when 'PAA' then 'PayPeriod'
	  when 'PAY' then 'Employee#'
	  when 'PCA' then 'PartsTicket#'
	  when 'PCC' then 'PartsTicket#'
	  when 'PCD' then 'PartsTicket#'
	  when 'POT' then 'Vendor#'
	  when 'PVI' then 'Stock#'
	  when 'PVU' then 'Stock#'
	  when 'SCA' then 'RO#'
	  when 'SJE' then 'Auto'
	  when 'STD' then 'Auto'
	  when 'SVI' then 'RO#'
	  when 'SWA' then 'RO#'
	  when 'VSN' then 'Stock#'
	  when 'VSU' then 'Stock#'
	  when 'WCM' then 'Doc#'
	  when 'WTD' then 'Stock#'
	  else 'Unknown'
	end,
    case gjjrnl
	  when 'BTD' then 'None'
	  when 'CDH' then 'Vendor#'
	  when 'CDP' then 'Vendor#'
	  when 'CRC' then 'None'
	  when 'DRI' then 'AR#'
	  when 'EFT' then 'None'
	  when 'GJE' then 'None'
	  when 'PAA' then 'None'
	  when 'PAY' then 'None'
	  when 'PCA' then 'None'
	  when 'PCC' then 'None'
	  when 'PCD' then 'None'
	  when 'POT' then 'Vendor#'
	  when 'PVI' then 'None'
	  when 'PVU' then 'None'
	  when 'SCA' then 'Stock#'
	  when 'SJE' then 'None'
	  when 'STD' then 'None'
	  when 'SVI' then 'Stock#'
	  when 'SWA' then 'None'
	  when 'VSN' then 'Last6VIN#'
	  when 'VSU' then 'Last6VIN#'
	  when 'WCM' then 'None'
	  when 'WTD' then 'None'
	  else 'Unknown'
	end
  from stgArkonaGLPJRND;

  
  

/***** Source Dimension *****/
-- BTD Journal -- Deposit# from Deposits
-- CDH Journal -- Check#
-- CDP Journal -- Check#
-- CRC Journal -- CashReceipt#
-- DRI Journal -- Vendor#
-- EFT Journal -- Auto
-- GJE Journal -- Auto
-- PAA Journal -- PayPeriod
-- PAY Journal -- Employee#
-- PCA Journal -- PartsTicket#
-- PCC Journal -- PartsTicket#
-- PCD Journal -- PartsTicket#
-- POT Journal -- Vendor#
-- PVI Journal -- Stock#
-- PVU Journal -- Stock#
-- SCA Journal -- RO#
-- SJE Journal -- Auto
-- STD Journal -- Auto
-- SVI Journal -- RO#
-- SWA Journal -- RO#
-- VSN Journal -- Stock#
-- VSU Journal -- Stock#
-- WCM Journal -- Doc#
-- WTD Journal -- Stock#

drop table edwSourceDim;
create table edwSourceDim(
  SourceKey AutoInc,
  StoreCode cichar(3),
  Store cichar(30),
  JournalCode cichar(3),
  ControlCode cichar(20),
  DocumentCode cichar(20),
  DocumentType cichar(20),
  Document cichar(40));
EXECUTE PROCEDURE sp_CreateIndex90('edwSourceDim', 'edwSourceDim.adi', 'SourceKey', 'SourceKey', '', 2, 512, ''); 
EXECUTE PROCEDURE sp_CreateIndex90('edwSourceDim', 'edwSourceDim.adi', 'StoreCode', 'StoreCode', '', 2, 512, ''); 
EXECUTE PROCEDURE sp_CreateIndex90('edwSourceDim', 'edwSourceDim.adi', 'DocumentCode', 'DocumentCode', '', 2, 512, ''); 
insert into edwSourceDim(StoreCode, Store, JournalCode, ControlCode, DocumentCode)
  select case gmstyp when 'A' then 'RY1' when 'B' then 'RY2' when 'C' then 'RY3' end,
    case gmstyp when 'A' then 'Rydell' when 'B' then 'Honda' when 'C' then 'Crookston' end, trim(gtjrnl), trim(gtctl#), trim(gtdoc#)
  from stgArkonaGlptrns gl
  inner join stgArkonaGLPMAST gm on gm.gmco# = gl.gtco# and gm.gmacct = gl.gtacct
  where gtpost = 'Y' and year(gtdate) = 2011 and month(gtdate) in (11)
  group by gmstyp, trim(gtjrnl), trim(gtctl#), trim(gtdoc#);
update sd
  set Document = py.ymname, 
      DocumentType = 'Employee'
  from edwSourceDim sd
	inner join stgArkonaPYMAST py on trim(py.ymempn) = sd.ControlCode and sd.JournalCode = 'PAY';
update sd
  set Document = sv.ptcnam,
      DocumentType = 'RO'
  from edwSourceDim sd
    inner join stgArkonaSDPRHDR sv on sv.ptro# = sd.DocumentCode and sd.JournalCode = 'SCA';

/***** Accounting Fact *****/
--update stgArkonaGlptrns
--  set gtctl# = trim(gtctl#)
drop table edwAccountingFact;
create table edwAccountingFact(
  PostedDateKey Integer,
  AccountKey Integer,
  JournalKey Integer,
  SourceKey Integer,
  ControlKey Integer,
  Amount Money);
EXECUTE PROCEDURE sp_CreateIndex90('edwAccountingFact', 'edwAccountingFact.adi', 'PostedDateKey', 'PostedDateKey', '', 2, 512, ''); 
EXECUTE PROCEDURE sp_CreateIndex90('edwAccountingFact', 'edwAccountingFact.adi', 'AccountKey',    'AccountKey',    '', 2, 512, ''); 
EXECUTE PROCEDURE sp_CreateIndex90('edwAccountingFact', 'edwAccountingFact.adi', 'JournalKey',    'JournalKey',    '', 2, 512, ''); 
EXECUTE PROCEDURE sp_CreateIndex90('edwAccountingFact', 'edwAccountingFact.adi', 'SourceKey',     'SourceKey',     '', 2, 512, ''); 
EXECUTE PROCEDURE sp_CreateIndex90('edwAccountingFact', 'edwAccountingFact.adi', 'ControlKey',    'ControlKey',    '', 2, 512, ''); 
insert into edwAccountingFact(PostedDateKey, AccountKey, JournalKey, SourceKey, Amount)
  select DateKey, AccountKey, JournalKey, SourceKey, gttamt
  from stgArkonaGlptrns gl
    inner join Day on Day.TheDate = gl.gtdate
    inner join edwAccountDim ad on ad.Account = gl.gtacct
	inner join edwJournalDim jd on jd.Code = gl.gtjrnl and jd.StoreCode = gl.gtco#
	left join edwControlDim cd on (cd.Type = jd.PrimaryControl or cd.Type = jd.SecondaryControl) and cd.Number = gl.gtctl#
	left join edwSourceDim sd on sd.JournalCode = gl.gtjrnl and sd.ControlCode = trim(gl.gtctl#) and sd.DocumentCode = trim(gl.gtdoc#) and gl.gtjrnl in ('PAY', 'SCA')
  where gtpost = 'Y' and year(gtdate) = 2011 and month(gtdate) in (11, 12)
  
 
  
  
  
  
  
  
  
  
  
  
select Account, Description, GLDepartment, GMFSDepartment 
from edwAccountDim
where CigarBoxSection not in ('Asset', 'Liability')
  and GMFSDepartment not in ('Dealership')
order by Account



  
/***** Work Queries *****/	
/*update sd
  set Document = py.ymname, 
      DocumentType = 'Employee'
select sd.*, py.*
  from edwSourceDim sd
	inner join stgArkonaPYMAST py on trim(py.ymempn) = trim(sd.DocumentCode) and sd.JournalCode = 'PAY';
	
	
select *
from edwSourceDim
where JournalCode = 'PVU'
	
/***** Control Dimension *****/
--drop table edwControlDim;
--create table edwControlDim(
--  ControlKey AutoInc,
--  Number cichar(20),
--  Type cichar(20),
--  Description cichar(60));
--EXECUTE PROCEDURE sp_CreateIndex90('edwControlDim', 'edwControlDim.adi', 'ControlKey', 'ControlKey', '', 2, 512, ''); 
--EXECUTE PROCEDURE sp_CreateIndex90('edwControlDim', 'edwControlDim.adi', 'Number', 'Number', '', 2, 512, ''); 
--insert into edwControlDim(Number, Type, Description)
--  select trim(ymempn), 'Employee#', ymname
--  from pymast;
  
  
select *
from stgArkonaGLPMAST
where gmyear = 2011


  
  
update edwAccountingFact	

update edw
  set SourceKey = sd.SourceKey 
  from edwAccountingFact edw 
    inner join edwJournalDim jd on edw.JournalKey = jd.JournalKey
    inner join edwSourceDim sd on jd.Code = 'PAY' and sd.DocumentType = 'Employee#' and edw.GMFSAccount = gm.FSAccount;  -- GMAccounts comes from an imported spreadsheet






-- How do I do the GM Statement Xref for Honda and Crookston?

  
/***** Parts Personnel for November *****/  
select GMFSDepartment, GMFSSection, sum(Amount)
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.TheYear = 2011 and MonthName in ('November')
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where GMFSDepartment = 'Parts'
  and GMFSSection = 'Personnel'
group by GMFSDepartment, GMFSSection

/***** Parts Personnel Drill Down *****/
select GMFSDepartment, GMFSSection, ad.Account, ad.Description, sum(Amount)
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.TheYear = 2011 and MonthName in ('November')
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where GMFSDepartment = 'Parts'
  and GMFSSection = 'Personnel'
group by GMFSDepartment, GMFSSection, ad.Account, ad.Description

/***** Parts Other Salaries Drill Down *****/
select GLDepartment, GMFSSection, ad.Account, ad.Description, cd.Description, sum(Amount)
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.TheYear = 2011 and MonthName in ('November')
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
  left join edwControlDim cd on cd.ControlKey = af.ControlKey
where GLDepartment = 'Detail'
  and GMFSSection = 'Personnel' and ad.Description = 'Other Salaries'
group by GLDepartment, GMFSSection, ad.Account, ad.Description, cd.Description


select 
/***** Parts Other Salaries by Finanacial Statment Account *****/
select sum(Amount)
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.TheYear = 2011 and MonthName in ('November')
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where GMFSAccount = '023F'

/***** Employee Dimension *****/
drop table edwEmployeeDim;
create table edwEmployeeDim(
  EmployeeKey AutoInc,
  Store cichar(30),
  EmployeeNumber cichar(12),
  Name cichar(30),
  Manager cichar(30),
  Department cichar(30),
  BirthDate Date);
insert into edwEmployeeDim(Store, EmployeeNumber, Name, Manager, Department, BirthDate)
  select case ymco# when 'RY1' then 'Rydell' when 'RY2' then 'Honda' when 'RY3' then 'Crookston' else 'None' end, trim(ymempn), trim(ymname), 'Manager', trim(ymdept), '1/1/2000'
  from pymast py;

/***** Cigar Boxes *****/  

/***** Total Store *****/
select CigarBoxSection as Section, sum(Amount) as Amount
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.YearMonth = 201112
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where GMFSDepartment not in ('Dealership', 'Admin')
group by CigarBoxSection, CigarBoxSectionSortOrder
order by CigarBoxSectionSortOrder

/***** Financial Statement Departments *****/
select GMFSDepartment as Dept, CigarBoxSection as Section, sum(Amount) as Amount
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.YearMonth = 201112
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where GMFSDepartment not in ('Dealership', 'Admin')
group by GMFSDepartment, CigarBoxSection, GMFSDepartmentSortOrder, CigarBoxSectionSortOrder
order by GMFSDepartmentSortOrder, CigarBoxSectionSortOrder

/***** Financial Statement Departments with Financial Statement Accounts *****/		
select GMFSDepartment as Dept, CigarBoxSection as Section, Description, sum(Amount) as Amount
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.YearMonth = 201112
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where GMFSDepartment not in ('Dealership', 'Admin')
group by GMFSDepartment, CigarBoxSection, Description, GMFSDepartmentSortOrder, CigarBoxSectionSortOrder, Line
order by GMFSDepartmentSortOrder, CigarBoxSectionSortOrder, Line

/***** General Ledger Departments *****/		
select GLDepartment as Dept, CigarBoxSection as Section, sum(Amount) as Amount
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.YearMonth = 201112
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where CigarBoxSection not in ('Asset', 'Liability')
  and GMFSDepartment not in ('Dealership', 'Admin')
group by GLDepartment, CigarBoxSection, GLDepartmentSortOrder, CigarBoxSectionSortOrder
order by GLDepartmentSortOrder, CigarBoxSectionSortOrder
  
/***** General Ledger Departments with Financial Statement Accounts *****/		
select GLDepartment as Dept, CigarBoxSection as Section, Description, sum(Amount) as Amount
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.YearMonth = 201112
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where CigarBoxSection not in ('Asset', 'Liability')
  and GMFSDepartment not in ('Dealership', 'Admin')
group by GLDepartment, CigarBoxSection, Description, GLDepartmentSortOrder, CigarBoxSectionSortOrder, Line
order by GLDepartmentSortOrder, CigarBoxSectionSortOrder, Line

select GLDepartment as Dept, GMFSDepartment as FSDept, Account, CigarBoxSection as Section, Description, sum(Amount) as Amount
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.YearMonth = 201112
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where CigarBoxSection not in ('Asset', 'Liability')
  and GMFSDepartment not in ('Dealership', 'Admin')
group by GLDepartment, GMFSDepartment, Account, CigarBoxSection, Description, GLDepartmentSortOrder, CigarBoxSectionSortOrder, Line
order by GLDepartmentSortOrder, CigarBoxSectionSortOrder, Line

select MonthName, sum(Amount) as Amount
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.YearMonth = 201112
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where GMFSAccount in ('467', '478')
  and GMFSDepartment not in ('Dealership', 'Admin')
group by MonthName

/***** Daily Mechanical Parts Split *****/
select TheDate, sum(Amount)
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.YearMonth = 201112
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where (GMFSAccount like '467%' or GMFSAccount like '667%' or GMFSAccount like '478%' or GMFSAccount like '678%')
  and GMFSDepartment not in ('Dealership', 'Admin')
group by TheDate
order by TheDate

/***** Daily Body Shop Parts Split *****/
select TheDate, sum(Amount)
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.YearMonth = 201112
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where (GMFSAccount like '477%' or GMFSAccount like '677%')
  and GMFSDepartment not in ('Dealership', 'Admin')
group by TheDate
order by TheDate




select *
from edwAccountDim
where CigarBoxSection not in ('Asset', 'Liability')
--  and (GLDepartment is null or GLDepartment = '')
  and GMFSDepartment not in ('Dealership', 'Admin')
  and Description = 'Shop Supplies' 
--  and GMFSDepartment = 'New'
--  and CigarBoxSection = 'SemiFixed'
order by GMFSDepartment


select * 
from stgArkonaFFPXREFDTA
where fxgact = '185700'
  and fxcyy = 2011


select *
from gmaccounts
where FSDescription = 'Shop Supplies'

select Description, sum(Amount) as Amount, Account, GMFSDepartment, GLDepartment
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.YearMonth = 201112
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where CigarBoxSection not in ('Asset', 'Liability')
  and GMFSDepartment not in ('Dealership', 'Admin')
  and Description = 'E-Commerce Advertising Fees'
  and GMFSDepartment = 'New'
group by Account, Description, GMFSDepartment, GLDepartment


order by GLDepartmentSortOrder, CigarBoxSectionSortOrder, Line

		 

		 
select case GMFSDepartment
         when 'New' then '1-New'
		 when 'Used' then '2-Used'
		 when 'Rental' then '3-Rental'
		 when 'Service' then '4-Service'
		 when 'Body' then '5-Body'
		 when 'Parts' then '6-Parts' end as Dept, 
	   case GMFSSection
	     when 'CostOfSales' then '1-Gross'
		 when 'Sales' then '1-Gross'
		 when 'Variable' then '2-Variable'
		 when 'Personnel' then '3-Personnel'
		 when 'SemiFixed' then '4-SemiFixed'
		 when 'Fixed' then '5-Fixed' end as Section, GMFSAccount, Description, Account, sum(Amount)
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.TheYear = 2011 and MonthName  in ('November') --'October'
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
where GMFSDepartment not in ('Dealership', 'Admin')
group by case GMFSDepartment
         when 'New' then '1-New'
		 when 'Used' then '2-Used'
		 when 'Rental' then '3-Rental'
		 when 'Service' then '4-Service'
		 when 'Body' then '5-Body'
		 when 'Parts' then '6-Parts' end, 
	   case GMFSSection
	     when 'CostOfSales' then '1-Gross'
		 when 'Sales' then '1-Gross'
		 when 'Variable' then '2-Variable'
		 when 'Personnel' then '3-Personnel'
		 when 'SemiFixed' then '4-SemiFixed'
		 when 'Fixed' then '5-Fixed' end, GMFSAccount, Description, Account
order by case GMFSDepartment
         when 'New' then '1-New'
		 when 'Used' then '2-Used'
		 when 'Rental' then '3-Rental'
		 when 'Service' then '4-Service'
		 when 'Body' then '5-Body'
		 when 'Parts' then '6-Parts' end, 
	   case GMFSSection
	     when 'CostOfSales' then '1-Gross'
		 when 'Sales' then '1-Gross'
		 when 'Variable' then '2-Variable'
		 when 'Personnel' then '3-Personnel'
		 when 'SemiFixed' then '4-SemiFixed'
		 when 'Fixed' then '5-Fixed' end, GMFSAccount, Description, Account


		 
select GMFSDepartment as Dept, GMFSSection as Section, GMFSAccount, Description, Account, Document, sum(Amount)
from edwAccountingFact af
  inner join Day on Day.DateKey = af.PostedDateKey and Day.TheYear = 2011 and MonthName  in ('November') --'October'
  left join edwAccountDim ad on ad.AccountKey = af.AccountKey
  left join edwSourceDim sd on sd.SourceKey = af.SourceKey
where GMFSDepartment not in ('Dealership', 'Admin')
  and GMFSSection = 'Variable'
  and GMFSDepartment = 'New'
--  and Description = 'O'
group by GMFSDepartment, GMFSSection, GMFSAccount, Description, Account, Document
order by GMFSDepartment, GMFSSection, GMFSAccount, Description, Account
		 
select count(*)
from edwAccountingFact
where SourceKey is not null


select *
from syscolumns
where column_text like '%Check%'
order by table_name

select *
from stgArkonaGLPTRNS
where gtjrnl = 'SCA'




select count(*)
from edwAccountingFact af
inner join edwJournalDim jd on jd.JournalKey = af.JournalKey
inner join edwAccoutDim 
  where jd.Code = 'PAY'
  



select count(*)
from stgArkonaGlptrns gl
inner join AllControls al on gl.gtdoc# = al.Control#
where gtdoc# <> '' and gtdoc# is not null


update gmaccounts
  set cigarbox = 'Gross'
  where cigarbox in ('Sales', 'CostOfSales')
  
select * from stgArkonaGlptrns
select count(*) from AllControls


select count(Control#) from allcontrols

/***** GM Financial Statement Categories *****/
create table edwAccountDimSupportGMFSCategories(
  Category cichar(30));
  
*/
