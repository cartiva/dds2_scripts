-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	  WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
  	WHEN data_type = 'numeric' THEN 'integer,'
  	WHEN data_type = 'timestmp' THEN 'timestamp,'
  	WHEN data_type = 'smallint' THEN 'integer,'
  	WHEN data_type = 'integer' THEN 'double,'
  	WHEN data_type = 'decimal' THEN 'double,'
  	WHEN data_type = 'date' THEN 'date,'
  	WHEN data_type = 'time' THEN 'cichar(8),'
  	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'BOPNAME' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text, ordinal_position
from syscolumns
WHERE table_name = 'BOPNAME' 
ORDER BY ordinal_position

-- 2. non char field list for spreadsheet
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'BOPNAME'  
  AND data_type <> 'CHAR'
ORDER BY ordinal_position

-- DROP TABLE stgArkonaBOPNAME
-- 3. revise DDL based ON step 2
CREATE TABLE stgArkonaBOPNAME (
bnco#      cichar(3),          
bnkey      integer,             
bntype     cichar(1),          
bnss#      integer,             
bnsnam     cichar(50),         
bnlnam     cichar(50),         
bnfnam     cichar(15),         
bnmidi     cichar(25),         
bnsalu     cichar(10),         
bngender   cichar(1),          
bnlang     cichar(1),          
bnadr1     cichar(60),         
bnadr2     cichar(60),         
bncity     cichar(20),         
bncnty     cichar(20),         
bnstcd     cichar(2),          
bnzip      cichar(9),             
bnphon     cichar(10),             
bnbphn     cichar(20),             
bnbext     cichar(4),             
bnfax#     cichar(10),             
bnbdat     date,             
bndlic     cichar(20),         
bncntc     cichar(25),         
bnpcntc    cichar(1),          
bnmail     cichar(1),          
bntaxe     cichar(20),         
bncsts     cichar(5),          
bnctype    cichar(1),          
bnprefphn  cichar(1),          
bncphon    cichar(10),             
bnpphon    cichar(10),             
bnophon    cichar(10),             
bnopdesc   cichar(15),         
bnemail    cichar(60),         
bnoptf     cichar(30),         
bnacbm     cichar(1),          
bnacbp     cichar(1),          
bnadbe     cichar(1),          
bnadr3     cichar(45),         
bnbexta    cichar(10),         
bnbphni    cichar(15),         
bncdat     date,             
bncphoni   cichar(15),         
bnekey     integer,             
bneml2     cichar(40),         
bnfax#i    cichar(15),         
bnophoni   cichar(15),         
bnphoni    cichar(20),         
bnpnam     cichar(50),         
bnpphoni   cichar(15),         
bnprln     cichar(3),          
bnzipi     cichar(10),         
bntoken    cichar(10),         
bnccid     cichar(25),         
bnsxfnam   cichar(4),          
bnsxlnam   cichar(4),          
bnsxadr1   cichar(4),          
bnoptf2    cichar(100),        
bntaxe2    cichar(20),         
bntaxe3    cichar(20),         
bntaxe4    cichar(20),         
bngstreg#  cichar(20),         
bnpstdat   integer,             
bndlrcd    integer,             
bncccd     integer,             
bncstver   integer,             
bnadrver   integer,             
bnadrvcur  integer,             
bncrtusr   integer,             
bncrtts    cichar(30),          
bnupdusr   integer,             
bnupdts    cichar(30),          
bnctrycd   cichar(4),          
bndlst     cichar(2),          
bnotptrcd  cichar(3),          
bnstatus   cichar(1)) IN database;    

--CREATE TABLE tmpBOPNAME() IN database;

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tableName string;
@tableName = 'BOPNAME';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   
  

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'BOPNAME';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;

-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaBOPNAME';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;

/************************* 4/22/12 ********************************************/
-- start looking at tool vs arkona for daily cross off review

SELECT * FROM stgArkonaBOPMAST WHERE bmstk# = '16788x'

SELECT bmco#, bmtype, bmvtyp, bmstk#, bmvin, 
  bmdtor AS "Orig Date", 
  bmdtsv AS "Date Saved",
  bmdtaprv AS "Date Appr",
  bmdtcap AS "Date Capped",
  bmdelvdt AS "Del Date"
FROM stgArkonaBOPMAST
WHERE bmco# = 'RY1' 
--  AND bmvtyp = 'U'
  AND (
    bmdtor = '04/20/2012' OR
    bmdtsv = '04/20/2012' OR
    bmdtaprv = '04/20/2012' OR
    bmdtcap = '04/20/2012' OR
    bmdelvdt = '04/20/2012')
    
SELECT bmco#, bmtype, bmvtyp, bmstk#, bmvin, 
  bmdtor AS "Orig Date", 
  bmdtsv AS "Date Saved",
  bmdtaprv AS "Date Appr",
  bmdtcap AS "Date Capped",
  bmdelvdt AS "Del Date"
FROM stgArkonaBOPMAST
WHERE bmstk# IN (
'15697A',
'14476C',
'14330b',
'10479na',
'16690x',
'16216a',
'16791x',
'15989xx',
'15879b',
'16740p') 

SELECT *
FROM stgArkonabopmast b
WHERE EXISTS (
  SELECT 1
  FROM stgArkonabopmast
  WHERE bmvin = b.bmvin
    AND bmco# = b.bmco#
    AND bmkey = b.bmkey
    AND TRIM(bmstk#) <> b.bmstk#)

-- 9/19/2012 removed the capped only filter FROM the scrape