/*
the TABLE ON ark has additional fields:
PTTIME
PTQOHCHG
PTLASTCHG
PTUPDUSER
PTIDENTITY
PTKTXAMT
PTLNTAXE
PTUPDATE
none of which seem to have any data
so i am NOT scraping them
*/

-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	  WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
  	WHEN data_type = 'numeric' THEN 'integer,'
  	WHEN data_type = 'timestmp' THEN 'timestamp,'
  	WHEN data_type = 'smallint' THEN 'integer,'
  	WHEN data_type = 'integer' THEN 'double,'
  	WHEN data_type = 'decimal' THEN 'double,'
  	WHEN data_type = 'date' THEN 'date,'
  	WHEN data_type = 'time' THEN 'cichar(8),'
  	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'PDPTDET' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'PDPTDET'  
AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'PDPTDET'  
AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaPDPTDET
-- 3. revise DDL based ON step 2, CREATE table
CREATE TABLE stgArkonaPDPTDET (
ptco#      cichar(3),          
ptinv#     cichar(9),          
ptline     integer,             
ptseq#     integer,             
pttgrp     cichar(1),          
ptcode     cichar(2),          
ptsoep     cichar(1),          
ptdate     date,             
ptcdate    date,             
ptcpid     cichar(3),          
ptmanf     cichar(3),          
ptpart     cichar(25),         
ptsgrp     cichar(3),          
ptqty      integer,             
ptcost     money,             
ptlist     money,             
ptnet      money,             
ptepcdiff  money,             
ptspcd     cichar(1),          
ptorso     cichar(1),          
ptpovr     cichar(1),          
ptgprc     cichar(1),          
ptxcld     cichar(1),          
ptfprt     cichar(1),          
ptrtrn     cichar(1),          
ptohat     integer,             
ptvatcode  cichar(1),          
ptvatamt   money) IN database;     

CREATE TABLE tmpPDPTDET (
ptco#      cichar(3),          
ptinv#     cichar(9),          
ptline     integer,             
ptseq#     integer,             
pttgrp     cichar(1),          
ptcode     cichar(2),          
ptsoep     cichar(1),          
ptdate     date,             
ptcdate    date,             
ptcpid     cichar(3),          
ptmanf     cichar(3),          
ptpart     cichar(25),         
ptsgrp     cichar(3),          
ptqty      integer,             
ptcost     money,             
ptlist     money,             
ptnet      money,             
ptepcdiff  money,             
ptspcd     cichar(1),          
ptorso     cichar(1),          
ptpovr     cichar(1),          
ptgprc     cichar(1),          
ptxcld     cichar(1),          
ptfprt     cichar(1),          
ptrtrn     cichar(1),          
ptohat     integer,             
ptvatcode  cichar(1),          
ptvatamt   money) IN database;        

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PDPTDET';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PDPTDET';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaPDPTDET';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
 
/************ 4/23 *************/
nightly scrape
just DO a flat 30 day ON ptdate OR ptcdate
worry about integrity of the data
IN the xfm to it's use
