alter TABLE xfmEmployeeDim 
alter column StoreCodeDesc Store CIChar( 30 )
alter column ActiveCodeDesc Active CIChar( 8 )
alter column FullPartTimeCodeDesc FullPartTime CIChar( 8 )
alter column PyDeptCodeDesc PyDept CIChar( 30 )
alter column DistCodeDesc Distribution CIChar( 25 )
alter column PayrollClassCodeDesc PayrollClass CIChar( 12 )
alter column PayPeriodCodeDesc PayPeriod CIChar( 15 );


alter TABLE edwEmployeeDim  
alter column StoreCodeDesc Store CIChar( 30 )
alter column ActiveCodeDesc Active CIChar( 8 )
alter column FullPartTimeCodeDesc FullPartTime CIChar( 8 )
alter column PyDeptCodeDesc PyDept CIChar( 30 )
alter column DistCodeDesc Distribution CIChar( 25 )
alter column PayrollClassCodeDesc PayrollClass CIChar( 12 )
alter column PayPeriodCodeDesc PayPeriod CIChar( 15 );


ALTER PROCEDURE xfmEmpDIM
   ( 
   ) 
BEGIN 
/*
EXECUTE PROCEDURE xfmEmpDIM()

Populates TABLE xfmEmployeeDim with stgArkonaPYMAST
  field names
  surrogate keys FROM Day & resArkonaCodes
Constraint checks against xfmEmployeeDim  
Adds news records to edwEmployeeDim
Updates Type 1 changes IN edwEmployeeDim
Updates Type 2 changes IN edwEmployeeDim
Constraint check against edwEmployeeDim
  emp#/store/MostCur = true

12/31
  also, modify edwED structure to use ALL ALL 5 Kimball type 2 dimension columns  
  
1/9/12
  broke out act/term AND full/part INTO separate columns
  decode the values with CASE statements IN select 
1/11/12
  went with kimball metadata columns for the dimension  
  NK becomes co#/emp#
1/26/12
  accomodate rehire & emp# reuse  
  ADD columns DistCode, Distribution, TechNumber
  ADD RowChangeReason to Type2 changes
1/28
  ADD columns EmpKeyFrom/ThruDate & Key  
2/1
  ADD columns PayrollClassCode/DESC, Salary, HourlyRate ALL type 2  
2/6
  ADD columns 
    PayPeriodCode CIChar (1) : B = Bi-Weekly Type 1
    PayPeriod CIChar(15) : S = Semi-Monthly Type 1  
2/18
  Accomodate type 2 changes IN PYPCLKCTL, PYACTGR  
2/23   
// check constraints ON xfmEmployeeDIM
-- NULL fields
-- commented out birthdate & birthdatekey
-- see FB 53
*/
DECLARE @NowTS Timestamp;
TRY
  EXECUTE PROCEDURE sp_ZapTable('xfmEmployeeDim');
  BEGIN TRANSACTION; 
  TRY 
    INSERT INTO xfmEmployeeDim (StoreCode, Store, EmployeeNumber, 
      ActiveCode, Active, FullPartTimeCode, FullPartTime,
      PyDeptCode, PyDept, DistCode, Distribution, TechNumber, Name, 
      BirthDate, BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey,
      PayrollClassCode, PayrollClass, Salary, HourlyRate, 
      PayPeriodCode, PayPeriod)
    SELECT ymco# AS StoreCode,
      store.codeDesc AS Store, ymempn AS EmployeeNumber, p.ymactv AS ActiveCode, 
      CASE 
        WHEN p.ymactv = 'A' THEN 'Active'
        WHEN p.ymactv = 'P' THEN 'Active'
        WHEN p.ymactv = 'T' THEN 'Term'
      END AS Active, 
      p.ymactv AS FullPartTimeCode,  
      CASE 
        WHEN p.ymactv = 'A' THEN 'Full'
        WHEN p.ymactv = 'P' THEN 'Part'
      END AS FullPartTime,      
--      trim(dept.code) AS PyDeptCode,
--      trim(dept.codeDesc) AS PyDeptCodeDesc,
      pc.yicdept AS PyDeptCode, pc.yicdesc AS PyDept,
      ymdist AS DistCode, d.ytadsc AS Distribution, t.sttech AS TechNumber, ymname AS Name, 
      ymbdte AS BirthDate, bd.DateKey AS BirthDateKey, ymhdte AS HireDate, 
      hd.DateKey AS HireDateKey, ymtdte AS TermDate, td.DateKey AS TermDateKey,
      ymclas AS PayrollClassCode, 
      CASE
        WHEN p.ymclas = 'H' THEN 'Hourly' 
        WHEN p.ymclas = 'S' THEN 'Salary'
        WHEN p.ymclas = 'C' THEN 'Commission'
      END AS DescrPayrollClass, ymsaly AS Salary, ymrate AS HourlyRate,
      ympper AS PayPeriodCode, 
      CASE ympper
        WHEN 'B' THEN 'Bi-Weekly'
        WHEN 'S' THEN 'Semi-Monthly'
      END as PayPeriod      
    FROM stgArkonaPYMAST p
    LEFT JOIN day bd ON p.ymbdte = bd.thedate
    LEFT JOIN day hd ON p.ymhdte = hd.thedate
    LEFT JOIN day td ON p.ymtdte = td.thedate
    LEFT JOIN resArkonaCodes store ON store.stgTableName = 'stgArkonaPYMAST'
      AND store.stgColumnName = 'ymco#'
      AND store.storeCode = 'All'
      AND p.ymco# = store.code
    LEFT JOIN stgArkonaPYPCLKCTL pc ON p.ymco# = pc.yicco#
      AND p.ymdept = pc. yicdept
      AND pc.CurrentRow = true
    LEFT JOIN ( -- 1 row per store/code/desc
        SELECT ytaco#, ytadic, ytadsc
        FROM stgArkonaPYACTGR
        WHERE CurrentRow = true
        GROUP BY ytaco#, ytadic, ytadsc) d 
      ON p.ymco# = d.ytaco# 
      AND p.ymdist = d.ytadic  
    LEFT JOIN ( -- this was necessary because sdptech IS a mish mash
      SELECT MAX(sttech) AS sttech, stpyemp#, stco#
      FROM stgArkonaSDPTECH
      WHERE TRIM(sttech) NOT LIKE 'M%'
      GROUP BY stpyemp#, stco#) t ON p.ymempn = t.stpyemp#
        AND p.ymco# = t.stco#;    
  COMMIT WORK;
  CATCH ALL 
    ROLLBACK;
	  RAISE SPxfmEmpDIM(100, 'INSERT INTO xfmEmployeeDim');
  END TRY; // TRANSACTION  
// check constraints ON xfmEmployeeDIM
  TRY 
    IF ( -- 1 row per EmployeeNumber, StoreCode
      SELECT COUNT(*)
      FROM (
        SELECT EmployeeNumber
        FROM xfmEmployeeDim
        GROUP BY EmployeeNumber, StoreCode
  	    HAVING COUNT(*) > 1) x) > 0 THEN
      RAISE SPxfmEmpDIM(200, '1 row per EmployeeNumber, StoreCode');
    END IF;
    IF ( -- missing departments, IF NOT term, must have dept
      SELECT COUNT(*)
      FROM xfmEmployeeDim
      WHERE coalesce(PyDeptCode, '') = ''
        AND ActiveCode <> 'T') > 0 THEN
      RAISE SPxfmEmpDIM(300, 'missing departments');
    END IF;
    IF ( -- missing DistCode, IF NOT term, must have DistCode
      SELECT COUNT(*)
      FROM xfmEmployeeDim
      WHERE coalesce(DistCode, '') = ''
        AND ActiveCode <> 'T') > 0 THEN
      RAISE SPxfmEmpDIM(400, 'missing dist code');
    END IF;  
    IF ( -- NULL fields
      SELECT COUNT(*)
      FROM xfmEmployeeDim
      WHERE (
        EmployeeNumber IS NULL OR
        ActiveCode IS NULL OR
        Active IS NULL OR
--        BirthDateKey IS NULL OR 
--        HireDateKey IS NULL OR
        PayrollClassCode IS NULL OR
        PayPeriodCode IS NULL)) > 0 THEN 
      RAISE SPxfmEmpDIM(777, 'Null Fields');
    END IF; 
  CATCH ALL 
--    RAISE SPxfmEmpDIM(500, __errtext);
    RAISE; // nested exceptions, only the innermost RAISE contain info, ALL OUTER layers must be just RAISE
  END TRY;
// new records/SCD  
// the TRANSACTION should encompass ALL possible changes to the Dimension Table
  BEGIN TRANSACTION; 
  TRY
    TRY 
      @NowTS = (SELECT now() FROM system.iota);
// new records	  
      INSERT INTO edwEmployeeDim(StoreCode, Store, EmployeeNumber, 
        ActiveCode, Active, FullPartTimeCode, FullPartTime,
        PyDeptCode, PyDept, DistCode, Distribution, TechNumber, Name, BirthDate, 
        BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, CurrentRow,
        EmployeeKeyFromDate, EmployeeKeyFromDateKey, EmployeeKeyThruDate, EmployeeKeyThruDateKey,
        PayrollClassCode, PayrollClass, Salary, HourlyRate, 
        PayPeriodCode, PayPeriod) 
      SELECT x.StoreCode, x.Store, x.EmployeeNumber, 
        x.ActiveCode, x.Active, x.FullPartTimeCode, x.FullPartTime,
        x.PyDeptCode, x.PyDept, x.DistCode, x.Distribution, x.TechNumber,x.Name, x.BirthDate, 
        x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, true,
        x.HireDate AS EKFrom, x.HireDateKey AS EKFromKey,
        (SELECT thedate FROM day WHERE datetype = 'NA') AS EKThru,
        (SELECT datekey FROM day WHERE datetype = 'NA') as EKThruKey, 
        x.PayrollClassCode, x.PayrollClass, x.Salary, x.HourlyRate,
        x.PayPeriodCode, x.PayPeriod
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
  		  AND edwED.CurrentRow = true
      WHERE edwED.StoreCode IS NULL;  
    CATCH ALL
      RAISE SPxfmEmpDIM(111, 'New Records');
    END TRY;
    TRY 
// type 1 changes: HireDate, BirthDate, Term, TechNumber, PayPeriodCode	
    -- HireDate
      --    * HireDate change only, skip the rehire/reuse cases
      UPDATE edwED
        SET edwED.HireDateKey = x.HireDateKey
      -- SELECT *
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE x.HireDateKey <> edwED.HireDateKey
        AND x.ActiveCode = edwED.ActiveCode -- *
        AND x.TermDateKey = edwED.TermDateKey; -- * 
    -- BirthDate
      UPDATE edwED
        SET edwED.BirthDateKey = x.BirthDateKey
      -- SELECT * 
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE x.BirthDateKey <> edwED.BirthDateKey; 
    -- Active 
      --  only those going FROM A OR P to T - terms
      UPDATE edwED
        SET edwED.ActiveCode = x.ActiveCode,
            edwED.Active = x.Active,
            edwED.TermDate = x.TermDate,
            edwED.TermDateKey = x.TermDateKey,
            edwED.EmployeeKeyThruDate = x.TermDate,
            edwED.EmployeeKeyThruDateKey = x.TermDateKey
      -- SELECT *
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true  
      WHERE edwED.ActiveCode IN ('A','P') 
        AND x.ActiveCode = 'T';
    -- TechNumber
      UPDATE edwED
        SET edwED.TechNumber = x.TechNumber
      -- SELECT * 
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE x.TechNumber <> edwED.TechNumber;
    -- PayPeriodCode
      UPDATE edwED
        SET edwED.PayPeriodCode = x.PayPeriodCode,
            edwED.PayPeriod = x.PayPeriod
      -- SELECT * 
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE x.PayPeriodCode <> edwED.PayPeriodCode;      
    CATCH ALL
      RAISE SPxfmEmpDIM(222, 'Type 1');
    END TRY;
      
// type 2 changes: name, full/parttime, dept, dept desc, DistCode, 
//   PayrollClassCode, Salary, HourlyRate, DistCode Desc
    TRY 
      SELECT Utilities.DropTablesIfExist('#Type2') FROM system.iota;
      -- DROP TABLE #Type2
      SELECT edwED.EmployeeKey, 
        CASE WHEN x.Name <> edwED.Name THEN 'Name ' ELSE '' END
        +
        CASE WHEN x.FullPartTimeCode <> edwED.FullPartTimeCode THEN 'FullPart ' ELSE '' END
        +
        CASE WHEN x.PyDeptCode <> edwED.PyDeptCode THEN 'Dept ' ELSE '' END
        +
        CASE WHEN x.PyDept <> edwED.PyDept THEN 'DeptDescription ' ELSE '' END
        +        
        CASE WHEN x.DistCode <> edwED.DistCode THEN 'DistCode ' ELSE '' END 
        +
        CASE WHEN x.PayrollClassCode <> edwED.PayrollClassCode THEN 'PayrollClassCode ' ELSE '' END
        +
        CASE WHEN x.Salary <> edwED.Salary THEN 'Salary ' ELSE '' END 
        +
        CASE WHEN x.HourlyRate <> edwED.HourlyRate THEN 'HourlyRate ' ELSE '' END 
        +
        CASE WHEN x.Distribution <> edwED.Distribution THEN 'Distribution ' ELSE '' END AS RowChangeReason,        
        x.*	
      INTO #Type2  
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE (
          x.Name <> edwED.name OR
          ((edwED.FullPartTimeCode = 'A' and x.FullPartTimeCode = 'P') 
            OR (edwED.FullPartTimeCode = 'P' and x.FullPartTimeCode = 'A')) OR -- full to part OR part to full
          x.PyDeptCode <> edwED.PyDeptCode OR
          x.DistCode <> edwED.DistCode OR
          x.PayrollClassCode <> edwED.PayrollClassCode OR
          x.Salary <> edwED.Salary OR
          x.HourlyRate <> edwED.HourlyRate OR
          x.PyDept <> edwED.PyDept OR 
          x.Distribution <> edwED.Distribution)
        AND edwED.ActiveCode <> 'T'; -- exclude reuse records that could be picked up BY name OR dept
    CATCH ALL
      RAISE SPxfmEmpDIM(333, 'generate #Type2'); 
    END TRY;
    TRY   
      IF (SELECT COUNT(*) FROM #Type2) > 0 THEN 
    -- UPDATE the old record   
        UPDATE edwEmployeeDim
        SET RowChangeDate = CAST(@NowTS AS sql_date),
            RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date)),
            RowThruTS = @NowTS,
            CurrentRow = false, 
            RowChangeReason = x.RowChangeReason,
            EmployeeKeyThruDate = CAST(timestampadd(sql_tsi_day, -1, @NowTS) AS sql_date),
            EmployeeKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST(timestampadd(sql_tsi_day, -1, @NowTS) AS sql_date))
        -- SELECT *   
        FROM #Type2 x
        LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
          AND x.employeeNumber = e.EmployeeNumber
          AND e.CurrentRow = true;          
    -- INSERT new record        
        INSERT INTO edwEmployeeDim(StoreCode, Store, EmployeeNumber, 
            ActiveCode, Active, FullPartTimeCode, FullPartTime,
            PyDeptCode, PyDept, DistCode, Distribution, TechNumber, Name, BirthDate,  
            BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
            RowFromTS, CurrentRow, EmployeeKeyFromDate, EmployeeKeyFromDateKey,
            EmployeeKeyThruDate, EmployeeKeyThruDateKey, 
            PayrollClassCode, PayrollClass, Salary, HourlyRate,
            PayPeriodCode, PayPeriod) 
        SELECT StoreCode, Store, EmployeeNumber, 
            ActiveCode, Active, FullPartTimeCode, FullPartTime,
            PyDeptCode, PyDept, DistCode, Distribution, TechNumber, Name, BirthDate, 
            BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
            @NowTS, TRUE,     
            CAST(@NowTS AS sql_date),
            (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date)),
            (SELECT thedate FROM day WHERE datetype = 'NA'),
            (SELECT datekey FROM day WHERE datetype = 'NA'),
            PayrollClassCode, PayrollClass, Salary, HourlyRate,
            PayPeriodCode, PayPeriod           
        FROM #Type2; 
      END IF; // IF (SELECT COUNT(*) FROM #Type2) > 0	
    CATCH ALL
      RAISE SPxfmEmpDIM(444, 'Type2 Updates');	 		
    END TRY;
// Rehire/Reuse: type 2 changes
    TRY 
      SELECT Utilities.DropTablesIfExist('#Rehire') FROM system.iota;
      -- DROP TABLE #Rehire
      SELECT edwED.EmployeeKey, x.*	
      INTO #Rehire 
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE edwED.ActiveCode = 'T' 
        AND x.ActiveCode IN ('P','A');
    
      IF (SELECT COUNT(*) FROM #Rehire) > 0 THEN 
    -- UPDATE the old record
        UPDATE edwEmployeeDim 
        SET RowChangeDate = CAST(@NowTS AS sql_date),
            RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date)),
            RowThruTS = @NowTS,
            CurrentRow = false,
            RowChangeReason = 'Rehire/Reuse',
            EmployeeKeyThruDate = cast(timestampadd(sql_tsi_day, -1, x.HireDate) AS sql_date),
            EmployeeKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = cast(timestampadd(sql_tsi_day, -1, x.HireDate) AS sql_date))
        -- SELECT *    
        FROM #Rehire x
        LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
          AND x.employeeNumber = e.EmployeeNumber
          AND e.CurrentRow = true;
    -- INSERT new record        
        INSERT INTO edwEmployeeDim(StoreCode, Store, EmployeeNumber, 
            ActiveCode, Active, FullPartTimeCode, FullPartTime,
            PyDeptCode, PyDept, DistCode, Distribution, TechNumber, Name, BirthDate, 
            BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
            RowFromTS, CurrentRow, EmployeeKeyFromDate, EmployeeKeyFromDateKey,
            EmployeeKeyThruDate, EmployeeKeyThruDateKey,
            PayrollClassCode, PayrollClass, Salary, HourlyRate,
            PayPeriodCode, PayPeriod) 
        SELECT StoreCode, Store, EmployeeNumber, 
            ActiveCode, Active, FullPartTimeCode, FullPartTime,
            PyDeptCode, PyDept, DistCode, Distribution, TechNumber, Name, BirthDate, 
            BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
            @NowTS, TRUE,
            HireDate AS EKFrom, HireDateKey AS EKFromKey,
            (SELECT thedate FROM day WHERE datetype = 'NA'),
            (SELECT datekey FROM day WHERE datetype = 'NA'),
            PayrollClassCode, PayrollClass, Salary, HourlyRate,
            PayPeriodCode, PayPeriod                     
        FROM #Rehire; 
      END IF; // IF (SELECT COUNT(*) FROM #Rehire) > 0		 
    CATCH ALL
      RAISE SPxfmEmpDIM(555, 'Rehire Reuse');	  
	  END TRY;
// edwEmployeeDimension constraint checks
-- 1 CURRENT row per EmployeeNumber, StoreCode
    IF ( -- 1 CURRENT row per EmployeeNumber, StoreCode
      SELECT COUNT(*)
      FROM (
        SELECT EmployeeNumber, StoreCode
        FROM edwEmployeeDim
        WHERE CurrentRow = true
        GROUP BY EmployeeNumber, StoreCode
          HAVING COUNT(*) > 1)x) > 0 THEN
      RAISE SPxfmEmpDIM(450, 'edwEmployeeDimension constraint checks');
    END IF; 
  COMMIT WORK;  	  
  CATCH ALL
    ROLLBACK;
    RAISE SPxfmEmpDIM(999, __errtext);   
  END TRY; // transaction
CATCH ALL
--  RAISE SPxfmEmpDIM(666, __errtext);
  RAISE;
END TRY;




END;

