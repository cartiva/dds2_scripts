@nowts was NOT SET IN xfmDimTech
@nowts used for type 2 updates
dimtech runs at 01:15:05

wednesdays back up ok
thursdays back up fucked up

therefore @nowts = '07/25/2013 01:15:05'
-- old rows
SELECT *
FROM dimtech
WHERE rowchangereason = 'laborcost'

DECLARE @NowTS timestamp;
@NowTS = (SELECT CAST('07/25/2013 01:15:05' AS sql_timestamp) FROM system.iota);
UPDATE dimTech
SET RowChangeDate = cast(@NowTS AS sql_Date),
    RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = cast(@NowTS AS sql_Date)),
    RowThruTS = @NowTS,
--    CurrentRow = false,
--    RowChangeReason = a.RowChangeReason,
    TechKeyThruDate = CAST(timestampadd(sql_tsi_day, -1, @NowTS) AS sql_date),
    TechKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST(timestampadd(sql_tsi_day, -1, @NowTS) AS sql_date))
WHERE rowchangereason = 'laborcost'    

-- new rows
SELECT *
FROM dimtech
WHERE techkeyfromdate IS NULL 

DECLARE @NowTS timestamp;
@NowTS = (SELECT CAST('07/25/2013 01:15:05' AS sql_timestamp) FROM system.iota);
UPDATE dimtech
SET RowFromTS = @NowTS,
    TechKeyFromDate = CAST(@NowTS AS sql_date), 
    TechKeyFromDateKey = (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date))
WHERE techkeyfromdate IS NULL 

SELECT *
FROM dimtech
WHERE (
  techkeyfromdate IS NULL 
  OR techkeyfromdatekey is NULL
  OR techkeythrudate IS NULL 
  OR techkeythrudatekey IS NULL);
  
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimtech','TechKeyFromDate', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimtech','TechKeyFromDateKey', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimtech','TechKeyThruDate', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimtech','TechKeyThruDateKey', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );      
  
-- also enclosed ALL edits/updates/inserts to dimtech IN a transaction  