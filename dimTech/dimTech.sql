SELECT t.*, e.*
FROM stgArkonaSDPTECH t
LEFT JOIN edwEmployeeDim e ON t.stpyemp# = e.employeenumber
  AND e.currentrow = true
  
SELECT *
FROM zalltheros
WHERE pttech = 'M17'

CREATE TABLE dimTech (
  TechKey autoinc,
  StoreCode cichar(3),
  Store cichar (30),
  EmployeeNumber cichar(7),
  Name CIChar( 25 ), 
  Description CIChar(20),
  TechNumber cichar(3),
  LaborCost money,
  CurrentRow Logical,
  RowChangeDate Date,
  RowChangeDateKey Integer,
  RowFromTS TimeStamp,
  RowThruTS TimeStamp,
  RowChangeReason CIChar( 100 ),
  TechKeyFromDate Date,
  TechKeyFromDateKey Integer,
  TechKeyThruDate Date,
  TechKeyThruDateKey Integer) IN database; 
  
-- 5/26, going with ALL type 1
ALTER TABLE dimtech
DROP column  CurrentRow
DROP column RowChangeDate 
DROP column RowChangeDateKey
DROP column RowFromTS
DROP column RowThruTS
DROP column RowChangeReason
DROP column TechKeyFromDate
DROP column TechKeyFromDateKey
DROP column TechKeyThruDate
DROP column TechKeyThruDateKey

  
-- initially populate techkeyfrom

SELECT e.storecode, e.employeenumber, e.name, e.TechNumber, t.sttech, t.stlrat,
  MIN(e.hiredate) AS TechKeyFromDate, t.stactv
FROM edwEmployeeDim e
INNER JOIN stgArkonaSDPTECH t ON e.storecode = t.stco#
  AND e.Employeenumber = t.stpyemp#
GROUP BY e.storecode, e.employeenumber, e.name, e.TechNumber, t.sttech, t.stlrat, t.stactv  
ORDER BY name

SELECT *
FROM (
SELECT e.name, e.employeenumber, e.TechNumber, t.sttech, t.stlrat,
  MIN(e.hiredate) AS TechKeyFromDate, t.stactv
FROM edwEmployeeDim e
INNER JOIN stgArkonaSDPTECH t ON e.storecode = t.stco#
  AND e.Employeenumber = t.stpyemp#
GROUP BY e.storecode, e.employeenumber, e.name, e.TechNumber, t.sttech, t.stlrat, t.stactv) x
WHERE name IN ( 
  SELECT e.name
  FROM edwEmployeeDim e
  INNER JOIN stgArkonaSDPTECH t ON e.storecode = t.stco#
    AND e.Employeenumber = t.stpyemp#
  GROUP BY e.storecode, e.employeenumber, e.name
  HAVING COUNT(*) > 1) 
ORDER BY employeenumber  

SELECT DISTINCT storecode, name, employeenumber, technumber, t.*
FROM stgArkonaSDPTECH t
LEFT JOIN edwEmployeeDim e ON e.storecode = t.stco#
  AND e.employeenumber = t.stpyemp#
WHERE technumber IS NOT NULL 
ORDER BY name



SELECT pttech, MIN(ptdate), MAX(ptdate), COUNT(*)
FROM zalltheros
WHERE pttech IN ('17','28')
GROUP BY pttech

-- 5/9
-- going with dimTech
-- need to populate AND maintain
-- base ON matching store/employeenumber to edwEmployeeDim 
-- AND of course, the ongoing question of how much history to dummy up
-- CASE IN point, nick fetsch
SELECT storecode, name, employeekey, employeenumber, technumber, t.*
FROM stgArkonaSDPTECH t
LEFT JOIN edwEmployeeDim e ON e.storecode = t.stco#
  AND e.employeenumber = t.stpyemp#
WHERE stpyemp# = '24410'
ORDER BY name 
-- BETWEEN 7/09 AND 2/10: 333
-- BETWEEN 11/10 AND 12/10: 51
SELECT pttech, MIN(ptdate), MAX(ptdate), COUNT(*)
-- SELECT *
FROM zalltheros 
WHERE pttech IN ('17','28')
GROUP BY pttech

-- edwEmployeeDim shows no interim term, a single empkey
SELECT *
FROM edwEmployeeDim
WHERE employeenumber = '24410'

-- nothing FROM before 10/11
SELECT yearmonth, SUM(clockhours)
FROM edwClockHoursFact f
LEFT JOIN day d ON f.datekey = d.datekey
WHERE employeekey in (
  select employeekey
  FROM edwEmployeeDim
  WHERE employeenumber = '24410')
GROUP BY yearmonth

-- uh oh, how did i lose his clockin data FROM 2009?
-- 5/10
-- fuckit, the straw was ark now shows orig AND last hire date AS 11/8/10
-- edwEmployeeDim shows the 7/09 hire date
-- history IS going to be fucked up, just that simple,
-- dimTech IS NOT the place to fix that
-- yet another extension of the problem reconciling hiredate/termdate AND clockhours
-- so,  
-- employeenum with multiple tech numbers
-- IS this a problem, why?
SELECT *
FROM stgArkonaSDPTECH
WHERE stpyemp# IN (
  SELECT stpyemp#
  FROM (
    SELECT stpyemp#, sttech
    FROM stgArkonaSDPTECH
    WHERE stpyemp# <> ''
    GROUP BY stpyemp#, sttech) x
  GROUP BY stpyemp# 
  HAVING COUNT(*) > 1)
ORDER BY stpyemp#  

-- chad gardner emp# 15105 bs tech# 251, mech tech# 521
-- WHERE & how IS dimTech to be used
-- 1. generate flag hours, that's it, for now at least
-- DO i really only care about stgArkSDPTECH records with a value for stpyemp#?
-- that's the assumption i'm going to use,
-- what about labor sales, tech 103 (pdq)

-- would we ever look at anything based ON a generic tech#
-- fuck, greg says yes


-- ALL the tech#s FROM  ros
SELECT pttech, COUNT(*)
FROM zalltheros
WHERE pttech <> ''
GROUP BY pttech

-- first, clean up

SELECT yearmonth, SUM(clockhours)
FROM stgArkonaSDPTECH
GROUP BY yearmonth
ORDER BY stname


/*   5/23   */
SELECT ptco#, pttech, min(opendate) AS minDate, max(opendate) AS maxDate, COUNT(*)
INTO #allTech
FROM zalltheros
WHERE pttech <> ''
GROUP BY ptco#, pttech

-- this IS good
-- but returns mult for some, sdptech:edwEmployeeDim = 1:M
SELECT distinct a.*, t.*, e.name, e.storecode
FROM #allTech a
LEFT JOIN stgArkonaSDPTECH t ON a.ptco# = t.stco# AND a.pttech = t.sttech
LEFT JOIN edwEmployeeDim e ON t.stpyemp# = e.employeenumber
ORDER BY pttech

SELECT ptco#, pttech
FROM (
  SELECT distinct a.*, t.*, e.name
  FROM #allTech a
  LEFT JOIN stgArkonaSDPTECH t ON a.ptco# = t.stco# AND a.pttech = t.sttech
  LEFT JOIN edwEmployeeDim e ON t.stpyemp# = e.employeenumber) x
GROUP BY ptco#, pttech
HAVING COUNT(*) > 1

SELECT *
FROM #alltech
WHERE pttech IN ('20','616')

SELECT *
FROM (
  SELECT distinct a.*, t.*, e.name, e.storecode
  FROM #allTech a
  LEFT JOIN stgArkonaSDPTECH t ON a.ptco# = t.stco# AND a.pttech = t.sttech
  LEFT JOIN edwEmployeeDim e ON t.stpyemp# = e.employeenumber) x
WHERE pttech IN ('20','616')
  
SELECT *
FROM zalltheros
WHERE pttech LIKE 'm%'

-- so WHERE this LEFT off IS, given tech#, which employeekey IS relevant

-- does this help
-- edwEmployeeDim nk: Emp# & RowFromTS
SELECT employeenumber, coalesce(rowfromts, now())
FROM edwEmployeeDim
GROUP BY employeenumber, coalesce(rowfromts, now())
HAVING COUNT(*) > 1

/*  5/24  */
-- maximum number of techs per line, looks LIKE 4
SELECT ptco#, ptro#, ptline, COUNT(*)
FROM (
  SELECT ptco#, ptro#, ptline, pttech
  FROM zalltheros
  WHERE pttech <> ''
  GROUP BY ptco#, ptro#, ptline, pttech) x
GROUP BY ptco#, ptro#, ptline
ORDER BY COUNT(*) DESC
-- unique combination of techs/line
-- this IS the tech GROUP
-- 16001006 line 4: 501/595

-- build the tech groups
-- DON'T GET MINDFUCKED BY EST TIME
SELECT a.ptco#, a.ptro#, a.ptline, a.pttech, a.ptlhrs
INTO #wtf
FROM (
  SELECT ptco#, ptro#, ptline, pttech, SUM(ptlhrs) AS ptlhrs
  FROM zalltheros
  WHERE ptltyp = 'L'
    AND ptcode = 'TT'
    AND ptlhrs <> 0
  GROUP BY ptco#, ptro#, ptline, pttech) a  
INNER JOIN (
    SELECT ptco#, ptro#, ptline, pttech, SUM(ptlhrs) AS ptlhrs
    FROM zalltheros
    WHERE ptltyp = 'L'
      AND ptcode = 'TT'
      AND ptlhrs <> 0
    GROUP BY ptco#, ptro#, ptline, pttech) b ON a.ptco# = b.ptco#
  AND a.ptro# = b.ptro#
  AND a.ptline = b.ptline
  AND a.pttech <> b.pttech   

SELECT * FROM #wtf
-- generate the weighting factor
SELECT a.ptco#, a.ptro#, a.ptline, a.total, b.pttech, b.ind, round((b.ind/a.total), 2) AS weight
FROM (
  SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS total
  FROM #wtf
  GROUP BY ptco#, ptro#, ptline) a
LEFT JOIN ( 
    SELECT ptco#, ptro#, ptline, pttech, SUM(ptlhrs) AS ind
    FROM #wtf
    GROUP BY ptco#, ptro#, ptline, pttech) b ON a.ptco# = b.ptco#
  AND a.ptro# = b.ptro#
  AND a.ptline = b.ptline  
WHERE total <> 0 

-- SUM(weight) should ALL be one
-- but of course IS NOT, eg total = 6, 3 ind @ 2 each, etc

-- flag hours in fact table IS per line, NOT per tech

SELECT DISTINCT technumber FROM edwEmployeeDim WHERE currentrow = true AND activecode = 'a'
-- these are the dist codes for for service
SELECT storecode, distcode, COUNT(*)
FROM edwEmployeeDim
WHERE currentrow = true 
  AND activecode = 'a'
  AND pydeptcode = '04'
GROUP BY storecode, distcode  

/*         CREATE & populate dimTech   */

/*   5/25  CREATE & populate dimTech   */
SELECT ptco#, pttech, min(ptdate) AS minDate, max(ptdate) AS maxDate, COUNT(*)
INTO #allTech
FROM zalltheros
WHERE pttech <> ''
GROUP BY ptco#, pttech

SELECT * FROM #alltech

SELECT distinct a.*, t.*, e.name, e.storecode
FROM #allTech a
LEFT JOIN stgArkonaSDPTECH t ON a.ptco# = t.stco# AND a.pttech = t.sttech
LEFT JOIN edwEmployeeDim e ON t.stpyemp# = e.employeenumber
ORDER BY pttech


INSERT INTO dimTech (storecode, store, employeenumber, name, description, 
  technumber, laborcost)
-- SELECT distinct a.*, t.*, e.name, e.storecode
SELECT distinct a.ptco#, 
  CASE a.ptco#
    WHEN 'ry1' THEN 'Rydells'
    WHEN 'ry2' THEN 'Honda'
    WHEN 'ry3' THEN 'Crookston'
  END, 
  e.employeenumber, e.name, t.stname, t.sttech, stlrat
FROM #allTech a
LEFT JOIN stgArkonaSDPTECH t ON a.ptco# = t.stco# AND a.pttech = t.sttech
LEFT JOIN edwEmployeeDim e ON t.stpyemp# = e.employeenumber
WHERE stname <> ''
-- shit, this doesn't include records FROM sdptech that aren't yet IN zalltheros
-- phase 2
INSERT INTO dimTech (storecode, store, employeenumber, name, description, 
  technumber, laborcost)
SELECT distinct t.stco#, 
  CASE t.stco#
    WHEN 'ry1' THEN 'Rydells'
    WHEN 'ry2' THEN 'Honda'
    WHEN 'ry3' THEN 'Crookston'
  END,
  e.employeenumber, e.name, t.stname, t.sttech, t.stlrat
FROM stgArkonaSDPTECH t
LEFT JOIN edwEmployeeDim e ON t.stpyemp# = e.employeenumber
WHERE NOT EXISTS (
    SELECT 1
    FROM dimTech
    WHERE technumber = t.sttech) 
  AND stname <> ''    

-- AND now the clean up
-- dup store/tech#  
SELECT *
FROM dimTech a
INNER JOIN (
    SELECT storecode, technumber
    FROM dimtech
    GROUP BY storecode, technumber
    HAVING COUNT(*) > 1) b ON a.storecode = b.storecode
  AND a.technumber = b.technumber  
  
DELETE FROM dimtech
WHERE techkey IN (147,144)  

 


-- 2 tech records per employeenumber
SELECT a.*
FROM dimtech a
INNER JOIN ( 
    SELECT name, employeenumber
    FROM dimtech
    WHERE name IS not NULL 
      AND employeenumber IS NOT NULL 
    GROUP BY name, employeenumber
    HAVING COUNT(*) > 1) b ON a.name = b.name
  AND a.employeenumber = b.employeenumber    
ORDER BY a.name  
-- 5/25 just need to figure out what to DO with these AND good to go
-- 5/26 conformed attributes empl#  dimTech:dimEmp has to be 1:1 ??

/****  active field  - actually appears to be of no real use ****/

-- so that means i can't have ???
-- 24410: fetsch 2 periods of time emp# reuse, each with a separate tech #, 17, 28
-- 150105: chad gardner bs tech# AND mr tech#
-- 184878: bo lemer, the M one looks fucked up, DESC i dustin hettervig
-- 218010: pantalion 2 periods of time emp# reuse, each with separate tech# 21, 22

-- 1. 184878: bo lemer, the M one looks fucked up, DESC i dustin hettervig
SELECT * FROM dimtech WHERE name LIKE 'lemer%'
SELECT * FROM zalltheros WHERE pttech = 'm17'
SELECT * FROM edwEmployeeDim WHERE name LIKE 'hetter%'
SELECT * FROM dimTech
WHERE name LIKE 'lemer%'
CONCLUSION
temporary:
  remove M17 FROM dimTech
  replace ALL instances of M17 in ros with 506

DELETE FROM dimTechGroup
WHERE TechGroupKey = (
  SELECT TechGroupKey
  FROM bridgeTechGroup
  WHERE TechKey = (
    SELECT TechKey
    FROM dimTech
    WHERE technumber = 'M17'));  
DELETE FROM bridgeTechGroup
WHERE TechKey = (   
  SELECT TechKey
  FROM dimTech
  WHERE technumber = 'M17'); 
DELETE FROM dimTech
WHERE technumber = 'M17';   

-- 184878: bo lemer, the M one looks fucked up, DESC i dustin hettervig

-- 2. 218010: pantalion 2 periods of time emp# reuse, each with separate tech# 21, 22
SELECT * FROM dimtech WHERE technumber IN ('21','22')

SELECT 
  CASE WHEN pttech = '21' then min(ptdate) end as Min21,
  CASE WHEN pttech = '21' then max(ptdate) end as Max21,
  CASE WHEN pttech = '22' then min(ptdate) end as Min22,
  CASE WHEN pttech = '22' then max(ptdate) end as MAx22
FROM zalltheros 
WHERE pttech IN ('21','22')
GROUP BY pttech

SELECT pttech,
  SUM(CASE WHEN pttech = '21' THEN 1 ELSE 0 END) AS t21,
  SUM(CASE WHEN pttech = '22' THEN 1 ELSE 0 END) AS t22
FROM zalltheros 
WHERE pttech IN ('21','22')
GROUP BY pttech

CONCLUSION
temporary
  remove tech 21 FROM dimtech
  replace ALL instances of tech# 21 IN ros with 22 
DELETE FROM dimTechGroup
WHERE TechGroupKey = (
  SELECT TechGroupKey
  FROM bridgeTechGroup
  WHERE TechKey = (
    SELECT TechKey
    FROM dimTech
    WHERE technumber = '21'));  
DELETE FROM bridgeTechGroup
WHERE TechKey = (   
  SELECT TechKey
  FROM dimTech
  WHERE technumber = '21'); 
DELETE FROM dimTech
WHERE technumber = '21';    
-- 218010: pantalion 2 periods of time emp# reuse, each with separate tech# 21, 22

-- 3. 150105: chad gardner bs tech# AND mr tech# ----------------------------------
/*
attempt to utilize tech default service type to allow multiple tech#/emp#
need to match the service type
1. more service types IN ros than EXISTS IN techs: CM, CW & EM
2.tech TABLE IS NOT consistent OR correct. tech 625 (Stryker) NOT categorized
                                           tech 623 (Swift) IS SD but IS categorized AS BS
so, that won't WORK
what about type 2 
  nope, date IS NOT an accurate separation, 251 & 521 overlap
    SELECT 
      CASE WHEN pttech = '251' then min(ptdate) end as Min251,
      CASE WHEN pttech = '251' then max(ptdate) end as Max251,
      CASE WHEN pttech = '521' then min(ptdate) end as Min521,
      CASE WHEN pttech = '521' then max(ptdate) end as MAx521
    FROM zalltheros 
    WHERE pttech IN ('251','521')
    GROUP BY pttech
    -- days with flag hours for 251 & 521
    SELECT a.pttech, a.hrs, a.ptdate, b.pttech, b.hrs
    FROM (
      SELECT pttech, ptdate, SUM(ptlhrs) AS hrs
      FROM zalltheros
      WHERE pttech = '251'
      GROUP BY pttech, ptdate) a
    INNER JOIN (  
      SELECT pttech, ptdate, SUM(ptlhrs) AS hrs
      FROM zalltheros
      WHERE pttech = '521'
      GROUP BY pttech, ptdate) b ON a.ptdate = b.ptdate
    ORDER BY a.ptdate        
SELECT * FROM dimtech WHERE employeenumber = '150105'                                               
*/
CONCLUSION
temporary: 
  remove tech 521 FROM dimtech
  replace ALL instances of tech# 521 IN ros with 251
DELETE FROM dimTechGroup
WHERE TechGroupKey = (
  SELECT TechGroupKey
  FROM bridgeTechGroup
  WHERE TechKey = (
    SELECT TechKey
    FROM dimTech
    WHERE technumber = '521'));  
DELETE FROM bridgeTechGroup
WHERE TechKey = (   
  SELECT TechKey
  FROM dimTech
  WHERE technumber = '521'); 
DELETE FROM dimTech
WHERE technumber = '521';      
-- 150105: chad gardner bs tech# AND mr tech# ----------------------------------

-- 4. 24410: fetsch 2 periods of time emp# reuse, each with a separate tech #, 17, 28
SELECT * FROM dimtech WHERE technumber IN ('17','28')

SELECT 
  CASE WHEN pttech = '17' then min(ptdate) end as Min17,
  CASE WHEN pttech = '17' then max(ptdate) end as Max17,
  CASE WHEN pttech = '28' then min(ptdate) end as Min28,
  CASE WHEN pttech = '28' then max(ptdate) end as MAx28
FROM zalltheros 
WHERE pttech IN ('17','28')
GROUP BY pttech

SELECT pttech,
  SUM(CASE WHEN pttech = '17' THEN 1 ELSE 0 END) AS t17,
  SUM(CASE WHEN pttech = '28' THEN 1 ELSE 0 END) AS t29
FROM zalltheros 
WHERE pttech IN ('17','28')
GROUP BY pttech
CONCLUSION
temporary: 
  remove tech 28 FROM dimtech
  replace ALL instances of tech# 28 IN ros with 17
DELETE FROM dimTechGroup
WHERE TechGroupKey = (
  SELECT TechGroupKey
  FROM bridgeTechGroup
  WHERE TechKey = (
    SELECT TechKey
    FROM dimTech
    WHERE technumber = '28'));  
DELETE FROM bridgeTechGroup
WHERE TechKey = (   
  SELECT TechKey
  FROM dimTech
  WHERE technumber = '28'); 
DELETE FROM dimTech
WHERE technumber = '28';      
-- 24410: fetsch 2 periods of time emp# reuse, each with a separate tech #, 17, 28

-- techs IN sdptech NOT ON any ro
-- these tech#s only appear ON preconversion ros  
SELECT *
FROM stgArkonaSDPTECH t
WHERE NOT EXISTS (
  SELECT 1
  FROM dimTech
  WHERE technumber = t.sttech)
SELECT *
FROM zalltheros
WHERE pttech IN ('m16', '127')  
  
SELECT *
FROM stgArkonaSDPRDET
WHERE pttech IN ('m16', '127')
  
CONCLUSION remove techs m16, 127 FROM dimtech
DELETE FROM dimTechGroup
WHERE TechGroupKey = (
  SELECT TechGroupKey
  FROM bridgeTechGroup
  WHERE TechKey = (
    SELECT TechKey
    FROM dimTech
    WHERE technumber IN ('m16', '127')));  
DELETE FROM bridgeTechGroup
WHERE TechKey = (   
  SELECT TechKey
  FROM dimTech
  WHERE technumber IN ('m16', '127')); 
DELETE FROM dimTech
WHERE technumber IN ('m16', '127');   
-- techs IN sdptech NOT ON any ro

-- tech#s NOT IN dimTech
-- only XXX
-- tech XXX ON PDET WHERE ptltyp = P OR SDET WHERE ptline = 900
SELECT DISTINCT z.pttech
FROM zalltheros z
LEFT JOIN dimTech t ON z.pttech = t.technumber
WHERE z.pttech <> ''
  AND t.technumber IS NULL
  
SELECT * FROM zalltheros WHERE pttech = 'xxx'  
CONCLUSION  DO nothing
-- tech#s NOT IN dimTech


-- DO nullable fields, indexes
-- unique index: storecode/technumber 
SELECT storecode, technumber
FROM dimTech
GROUP BY storecode, technumber
HAVING COUNT(*) > 1
  
CREATE TABLE bridgeTechGroup (
  TechGroupKey autoinc, 
  TechKey integer,
  WeightFactor double,

INSERT INTO bridgeTechGroup (TechKey, WeightFactor, NumberOfTechs)
SELECT techkey, 1, 1
FROM dimTech;
  
CREATE TABLE dimTechGroup (
  TechGroupKey integer) IN database; 
   
INSERT INTO dimTechGroup
SELECT TechGroupKey
FROM bridgeTechGroup;  

-- 5/26, ready to load up the bridge

---------- 6/3/12 ---------------------
-- techgroup done elsewhere: etl - bridgeTechGroup4
-- back to the focus of nightly dimTech updates
-- moved code FROM ZapReload to ServiceLine
-- will still DO a zap AND reload, but at the END of the proc, xfmDimTech

-- store/tech# unique IN both tables
SELECT stco#, sttech
FROM stgArkonaSDPTECH
GROUP BY stco#, sttech
HAVING COUNT(*) > 1

SELECT storecode, technumber
FROM dimtech
GROUP BY storecode, technumber
HAVING COUNT(*) > 1

-- potentially legitimate new tech records
-- exclusions pertain to the initial historical feed
SELECT *
FROM stgArkonaSDPTECH a
LEFT JOIN dimtech b ON a.stco# = b.storecode
  AND a.sttech = b.technumber
WHERE b.storecode IS NULL 
  AND a.sttech NOT IN ('M16','M17','21','521','28', '127');
  
  
SELECT a.*, c.*
FROM stgArkonaSDPTECH a
LEFT JOIN dimtech b ON a.stco# = b.storecode
  AND a.sttech = b.technumber
LEFT JOIN edwEmployeeDim c ON a.stpyemp# = c.employeenumber  
WHERE b.storecode IS NULL -- NOT IN dimTech
  AND a.sttech NOT IN ('M16','M17','21','521','28', '127')  
  AND c.storecode IS NOT NULL 
---------- 6/3/12 ---------------------


