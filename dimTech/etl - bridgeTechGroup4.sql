/* 5/31,  */
/*
ALL the pieces
1. zalltheros
   issues
     no A line etl-SLF2
     mult paymethods per A line etl-SLF2
2. zro
3. zlin3
4. ztech
use techkey instead of tech# 
*/   
SELECT * FROM #weight
DROP TABLE #weight;
SELECT l.ptco#, l.ptro#, l.ptline, l.ptlhrs AS LineHours,  
  t.pttech, t.ptlhrs AS FlagHours, t.FlagDate, round(t.ptlhrs/l.ptlhrs, 2) AS weight
INTO #weight  
FROM zline l
LEFT JOIN ztech t ON l.ptco# = t.ptco#
  AND l.ptro# = t.ptro#
  AND l.ptline = t.ptline
WHERE l.ptlhrs <> 0;
  
-- need to include ptco# because this IS using pttech, NOT techkey
DROP TABLE zkey;
CREATE TABLE zkey (
  theKey autoinc,
  pttech cichar(3),
  weight double,
  ptco# cichar(3));
  
INSERT INTO zkey (ptco#, pttech, weight)  
SELECT ptco#, pttech, weight
FROM (
  SELECT DISTINCT ptco#, ptro#, ptline, pttech, flagdate, weight
  FROM #weight) a
GROUP BY ptco#, pttech, weight;  

SELECT * FROM zkey
SELECT COUNT(*) FROM zkey

EXECUTE PROCEDURE sp_zaptable('bridgeTechGroup');
INSERT INTO bridgeTechGroup 
SELECT k.thekey, t.techkey, w.weight, w.ptco#
FROM #weight w
LEFT JOIN zkey k ON w.ptco# = k.ptco# -- added ptco# to join
  AND w.pttech = k.pttech
  AND w.weight = k.weight
LEFT JOIN dimTech t ON w.ptco# = t.storecode
  AND w.pttech = t.technumber 
GROUP BY k.thekey, t.techkey, w.weight, w.ptco#
  
-- 5/30 ok, now that tgkey IS done
DROP TABLE #jon;
SELECT a.ptco#, a.ptro#, a.ptline, b.pttech, b.FlagDate, a.LineHours, b.TechLineHours, 
  round(b.TechLineHours/a.LineHours, 2)as weight,
  c.techkey, d.*
INTO #jon  
FROM (
  SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS LineHours 
  FROM zline 
  WHERE ptlhrs <> 0
--    AND LEFT(ptro#, 5) = '16088'
  GROUP BY ptco#, ptro#, ptline) a
LEFT JOIN (
    SELECT ptco#, ptro#, ptline, pttech, FlagDate, SUM(ptlhrs) AS TechLineHours
    from ztech 
    WHERE ptlhrs <> 0
--      AND LEFT(ptro#, 5) = '16088'
    GROUP BY ptco#, ptro#, ptline, pttech, FlagDate) b ON a.ptco# = b.ptco#
  AND a.ptro# = b.ptro#
  AND a.ptline = b.ptline
LEFT JOIN dimTech c ON b.ptco# = c.storecode
  AND b.pttech = c.technumber 
LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
  AND round(b.TechLineHours/a.LineHours, 2) = weightfactor 

-- 5/31  
-- NULL techgroupkey : none
SELECT *
FROM #jon
WHERE techgroupkey IS NULL   

SELECT COUNT(*) FROM #jon
-- unique: ptco#, ptro#, ptline, flagdate, linehours, techgroupkey
SELECT ptco#, ptro#, ptline, flagdate, techgroupkey
FROM #jon
GROUP BY ptco#, ptro#, ptline, flagdate, techgroupkey
HAVING COUNT(*) > 1

select ptco#, ptro#, ptline, flagdate, linehours, techgroupkey
FROM #jon

DROP TABLE zFlagHours;
CREATE TABLE zFlagHours (
  ptco# cichar(3),
  ptro# cichar(9),
  ptline integer, 
  flagDateKey integer,
  lineHours double,
  techGroupKey integer);
INSERT INTO zFlagHours
select ptco#, ptro#, ptline, (select datekey from day where thedate = flagdate), linehours, techgroupkey
FROM #jon;  

-- rename it to brdgTechFlagHoursGroup

SELECT t.technumber, t.name, round(SUM(f.linehours*g.weightfactor), 2) AS flaghours
FROM zflaghours f
INNER JOIN day d ON f.flagdatekey = d.datekey
INNER JOIN bridgeTechGroup g ON f.techgroupkey = g.techgroupkey
INNER JOIN dimtech t ON g.techkey = t.techkey
WHERE d.thedate BETWEEN '04/01/2012' AND '04/30/2012'
  AND (technumber LIKE '5%' or technumber like '6%')
  AND f.ptco# = 'ry1'
GROUP BY t.technumber, t.name


-- what dimEmployee info based ON what period of time
-- need employeekey to drill accross to clockhours

-- flaghourfact>---dimTechGroup--<bridgeTechGroup>----dimTech-|-------|-dimEmp>---ClockHoursFact
drilling accross dimensions, based ON co/emp#, but the 2 fact tables need to be constrained ON the same dates
those dates will tell me what employeekey to use
store/emp#/RowFromTS

SELECT storecode, employeenumber, rowfromts
FROM edwEmployeeDim
group by storecode, employeenumber, rowfromts
HAVING COUNT(*) > 1

SELECT f.ptco#, t.employeenumber, t.technumber, t.name, round(SUM(f.linehours*g.weightfactor), 2) AS flaghours
FROM zflaghours f
INNER JOIN day d ON f.flagdatekey = d.datekey
INNER JOIN bridgeTechGroup g ON f.techgroupkey = g.techgroupkey
INNER JOIN dimtech t ON g.techkey = t.techkey
WHERE d.thedate BETWEEN '04/01/2012' AND '04/30/2012'
  AND f.ptco# = 'ry1'
  AND (technumber LIKE '5%' or technumber like '6%')
  AND t.employeenumber IS NOT NULL 
GROUP BY f.ptco#, t.employeenumber, t.technumber, t.name

an employees clockhours regardless of the number of empkeys

SELECT d.thedate, e.employeenumber, sum(f.ClockHours) 
FROM edwClockHoursFact f
INNER JOIN day d ON f.datekey = d.datekey
INNER JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
  AND e.storecode = 'ry1'
  AND e.distcode = 'STEC'
WHERE d.thedate BETWEEN '04/01/2012' AND '04/30/2012'
GROUP BY d.thedate, e.employeenumber

SELECT a.employeenumber, a.technumber, a.name, a.flaghours, b.clockhours,
  round(a.flaghours/b.clockhours, 2) AS prof
FROM (
  SELECT f.ptco#, t.employeenumber, t.technumber, t.name, round(100*(SUM(f.linehours*g.weightfactor)), 2) AS flaghours
  FROM zflaghours f
  INNER JOIN day d ON f.flagdatekey = d.datekey
  INNER JOIN bridgeTechGroup g ON f.techgroupkey = g.techgroupkey
  INNER JOIN dimtech t ON g.techkey = t.techkey
  WHERE d.thedate BETWEEN '04/01/2012' AND '04/30/2012'
    AND f.ptco# = 'ry1'
    AND (technumber LIKE '5%' or technumber like '6%')
    AND t.employeenumber IS NOT NULL 
  GROUP BY f.ptco#, t.employeenumber, t.technumber, t.name) a
LEFT JOIN (
    SELECT e.storecode, e.employeenumber, sum(f.ClockHours) AS clockhours 
    FROM edwClockHoursFact f
    INNER JOIN day d ON f.datekey = d.datekey
    INNER JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
      AND e.storecode = 'ry1'
      AND e.distcode = 'STEC'
    WHERE d.thedate BETWEEN '04/01/2012' AND '04/30/2012'
    GROUP BY e.storecode, e.employeenumber) b ON a.ptco# = b.storecode 
  AND a.employeenumber = b.employeenumber
ORDER BY technumber 


-- without the JOIN -- may need this style for daily, WHERE some clock days will be 0, some flag days will be 0
-- daily for an interval
SELECT a.thedate, b.employeenumber, b.technumber, b.name, coalesce(b.flaghours, 0) AS flaghours, 
  coalesce(c.clockhours) AS clockhours,
  case when c.clockhours <> 0 then round(100*b.flaghours/c.clockhours, 2) ELSE 0 END AS prof
FROM (
  SELECT thedate, datekey
  FROM day
  WHERE thedate BETWEEN '04/01/2012' AND '04/30/2012') a,
  (
  SELECT f.ptco#, t.employeenumber, t.technumber, t.name, f.flagdatekey, round(SUM(f.linehours*g.weightfactor), 2) AS flaghours
  FROM zflaghours f
  INNER JOIN bridgeTechGroup g ON f.techgroupkey = g.techgroupkey
  INNER JOIN dimtech t ON g.techkey = t.techkey
  WHERE f.ptco# = 'ry1'
    AND t.employeenumber IS NOT NULL 
  GROUP BY f.ptco#, t.employeenumber, t.technumber, t.name, f.flagdatekey) b,
 (
    SELECT e.storecode, e.employeenumber, f.datekey, sum(f.ClockHours) AS clockhours 
    -- SELECT *
    FROM edwClockHoursFact f
    INNER JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
      AND e.storecode = 'ry1'
      AND e.distcode in ('STEC', 'SRVM') -- srvm: reed
    GROUP BY e.storecode, e.employeenumber, f.datekey) c
WHERE b.flagdatekey = a.datekey 
  AND c.datekey = a.datekey
  AND b.ptco# = c.storecode
  AND b.employeenumber = c.employeenumber
  AND b.employeenumber = '1106399'
        
-- SundayToSaturdayWeek
SELECT min(a.thedate), max(a.thedate), b.employeenumber, b.technumber, b.name, 
  sum(b.flaghours) AS flaghours, sum(c.clockhours) AS clockhours,
  round(SUM(b.flaghours)/SUM(c.clockhours), 2) AS prof
FROM (
  SELECT thedate, datekey, SundayToSaturdayWeek
  FROM day
  WHERE thedate BETWEEN '04/01/2012' AND '04/28/2012') a,
  (
  SELECT f.ptco#, t.employeenumber, t.technumber, t.name, f.flagdatekey, round(SUM(f.linehours*g.weightfactor), 2) AS flaghours
  FROM zflaghours f
  INNER JOIN bridgeTechGroup g ON f.techgroupkey = g.techgroupkey
  INNER JOIN dimtech t ON g.techkey = t.techkey
  WHERE f.ptco# = 'ry1'
    AND t.employeenumber IS NOT NULL 
  GROUP BY f.ptco#, t.employeenumber, t.technumber, t.name, f.flagdatekey) b,
  (
    SELECT e.storecode, e.employeenumber, f.datekey, sum(f.ClockHours) AS clockhours 
    FROM edwClockHoursFact f
    INNER JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
      AND e.storecode = 'ry1'
      AND e.distcode in ('STEC', 'SRVM') -- srvm: reed
    GROUP BY e.storecode, e.employeenumber, f.datekey) c
WHERE a.datekey = b.flagdatekey
  AND a.datekey = c.datekey
  AND b.ptco# = c.storecode
  AND b.employeenumber = c.employeenumber  
GROUP BY a.SundayToSaturdayWeek, b.employeenumber, b.technumber, b.name  
ORDER BY technumber      

-- 6/1
find a day with 0 flag hours
SELECT *
FROM zflaghours

SELECT *
FROM day d
WHERE thedate BETWEEN '03/01/2012' AND '04/30/2012'
AND NOT EXISTS (
  SELECT 1
  FROM zflaghours
  WHERE flagdatekey = d.datekey)



-- 3 dups
-- employee # with multiple distcode WHERE at least one of them IS STEC
SELECT storecode, employeenumber
FROM (
SELECT storecode, employeenumber, distcode
FROM edwEmployeeDim e
WHERE EXISTS (
  SELECT 1
  FROM edwEmployeeDim 
  WHERE employeenumber = e.employeenumber
    AND distcode = 'STEC')
GROUP BY storecode, employeenumber, distcode) a
GROUP BY storecode, employeenumber
HAVING COUNT(*) > 1

SELECT *
FROM edwEmployeeDim
WHERE employeenumber IN ('1150400','1150105','187800')


  
  

/******************************************************************************/
/******************************************************************************/
ptco# should NOT be IN bridgeTG, because it IS using techKey
/******************************************************************************/
/******************************************************************************/




