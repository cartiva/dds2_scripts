--5/3 looks ok against 5/2 backup (locally)
-- ALL inactive technumbers
-- 5/7 looks ok took 6 minutes to run-------------------------------------------------------------------------------------------
-- 5/21 replaced tmpSDPTECH with stgArkonaSDPTECH
-- run it ALL against \dailycut\dds\monday\copy
-------------------------- prelim ---------------------------------------------------------
ALTER TABLE keyMapDimTech
ADD COLUMN EmployeeNumber cichar(7);
-- for consistency with default service type BETWEEN techs AND writers
UPDATE dimtech
SET flagdeptcode = 'MR'
WHERE flagdeptcode = 'SD';  

-- ok first a crude UPDATE ( i already manually did 620,621)
UPDATE a
  SET active = CASE WHEN b.stactv = 'n' THEN false ELSE true END  
-- SELECT a.techkey, a.storecode, a.technumber, a.active, a.laborcost, a.flagdeptcode, b.stactv
FROM dimtech a
LEFT JOIN stgArkonaSDPTECH b ON a.storecode = b.stco# AND a.technumber = b.sttech 
WHERE a.storecode <> 'ry3' 
  AND a.technumber NOT IN ('620','621');
  
------- get ALL the dates --------------------------------------
--DROP TABLE #ark;
SELECT ptco#, pttech, MIN(ptdate) AS minArk, MAX(ptdate) AS maxArk
INTO #ark
FROM stgArkonaSDPRDET
WHERE pttech <> ''
GROUP BY ptco#, pttech;

SELECT a.techkey, MIN(b.thedate) AS minDay, MAX(b.thedate) AS maxDay
INTO #techDay
FROM factTechFlagHoursByDay a
LEFT JOIN day b ON a.flagdatekey = b.datekey
GROUP BY techkey;

SELECT c.techkey, MIN(thedate) AS minLine, MAX(thedate) AS maxLine
INTO #techLine
FROM factTechLineFlagDateHours a
LEFT JOIN day b ON a.flagdatekey = b.datekey
LEFT JOIN bridgetechgroup c ON a.techgroupkey = c.techgroupkey
GROUP BY techkey;

--DROP TABLE #allDates;
SELECT a.techkey, a.storecode, a.technumber, a.name, a.description, a.employeenumber, 
  a.flagdept, a.active, a.techkeyfromdate, a.techkeythrudate,
  b.minArk, b.maxArk, c.minDay, c.maxDay, d.minLine, d.maxLine,
  e.hiredate, e.termdate
INTO #allDates  
FROM dimtech a
LEFT JOIN #ark b ON a.storecode = b.ptco#
  AND a.technumber = b.pttech
LEFT JOIN #techday c ON a.techkey = c.techkey 
LEFT JOIN #techLine d ON a.techkey = d.techkey
LEFT JOIN edwEmployeeDim e ON a.storecode = e.storecode
  AND a.employeenumber = e.employeenumber
  AND e.currentrow = true
WHERE a.storecode <> 'ry3';  
-------/ get ALL the dates --------------------------------------
------- get rid of ALL inactive tech keys that have never flagged hours------

--SELECT parent FROM system.columns WHERE name = 'techkey';  
--SELECT parent FROM system.columns WHERE name = 'techgroupkey'; 
--need to generate a list of these technumbers to exclude FROM nightly scrape
DELETE 
FROM factTechLineFlagDateHours
WHERE techgroupkey IN (
  SELECT techgroupkey
  FROM bridgetechgroup
  WHERE techkey IN (
    SELECT techkey
    FROM #allDates
    WHERE minArk IS NULL 
      AND active = false));
DELETE FROM keymapbridgetechgroup
WHERE techkey IN (
  SELECT techkey
  FROM #allDates
  WHERE minArk IS NULL 
    AND active = false);
DELETE FROM keyMapDimTech
WHERE techkey IN (
  SELECT techkey
  FROM #allDates
  WHERE minArk IS NULL 
    AND active = false);
DELETE FROM bridgeTechGroup
WHERE techkey IN (
  SELECT techkey
  FROM #allDates
  WHERE minArk IS NULL 
    AND active = false);
DELETE FROM factTechFlagHoursByDay
WHERE techkey IN (
  SELECT techkey
  FROM #allDates
  WHERE minArk IS NULL 
    AND active = false);
DELETE FROM dimTech
WHERE techkey IN (
  SELECT techkey
  FROM #allDates
  WHERE minArk IS NULL 
    AND active = false);           
-------/ get rid of ALL inactive tech keys that have never flagged hours------
--------------------------/ prelim ---------------------------------------------------------

-------------------------- fix current dimTech first --------------------------------------
-- UPDATE IN such a way that there will be no fact rows dependent ON techkey
--   that need to be generated for the new inactive tech keys

-- ALL inactive departmental techs w/MAX date of flaghours -------------------- 
-- DROP TABLE #wtf;
SELECT a.storecode, technumber, a.techkey, d.maxDate AS maxLine
INTO #wtf
FROM dimtech a
LEFT JOIN (
  SELECT c.techkey, MIN(b.thedate) AS minDate, MAX(b.thedate) AS maxDate
  FROM factTechLineFlagDateHours a
  LEFT JOIN day b ON a.flagdatekey = b.datekey
  LEFT JOIN bridgetechgroup c ON a.techgroupkey = c.techgroupkey
  GROUP BY c.techkey) d ON a.techkey = d.techkey  
WHERE a.active = false
  AND a.storecode <> 'ry3'
  AND a.employeenumber = 'na'
  AND a.technumber NOT IN ('620','621','628');

-- this worked  
BEGIN TRANSACTION;
TRY 
  UPDATE b
  SET currentrow = false,
      active = true,
      rowchangedate = curdate(),
      rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
      rowThruTS = now(),
      techkeythrudate = a.maxLine,
      techkeythrudatekey = (SELECT datekey FROM day WHERE thedate = a.maxLine),
      rowchangereason = 'active to inactive'
  FROM #wtf a
  INNER JOIN dimtech b ON a.techkey = b.techkey;    
  
  -- this worked
  INSERT INTO dimtech (storecode, employeenumber, name, description, 
    technumber, active, laborcost, flagdeptcode, flagdept, currentrow,
    rowFromTS, techkeyfromdate, techkeyfromdatekey, techkeythrudate, techkeythrudatekey)
  SELECT a.storecode, a.employeenumber, a.name, a.description, 
    a.technumber, false, a.laborcost, a.flagdeptcode, a.flagdept, true,
    now(),
    b.maxLine + 1, (select datekey from day where thedate = b.maxLine + 1), 
    (SELECT thedate FROM day WHERE datetype <> 'date'),
    (SELECT datekey FROM day WHERE datetype <> 'date')
  FROM dimtech a
  INNER JOIN #wtf b ON a.storecode = b.storecode AND a.technumber = b.technumber;
  
  -- need keymapDimTech
  merge keymapDimTech AS a
  using dimTech AS b ON a.techkey = b.techkey
  WHEN matched THEN
  UPDATE  
  SET a.active = b.active,
      a.flagdeptcode = b.flagdeptcode,
      a.laborcost = b.laborcost,
      a.employeenumber = b.employeenumber
  WHEN NOT matched THEN 
  INSERT values (b.techkey, b.storecode, b.technumber, b.active, b.flagdeptcode, 
    b.laborcost, b.employeenumber);      
  
  -- AND bridgeTechGroup
  INSERT INTO bridgeTechGroup (TechKey, WeightFactor)        
  SELECT a.techkey, b.WeightFactor
  FROM dimtech a
  LEFT JOIN (
    SELECT n/100.0 AS weightfactor
    FROM tally
    WHERE n BETWEEN 1 AND 100) b ON 1 = 1
  LEFT JOIN bridgeTechGroup c ON a.techKey = c.TechKey
  WHERE c.TechKey IS NULL; 
COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE;
END TRY;

--/ ALL inactive departmental techs w/MAX date of flaghours --------------------

-- ALL inactive individual techs w/MAX date of flaghours ----------------------
DROP TABLE #wtf;
SELECT a.storecode, technumber, a.techkey, d.maxDate AS maxLine
INTO #wtf
FROM dimtech a
LEFT JOIN (
  SELECT c.techkey, MIN(b.thedate) AS minDate, MAX(b.thedate) AS maxDate
  FROM factTechLineFlagDateHours a
  LEFT JOIN day b ON a.flagdatekey = b.datekey
  LEFT JOIN bridgetechgroup c ON a.techgroupkey = c.techgroupkey
  GROUP BY c.techkey) d ON a.techkey = d.techkey  
WHERE a.active = false
  AND a.storecode <> 'ry3'
  AND a.employeenumber <> 'na'
  AND a.technumber NOT IN ('620','621','628');
  
BEGIN TRANSACTION;
TRY 
  UPDATE b
  SET currentrow = false,
      active = true,
      rowchangedate = curdate(),
      rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
      rowThruTS = now(),
      techkeythrudate = a.maxLine,
      techkeythrudatekey = (SELECT datekey FROM day WHERE thedate = a.maxLine),
      rowchangereason = 'active to inactive'
  FROM #wtf a
  INNER JOIN dimtech b ON a.techkey = b.techkey;    
  
  -- this worked
  INSERT INTO dimtech (storecode, employeenumber, name, description, 
    technumber, active, laborcost, flagdeptcode, flagdept, currentrow,
    rowFromTS, techkeyfromdate, techkeyfromdatekey, techkeythrudate, techkeythrudatekey)
  SELECT a.storecode, a.employeenumber, a.name, a.description, 
    a.technumber, false, a.laborcost, a.flagdeptcode, a.flagdept, true,
    now(),
    b.maxLine + 1, (select datekey from day where thedate = b.maxLine + 1), 
    (SELECT thedate FROM day WHERE datetype <> 'date'),
    (SELECT datekey FROM day WHERE datetype <> 'date')
  FROM dimtech a
  INNER JOIN #wtf b ON a.storecode = b.storecode AND a.technumber = b.technumber;
  
  -- need keymapDimTech
  merge keymapDimTech AS a
  using dimTech AS b ON a.techkey = b.techkey
  WHEN matched THEN
  UPDATE  
  SET a.active = b.active,
      a.flagdeptcode = b.flagdeptcode,
      a.laborcost = b.laborcost,
      a.employeenumber = b.employeenumber
  WHEN NOT matched THEN 
  INSERT values (b.techkey, b.storecode, b.technumber, b.active, b.flagdeptcode, 
    b.laborcost, b.employeenumber);      
  
  -- AND bridgeTechGroup
  INSERT INTO bridgeTechGroup (TechKey, WeightFactor)        
  SELECT a.techkey, b.WeightFactor
  FROM dimtech a
  LEFT JOIN (
    SELECT n/100.0 AS weightfactor
    FROM tally
    WHERE n BETWEEN 1 AND 100) b ON 1 = 1
  LEFT JOIN bridgeTechGroup c ON a.techKey = c.TechKey
  WHERE c.TechKey IS NULL; 
COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE;
END TRY;  
--/ ALL inactive individual techs w/MAX date of flaghours ----------------------
--------------------------/ fix current dimTech first --------------------------------------
--/ 5/7 looks ok took 6 minutes to run-------------------------------------------------------------------------------------------
-- anomalies ------------------------------------------------------------------------
-- 1. syverson 
DELETE FROM keyMapBridgeTechGroup WHERE techkey = 183;
DELETE FROM keyMapDimTech WHERE techkey = 183;
DELETE FROM dimTechGroup WHERE techgroupkey in (select techgroupkey from bridgeTechGroup where techkey = 183);
DELETE FROM bridgeTechGroup WHERE techkey = 183;
DELETE FROM dimTech WHERE techkey = 183;
-- 2.gulled
UPDATE dimtech
SET rowchangedate = NULL,
    rowchangedatekey = NULL,
    rowthruts = NULL,
    rowchangereason = NULL,
    techkeyfromdate = '01/19/2013',
    techkeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '01/19/2013'),
    techkeythrudate = (SELECT thedate FROM day WHERE datetype <> 'date'),
    techkeythrudatekey = (SELECT datekey FROM day WHERE datetype <> 'date')    
WHERE techkey = 201;
/* 
5/18: these aren't necessary, tech# 215 cleaned up IN ALL inactive individual techs w/MAX date of flaghours
INSERT INTO dimtech (storecode, employeenumber, name, description, 
  technumber, active, laborcost, flagdeptcode, flagdept, currentrow,
  rowFromTS, techkeyfromdate, techkeyfromdatekey, techkeythrudate, techkeythrudatekey)
SELECT storecode, employeenumber, name, description, 
  technumber, false, laborcost, flagdeptcode, flagdept, true,
  now(), '01/19/2013', (select datekey FROM day WHERE thedate = '01/19/2013'),
  (SELECT thedate FROM day WHERE datetype <> 'date'), 
  (SELECT datekey FROM day WHERE datetype <> 'date')
FROM dimtech WHERE techkey = 211;  
  
UPDATE dimTech
SET currentrow = false,
    rowchangedate = curdate(),
    rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
    rowThruTS = now(),
    rowchangereason = 'active to inactive',
    techkeythrudate = '01/18/2013',
    techkeythrudatekey = (SELECT datekey FROM day WHERE thedate = '01/18/2013')    
WHERE techkey = 211;   

merge keymapDimTech AS a
using dimTech AS b ON a.techkey = b.techkey
WHEN matched THEN
UPDATE  
SET a.active = b.active,
    a.flagdeptcode = b.flagdeptcode,
    a.laborcost = b.laborcost,
    a.employeenumber = b.employeenumber
WHEN NOT matched THEN 
INSERT values (b.techkey, b.storecode, b.technumber, b.active, b.flagdeptcode, 
  b.laborcost, b.employeenumber);      

INSERT INTO bridgeTechGroup (TechKey, WeightFactor)        
SELECT a.techkey, b.WeightFactor
FROM dimtech a
LEFT JOIN (
  SELECT n/100.0 AS weightfactor
  FROM tally
  WHERE n BETWEEN 1 AND 100 ) b ON 1 = 1
LEFT JOIN bridgeTechGroup c ON a.techKey = c.TechKey
WHERE c.TechKey IS NULL; 
*/
-- 3. ry3
DELETE FROM dimTechGroup WHERE techgroupkey IN (
  SELECT techgroupkey FROM bridgeTechGroup WHERE techkey IN (
    SELECT techkey FROM dimtech WHERE storecode = 'ry3'));
DELETE FROM factTechLineFlagDateHours WHERE techgroupkey IN (
  SELECT techgroupkey FROM bridgeTechGroup WHERE techkey IN (
    SELECT techkey FROM dimtech WHERE storecode = 'ry3'));  
DELETE FROM keyMapBridgeTechGroup WHERE techgroupkey IN (
  SELECT techgroupkey FROM bridgeTechGroup WHERE techkey IN (
    SELECT techkey FROM dimtech WHERE storecode = 'ry3'));     
DELETE FROM factTechFlagHoursByDay WHERE techkey IN (
    SELECT techkey FROM dimtech WHERE storecode = 'ry3');         
DELETE FROM bridgeTechGroup WHERE techkey IN (SELECT techkey FROM dimtech WHERE storecode = 'ry3');
DELETE FROM keyMapDimTech WHERE techkey IN (SELECT techkey FROM dimtech WHERE storecode = 'ry3');
DELETE FROM dimtech WHERE storecode = 'ry3';

-- 4. Joseph RUD a rehire/reuse
UPDATE dimtech
SET rowthruts = NULL,
    techkeythrudate = (SELECT thedate FROM day WHERE datetype <> 'date'),
    techkeythrudatekey = (SELECT datekey FROM day WHERE datetype <> 'date')
WHERE techkey = 116;

-- 5. rowthruts ON activerows
UPDATE dimtech
SET rowthruts = null
WHERE active = true 
  AND currentrow = true;
  
--/ anomalies -----------------------------------------------------------------------
index ON dimtech:  storecode;technumber;employeenumber;active;flagdeptcode;laborcost
-- new techs ------------------------------------------------------------------------
-- departmental --------------
SELECT *
FROM stgArkonaSDPTECH a
LEFT JOIN dimtech b ON a.stco# = b.storecode AND a.sttech = b.technumber
WHERE b.storecode IS NULL 
  AND a.stactv <> 'n'
  AND a.stco# <> 'ry3'

-- DELETE FROM dimtech WHERE technumber = 'bs1'
DECLARE @store string;
DECLARE @tech string;
@store = 'RY1';
@tech = 'bs4';
INSERT INTO dimtech (storecode, employeenumber, description, technumber, active,
  laborcost, flagDeptCode, flagDept, CurrentRow, techkeyfromdate, 
  techkeyfromdatekey, techkeythrudate, techkeythrudatekey)  
SELECT a.stco#, 'NA', a.stname, a.sttech, true, a.stlrat, 
  CASE 
    WHEN a.stdftsvct = 'AM' or a.stdftsvct = 'MR' THEN 'MR'
    ELSE a.stdftsvct
  END,  
  CASE 
    WHEN a.stdftsvct = 'BS' THEN 'Body Shop'
    WHEN a.stdftsvct = 'AM' or stdftsvct = 'MR' THEN 'Service'
    WHEN a.stdftsvct = 'QL' THEN 'Quick Lane'
    WHEN a.stdftsvct = 'RE' THEN 'Detail'
  END,
  true, b.minArk,
  (SELECT datekey FROM day WHERE thedate = minArk),
  (SELECT thedate FROM day WHERE datetype <> 'date'),
  (SELECT datekey FROM day WHERE datetype <> 'date')
FROM stgArkonaSDPTECH a
LEFT JOIN (
  SELECT ptco#, pttech, coalesce(MIN(ptdate), curdate()) AS minArk
  FROM stgArkonaSDPRDET
  WHERE pttech = @tech
  GROUP BY ptco#, pttech) b ON a.sttech = b.pttech
WHERE a.sttech = @tech
  AND a.stco# = @store;

INSERT INTO keymapdimtech
SELECT techkey, storecode, technumber, active, flagdeptcode,
  laborcost, employeenumber
FROM dimtech
WHERE technumber = @tech
  AND storecode = @store;
  
INSERT INTO bridgeTechGroup (TechKey, WeightFactor)        
SELECT a.techkey, b.WeightFactor
FROM dimtech a
LEFT JOIN (
  SELECT n/100.0 AS weightfactor
  FROM tally
  WHERE n BETWEEN 1 AND 100) b ON 1 = 1
WHERE a.technumber = @tech
  AND a.storecode = @store;    
--/ departmental --------------
-- people ---------------------
SELECT * -- new techs
FROM stgArkonaSDPTECH a
LEFT JOIN dimtech b ON a.stco# = b.storecode AND a.sttech = b.technumber
WHERE b.storecode IS NULL 
  AND a.stactv <> 'n'
  AND a.stco# <> 'ry3' 
  AND a.sttech <> '521' 
  

SELECT * FROM edwEmployeeDim WHERE employeenumber = '1142910'

-- to check for earliest flag date vs hire date, comment out the INSERT 
-- AND uncomment c.hiredate  
--DELETE FROM bridgetechgroup WHERE techkey IN (SELECT techkey FROM dimtech WHERE technumber = '633');
--DELETE FROM keymapdimtech WHERE techkey = (SELECT techkey FROM dimtech WHERE technumber = '633');
--DELETE FROM dimtech WHERE technumber = '633';
DECLARE @store string;
DECLARE @tech string;
DECLARE @empkey integer;
@store = 'RY1';
@tech = '630';
@empkey = 2027;
INSERT INTO dimtech (storecode, employeenumber, name, description, technumber, active,
  laborcost, flagDeptCode, flagDept, CurrentRow, techkeyfromdate, 
  techkeyfromdatekey, techkeythrudate, techkeythrudatekey)  
SELECT a.stco#, c.employeenumber, c.name, a.stname, a.sttech, true, a.stlrat, 
  CASE 
    WHEN a.stdftsvct = 'AM' or a.stdftsvct = 'MR' THEN 'MR'
    ELSE a.stdftsvct
  END,  
  CASE 
    WHEN a.stdftsvct = 'BS' THEN 'Body Shop'
    WHEN a.stdftsvct = 'AM' or stdftsvct = 'MR' THEN 'Service'
    WHEN a.stdftsvct = 'QL' THEN 'Quick Lane'
    WHEN a.stdftsvct = 'RE' THEN 'Detail'
  END,
  true, b.minArk,
  (SELECT datekey FROM day WHERE thedate = minArk),
  (SELECT thedate FROM day WHERE datetype <> 'date'),
  (SELECT datekey FROM day WHERE datetype <> 'date')--, c.hiredate
FROM stgArkonaSDPTECH a
LEFT JOIN (
  SELECT ptco#, pttech, coalesce(MIN(ptdate), curdate()) AS minArk
  FROM stgArkonaSDPRDET
  WHERE pttech = @tech
  GROUP BY ptco#, pttech) b ON a.sttech = b.pttech
LEFT JOIN edwEmployeeDim c ON 1 = 1
  AND c.employeekey = @empkey 
WHERE a.sttech = @tech
  AND a.stco# = @store; 
   
INSERT INTO keymapdimtech
SELECT techkey, storecode, technumber, active, flagdeptcode,
  laborcost, employeenumber
FROM dimtech
WHERE technumber = @tech
  AND storecode = @store;

INSERT INTO bridgeTechGroup (TechKey, WeightFactor)        
SELECT a.techkey, b.WeightFactor
FROM dimtech a
LEFT JOIN (
  SELECT n/100.0 AS weightfactor
  FROM tally
  WHERE n BETWEEN 1 AND 100) b ON 1 = 1
WHERE a.technumber = @tech
  AND a.storecode = @store;  
--/ people  
--/ new techs -----------------------------------------------------------------------
-- xfmDimTech -----------------------------------------------------------------------
DROP TABLE xfmTechDim;
CREATE TABLE xfmTechDim ( 
  storecode CIChar( 3 ),
  technumber CIChar( 3 ),
  description CIChar( 20 ),
  laborcost Money,
  password CIChar( 6 ),
  employeenumber CIChar( 7 ),
  active logical,
  cert CIChar( 10 ),
  flagdeptcode CIChar( 2 )) IN DATABASE;
--/ xfmDimTech -----------------------------------------------------------------------      
-- type 2 ---------------------------------------------------------------------------
-- initial load,  DO i 1. generate a techkey based
-- ON WHEN the salary changed, AND THEN UPDATE ALL the tables with a techkey
-- to use the new techkey, OR 2. just start now, bang, forget laborcost history
-- AND move forward
-- the answer IS 2
--this script IS about generating a know starting point
--the type 2 changes to laborcost are NOT broken out, but SET to whatever IS current
-- 5/21 exclude Nokelby (507)(flagdeptcode/flagdept:: NA/NA) he should NOT be included IN a census

DELETE FROM xfmTechDim;
INSERT INTO xfmTechDim
SELECT stco#, sttech, stname, stlrat, stpwrd, stpyemp#, 
  CASE 
    WHEN stactv = 'n' THEN false
    ELSE true
  END, stcrt#, stdftsvct
FROM stgArkonaSDPTECH 
WHERE ( -- exclude list
  (stco# = 'ry1' AND sttech NOT IN ('m1','m15','m16','m17','m3','m6','tc','523','530','632','635',
    '521','628','d03','d05','d10','d14','d15','507'))
    OR (stco# = 'ry2' AND sttech NOT IN ('042','176','200','21','28','495','698','980')))
  AND stco# <> 'ry3';  
  
  
SELECT Utilities.DropTablesIfExist('#Type2') FROM system.iota;  
-- DROP TABLE #Type2;       
SELECT b.techkey,
  CASE WHEN a.active = false THEN 'active to inactive ' ELSE '' END 
  +
  CASE WHEN a.laborcost <> b.laborcost THEN 'laborcost ' ELSE '' END 
  + 
  CASE WHEN b.flagdeptcode <> CASE WHEN a.flagdeptcode = 'AM' THEN 'MR' ELSE a.flagdeptcode END THEN 'flagdept ' ELSE '' END 
  AS RowChangeReason,
  a.*
INTO #Type2  
FROM xfmTechDim a
INNER JOIN dimtech b ON a.storecode = b.storecode
  AND a.technumber = b.technumber
WHERE b.currentrow = true  
  AND b.active = true
  AND (
    a.active = false 
    OR a.laborcost <> b.laborcost
    OR b.flagdeptcode <> 
      CASE
        WHEN a.flagdeptcode = 'AM' THEN 'MR'
        ELSE a.flagdeptcode
      END);  
      
UPDATE dimtech
SET laborcost = 19.80
WHERE techkey = 57;  
UPDATE dimtech
SET laborcost = 12
WHERE techkey = 76; 
UPDATE dimtech
SET laborcost = 12.60
WHERE techkey = 176;     
--/ type 2 ---------------------------------------------------------------------------
