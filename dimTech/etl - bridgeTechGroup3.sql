/* 5/30, regroup, pull out the necessary pieces AND move ON */
/* 
*/   
DROP TABLE #weight;
SELECT l.ptco#, l.ptro#, l.ptline, l.ptlhrs AS LineHours,  
  t.pttech, t.ptlhrs AS FlagHours, t.FlagDate, round(t.ptlhrs/l.ptlhrs, 2) AS weight
INTO #weight  
FROM zline l
LEFT JOIN ztech t ON l.ptco# = t.ptco#
  AND l.ptro# = t.ptro#
  AND l.ptline = t.ptline
WHERE l.ptlhrs <> 0;
  
-- need to include ptco# because this IS using pttech, NOT techkey
DROP TABLE zkey;
CREATE TABLE zkey (
  theKey autoinc,
  pttech cichar(3),
  weight double,
  ptco# cichar(3));
  
INSERT INTO zkey (ptco#, pttech, weight)  
SELECT ptco#, pttech, weight
FROM (
  SELECT DISTINCT ptco#, ptro#, ptline, pttech, flagdate, weight
  FROM #weight) a
GROUP BY ptco#, pttech, weight;  

SELECT * FROM zkey
SELECT COUNT(*) FROM zkey

EXECUTE PROCEDURE sp_zaptable('bridgeTechGroup');
INSERT INTO bridgeTechGroup 
SELECT k.thekey, t.techkey, w.weight, w.ptco#
FROM #weight w
LEFT JOIN zkey k ON w.ptco# = k.ptco# -- added ptco# to join
  AND w.pttech = k.pttech
  AND w.weight = k.weight
LEFT JOIN dimTech t ON w.ptco# = t.storecode
  AND w.pttech = t.technumber 
GROUP BY k.thekey, t.techkey, w.weight, w.ptco#
  
-- 5/30 ok, now that tgkey IS done
DROP TABLE #jon;
SELECT a.ptco#, a.ptro#, a.ptline, b.pttech, b.FlagDate, a.LineHours, b.TechLineHours, 
  round(b.TechLineHours/a.LineHours, 2)as weight,
  c.techkey, d.*
INTO #jon  
FROM (
  SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS LineHours 
  FROM zline 
  WHERE ptlhrs <> 0
--    AND LEFT(ptro#, 5) = '16088'
  GROUP BY ptco#, ptro#, ptline) a
LEFT JOIN (
    SELECT ptco#, ptro#, ptline, pttech, FlagDate, SUM(ptlhrs) AS TechLineHours
    from ztech 
    WHERE ptlhrs <> 0
--      AND LEFT(ptro#, 5) = '16088'
    GROUP BY ptco#, ptro#, ptline, pttech, FlagDate) b ON a.ptco# = b.ptco#
  AND a.ptro# = b.ptro#
  AND a.ptline = b.ptline
LEFT JOIN dimTech c ON b.ptco# = c.storecode
  AND b.pttech = c.technumber 
LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
  AND round(b.TechLineHours/a.LineHours, 2) = weightfactor 

-- 5/31  
-- NULL techgroupkey : none
SELECT *
FROM #jon
WHERE techgroupkey IS NULL   

SELECT COUNT(*) FROM #jon
-- unique: ptco#, ptro#, ptline, flagdate, linehours, techgroupkey
SELECT ptco#, ptro#, ptline, flagdate, linehours, techgroupkey
FROM #jon
GROUP BY ptco#, ptro#, ptline, flagdate, linehours, techgroupkey
HAVING COUNT(*) > 1

select ptco#, ptro#, ptline, flagdate, linehours, (linehours*b.weightfactor), t.technumber, t.description, t.name
FROM #jon j
LEFT JOIN bridgeTechGroup b ON j.techgroupkey = b.techgroupkey
LEFT JOIN dimTech t ON b.techkey = t.techkey
WHERE flagdate BETWEEN '05/01/2011' AND '05/14/2011'
  AND t.name IS NOT NULL 
ORDER BY j.ptro#  
  
SELECT ptco#, ptro#, source, status
--SELECT *
FROM zalltheros 
WHERE finalclosedate = '12/31/9999' 
GROUP BY ptco#, ptro#, source, status
  
SELECT *
FROM zkey
/******************************************************************************/
/******************************************************************************/
ptco# should NOT be IN bridgeTG, because it IS using techKey
/******************************************************************************/
/******************************************************************************/




