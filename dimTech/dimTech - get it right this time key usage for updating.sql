DECLARE @keyname string;
DECLARE @key string; 
DECLARE @key2 string;
DECLARE @parent string;
DECLARE @stmt string;
DECLARE @ParentCur CURSOR AS
  SELECT * FROM tmpParent;
@keyname = 'techkey';
@key = '211';

DROP TABLE tmpParent;
CREATE TABLE tmpParent (
  parent cichar(50));

DROP TABLE tmpParentKeyCount; 
CREATE TABLE tmpParentKeyCount (
  theKey cichar(24),
  parent cichar(50),
  keycount integer);  
  
@stmt = 'INSERT INTO tmpParent SELECT left(parent, 50) FROM system.columns WHERE name = ' +  '''' + @keyname + '''';
EXECUTE immediate @stmt;

OPEN @ParentCur;
TRY
  WHILE FETCH @ParentCur DO
    @parent = @ParentCur.parent;
    @stmt = 'insert INTO tmpParentKeyCount SELECT '  +  '''' + @keyname + ''''+ ',' + '''' + TRIM(@parent) + '''' + ', COUNT(*) FROM ' + '' + @Parent + '' 
      + ' WHERE cast( ' + '' + @keyname + '' + ' AS sql_char) = ' + '''' + @key + '''';
    EXECUTE immediate @stmt;
  END WHILE;
FINALLY
  CLOSE @ParentCur;
END TRY;



/*
this misses
  dimTechGroup
  factTechLineeFlagDateHours
  keyMapBridgeTechGroup 
*/ 

--there will be many techgroup records for each techkey
--SELECT techgroupkey FROM bridgetechgroup WHERE techkey = 49
--SELECT * FROM tmpParentKeyCount;
DELETE FROM  tmpParent;
@keyname = 'techgroupkey';
@stmt = 'INSERT INTO tmpParent SELECT left(parent, 50) FROM system.columns WHERE name = ' +  '''' + @keyname + '''';
EXECUTE immediate @stmt;
OPEN @ParentCur;
TRY
  WHILE FETCH @ParentCur DO
    @parent = @ParentCur.parent;
    @stmt = 'insert INTO tmpParentKeyCount SELECT '  +  '''' + @keyname + ''''+ ',' + '''' + TRIM(@parent) + '''' + ', COUNT(*) FROM ' + '' + @Parent + '' 
--      + ' WHERE cast( ' + '' + @keyname + '' + ' AS sql_char) = ' + '''' + @key + '''';
      + ' WHERE CAST(' + '' + @keyname + '' + ' AS sql_char)  in (SELECT cast(techgroupkey as sql_char) FROM bridgetechgroup WHERE techkey = CAST(' + '''' + @key + '''' + 'as sql_integer))';
    EXECUTE immediate @stmt;
  END WHILE;
FINALLY
  CLOSE @ParentCur;
END TRY;

/*

the 2 tables that might need to be updated to a new key are factTechFlagHoursByDay AND factTechLineFlagDateHours
get the min/MAX dates for the existing techkey IN each of the tables, 
make the techkeyfromdate for the new inactive key MAX existing date + 1
*/

SELECT a.*, b.minDate, b.maxDate, c.minDate, c.maxDate  
FROM tmpParentKeyCount a
LEFT JOIN (
  SELECT 'factTechFlagHoursByDay' as theTable, MIN(b.thedate) AS minDate, MAX(b.thedate) AS maxDate
  FROM factTechFlagHoursByDay a
  LEFT JOIN day b ON a.flagdatekey = b.datekey
  WHERE techkey = CAST(@key AS sql_integer)) b ON a.parent = b.theTable collate ads_default_ci
LEFT JOIN (  
SELECT 'factTechLineFlagDateHours' as theTable, MIN(b.thedate) AS minDate, MAX(thedate) AS maxDate 
FROM factTechLineFlagDateHours a
LEFT JOIN day b ON a.flagdatekey = b.datekey
WHERE techgroupkey IN (
  SELECT techgroupkey
  FROM bridgetechgroup
  WHERE techkey = CAST(@key AS sql_integer))) c ON a.parent = c.theTable collate ads_default_ci
  
/*
DROP table tmpParent;
DROP TABLE tmpParentKeyCount;

SELECT MIN(b.thedate), MAX(thedate) 
FROM factTechLineFlagDateHours a
LEFT JOIN day b ON a.flagdatekey = b.datekey
WHERE techgroupkey IN (
  SELECT techgroupkey
  FROM bridgetechgroup
  WHERE techkey = 87)
  
SELECT b.thedate, a.*
FROM factTechLineFlagDateHours a
LEFT JOIN day b ON a.flagdatekey = b.datekey

WHERE techgroupkey IN (
  SELECT techgroupkey
  FROM bridgetechgroup
  WHERE techkey = 146)  
  AND b.thedate > '08/23/2011'  
  
  
*/
-- SELECT * FROM tmpparent

