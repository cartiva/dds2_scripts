ALTER PROCEDURE xfmDimTech
   ( 
      dupTech Integer OUTPUT,
      missingTech Integer OUTPUT,
      newTech Integer OUTPUT
   ) 
BEGIN 
/*
dimTech: NK: storecode/technumber
         CK: storecode/employeenumber (unique WHERE employeenumber <> '')
  conformed attributes: dimTech.name = edwEmployeeDim.name & dimTech.employeenumber = edwEmployeeDim.employeenumber
                   there will be no records IN dimTech WHERE the name for an employeenumber
                   IS NOT the same AS dimTech.name for that employeenumber

                  Conformed attributes
                  each dimEmp record may exist IN 1 and only 1 dimTech record
                  each dimTech record must exist IN 1 and only 1 dimEmp record
                     dimEmp              dimTech 
                   -----------       --------------
                     |store |          | store |
                  NK |emp#  |-|------|o| emp#  | FK
                     |name  |          | name  |
           currentrow = true|
           
        Type 1 Changes:  Name, Description, LaborCost      
6/7/12
  Dustin Grohs (tech 24) name changed IN dimEmp
  that change did NOT take affect IN dimTech - wtf - name change a type 1 change?       
  
???????????? WHERE ed.currentrow = true ??????????????????????????  
should pick it up AND UPDATE (type 1: dimTech.name)

the script Z:\E\DDS2\Scripts\Maintenance\EmployeeDim\maintEmployeeDim-NameChange.sql
will fix it IF the name change was only a correction

RI violated: for each record IN dimTech (store/emp#/name) there IS exactly one record IN dimEmp(store/emp#/name WHERE currentrow = true)
-- this would have picked it up
-- why am i NOT doing this check 
SELECT *
FROM dimtech a
LEFT JOIN edwEmployeeDim b ON a.storecode = b.storecode
  AND a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE a.name <> b.name 

the issue IS the above RI

the issue IS that i am NOT using the name field FROM sdptech AS name but AS description
therefore the name IN IN xfmTechDim IS necessarily the current name FROM edwEmployeeDim 
becomes a simple type 1 UPDATE to dimTech.name

9/6/12
  new techs aren't populating AS empkeys, ALL the Dtechs, need to see what others have been missed
    the problem IS new techs being added, but the stpyemp# field (Payroll Emp Number)
    NOT being populated therefor, the script thinks it's a non-employee tech#
    
    updated dimtech manually, this has only been happening since 8/28, so ALL 
    peripheral tables depending upon dimtech will have the incorrect records deleteted
    to be replaced with correct records BY the nightly run that goes back to 45 days of ros
  
  what tables get populated based ON the empkey being derived FROM the techkey
    rptFlagHours
  shit, it's also tables with both techkey AND employeekey
    factTechFlagHoursByDay
    
so, modify existing dimtech rows, techkey does NOT change    
    
i'm actually going to have to flag new techs AND make sure the info IS correct
there IS no way for the software to know IF the new tech record IS GROUP OR employee 

5/18/13 get it right this time

type 1:
  name
  description
type 2:
  active
  laborcost
  flagdeptcode  
  
NK: storecode;technumber;employeenumber;active;flagdeptcode;laborcost  

1. raw tests against stgArkonaSDPTECH
2. valid data FROM stgArkonaSDPTECH INTO xfmDimTech
3. new techs to dimTech
4. type2 to dimTech
5. type1 to dimTech
    
EXECUTE PROCEDURE xfmDimTech()
*/

DECLARE @dupTech integer;
DECLARE @missingTech integer;
DECLARE @newTech integer;
DECLARE @NowTS timestamp;

-- tests against stgArkonaSDPTECH ----------------------------------------------

-- this one IS different, NOT bad data IN SDPTECH, but missing data IN SDPTECH
@missingTech =  ( -- techs IN sdprdet NOT IN sdptech, why doesn't the tech exist Arkona yet?
  SELECT COUNT(*)
  FROM (
    SELECT DISTINCT pttech
    FROM tmpSDPRDET a
    WHERE ptltyp = 'L'
      AND ptcode = 'TT'
      AND pttech <> '533' -- ro FROM the future 320481 7/4/2013
      AND NOT EXISTS (
        SELECT 1
        FROM stgArkonaSDPTECH
        WHERE sttech = a.pttech)) b);
        
-- verify that base data IS usable
-- holy shit, this IS NOT even necessary, arkona will NOT allow a dup store/tech
@dupTech = (-- test SDPTECH for unique store/tech#
  SELECT COUNT(*)
  FROM (
    SELECT stco#, sttech
    FROM stgArkonaSDPTECH
    GROUP BY stco#, sttech
    HAVING COUNT(*) > 1) a);



/// tests against stgArkonaSDPTECH ----------------------------------------------

-- tech#s to exclude FROM xfmDimTech ---------------------------------------------
-- based ON being current RIGHT MEOW, these are the stgArkonaSDPTECH rows that do 
-- not currently exist in dimTech, and will never
-- generate a list of tech numbers to exclude
-- FROM inserting INTO xfmTechDim
-- ok, this IS the golden exclude list
-- 5/21 exclude Nokelby (507)(flagdeptcode/flagdept:: NA/NA) he should NOT be included IN a census
DELETE FROM xfmTechDim;
INSERT INTO xfmTechDim
SELECT stco#, sttech, stname, stlrat, stpwrd, stpyemp#, 
  CASE 
    WHEN stactv = 'n' THEN false
    ELSE true
  END, stcrt#, stdftsvct
FROM stgArkonaSDPTECH 
WHERE ( -- exclude list
  (stco# = 'ry1' AND sttech NOT IN ('m1','m15','m16','m17','m3','m6','tc','523','530','632','635',
    '521','628','d03','d05','d10','d14','d15','507'))
    OR (stco# = 'ry2' AND sttech NOT IN ('042','176','200','21','28','495','698','980')))
  AND stco# <> 'ry3';   
/// tech#s to exclude FROM xfmDimTech ---------------------------------------------  

-- new tech -----------------------------------------------------------------------
-- ALL i need to DO IS generate an email, no way to programatically ADD a new tech
-- sdptech data IS too non specific to determine departmental vs person

@newTech = (
  SELECT COUNT(*)
  FROM xfmTechDim a
  LEFT JOIN dimtech b ON a.storecode = b.storecode
    AND a.technumber = b.technumber
  WHERE b.storecode IS NULL);
// new tech -------------------------------------------------------------------//

-- type 2 -------------------------------------------------------------------------
-- INNER JOIN eliminates the new techs
-- active
  -- active to inactive
  -- inactive to active, dimtech = inactive, sdptech = active
  -- hmm, IN arkona, there IS no way to "reactivate a tech", can NOT reuse a technumber IN a store
  
SELECT Utilities.DropTablesIfExist('#Type2') FROM system.iota;  
-- DROP TABLE #Type2        
SELECT b.techkey,
  CASE WHEN a.active = false THEN 'active to inactive ' ELSE '' END 
  +
  CASE WHEN a.laborcost <> b.laborcost THEN 'laborcost ' ELSE '' END 
  + 
  CASE WHEN b.flagdeptcode <> CASE WHEN a.flagdeptcode = 'AM' THEN 'MR' ELSE a.flagdeptcode END THEN 'flagdept ' ELSE '' END 
  AS RowChangeReason,
  a.*
INTO #Type2  
FROM xfmTechDim a
INNER JOIN dimtech b ON a.storecode = b.storecode
  AND a.technumber = b.technumber
WHERE b.currentrow = true  
  AND b.active = true
  AND (
    a.active = false 
    OR a.laborcost <> b.laborcost
    OR b.flagdeptcode <> 
      CASE
        WHEN a.flagdeptcode = 'AM' THEN 'MR'
        ELSE a.flagdeptcode
      END);
IF (SELECT COUNT(*) FROM #Type2) > 0 THEN 
-- UPDATE old record
  UPDATE dimTech
  SET RowChangeDate = cast(@NowTS AS sql_Date),
      RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = cast(@NowTS AS sql_Date)),
      RowThruTS = @NowTS,
      CurrentRow = false,
      RowChangeReason = a.RowChangeReason,
      TechKeyThruDate = CAST(timestampadd(sql_tsi_day, -1, @NowTS) AS sql_date),
      TechKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST(timestampadd(sql_tsi_day, -1, @NowTS) AS sql_date))
  FROM #Type2 a
  INNER JOIN dimTech b ON a.techkey = b.techkey;   
-- INSERT new row  
  INSERT INTO dimtech (storecode, employeenumber, name, description, technumber,
    active, laborcost, flagdeptcode, flagdept, currentrow, rowfromts, rowthruts, 
    techkeyfromdate, techkeyfromdatekey, techkeythrudate, techkeythrudatekey)
  SELECT a.storecode, b.employeenumber, b.name, a.description, a.technumber, 
    a.active, a.laborcost, a.flagdeptcode,
    CASE 
      WHEN a.flagdeptcode = 'BS' THEN 'Body Shop'
      WHEN a.flagdeptcode = 'AM' or a.flagdeptcode = 'MR' THEN 'Service'
      WHEN a.flagdeptcode = 'QL' THEN 'Quick Lane'
      WHEN a.flagdeptcode = 'RE' THEN 'Detail'
    END, true, @NowTS, CAST(NULL AS sql_timestamp),
    CAST(@NowTS AS sql_date), (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date)),
    (SELECT thedate FROM day WHERE datetype = 'NA'),
    (SELECT datekey FROM day WHERE datetype = 'NA')    
  FROM #Type2 a
  INNER JOIN dimtech b ON a.techkey = b.techkey;
END IF;

INSERT INTO keymapdimtech
SELECT techkey, storecode, technumber, active, flagdeptcode,
  laborcost, employeenumber
FROM dimtech a
WHERE NOT EXISTS (
  SELECT 1 
  FROM keymapdimtech
  WHERE techkey = a.techkey);
  
INSERT INTO bridgeTechGroup (TechKey, WeightFactor)        
SELECT a.techkey, b.WeightFactor
FROM dimtech a
LEFT JOIN (
  SELECT n/100.0 AS weightfactor
  FROM tally
  WHERE n BETWEEN 1 AND 100) b ON 1 = 1
WHERE NOT EXISTS (
  SELECT 1
  FROM bridgeTechGroup
  WHERE techkey = a.techkey);
// type 2 ---------------------------------------------------------------------//

-- type 1 -------------------------------------------------------------------------
-- name
--current name FROM edwEmployeeDim 
UPDATE dimtech
SET name = b.name
--SELECT a.techkey, a.storecode, a.employeenumber, a.name, b.name
FROM dimtech a
LEFT JOIN (
  SELECT employeenumber, name
  FROM edwEmployeeDim
  WHERE currentrow = true) b ON a.employeenumber = b.employeenumber
WHERE a.currentrow = true
  AND a.active = true
  AND a.employeenumber <> 'NA'
  AND a.name <> b.name;
-- description
--current description FROM xfmTechDim
UPDATE dimtech
SET description = b.description
--SELECT a.techkey, a.storecode, a.employeenumber, a.description, b.description
FROM dimtech a
INNER JOIN xfmtechdim b ON a.storecode = b.storecode
  AND a.technumber = b.technumber
WHERE a.currentrow = true
  AND a.active = true
  AND a.description <> b.description;

// type 1 ---------------------------------------------------------------------//

INSERT INTO __output
SELECT @dupTech, @missingTech, @newTech FROM system.iota;



END;

