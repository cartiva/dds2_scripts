dimTech
type2 attributes: technumber 
                  flagdeptcode
                  flagdept
AND somehow the notion of changing FROM emptech to dept tech
                  
ADD rows:
  currentrow 
  RowChangeDate
  RowChangeDateKey
  RowFromTS
  RowThruTS
  RowChangeReason
  techKeyFromDate
  techKeyFromDateKey
  techKeyThruDate
  techKeyThruDateKey
  
  thinking i also want a rowcreatedTS, have it for type2 rows (RowFromTS) but NOT
  for new rows, maybe RowFromTS for ALL rows?
  
  WHEN i get to restructuring the executables, will incorporate the notion of batches, runs, ...
  
DROP index dimtech.nk;
DROP index dimtech.pk;
DROP index keymapDimTech.nk;

                                    
-- so what the fuck DO i DO for startdates?

SELECT a.storecode, a.technumber, a.employeenumber, coalesce(a.name, a.description), b.hiredate, b.termdate
FROM dimtech a
LEFT JOIN edwEmployeeDim b ON a.employeenumber = b.employeenumber
  AND b.currentrow = true
  
techkeythru remains 12/31/9999 until the row IS updated to a new key
meaning i DO NOT give a shit about WHEN someone IS fired
thinking AS long AS there are flag hours ON a day for a tech it IS valid
but DO i need a notion of active/inactive
ie, how many active techs IN each dept?
that IS certainly a question that will be asked, DO NOT want to determine date
of last flag hours to answer that one

so the list of currently employed techs ON a given date may NOT include techs for which there are flag
hours ON that date

so, WHEN does a tech become inactive
ok ok ok ok, use active/termdate ONLY for shit LIKE the above question
the duration of tech activity has nothing to DO with filtering flag hours
IF a flag line EXISTS with a tech number, THEN BY golly that tech flagged those
hours ON that day, whether payroll says they were active OR termed

IS a departmental tech good forever?
yeah, guess so, could get typed 2 to another dept


only 1 techkey can be active for an employeenumber at a time;

so duration of techkeyfrom/thru IS only meaningful for the COUNT of techs currently employed for a dept
but what about type 2
which IS the correct techkey for inclusion IN fact tables

ok smartass, give me an example
a person tech that has flaghours beyond their termdate
here you go:
SELECT a.technumber, a.name, c.ro, e.termdate, d.thedate
FROM dimtech a
INNER JOIN bridgetechgroup b ON a.techkey = b.techkey
INNER JOIN facttechlineflagdatehours c ON b.techgroupkey = c.techgroupkey
INNER JOIN day d ON c.flagdatekey = d.datekey
LEFT JOIN edwEmployeeDim e ON a.employeenumber = e.employeenumber
  AND e.currentrow = true
  AND e.termdate <> '12/31/9999'
WHERE a.employeenumber IS NOT NULL 
  AND d.thedate > e.termdate
  AND a.technumber IN ('620','621','627')
--GROUP BY a.technumber, a.name


SELECT a.storecode, a.technumber, a.name, e.termdate, MIN(ro), MAX(ro), MIN(thedate), MAX(thedate)
FROM dimtech a
INNER JOIN bridgetechgroup b ON a.techkey = b.techkey
INNER JOIN facttechlineflagdatehours c ON b.techgroupkey = c.techgroupkey
INNER JOIN day d ON c.flagdatekey = d.datekey
LEFT JOIN edwEmployeeDim e ON a.employeenumber = e.employeenumber
  AND e.currentrow = true
  AND e.termdate <> '12/31/9999'
WHERE a.employeenumber IS NOT NULL 
  AND d.thedate > e.termdate
  AND a.technumber IN ('620','621')
GROUP BY a.storecode, a.technumber, a.name, e.termdate

hmm, this might provision a tech changing FROM employee to dept (FROM flagdept, 620 & 621
  were the technumbers that changed FROM emp to dept that i blew off)
can a tech change FROM employee to dept?
so, 621 changes FROM ted brown to recontech3 ON 7/31/12
    620 changes FROM steve loken to recontech2 ON 11/10/10
so i guess i need to test for clockhours > termdate, AND determine IF it IS just sloppy
HR OR the tech has changed    

so besides those 2 have to also DO the type2 dance ON gulled

-- 1/15/13
-- 1
initial rowfromts values: hiredate for tech, 1/1/2009 for dept
tkfrom: hiredate for tech, 1/1/2009 for dept
tkthru: termdate for tech OR 12/31/9999 (NOT NULL)
-- 2
DO type 2 for 620,621,gulled
-- 3
UPDATE ALL fact tables that use techkey for new techkeys
hoping to be able to DO it based ON dates
-- non NULL fields
PK: techkey
NK: storecode, technumber & ???

SO, WHAT the fuck IS the difference BETWEEN ekfrom/thru AND rowfrom/thru?
row IS physical meta data
IN the initial load, they will necessarily be the same
duh, row IS ts, tk IS date

-- DO NOT fucking JOIN ON technumber, joins ON techkey (same thing for edwEmployeeDim)

WHERE i think i am at IS tkfrom/thru used only for assessing population at a point IN time
i DO NOT care how many dept techs exist at any point in time, those are relevant
only IN the context of flag hours

tkfrom/thru irrelevant IN the context of flaghours ???
yeah, well, i must determine the correct techkey for a given fact record

at any point IN time there IS only one techkey that IS valid for a technumber
techkey used to determine dept & individual flag hours
technumber IS NOT the natural key because it IS NOT durable: 620, 621


-- thinking it's ALL going to be manual, which will be ok, IF the susbsequent
-- dependencies are adjusted to exempt the appropriate records
so AS i look at stgSDPTECH for a technumber look for any changes IN 
   1. new storecode/tech#
   2. changes
      a. stname
      b. stpyemp#
      c. stactv
      d. stdftsvct
populating fact tables
  with techkey        
620: techkey: 101 termdate 11/05/2010  depttech 7/23/12 -> 9/25/12
621: techkey: 70  termdate 04/03/2012  depttech 7/31/12  -> curdate     
  
so rowthruts IS the actual ts WHEN the type2 change IS entered IN the table  


SELECT parent
FROM system.columns
WHERE name = 'techkey'

x dimTech 
1 keyMapDimTech  
2 bridgeTechGroup  done IN stgRO      
3 keyMapBridgeTechGroup                                                                                                                                                                                           
4 factTechFlagHoursByDay                                                                                                                                                                                  
x zbridgeTechGroup      


-- etl - dimtechType2Script does ALL the backfitting for 620 & 621

now gulled
ok, 
he has flaghours as 215 since 11/11
he has 0 flaghours AS D17
                                               
-- 1/16
thinking name, emp# etc should be NA instead of NULL
look at dimtech for NULL flagdept
SELECT *
FROM dimtech WHERE flagdeptcode IS NULL 

-- ros with tech NOT IN dimtech
-- ok, no flaghours for techs with NULL flagdept
-- syverson was fixed before
SELECT b.thedate, a.*, d.* 
FROM facttechlineflagdatehours a
INNER JOIN day b ON a.flagdatekey = b.datekey
INNER JOIN bridgetechgroup c ON a.techgroupkey = c.techgroupkey
INNER JOIN dimtech d ON c.techkey = d.techkey
  AND d.flagdeptcode IS NULL 
ORDER BY thedate  
  
syverson 2 different tech numbers, 599, 628
SELECT a.ptro#, a.ptdate, a.pttech, b.ptro#, b.ptdate, b.pttech 
FROM stgarkonasdprdet a
INNER JOIN stgarkonasdprdet b ON a.ptdate = b.ptdate
  AND b.pttech = '599'
WHERE a.pttech = '628'  
                                                                                                



  
  