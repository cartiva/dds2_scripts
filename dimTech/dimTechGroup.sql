SELECT DISTINCT ptco#, pttech
INTO #wtf
FROM xfmroinitial 
WHERE length(TRIM(pttech)) > 0  
  AND ptco# IN ('ry1','ry2','ry3')



SELECT *
FROM dimtech x,  
(
  select n/100.0 AS weightfactor
  FROM tally
  WHERE n < 101) y
ORDER BY technumber, weightfactor  


SELECT *
FROM dimtech a
LEFT JOIN #wtf b ON a.storecode = b.ptco#
  AND a.technumber = b.pttech
WHERE b.pttech IS NULL   



CREATE TABLE zbridgeTechGroup ( 
      TechGroupKey AutoInc,
      TechKey Integer,
      WeightFactor Double( 8 )) IN DATABASE;
INSERT INTO zbridgeTechGroup
SELECT * FROM bridgeTechGroup

DELETE FROM bridgeTechGroup;

EXECUTE PROCEDURE sp_packtable('bridgeTechGroup')

INSERT INTO bridgeTechGroup (TechKey, WeightFactor)
SELECT techkey, weightfactor
FROM dimtech x,  
(
  select n/100.0 AS weightfactor
  FROM tally
  WHERE n < 101) y
  


DROP TABLE bridgeTechGroup;  
CREATE TABLE bridgeTechGroup ( 
      TechGroupKey AutoInc,
      TechKey Integer,
      WeightFactor Double( 8 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'bridgeTechGroup',
   'bridgeTechGroup.adi',
   'TECHKEY',
   'TechKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'bridgeTechGroup',
   'bridgeTechGroup.adi',
   'TECHGROUPKEY',
   'TechGroupKey',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'bridgeTechGroup',
   'bridgeTechGroup.adi',
   'NK',
   'TechKey;WeightFactor',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'bridgeTechGroup',
   'bridgeTechGroup.adi',
   'WEIGHTFACTOR',
   'WeightFactor',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'bridgeTechGroup', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'bridgeTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'bridgeTechGroup', 
   'Table_Primary_Key', 
   'TECHGROUPKEY', 'APPEND_FAIL', 'bridgeTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'bridgeTechGroup', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'bridgeTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'bridgeTechGroup', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'bridgeTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'bridgeTechGroup', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'bridgeTechGroupfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'bridgeTechGroup', 
      'TechGroupKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'bridgeTechGroupfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'bridgeTechGroup', 
      'TechKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'bridgeTechGroupfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'bridgeTechGroup', 
      'WeightFactor', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'bridgeTechGroupfail' ); 

INSERT INTO bridgeTechGroup (TechKey, WeightFactor)
SELECT techkey, weightfactor
FROM dimtech x,  
(
  select n/100.0 AS weightfactor
  FROM tally
  WHERE n < 101) y;      
  
  
DROP TABLE dimTechGroup;
CREATE TABLE dimTechGroup ( 
      TechGroupKey Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimTechGroup',
   'dimTechGroup.adi',
   'TECHGROUPKEY',
   'TechGroupKey',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'dimTechGroup', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'dimTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'dimTechGroup', 
   'Table_Primary_Key', 
   'TECHGROUPKEY', 'APPEND_FAIL', 'dimTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'dimTechGroup', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'dimTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'dimTechGroup', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'dimTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'dimTechGroup', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'dimTechGroupfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimTechGroup', 
      'TechGroupKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'dimTechGroupfail' ); 
      
INSERT INTO dimTechGroup
SELECT TechGroupKey
FROM bridgeTechGroup;       