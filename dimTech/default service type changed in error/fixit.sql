on 9/3 ben AND andrew pointed out that tim oneil does NOT 
show up on the production page under the weekly view
for the week of 8/24, though he does show up IN the monthly view
discovered onat on 8/20, someone changed his default service type
IN arkona to RE, which generated a type2 change IN dimtech
with a new techkey of 447

2 choices:
1. UPDATE ALL instances of techkey 447 everywher to 446 AND DELETE the
     447 row IN dimTech AND keyMapDimTech
2. keep 447, just edit dimTech so that 447 IS MR, ADD to the 
     rowchangereason of 446 a brief explanation   

going with #1, may have some unknown side effects, but leaves the
model more consistent

-- ALL tables with techkey AS a COLUMN
SELECT parent
FROM system.columns
WHERE name = 'techkey'
AND parent NOT LIKE 'zunu%';

UPDATE factRepairOrder
SET techKey = 446
WHERE techKey = 447;
UPDATE tmpFactRepairOrder
SET techKey = 446
WHERE techKey = 447;
UPDATE todayFactRepairOrder
SET techKey = 446
WHERE techKey = 447;
UPDATE tmpRoTechFlagCorKeys
SET techKey = 446
WHERE techKey = 447;
UPDATE todayRoTechFlagCorKeys
SET techKey = 446
WHERE techKey = 447;

DELETE 
FROM keymapDimTech
WHERE techkey = 447;

DELETE 
FROM dimTech
WHERE techKey = 447;

UPDATE dimTech
SET rowchangedate = CAST(NULL AS sql_date),
    rowchangeDateKey = CAST(NULL AS sql_integer),
    rowThruTS = CAST(NULL AS sql_timestamp),
    rowChangeReason = CAST(NULL AS sql_char),
    techKeyThruDate = '12/31/9999',
    techKeyThruDateKey = 7306, 
    currentRow = true
WHERE techKey = 446;

UPDATE scotest.tmpDeptTechCensus
  SET techkey = 446,
      flagDeptCode = 'MR'
WHERE firstname = 'timothy';
