/*
4/23/12
considering role based dimesions

DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'SDPTECH';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;  

4/25: NOT bringing over more fields, but added a few

*/
-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	  WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
  	WHEN data_type = 'numeric' THEN 'integer,'
  	WHEN data_type = 'timestmp' THEN 'timestamp,'
  	WHEN data_type = 'smallint' THEN 'integer,'
  	WHEN data_type = 'integer' THEN 'double,'
  	WHEN data_type = 'decimal' THEN 'double,'
  	WHEN data_type = 'date' THEN 'date,'
  	WHEN data_type = 'time' THEN 'cichar(8),'
  	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'SDPTECH' 
  AND column_name IN ('STCO#', 'STTECH', 'STNAME', 'STEMP#',  'STLRAT', 
    'STPWRD', 'STPYEMP#','STACTV', 'STCRT#', 'STDFTSVCT')
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'SDPTECH'  
  AND column_name IN ('STCO#', 'STTECH', 'STNAME', 'STEMP#',  'STLRAT', 
    'STPWRD', 'STPYEMP#','STACTV', 'STCRT#', 'STDFTSVCT')
AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'SDPTECH'  
  AND column_name IN ('STCO#', 'STTECH', 'STNAME', 'STEMP#',  'STLRAT', 
    'STPWRD', 'STPYEMP#','STACTV', 'STCRT#', 'STDFTSVCT')
AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaSDPTECH
-- 3. revise DDL based ON step 2, CREATE table
CREATE TABLE stgArkonaSDPTECH (
stco#      cichar(3),          
sttech     cichar(3),          
stname     cichar(20),         
stpyemp#   cichar(7),          
stactv     cichar(1)) IN database;     
      

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'SDPTECH';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename
  AND column_name IN ('STCO#', 'STTECH', 'STNAME', 'STEMP#',  'STLRAT', 
    'STPWRD', 'STPYEMP#','STACTV', 'STCRT#', 'STDFTSVCT'));
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce(
    (SELECT TRIM(column_name) + ', '  
      FROM syscolumns 
      WHERE ordinal_position = @j 
      AND table_name = @tableName 
  AND column_name IN ('STCO#', 'STTECH', 'STNAME', 'STEMP#',  'STLRAT', 
    'STPWRD', 'STPYEMP#','STACTV', 'STCRT#', 'STDFTSVCT')), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'SDPTECH';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce(
    (SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  
      FROM syscolumns 
      WHERE ordinal_position = @j 
      AND table_name = @tableName 
  AND column_name IN ('STCO#', 'STTECH', 'STNAME', 'STEMP#',  'STLRAT', 
    'STPWRD', 'STPYEMP#','STACTV', 'STCRT#', 'STDFTSVCT')), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaSDPTECH';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name
  AND name IN ('STCO#', 'STTECH', 'STNAME', 'STEMP#',  'STLRAT', 
    'STPWRD', 'STPYEMP#','STACTV', 'STCRT#', 'STDFTSVCT');
 
/**** 4/25 ****/
ALTER TABLE stgArkonaSDPTECH
ADD COLUMN stpwrd cichar(6)
ADD COLUMN stemp# cichar(9)
ADD COLUMN stcrt# cichar(10)
ADD COLUMN stdftsvct cichar(2)

